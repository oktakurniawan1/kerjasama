<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Survey Controller
*| --------------------------------------------------------------------------
*| Survey site
*|
*/
class Survey extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_survey');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* form filter data 
	*/
	public function filter_session_data()
	{
		$fields['question'] = $this->input->post('question');
		$this->session->set_userdata('filterdata_survey', $fields);
		$this->session->set_userdata('is_filtered_survey',TRUE);
		echo json_encode(['url' => base_url('administrator/survey')]);
	}

	public function clear_filter_session_data()
	{
		$this->session->unset_userdata('filterdata_survey');
		$this->session->unset_userdata('is_filtered_survey');
		echo json_encode(['url' => base_url('administrator/survey')]);
	}

	public function form_filter_data()
	{
		if (!$this->session->has_userdata('filterdata_survey')) {
			$fields['question'] = 'all';
			$this->session->set_userdata('filterdata_survey', $fields);
		}
			$data['question'] = [];
			$this->load->view('form-filter-data', $data);
	}
		/**
	* show all Surveys
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('survey_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if(!is_null($q)) $this->session->set_userdata('survey_filter',$q);
		if(!is_null($f)) $this->session->set_userdata('survey_field',$f);
		$this->session->set_userdata('_page',$offset);

		if(!is_null($this->input->get('reset_filter'))){
			$this->session->unset_userdata('survey_filter');
			$this->session->unset_userdata('survey_field');
		}
		$filter = $this->session->userdata('survey_filter');
		$field = $this->session->userdata('survey_field');
		$offset = $this->session->userdata('_page',$offset);

		$this->data['surveys'] = $this->model_survey->get($filter, $field, $this->limit_page, $offset);
		$this->data['survey_counts'] = $this->model_survey->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/survey/index/',
			'total_rows'   => $this->model_survey->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Survey List');
		$this->render('backend/standart/administrator/survey/survey_list', $this->data);
	}
	
	/**
	* Add new surveys
	*
	*/
	public function add()
	{
		$this->is_allowed('survey_add');

		$this->template->title('Survey New');
		$this->render('backend/standart/administrator/survey/survey_add', $this->data);
	}

	/**
	* Add New Surveys
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('survey_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('question', 'Question', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'question' => $this->input->post('question'),
			];

			
			$save_survey = $this->model_survey->store($save_data);
            

			if ($save_survey) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_survey;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/survey/edit/' . $save_survey, 'Edit Survey'),
						anchor('administrator/survey', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/survey/edit/' . $save_survey, 'Edit Survey')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/survey');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/survey');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Surveys
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('survey_update');

		$this->data['survey'] = $this->model_survey->find($id);

		$this->template->title('Survey Update');
		$this->render('backend/standart/administrator/survey/survey_update', $this->data);
	}

	/**
	* Update Surveys
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('survey_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('question', 'Question', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'question' => $this->input->post('question'),
			];

			
			$save_survey = $this->model_survey->change($id, $save_data);

			if ($save_survey) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/survey', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/survey/index/'.$this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/survey/index/'.$this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Surveys
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('survey_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'survey'), 'success');
        } else {
            set_message(cclang('error_delete', 'survey'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Surveys
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('survey_view');

		$this->data['survey'] = $this->model_survey->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Survey Detail');
		$this->render('backend/standart/administrator/survey/survey_view', $this->data);
	}
	
	/**
	* delete Surveys
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$survey = $this->model_survey->find($id);

		
		
		return $this->model_survey->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
				'question',
				];
		$data = $this->model_survey->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_survey->export2($data, 'file', $selected_field);

		/*
		$this->is_allowed('survey_export');

		$this->model_survey->export(
			'survey', 
			'survey',
			$this->model_survey->field_search
		);
		*/
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('survey_export');

		$this->model_survey->pdf('survey', 'survey');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('survey_export');

		$table = $title = 'survey';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_survey->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file survey.php */
/* Location: ./application/controllers/administrator/Survey.php */