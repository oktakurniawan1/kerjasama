<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Perjalun Controller
 *| --------------------------------------------------------------------------
 *| Perjalun site
 *|
 */
class Perjalun extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_perjalun');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * show all Perjaluns
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('perjalun_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('perjalun_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('perjalun_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('perjalun_filter');
			$this->session->unset_userdata('perjalun_field');
		}
		$filter = $this->session->userdata('perjalun_filter');
		$field = $this->session->userdata('perjalun_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['perjaluns'] = $this->model_perjalun->get($filter, $field, $this->limit_page, $offset);
		$this->data['perjalun_counts'] = $this->model_perjalun->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/perjalun/index/',
			'total_rows'   => $this->model_perjalun->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Perjalun List');
		$this->render('backend/standart/administrator/perjalun/perjalun_list', $this->data);
	}

	/**
	 * Add new perjaluns
	 *
	 */
	public function add()
	{
		$this->is_allowed('perjalun_add');

		$this->template->title('Perjalun New');
		$filter_prodi = get_filter_prodi();
		$this->data['filter_prodi'] = $filter_prodi;
		$this->render('backend/standart/administrator/perjalun/perjalun_add', $this->data);
	}

	/**
	 * Add New Perjaluns
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('perjalun_add', false)) {
			set_message(cclang('Anda tidak bisa mengubah, karena data sudah divalidasi', 'mhs_mobility'), 'warning');
			redirect_back();
		}

		$this->form_validation->set_rules('nip', 'NIP/NRP', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kd_prodi', 'Prodi/Unit', 'trim|required|max_length[8]');
		$this->form_validation->set_rules('judul_kegiatan', 'Judul Kegiatan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('keg_id', 'Jenis Kegiatan', 'numeric|required');
		$this->form_validation->set_rules('tujuan_instansi', 'Tujuan Instansi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('negara_tujuan', 'Negara Tujuan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'trim|required');
		$this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'trim|required');
		$this->form_validation->set_rules('perjalun_file_kegiatan_name', 'File Kegiatan', 'trim|required');
		$this->form_validation->set_rules('mou_ks', 'Nota Kesepahaman (MoU)', 'required');
		$this->form_validation->set_rules('perjalun_file_loa_name', 'File Undangan (LoA)', 'trim|required');
		$this->form_validation->set_rules('perjalun_file_relevansi_name', 'File Relevansi', 'trim|required');
		$this->form_validation->set_rules('perjalun_file_kerangka_name', 'File Kerangka Kerja', 'trim|required');
		$this->form_validation->set_rules('perjalun_file_rab_name', 'File RAB', 'trim|required');
		$this->form_validation->set_rules('perjalun_file_ktp_name', 'File KTP', 'trim|required');
		$this->form_validation->set_rules('perjalun_file_drh_name', 'File Drh', 'trim|required');
		// $this->form_validation->set_rules('perjalun_file_laporan_name', 'File Laporan', 'trim|required');
		$this->form_validation->set_rules('catatan', 'Catatan', 'trim|max_length[255]');
		$this->form_validation->set_rules('perjalun_file_sk_name', 'FIle SK', 'trim');


		if ($this->form_validation->run()) {
			$perjalun_file_kegiatan_uuid = $this->input->post('perjalun_file_kegiatan_uuid');
			$perjalun_file_kegiatan_name = $this->input->post('perjalun_file_kegiatan_name');
			$perjalun_file_loa_uuid = $this->input->post('perjalun_file_loa_uuid');
			$perjalun_file_loa_name = $this->input->post('perjalun_file_loa_name');
			$perjalun_file_relevansi_uuid = $this->input->post('perjalun_file_relevansi_uuid');
			$perjalun_file_relevansi_name = $this->input->post('perjalun_file_relevansi_name');
			$perjalun_file_kerangka_uuid = $this->input->post('perjalun_file_kerangka_uuid');
			$perjalun_file_kerangka_name = $this->input->post('perjalun_file_kerangka_name');
			$perjalun_file_rab_uuid = $this->input->post('perjalun_file_rab_uuid');
			$perjalun_file_rab_name = $this->input->post('perjalun_file_rab_name');
			$perjalun_file_ktp_uuid = $this->input->post('perjalun_file_ktp_uuid');
			$perjalun_file_ktp_name = $this->input->post('perjalun_file_ktp_name');
			$perjalun_file_drh_uuid = $this->input->post('perjalun_file_drh_uuid');
			$perjalun_file_drh_name = $this->input->post('perjalun_file_drh_name');
			$perjalun_file_laporan_uuid = $this->input->post('perjalun_file_laporan_uuid');
			$perjalun_file_laporan_name = $this->input->post('perjalun_file_laporan_name');
			$perjalun_file_sk_uuid = $this->input->post('perjalun_file_sk_uuid');
			$perjalun_file_sk_name = $this->input->post('perjalun_file_sk_name');
			//file pengantar
			$perjalun_file_pengantar_uuid = $this->input->post('perjalun_file_pengantar_uuid');
			$perjalun_file_pengantar_name = $this->input->post('perjalun_file_pengantar_name');
			// batas tanggal input
			$batas = strtotime('+2 week');
			$tgl_mulai = strtotime($this->input->post('tgl_mulai'));
			if ($tgl_mulai < $batas) {
				echo json_encode([
					'success' => false,
					'message' => cclang('Tidak bisa melanjutkan input data, karena tanggal input setidaknya 2 minggu sebelum tanggal pelaksanaan')
				]);
				exit;
			}


			$save_data = [
				'nip' => $this->input->post('nip'),
				'nama' => $this->input->post('nama'),
				'kd_prodi' => $this->input->post('kd_prodi'),
				'judul_kegiatan' => $this->input->post('judul_kegiatan'),
				'keg_id' => $this->input->post('keg_id'),
				'tujuan_instansi' => $this->input->post('tujuan_instansi'),
				'negara_tujuan' => $this->input->post('negara_tujuan'),
				'tgl_mulai' => $this->input->post('tgl_mulai'),
				'tgl_selesai' => $this->input->post('tgl_selesai'),
				'mou_ks' => $this->input->post('mou_ks'),
				'catatan' => $this->input->post('catatan'),
				'validasi' => $this->input->post('validasi'),
			];

			if (!is_dir(FCPATH . '/uploads/perjalun/')) {
				mkdir(FCPATH . '/uploads/perjalun/');
			}

			//file pengantar
			if (!empty($perjalun_file_pengantar_name)) {
				$perjalun_file_pengantar_name_copy = date('YmdHis') . '-' . $perjalun_file_pengantar_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_pengantar_uuid . '/' . $perjalun_file_pengantar_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_pengantar_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_pengantar_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_pengantar'] = $perjalun_file_pengantar_name_copy;
			}

			if (!empty($perjalun_file_drh_name)) {
				$perjalun_file_drh_name_copy = date('YmdHis') . '-' . $perjalun_file_drh_name;
				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_drh_uuid . '/' . $perjalun_file_drh_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_drh_name_copy
				);
				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_drh_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}
				$save_data['file_drh'] = $perjalun_file_drh_name_copy;
			}

			if (!empty($perjalun_file_kegiatan_name)) {
				$perjalun_file_kegiatan_name_copy = date('YmdHis') . '-' . $perjalun_file_kegiatan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_kegiatan_uuid . '/' . $perjalun_file_kegiatan_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_kegiatan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_kegiatan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_kegiatan'] = $perjalun_file_kegiatan_name_copy;
			}

			if (!empty($perjalun_file_loa_name)) {
				$perjalun_file_loa_name_copy = date('YmdHis') . '-' . $perjalun_file_loa_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_loa_uuid . '/' . $perjalun_file_loa_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_loa_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_loa'] = $perjalun_file_loa_name_copy;
			}

			if (!empty($perjalun_file_relevansi_name)) {
				$perjalun_file_relevansi_name_copy = date('YmdHis') . '-' . $perjalun_file_relevansi_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_relevansi_uuid . '/' . $perjalun_file_relevansi_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_relevansi_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_relevansi_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_relevansi'] = $perjalun_file_relevansi_name_copy;
			}

			if (!empty($perjalun_file_kerangka_name)) {
				$perjalun_file_kerangka_name_copy = date('YmdHis') . '-' . $perjalun_file_kerangka_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_kerangka_uuid . '/' . $perjalun_file_kerangka_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_kerangka_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_kerangka_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_kerangka'] = $perjalun_file_kerangka_name_copy;
			}

			if (!empty($perjalun_file_rab_name)) {
				$perjalun_file_rab_name_copy = date('YmdHis') . '-' . $perjalun_file_rab_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_rab_uuid . '/' . $perjalun_file_rab_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_rab_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_rab_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_rab'] = $perjalun_file_rab_name_copy;
			}

			if (!empty($perjalun_file_ktp_name)) {
				$perjalun_file_ktp_name_copy = date('YmdHis') . '-' . $perjalun_file_ktp_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_ktp_uuid . '/' . $perjalun_file_ktp_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_ktp_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_ktp_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ktp'] = $perjalun_file_ktp_name_copy;
			}

			if (!empty($perjalun_file_laporan_name)) {
				$perjalun_file_laporan_name_copy = date('YmdHis') . '-' . $perjalun_file_laporan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_laporan_uuid . '/' . $perjalun_file_laporan_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_laporan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_laporan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_laporan'] = $perjalun_file_laporan_name_copy;
			}

			if (!empty($perjalun_file_sk_name)) {
				$perjalun_file_sk_name_copy = date('YmdHis') . '-' . $perjalun_file_sk_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_sk_uuid . '/' . $perjalun_file_sk_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_sk_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_sk_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_sk'] = $perjalun_file_sk_name_copy;
			}


			$save_perjalun = $this->model_perjalun->store($save_data);


			if ($save_perjalun) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_perjalun;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/perjalun/edit/' . $save_perjalun, 'Edit Perjalun'),
						anchor('administrator/perjalun', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/perjalun/edit/' . $save_perjalun, 'Edit Perjalun')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perjalun');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perjalun');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Perjaluns
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('perjalun_update');

		$this->data['perjalun'] = $this->model_perjalun->find($id);

		if (!is_groups_in(['Admin', 'Kerjasama'])) :
			if ($this->data['perjalun']->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa mengubah, karena data sudah divalidasi', 'si_kerjasama'), 'warning');
				redirect_back();
			}
		endif;

		$list_mou = db_get_all_data('mou_kerjasama', "scope='LN' AND tgl_selesai > curdate() ");
		$cur_mou = db_get_all_data('mou_kerjasama', "id = '{$this->data['perjalun']->mou_ks}'", 'row');

		if (!is_null($cur_mou)) {
			$tgl = DateTime::createFromFormat('Y-m-d', $cur_mou->tgl_selesai);
			$now = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
			if ($tgl < $now) $list_mou[] = $cur_mou;
		}

		$this->template->title('Perjalun Update');
		$this->template->title('Perjalun New');
		$list_mou[]                 = (object)['id' => '1', 'nama_mitra' => 'Pengajuan Baru'];
		$this->data['list_mou']     = $list_mou;
		$this->data['filter_prodi'] = get_filter_prodi();
		$this->render('backend/standart/administrator/perjalun/perjalun_update', $this->data);
	}

	/**
	 * Update Perjaluns
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('perjalun_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('nip', 'NIP/NRP', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kd_prodi', 'Prodi/Unit', 'trim|required|max_length[8]');
		$this->form_validation->set_rules('judul_kegiatan', 'Judul Kegiatan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('keg_id', 'Judul keg_id', 'numeric|required');
		$this->form_validation->set_rules('tujuan_instansi', 'Tujuan Instansi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('negara_tujuan', 'Negara Tujuan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'trim|required');
		$this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'trim|required');
		$this->form_validation->set_rules('perjalun_file_kegiatan_name', 'File Kegiatan', 'trim|required');
		$this->form_validation->set_rules('mou_ks', 'Nota Kesepahaman (MoU)', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('perjalun_file_loa_name', 'File Undangan (LoA)', 'trim|required');
		$this->form_validation->set_rules('perjalun_file_relevansi_name', 'File Relevansi', 'trim|required');
		$this->form_validation->set_rules('perjalun_file_kerangka_name', 'File Kerangka Kerja', 'trim|required');
		$this->form_validation->set_rules('perjalun_file_rab_name', 'File RAB', 'trim|required');
		$this->form_validation->set_rules('perjalun_file_ktp_name', 'File KTP', 'trim|required');
		$this->form_validation->set_rules('perjalun_file_drh_name', 'File Drh', 'trim|required');
		// $this->form_validation->set_rules('perjalun_file_laporan_name', 'File Laporan', 'trim|required');
		$this->form_validation->set_rules('catatan', 'Catatan', 'trim|max_length[255]');
		$this->form_validation->set_rules('perjalun_file_sk_name', 'FIle SK', 'trim');

		if ($this->form_validation->run()) {
			$perjalun_file_kegiatan_uuid = $this->input->post('perjalun_file_kegiatan_uuid');
			$perjalun_file_kegiatan_name = $this->input->post('perjalun_file_kegiatan_name');
			$perjalun_file_loa_uuid = $this->input->post('perjalun_file_loa_uuid');
			$perjalun_file_loa_name = $this->input->post('perjalun_file_loa_name');
			$perjalun_file_relevansi_uuid = $this->input->post('perjalun_file_relevansi_uuid');
			$perjalun_file_relevansi_name = $this->input->post('perjalun_file_relevansi_name');
			$perjalun_file_kerangka_uuid = $this->input->post('perjalun_file_kerangka_uuid');
			$perjalun_file_kerangka_name = $this->input->post('perjalun_file_kerangka_name');
			$perjalun_file_rab_uuid = $this->input->post('perjalun_file_rab_uuid');
			$perjalun_file_rab_name = $this->input->post('perjalun_file_rab_name');
			$perjalun_file_ktp_uuid = $this->input->post('perjalun_file_ktp_uuid');
			$perjalun_file_ktp_name = $this->input->post('perjalun_file_ktp_name');
			$perjalun_file_drh_uuid = $this->input->post('perjalun_file_drh_uuid');
			$perjalun_file_drh_name = $this->input->post('perjalun_file_drh_name');
			$perjalun_file_laporan_uuid = $this->input->post('perjalun_file_laporan_uuid');
			$perjalun_file_laporan_name = $this->input->post('perjalun_file_laporan_name');
			$perjalun_file_sk_uuid = $this->input->post('perjalun_file_sk_uuid');
			$perjalun_file_sk_name = $this->input->post('perjalun_file_sk_name');
			//file pengantar
			$perjalun_file_pengantar_uuid = $this->input->post('perjalun_file_pengantar_uuid');
			$perjalun_file_pengantar_name = $this->input->post('perjalun_file_pengantar_name');

			$save_data = [
				'nip' => $this->input->post('nip'),
				'nama' => $this->input->post('nama'),
				'kd_prodi' => $this->input->post('kd_prodi'),
				'judul_kegiatan' => $this->input->post('judul_kegiatan'),
				'keg_id' => $this->input->post('keg_id'),
				'tujuan_instansi' => $this->input->post('tujuan_instansi'),
				'negara_tujuan' => $this->input->post('negara_tujuan'),
				'tgl_mulai' => $this->input->post('tgl_mulai'),
				'tgl_selesai' => $this->input->post('tgl_selesai'),
				'mou_ks' => $this->input->post('mou_ks'),
				'catatan' => $this->input->post('catatan'),
				'validasi' => $this->input->post('validasi'),
			];

			if (!is_dir(FCPATH . '/uploads/perjalun/')) {
				mkdir(FCPATH . '/uploads/perjalun/');
			}

			//file pengantar
			if (!empty($perjalun_file_pengantar_uuid)) {
				$perjalun_file_pengantar_name_copy = date('YmdHis') . '-' . $perjalun_file_pengantar_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_pengantar_uuid . '/' . $perjalun_file_pengantar_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_pengantar_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_pengantar_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_pengantar'] = $perjalun_file_pengantar_name_copy;
			}

			if (!empty($perjalun_file_drh_uuid)) {
				$perjalun_file_drh_name_copy = date('YmdHis') . '-' . $perjalun_file_drh_name;
				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_drh_uuid . '/' . $perjalun_file_drh_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_drh_name_copy
				);
				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_drh_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}
				$save_data['file_drh'] = $perjalun_file_drh_name_copy;
			}

			if (!empty($perjalun_file_kegiatan_uuid)) {
				$perjalun_file_kegiatan_name_copy = date('YmdHis') . '-' . $perjalun_file_kegiatan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_kegiatan_uuid . '/' . $perjalun_file_kegiatan_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_kegiatan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_kegiatan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_kegiatan'] = $perjalun_file_kegiatan_name_copy;
			}

			if (!empty($perjalun_file_loa_uuid)) {
				$perjalun_file_loa_name_copy = date('YmdHis') . '-' . $perjalun_file_loa_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_loa_uuid . '/' . $perjalun_file_loa_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_loa_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_loa'] = $perjalun_file_loa_name_copy;
			}

			if (!empty($perjalun_file_relevansi_uuid)) {
				$perjalun_file_relevansi_name_copy = date('YmdHis') . '-' . $perjalun_file_relevansi_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_relevansi_uuid . '/' . $perjalun_file_relevansi_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_relevansi_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_relevansi_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_relevansi'] = $perjalun_file_relevansi_name_copy;
			}

			if (!empty($perjalun_file_kerangka_uuid)) {
				$perjalun_file_kerangka_name_copy = date('YmdHis') . '-' . $perjalun_file_kerangka_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_kerangka_uuid . '/' . $perjalun_file_kerangka_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_kerangka_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_kerangka_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_kerangka'] = $perjalun_file_kerangka_name_copy;
			}

			if (!empty($perjalun_file_rab_uuid)) {
				$perjalun_file_rab_name_copy = date('YmdHis') . '-' . $perjalun_file_rab_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_rab_uuid . '/' . $perjalun_file_rab_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_rab_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_rab_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_rab'] = $perjalun_file_rab_name_copy;
			}

			if (!empty($perjalun_file_ktp_uuid)) {
				$perjalun_file_ktp_name_copy = date('YmdHis') . '-' . $perjalun_file_ktp_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_ktp_uuid . '/' . $perjalun_file_ktp_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_ktp_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_ktp_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ktp'] = $perjalun_file_ktp_name_copy;
			}

			if (!empty($perjalun_file_laporan_uuid)) {
				$perjalun_file_laporan_name_copy = date('YmdHis') . '-' . $perjalun_file_laporan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_laporan_uuid . '/' . $perjalun_file_laporan_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_laporan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_laporan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_laporan'] = $perjalun_file_laporan_name_copy;
			}

			if (!empty($perjalun_file_sk_uuid)) {
				$perjalun_file_sk_name_copy = date('YmdHis') . '-' . $perjalun_file_sk_name;

				rename(
					FCPATH . 'uploads/tmp/' . $perjalun_file_sk_uuid . '/' . $perjalun_file_sk_name,
					FCPATH . 'uploads/perjalun/' . $perjalun_file_sk_name_copy
				);

				if (!is_file(FCPATH . '/uploads/perjalun/' . $perjalun_file_sk_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_sk'] = $perjalun_file_sk_name_copy;
			}


			$save_perjalun = $this->model_perjalun->change($id, $save_data);

			if ($save_perjalun) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/perjalun', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/perjalun/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/perjalun/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Perjaluns
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('perjalun_delete');

		if (is_groups_in(['Dosen', 'Tendik'])) {
			$jalun	= $this->model_perjalun->find($id);
			if ($jalun->nip == fromsess('identitas')) {
				if ($jalun->validasi == 'Y') {
					set_message(cclang('Gagal hapus, karena data sudah divalidasi', 'perjalun'), 'warning');
					redirect_back();
				}
			} else {
				set_message(cclang('sorry_you_do_not_have_permission_to_access', 'perjalun'), 'error');
				redirect_back();
			}
		}

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'perjalun'), 'success');
		} else {
			set_message(cclang('error_delete', 'perjalun'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Perjaluns
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('perjalun_view');

		$this->data['perjalun'] = $this->model_perjalun->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Perjalun Detail');
		$this->render('backend/standart/administrator/perjalun/perjalun_view', $this->data);
	}

	/**
	 * delete Perjaluns
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$perjalun = $this->model_perjalun->find($id);

		if (!empty($perjalun->file_drh)) {
			$path = FCPATH . '/uploads/perjalun/' . $perjalun->file_drh;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}

		//file pengantar
		if (!empty($perjalun->file_pengantar)) {
			$path = FCPATH . '/uploads/perjalun/' . $perjalun->file_pengantar;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}

		if (!empty($perjalun->file_kegiatan)) {
			$path = FCPATH . '/uploads/perjalun/' . $perjalun->file_kegiatan;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($perjalun->file_loa)) {
			$path = FCPATH . '/uploads/perjalun/' . $perjalun->file_loa;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($perjalun->file_relevansi)) {
			$path = FCPATH . '/uploads/perjalun/' . $perjalun->file_relevansi;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($perjalun->file_kerangka)) {
			$path = FCPATH . '/uploads/perjalun/' . $perjalun->file_kerangka;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($perjalun->file_rab)) {
			$path = FCPATH . '/uploads/perjalun/' . $perjalun->file_rab;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($perjalun->file_ktp)) {
			$path = FCPATH . '/uploads/perjalun/' . $perjalun->file_ktp;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($perjalun->file_laporan)) {
			$path = FCPATH . '/uploads/perjalun/' . $perjalun->file_laporan;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($perjalun->file_sk)) {
			$path = FCPATH . '/uploads/perjalun/' . $perjalun->file_sk;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}


		return $this->model_perjalun->remove($id);
	}


	/**
	 * Upload Image Perjalun	* 
	 * @return JSON
	 */
	public function upload_file_pengantar_file()
	{
		if (!$this->is_allowed('perjalun_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'perjalun',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Perjalun	* 
	 * @return JSON
	 */
	public function delete_file_pengantar_file($uuid)
	{
		if (!$this->is_allowed('perjalun_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_pengantar',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/'
		]);
	}

	/**
	 * Get Image Perjalun	* 
	 * @return JSON
	 */
	public function get_file_pengantar_file($id)
	{
		if (!$this->is_allowed('perjalun_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$perjalun = $this->model_perjalun->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_pengantar',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/',
			'delete_endpoint'   => 'administrator/perjalun/delete_file_pengantar_file'
		]);
	}

	/**
	 * Upload Image Perjalun	* 
	 * @return JSON
	 */
	public function upload_file_kegiatan_file()
	{
		if (!$this->is_allowed('perjalun_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'perjalun',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Perjalun	* 
	 * @return JSON
	 */
	public function delete_file_kegiatan_file($uuid)
	{
		if (!$this->is_allowed('perjalun_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_kegiatan',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/'
		]);
	}

	/**
	 * Get Image Perjalun	* 
	 * @return JSON
	 */
	public function get_file_kegiatan_file($id)
	{
		if (!$this->is_allowed('perjalun_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$perjalun = $this->model_perjalun->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_kegiatan',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/',
			'delete_endpoint'   => 'administrator/perjalun/delete_file_kegiatan_file'
		]);
	}

	/**
	 * Upload Image Perjalun	* 
	 * @return JSON
	 */
	public function upload_file_loa_file()
	{
		if (!$this->is_allowed('perjalun_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'perjalun',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Perjalun	* 
	 * @return JSON
	 */
	public function delete_file_loa_file($uuid)
	{
		if (!$this->is_allowed('perjalun_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_loa',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/'
		]);
	}

	/**
	 * Get Image Perjalun	* 
	 * @return JSON
	 */
	public function get_file_loa_file($id)
	{
		if (!$this->is_allowed('perjalun_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$perjalun = $this->model_perjalun->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_loa',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/',
			'delete_endpoint'   => 'administrator/perjalun/delete_file_loa_file'
		]);
	}

	/**
	 * Upload Image Perjalun	* 
	 * @return JSON
	 */
	public function upload_file_relevansi_file()
	{
		if (!$this->is_allowed('perjalun_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'perjalun',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Perjalun	* 
	 * @return JSON
	 */
	public function delete_file_relevansi_file($uuid)
	{
		if (!$this->is_allowed('perjalun_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_relevansi',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/'
		]);
	}

	/**
	 * Get Image Perjalun	* 
	 * @return JSON
	 */
	public function get_file_relevansi_file($id)
	{
		if (!$this->is_allowed('perjalun_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$perjalun = $this->model_perjalun->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_relevansi',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/',
			'delete_endpoint'   => 'administrator/perjalun/delete_file_relevansi_file'
		]);
	}

	/**
	 * Upload Image Perjalun	* 
	 * @return JSON
	 */
	public function upload_file_kerangka_file()
	{
		if (!$this->is_allowed('perjalun_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'perjalun',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Perjalun	* 
	 * @return JSON
	 */
	public function delete_file_kerangka_file($uuid)
	{
		if (!$this->is_allowed('perjalun_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_kerangka',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/'
		]);
	}

	/**
	 * Get Image Perjalun	* 
	 * @return JSON
	 */
	public function get_file_kerangka_file($id)
	{
		if (!$this->is_allowed('perjalun_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$perjalun = $this->model_perjalun->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_kerangka',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/',
			'delete_endpoint'   => 'administrator/perjalun/delete_file_kerangka_file'
		]);
	}

	/**
	 * Upload Image Perjalun	* 
	 * @return JSON
	 */
	public function upload_file_rab_file()
	{
		if (!$this->is_allowed('perjalun_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'perjalun',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Perjalun	* 
	 * @return JSON
	 */
	public function delete_file_rab_file($uuid)
	{
		if (!$this->is_allowed('perjalun_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_rab',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/'
		]);
	}

	/**
	 * Get Image Perjalun	* 
	 * @return JSON
	 */
	public function get_file_rab_file($id)
	{
		if (!$this->is_allowed('perjalun_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$perjalun = $this->model_perjalun->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_rab',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/',
			'delete_endpoint'   => 'administrator/perjalun/delete_file_rab_file'
		]);
	}

	/**
	 * Upload Image Perjalun	* 
	 * @return JSON
	 */
	public function upload_file_ktp_file()
	{
		if (!$this->is_allowed('perjalun_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'perjalun',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Perjalun	* 
	 * @return JSON
	 */
	public function delete_file_ktp_file($uuid)
	{
		if (!$this->is_allowed('perjalun_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ktp',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/'
		]);
	}

	/**
	 * Get Image Perjalun	* 
	 * @return JSON
	 */
	public function get_file_ktp_file($id)
	{
		if (!$this->is_allowed('perjalun_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$perjalun = $this->model_perjalun->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ktp',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/',
			'delete_endpoint'   => 'administrator/perjalun/delete_file_ktp_file'
		]);
	}

	public function upload_file_drh_file()
	{
		if (!$this->is_allowed('perjalun_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'perjalun',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Perjalun	* 
	 * @return JSON
	 */
	public function delete_file_drh_file($uuid)
	{
		if (!$this->is_allowed('perjalun_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_drh',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/'
		]);
	}

	/**
	 * Get Image Perjalun	* 
	 * @return JSON
	 */
	public function get_file_drh_file($id)
	{
		if (!$this->is_allowed('perjalun_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$perjalun = $this->model_perjalun->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_drh',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/',
			'delete_endpoint'   => 'administrator/perjalun/delete_file_drh_file'
		]);
	}

	/**
	 * Upload Image Perjalun	* 
	 * @return JSON
	 */
	public function upload_file_laporan_file()
	{
		if (!$this->is_allowed('perjalun_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'perjalun',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Perjalun	* 
	 * @return JSON
	 */
	public function delete_file_laporan_file($uuid)
	{
		if (!$this->is_allowed('perjalun_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_laporan',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/'
		]);
	}

	/**
	 * Get Image Perjalun	* 
	 * @return JSON
	 */
	public function get_file_laporan_file($id)
	{
		if (!$this->is_allowed('perjalun_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$perjalun = $this->model_perjalun->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_laporan',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/',
			'delete_endpoint'   => 'administrator/perjalun/delete_file_laporan_file'
		]);
	}

	/**
	 * Upload Image Perjalun	* 
	 * @return JSON
	 */
	public function upload_file_sk_file()
	{
		if (!$this->is_allowed('perjalun_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'perjalun',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Perjalun	* 
	 * @return JSON
	 */
	public function delete_file_sk_file($uuid)
	{
		if (!$this->is_allowed('perjalun_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_sk',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/'
		]);
	}

	/**
	 * Get Image Perjalun	* 
	 * @return JSON
	 */
	public function get_file_sk_file($id)
	{
		if (!$this->is_allowed('perjalun_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$perjalun = $this->model_perjalun->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_sk',
			'table_name'        => 'perjalun',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/perjalun/',
			'delete_endpoint'   => 'administrator/perjalun/delete_file_sk_file'
		]);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('perjalun_export');

		$this->model_perjalun->export(
			'perjalun',
			'perjalun',
			$this->model_perjalun->field_search
		);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('perjalun_export');

		$this->model_perjalun->pdf('perjalun', 'perjalun');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('perjalun_export');

		$table = $title = 'perjalun';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_perjalun->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
}


/* End of file perjalun.php */
/* Location: ./application/controllers/administrator/Perjalun.php */