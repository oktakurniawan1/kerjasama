<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_perjalun extends MY_Model
{

	private $primary_key    = 'id';
	private $table_name     = 'perjalun';
	public $field_search   = ['nip', 'nama', 'kd_prodi', 'judul_kegiatan', 'tujuan_instansi', 'negara_tujuan', 'validasi'];

	public function __construct()
	{
		$config = array(
			'primary_key'   => $this->primary_key,
			'table_name'    => $this->table_name,
			'field_search'  => $this->field_search,
		);

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "perjalun." . $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "perjalun." . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			if ($field == 'nama_prodi') $where .= "(" . "ref_prodi." . $field . " LIKE '%" . $q . "%' )";
			else $where .= "(" . "perjalun." . $field . " LIKE '%" . $q . "%' )";
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "perjalun." . $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "perjalun." . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			if ($field == 'nama_prodi') $where .= "(" . "ref_prodi." . $field . " LIKE '%" . $q . "%' )";
			else $where .= "(" . "perjalun." . $field . " LIKE '%" . $q . "%' )";
		}

		if (is_array($select_field) and count($select_field)) {
			$this->db->select($select_field);
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);
		$this->db->limit($limit, $offset);

		$this->sortable();

		$query = $this->db->get($this->table_name);

		return $query->result();
	}

	public function join_avaiable()
	{
		$this->db->join('negara', 'negara.nama_negara = perjalun.negara_tujuan', 'LEFT');
		$this->db->join('ref_prodi', 'ref_prodi.kode = perjalun.kd_prodi', 'LEFT');
		$this->db->join('mou_kerjasama', 'mou_kerjasama.id = perjalun.mou_ks', 'LEFT');
		$this->db->select('ref_prodi.nama_prodi as nama_prodi');
		$this->db->select('mou_kerjasama.doc_file as doc_mou');
		$this->db->select('perjalun.*,negara.nama_negara as negara_nama_negara');


		return $this;
	}

	public function filter_avaiable()
	{

		// if (!$this->aauth->is_admin()) {
		//     }
		$prodi = [];
		if (is_groups_in(['Dosen', 'Tendik'])) {
			if (is_non_user()) $nip = fromsess('nip_baru');
			else $nip = get_user_data('username');
			$this->db->where('nip', $nip);
		} else if (is_member('Kaprodi')) {
			if (is_non_user()) $prodi[] = fromsess('kd_prodi');
			else {
				$prodi = get_filter_prodi(true);
			}
			$this->db->where_in('kd_prodi', $prodi);
		} else if (is_member('Unit')) {
			if (is_non_user()) $prodi[] = fromsess('kd_prodi');
			else {
				$prodi = get_filter_prodi(true);
			}
			$this->db->where_in('kd_prodi', $prodi);
		}

		return $this;
	}
}

/* End of file Model_perjalun.php */
/* Location: ./application/models/Model_perjalun.php */