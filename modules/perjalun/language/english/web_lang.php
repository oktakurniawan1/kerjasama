<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['perjalun'] = 'Perjalanan Luar Negeri';
$lang['id'] = 'Id';
$lang['nip'] = 'NIP/NRP';
$lang['nama'] = 'Nama';
$lang['kd_prodi'] = 'Prodi/Unit';
$lang['judul_kegiatan'] = 'Judul Kegiatan';
$lang['tujuan_instansi'] = 'Tujuan Instansi';
$lang['negara_tujuan'] = 'Negara Tujuan';
$lang['tgl_mulai'] = 'Tanggal Mulai';
$lang['tgl_selesai'] = 'Tanggal Selesai';
$lang['file_kegiatan'] = 'File Kegiatan';
$lang['mou_ks'] = 'Nota Kesepahaman (MoU)';
$lang['file_loa'] = 'File Undangan (LoA)';
$lang['file_pengantar'] = 'File Pengantar';
$lang['file_relevansi'] = 'File Relevansi';
$lang['file_kerangka'] = 'File Kerangka Kerja';
$lang['file_rab'] = 'File RAB';
$lang['file_ktp'] = 'File KTP';
$lang['file_drh'] = 'File Daftar Riwayat Hidup';
$lang['file_laporan'] = 'File Laporan';
$lang['catatan'] = 'Catatan';
$lang['validasi'] = 'Validasi';
$lang['file_sk'] = 'FIle SK';
