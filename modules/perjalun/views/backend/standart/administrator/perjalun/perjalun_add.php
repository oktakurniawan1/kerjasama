<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<link href="<?= BASE_ASSET ?>/nakupanda/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?= BASE_ASSET ?>/nakupanda/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="<?= BASE_ASSET ?>/nakupanda/bootstrap.min.js"></script>
<script src="<?= BASE_ASSET ?>/nakupanda/bootstrap-dialog.min.js"></script>

<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Perjalanan Luar Negeri <small><?= cclang('new', ['Perjalanan Luar Negeri']); ?> </small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/perjalun'); ?>">Perjalanan Luar Negeri</a></li>
		<li class="active"><?= cclang('new'); ?></li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Perjalanan Luar Negeri</h3>
							<h5 class="widget-user-desc"><?= cclang('new', ['Perjalanan Luar Negeri']); ?></h5>
							<hr>
						</div>
						<?= form_open('', [
							'name'    => 'form_perjalun',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_perjalun',
							'enctype' => 'multipart/form-data',
							'method'  => 'POST'
						]); ?>

						<div class="form-group ">
							<label for="identitas" class="col-sm-2 control-label">NIP/NRP
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<?php if (is_only_groups(['Dosen', 'Tendik'])) :
									if (is_non_user()) : $nip_baru = fromsess('nip_baru');
									else : $nip_baru = get_user_data('username');
									endif; ?>
									<input type="text" class="form-control" name="nip" id="nip" placeholder="NIP/NRP" value="<?= $nip_baru ?> " readonly>
								<?php elseif (is_groups_in(['Unit', 'Kerjasama', 'Admin', 'Kaprodi','Viewer'])) : ?>
									<input type="text" class="form-control" name="nip" id="nip" placeholder="NIP/NRP" value="<?= set_value('nip'); ?>">
									<button style='margin-top:10px' class="btn btn-primary btn-sm" type="button" onclick="cari_pegawai()">Cari di Simpeg</button>
								<?php endif; ?>
								<small class="info help-block">
									<b>Input Identitas NIP/NRP</b> Max Length : 20.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nama" class="col-sm-2 control-label">Nama
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<?php if (is_only_groups(['Dosen', 'Tendik', 'Kaprodi'])) :
									if (is_non_user()) : $nama = fromsess('nama');
									else : $nama = get_user_data('full_name');
									endif; ?>
									<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?= $nama; ?>">
								<?php else : ?>
									<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?= set_value('nama'); ?>">
								<?php endif; ?>
								<small class="info help-block">
									<b>Input Nama</b> Max Length : 255.</small>
							</div>
						</div>

						<?php
						if (is_only_groups(['Dosen', 'Tendik', 'Kaprodi'])) :
							if (is_non_user()) : $kd_prodi = fromsess('kd_prodi');
							else : $kd_prodi = '';
							endif;
						else : $kd_prodi = '';
						endif;
						?>
						<div class="form-group ">
							<label for="kd_prodi" class="col-sm-2 control-label">Program studi/Unit
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="kd_prodi" id="kd_prodi" data-placeholder="Program studi/Unit">
									<option value=""></option>
									<?php foreach ($filter_prodi as $row) : ?>
										<option <?= $row->kode ==  $kd_prodi ? 'selected' : ''; ?> value="<?= $row->kode ?>"><?= $row->kode . ' - ' . $row->nama_prodi; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									Pilih<b> Program studi/Unit</b></small>
							</div>
						</div>

						<div class="form-group ">
							<label for="judul_kegiatan" class="col-sm-2 control-label">Judul Kegiatan
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="judul_kegiatan" id="judul_kegiatan" placeholder="Judul Kegiatan" value="<?= set_value('judul_kegiatan'); ?>">
								<small class="info help-block">
									<b>Input Judul Kegiatan</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="keg_id" class="col-sm-2 control-label">Jenis Kegiatan
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="keg_id" id="keg_id" data-placeholder="Select Kegiatan">
									<option value=""></option>
									<?php foreach (db_get_all_data('keg_setneg') as $row) : ?>
										<option value="<?= $row->id ?>"><?= $row->keg_setneg; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Pilih Jenis Kegiatan</b></small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tujuan_instansi" class="col-sm-2 control-label">Tujuan Instansi
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="tujuan_instansi" id="tujuan_instansi" placeholder="Tujuan Instansi" value="<?= set_value('tujuan_instansi'); ?>">
								<small class="info help-block">
									<b>Input Tujuan Instansi</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="negara_tujuan" class="col-sm-2 control-label">Negara Tujuan
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="negara_tujuan" id="negara_tujuan" data-placeholder="Select Negara Tujuan">
									<option value=""></option>
									<?php foreach (db_get_all_data('negara') as $row) : ?>
										<option value="<?= $row->nama_negara ?>"><?= $row->nama_negara; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Input Negara Tujuan</b> Max Length : 255.</small>
							</div>
						</div>


						<div class="form-group ">
							<label for="tgl_mulai" class="col-sm-2 control-label">Tanggal Mulai
								<i class="required">*</i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-8">
									<input type="date" class="form-control pull-right" name="tgl_mulai" placeholder="Tanggal Mulai" id="tgl_mulai">
								</div>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_selesai" class="col-sm-2 control-label">Tanggal Selesai
								<i class="required">*</i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-8">
									<input type="date" class="form-control pull-right" name="tgl_selesai" placeholder="Tanggal Selesai" id="tgl_selesai">
								</div>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_kegiatan" class="col-sm-2 control-label">File Jadwal Kegiatan
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="perjalun_file_kegiatan_galery"></div>
								<input class="data_file" name="perjalun_file_kegiatan_uuid" id="perjalun_file_kegiatan_uuid" type="hidden" value="<?= set_value('perjalun_file_kegiatan_uuid'); ?>">
								<input class="data_file" name="perjalun_file_kegiatan_name" id="perjalun_file_kegiatan_name" type="hidden" value="<?= set_value('perjalun_file_kegiatan_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="mou_ks" class="col-sm-2 control-label">Nota Kesepahaman (MoU)
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="mou_ks" id="mou_ks" data-placeholder="Select MoU Kerjasama">
								</select>
								<small class="info help-block">
									<b>Pilih MoU</b>, jika tidak ada silahkan hubungi bagian kerjasama, atau pilih <b>Pengajuan Baru</b></small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_loa" class="col-sm-2 control-label">File Undangan (LoA)
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="perjalun_file_loa_galery"></div>
								<input class="data_file" name="perjalun_file_loa_uuid" id="perjalun_file_loa_uuid" type="hidden" value="<?= set_value('perjalun_file_loa_uuid'); ?>">
								<input class="data_file" name="perjalun_file_loa_name" id="perjalun_file_loa_name" type="hidden" value="<?= set_value('perjalun_file_loa_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_pengantar" class="col-sm-2 control-label">File Pengantar (Fakultas/Unit)
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="perjalun_file_pengantar_galery"></div>
								<input class="data_file" name="perjalun_file_pengantar_uuid" id="perjalun_file_pengantar_uuid" type="hidden" value="<?= set_value('perjalun_file_pengantar_uuid'); ?>">
								<input class="data_file" name="perjalun_file_pengantar_name" id="perjalun_file_pengantar_name" type="hidden" value="<?= set_value('perjalun_file_pengantar_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_relevansi" class="col-sm-2 control-label">File Relevansi
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="perjalun_file_relevansi_galery"></div>
								<input class="data_file" name="perjalun_file_relevansi_uuid" id="perjalun_file_relevansi_uuid" type="hidden" value="<?= set_value('perjalun_file_relevansi_uuid'); ?>">
								<input class="data_file" name="perjalun_file_relevansi_name" id="perjalun_file_relevansi_name" type="hidden" value="<?= set_value('perjalun_file_relevansi_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_kerangka" class="col-sm-2 control-label">File Kerangka Kerja
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="perjalun_file_kerangka_galery"></div>
								<input class="data_file" name="perjalun_file_kerangka_uuid" id="perjalun_file_kerangka_uuid" type="hidden" value="<?= set_value('perjalun_file_kerangka_uuid'); ?>">
								<input class="data_file" name="perjalun_file_kerangka_name" id="perjalun_file_kerangka_name" type="hidden" value="<?= set_value('perjalun_file_kerangka_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_rab" class="col-sm-2 control-label">File RAB
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="perjalun_file_rab_galery"></div>
								<input class="data_file" name="perjalun_file_rab_uuid" id="perjalun_file_rab_uuid" type="hidden" value="<?= set_value('perjalun_file_rab_uuid'); ?>">
								<input class="data_file" name="perjalun_file_rab_name" id="perjalun_file_rab_name" type="hidden" value="<?= set_value('perjalun_file_rab_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_ktp" class="col-sm-2 control-label">File KTP
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="perjalun_file_ktp_galery"></div>
								<input class="data_file" name="perjalun_file_ktp_uuid" id="perjalun_file_ktp_uuid" type="hidden" value="<?= set_value('perjalun_file_ktp_uuid'); ?>">
								<input class="data_file" name="perjalun_file_ktp_name" id="perjalun_file_ktp_name" type="hidden" value="<?= set_value('perjalun_file_ktp_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_drh" class="col-sm-2 control-label">File Drh
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="perjalun_file_drh_galery"></div>
								<input class="data_file" name="perjalun_file_drh_uuid" id="perjalun_file_drh_uuid" type="hidden" value="<?= set_value('perjalun_file_drh_uuid'); ?>">
								<input class="data_file" name="perjalun_file_drh_name" id="perjalun_file_drh_name" type="hidden" value="<?= set_value('perjalun_file_drh_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_laporan" class="col-sm-2 control-label">File Laporan
								<!-- <i class="required">*</i> -->
							</label>
							<div class="col-sm-8">
								<div id="perjalun_file_laporan_galery"></div>
								<input class="data_file" name="perjalun_file_laporan_uuid" id="perjalun_file_laporan_uuid" type="hidden" value="<?= set_value('perjalun_file_laporan_uuid'); ?>">
								<input class="data_file" name="perjalun_file_laporan_name" id="perjalun_file_laporan_name" type="hidden" value="<?= set_value('perjalun_file_laporan_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="catatan" class="col-sm-2 control-label">Catatan
							</label>
							<div class="col-sm-8">
								<textarea id="catatan" name="catatan" rows="5" class="textarea form-control"><?= set_value('catatan'); ?></textarea>
								<small class="info help-block">
									<b>Input Catatan</b> Max Length : 255.</small>
							</div>
						</div>

						<?php if (is_groups_in(['Admin', 'Kerjasama'])) : ?>
							<div class="form-group ">
								<label for="file_sk" class="col-sm-2 control-label">File Surat Setneg
								</label>
								<div class="col-sm-8">
									<div id="perjalun_file_sk_galery"></div>
									<input class="data_file" name="perjalun_file_sk_uuid" id="perjalun_file_sk_uuid" type="hidden" value="<?= set_value('perjalun_file_sk_uuid'); ?>">
									<input class="data_file" name="perjalun_file_sk_name" id="perjalun_file_sk_name" type="hidden" value="<?= set_value('perjalun_file_sk_name'); ?>">
									<small class="info help-block">
										<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
								</div>
							</div>
							<div class="form-group ">
								<label for="validasi" class="col-sm-2 control-label">Validasi
								</label>
								<div class="col-sm-8">
									<select class="form-control chosen chosen-select" name="validasi" id="validasi" data-placeholder="Select Validasi">
										<option value=""></option>
										<option value="Y">Valid</option>
										<option value="T">Invalid</option>
									</select>
									<small class="info help-block">
									</small>
								</div>
							</div>
						<?php endif; ?>

						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	$(document).ready(function() {


		$('#btn_cancel').click(function() {
			swal({
					title: "<?= cclang('are_you_sure'); ?>",
					text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/perjalun';
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();

			var form_perjalun = $('#form_perjalun');
			var data_post = form_perjalun.serializeArray();
			var save_type = $(this).attr('data-stype');

			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: BASE_URL + '/administrator/perjalun/add_save',
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('.steps li').removeClass('error');
					$('form').find('.error-input').remove();
					if (res.success) {
						var id_file_pengantar = $('#perjalun_file_pengantar_galery').find('li').attr('qq-file-id');
						var id_file_kegiatan = $('#perjalun_file_kegiatan_galery').find('li').attr('qq-file-id');
						var id_file_loa = $('#perjalun_file_loa_galery').find('li').attr('qq-file-id');
						var id_file_relevansi = $('#perjalun_file_relevansi_galery').find('li').attr('qq-file-id');
						var id_file_kerangka = $('#perjalun_file_kerangka_galery').find('li').attr('qq-file-id');
						var id_file_rab = $('#perjalun_file_rab_galery').find('li').attr('qq-file-id');
						var id_file_ktp = $('#perjalun_file_ktp_galery').find('li').attr('qq-file-id');
						var id_file_laporan = $('#perjalun_file_laporan_galery').find('li').attr('qq-file-id');
						var id_file_sk = $('#perjalun_file_sk_galery').find('li').attr('qq-file-id');
						var id_file_drh = $('#perjalun_file_drh_galery').find('li').attr('qq-file-id');

						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						resetForm();
						if (typeof id_file_pengantar !== 'undefined') {
							$('#perjalun_file_pengantar_galery').fineUploader('deleteFile', id_file_pengantar);
						}
						if (typeof id_file_kegiatan !== 'undefined') {
							$('#perjalun_file_kegiatan_galery').fineUploader('deleteFile', id_file_kegiatan);
						}
						if (typeof id_file_loa !== 'undefined') {
							$('#perjalun_file_loa_galery').fineUploader('deleteFile', id_file_loa);
						}
						if (typeof id_file_relevansi !== 'undefined') {
							$('#perjalun_file_relevansi_galery').fineUploader('deleteFile', id_file_relevansi);
						}
						if (typeof id_file_kerangka !== 'undefined') {
							$('#perjalun_file_kerangka_galery').fineUploader('deleteFile', id_file_kerangka);
						}
						if (typeof id_file_rab !== 'undefined') {
							$('#perjalun_file_rab_galery').fineUploader('deleteFile', id_file_rab);
						}
						if (typeof id_file_ktp !== 'undefined') {
							$('#perjalun_file_ktp_galery').fineUploader('deleteFile', id_file_ktp);
						}
						if (typeof id_file_laporan !== 'undefined') {
							$('#perjalun_file_laporan_galery').fineUploader('deleteFile', id_file_laporan);
						}
						if (typeof id_file_sk !== 'undefined') {
							$('#perjalun_file_sk_galery').fineUploader('deleteFile', id_file_sk);
						}
						if (typeof id_file_drh !== 'undefined') {
							$('#perjalun_file_drh_galery').fineUploader('deleteFile', id_file_drh);
						}

						$('.chosen option').prop('selected', false).trigger('chosen:updated');

					} else {
						if (res.errors) {

							$.each(res.errors, function(index, val) {
								$('form #' + index).parents('.form-group').addClass('has-error');
								$('form #' + index).parents('.form-group').find('small').prepend(`
                      <div class="error-input">` + val + `</div>
                      `);
							});
							$('.steps li').removeClass('error');
							$('.content section').each(function(index, el) {
								if ($(this).find('.has-error').length) {
									$('.steps li:eq(' + index + ')').addClass('error').find('a').trigger('click');
								}
							});
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/

		var params = {};
		params[csrf] = token;

		$('#perjalun_file_pengantar_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/perjalun/upload_file_pengantar_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/perjalun/delete_file_pengantar_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#perjalun_file_pengantar_galery').fineUploader('getUuid', id);
						$('#perjalun_file_pengantar_uuid').val(uuid);
						$('#perjalun_file_pengantar_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#perjalun_file_pengantar_uuid').val();
					$.get(BASE_URL + '/administrator/perjalun/delete_file_pengantar_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#perjalun_file_pengantar_uuid').val('');
						$('#perjalun_file_pengantar_name').val('');
					}
				}
			}
		}); /*end file_pengantar galery*/

		var params = {};
		params[csrf] = token;

		$('#perjalun_file_kegiatan_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/perjalun/upload_file_kegiatan_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/perjalun/delete_file_kegiatan_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#perjalun_file_kegiatan_galery').fineUploader('getUuid', id);
						$('#perjalun_file_kegiatan_uuid').val(uuid);
						$('#perjalun_file_kegiatan_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#perjalun_file_kegiatan_uuid').val();
					$.get(BASE_URL + '/administrator/perjalun/delete_file_kegiatan_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#perjalun_file_kegiatan_uuid').val('');
						$('#perjalun_file_kegiatan_name').val('');
					}
				}
			}
		}); /*end file_kegiatan galery*/
		var params = {};
		params[csrf] = token;

		$('#perjalun_file_loa_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/perjalun/upload_file_loa_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/perjalun/delete_file_loa_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#perjalun_file_loa_galery').fineUploader('getUuid', id);
						$('#perjalun_file_loa_uuid').val(uuid);
						$('#perjalun_file_loa_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#perjalun_file_loa_uuid').val();
					$.get(BASE_URL + '/administrator/perjalun/delete_file_loa_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#perjalun_file_loa_uuid').val('');
						$('#perjalun_file_loa_name').val('');
					}
				}
			}
		}); /*end file_loa galery*/
		var params = {};
		params[csrf] = token;

		$('#perjalun_file_relevansi_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/perjalun/upload_file_relevansi_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/perjalun/delete_file_relevansi_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#perjalun_file_relevansi_galery').fineUploader('getUuid', id);
						$('#perjalun_file_relevansi_uuid').val(uuid);
						$('#perjalun_file_relevansi_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#perjalun_file_relevansi_uuid').val();
					$.get(BASE_URL + '/administrator/perjalun/delete_file_relevansi_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#perjalun_file_relevansi_uuid').val('');
						$('#perjalun_file_relevansi_name').val('');
					}
				}
			}
		}); /*end file_relevansi galery*/
		var params = {};
		params[csrf] = token;

		$('#perjalun_file_kerangka_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/perjalun/upload_file_kerangka_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/perjalun/delete_file_kerangka_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#perjalun_file_kerangka_galery').fineUploader('getUuid', id);
						$('#perjalun_file_kerangka_uuid').val(uuid);
						$('#perjalun_file_kerangka_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#perjalun_file_kerangka_uuid').val();
					$.get(BASE_URL + '/administrator/perjalun/delete_file_kerangka_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#perjalun_file_kerangka_uuid').val('');
						$('#perjalun_file_kerangka_name').val('');
					}
				}
			}
		}); /*end file_kerangka galery*/
		var params = {};
		params[csrf] = token;

		$('#perjalun_file_rab_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/perjalun/upload_file_rab_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/perjalun/delete_file_rab_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#perjalun_file_rab_galery').fineUploader('getUuid', id);
						$('#perjalun_file_rab_uuid').val(uuid);
						$('#perjalun_file_rab_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#perjalun_file_rab_uuid').val();
					$.get(BASE_URL + '/administrator/perjalun/delete_file_rab_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#perjalun_file_rab_uuid').val('');
						$('#perjalun_file_rab_name').val('');
					}
				}
			}
		}); /*end file_rab galery*/
		var params = {};
		params[csrf] = token;

		$('#perjalun_file_ktp_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/perjalun/upload_file_ktp_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/perjalun/delete_file_ktp_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#perjalun_file_ktp_galery').fineUploader('getUuid', id);
						$('#perjalun_file_ktp_uuid').val(uuid);
						$('#perjalun_file_ktp_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#perjalun_file_ktp_uuid').val();
					$.get(BASE_URL + '/administrator/perjalun/delete_file_ktp_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#perjalun_file_ktp_uuid').val('');
						$('#perjalun_file_ktp_name').val('');
					}
				}
			}
		}); /*end file_ktp galery*/

		var params = {};
		params[csrf] = token;

		$('#perjalun_file_drh_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/perjalun/upload_file_drh_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/perjalun/delete_file_drh_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#perjalun_file_drh_galery').fineUploader('getUuid', id);
						$('#perjalun_file_drh_uuid').val(uuid);
						$('#perjalun_file_drh_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#perjalun_file_drh_uuid').val();
					$.get(BASE_URL + '/administrator/perjalun/delete_file_drh_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#perjalun_file_drh_uuid').val('');
						$('#perjalun_file_drh_name').val('');
					}
				}
			}
		}); /*end file_drh galery*/

		var params = {};
		params[csrf] = token;

		$('#perjalun_file_laporan_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/perjalun/upload_file_laporan_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/perjalun/delete_file_laporan_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#perjalun_file_laporan_galery').fineUploader('getUuid', id);
						$('#perjalun_file_laporan_uuid').val(uuid);
						$('#perjalun_file_laporan_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#perjalun_file_laporan_uuid').val();
					$.get(BASE_URL + '/administrator/perjalun/delete_file_laporan_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#perjalun_file_laporan_uuid').val('');
						$('#perjalun_file_laporan_name').val('');
					}
				}
			}
		}); /*end file_laporan galery*/
		var params = {};
		params[csrf] = token;

		$('#perjalun_file_sk_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/perjalun/upload_file_sk_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/perjalun/delete_file_sk_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#perjalun_file_sk_galery').fineUploader('getUuid', id);
						$('#perjalun_file_sk_uuid').val(uuid);
						$('#perjalun_file_sk_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#perjalun_file_sk_uuid').val();
					$.get(BASE_URL + '/administrator/perjalun/delete_file_sk_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#perjalun_file_sk_uuid').val('');
						$('#perjalun_file_sk_name').val('');
					}
				}
			}
		}); /*end file_sk galery*/
		$.get('<?= site_url('administrator/mou_kerjasama/get_option_mou') ?>', {
			scope: 'LN'
		}, function(page) {
			$('#mou_ks').html(page).val('').trigger('chosen:updated');
		})
	}); /*end doc ready*/
	function cari_pegawai() {
		BootstrapDialog.show({
			title: 'Cari data Simpeg',
			draggable: true,
			message: function(dialog) {
				var $message = $('<div></div>');
				$.get('<?= site_url('administrator/pegawai/form_cari_pegawai') ?>', {}, function(response) {
					$message.html(response);
				})
				return $message;
			},
			buttons: [{
				label: 'Ambil',
				icon: 'glyphicon glyphicon-plus',
				cssClass: 'btn-primary',
				action: function(dialog) {
					$('#nip').val($('#pegawai option:selected').data('nip'));
					$('#nama').val($('#pegawai option:selected').data('nama'));
					dialog.close();
				}
			}, {
				label: 'Tutup',
				action: function(dialog) {
					dialog.close();
				}
			}],

		});
	}
</script>