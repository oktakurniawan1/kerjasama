<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['mitra_mbkm_unnes'] = 'Mitra MBKM Unnes';
$lang['id'] = 'Id';
$lang['skema'] = 'Skema';
$lang['semester'] = 'Semester';
$lang['program'] = 'Program';
$lang['mitra'] = 'Mitra';
$lang['unit_ks'] = 'MoA/IA/LoA (Unit)';
$lang['validasi'] = 'Validasi';
