<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Mitra Mbkm Unnes Controller
 *| --------------------------------------------------------------------------
 *| Mitra Mbkm Unnes site
 *|
 */
class Mitra_mbkm_unnes extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_mitra_mbkm_unnes');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * form filter data 
	 */
	public function filter_session_data()
	{
		$fields['skema'] = $this->input->post('skema');
		$fields['semester'] = $this->input->post('semester');
		$fields['validasi'] = $this->input->post('validasi');
		$this->session->set_userdata('filterdata_mitra_mbkm_unnes', $fields);
		$this->session->set_userdata('is_filtered_mitra_mbkm_unnes', TRUE);
		echo json_encode(['url' => base_url('administrator/mitra_mbkm_unnes')]);
	}

	public function clear_filter_session_data()
	{
		$this->session->unset_userdata('filterdata_mitra_mbkm_unnes');
		$this->session->unset_userdata('is_filtered_mitra_mbkm_unnes');
		echo json_encode(['url' => base_url('administrator/mitra_mbkm_unnes')]);
	}

	public function form_filter_data()
	{
		if (!$this->session->has_userdata('filterdata_mitra_mbkm_unnes')) {
			$fields['skema'] = 'all';
			$fields['semester'] = 'all';
			$fields['validasi'] = 'all';
			$this->session->set_userdata('filterdata_mitra_mbkm_unnes', $fields);
		}
		$skema = $this->model_mitra_mbkm_unnes->getskema();
		$data['skema'] = array_column($skema,'skema','skema');
		$data['skema']['all'] = 'ALL';
		$smt = array_semester([range(2020,date('Y')),['1','2']]);
		$data['semester'] = array_combine($smt,$smt);
		$data['semester']['all'] = 'ALL'; 
		$data['validasi'] = ['all'=>'ALL','Y'=>'Validasi','N'=>'Invalid'];
		$this->load->view('form-filter-data', $data);
	}
	/**
	 * show all Mitra Mbkm Unness
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('mitra_mbkm_unnes_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('mitra_mbkm_unnes_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('mitra_mbkm_unnes_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('mitra_mbkm_unnes_filter');
			$this->session->unset_userdata('mitra_mbkm_unnes_field');
		}
		$filter = $this->session->userdata('mitra_mbkm_unnes_filter');
		$field = $this->session->userdata('mitra_mbkm_unnes_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['mitra_mbkm_unness'] = $this->model_mitra_mbkm_unnes->get($filter, $field, $this->limit_page, $offset);
		$this->data['mitra_mbkm_unnes_counts'] = $this->model_mitra_mbkm_unnes->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/mitra_mbkm_unnes/index/',
			'total_rows'   => $this->model_mitra_mbkm_unnes->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Mitra Mbkm Unnes List');
		$this->render('backend/standart/administrator/mitra_mbkm_unnes/mitra_mbkm_unnes_list', $this->data);
	}

	/**
	 * Add new mitra_mbkm_unness
	 *
	 */
	public function add()
	{
		$this->is_allowed('mitra_mbkm_unnes_add');

		$this->template->title('Mitra Mbkm Unnes New');
		$this->render('backend/standart/administrator/mitra_mbkm_unnes/mitra_mbkm_unnes_add', $this->data);
	}

	/**
	 * Add New Mitra Mbkm Unness
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('mitra_mbkm_unnes_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('skema', 'Skema', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('semester', 'Semester', 'trim|required|max_length[5]');
		$this->form_validation->set_rules('program', 'Program', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('mitra', 'Mitra', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('unit_ks', 'MoA/IA/LoA (Unit)', 'trim|required|max_length[11]');


		if ($this->form_validation->run()) {

			$save_data = [
				'skema' => $this->input->post('skema'),
				'semester' => $this->input->post('semester'),
				'program' => $this->input->post('program'),
				'mitra' => $this->input->post('mitra'),
				'unit_ks' => $this->input->post('unit_ks'),
				'validasi' => $this->input->post('validasi'),
			];


			$save_mitra_mbkm_unnes = $this->model_mitra_mbkm_unnes->store($save_data);


			if ($save_mitra_mbkm_unnes) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_mitra_mbkm_unnes;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/mitra_mbkm_unnes/edit/' . $save_mitra_mbkm_unnes, 'Edit Mitra Mbkm Unnes'),
						anchor('administrator/mitra_mbkm_unnes', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/mitra_mbkm_unnes/edit/' . $save_mitra_mbkm_unnes, 'Edit Mitra Mbkm Unnes')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/mitra_mbkm_unnes');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/mitra_mbkm_unnes');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Mitra Mbkm Unness
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('mitra_mbkm_unnes_update');
		$row = $this->model_mitra_mbkm_unnes->find($id);
		if (is_groups_in(['Kaprodi', 'Unit'])) {
			if ($row->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa mengubah data, karena data sudah divalidasi', 'mbkm'), 'warning');
				redirect_back();
			}
		}
		$this->data['mitra_mbkm_unnes'] = $row;
		$kd_prodi = sprintf("'%s'", implode("', '", get_filter_prodi(true)));
		// $results = db_get_all_data('si_kerjasama', "tgl_selesai > curdate() AND kd_prodi IN ({$kd_prodi})");
		$results = db_get_all_data('si_kerjasama', "kd_prodi IN ({$kd_prodi})");
		if (!is_null($row->unit_ks)) {
			$cur_moa = db_get_all_data('si_kerjasama', ['id' => $row->unit_ks], 'row');
			if (!is_null($cur_moa)) {
				$tgl = DateTime::createFromFormat('Y-m-d', $cur_moa->tgl_selesai);
				$now = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
				if ($tgl < $now) $results[] = $cur_moa;
			}
		}
		$this->data['unitks'] = $results;
		$this->template->title('Mitra Mbkm Unnes Update');
		$this->render('backend/standart/administrator/mitra_mbkm_unnes/mitra_mbkm_unnes_update', $this->data);
	}

	/**
	 * Update Mitra Mbkm Unness
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('mitra_mbkm_unnes_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('skema', 'Skema', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('semester', 'Semester', 'trim|required|max_length[5]');
		$this->form_validation->set_rules('program', 'Program', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('mitra', 'Mitra', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('unit_ks', 'MoA/IA/LoA (Unit)', 'trim|required|max_length[11]');

		if ($this->form_validation->run()) {

			$save_data = [
				'skema' => $this->input->post('skema'),
				'semester' => $this->input->post('semester'),
				'program' => $this->input->post('program'),
				'mitra' => $this->input->post('mitra'),
				'unit_ks' => $this->input->post('unit_ks'),
				'validasi' => $this->input->post('validasi'),
			];


			$save_mitra_mbkm_unnes = $this->model_mitra_mbkm_unnes->change($id, $save_data);

			if ($save_mitra_mbkm_unnes) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/mitra_mbkm_unnes', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/mitra_mbkm_unnes/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/mitra_mbkm_unnes/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Mitra Mbkm Unness
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('mitra_mbkm_unnes_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'mitra_mbkm_unnes'), 'success');
		} else {
			set_message(cclang('error_delete', 'mitra_mbkm_unnes'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Mitra Mbkm Unness
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('mitra_mbkm_unnes_view');

		$this->data['mitra_mbkm_unnes'] = $this->model_mitra_mbkm_unnes->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Mitra Mbkm Unnes Detail');
		$this->render('backend/standart/administrator/mitra_mbkm_unnes/mitra_mbkm_unnes_view', $this->data);
	}

	/**
	 * delete Mitra Mbkm Unness
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$mitra_mbkm_unnes = $this->model_mitra_mbkm_unnes->find($id);



		return $this->model_mitra_mbkm_unnes->remove($id);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
			'skema',
			'semester',
			'program',
			'mitra',
			'unit_ks',
			'validasi',
		];
		$data = $this->model_mitra_mbkm_unnes->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_mitra_mbkm_unnes->export2($data, 'file', $selected_field);

		/*
		$this->is_allowed('mitra_mbkm_unnes_export');

		$this->model_mitra_mbkm_unnes->export(
			'mitra_mbkm_unnes', 
			'mitra_mbkm_unnes',
			$this->model_mitra_mbkm_unnes->field_search
		);
		*/
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('mitra_mbkm_unnes_export');

		$this->model_mitra_mbkm_unnes->pdf('mitra_mbkm_unnes', 'mitra_mbkm_unnes');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('mitra_mbkm_unnes_export');

		$table = $title = 'mitra_mbkm_unnes';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_mitra_mbkm_unnes->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
}


/* End of file mitra_mbkm_unnes.php */
/* Location: ./application/controllers/administrator/Mitra Mbkm Unnes.php */