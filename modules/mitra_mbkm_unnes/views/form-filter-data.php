<form action="" method="POST" role="form">
	<div class="row">
		<div class="col-md-8">
			<div class="form-group">
				<label for="">Skema</label>
				<?= form_dropdown('skema', $skema, fromsess('filterdata_mitra_mbkm_unnes')['skema'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label for="">Semester</label>
				<?= form_dropdown('semester', $semester, fromsess('filterdata_mitra_mbkm_unnes')['semester'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label for="">validasi</label>
				<?= form_dropdown('validasi', $validasi, fromsess('filterdata_mitra_mbkm_unnes')['validasi'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
			</div>
		</div>
	</div>
</form>

<script>
	$(function() {
		// $('select[name=name1],select[name=name2],select[name=name3]').select2({
		// 	theme: 'classic',
		// 	dropdownParent: $('#filtermitra_mbkm_unnes-dialog'),
		// });
		// $('select[name=name4]').select2({
		// 	tags:true
		// })
	})
</script>