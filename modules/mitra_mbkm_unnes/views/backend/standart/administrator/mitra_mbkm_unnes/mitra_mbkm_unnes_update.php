<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Mitra Mbkm Unnes <small>Edit Mitra Mbkm Unnes</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/mitra_mbkm_unnes'); ?>">Mitra Mbkm Unnes</a></li>
		<li class="active">Edit</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Mitra Mbkm Unnes</h3>
							<h5 class="widget-user-desc">Edit Mitra Mbkm Unnes</h5>
							<hr>
						</div>
						<?= form_open(base_url('administrator/mitra_mbkm_unnes/edit_save/' . $this->uri->segment(4)), [
							'name'    => 'form_mitra_mbkm_unnes',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_mitra_mbkm_unnes',
							'method'  => 'POST'
						]); ?>

						<div class="form-group ">
							<label for="skema" class="col-sm-2 control-label">Skema
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="skema" id="skema" readonly placeholder="Skema" value="<?= set_value('skema', $mitra_mbkm_unnes->skema); ?>">
								<small class="info help-block">
									<b>Input Skema</b> Max Length : 100.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="semester" class="col-sm-2 control-label">Semester
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="number" class="form-control" name="semester" id="semester" readonly placeholder="Semester" value="<?= set_value('semester', $mitra_mbkm_unnes->semester); ?>">
								<small class="info help-block">
									<b>Input Semester</b> Max Length : 5.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="program" class="col-sm-2 control-label">Program
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="program" id="program" readonly placeholder="Program" value="<?= set_value('program', $mitra_mbkm_unnes->program); ?>">
								<small class="info help-block">
									<b>Input Program</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="mitra" class="col-sm-2 control-label">Mitra
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="mitra" id="mitra" readonly placeholder="Mitra" value="<?= set_value('mitra', $mitra_mbkm_unnes->mitra); ?>">
								<small class="info help-block">
									<b>Input Mitra</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="unit_ks" class="col-sm-2 control-label">MoA/IA/LoA (Unit)
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="unit_ks" id="unit_ks" data-placeholder="Select MoA/IA/LoA (Unit)">
									<option value=""></option>
									<?php foreach ($unitks as $row) : ?>
										<option <?= $row->id ==  $mitra_mbkm_unnes->unit_ks ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->nama_mitra.' - '.$row->doc_nomor; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Input Unit Ks</b> Max Length : 11.</small>
							</div>
						</div>

						<?php if (is_groups_in(['Admin', 'Kerjasama'])) : ?>
							<div class="form-group ">
								<label for="validasi" class="col-sm-2 control-label">Validasi
								</label>
								<div class="col-sm-8">
									<select class="form-control chosen chosen-select" name="validasi" id="validasi" data-placeholder="Select Validasi">
										<option value=""></option>
										<option <?= $mitra_mbkm_unnes->validasi == "Y" ? 'selected' : ''; ?> value="Y">Valid</option>
										<option <?= $mitra_mbkm_unnes->validasi == "N" ? 'selected' : ''; ?> value="N">Invalid</option>
									</select>
									<small class="info help-block">
									</small>
								</div>
							</div>
						<?php endif;?>

						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	$(document).ready(function() {



		$('#btn_cancel').click(function() {
			swal({
					title: "Are you sure?",
					text: "the data that you have created will be in the exhaust!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/mitra_mbkm_unnes/index/' + "<?= $this->session->userdata('_page') ?>";
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();

			var form_mitra_mbkm_unnes = $('#form_mitra_mbkm_unnes');
			var data_post = form_mitra_mbkm_unnes.serializeArray();
			var save_type = $(this).attr('data-stype');
			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: form_mitra_mbkm_unnes.attr('action'),
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('form').find('.error-input').remove();
					$('.steps li').removeClass('error');
					if (res.success) {
						var id = $('#mitra_mbkm_unnes_image_galery').find('li').attr('qq-file-id');
						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						$('.data_file_uuid').val('');

					} else {
						if (res.errors) {
							parseErrorField(res.errors);
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/

		async function chain() {}

		chain();

	}); /*end doc ready*/
</script>