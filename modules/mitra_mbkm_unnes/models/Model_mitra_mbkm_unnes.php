<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_mitra_mbkm_unnes extends MY_Model
{

	private $primary_key    = 'id';
	private $table_name     = 'mitra_mbkm_unnes';
	public $field_search   = ['skema', 'semester', 'program', 'mitra', 'unit_ks', 'validasi'];

	public function __construct()
	{
		$config = array(
			'primary_key'   => $this->primary_key,
			'table_name'    => $this->table_name,
			'field_search'  => $this->field_search,
		);

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "mitra_mbkm_unnes." . $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "mitra_mbkm_unnes." . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			$where .= "(" . "mitra_mbkm_unnes." . $field . " LIKE '%" . $q . "%' )";
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);

		/**form filter data */
		if ($this->session->has_userdata('filterdata_mitra_mbkm_unnes')) {
			foreach (fromsess('filterdata_mitra_mbkm_unnes') as $key => $row) {
				if ($row != 'all') $this->db->having($key, $row);
			}
		}
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [], $result = TRUE)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "mitra_mbkm_unnes." . $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "mitra_mbkm_unnes." . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			$where .= "(" . "mitra_mbkm_unnes." . $field . " LIKE '%" . $q . "%' )";
		}

		if (is_array($select_field) and count($select_field)) {
			$this->db->select($select_field);
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);

		/**form filter data */
		if ($this->session->has_userdata('filterdata_mitra_mbkm_unnes')) {
			foreach (fromsess('filterdata_mitra_mbkm_unnes') as $key => $row) {
				if ($row != 'all') $this->db->having($key, $row);
			}
		}
		$this->db->limit($limit, $offset);

		$this->sortable();

		$query = $this->db->get($this->table_name);

		if ($result)
			return $query->result();
		else return $query;
	}
	public function getskema(){
		return $this->db->select('skema')->group_by('skema')->get($this->table_name)->result_array();
	}

	public function join_avaiable()
	{
		$this->db->join('si_kerjasama', 'si_kerjasama.id = mitra_mbkm_unnes.unit_ks', 'LEFT');

		$this->db->select('mitra_mbkm_unnes.*,si_kerjasama.nama_mitra as si_kerjasama_nama_mitra');


		return $this;
	}

	public function filter_avaiable()
	{

		if (!$this->aauth->is_admin()) {
		}

		return $this;
	}
}

/* End of file Model_mitra_mbkm_unnes.php */
/* Location: ./application/models/Model_mitra_mbkm_unnes.php */