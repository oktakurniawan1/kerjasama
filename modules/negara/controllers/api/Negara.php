<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Negara extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_negara');
	}

	/**
	 * @api {get} /negara/all Get all negaras.
	 * @apiVersion 0.1.0
	 * @apiName AllNegara 
	 * @apiGroup negara
	 * @apiHeader {String} X-Api-Key Negaras unique access-key.
	 * @apiPermission Negara Cant be Accessed permission name : api_negara_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Negaras.
	 * @apiParam {String} [Field="All Field"] Optional field of Negaras : id, nama_negara.
	 * @apiParam {String} [Start=0] Optional start index of Negaras.
	 * @apiParam {String} [Limit=10] Optional limit data of Negaras.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of negara.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataNegara Negara data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_negara_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'nama_negara'];
		$negaras = $this->model_api_negara->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_negara->count_all($filter, $field);
		$negaras = array_map(function($row){
						
			return $row;
		}, $negaras);

		$data['negara'] = $negaras;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Negara',
			'data'	 	=> $data,
			'total' 	=> $total,
		], API::HTTP_OK);
	}

		/**
	 * @api {get} /negara/detail Detail Negara.
	 * @apiVersion 0.1.0
	 * @apiName DetailNegara
	 * @apiGroup negara
	 * @apiHeader {String} X-Api-Key Negaras unique access-key.
	 * @apiPermission Negara Cant be Accessed permission name : api_negara_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Negaras.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of negara.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NegaraNotFound Negara data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_negara_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'nama_negara'];
		$negara = $this->model_api_negara->find($id, $select_field);

		if (!$negara) {
			$this->response([
					'status' 	=> false,
					'message' 	=> 'Blog not found'
				], API::HTTP_NOT_FOUND);
		}

					
		$data['negara'] = $negara;
		if ($data['negara']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Negara',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Negara not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /negara/add Add Negara.
	 * @apiVersion 0.1.0
	 * @apiName AddNegara
	 * @apiGroup negara
	 * @apiHeader {String} X-Api-Key Negaras unique access-key.
	 * @apiPermission Negara Cant be Accessed permission name : api_negara_add
	 *
 	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_negara_add', false);

		
		if ($this->form_validation->run()) {

			$save_data = [
			];
			
			$save_negara = $this->model_api_negara->store($save_data);

			if ($save_negara) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /negara/update Update Negara.
	 * @apiVersion 0.1.0
	 * @apiName UpdateNegara
	 * @apiGroup negara
	 * @apiHeader {String} X-Api-Key Negaras unique access-key.
	 * @apiPermission Negara Cant be Accessed permission name : api_negara_update
	 *
	 * @apiParam {Integer} id Mandatory id of Negara.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_negara_update', false);

		
		
		if ($this->form_validation->run()) {

			$save_data = [
			];
			
			$save_negara = $this->model_api_negara->change($this->post('id'), $save_data);

			if ($save_negara) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /negara/delete Delete Negara. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteNegara
	 * @apiGroup negara
	 * @apiHeader {String} X-Api-Key Negaras unique access-key.
	 	 * @apiPermission Negara Cant be Accessed permission name : api_negara_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Negaras .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_negara_delete', false);

		$negara = $this->model_api_negara->find($this->post('id'));

		if (!$negara) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Negara not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_negara->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Negara deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Negara not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
}

/* End of file Negara.php */
/* Location: ./application/controllers/api/Negara.php */