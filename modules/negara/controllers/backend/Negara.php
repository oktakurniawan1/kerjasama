<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Negara Controller
*| --------------------------------------------------------------------------
*| Negara site
*|
*/
class Negara extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_negara');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Negaras
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('negara_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if(!is_null($q)) $this->session->set_userdata('negara_filter',$q);
		if(!is_null($f)) $this->session->set_userdata('negara_field',$f);
		$this->session->set_userdata('_page',$offset);

		if(!is_null($this->input->get('reset_filter'))){
			$this->session->unset_userdata('negara_filter');
			$this->session->unset_userdata('negara_field');
		}
		$filter = $this->session->userdata('negara_filter');
		$field = $this->session->userdata('negara_field');
		$offset = $this->session->userdata('_page',$offset);

		$this->data['negaras'] = $this->model_negara->get($filter, $field, $this->limit_page, $offset);
		$this->data['negara_counts'] = $this->model_negara->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/negara/index/',
			'total_rows'   => $this->model_negara->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Negara List');
		$this->render('backend/standart/administrator/negara/negara_list', $this->data);
	}
	
	/**
	* Add new negaras
	*
	*/
	public function add()
	{
		$this->is_allowed('negara_add');

		$this->template->title('Negara New');
		$this->render('backend/standart/administrator/negara/negara_add', $this->data);
	}

	/**
	* Add New Negaras
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('negara_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('nama_negara', 'Nama Negara', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'nama_negara' => $this->input->post('nama_negara'),
			];

			
			$save_negara = $this->model_negara->store($save_data);
            

			if ($save_negara) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_negara;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/negara/edit/' . $save_negara, 'Edit Negara'),
						anchor('administrator/negara', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/negara/edit/' . $save_negara, 'Edit Negara')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/negara');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/negara');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Negaras
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('negara_update');

		$this->data['negara'] = $this->model_negara->find($id);

		$this->template->title('Negara Update');
		$this->render('backend/standart/administrator/negara/negara_update', $this->data);
	}

	/**
	* Update Negaras
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('negara_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('nama_negara', 'Nama Negara', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'nama_negara' => $this->input->post('nama_negara'),
			];

			
			$save_negara = $this->model_negara->change($id, $save_data);

			if ($save_negara) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/negara', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/negara/index/'.$this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/negara/index/'.$this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Negaras
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('negara_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'negara'), 'success');
        } else {
            set_message(cclang('error_delete', 'negara'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Negaras
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('negara_view');

		$this->data['negara'] = $this->model_negara->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Negara Detail');
		$this->render('backend/standart/administrator/negara/negara_view', $this->data);
	}
	
	/**
	* delete Negaras
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$negara = $this->model_negara->find($id);

		
		
		return $this->model_negara->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('negara_export');

		$this->model_negara->export(
			'negara', 
			'negara',
			$this->model_negara->field_search
		);
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('negara_export');

		$this->model_negara->pdf('negara', 'negara');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('negara_export');

		$table = $title = 'negara';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_negara->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file negara.php */
/* Location: ./application/controllers/administrator/Negara.php */