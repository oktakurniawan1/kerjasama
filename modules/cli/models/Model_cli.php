<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_cli extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}
	public function update_categori($data)
	{
		foreach ($data as $key => $val) {
			$data = [
				'id_skim'   => $key,
				'id_bid'    => $val[1],
				'bidang'    => $val[2],
				'nama_skim' => $val[0]
			];
			// $this->db->insert('skim_sipp', $data);
			$insert_query = $this->db->insert_string('skim_sipp', $data);
			$insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
			$this->db->query($insert_query);
		}
	}
	public function updateKaryawan($data)
	{
		$this->db->trans_start();
		$this->db->truncate('karyawan');
		foreach ($data as $row) {
			$this->db->insert('karyawan', (array) $row);
			//echo "{$row->nip}\n";
		}
		$this->db->trans_complete();
	}
	public function update_mhs_aktif()
	{
		$sql = "SELECT
        m.nim,
        m.nama,
        m.program_studi,
        concat(
            p.nama_ktm,
            if(p.konsentrasi REGEXP '[[:alpha:]]+',concat(' - ',p.konsentrasi),''),
            if(p.strjjg REGEXP '[[:alpha:]]+',concat(' - ',p.strjjg),'')
        ) as prodi,
        m.ta,
        m.email,
        m.telp,
        tlhr 
    FROM
        sikadu_unnes.mhs m
        INNER JOIN sikadu_unnes.prodi p ON (
        m.program_studi = p.kode 
        AND m.lls = '0')";
	}
}
