<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *| --------------------------------------------------------------------------
 *| Rekap Controller
 *| --------------------------------------------------------------------------
 *| Rekap
 *|
 */

use GuzzleHttp\Client;
use Aws\S3\S3Client;
use Aws\Exception\AwsException;

class Cli extends MX_Controller
{

	private $url = "http://sekaran.unnes.ac.id/api/";
	private $username = "kerjasama";
	private $password = "k4d4lb4hagi4";
	public function __construct()
	{
		if (!$this->input->is_cli_request()) {
			die('only command line request!');
		}
		$this->load->model('Model_cli');
		$this->load->helper('file'); // Load file helper
		$this->load->config('aws'); // Load AWS config file
		$this->load->database(); // Load database configuration
	}
	public function index()
	{
	}
	public function callapi($path)
	{
		$url = $this->url . $path;
		$result = callApiWithURL($url, [], $this->username, $this->password);
		$result = json_decode($result);
		if ($result->no_data) {
			return array();
		} else {
			return obj2arr($result);
		}
	}
	public function get_katerogi_sipp()
	{
		$result = $this->callapi("kategorikegiatantridharma");
		$this->Model_cli->update_categori($result);
	}
	public function get_sipp_dosen()
	{
		// $result = $this->callapi("tridharma/198707062014041003/2019-01-01/2019-12-31/791,801");
		// print_r($result);die();
		//$tahuns = ['2017','2018','2019','2014','2013','2012','2011','2010','2020'];
		$tahuns = range(2010, 2020);
		foreach ($tahuns as $tahun) {
			$rentang = "$tahun-01-01/$tahun-12-31";
			$nips = $this->db->get_where('_nip', ["_$tahun" => '0'])->result();
			//$nips = $this->db->get('_nip')->result();
			$keg = $this->db->select("GROUP_CONCAT(id_skim) as idkeg", false)->where('used', 'Y')->from('skim_sipp')->get()->row();
			foreach ($nips as $i => $row) {
				$result = $this->callapi("tridharma/{$row->nip_baru}/{$rentang}/{$keg->idkeg}");
				if (count($result)) {
					foreach ($result['detail'] as $key => $val) {
						$val['id_sipp'] = $key;
						$val['nip'] = $row->nip_baru;
						if (!array_key_exists('skim_kegiatan', $val)) {
							$skim = $this->db->select('nama_skim')->where('id_skim', $val['kategori_kegiatan'])->get('skim_sipp')->row();
							$val['skim_kegiatan'] = $skim->nama_skim;
						}
						$insert_query = $this->db->insert_string('tridarma', $val);
						$insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
						$this->db->query($insert_query);
					}
					$this->db->where('nip_baru', $row->nip_baru)->update('_nip', ["_$tahun" => 1]);
				} else $this->db->where('nip_baru', $row->nip_baru)->update('_nip', ["_$tahun" => 'x']);
				echo "{$tahun} NIP : " . $row->nip_baru . " | {$i} \n";
			}
		}
	}
	public function create_column_skim_sipp()
	{
		$result = $this->callapi("kolomkegiatantridharma/79");
		// dd($result);
		$this->load->dbforge();
		$keg = $this->db->select("id_skim")->where('used', '1')->from('skim_sipp')->get()->result();
		$col = [];
		foreach ($keg as $row) {
			$result = $this->callapi("kolomkegiatantridharma/{$row->id_skim}");
			if (count($result)) {
				foreach ($result as $k => $val) {
					if (!in_array($val['ref_column'], $col))
						$this->dbforge->add_column('tridarma1', "COLUMN {$val['ref_column']} VARCHAR(255)");
					$col[] = $val['ref_column'];
				}
			}
		}
	}
	/**
	 * student mobility inBound
	 */
	public function get_student_mob_inbound()
	{
		$result = callApiWithURL("https://daftar.unnes.ac.id/index.php/api/getdata/get_pendaftar/3");
		$result = callApiWithURL("https://daftar.unnes.ac.id/index.php/api/getdata/get_pendaftar/3");
		if ($result) {
			print_r($result);
		}
	}

	/**
	 * CRONJOB
	 * @return void 
	 */

	public function getDoskarAktif()
	{
		$pass = md5(date('d-Y-m') . '123');
		//die($pass);
		$url = "http://pagoda.unnes.ac.id/api/getDoskarAktif?pass=$pass";
		$result = callApiWithCurl($url);
		$result = json_decode($result);
		// print_r($result);
		$this->Model_cli->updateKaryawan($result);
	}

	public function update_sipp($tahun = null)
	{
		if (is_null($tahun)) $tahun = date('Y');
		$rentang = $tahun . '-01-01/' . $tahun . '-12-31';
		$nips = $this->db->select('nip_baru')->get_where('pegawai', ['jns_pegawai' => 'dosen'])->result();
		$keg = $this->db->select("GROUP_CONCAT(id_skim) as idkeg", false)->where('used', 'Y')->from('skim_sipp')->get()->row();
		$total = count($nips);
		foreach ($nips as $i => $row) {
			// echo "$row->nip_baru\n";
			$result = $this->callapi("tridharma/{$row->nip_baru}/{$rentang}/{$keg->idkeg}");
			if (count($result)) {
				foreach ($result['detail'] as $key => $val) {
					$val['id_sipp'] = $key;
					$val['nip'] = $row->nip_baru;
					if (!array_key_exists('skim_kegiatan', $val)) {
						$skim = $this->db->select('nama_skim')->where('id_skim', $val['kategori_kegiatan'])->get('skim_sipp')->row();
						$val['skim_kegiatan'] = $skim->nama_skim;
					}
					$insert_query = $this->db->insert_string('tridarma', $val);
					$insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
					$this->db->query($insert_query);
				}
			}
			// echo "NIP : " . $row->nip_baru . " | {$total} | {$i} \n";
		}
	}

	/**
	 * BACKUP DATABASE TO S3
	 * @return void 
	 */
	public function backups3()
	{
		$args    = $_SERVER['argv'];
		$options = [];
		foreach ($args as $arg) {
			switch ($arg) {
				case '--db':
					$options['db'] = true;
					break;
				case '--app':
					$options['app'] = true;
					break;
			}
		}
		if (isset($options['db'])) {
			$this->backupDatabaseToS3();
		}

		if (isset($options['app'])) {
			$this->backupAppFilesToS3();
		}
	}
	private function backupDatabaseToS3()
	{
		// Logic to backup database to S3
		$databaseName = $this->db->database;
		$username     = $this->db->username;
		$password     = $this->db->password;
		$hostname     = $this->db->hostname;

		// Example: Use mysqldump to create database dump file
		$dumpFileName = 'database_backup.sql';
		$command = "mysqldump -u $username -p$password -h$hostname $databaseName > $dumpFileName";
		exec($command);

		// Upload database dump file to S3
		$bucket   = 'myunnes-backup';
		$key      = 'Simks/' . date('Y-m-d_His') . 'database_backup.sql';
		$filePath = $dumpFileName;

		try {
			$s3Client = new S3Client([
				'version'     => 'latest',
				'region'      => $this->config->item('aws_region'),
				'credentials' =>
				[
					'key'    => $this->config->item('aws_access_key'),
					'secret' => $this->config->item('aws_secret_key'),
				],
			]);

			$result = $s3Client->putObject([
				'Bucket' => $bucket,
				'Key'    => $key,
				'Body'   => fopen($filePath, 'r'),
				'ACL'    => 'private', //  public-read
			]);

			echo "Database backup uploaded successfully: " . $result['ObjectURL'] . PHP_EOL;
		} catch (AwsException $e) {
			echo "Error uploading database backup: " . $e->getMessage() . PHP_EOL;
		}

		// Clean up: Remove the temporary database dump file
		unlink($dumpFileName);
	}

	private function backupAppFilesToS3()
	{
		// Path to directory to backup (adjust as needed)
		$sourceDir = FCPATH . 'application'; // Application directory

		// Create a temporary zip file
		$backupFileName = 'app_backup_' . date('YmdHis') . '.zip';
		$zipFilePath = sys_get_temp_dir() . '/' . $backupFileName;

		// Zip the application directory
		$this->zipFiles($sourceDir, $zipFilePath);

		// Upload zip file to S3
		$bucket = 'your-bucket';
		$key = 'Simks/' . $backupFileName;

		try {
			$s3Client = new S3Client([
				'version'     => 'latest',
				'region'      => $this->config->item('aws_region'),
				'credentials' =>
				[
					'key'    => $this->config->item('aws_access_key'),
					'secret' => $this->config->item('aws_secret_key'),
				],
			]);

			$result = $s3Client->putObject([
				'Bucket' => $bucket,
				'Key'    => $key,
				'Body'   => fopen($zipFilePath, 'r'),
				'ACL'    => 'private',    //  public-read
			]);

			echo "App files backup uploaded successfully: " . $result['ObjectURL'] . PHP_EOL;
		} catch (AwsException $e) {
			echo "Error uploading app files backup: " . $e->getMessage() . PHP_EOL;
		}

		// Clean up: Remove the temporary zip file
		unlink($zipFilePath);
	}

	private function zipFiles($sourceDir, $zipFilePath)
	{
		// Create a zip archive
		$zip = new ZipArchive();
		if ($zip->open($zipFilePath, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE) {
			// Add files to zip archive
			$files = new RecursiveIteratorIterator(
				new RecursiveDirectoryIterator($sourceDir),
				RecursiveIteratorIterator::LEAVES_ONLY
			);

			foreach ($files as $name => $file) {
				// Skip directories (we only want files)
				if (!$file->isDir()) {
					// Get real and relative path for current file
					$filePath = $file->getRealPath();
					$relativePath = substr($filePath, strlen($sourceDir) + 1);

					// Add current file to archive
					$zip->addFile($filePath, $relativePath);
				}
			}

			$zip->close();
		}
	}
	/**END BACKUP DATABASE TO S3 */


	public function cronrekapmbkm()
	{
		$client = new Client([
			'base_uri' => 'https://mbkm.unnes.ac.id/api/simks/',
			'auth'     => ['simks', 'iyngncmblozglfwhgwnwdvvxhxkrnauc']
		]);
		$response = $client->request('GET', 'student-exchange', [
			'query' => ['semester' => '20211']
		]);

		$data = json_decode($response->getBody());
		$data = obj2arr($data);
		if ($data['success'] == 1) {
			foreach ($data['data'] as $row) {
				$row['tahun'] = substr($row['semester'], 0, 4);
				$insert_query = $this->db->insert_string('data_mhs_mbkm', $row);
				$insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
				$this->db->query($insert_query);
			}
		}
	}
	public function antrian_telegram()
	{
		$data = $this->db->where('is_exec', 'N')->get('antrian_telegram')->result();
		foreach ($data as $row) {
			send_telegram($row->identitas, $row->pesan);
			$this->db->where('id', $row->id)->update('antrian_telegram', ['is_exec' => 'Y']);
		}
	}
	public function cron_mbkm_unnes()
	{
		ini_set('memory_limit', '512M');
		$data = callApi($this->config->item('apimbkm'), $this->config->item('authmbkm'), 'bmu-partnership', 'GET', []);
		foreach ($data['data'] as $row) {
			$row['mitra']  = $this->db->escape($row['mitra']);
			$insert_query  = $this->db->insert_string('mitra_mbkm_unnes', $row);
			$insert_query .= " ON DUPLICATE KEY UPDATE 
				mitra_mbkm_unnes.tanggal_mulai   = '{$row['tanggal_mulai']}',
				mitra_mbkm_unnes.tanggal_selesai = '{$row['tanggal_selesai']}',
				mitra_mbkm_unnes.mitra_id        = '{$row['mitra_id']}',
				mitra_mbkm_unnes.mitra           = {$row['mitra']}"; // Note: No single quotes here
			$this->db->query($insert_query);
		}
	}
	public function test_cron()
	{
		$this->db->insert('test_cron', ['date_created' => date('Y-m-d H:i:s')]);
	}
	public function update_mhs_asing()
	{
		$db = $this->load->database('sikadu', TRUE);
		$data = $this->db->select('nim')->where('lls', 0)->get('mhs_asing')->result();
		foreach ($data as $row) {
			$mhs = $db->where('nim', $row->nim)->select(['lls', 'tgl_lulus', 'ipk'])->get('mhs')->row();

			$update['lls'] = $mhs->lls;
			$update['tgl_lulus'] = $mhs->tgl_lulus;
			$update['ipk'] = $mhs->ipk;

			$this->db->where('nim', $row->nim)->update('mhs_asing', $update);
		}
	}
	public function update_data_local_mhs()
	{
		$dbsikadu = $this->load->database('sikadu', TRUE);
		$limit = 10000;
		$offset = 0;

		do {
			$dbsikadu->select('
				m.nim,
				m.nama,
				m.program_studi,
				m.kodefak,
				CONCAT(p.nama_ktm, IF(p.konsentrasi REGEXP "[[:alnum:]]+", CONCAT(" - ", p.konsentrasi), ""), IF(p.strjjg REGEXP "[[:alnum:]]+", CONCAT(" - ", p.strjjg), "")) as prodi,
				m.ta,
				m.email,
				m.telp,
				m.tlhr,
				m.lls,
				m.tgl_lulus,
				m.ipk
			');
			$dbsikadu->from('mhs m');
			$dbsikadu->join('prodi p', 'm.program_studi = p.kode', 'inner');
			$dbsikadu->limit($limit, $offset);
			$query = $dbsikadu->get();

			if ($query->num_rows() > 0) {
				$this->db->trans_start();
				foreach ($query->result_array() as $row) {
					// Insert or update logic
					$this->db->where('nim', $row['nim']);
					$exist = $this->db->get('mhs')->row();

					if ($exist) {
						$this->db->where('nim', $row['nim']);
						$this->db->update('mhs', $row);
					} else {
						$this->db->insert('mhs', $row);
					}
				}
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE) {
					echo "Gagal melakukan update pada mhs";
					break;
				}
			}

			$offset += $limit;
		} while ($query->num_rows() == $limit);
	}

	/**
	 * Generate download exel
	 */
	public function cron_generate_export_excel()
	{
		ini_set('memory_limit', '512M');
		$this->load->helper('string');
		$this->load->model('../../si_kerjasama/models/model_si_kerjasama');
		$data = $this->db->where('is_finish', 0)->get('export_file')->result();
		foreach ($data as $row) {
			$result = $this->db->query($row->sql_query);
			$this->model_si_kerjasama->export2($result, null, json_decode($row->selected_field, true), json_decode($row->format, true), true, $row->file_name);
			$this->db->where('id', $row->id)->update('export_file', ['is_finish' => 1]);
		}

		$data = $this->db->where('created_at <', 'NOW() - INTERVAL 1 HOUR', FALSE)
			->where('is_finish', 1)->get('export_file')->result();
		foreach ($data as $row) {
			$path = FCPATH . '/uploads/excel/' . $row->file_name;
			if (is_file($path)) {
				unlink($path);
			}
			$this->db->where('id', $row->id)->delete('export_file');
		}
	}
	public function cron_simpeg_baru()
	{
		$client = new Client([
			'base_uri' => 'https://simpeg2.unnes.ac.id/api/',
			'timeout'  => 30.0,
			'auth'     => [$this->config->item('simpeg2_api_username'), $this->config->item('simpeg2_api_password')],
			'verify'   => false, // Menambahkan opsi ini untuk melewati verifikasi sertifikat SSL
		]);

		// Dapatkan data unit
		try {
			$response = $client->request('GET', 'ref/unit');
			$unitData = json_decode($response->getBody(), true);

			if ($unitData['status'] === 'success' && isset($unitData['data'])) {
				foreach ($unitData['data'] as $unit) {
					$this->db->query("
                        INSERT INTO unit (id_unit, induk, level, id_jns_unit, nm_unit, nm_unit_singkat, id_akademik, jenjang, kode, is_show, created_at, updated_at, deleted_at, created_by, updated_by, deleted_by)
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                        ON DUPLICATE KEY UPDATE 
                            induk           = VALUES(induk),
                            level           = VALUES(level),
                            id_jns_unit     = VALUES(id_jns_unit),
                            nm_unit         = VALUES(nm_unit),
                            nm_unit_singkat = VALUES(nm_unit_singkat),
                            id_akademik     = VALUES(id_akademik),
                            jenjang         = VALUES(jenjang),
                            kode            = VALUES(kode),
                            is_show         = VALUES(is_show),
                            deleted_by      = VALUES(deleted_by)
                    ", [
						$unit['id_unit'], $unit['induk'], $unit['level'], $unit['id_jns_unit'], $unit['nm_unit'], $unit['nm_unit_singkat'], $unit['id_akademik'], $unit['jenjang'], $unit['kode'], $unit['is_show'], $unit['created_at'], $unit['updated_at'], $unit['deleted_at'], $unit['created_by'], $unit['updated_by'], $unit['deleted_by']
					]);
				}
				echo "Data unit berhasil diimpor atau diperbarui.\n";
			} else {
				echo "Data unit tidak berhasil diambil.\n";
			}
		} catch (Exception $e) {
			echo "Error: " . $e->getMessage() . "\n";
		}
		// Dapatkan jumlah total data pegawai
		try {
			$response = $client->request('GET', 'pegawai/count');
			$dataCount = json_decode($response->getBody(), true)['data'];
			$chunkSize = 500;
			$pages = ceil($dataCount / $chunkSize);

			for ($page = 1; $page <= $pages; $page++) {
				$response = $client->request('GET', 'pegawai', [
					'query' => ['per_page' => $chunkSize, 'page' => $page]
				]);
				$responseData = json_decode($response->getBody(), true);

				if ($responseData['success'] && isset($responseData['data']['data'])) {
					$dataPegawai = $responseData['data']['data'];

					if ($page === 1) {
						$this->alterTableIfNeeded($dataPegawai[0]);
					}

					$this->db->trans_start();
					foreach ($dataPegawai as $pegawai) {
						$this->insertOrUpdatePegawai($pegawai);
					}
					$this->db->trans_complete();

					if ($this->db->trans_status() === FALSE) {
						throw new Exception("Gagal menyimpan data pada halaman $page");
					}
				} else {
					throw new Exception("Gagal mengambil data pada halaman $page");
				}
			}
			echo "Data pegawai berhasil diimpor atau diperbarui.\n";
		} catch (Exception $e) {
			echo "Error: " . $e->getMessage() . "\n";
		}
		/**
		 * update auth_users
		 */
		$sql = "UPDATE aauth_users 
				INNER JOIN pegawai ON aauth_users.username = pegawai.nip_baru 
				SET aauth_users.full_name = pegawai.nama_gelar";
		$this->db->query($sql);
		echo "Data user berhasil diperbarui.\n";
	}
	private function insertOrUpdatePegawai($pegawai)
	{
		$existingColumns = $this->db->list_fields('pegawai');
		$filteredData = array_intersect_key($pegawai, array_flip($existingColumns));

		$keyColumn = 'id_pegawai'; // Sesuaikan dengan kolom kunci yang sebenarnya

		$existingRecord = $this->db->get_where('pegawai', [$keyColumn => $filteredData[$keyColumn]])->row_array();

		if ($existingRecord) {
			$this->db->where($keyColumn, $filteredData[$keyColumn]);
			$this->db->update('pegawai', $filteredData);
		} else {
			$this->db->insert('pegawai', $filteredData);
		}
	}
	/**
	 * Fungsi untuk mendeteksi kolom baru dan alter tabel jika diperlukan
	 */
	private function alterTableIfNeeded($sampleData)
	{
		$previousColumn = '';

		foreach ($sampleData as $column => $value) {
			$columnExists = $this->db->query("SHOW COLUMNS FROM pegawai LIKE '$column'")->num_rows() > 0;

			if (!$columnExists) {
				$columnType = $this->detectColumnType($column, $value);
				$positionClause = $previousColumn ? "AFTER `$previousColumn`" : "FIRST";

				$sql = "ALTER TABLE pegawai ADD COLUMN `$column` $columnType $positionClause";
				$this->db->query($sql);
			}

			$previousColumn = $column;
		}
	}

	private function detectColumnType($columnName, $value)
	{
		if (preg_match('/^(id|.*_id|id_.*)$/i', $columnName)) {
			if (is_string($value) && strlen($value) === 36) {
				return 'CHAR(36)';
			}
			if (is_numeric($value)) {
				return strlen($value) > 10 ? 'BIGINT' : 'INT';
			}
			return 'VARCHAR(50)';
		} elseif (preg_match('/tanggal|tgl|date|tmt/i', $columnName)) {
			return 'DATE';
		} elseif (is_bool($value)) {
			return 'BOOLEAN';
		} elseif (is_numeric($value)) {
			return strpos($value, '.') !== false ? 'DECIMAL(10,2)' : 'INT';
		}
		return 'VARCHAR(255)';
	}
}
