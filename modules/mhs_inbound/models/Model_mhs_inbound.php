<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_mhs_inbound extends MY_Model
{

    private $primary_key    = 'id';
    private $table_name     = 'mhs_inbound';
    public $field_search   = ['nim', 'nama', 'id_inb', 'asal_univ', 'prodi_unnes', 'tgl_masuk', 'username', 'password'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
        );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "mhs_inbound." . $field . " LIKE '%" . $q . "%' ";
                    if($field =='kd_prodi') $where .= "OR " . "ref_prodi.nama_prodi LIKE '%" . $q . "%' ";
                    if($field =='id_inb') $where .= "OR " . "jenis_inbound.jenis_inb LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "mhs_inbound." . $field . " LIKE '%" . $q . "%' ";
                    if($field =='kd_prodi') $where .= "OR " . "ref_prodi.nama_prodi LIKE '%" . $q . "%' ";
                    if($field =='id_inb') $where .= "OR " . "jenis_inbound.jenis_inb LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '(' . $where . ')';
        } else {
            if($field =='kd_prodi') $where .= "(" . "ref_prodi.nama_prodi LIKE '%" . $q . "%') ";
            else if($field =='id_inb') $where .= "(" . "jenis_inbound.jenis_inb LIKE '%" . $q . "%') ";
            else $where .= "(" . "mhs_inbound." . $field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "mhs_inbound." . $field . " LIKE '%" . $q . "%' ";
                    if($field =='prodi_unnes') $where .= "OR " . "ref_prodi.nama_prodi LIKE '%" . $q . "%' ";
                    if($field =='id_inb') $where .= "OR " . "jenis_inbound.jenis_inb LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "mhs_inbound." . $field . " LIKE '%" . $q . "%' ";
                    if($field =='prodi_unnes') $where .= "OR " . "ref_prodi.nama_prodi LIKE '%" . $q . "%' ";
                    if($field =='id_inb') $where .= "OR " . "jenis_inbound.jenis_inb LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '(' . $where . ')';
        } else {
            if($field =='prodi_unnes') $where .= "(" . "ref_prodi.nama_prodi LIKE '%" . $q . "%') ";
            else if($field =='id_inb') $where .= "(" . "jenis_inbound.jenis_inb LIKE '%" . $q . "%') ";
            else $where .= "(" . "mhs_inbound." . $field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) and count($select_field)) {
            $this->db->select($select_field);
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);

        $this->sortable();

        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable()
    {
        $this->db->join('jenis_inbound', 'jenis_inbound.id = mhs_inbound.id_inb', 'LEFT');
        $this->db->join('ref_prodi', 'ref_prodi.kode = mhs_inbound.prodi_unnes', 'LEFT');

        $this->db->select('mhs_inbound.*,jenis_inbound.jenis_inb as jenis_inbound_jenis_inb,ref_prodi.nama_prodi as ref_prodi_nama_prodi');

        return $this;
    }

    public function filter_avaiable()
    {

        // if (!$this->aauth->is_admin()) {
        // }
        $prodi = [];
        if (is_member('Kaprodi')) {
            if (is_non_user()) $prodi[] = fromsess('kd_prodi');
            else {
                $prodi = get_user_data('prodi');
                $prodi = explode(',', $prodi);
            }
            $this->db->where_in('prodi_unnes', $prodi);
        }
        if (is_member('Unit')) {
            if (is_non_user()) $prodi[] = fromsess('kd_prodi');
            else {
                $prodi = get_user_data('prodi');
                $prodi = explode(',', $prodi);
            }
            $this->db->where_in('prodi_unnes', $prodi);
        }

        return $this;
    }
}

/* End of file Model_mhs_inbound.php */
/* Location: ./application/models/Model_mhs_inbound.php */