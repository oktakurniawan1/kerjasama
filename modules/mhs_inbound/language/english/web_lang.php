<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['mhs_inbound'] = 'Mahasiswa Inbound';
$lang['id'] = 'Id';
$lang['nim'] = 'Nim';
$lang['nama'] = 'Nama';
$lang['id_inb'] = 'Jenis Inbound';
$lang['asal_univ'] = 'Asal Universitas';
$lang['prodi_unnes'] = 'Prodi Unnes';
$lang['tgl_masuk'] = 'Tanggal Masuk';
$lang['username'] = 'Username';
$lang['password'] = 'Password';
