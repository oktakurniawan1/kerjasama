<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Mhs Inbound Controller
 *| --------------------------------------------------------------------------
 *| Mhs Inbound site
 *|
 */
class Mhs_inbound extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_mhs_inbound');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * show all Mhs Inbounds
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('mhs_inbound_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('mhs_inbound_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('mhs_inbound_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('mhs_inbound_filter');
			$this->session->unset_userdata('mhs_inbound_field');
		}
		$filter = $this->session->userdata('mhs_inbound_filter');
		$field = $this->session->userdata('mhs_inbound_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['mhs_inbounds'] = $this->model_mhs_inbound->get($filter, $field, $this->limit_page, $offset);
		$this->data['mhs_inbound_counts'] = $this->model_mhs_inbound->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/mhs_inbound/index/',
			'total_rows'   => $this->model_mhs_inbound->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Mhs Inbound List');
		$this->render('backend/standart/administrator/mhs_inbound/mhs_inbound_list', $this->data);
	}

	/**
	 * Add new mhs_inbounds
	 *
	 */
	public function add()
	{
		$this->is_allowed('mhs_inbound_add');

		$this->template->title('Mhs Inbound New');
		$this->render('backend/standart/administrator/mhs_inbound/mhs_inbound_add', $this->data);
	}

	/**
	 * Add New Mhs Inbounds
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('mhs_inbound_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('nim', 'Nim', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('asal_univ', 'Asal Universitas', 'trim|max_length[255]');
		$this->form_validation->set_rules('prodi_unnes', 'Prodi Unnes', 'trim|max_length[8]');
		$this->form_validation->set_rules('tgl_masuk', 'Tanggal Masuk', 'trim|callback_valid_date');
		$this->form_validation->set_rules('username', 'Username', 'trim|max_length[255]');
		$this->form_validation->set_rules('password', 'Password', 'trim|max_length[255]');


		if ($this->form_validation->run()) {

			$save_data = [
				'nim' => $this->input->post('nim'),
				'nama' => $this->input->post('nama'),
				'id_inb' => $this->input->post('id_inb'),
				'asal_univ' => $this->input->post('asal_univ'),
				'prodi_unnes' => $this->input->post('prodi_unnes'),
				'tgl_masuk' => $this->input->post('tgl_masuk'),
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
			];


			$save_mhs_inbound = $this->model_mhs_inbound->store($save_data);


			if ($save_mhs_inbound) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_mhs_inbound;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/mhs_inbound/edit/' . $save_mhs_inbound, 'Edit Mhs Inbound'),
						anchor('administrator/mhs_inbound', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/mhs_inbound/edit/' . $save_mhs_inbound, 'Edit Mhs Inbound')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/mhs_inbound');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/mhs_inbound');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Mhs Inbounds
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('mhs_inbound_update');

		$this->data['mhs_inbound'] = $this->model_mhs_inbound->find($id);

		$this->template->title('Mhs Inbound Update');
		$this->render('backend/standart/administrator/mhs_inbound/mhs_inbound_update', $this->data);
	}

	/**
	 * Update Mhs Inbounds
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('mhs_inbound_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('nim', 'Nim', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('asal_univ', 'Asal Universitas', 'trim|max_length[255]');
		$this->form_validation->set_rules('prodi_unnes', 'Prodi Unnes', 'trim|max_length[8]');
		$this->form_validation->set_rules('tgl_masuk', 'Tanggal Masuk', 'trim|callback_valid_date');
		$this->form_validation->set_rules('username', 'Username', 'trim|max_length[255]');
		$this->form_validation->set_rules('password', 'Password', 'trim|max_length[255]');

		if ($this->form_validation->run()) {

			$save_data = [
				'nim' => $this->input->post('nim'),
				'nama' => $this->input->post('nama'),
				'id_inb' => $this->input->post('id_inb'),
				'asal_univ' => $this->input->post('asal_univ'),
				'prodi_unnes' => $this->input->post('prodi_unnes'),
				'tgl_masuk' => $this->input->post('tgl_masuk'),
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
			];


			$save_mhs_inbound = $this->model_mhs_inbound->change($id, $save_data);

			if ($save_mhs_inbound) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/mhs_inbound', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/mhs_inbound/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/mhs_inbound/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Mhs Inbounds
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('mhs_inbound_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'mhs_inbound'), 'success');
		} else {
			set_message(cclang('error_delete', 'mhs_inbound'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Mhs Inbounds
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('mhs_inbound_view');

		$this->data['mhs_inbound'] = $this->model_mhs_inbound->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Mhs Inbound Detail');
		$this->render('backend/standart/administrator/mhs_inbound/mhs_inbound_view', $this->data);
	}

	/**
	 * delete Mhs Inbounds
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$mhs_inbound = $this->model_mhs_inbound->find($id);



		return $this->model_mhs_inbound->remove($id);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('mhs_inbound_export');

		$this->model_mhs_inbound->export(
			'mhs_inbound',
			'mhs_inbound',
			$this->model_mhs_inbound->field_search
		);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('mhs_inbound_export');

		$this->model_mhs_inbound->pdf('mhs_inbound', 'mhs_inbound');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('mhs_inbound_export');

		$table = $title = 'mhs_inbound';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_mhs_inbound->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
}


/* End of file mhs_inbound.php */
/* Location: ./application/controllers/administrator/Mhs Inbound.php */