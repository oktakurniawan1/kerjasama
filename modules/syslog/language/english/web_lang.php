<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['syslog'] = 'Catatan sistem';
$lang['id'] = 'Id';
$lang['username'] = 'Username';
$lang['full_name'] = 'Name';
$lang['table_name'] = 'Table Name';
$lang['action'] = 'Action';
$lang['data'] = 'Data';
$lang['waktu'] = 'Waktu';
