<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Syslog Controller
*| --------------------------------------------------------------------------
*| Syslog site
*|
*/
class Syslog extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_syslog');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Syslogs
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('syslog_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if(!is_null($q)) $this->session->set_userdata('syslog_filter',$q);
		if(!is_null($f)) $this->session->set_userdata('syslog_field',$f);
		if(!is_null($this->input->get('reset_filter'))){
			$this->session->unset_userdata('syslog_filter');
			$this->session->unset_userdata('syslog_field');
		}
		$filter = $this->session->userdata('syslog_filter');
		$field = $this->session->userdata('syslog_field');

		$this->data['syslogs'] = $this->model_syslog->get($filter, $field, $this->limit_page, $offset);
		$this->data['syslog_counts'] = $this->model_syslog->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/syslog/index/',
			'total_rows'   => $this->model_syslog->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Tabel Log List');
		$this->render('backend/standart/administrator/syslog/syslog_list', $this->data);
	}
	
	
		/**
	* Update view Syslogs
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('syslog_update');

		$this->data['syslog'] = $this->model_syslog->find($id);

		$this->template->title('Tabel Log Update');
		$this->render('backend/standart/administrator/syslog/syslog_update', $this->data);
	}

	/**
	* Update Syslogs
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('syslog_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('table_name', 'Table Name', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('action', 'Action', 'trim|required');
		$this->form_validation->set_rules('data', 'Data', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'username' => $this->input->post('username'),
				'table_name' => $this->input->post('table_name'),
				'action' => $this->input->post('action'),
				'data' => $this->input->post('data'),
				'waktu' => $this->input->post('waktu'),
			];

			
			$save_syslog = $this->model_syslog->change($id, $save_data);

			if ($save_syslog) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/syslog', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/syslog');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/syslog');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Syslogs
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('syslog_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'syslog'), 'success');
        } else {
            set_message(cclang('error_delete', 'syslog'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Syslogs
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('syslog_view');

		$this->data['syslog'] = $this->model_syslog->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Tabel Log Detail');
		$this->render('backend/standart/administrator/syslog/syslog_view', $this->data);
	}
	
	/**
	* delete Syslogs
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$syslog = $this->model_syslog->find($id);

		
		
		return $this->model_syslog->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('syslog_export');

		$this->model_syslog->export(
			'syslog', 
			'syslog',
			$this->model_syslog->field_search
		);
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('syslog_export');

		$this->model_syslog->pdf('syslog', 'syslog');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('syslog_export');

		$table = $title = 'syslog';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_syslog->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file syslog.php */
/* Location: ./application/controllers/administrator/Syslog.php */