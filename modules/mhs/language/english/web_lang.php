<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['mhs'] = 'Data Mahasiswa Aktif';
$lang['id'] = 'Id';
$lang['nim'] = 'Nim';
$lang['nama'] = 'Nama';
$lang['program_studi'] = 'Kode Prodi';
$lang['prodi'] = 'Program Studi';
$lang['ta'] = 'TA';
$lang['email'] = 'Email';
