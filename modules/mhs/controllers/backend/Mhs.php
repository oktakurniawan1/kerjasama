<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Mhs Controller
 *| --------------------------------------------------------------------------
 *| Mhs site
 *|
 */
class Mhs extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_mhs');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * show all Mhss
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('mhs_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('mhs_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('mhs_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('mhs_filter');
			$this->session->unset_userdata('mhs_field');
		}
		$filter = $this->session->userdata('mhs_filter');
		$field = $this->session->userdata('mhs_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['mhss'] = $this->model_mhs->get($filter, $field, $this->limit_page, $offset);
		$this->data['mhs_counts'] = $this->model_mhs->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/mhs/index/',
			'total_rows'   => $this->model_mhs->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Mhs List');
		$this->render('backend/standart/administrator/mhs/mhs_list', $this->data);
	}



	/**
	 * delete Mhss
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('mhs_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'mhs'), 'success');
		} else {
			set_message(cclang('error_delete', 'mhs'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Mhss
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('mhs_view');

		$this->data['mhs'] = $this->model_mhs->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Mhs Detail');
		$this->render('backend/standart/administrator/mhs/mhs_view', $this->data);
	}

	/**
	 * delete Mhss
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$mhs = $this->model_mhs->find($id);



		return $this->model_mhs->remove($id);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('mhs_export');

		$this->model_mhs->export(
			'mhs',
			'mhs',
			$this->model_mhs->field_search
		);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('mhs_export');

		$this->model_mhs->pdf('mhs', 'mhs');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('mhs_export');

		$table = $title = 'mhs';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_mhs->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
}


/* End of file mhs.php */
/* Location: ./application/controllers/administrator/Mhs.php */