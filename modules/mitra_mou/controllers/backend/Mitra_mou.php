<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Mitra Mou Controller
 *| --------------------------------------------------------------------------
 *| Mitra Mou site
 *|
 */
class Mitra_mou extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_mitra_mou');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * form filter data 
	 */
	public function filter_session_data()
	{
		$fields['scope']  = $this->input->post('scope');
		$fields['negara'] = $this->input->post('negara');
		$fields['jenis']  = $this->input->post('jenis');
		$this->session->set_userdata('filterdata_mitra_mou', $fields);
		$this->session->set_userdata('is_filtered_mitra_mou', TRUE);
		echo json_encode(['url' => base_url('administrator/mitra_mou')]);
	}

	public function clear_filter_session_data()
	{
		$this->session->unset_userdata('filterdata_mitra_mou');
		$this->session->unset_userdata('is_filtered_mitra_mou');
		echo json_encode(['url' => base_url('administrator/mitra_mou')]);
	}

	public function form_filter_data()
	{
		if (!$this->session->has_userdata('filterdata_mitra_mou')) {
			$fields['scope']  = 'all';
			$fields['negara'] = 'all';
			$fields['jenis']  = 'all';
			$this->session->set_userdata('filterdata_mitra_mou', $fields);
		}

		$negara        = array_column(db_get_all_data('negara'), 'nama_negara', 'nama_negara');
		$jenis         = array_column(db_get_all_data('jenis_mitra'), 'jenis_mitra', 'id');
		$negara['all'] = 'ALL';
		$jenis['all']  = 'ALL';

		$data['scope']  = array('DN' => 'DN', 'LN' => 'LN', 'all' => 'ALL');
		$data['negara'] = $negara;
		$data['jenis']  = $jenis;
		$this->load->view('form-filter-data', $data);
	}
	/**
	 * show all Mitra Mous
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('mitra_mou_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('mitra_mou_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('mitra_mou_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('mitra_mou_filter');
			$this->session->unset_userdata('mitra_mou_field');
		}
		$filter = $this->session->userdata('mitra_mou_filter');
		$field = $this->session->userdata('mitra_mou_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['mitra_mous'] = $this->model_mitra_mou->get($filter, $field, $this->limit_page, $offset);
		$this->data['mitra_mou_counts'] = $this->model_mitra_mou->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/mitra_mou/index/',
			'total_rows'   => $this->model_mitra_mou->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Mitra Mou List');
		$this->render('backend/standart/administrator/mitra_mou/mitra_mou_list', $this->data);
	}

	/**
	 * Add new mitra_mous
	 *
	 */
	public function add()
	{
		$this->is_allowed('mitra_mou_add');

		$this->template->title('Mitra Mou New');
		$this->render('backend/standart/administrator/mitra_mou/mitra_mou_add', $this->data);
	}

	/**
	 * Add New Mitra Mous
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('mitra_mou_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('scope', 'Scope', 'trim|required');
		$this->form_validation->set_rules('nama_mitra', 'Nama Mitra', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email');
		$this->form_validation->set_rules('negara', 'Negara', 'trim|required');


		if ($this->form_validation->run()) {

			$save_data = [
				'scope' => $this->input->post('scope'),
				'nama_mitra' => $this->input->post('nama_mitra'),
				'telp' => $this->input->post('telp'),
				'email' => $this->input->post('email'),
				'alamat' => $this->input->post('alamat'),
				'negara' => $this->input->post('negara'),
				'jenis' => $this->input->post('jenis'),
			];


			$save_mitra_mou = $this->model_mitra_mou->store($save_data);


			if ($save_mitra_mou) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_mitra_mou;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/mitra_mou/edit/' . $save_mitra_mou, 'Edit Mitra Mou'),
						anchor('administrator/mitra_mou', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/mitra_mou/edit/' . $save_mitra_mou, 'Edit Mitra Mou')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/mitra_mou');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/mitra_mou');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Mitra Mous
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('mitra_mou_update');

		$this->data['mitra_mou'] = $this->model_mitra_mou->find($id);

		$this->template->title('Mitra Mou Update');
		$this->render('backend/standart/administrator/mitra_mou/mitra_mou_update', $this->data);
	}

	/**
	 * Update Mitra Mous
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('mitra_mou_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('scope', 'Scope', 'trim|required');
		$this->form_validation->set_rules('nama_mitra', 'Nama Mitra', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email');
		$this->form_validation->set_rules('negara', 'Negara', 'trim|required');

		if ($this->form_validation->run()) {

			$save_data = [
				'scope' => $this->input->post('scope'),
				'nama_mitra' => $this->input->post('nama_mitra'),
				'telp' => $this->input->post('telp'),
				'email' => $this->input->post('email'),
				'alamat' => $this->input->post('alamat'),
				'negara' => $this->input->post('negara'),
				'jenis' => $this->input->post('jenis'),
			];


			$save_mitra_mou = $this->model_mitra_mou->change($id, $save_data);

			if ($save_mitra_mou) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/mitra_mou', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/mitra_mou/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/mitra_mou/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Mitra Mous
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('mitra_mou_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'mitra_mou'), 'success');
		} else {
			set_message(cclang('error_delete', 'mitra_mou'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Mitra Mous
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('mitra_mou_view');

		$this->data['mitra_mou'] = $this->model_mitra_mou->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Mitra Mou Detail');
		$this->render('backend/standart/administrator/mitra_mou/mitra_mou_view', $this->data);
	}

	/**
	 * delete Mitra Mous
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$mitra_mou = $this->model_mitra_mou->find($id);



		return $this->model_mitra_mou->remove($id);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
			'scope',
			'nama_mitra',
			'telp',
			'email',
			'alamat',
			'negara',
			'jenis',
		];
		$data = $this->model_mitra_mou->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_mitra_mou->export2($data, 'file', $selected_field);

		/*
		$this->is_allowed('mitra_mou_export');

		$this->model_mitra_mou->export(
			'mitra_mou', 
			'mitra_mou',
			$this->model_mitra_mou->field_search
		);
		*/
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('mitra_mou_export');

		$this->model_mitra_mou->pdf('mitra_mou', 'mitra_mou');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('mitra_mou_export');

		$table = $title = 'mitra_mou';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_mitra_mou->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
}


/* End of file mitra_mou.php */
/* Location: ./application/controllers/administrator/Mitra Mou.php */