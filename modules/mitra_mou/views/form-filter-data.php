<form action="" method="POST" role="form">
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<label for="">scope</label>
				<?= form_dropdown('scope', $scope, fromsess('filterdata_mitra_mou')['scope'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="">negara</label>
				<?= form_dropdown('negara', $negara, fromsess('filterdata_mitra_mou')['negara'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="">jenis</label>
				<?= form_dropdown('jenis', $jenis, fromsess('filterdata_mitra_mou')['jenis'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
			</div>
		</div>
	</div>
</form>

<script>
	$(function() {
		$('select[name=scope],select[name=negara],select[name=jenis]').select2({
			theme: 'classic',
			dropdownParent: $('#filtermitra_mou-dialog'),
		});
		// $('select[name=name4]').select2({
		// 	tags:true
		// })
	})
</script>