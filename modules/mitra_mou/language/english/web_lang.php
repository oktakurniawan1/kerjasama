<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['mitra_mou'] = 'Mitra Mou';
$lang['id'] = 'Id';
$lang['scope'] = 'Scope';
$lang['nama_mitra'] = 'Nama Mitra';
$lang['telp'] = 'Telepon';
$lang['email'] = 'Email';
$lang['alamat'] = 'Alamat';
$lang['negara'] = 'Negara';
$lang['jenis'] = 'Jenis';
