<?php
defined('BASEPATH') or exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Mou_kerjasama extends API
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_mou_kerjasama');
	}

	/**
	 * @api {get} /mou_kerjasama/all Get all mou_kerjasamas.
	 * @apiVersion 0.1.0
	 * @apiName AllMoukerjasama 
	 * @apiGroup mou_kerjasama
	 * @apiHeader {String} X-Api-Key Mou kerjasamas unique access-key.
	 * @apiPermission Mou kerjasama Cant be Accessed permission name : api_mou_kerjasama_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Mou kerjasamas.
	 * @apiParam {String} [Field="All Field"] Optional field of Mou kerjasamas : id, scope, jenis_ks, nama_mou, doc_nomor, doc_tanggal, doc_file, tgl_mulai, tgl_selesai, tahun, nama_mitra, negara, penanggung_jawab, pj_email, pj_tlp, penanggung_jawab_mitra, pj_mitra_email, pj_mitra_tlp, narahubung, deskripsi, foto1, foto2, tgl_input, user_add, is_aktif.
	 * @apiParam {String} [Start=0] Optional start index of Mou kerjasamas.
	 * @apiParam {String} [Limit=10] Optional limit data of Mou kerjasamas.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of mou_kerjasama.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataMou kerjasama Mou kerjasama data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_mou_kerjasama_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'scope', 'jenis_ks', 'nama_mou', 'doc_nomor', 'doc_tanggal', 'doc_file', 'tgl_mulai', 'tgl_selesai', 'tahun', 'nama_mitra', 'negara', 'penanggung_jawab', 'pj_email', 'pj_tlp', 'penanggung_jawab_mitra', 'pj_mitra_email', 'pj_mitra_tlp', 'narahubung', 'deskripsi', 'foto1', 'foto2', 'tgl_input', 'user_add', 'is_aktif'];
		$mou_kerjasamas = $this->model_api_mou_kerjasama->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_mou_kerjasama->count_all($filter, $field);
		$mou_kerjasamas = array_map(function ($row) {

			return $row;
		}, $mou_kerjasamas);

		$data['mou_kerjasama'] = $mou_kerjasamas;

		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Mou kerjasama',
			'data'	 	=> $data,
			'total' 	=> $total,
		], API::HTTP_OK);
	}
	/**
	 * Data rekap MoU untuk display Power BI
	 */
	public function rekap1_get(){
		$this->is_allowed('api_mou_kerjasama_all', false);
		
		$this->requiredInput(['thn_start','thn_end']);

		$rekap = $this->model_api_mou_kerjasama->get_rekap1($this->get('thn_start'),$this->get('thn_end'));
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data MOU',
			'data'	 	=> $rekap
		], API::HTTP_OK);
	}


	/**
	 * @api {get} /mou_kerjasama/detail Detail Mou kerjasama.
	 * @apiVersion 0.1.0
	 * @apiName DetailMou kerjasama
	 * @apiGroup mou_kerjasama
	 * @apiHeader {String} X-Api-Key Mou kerjasamas unique access-key.
	 * @apiPermission Mou kerjasama Cant be Accessed permission name : api_mou_kerjasama_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Mou kerjasamas.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of mou_kerjasama.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError Mou kerjasamaNotFound Mou kerjasama data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_mou_kerjasama_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'scope', 'jenis_ks', 'nama_mou', 'doc_nomor', 'doc_tanggal', 'doc_file', 'tgl_mulai', 'tgl_selesai', 'tahun', 'nama_mitra', 'negara', 'penanggung_jawab', 'pj_email', 'pj_tlp', 'penanggung_jawab_mitra', 'pj_mitra_email', 'pj_mitra_tlp', 'narahubung', 'deskripsi', 'foto1', 'foto2', 'tgl_input', 'user_add', 'is_aktif'];
		$mou_kerjasama = $this->model_api_mou_kerjasama->find($id, $select_field);

		if (!$mou_kerjasama) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Blog not found'
			], API::HTTP_NOT_FOUND);
		}


		$data['mou_kerjasama'] = $mou_kerjasama;
		if ($data['mou_kerjasama']) {

			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Mou kerjasama',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Mou kerjasama not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}


	/**
	 * @api {post} /mou_kerjasama/add Add Mou kerjasama.
	 * @apiVersion 0.1.0
	 * @apiName AddMou kerjasama
	 * @apiGroup mou_kerjasama
	 * @apiHeader {String} X-Api-Key Mou kerjasamas unique access-key.
	 * @apiPermission Mou kerjasama Cant be Accessed permission name : api_mou_kerjasama_add
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_mou_kerjasama_add', false);


		if ($this->form_validation->run()) {

			$save_data = [];

			$save_mou_kerjasama = $this->model_api_mou_kerjasama->store($save_data);

			if ($save_mou_kerjasama) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);
			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /mou_kerjasama/update Update Mou kerjasama.
	 * @apiVersion 0.1.0
	 * @apiName UpdateMou kerjasama
	 * @apiGroup mou_kerjasama
	 * @apiHeader {String} X-Api-Key Mou kerjasamas unique access-key.
	 * @apiPermission Mou kerjasama Cant be Accessed permission name : api_mou_kerjasama_update
	 *
	 * @apiParam {Integer} id Mandatory id of Mou Kerjasama.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_mou_kerjasama_update', false);

		if ($this->form_validation->run()) {

			$save_data = [];

			$save_mou_kerjasama = $this->model_api_mou_kerjasama->change($this->post('id'), $save_data);

			if ($save_mou_kerjasama) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);
			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /mou_kerjasama/delete Delete Mou kerjasama. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteMou kerjasama
	 * @apiGroup mou_kerjasama
	 * @apiHeader {String} X-Api-Key Mou kerjasamas unique access-key.
	 * @apiPermission Mou kerjasama Cant be Accessed permission name : api_mou_kerjasama_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Mou kerjasamas .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_mou_kerjasama_delete', false);

		$mou_kerjasama = $this->model_api_mou_kerjasama->find($this->post('id'));

		if (!$mou_kerjasama) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Mou kerjasama not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_mou_kerjasama->remove($this->post('id'));
		}

		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Mou kerjasama deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Mou kerjasama not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
}

/* End of file Mou kerjasama.php */
/* Location: ./application/controllers/api/Mou kerjasama.php */