<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Mou Kerjasama Controller
 *| --------------------------------------------------------------------------
 *| Mou Kerjasama site
 *|
 */
class Mou_kerjasama extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_mou_kerjasama');
		$this->lang->load('web_lang', $this->current_lang);
		//cek_isi_survey();
	}

	/**
	 * show all Mou Kerjasamas
	 *
	 * @var $offset String
	 */
	public function filter_session_data()
	{
		$fields['jenis_ks'] = $this->input->post('jenis_ks');
		$fields['tahun'] = $this->input->post('tahun');
		$fields['negara'] = $this->input->post('negara');
		$fields['is_aktif'] = $this->input->post('is_aktif');
		$fields['scope'] = $this->input->post('scope');
		$this->session->set_userdata('filterdata_mou', $fields);
		$this->session->set_userdata('is_filtered_mou', $fields);
		echo json_encode(['url' => base_url('administrator/mou_kerjasama')]);
	}
	public function clear_filter_session_data()
	{
		$this->session->unset_userdata('filterdata_mou');
		$this->session->unset_userdata('is_filtered_mou');
		echo json_encode(['url' => base_url('administrator/mou_kerjasama')]);
	}
	public function form_filter_data()
	{
		if (!$this->session->has_userdata('filterdata_mou')) {
			$fields = ['jenis_ks' => 'all', 'tahun' => 'all', 'negara' => 'all', 'is_aktif' => 'all', 'scope' => 'all'];
			$this->session->set_userdata('filterdata_mou', $fields);
		}

		$negara = array_column(db_get_all_data('negara'), 'nama_negara', 'nama_negara');
		$tahun = array_combine(range(2015, date('Y')), range(2015, date('Y')));
		$jenisks = array_column(db_get_all_data('si_jenis_ks'), 'jenis_ks', 'id');
		$negara['all'] = 'ALL';
		$tahun['all'] = 'ALL';
		$jenisks['all'] = 'ALL';
		$data['jenisks'] = $jenisks;
		$data['tahun'] = $tahun;
		$data['negara'] = $negara;
		$data['status'] = array('1' => 'Aktif', '0' => 'Selesai', 'all' => 'ALL');
		$data['scope'] = array('DN' => 'DN', 'LN' => 'LN', 'all' => 'ALL');
		$this->load->view('form-filter-data', $data);
	}

	public function index($offset = 0)
	{
		$this->is_allowed('mou_kerjasama_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('mou_kerjasama_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('mou_kerjasama_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('mou_kerjasama_filter');
			$this->session->unset_userdata('mou_kerjasama_field');
		}
		$filter = $this->session->userdata('mou_kerjasama_filter');
		$field = $this->session->userdata('mou_kerjasama_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['mou_kerjasamas'] = $this->model_mou_kerjasama->get($filter, $field, $this->limit_page, $offset);
		$this->data['mou_kerjasama_counts'] = $this->model_mou_kerjasama->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/mou_kerjasama/index/',
			'total_rows'   => $this->model_mou_kerjasama->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Mou Kerjasama List');
		$this->render('backend/standart/administrator/mou_kerjasama/mou_kerjasama_list', $this->data);
	}

	/**
	 * Add new mou_kerjasamas
	 *
	 */
	public function add()
	{
		$this->is_allowed('mou_kerjasama_add');

		$this->template->title('Mou Kerjasama New');
		$this->render('backend/standart/administrator/mou_kerjasama/mou_kerjasama_add', $this->data);
	}

	/**
	 * Add New Mou Kerjasamas
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('mou_kerjasama_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('scope', 'Scope', 'trim|required');
		$this->form_validation->set_rules('jenis_ks', 'Jenis Kerjasama', 'trim|required|max_length[4]');
		$this->form_validation->set_rules('doc_nomor', 'Nomor Dokumen', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('doc_tanggal', 'Tanggal Dokumen', 'trim|required');
		$this->form_validation->set_rules('mou_kerjasama_doc_file_name', 'File Dokumen', 'trim|required');
		$this->form_validation->set_rules('negara', 'Negara', 'trim|required');
		$this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'trim|required');
		$this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'trim|required');
		$this->form_validation->set_rules('nama_mitra', 'Nama Mitra', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('penanggung_jawab', 'Penanggung Jawab', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('penanggung_jawab_mitra', 'Mitra Penanggung Jawab', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
		$this->form_validation->set_rules('mou_kerjasama_foto1_name', 'Bukti Foto 1', 'trim');
		$this->form_validation->set_rules('mou_kerjasama_foto2_name', 'Bukti Foto 2', 'trim');


		if ($this->form_validation->run()) {
			$mou_kerjasama_doc_file_uuid = $this->input->post('mou_kerjasama_doc_file_uuid');
			$mou_kerjasama_doc_file_name = $this->input->post('mou_kerjasama_doc_file_name');
			$mou_kerjasama_foto1_uuid = $this->input->post('mou_kerjasama_foto1_uuid');
			$mou_kerjasama_foto1_name = $this->input->post('mou_kerjasama_foto1_name');
			$mou_kerjasama_foto2_uuid = $this->input->post('mou_kerjasama_foto2_uuid');
			$mou_kerjasama_foto2_name = $this->input->post('mou_kerjasama_foto2_name');

			$save_data = [
				'scope' => $this->input->post('scope'),
				'jenis_ks' => $this->input->post('jenis_ks'),
				'doc_nomor' => $this->input->post('doc_nomor'),
				'doc_tanggal' => $this->input->post('doc_tanggal'),
				'negara' => $this->input->post('negara'),
				'tgl_mulai' => $this->input->post('tgl_mulai'),
				'tgl_selesai' => $this->input->post('tgl_selesai'),
				'id_mitra' => $this->input->post('id_mitra'),
				'nama_mitra' => $this->input->post('nama_mitra'),
				'penanggung_jawab' => $this->input->post('penanggung_jawab'),
				'pj_email' => $this->input->post('pj_email'),
				'pj_tlp' => $this->input->post('pj_tlp'),
				'penanggung_jawab_mitra' => $this->input->post('penanggung_jawab_mitra'),
				'pj_mitra_email' => $this->input->post('pj_mitra_email'),
				'pj_mitra_tlp' => $this->input->post('pj_mitra_tlp'),
				'narahubung' => $this->input->post('narahubung'),
				'deskripsi' => $this->input->post('deskripsi'),
				'user_add' => get_user_data('username'),
				'tgl_input' => date('Y-m-d')
			];

			if (!is_dir(FCPATH . '/uploads/mou_kerjasama/')) {
				mkdir(FCPATH . '/uploads/mou_kerjasama/');
			}

			if (!empty($mou_kerjasama_doc_file_name)) {
				$mou_kerjasama_doc_file_name_copy = date('YmdHis') . '-' . $mou_kerjasama_doc_file_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mou_kerjasama_doc_file_uuid . '/' . $mou_kerjasama_doc_file_name,
					FCPATH . 'uploads/mou_kerjasama/' . $mou_kerjasama_doc_file_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mou_kerjasama/' . $mou_kerjasama_doc_file_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['doc_file'] = $mou_kerjasama_doc_file_name_copy;
			}

			if (!empty($mou_kerjasama_foto1_name)) {
				$mou_kerjasama_foto1_name_copy = date('YmdHis') . '-' . $mou_kerjasama_foto1_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mou_kerjasama_foto1_uuid . '/' . $mou_kerjasama_foto1_name,
					FCPATH . 'uploads/mou_kerjasama/' . $mou_kerjasama_foto1_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mou_kerjasama/' . $mou_kerjasama_foto1_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['foto1'] = $mou_kerjasama_foto1_name_copy;
			}

			if (!empty($mou_kerjasama_foto2_name)) {
				$mou_kerjasama_foto2_name_copy = date('YmdHis') . '-' . $mou_kerjasama_foto2_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mou_kerjasama_foto2_uuid . '/' . $mou_kerjasama_foto2_name,
					FCPATH . 'uploads/mou_kerjasama/' . $mou_kerjasama_foto2_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mou_kerjasama/' . $mou_kerjasama_foto2_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['foto2'] = $mou_kerjasama_foto2_name_copy;
			}


			$save_mou_kerjasama = $this->model_mou_kerjasama->store($save_data);


			if ($save_mou_kerjasama) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_mou_kerjasama;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/mou_kerjasama/edit/' . $save_mou_kerjasama, 'Edit Mou Kerjasama'),
						anchor('administrator/mou_kerjasama', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/mou_kerjasama/edit/' . $save_mou_kerjasama, 'Edit Mou Kerjasama')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/mou_kerjasama');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/mou_kerjasama');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Mou Kerjasamas
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('mou_kerjasama_update');

		$this->data['mou_kerjasama'] = $this->model_mou_kerjasama->find($id);

		$this->template->title('Mou Kerjasama Update');
		$this->render('backend/standart/administrator/mou_kerjasama/mou_kerjasama_update', $this->data);
	}

	/**
	 * Update Mou Kerjasamas
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('mou_kerjasama_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('scope', 'Scope', 'trim|required');
		$this->form_validation->set_rules('jenis_ks', 'Jenis Kerjasama', 'trim|required|max_length[4]');
		$this->form_validation->set_rules('doc_nomor', 'Nomor Dokumen', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('doc_tanggal', 'Tanggal Dokumen', 'trim|required');
		$this->form_validation->set_rules('mou_kerjasama_doc_file_name', 'File Dokumen', 'trim|required');
		$this->form_validation->set_rules('negara', 'Negara', 'trim|required');
		$this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'trim|required');
		$this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'trim|required');
		$this->form_validation->set_rules('nama_mitra', 'Nama Mitra', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('penanggung_jawab', 'Penanggung Jawab', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('penanggung_jawab_mitra', 'Penanggung Jawab Mitra', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
		$this->form_validation->set_rules('mou_kerjasama_foto1_name', 'Bukti Foto 1', 'trim');
		$this->form_validation->set_rules('mou_kerjasama_foto2_name', 'Bukti Foto 2', 'trim');

		if ($this->form_validation->run()) {
			$mou_kerjasama_doc_file_uuid = $this->input->post('mou_kerjasama_doc_file_uuid');
			$mou_kerjasama_doc_file_name = $this->input->post('mou_kerjasama_doc_file_name');
			$mou_kerjasama_foto1_uuid = $this->input->post('mou_kerjasama_foto1_uuid');
			$mou_kerjasama_foto1_name = $this->input->post('mou_kerjasama_foto1_name');
			$mou_kerjasama_foto2_uuid = $this->input->post('mou_kerjasama_foto2_uuid');
			$mou_kerjasama_foto2_name = $this->input->post('mou_kerjasama_foto2_name');

			$save_data = [
				'scope' => $this->input->post('scope'),
				'jenis_ks' => $this->input->post('jenis_ks'),
				'doc_nomor' => $this->input->post('doc_nomor'),
				'doc_tanggal' => $this->input->post('doc_tanggal'),
				'negara' => $this->input->post('negara'),
				'tgl_mulai' => $this->input->post('tgl_mulai'),
				'tgl_selesai' => $this->input->post('tgl_selesai'),
				'id_mitra' => $this->input->post('id_mitra'),
				'nama_mitra' => $this->input->post('nama_mitra'),
				'penanggung_jawab' => $this->input->post('penanggung_jawab'),
				'pj_email' => $this->input->post('pj_email'),
				'pj_tlp' => $this->input->post('pj_tlp'),
				'penanggung_jawab_mitra' => $this->input->post('penanggung_jawab_mitra'),
				'pj_mitra_email' => $this->input->post('pj_mitra_email'),
				'pj_mitra_tlp' => $this->input->post('pj_mitra_tlp'),
				'narahubung' => $this->input->post('narahubung'),
				'deskripsi' => $this->input->post('deskripsi'),
			];

			if (!is_dir(FCPATH . '/uploads/mou_kerjasama/')) {
				mkdir(FCPATH . '/uploads/mou_kerjasama/');
			}

			if (!empty($mou_kerjasama_doc_file_uuid)) {
				$mou_kerjasama_doc_file_name_copy = date('YmdHis') . '-' . $mou_kerjasama_doc_file_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mou_kerjasama_doc_file_uuid . '/' . $mou_kerjasama_doc_file_name,
					FCPATH . 'uploads/mou_kerjasama/' . $mou_kerjasama_doc_file_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mou_kerjasama/' . $mou_kerjasama_doc_file_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['doc_file'] = $mou_kerjasama_doc_file_name_copy;
			}

			if (!empty($mou_kerjasama_foto1_uuid)) {
				$mou_kerjasama_foto1_name_copy = date('YmdHis') . '-' . $mou_kerjasama_foto1_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mou_kerjasama_foto1_uuid . '/' . $mou_kerjasama_foto1_name,
					FCPATH . 'uploads/mou_kerjasama/' . $mou_kerjasama_foto1_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mou_kerjasama/' . $mou_kerjasama_foto1_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['foto1'] = $mou_kerjasama_foto1_name_copy;
			}

			if (!empty($mou_kerjasama_foto2_uuid)) {
				$mou_kerjasama_foto2_name_copy = date('YmdHis') . '-' . $mou_kerjasama_foto2_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mou_kerjasama_foto2_uuid . '/' . $mou_kerjasama_foto2_name,
					FCPATH . 'uploads/mou_kerjasama/' . $mou_kerjasama_foto2_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mou_kerjasama/' . $mou_kerjasama_foto2_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['foto2'] = $mou_kerjasama_foto2_name_copy;
			}


			$save_mou_kerjasama = $this->model_mou_kerjasama->change($id, $save_data);

			if ($save_mou_kerjasama) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/mou_kerjasama', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/mou_kerjasama/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/mou_kerjasama/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Mou Kerjasamas
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('mou_kerjasama_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'mou_kerjasama'), 'success');
		} else {
			set_message(cclang('error_delete', 'mou_kerjasama'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Mou Kerjasamas
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('mou_kerjasama_view');

		$this->data['mou_kerjasama'] = $this->model_mou_kerjasama->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Mou Kerjasama Detail');
		$this->render('backend/standart/administrator/mou_kerjasama/mou_kerjasama_view', $this->data);
	}

	/**
	 * delete Mou Kerjasamas
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$mou_kerjasama = $this->model_mou_kerjasama->find($id);

		if (!empty($mou_kerjasama->doc_file)) {
			$path = FCPATH . '/uploads/mou_kerjasama/' . $mou_kerjasama->doc_file;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($mou_kerjasama->foto1)) {
			$path = FCPATH . '/uploads/mou_kerjasama/' . $mou_kerjasama->foto1;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($mou_kerjasama->foto2)) {
			$path = FCPATH . '/uploads/mou_kerjasama/' . $mou_kerjasama->foto2;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}


		return $this->model_mou_kerjasama->remove($id);
	}

	/**
	 * Upload Image Mou Kerjasama	* 
	 * @return JSON
	 */
	public function upload_doc_file_file()
	{
		if (!$this->is_allowed('mou_kerjasama_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mou_kerjasama',
			'allowed_types' => 'pdf|jpg|jpeg',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Mou Kerjasama	* 
	 * @return JSON
	 */
	public function delete_doc_file_file($uuid)
	{
		if (!$this->is_allowed('mou_kerjasama_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'doc_file',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mou_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mou_kerjasama/'
		]);
	}

	/**
	 * Get Image Mou Kerjasama	* 
	 * @return JSON
	 */
	public function get_doc_file_file($id)
	{
		if (!$this->is_allowed('mou_kerjasama_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mou_kerjasama = $this->model_mou_kerjasama->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'doc_file',
			'table_name'        => 'mou_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mou_kerjasama/',
			'delete_endpoint'   => 'administrator/mou_kerjasama/delete_doc_file_file'
		]);
	}

	/**
	 * Upload Image Mou Kerjasama	* 
	 * @return JSON
	 */
	public function upload_foto1_file()
	{
		if (!$this->is_allowed('mou_kerjasama_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mou_kerjasama',
			'allowed_types' => 'jpg|jpeg',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Mou Kerjasama	* 
	 * @return JSON
	 */
	public function delete_foto1_file($uuid)
	{
		if (!$this->is_allowed('mou_kerjasama_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'foto1',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mou_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mou_kerjasama/'
		]);
	}

	/**
	 * Get Image Mou Kerjasama	* 
	 * @return JSON
	 */
	public function get_foto1_file($id)
	{
		if (!$this->is_allowed('mou_kerjasama_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mou_kerjasama = $this->model_mou_kerjasama->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'foto1',
			'table_name'        => 'mou_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mou_kerjasama/',
			'delete_endpoint'   => 'administrator/mou_kerjasama/delete_foto1_file'
		]);
	}

	/**
	 * Upload Image Mou Kerjasama	* 
	 * @return JSON
	 */
	public function upload_foto2_file()
	{
		if (!$this->is_allowed('mou_kerjasama_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mou_kerjasama',
			'allowed_types' => 'jpg|jpeg',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Mou Kerjasama	* 
	 * @return JSON
	 */
	public function delete_foto2_file($uuid)
	{
		if (!$this->is_allowed('mou_kerjasama_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'foto2',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mou_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mou_kerjasama/'
		]);
	}

	/**
	 * Get Image Mou Kerjasama	* 
	 * @return JSON
	 */
	public function get_foto2_file($id)
	{
		if (!$this->is_allowed('mou_kerjasama_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mou_kerjasama = $this->model_mou_kerjasama->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'foto2',
			'table_name'        => 'mou_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mou_kerjasama/',
			'delete_endpoint'   => 'administrator/mou_kerjasama/delete_foto2_file'
		]);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('mou_kerjasama_export');

		$filter = $this->session->userdata('mou_kerjasama_filter');
		$field = $this->session->userdata('mou_kerjasama_field');
		$offset = $this->session->userdata('_page');

		$selected_field = [
			'scope',
			'si_jenis_ks_jenis_ks',
			'doc_nomor',
			'doc_tanggal',
			'tgl_mulai',
			'tgl_selesai',
			'tahun',
			'nama_mitra',
			'negara',
			'penanggung_jawab',
			'pj_email',
			'pj_tlp',
			'penanggung_jawab_mitra',
			'pj_mitra_email',
			'pj_mitra_tlp',
			'narahubung',
			'is_aktif',
			'doc_mou',
			'status',
			'sisa_bulan'
		];
		$data = $this->model_mou_kerjasama->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_mou_kerjasama->export2($data, 'file', $selected_field);

		// $this->model_mou_kerjasama->export(
		// 	'mou_kerjasama',
		// 	'mou_kerjasama',
		// 	$this->model_mou_kerjasama->field_search
		// );
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('mou_kerjasama_export');

		$this->model_mou_kerjasama->pdf('mou_kerjasama', 'mou_kerjasama');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('mou_kerjasama_export');

		$table = $title = 'mou_kerjasama';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_mou_kerjasama->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
	public function get_doskar()
	{
		$data['pegawai'] = $this->model_mou_kerjasama->get_karyawan();
		$this->load->view('_cari_pegawai', $data);
	}
	public function get_option_mou()
	{
		echo $this->model_mou_kerjasama->get_option_mou($this->input->get());
	}
}


/* End of file mou_kerjasama.php */
/* Location: ./application/controllers/administrator/Mou Kerjasama.php */