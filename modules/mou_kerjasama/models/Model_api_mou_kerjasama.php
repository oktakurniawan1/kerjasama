<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_api_mou_kerjasama extends MY_Model
{

	private $primary_key 	= 'id';
	private $table_name 	= 'mou_kerjasama';
	private $field_search 	= ['id', 'scope', 'jenis_ks', 'nama_mou', 'doc_nomor', 'doc_tanggal', 'doc_file', 'tgl_mulai', 'tgl_selesai', 'tahun', 'nama_mitra', 'negara', 'penanggung_jawab', 'pj_email', 'pj_tlp', 'penanggung_jawab_mitra', 'pj_mitra_email', 'pj_mitra_tlp', 'narahubung', 'deskripsi', 'foto1', 'foto2', 'tgl_input', 'user_add', 'is_aktif'];

	public function __construct()
	{
		$config = array(
			'primary_key' 	=> $this->primary_key,
			'table_name' 	=> $this->table_name,
			'field_search' 	=> $this->field_search,
		);

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			$where .= "(" . $field . " LIKE '%" . $q . "%' )";
		}

		$this->db->where($where);
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			if (in_array($field, $select_field)) {
				$where .= "(" . $field . " LIKE '%" . $q . "%' )";
			}
		}

		if (is_array($select_field) and count($select_field)) {
			$this->db->select($select_field);
		}

		if ($where) {
			$this->db->where($where);
		}
		$this->db->limit($limit, $offset);
		$this->db->order_by($this->primary_key, "DESC");
		$query = $this->db->get($this->table_name);

		return $query->result();
	}
	public function get_rekap1($thn_start = 2020, $thn_end = 2020)
	{
		$sql = "SELECT
					u.kd_prodi,
					rp.nama_prodi,
					u.nama_mitra,
					u.scope,
					if(u.mou_ks=1,'N','Y') as with_mou,
					jk.jenis_ks,
					doc_tipe,
					u.negara,
					validasi,
					IF(is_aktif=1,'Aktif','Selesai') is_aktif
				FROM
					si_kerjasama u 
				LEFT JOIN si_jenis_ks AS jk ON (u.jenis_ks = jk.id)
				INNER JOIN ref_prodi rp on (u.kd_prodi = rp.kode AND rp.kode_fak != 0 and rp.tingkat = 3 AND (u.tahun BETWEEN $thn_start AND $thn_end))
				ORDER BY kd_prodi";
		return $this->db->query($sql)->result();
	}
}

/* End of file Model_mou_kerjasama.php */
/* Location: ./application/models/Model_mou_kerjasama.php */