<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_mou_kerjasama extends MY_Model
{

	private $primary_key    = 'id';
	private $table_name     = 'mou_kerjasama';
	public $field_search   = ['scope', 'jenis_ks', 'doc_nomor', 'doc_tanggal', 'negara', 'tgl_mulai', 'tgl_selesai', 'nama_mitra', 'penanggung_jawab', 'penanggung_jawab_mitra', 'tgl_input'];

	public function __construct()
	{
		$config = array(
			'primary_key'   => $this->primary_key,
			'table_name'    => $this->table_name,
			'field_search'  => $this->field_search,
		);

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "mou_kerjasama." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'jenis_ks') $where .= "OR si_jenis_ks.jenis_ks" . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "mou_kerjasama." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'jenis_ks') $where .= "OR si_jenis_ks.jenis_ks" . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			if ($field == 'jenis_ks') $where .= "(" . "si_jenis_ks.jenis_ks LIKE '%" . $q . "%' )";
			elseif ($field == 'sisa_bulan') $where = NULL;
			elseif ($field == 'status') $where = NULL;
			else $where .= "(" . "mou_kerjasama." . $field . " LIKE '%" . $q . "%' )";
		}
		if ($field == 'sisa_bulan') $this->db->having('sisa_bulan', $q);
		if ($field == 'status') $this->db->having('status', $q);

		$this->join_avaiable()->filter_avaiable();
		if (!is_null($where)) $this->db->where($where);

		/**form filter data */
		if ($this->session->has_userdata('filterdata_mou')) {
			foreach (fromsess('filterdata_mou') as $key => $row) {
				if ($row != 'all') $this->db->having($key, $row);
			}
		}

		$this->db->order_by('id'); //fix bug when use HAVING sql with variable(@) field CI, must using  any order_by
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [], $result = TRUE)
	{
		$iterasi = 1;
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "mou_kerjasama." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'jenis_ks') $where .= "OR si_jenis_ks.jenis_ks" . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "mou_kerjasama." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'jenis_ks') $where .= "OR si_jenis_ks.jenis_ks" . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			if ($field == 'jenis_ks') $where .= "(" . "si_jenis_ks.jenis_ks LIKE '%" . $q . "%' )";
			elseif ($field == 'sisa_bulan') $where = NULL;
			elseif ($field == 'status') $where = NULL;
			else $where .= "(" . "mou_kerjasama." . $field . " LIKE '%" . $q . "%' )";
		}
		if ($field == 'sisa_bulan') $this->db->having('sisa_bulan', $q);
		if ($field == 'status') $this->db->having('status', $q);

		if (is_array($select_field) and count($select_field)) {
			$this->db->select($select_field);
		}

		$this->join_avaiable()->filter_avaiable();
		if (!is_null($where)) $this->db->where($where);

		/**form filter data */
		if ($this->session->has_userdata('filterdata_mou')) {
			foreach (fromsess('filterdata_mou') as $key => $row) {
				if ($row != 'all') $this->db->having($key, $row);
			}
		}

		$this->db->limit($limit, $offset);

		$this->sortable();

		$query = $this->db->get($this->table_name);

		if ($result)
			return $query->result();
		else return $query;
	}

	public function join_avaiable()
	{
		$this->db->join('si_jenis_ks', 'si_jenis_ks.id = mou_kerjasama.jenis_ks', 'LEFT');
		$this->db->select('mou_kerjasama.*,si_jenis_ks.jenis_ks as si_jenis_ks_jenis_ks');
		$this->db->select('CONCAT("https://simkerjasama.unnes.ac.id/administrator/file/download/mou_kerjasama/",mou_kerjasama.doc_file) AS doc_mou');
		$this->db->select('IF(DATEDIFF(tgl_selesai,CURDATE())>0,"aktif","selesai") as status');
		$this->db->select("@sisa:=PERIOD_DIFF(EXTRACT(YEAR_MONTH FROM tgl_selesai),EXTRACT(YEAR_MONTH FROM CURDATE())) as _sb,if(@sisa<0,0,@sisa) as sisa_bulan");

		return $this;
	}

	public function filter_avaiable()
	{

		// if (!$this->aauth->is_admin()) {
		//     }
		if (!is_groups_in(['Admin', 'Kerjasama'])) {
			$this->db->where('mou_kerjasama.id !=', 1);
		}
		return $this;
	}
	public function get_karyawan()
	{
		return $this->db->select('nip_baru,gelar_depan,nama,gelar_belakang,dos_kar,nama_unit,telp,email')->get('karyawan')->result();
	}

	public function get_option_mou($where)
	{
		foreach ($where as $key => $val) {
			$this->db->where($key, $val);
		}
		$data = $this->db->select('id,concat(nama_mitra," - ",doc_nomor) as name', false)->where('tgl_selesai > CURDATE()', null, false)->get('mou_kerjasama')->result();
		$option = build_option($data, 'id', 'nama_mou');
		$option .= "<option value='1'>Pengajuan Baru</option>";
		return $option;
	}
}

/* End of file Model_mou_kerjasama.php */
/* Location: ./application/models/Model_mou_kerjasama.php */