<script defer src="<?= BASE_ASSET; ?>/js/custom.js"></script>
<div class="row">
	<div class="form-group ">
		<label for="" class="col-sm-2 control-label">Cari Pegawai</label>
		<div class="col-sm-10">
			<select class="form-control chosen chosen-select-deselect" name="pegawai" id="pegawai" data-placeholder="cari nip,nama pegawai...">
				<option value=""></option>
				<?php foreach ($pegawai as $row) :
					$gd = ($row->gelar_depan == TRUE ? $row->gelar_depan : '');
					$gb = ($row->gelar_belakang == TRUE ? ' ' . $row->gelar_belakang : '');
					$data = "data-nip='{$row->nip_baru}' data-nama='{$gd}{$row->nama}{$gb}' data-tlp='{$row->telp}' data-email='{$row->email}'"; ?>
					<option <?= $data ?> value="<?= $row->nip_baru ?>"><?= $row->dos_kar . ' - ' . $row->nip_baru . ' - ' . $row->gelar_depan . ' ' . $row->nama . ' ' . $row->gelar_belakang ?></option>
				<?php endforeach; ?>
			</select>
			<small class="info help-block">
				cari nama pegawai di simpeg
			</small>
		</div>
	</div>
</div>