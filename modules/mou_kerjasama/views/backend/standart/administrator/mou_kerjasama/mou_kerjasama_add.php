<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		MoU Kerjasama <small><?= cclang('new', ['MoU Kerjasama']); ?> </small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/mou_kerjasama'); ?>">MoU Kerjasama</a></li>
		<li class="active"><?= cclang('new'); ?></li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">MoU Kerjasama</h3>
							<h5 class="widget-user-desc"><?= cclang('new', ['MoU Kerjasama']); ?></h5>
							<hr>
						</div>
						<?= form_open('', [
							'name'    => 'form_mou_kerjasama',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_mou_kerjasama',
							'enctype' => 'multipart/form-data',
							'method'  => 'POST'
						]); ?>

						<div class="form-group ">
							<label for="scope" class="col-sm-2 control-label">DN/LN
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select" name="scope" id="scope" data-placeholder="Select Scope">
									<option value=""></option>
									<option value="DN">Dalam Negeri</option>
									<option value="LN">Luar Negeri</option>
								</select>
								<small class="info help-block">
									DN:Dalam negeri LN:Luar negeri
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="jenis_ks" class="col-sm-2 control-label">Jenis Kerjasama
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="jenis_ks" id="jenis_ks" data-placeholder="Select Jenis Kerjasama">
									<option value=""></option>
									<?php foreach (db_get_all_data('si_jenis_ks') as $row) : ?>
										<option value="<?= $row->id ?>"><?= $row->jenis_ks; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Input Jenis Ks</b> Max Length : 4.</small>
							</div>
						</div>

						<input type="hidden" name="id_mitra" value="<?= set_value('id_mitra'); ?>">
						<div class="form-group ">
							<label for="nama_mitra" class="col-sm-2 control-label">Nama Mitra
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
							<div class="input-group">
								<span class="input-group-btn">
									<button class="btn btn-default" type="button" onclick="cari_mitra()">Cari mitra</button>
								</span>
								<input type="text" style="margin-left: 5px;" class="form-control" name="nama_mitra" id="nama_mitra" placeholder="Nama Mitra" value="<?= set_value('nama_mitra'); ?>" readonly>
							</div>
							<small class="info help-block">
									<b>Input Nama Mitra</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="doc_nomor" class="col-sm-2 control-label">Nomor Dokumen
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="doc_nomor" id="doc_nomor" placeholder="Nomor Dokumen" value="<?= set_value('doc_nomor'); ?>">
								<small class="info help-block">
									<b>Input Doc Nomor</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="doc_tanggal" class="col-sm-2 control-label">Tanggal Dokumen
								<i class="required">*</i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-8">
									<input type="text" class="form-control pull-right datepicker" name="doc_tanggal" placeholder="Tanggal Dokumen" id="doc_tanggal">
								</div>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="doc_file" class="col-sm-2 control-label">File Dokumen
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="mou_kerjasama_doc_file_galery"></div>
								<input class="data_file" name="mou_kerjasama_doc_file_uuid" id="mou_kerjasama_doc_file_uuid" type="hidden" value="<?= set_value('mou_kerjasama_doc_file_uuid'); ?>">
								<input class="data_file" name="mou_kerjasama_doc_file_name" id="mou_kerjasama_doc_file_name" type="hidden" value="<?= set_value('mou_kerjasama_doc_file_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF,JPG,JPEG, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="negara" class="col-sm-2 control-label">Negara
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="negara" id="negara" data-placeholder="Select negara">
									<option value=""></option>
									<?php foreach (db_get_all_data('negara') as $row) : ?>
										<option value="<?= $row->nama_negara ?>"><?= $row->nama_negara; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Pilih negara</b></small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_mulai" class="col-sm-2 control-label">Tanggal Mulai
								<i class="required">*</i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-8">
									<input type="text" class="form-control pull-right datepicker" name="tgl_mulai" placeholder="Tanggal Mulai" id="tgl_mulai">
								</div>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_selesai" class="col-sm-2 control-label">Tanggal Selesai
								<i class="required">*</i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-8">
									<input type="text" class="form-control pull-right datepicker" name="tgl_selesai" placeholder="Tanggal Selesai" id="tgl_selesai">
								</div>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="penanggung_jawab" class="col-sm-2 control-label">Penanggung Jawab
								<i class="required">*</i>
							</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="penanggung_jawab" id="penanggung_jawab" placeholder="Penanggung jawab" value="<?= set_value('penanggung_jawab'); ?>">
								<small class="info help-block">
									<button class="btn btn-primary btn-sm float-xl-left" type="button" onclick="cari_pegawai()">Cari di Simpeg</button>
								</small>
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="pj_email" id="pj_email" placeholder="Email" value="<?= set_value('pj_email'); ?>">
								<small class="info help-block"><b>Email</b></small>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control" name="pj_tlp" id="pj_tlp" placeholder="Telepon" value="<?= set_value('pj_tlp'); ?>">
								<small class="info help-block"><b>Telp</b></small>
							</div>
						</div>

						<div class="form-group ">
							<label for="narahubung" class="col-sm-2 control-label">Narahubung
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="narahubung" id="narahubung" placeholder="Narahubung" value="<?= set_value('narahubung'); ?>">
								<small class="info help-block">
									<b>Input Narahubung</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="penanggung_jawab" class="col-sm-2 control-label">Mitra Penanggung Jawab
								<i class="required">*</i>
							</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="penanggung_jawab_mitra" id="penanggung_jawab_mitra" placeholder="Nama mitra penanggung jawab" value="<?= set_value('penanggung_jawab_mitra'); ?>">
								<small class="info help-block">
									<b>Nama</b>
								</small>
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="pj_mitra_email" id="pj_mitra_email" placeholder="Email mitra" value="<?= set_value('pj_mitra_email'); ?>">
								<small class="info help-block"><b>Email</b></small>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control" name="pj_mitra_tlp" id="pj_mitra_tlp" placeholder="Telepon mitra" value="<?= set_value('pj_mitra_tlp'); ?>">
								<small class="info help-block"><b>Telp</b></small>
							</div>
						</div>

						<div class="form-group ">
							<label for="deskripsi" class="col-sm-2 control-label">Deskripsi
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<textarea id="deskripsi" name="deskripsi" rows="5" cols="80"><?= set_value('Deskripsi'); ?></textarea>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="foto1" class="col-sm-2 control-label">Bukti Foto 1
							</label>
							<div class="col-sm-8">
								<div id="mou_kerjasama_foto1_galery"></div>
								<input class="data_file" name="mou_kerjasama_foto1_uuid" id="mou_kerjasama_foto1_uuid" type="hidden" value="<?= set_value('mou_kerjasama_foto1_uuid'); ?>">
								<input class="data_file" name="mou_kerjasama_foto1_name" id="mou_kerjasama_foto1_name" type="hidden" value="<?= set_value('mou_kerjasama_foto1_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> JPG,JPEG, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="foto2" class="col-sm-2 control-label">Bukti Foto 2
							</label>
							<div class="col-sm-8">
								<div id="mou_kerjasama_foto2_galery"></div>
								<input class="data_file" name="mou_kerjasama_foto2_uuid" id="mou_kerjasama_foto2_uuid" type="hidden" value="<?= set_value('mou_kerjasama_foto2_uuid'); ?>">
								<input class="data_file" name="mou_kerjasama_foto2_name" id="mou_kerjasama_foto2_name" type="hidden" value="<?= set_value('mou_kerjasama_foto2_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> JPG,JPEG, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>



						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<script src="<?= BASE_ASSET; ?>ckeditor/ckeditor.js"></script>
<!-- Page script -->
<script>
	$(document).ready(function() {

		CKEDITOR.replace('deskripsi');
		var deskripsi = CKEDITOR.instances.deskripsi;

		$('#btn_cancel').click(function() {
			swal({
					title: "<?= cclang('are_you_sure'); ?>",
					text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/mou_kerjasama';
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();
			$('#deskripsi').val(deskripsi.getData());

			var form_mou_kerjasama = $('#form_mou_kerjasama');
			var data_post = form_mou_kerjasama.serializeArray();
			var save_type = $(this).attr('data-stype');

			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: BASE_URL + '/administrator/mou_kerjasama/add_save',
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('.steps li').removeClass('error');
					$('form').find('.error-input').remove();
					if (res.success) {
						var id_doc_file = $('#mou_kerjasama_doc_file_galery').find('li').attr('qq-file-id');
						var id_foto1 = $('#mou_kerjasama_foto1_galery').find('li').attr('qq-file-id');
						var id_foto2 = $('#mou_kerjasama_foto2_galery').find('li').attr('qq-file-id');

						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						resetForm();
						if (typeof id_doc_file !== 'undefined') {
							$('#mou_kerjasama_doc_file_galery').fineUploader('deleteFile', id_doc_file);
						}
						if (typeof id_foto1 !== 'undefined') {
							$('#mou_kerjasama_foto1_galery').fineUploader('deleteFile', id_foto1);
						}
						if (typeof id_foto2 !== 'undefined') {
							$('#mou_kerjasama_foto2_galery').fineUploader('deleteFile', id_foto2);
						}
						$('.chosen option').prop('selected', false).trigger('chosen:updated');
						deskripsi.setData('');

					} else {
						if (res.errors) {

							$.each(res.errors, function(index, val) {
								$('form #' + index).parents('.form-group').addClass('has-error');
								$('form #' + index).parents('.form-group').find('small').prepend(`
                      <div class="error-input">` + val + `</div>
                      `);
							});
							$('.steps li').removeClass('error');
							$('.content section').each(function(index, el) {
								if ($(this).find('.has-error').length) {
									$('.steps li:eq(' + index + ')').addClass('error').find('a').trigger('click');
								}
							});
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/

		var params = {};
		params[csrf] = token;

		$('#mou_kerjasama_doc_file_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mou_kerjasama/upload_doc_file_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mou_kerjasama/delete_doc_file_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf", "jpg", "jpeg"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mou_kerjasama_doc_file_galery').fineUploader('getUuid', id);
						$('#mou_kerjasama_doc_file_uuid').val(uuid);
						$('#mou_kerjasama_doc_file_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mou_kerjasama_doc_file_uuid').val();
					$.get(BASE_URL + '/administrator/mou_kerjasama/delete_doc_file_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mou_kerjasama_doc_file_uuid').val('');
						$('#mou_kerjasama_doc_file_name').val('');
					}
				}
			}
		}); /*end doc_file galery*/
		var params = {};
		params[csrf] = token;

		$('#mou_kerjasama_foto1_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mou_kerjasama/upload_foto1_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mou_kerjasama/delete_foto1_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["jpg", "jpeg"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mou_kerjasama_foto1_galery').fineUploader('getUuid', id);
						$('#mou_kerjasama_foto1_uuid').val(uuid);
						$('#mou_kerjasama_foto1_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mou_kerjasama_foto1_uuid').val();
					$.get(BASE_URL + '/administrator/mou_kerjasama/delete_foto1_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mou_kerjasama_foto1_uuid').val('');
						$('#mou_kerjasama_foto1_name').val('');
					}
				}
			}
		}); /*end foto1 galery*/
		var params = {};
		params[csrf] = token;

		$('#mou_kerjasama_foto2_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mou_kerjasama/upload_foto2_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mou_kerjasama/delete_foto2_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["jpg", "jpeg"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mou_kerjasama_foto2_galery').fineUploader('getUuid', id);
						$('#mou_kerjasama_foto2_uuid').val(uuid);
						$('#mou_kerjasama_foto2_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mou_kerjasama_foto2_uuid').val();
					$.get(BASE_URL + '/administrator/mou_kerjasama/delete_foto2_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mou_kerjasama_foto2_uuid').val('');
						$('#mou_kerjasama_foto2_name').val('');
					}
				}
			}
		}); /*end foto2 galery*/
	}); /*end doc ready*/
	function cari_pegawai() {
		BootstrapDialog.show({
			title: 'Cari data Simpeg',
			draggable: true,
			message: function(dialog) {
				var $message = $('<div></div>');
				$.get('<?= site_url('administrator/pegawai/form_cari_pegawai') ?>', {}, function(response) {
					$message.html(response);
				})
				return $message;
			},
			buttons: [{
				label: 'Ambil',
				icon: 'glyphicon glyphicon-plus',
				cssClass: 'btn-primary',
				action: function(dialog) {
					$('#penanggung_jawab').val($('#pegawai option:selected').data('nama'));
					$('#pj_email').val($('#pegawai option:selected').data('email'));
					$('#pj_tlp').val($('#pegawai option:selected').data('tlp'));
					dialog.close();
				}
			}, {
				label: 'Tutup',
				action: function(dialog) {
					dialog.close();
				}
			}],
		});
	}
	function cari_mitra() {
		BootstrapDialog.show({
			title: 'Cari mitra',
			draggable: true,
			size: 'size-wide',
			message: function(dialog) {
				var $message = $('<div></div>');
				$.get('<?= site_url('administrator/si_kerjasama/form_cari_mitra') ?>', {page:1}, function(response) {
					$message.html(response);
				})
				return $message;
			},
			buttons: [{
				label: 'Ambil',
				icon: 'glyphicon glyphicon-plus',
				cssClass: 'btn-primary',
				action: function(dialog) {
					var rowData = oTable.rows($('input[name="selectRow"]:checked').closest('tr')).data()[0];
					// console.log(rowData);
					if(typeof rowData  !== 'undefined'){
						$('input[name=nama_mitra]').val(rowData.nama_mitra)
						$('input[name=pj_mitra_email]').val(rowData.email)
						$('input[name=pj_mitra_tlp]').val(rowData.telp)
						$('input[name=id_mitra]').val(rowData.id)
						dialog.close();
					}
				}
			}, {
				label: 'Tutup',
				action: function(dialog) {
					dialog.close();
				}
			}],
		});
	}
</script>