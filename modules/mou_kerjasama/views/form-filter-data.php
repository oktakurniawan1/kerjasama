<form action="" method="POST" role="form">
   <div class="row">
      <div class="col-md-8">
         <div class="form-group">
            <label for="">Jenis Kerjsama</label>
            <?= form_dropdown('jenis_ks', $jenisks, fromsess('filterdata_mou')['jenis_ks'], ['class' => 'form-control']) ?>
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            <label for="">Tahun</label>
            <?= form_dropdown('tahun', $tahun, fromsess('filterdata_mou')['tahun'], ['class' => 'form-control']) ?>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-3">
         <div class="form-group">
            <label for="">Scope</label>
            <?= form_dropdown('scope', $scope, fromsess('filterdata_mou')['scope'], ['class' => 'form-control']) ?>
         </div>
      </div>
      <div class="col-md-6">
         <div class="form-group">
            <label for="">Negara</label>
            <?= form_dropdown('negara', $negara, fromsess('filterdata_mou')['negara'], ['class' => 'form-control']) ?>
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            <label for="">Status</label>
            <?= form_dropdown('is_aktif', $status, fromsess('filterdata_mou')['is_aktif'], ['class' => 'form-control']) ?>
         </div>
      </div>
   </div>
</form>