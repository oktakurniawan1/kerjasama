<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['mou_kerjasama'] = 'MoU Kerjasama';
$lang['id'] = 'Id';
$lang['scope'] = 'DN/LN';
$lang['jenis_ks'] = 'Jenis Kerjasama';
$lang['doc_nomor'] = 'Nomor Dokumen';
$lang['doc_tanggal'] = 'Tanggal Dokumen';
$lang['doc_file'] = 'File Dokumen';
$lang['negara'] = 'Negara';
$lang['tgl_mulai'] = 'Tanggal Mulai';
$lang['tgl_selesai'] = 'Tanggal Selesai';
$lang['nama_mitra'] = 'Nama Mitra';
$lang['penanggung_jawab'] = 'Penanggung Jawab';
$lang['deskripsi'] = 'Deskripsi';
$lang['foto1'] = 'Bukti Foto 1';
$lang['foto2'] = 'Bukti Foto 2';
$lang['user_add'] = 'User Add';
