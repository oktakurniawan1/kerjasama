<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<script type="text/javascript">
   //This page is a result of an autogenerated content made by running test.html with firefox.
   function domo() {

      // Binding keys
      $('*').bind('keydown', 'Ctrl+a', function assets() {
         window.location.href = BASE_URL + '/administrator/Jenis_mitra/add';
         return false;
      });

      $('*').bind('keydown', 'Ctrl+f', function assets() {
         $('#sbtn').trigger('click');
         return false;
      });

      $('*').bind('keydown', 'Ctrl+x', function assets() {
         $('#reset').trigger('click');
         return false;
      });

      $('*').bind('keydown', 'Ctrl+b', function assets() {

         $('#reset').trigger('click');
         return false;
      });
   }

   jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
      <?= cclang('jenis_mitra') ?><small><?= cclang('list_all'); ?></small>
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?= cclang('jenis_mitra') ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">

      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                     <div class="row pull-right">
                        <?php is_allowed('jenis_mitra_add', function () { ?>
                           <a class="btn btn-flat btn-success btn_add_new" id="btn_add_new" title="<?= cclang('add_new_button', [cclang('jenis_mitra')]); ?>  (Ctrl+a)" href="<?= site_url('administrator/jenis_mitra/add'); ?>"><i class="fa fa-plus-square-o"></i> <?= cclang('add_new_button', [cclang('jenis_mitra')]); ?></a>
                        <?php }) ?>
                     </div>
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username"><?= cclang('jenis_mitra') ?></h3>
                     <h5 class="widget-user-desc"><?= cclang('list_all', [cclang('jenis_mitra')]); ?> <i class="label bg-yellow"><?= $jenis_mitra_counts; ?> <?= cclang('items'); ?></i></h5>
                  </div>

                  <form name="form_jenis_mitra" id="form_jenis_mitra" action="<?= base_url('administrator/jenis_mitra/index'); ?>">
                     <div class="row">
                        <div class="col-md-8">
                           <?php is_allowed('jenis_mitra_delete', function () { ?>
                              <div class="col-sm-2 padd-left-0 ">
                                 <select type="text" class="form-control chosen chosen-select" name="bulk" id="bulk" placeholder="Site Email">
                                    <option value="">Bulk</option>
                                    <option value="delete">Delete</option>
                                 </select>
                              </div>
                              <div class="col-sm-2 padd-left-0 ">
                                 <button type="button" class="btn btn-flat" name="apply" id="apply" title="<?= cclang('apply_bulk_action'); ?>"><?= cclang('apply_button'); ?></button>
                              </div>
                           <?php }) ?>
                           <div class="col-sm-3 padd-left-0  ">
                              <input type="text" class="form-control" name="q" id="filter" placeholder="<?= cclang('filter'); ?>" value="<?= $this->session->userdata('jenis_mitra_filter'); ?>">
                           </div>
                           <div class="col-sm-3 padd-left-0 ">
                              <select type="text" class="form-control chosen chosen-select" name="f" id="field">
                                 <option value=""><?= cclang('all'); ?></option>
                                 <option <?= $this->session->userdata('jenis_mitra_field') == 'jenis_mitra' ? 'selected' : ''; ?> value="jenis_mitra">Jenis Mitra</option>
                              </select>
                           </div>
                           <div class="col-sm-1 padd-left-0 ">
                              <button type="submit" class="btn btn-flat" name="sbtn" id="sbtn" value="Apply" title="<?= cclang('filter_search'); ?>">
                                 Filter
                              </button>
                           </div>
                           <div class="col-sm-1 padd-left-0 ">
                              <a class="btn btn-default btn-flat" name="reset" id="reset" value="Apply" href="<?= base_url('administrator/jenis_mitra/?reset_filter='); ?>" title="<?= cclang('reset_filter'); ?>">
                                 <i class="fa fa-undo"></i>
                              </a>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="dataTables_paginate paging_simple_numbers pull-right" id="example2_paginate">
                              <?= $pagination; ?>
                           </div>
                        </div>
                     </div>
                     <div class="table-responsive">
                        <table class="table table-bordered table-striped dataTable">
                           <thead>
                              <tr class="">
                                 <th>
                                    <input type="checkbox" class="flat-red toltip" id="check_all" name="check_all" title="check all">
                                 </th>
                                 <th data-field="jenis_mitra" data-sort="1" data-primary-key="0"> <?= cclang('jenis_mitra') ?></th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody id="tbody_jenis_mitra">
                              <?php foreach ($jenis_mitras as $jenis_mitra) : ?>
                                 <tr>
                                    <td width="5">
                                       <input type="checkbox" class="flat-red check" name="id[]" value="<?= $jenis_mitra->id; ?>">
                                    </td>

                                    <td><?= _ent($jenis_mitra->jenis_mitra); ?></td>
                                    <td width="120">

                                       <?php is_allowed('jenis_mitra_view', function () use ($jenis_mitra) { ?>
                                          <a href="<?= site_url('administrator/jenis_mitra/view/' . $jenis_mitra->id); ?>" class="btn btn-default btn-xs"><i class="fa fa-eye text-info"></i></a>
                                       <?php }) ?>
                                       <?php is_allowed('jenis_mitra_update', function () use ($jenis_mitra) { ?>
                                          <a href="<?= site_url('administrator/jenis_mitra/edit/' . $jenis_mitra->id); ?>" class="btn btn-default btn-xs"><i class="fa fa-edit fa-lg text-warning"></i></a>
                                       <?php }) ?> <?php is_allowed('jenis_mitra_delete', function () use ($jenis_mitra) { ?>
                                          <a href="javascript:void(0);" data-href="<?= site_url('administrator/jenis_mitra/delete/' . $jenis_mitra->id); ?>" class="btn btn-default btn-xs remove-data"><i class="fa fa-trash fa-lg text-danger"></i></a>
                                       <?php }) ?>

                                    </td>
                                 </tr>
                              <?php endforeach; ?>
                              <?php if ($jenis_mitra_counts == 0) : ?>
                                 <tr>
                                    <td colspan="100">
                                       Jenis Mitra data is not available
                                    </td>
                                 </tr>
                              <?php endif; ?>

                           </tbody>
                        </table>
                     </div>
               </div>
               <!-- /.widget-user -->
               <hr>
               </form>
               <div class="row">
                  <div class="col-md-4 col-md-offset-8">
                     <div class="dataTables_paginate paging_simple_numbers pull-right" id="example2_paginate">
                        <?= $pagination; ?>
                     </div>
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->

<!-- Page script -->
<script>
   $(document).ready(function() {

      $('.remove-data').click(function() {

         var url = $(this).attr('data-href');

         swal({
               title: "<?= cclang('are_you_sure'); ?>",
               text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#DD6B55",
               confirmButtonText: "<?= cclang('yes_delete_it'); ?>",
               cancelButtonText: "<?= cclang('no_cancel_plx'); ?>",
               closeOnConfirm: true,
               closeOnCancel: true
            },
            function(isConfirm) {
               if (isConfirm) {
                  document.location.href = url;
               }
            });

         return false;
      });


      $('#apply').click(function() {

         var bulk = $('#bulk');
         var serialize_bulk = $('#form_jenis_mitra').serialize();

         if (bulk.val() == 'delete') {
            swal({
                  title: "<?= cclang('are_you_sure'); ?>",
                  text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "<?= cclang('yes_delete_it'); ?>",
                  cancelButtonText: "<?= cclang('no_cancel_plx'); ?>",
                  closeOnConfirm: true,
                  closeOnCancel: true
               },
               function(isConfirm) {
                  if (isConfirm) {
                     document.location.href = BASE_URL + '/administrator/jenis_mitra/delete?' + serialize_bulk;
                  }
               });

            return false;

         } else if (bulk.val() == '') {
            swal({
               title: "Upss",
               text: "<?= cclang('please_choose_bulk_action_first'); ?>",
               type: "warning",
               showCancelButton: false,
               confirmButtonColor: "#DD6B55",
               confirmButtonText: "Okay!",
               closeOnConfirm: true,
               closeOnCancel: true
            });

            return false;
         }

         return false;

      }); /*end appliy click*/


      //check all
      var checkAll = $('#check_all');
      var checkboxes = $('input.check');

      checkAll.on('ifChecked ifUnchecked', function(event) {
         if (event.type == 'ifChecked') {
            checkboxes.iCheck('check');
         } else {
            checkboxes.iCheck('uncheck');
         }
      });

      checkboxes.on('ifChanged', function(event) {
         if (checkboxes.filter(':checked').length == checkboxes.length) {
            checkAll.prop('checked', 'checked');
         } else {
            checkAll.removeProp('checked');
         }
         checkAll.iCheck('update');
      });
      initSortable('jenis_mitra', $('table.dataTable'));

      $('#data-filter-btn').click(function() {
         form_filter_data();
      })
   }); /*end doc ready*/

   function form_filter_data() {
      var dialog = new BootstrapDialog({
         title: 'Filter Data',
         draggable: true,
         message: function(dialog) {
            var $message = $('<div></div>');
            $.get("<?= site_url('administrator/jenis_mitra/form_filter_data') ?>", {}, function(response) {
               $message.html(response);
            })
            return $message;
         },
         buttons: [{
               label: 'Filter',
               cssClass: 'btn-primary',
               action: function(dialog) {
                  var data = {};
                  data[csrf] = token;
                  data['jenis_mitra'] = $('select[name=jenis_mitra] option:selected').val();
                  $.post("<?= site_url('administrator/jenis_mitra/filter_session_data') ?>", data, function(respon) {
                     document.location.href = respon.url;
                  }, 'json')
                  dialog.close();
               }
            },
            {
               label: 'Clear Filter',
               cssClass: 'btn-warning',
               action: function(dialog) {
                  $.get("<?= site_url('administrator/jenis_mitra/clear_filter_session_data') ?>", {}, function(respon) {
                     document.location.href = respon.url;
                  }, 'json')
                  dialog.close();
               }
            },
            {
               label: 'Tutup',
               action: function(dialog) {
                  dialog.close();
               }
            }
         ],
      });
      dialog.realize(),
         dialog.getModalBody().prop('id', 'filterjenis_mitra-dialog');
      dialog.open();
   }
</script>