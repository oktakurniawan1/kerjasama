<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Jenis Mitra Controller
*| --------------------------------------------------------------------------
*| Jenis Mitra site
*|
*/
class Jenis_mitra extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_jenis_mitra');
		$this->lang->load('web_lang', $this->current_lang);
	}

		/**
	* show all Jenis Mitras
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('jenis_mitra_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if(!is_null($q)) $this->session->set_userdata('jenis_mitra_filter',$q);
		if(!is_null($f)) $this->session->set_userdata('jenis_mitra_field',$f);
		$this->session->set_userdata('_page',$offset);

		if(!is_null($this->input->get('reset_filter'))){
			$this->session->unset_userdata('jenis_mitra_filter');
			$this->session->unset_userdata('jenis_mitra_field');
		}
		$filter = $this->session->userdata('jenis_mitra_filter');
		$field = $this->session->userdata('jenis_mitra_field');
		$offset = $this->session->userdata('_page',$offset);

		$this->data['jenis_mitras'] = $this->model_jenis_mitra->get($filter, $field, $this->limit_page, $offset);
		$this->data['jenis_mitra_counts'] = $this->model_jenis_mitra->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/jenis_mitra/index/',
			'total_rows'   => $this->model_jenis_mitra->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Jenis Mitra List');
		$this->render('backend/standart/administrator/jenis_mitra/jenis_mitra_list', $this->data);
	}
	
	/**
	* Add new jenis_mitras
	*
	*/
	public function add()
	{
		$this->is_allowed('jenis_mitra_add');

		$this->template->title('Jenis Mitra New');
		$this->render('backend/standart/administrator/jenis_mitra/jenis_mitra_add', $this->data);
	}

	/**
	* Add New Jenis Mitras
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('jenis_mitra_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('jenis_mitra', 'Jenis Mitra', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'jenis_mitra' => $this->input->post('jenis_mitra'),
			];

			
			$save_jenis_mitra = $this->model_jenis_mitra->store($save_data);
            

			if ($save_jenis_mitra) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_jenis_mitra;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/jenis_mitra/edit/' . $save_jenis_mitra, 'Edit Jenis Mitra'),
						anchor('administrator/jenis_mitra', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/jenis_mitra/edit/' . $save_jenis_mitra, 'Edit Jenis Mitra')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/jenis_mitra');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/jenis_mitra');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Jenis Mitras
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('jenis_mitra_update');

		$this->data['jenis_mitra'] = $this->model_jenis_mitra->find($id);

		$this->template->title('Jenis Mitra Update');
		$this->render('backend/standart/administrator/jenis_mitra/jenis_mitra_update', $this->data);
	}

	/**
	* Update Jenis Mitras
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('jenis_mitra_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('jenis_mitra', 'Jenis Mitra', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'jenis_mitra' => $this->input->post('jenis_mitra'),
			];

			
			$save_jenis_mitra = $this->model_jenis_mitra->change($id, $save_data);

			if ($save_jenis_mitra) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/jenis_mitra', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/jenis_mitra/index/'.$this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/jenis_mitra/index/'.$this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Jenis Mitras
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('jenis_mitra_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'jenis_mitra'), 'success');
        } else {
            set_message(cclang('error_delete', 'jenis_mitra'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Jenis Mitras
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('jenis_mitra_view');

		$this->data['jenis_mitra'] = $this->model_jenis_mitra->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Jenis Mitra Detail');
		$this->render('backend/standart/administrator/jenis_mitra/jenis_mitra_view', $this->data);
	}
	
	/**
	* delete Jenis Mitras
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$jenis_mitra = $this->model_jenis_mitra->find($id);

		
		
		return $this->model_jenis_mitra->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
				'jenis_mitra',
				];
		$data = $this->model_jenis_mitra->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_jenis_mitra->export2($data, 'file', $selected_field);

		/*
		$this->is_allowed('jenis_mitra_export');

		$this->model_jenis_mitra->export(
			'jenis_mitra', 
			'jenis_mitra',
			$this->model_jenis_mitra->field_search
		);
		*/
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('jenis_mitra_export');

		$this->model_jenis_mitra->pdf('jenis_mitra', 'jenis_mitra');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('jenis_mitra_export');

		$table = $title = 'jenis_mitra';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_jenis_mitra->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file jenis_mitra.php */
/* Location: ./application/controllers/administrator/Jenis Mitra.php */