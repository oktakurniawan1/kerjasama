<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_jenis_mitra extends MY_Model {

    private $primary_key    = 'id';
    private $table_name     = 'jenis_mitra';
    public $field_search   = ['jenis_mitra'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "jenis_mitra.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "jenis_mitra.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "jenis_mitra.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);

		/**form filter data */
		if ($this->session->has_userdata('filterdata_jenis_mitra')) {
			foreach (fromsess('filterdata_jenis_mitra') as $key => $row) {
				if ($row != 'all') $this->db->having($key, $row);
			}
		}
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [], $result = TRUE)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "jenis_mitra.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "jenis_mitra.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "jenis_mitra.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);

		/**form filter data */
		if ($this->session->has_userdata('filterdata_jenis_mitra')) {
			foreach (fromsess('filterdata_jenis_mitra') as $key => $row) {
				if ($row != 'all') $this->db->having($key, $row);
			}
		}
        $this->db->limit($limit, $offset);
        
        $this->sortable();
        
        $query = $this->db->get($this->table_name);

        if ($result)
			return $query->result();
			else return $query;
    }

    public function join_avaiable() {
        
        $this->db->select('jenis_mitra.*');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_jenis_mitra.php */
/* Location: ./application/models/Model_jenis_mitra.php */