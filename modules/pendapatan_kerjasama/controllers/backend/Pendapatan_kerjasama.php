<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Pendapatan Kerjasama Controller
 *| --------------------------------------------------------------------------
 *| Pendapatan Kerjasama site
 *|
 */
class Pendapatan_kerjasama extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_pendapatan_kerjasama');
		$this->load->model('Model_detil_sp2d');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * form filter data 
	 */
	public function filter_session_data()
	{
		$fields['kd_prodi'] = $this->input->post('kd_prodi');
		$fields['unit_ks'] = $this->input->post('unit_ks');
		$fields['nilai_kontrak'] = $this->input->post('nilai_kontrak');
		$fields['nilai_diterima'] = $this->input->post('nilai_diterima');
		$fields['kebutuhan_rab'] = $this->input->post('kebutuhan_rab');
		$fields['validasi'] = $this->input->post('validasi');
		$this->session->set_userdata('filterdata_pendapatan_kerjasama', $fields);
		$this->session->set_userdata('is_filtered_pendapatan_kerjasama', TRUE);
		echo json_encode(['url' => base_url('administrator/pendapatan_kerjasama')]);
	}

	public function clear_filter_session_data()
	{
		$this->session->unset_userdata('filterdata_pendapatan_kerjasama');
		$this->session->unset_userdata('is_filtered_pendapatan_kerjasama');
		echo json_encode(['url' => base_url('administrator/pendapatan_kerjasama')]);
	}

	public function form_filter_data()
	{
		if (!$this->session->has_userdata('filterdata_pendapatan_kerjasama')) {
			$fields['kd_prodi'] = 'all';
			$fields['validasi'] = 'all';
			$this->session->set_userdata('filterdata_pendapatan_kerjasama', $fields);
		}
		$prodi = array_column(get_filter_prodi(), 'nama_prodi', 'kode');
		$prodi['all'] = 'ALL';
		$data['kd_prodi'] = $prodi;
		$data['validasi'] = ['all' => 'ALL', 'Y' => 'Valid', 'N' => 'Invalid'];
		$this->load->view('form-filter-data', $data);
	}
	/**
	 * Upload SP2D
	 */
	public function form_upload_sp2d()
	{
		$data['pendapatan'] = $this->model_pendapatan_kerjasama->find($this->input->get('id'));
		$this->load->view('form-mnj-sp2d', $data);
	}

	public function browse_sp2d()
	{
		echo $this->model_pendapatan_kerjasama->getSp2d($this->input->post('id_pendapatan'));
	}
	public function hapus_sp2d()
	{
		$data = $this->Model_detil_sp2d->find($this->input->get('id'));
		@unlink($data->file_sp2d);
		$status = $this->Model_detil_sp2d->remove($data->id);
		echo json_encode(['status' => $status]);
	}
	public function update_sp2d()
	{
		$this->Model_detil_sp2d->change($this->input->post('id'), ['deskripsi' => $this->input->post('deskripsi')]);
	}

	public function upload_file_sp2d()
	{
		$config['upload_path']      = FCPATH . '/uploads/pendapatan_kerjasama/';
		$config['allowed_types']    = 'pdf';
		$config['max_size']         = 5210;
		$config['encrypt_name']     = true;
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('file_sp2d')) {
			$errors = $this->upload->display_errors();
			echo json_encode(['status' => FALSE, 'data' => html_alert($errors, 'Galat', 'warning')]);
		} else {
			$file = $this->upload->data();
			$data['deskripsi'] = $this->input->post('deskripsi');
			// $data['file_sp2d'] = $file['full_path'];
			$data['file_sp2d'] = $file['file_name'];
			$data['tgl_upload'] = date('Y-m-d :H:s:s');
			$data['id_pendapatan'] = $this->input->post('id_pendapatan');
			$r = $this->Model_detil_sp2d->store($data);
			echo json_encode(['status' => TRUE, 'data' => html_alert('Berhasil upload', 'Informasi', 'success')]);
			// @unlink($file['full_path']);
		}
	}
	/**
	 * show all Pendapatan Kerjasamas
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('pendapatan_kerjasama_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('pendapatan_kerjasama_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('pendapatan_kerjasama_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('pendapatan_kerjasama_filter');
			$this->session->unset_userdata('pendapatan_kerjasama_field');
		}
		$filter = $this->session->userdata('pendapatan_kerjasama_filter');
		$field = $this->session->userdata('pendapatan_kerjasama_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['pendapatan_kerjasamas'] = $this->model_pendapatan_kerjasama->get($filter, $field, $this->limit_page, $offset);
		$this->data['pendapatan_kerjasama_counts'] = $this->model_pendapatan_kerjasama->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/pendapatan_kerjasama/index/',
			'total_rows'   => $this->model_pendapatan_kerjasama->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Pendapatan Kerjasama List');
		$this->render('backend/standart/administrator/pendapatan_kerjasama/pendapatan_kerjasama_list', $this->data);
	}

	/**
	 * Add new pendapatan_kerjasamas
	 *
	 */
	public function add()
	{
		$this->is_allowed('pendapatan_kerjasama_add');
		$filter_prodi = get_filter_prodi();
		$this->data['filter_prodi'] = $filter_prodi;
		$this->template->title('Pendapatan Kerjasama New');
		$this->render('backend/standart/administrator/pendapatan_kerjasama/pendapatan_kerjasama_add', $this->data);
	}

	/**
	 * Add New Pendapatan Kerjasamas
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('pendapatan_kerjasama_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('unit_ks', 'MoA/LoA/IA', 'trim|required');
		$this->form_validation->set_rules('nilai_kontrak', 'Nominal Kontrak', 'trim|required|max_length[255]|callback_valid_number');
		$this->form_validation->set_rules('nilai_diterima', 'Nominal Diterima', 'trim|required|max_length[255]|callback_valid_number');
		$this->form_validation->set_rules('kebutuhan_rab', 'Nominal Kebutuhan RAB', 'trim|required|max_length[255]|callback_valid_number');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
		$this->form_validation->set_rules('pendapatan_kerjasama_file_pengantar_name', 'File Pengantar', 'trim|required');
		$this->form_validation->set_rules('pendapatan_kerjasama_file_sptjm_name', 'File SPTJM', 'trim|required');
		$this->form_validation->set_rules('pendapatan_kerjasama_file_rab_name', 'File RAB', 'trim|required');
		$this->form_validation->set_rules('pendapatan_kerjasama_file_pendukung_name', 'File Pendukung', 'trim');
		$this->form_validation->set_rules('bank_rekening', 'Nomor Rekening', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('bank_nama', 'Nama Bank', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('bank_atasnama', 'Nama Pemegang Rekening', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('validasi', 'Validasi', 'trim');


		if ($this->form_validation->run()) {
			$pendapatan_kerjasama_file_pengantar_uuid = $this->input->post('pendapatan_kerjasama_file_pengantar_uuid');
			$pendapatan_kerjasama_file_pengantar_name = $this->input->post('pendapatan_kerjasama_file_pengantar_name');
			$pendapatan_kerjasama_file_sptjm_uuid = $this->input->post('pendapatan_kerjasama_file_sptjm_uuid');
			$pendapatan_kerjasama_file_sptjm_name = $this->input->post('pendapatan_kerjasama_file_sptjm_name');
			$pendapatan_kerjasama_file_rab_uuid = $this->input->post('pendapatan_kerjasama_file_rab_uuid');
			$pendapatan_kerjasama_file_rab_name = $this->input->post('pendapatan_kerjasama_file_rab_name');
			$pendapatan_kerjasama_file_pendukung_uuid = $this->input->post('pendapatan_kerjasama_file_pendukung_uuid');
			$pendapatan_kerjasama_file_pendukung_name = $this->input->post('pendapatan_kerjasama_file_pendukung_name');

			$save_data = [
				'kd_prodi' => $this->input->post('kd_prodi'),
				'unit_ks' => $this->input->post('unit_ks'),
				'nilai_kontrak' => preg_replace('/[^0-9]/', '', $this->input->post('nilai_kontrak')),
				'nilai_diterima' => preg_replace('/[^0-9]/', '', $this->input->post('nilai_diterima')),
				'kebutuhan_rab' => preg_replace('/[^0-9]/', '', $this->input->post('kebutuhan_rab')),
				'deskripsi' => $this->input->post('deskripsi'),
				'validasi' => $this->input->post('validasi'),
				'bank_rekening' => $this->input->post('bank_rekening'),
				'bank_nama' => $this->input->post('bank_nama'),
				'bank_atasnama' => $this->input->post('bank_atasnama'),
			];

			if (!is_dir(FCPATH . '/uploads/pendapatan_kerjasama/')) {
				mkdir(FCPATH . '/uploads/pendapatan_kerjasama/');
			}

			if (!empty($pendapatan_kerjasama_file_pengantar_name)) {
				$pendapatan_kerjasama_file_pengantar_name_copy = date('YmdHis') . '-' . $pendapatan_kerjasama_file_pengantar_name;

				rename(
					FCPATH . 'uploads/tmp/' . $pendapatan_kerjasama_file_pengantar_uuid . '/' . $pendapatan_kerjasama_file_pengantar_name,
					FCPATH . 'uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama_file_pengantar_name_copy
				);

				if (!is_file(FCPATH . '/uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama_file_pengantar_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_pengantar'] = $pendapatan_kerjasama_file_pengantar_name_copy;
			}

			if (!empty($pendapatan_kerjasama_file_sptjm_name)) {
				$pendapatan_kerjasama_file_sptjm_name_copy = date('YmdHis') . '-' . $pendapatan_kerjasama_file_sptjm_name;

				rename(
					FCPATH . 'uploads/tmp/' . $pendapatan_kerjasama_file_sptjm_uuid . '/' . $pendapatan_kerjasama_file_sptjm_name,
					FCPATH . 'uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama_file_sptjm_name_copy
				);

				if (!is_file(FCPATH . '/uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama_file_sptjm_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_sptjm'] = $pendapatan_kerjasama_file_sptjm_name_copy;
			}

			if (!empty($pendapatan_kerjasama_file_rab_name)) {
				$pendapatan_kerjasama_file_rab_name_copy = date('YmdHis') . '-' . $pendapatan_kerjasama_file_rab_name;

				rename(
					FCPATH . 'uploads/tmp/' . $pendapatan_kerjasama_file_rab_uuid . '/' . $pendapatan_kerjasama_file_rab_name,
					FCPATH . 'uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama_file_rab_name_copy
				);

				if (!is_file(FCPATH . '/uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama_file_rab_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_rab'] = $pendapatan_kerjasama_file_rab_name_copy;
			}

			if (!empty($pendapatan_kerjasama_file_pendukung_name)) {
				$pendapatan_kerjasama_file_pendukung_name_copy = date('YmdHis') . '-' . $pendapatan_kerjasama_file_pendukung_name;

				rename(
					FCPATH . 'uploads/tmp/' . $pendapatan_kerjasama_file_pendukung_uuid . '/' . $pendapatan_kerjasama_file_pendukung_name,
					FCPATH . 'uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama_file_pendukung_name_copy
				);

				if (!is_file(FCPATH . '/uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama_file_pendukung_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_pendukung'] = $pendapatan_kerjasama_file_pendukung_name_copy;
			}


			$save_pendapatan_kerjasama = $this->model_pendapatan_kerjasama->store($save_data);


			if ($save_pendapatan_kerjasama) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_pendapatan_kerjasama;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/pendapatan_kerjasama/edit/' . $save_pendapatan_kerjasama, 'Edit Pendapatan Kerjasama'),
						anchor('administrator/pendapatan_kerjasama', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/pendapatan_kerjasama/edit/' . $save_pendapatan_kerjasama, 'Edit Pendapatan Kerjasama')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/pendapatan_kerjasama');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/pendapatan_kerjasama');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Pendapatan Kerjasamas
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('pendapatan_kerjasama_update');

		$this->data['pendapatan_kerjasama'] = $this->model_pendapatan_kerjasama->find($id);

		if (!is_groups_in(['Admin', 'Kerjasama'])) :
			if ($this->data['pendapatan_kerjasama']->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa mengubah, karena data sudah divalidasi', 'pendapatan_kerjasama'), 'warning');
				redirect_back();
			}
		endif;

		$filter_prodi = get_filter_prodi();
		$this->data['filter_prodi'] = $filter_prodi;
		$this->template->title('Pendapatan Kerjasama Update');
		$this->render('backend/standart/administrator/pendapatan_kerjasama/pendapatan_kerjasama_update', $this->data);
	}

	/**
	 * Update Pendapatan Kerjasamas
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('pendapatan_kerjasama_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('unit_ks', 'MoA/LoA/IA', 'trim|required');
		$this->form_validation->set_rules('nilai_kontrak', 'Nominal Kontrak', 'trim|required|max_length[255]|callback_valid_number');
		$this->form_validation->set_rules('nilai_diterima', 'Nominal Diterima', 'trim|required|max_length[255]|callback_valid_number');
		$this->form_validation->set_rules('kebutuhan_rab', 'Nominal Kebutuhan RAB', 'trim|required|max_length[255]|callback_valid_number');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
		$this->form_validation->set_rules('pendapatan_kerjasama_file_pengantar_name', 'File Pengantar', 'trim|required');
		$this->form_validation->set_rules('pendapatan_kerjasama_file_sptjm_name', 'File SPTJM', 'trim|required');
		$this->form_validation->set_rules('pendapatan_kerjasama_file_rab_name', 'File RAB', 'trim|required');
		$this->form_validation->set_rules('pendapatan_kerjasama_file_pendukung_name', 'File Pendukung', 'trim');
		$this->form_validation->set_rules('validasi', 'Validasi', 'trim');
		$this->form_validation->set_rules('bank_rekening', 'Nomor Rekening', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('bank_nama', 'Nama Bank', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('bank_atasnama', 'Nama Pemegang Rekening', 'trim|required|max_length[255]');

		if ($this->form_validation->run()) {
			$pendapatan_kerjasama_file_pengantar_uuid = $this->input->post('pendapatan_kerjasama_file_pengantar_uuid');
			$pendapatan_kerjasama_file_pengantar_name = $this->input->post('pendapatan_kerjasama_file_pengantar_name');
			$pendapatan_kerjasama_file_sptjm_uuid = $this->input->post('pendapatan_kerjasama_file_sptjm_uuid');
			$pendapatan_kerjasama_file_sptjm_name = $this->input->post('pendapatan_kerjasama_file_sptjm_name');
			$pendapatan_kerjasama_file_rab_uuid = $this->input->post('pendapatan_kerjasama_file_rab_uuid');
			$pendapatan_kerjasama_file_rab_name = $this->input->post('pendapatan_kerjasama_file_rab_name');
			$pendapatan_kerjasama_file_pendukung_uuid = $this->input->post('pendapatan_kerjasama_file_pendukung_uuid');
			$pendapatan_kerjasama_file_pendukung_name = $this->input->post('pendapatan_kerjasama_file_pendukung_name');

			$save_data = [
				'kd_prodi' => $this->input->post('kd_prodi'),
				'unit_ks' => $this->input->post('unit_ks'),
				'nilai_kontrak' => preg_replace('/[^0-9]/', '', $this->input->post('nilai_kontrak')),
				'nilai_diterima' => preg_replace('/[^0-9]/', '', $this->input->post('nilai_diterima')),
				'kebutuhan_rab' => preg_replace('/[^0-9]/', '', $this->input->post('kebutuhan_rab')),
				'deskripsi' => $this->input->post('deskripsi'),
				'validasi' => $this->input->post('validasi'),
				'bank_rekening' => $this->input->post('bank_rekening'),
				'bank_nama' => $this->input->post('bank_nama'),
				'bank_atasnama' => $this->input->post('bank_atasnama'),
			];

			if (!is_dir(FCPATH . '/uploads/pendapatan_kerjasama/')) {
				mkdir(FCPATH . '/uploads/pendapatan_kerjasama/');
			}

			if (!empty($pendapatan_kerjasama_file_pengantar_uuid)) {
				$pendapatan_kerjasama_file_pengantar_name_copy = date('YmdHis') . '-' . $pendapatan_kerjasama_file_pengantar_name;

				rename(
					FCPATH . 'uploads/tmp/' . $pendapatan_kerjasama_file_pengantar_uuid . '/' . $pendapatan_kerjasama_file_pengantar_name,
					FCPATH . 'uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama_file_pengantar_name_copy
				);

				if (!is_file(FCPATH . '/uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama_file_pengantar_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_pengantar'] = $pendapatan_kerjasama_file_pengantar_name_copy;
			}

			if (!empty($pendapatan_kerjasama_file_sptjm_uuid)) {
				$pendapatan_kerjasama_file_sptjm_name_copy = date('YmdHis') . '-' . $pendapatan_kerjasama_file_sptjm_name;

				rename(
					FCPATH . 'uploads/tmp/' . $pendapatan_kerjasama_file_sptjm_uuid . '/' . $pendapatan_kerjasama_file_sptjm_name,
					FCPATH . 'uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama_file_sptjm_name_copy
				);

				if (!is_file(FCPATH . '/uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama_file_sptjm_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_sptjm'] = $pendapatan_kerjasama_file_sptjm_name_copy;
			}

			if (!empty($pendapatan_kerjasama_file_rab_uuid)) {
				$pendapatan_kerjasama_file_rab_name_copy = date('YmdHis') . '-' . $pendapatan_kerjasama_file_rab_name;

				rename(
					FCPATH . 'uploads/tmp/' . $pendapatan_kerjasama_file_rab_uuid . '/' . $pendapatan_kerjasama_file_rab_name,
					FCPATH . 'uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama_file_rab_name_copy
				);

				if (!is_file(FCPATH . '/uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama_file_rab_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_rab'] = $pendapatan_kerjasama_file_rab_name_copy;
			}

			if (!empty($pendapatan_kerjasama_file_pendukung_uuid)) {
				$pendapatan_kerjasama_file_pendukung_name_copy = date('YmdHis') . '-' . $pendapatan_kerjasama_file_pendukung_name;

				rename(
					FCPATH . 'uploads/tmp/' . $pendapatan_kerjasama_file_pendukung_uuid . '/' . $pendapatan_kerjasama_file_pendukung_name,
					FCPATH . 'uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama_file_pendukung_name_copy
				);

				if (!is_file(FCPATH . '/uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama_file_pendukung_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_pendukung'] = $pendapatan_kerjasama_file_pendukung_name_copy;
			}


			$save_pendapatan_kerjasama = $this->model_pendapatan_kerjasama->change($id, $save_data);

			if ($save_pendapatan_kerjasama) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/pendapatan_kerjasama', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/pendapatan_kerjasama/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/pendapatan_kerjasama/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Pendapatan Kerjasamas
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('pendapatan_kerjasama_delete');

		if (is_groups_in(['Kaprodi', 'Unit'])) {
			$prodi = get_filter_prodi(true);
			$row = $this->model_pendapatan_kerjasama->find($id);
			if (!in_array($row->kd_prodi, $prodi)) {
				set_message(cclang('sorry_you_do_not_have_permission_to_access', 'pendapatan_kerjasama_delete'), 'error');
				redirect_back();
			}
			/**tolak jika sudah divalidasi */
			if ($row->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa menghapus data, karena data sudah divalidasi', 'pendapatan_kerjasama'), 'warning');
				redirect_back();
			}
		}

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'pendapatan_kerjasama'), 'success');
		} else {
			set_message(cclang('error_delete', 'pendapatan_kerjasama'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Pendapatan Kerjasamas
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('pendapatan_kerjasama_view');

		$this->data['pendapatan_kerjasama'] = $this->model_pendapatan_kerjasama->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Pendapatan Kerjasama Detail');
		$this->render('backend/standart/administrator/pendapatan_kerjasama/pendapatan_kerjasama_view', $this->data);
	}

	/**
	 * delete Pendapatan Kerjasamas
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$pendapatan_kerjasama = $this->model_pendapatan_kerjasama->find($id);

		if (!empty($pendapatan_kerjasama->file_pengantar)) {
			$path = FCPATH . '/uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama->file_pengantar;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($pendapatan_kerjasama->file_sptjm)) {
			$path = FCPATH . '/uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama->file_sptjm;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($pendapatan_kerjasama->file_rab)) {
			$path = FCPATH . '/uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama->file_rab;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($pendapatan_kerjasama->file_pendukung)) {
			$path = FCPATH . '/uploads/pendapatan_kerjasama/' . $pendapatan_kerjasama->file_pendukung;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}


		return $this->model_pendapatan_kerjasama->remove($id);
	}

	/**
	 * Upload Image Pendapatan Kerjasama	* 
	 * @return JSON
	 */
	public function upload_file_pengantar_file()
	{
		if (!$this->is_allowed('pendapatan_kerjasama_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'pendapatan_kerjasama',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Pendapatan Kerjasama	* 
	 * @return JSON
	 */
	public function delete_file_pengantar_file($uuid)
	{
		if (!$this->is_allowed('pendapatan_kerjasama_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_pengantar',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'pendapatan_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/pendapatan_kerjasama/'
		]);
	}

	/**
	 * Get Image Pendapatan Kerjasama	* 
	 * @return JSON
	 */
	public function get_file_pengantar_file($id)
	{
		if (!$this->is_allowed('pendapatan_kerjasama_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$pendapatan_kerjasama = $this->model_pendapatan_kerjasama->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_pengantar',
			'table_name'        => 'pendapatan_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/pendapatan_kerjasama/',
			'delete_endpoint'   => 'administrator/pendapatan_kerjasama/delete_file_pengantar_file'
		]);
	}

	/**
	 * Upload Image Pendapatan Kerjasama	* 
	 * @return JSON
	 */
	public function upload_file_sptjm_file()
	{
		if (!$this->is_allowed('pendapatan_kerjasama_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'pendapatan_kerjasama',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Pendapatan Kerjasama	* 
	 * @return JSON
	 */
	public function delete_file_sptjm_file($uuid)
	{
		if (!$this->is_allowed('pendapatan_kerjasama_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_sptjm',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'pendapatan_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/pendapatan_kerjasama/'
		]);
	}

	/**
	 * Get Image Pendapatan Kerjasama	* 
	 * @return JSON
	 */
	public function get_file_sptjm_file($id)
	{
		if (!$this->is_allowed('pendapatan_kerjasama_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$pendapatan_kerjasama = $this->model_pendapatan_kerjasama->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_sptjm',
			'table_name'        => 'pendapatan_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/pendapatan_kerjasama/',
			'delete_endpoint'   => 'administrator/pendapatan_kerjasama/delete_file_sptjm_file'
		]);
	}

	/**
	 * Upload Image Pendapatan Kerjasama	* 
	 * @return JSON
	 */
	public function upload_file_rab_file()
	{
		if (!$this->is_allowed('pendapatan_kerjasama_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'pendapatan_kerjasama',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Pendapatan Kerjasama	* 
	 * @return JSON
	 */
	public function delete_file_rab_file($uuid)
	{
		if (!$this->is_allowed('pendapatan_kerjasama_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_rab',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'pendapatan_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/pendapatan_kerjasama/'
		]);
	}

	/**
	 * Get Image Pendapatan Kerjasama	* 
	 * @return JSON
	 */
	public function get_file_rab_file($id)
	{
		if (!$this->is_allowed('pendapatan_kerjasama_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$pendapatan_kerjasama = $this->model_pendapatan_kerjasama->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_rab',
			'table_name'        => 'pendapatan_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/pendapatan_kerjasama/',
			'delete_endpoint'   => 'administrator/pendapatan_kerjasama/delete_file_rab_file'
		]);
	}

	/**
	 * Upload Image Pendapatan Kerjasama	* 
	 * @return JSON
	 */
	public function upload_file_pendukung_file()
	{
		if (!$this->is_allowed('pendapatan_kerjasama_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'pendapatan_kerjasama',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Pendapatan Kerjasama	* 
	 * @return JSON
	 */
	public function delete_file_pendukung_file($uuid)
	{
		if (!$this->is_allowed('pendapatan_kerjasama_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_pendukung',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'pendapatan_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/pendapatan_kerjasama/'
		]);
	}

	/**
	 * Get Image Pendapatan Kerjasama	* 
	 * @return JSON
	 */
	public function get_file_pendukung_file($id)
	{
		if (!$this->is_allowed('pendapatan_kerjasama_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$pendapatan_kerjasama = $this->model_pendapatan_kerjasama->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_pendukung',
			'table_name'        => 'pendapatan_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/pendapatan_kerjasama/',
			'delete_endpoint'   => 'administrator/pendapatan_kerjasama/delete_file_pendukung_file'
		]);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
			'kd_prodi',
			'unit_ks',
			'nilai_kontrak',
			'nilai_diterima',
			'kebutuhan_rab',
			'deskripsi',
			'validasi',
		];
		$data = $this->model_pendapatan_kerjasama->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_pendapatan_kerjasama->export2($data, 'file', $selected_field);

		/*
		$this->is_allowed('pendapatan_kerjasama_export');

		$this->model_pendapatan_kerjasama->export(
			'pendapatan_kerjasama', 
			'pendapatan_kerjasama',
			$this->model_pendapatan_kerjasama->field_search
		);
		*/
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('pendapatan_kerjasama_export');

		$this->model_pendapatan_kerjasama->pdf('pendapatan_kerjasama', 'pendapatan_kerjasama');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('pendapatan_kerjasama_export');

		$table = $title = 'pendapatan_kerjasama';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_pendapatan_kerjasama->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}

	public function ajax_unit_ks($id = null)
	{
		if (!$this->is_allowed('pendapatan_kerjasama_list', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$unit_ks = $this->input->get('unit_ks');

		// $results = db_get_all_data('si_kerjasama', ['kd_prodi' => $id]);
		$results = db_get_all_data('si_kerjasama', "tgl_selesai > curdate() AND kd_prodi ='{$id}' ");

		if (!is_null($unit_ks)) {
			$cur_moa = db_get_all_data('si_kerjasama', ['id' => $unit_ks], 'row');
			if (!is_null($cur_moa)) {
				$tgl = DateTime::createFromFormat('Y-m-d', $cur_moa->tgl_selesai);
				$now = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
				if ($tgl < $now) $results[] = $cur_moa; // tambahkan data ke dalam list
			}
		}
		$this->response($results);
	}
}


/* End of file pendapatan_kerjasama.php */
/* Location: ./application/controllers/administrator/Pendapatan Kerjasama.php */