<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_pendapatan_kerjasama extends MY_Model
{

	private $primary_key    = 'id';
	private $table_name     = 'pendapatan_kerjasama';
	public $field_search   = ['kd_prodi', 'unit_ks', 'nilai_kontrak', 'nilai_diterima', 'kebutuhan_rab', 'validasi','siakun_valid','sianggar_valid'];

	public function __construct()
	{
		$config = array(
			'primary_key'   => $this->primary_key,
			'table_name'    => $this->table_name,
			'field_search'  => $this->field_search,
		);

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);
		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "pendapatan_kerjasama." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'unit_ks') $where .= "OR si_kerjasama.nama_mitra LIKE '%" . $q . "%' OR si_kerjasama.doc_nomor LIKE '%".$q."%' ";
				} else {
					$where .= "OR " . "pendapatan_kerjasama." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'unit_ks') $where .= "OR si_kerjasama.nama_mitra LIKE '%" . $q . "%' OR si_kerjasama.doc_nomor LIKE '%".$q."%' ";
				}
				$iterasi++;
			}
			$where = '(' . $where . ')';
		} else {
			$where .= "(" . "pendapatan_kerjasama." . $field . " LIKE '%" . $q . "%' )";
			if ($field == 'unit_ks') $where .= "OR si_kerjasama.nama_mitra LIKE '%" . $q . "%' OR si_kerjasama.doc_nomor LIKE '%".$q."%' ";
		}
		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);
		/**form filter data */
		if ($this->session->has_userdata('filterdata_pendapatan_kerjasama')) {
			foreach (fromsess('filterdata_pendapatan_kerjasama') as $key => $row) {
				if ($row != 'all') $this->db->having($key, $row);
			}
		}
		$query = $this->db->group_by('pendapatan_kerjasama.id');
		$query = $this->db->get($this->table_name);
		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [], $result = TRUE)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);
		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "pendapatan_kerjasama." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'unit_ks') $where .= "OR si_kerjasama.nama_mitra LIKE '%" . $q . "%' OR si_kerjasama.doc_nomor LIKE '%".$q."%' ";
				} else {
					$where .= "OR " . "pendapatan_kerjasama." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'unit_ks') $where .= "OR si_kerjasama.nama_mitra LIKE '%" . $q . "%' OR si_kerjasama.doc_nomor LIKE '%".$q."%' ";
				}
				$iterasi++;
			}
			$where = '(' . $where . ')';
		} else {
			$where .= "(" . "pendapatan_kerjasama." . $field . " LIKE '%" . $q . "%' )";
			if ($field == 'unit_ks') $where .= "OR si_kerjasama.nama_mitra LIKE '%" . $q . "%' OR si_kerjasama.doc_nomor LIKE '%".$q."%' ";
		}
		if (is_array($select_field) and count($select_field)) {
			$this->db->select($select_field);
		}
		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);
		/**form filter data */
		if ($this->session->has_userdata('filterdata_pendapatan_kerjasama')) {
			foreach (fromsess('filterdata_pendapatan_kerjasama') as $key => $row) {
				if ($row != 'all') $this->db->having($key, $row);
			}
		}
		$this->db->limit($limit, $offset);
		$this->sortable();
		$query = $this->db->group_by('pendapatan_kerjasama.id');
		$query = $this->db->get($this->table_name);
		if ($result)
			return $query->result();
		else return $query;
	}

	public function join_avaiable()
	{
		$this->db->join('ref_prodi', 'ref_prodi.kode = pendapatan_kerjasama.kd_prodi', 'LEFT');
		$this->db->join('si_kerjasama', 'si_kerjasama.id = pendapatan_kerjasama.unit_ks', 'LEFT');
		$this->db->join('detail_sp2d', 'detail_sp2d.id_pendapatan= pendapatan_kerjasama.id', 'LEFT');
		$this->db->select('count(detail_sp2d.id) AS jml_sp2d');
		$this->db->select('si_kerjasama.is_aktif,si_kerjasama.tgl_mulai,si_kerjasama.tgl_selesai,si_kerjasama.doc_nomor as si_kerjasama_doc_nomor');
		$this->db->select("@sisa:=PERIOD_DIFF(EXTRACT(YEAR_MONTH FROM si_kerjasama.tgl_selesai),EXTRACT(YEAR_MONTH FROM CURDATE())) as _sb,if(@sisa<0,0,@sisa) as sisa_bulan");
		$this->db->select('pendapatan_kerjasama.*,ref_prodi.nama_prodi as ref_prodi_nama_prodi,si_kerjasama.nama_mitra as si_kerjasama_nama_mitra');
		return $this;
	}

	public function filter_avaiable()
	{
		if (!$this->aauth->is_admin()) {
		}
		return $this;
	}
	public function getSp2d($id_pendapatan){
		$this->load->library('Datatables', null, 'dtable');
		$this->dtable->set_database('default');
		$this->dtable->select('id,id_pendapatan,file_sp2d,tgl_upload,deskripsi')->where(['id_pendapatan' => $id_pendapatan])->from('detail_sp2d');
		return $this->dtable->generate();
	}
}

/* End of file Model_pendapatan_kerjasama.php */
/* Location: ./application/models/Model_pendapatan_kerjasama.php */