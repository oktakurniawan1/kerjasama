<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_detil_sp2d extends MY_Model
{
	private $primary_key    = 'id';
	private $table_name     = 'detail_sp2d';
	public $field_search   = ['id', 'id_pendapatan', 'file_sp2d', 'tgl_upload', 'deskripsi'];
	public function __construct()
	{
		$config = array(
			'primary_key'   => $this->primary_key,
			'table_name'    => $this->table_name,
			'field_search'  => $this->field_search,
		);

		parent::__construct($config);
	}
}