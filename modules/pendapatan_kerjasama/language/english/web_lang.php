<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['pendapatan_kerjasama'] = 'Pendapatan Kerjasama';
$lang['id'] = 'Id';
$lang['kd_prodi'] = 'Prodi/Unit';
$lang['unit_ks'] = 'MoA/LoA/IA';
$lang['nilai_kontrak'] = 'Nominal Kontrak';
$lang['nilai_diterima'] = 'Nominal Diterima';
$lang['kebutuhan_rab'] = 'Nominal RAB';
$lang['deskripsi'] = 'Deskripsi';
$lang['file_pengantar'] = 'File Pengantar';
$lang['file_sptjm'] = 'File SPTJM';
$lang['file_rab'] = 'File RAB';
$lang['tgl_mulai'] = 'Tgl mulai';
$lang['tgl_selesai'] = 'Tgl selesai';
$lang['is_aktif'] = 'Status';
$lang['file_pendukung'] = 'File Pendukung';
$lang['validasi'] = 'Validasi';
