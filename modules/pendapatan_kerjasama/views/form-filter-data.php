<form action="" method="POST" role="form">
	<div class="row">
		<div class="col-md-8">
			<div class="form-group">
				<label for="">Prodi/Unit</label>
				<?= form_dropdown('kd_prodi', $kd_prodi, fromsess('filterdata_pendapatan_kerjasama')['kd_prodi'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label for="">Validasi</label>
				<?= form_dropdown('validasi', $validasi, fromsess('filterdata_pendapatan_kerjasama')['validasi'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
			</div>
		</div>
	</div>
</form>

<script>
	$(function() {
		$('select[name=kd_prodi]').select2({
			theme: 'classic',
			dropdownParent: $('#filterpendapatan_kerjasama-dialog'),
		});
		// $('select[name=name4]').select2({
		// 	tags:true
		// })
	})
</script>