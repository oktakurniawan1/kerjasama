<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>

<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Pendapatan Kerjasama <small>Edit Pendapatan Kerjasama</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/pendapatan_kerjasama'); ?>">Pendapatan Kerjasama</a></li>
		<li class="active">Edit</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Pendapatan Kerjasama</h3>
							<h5 class="widget-user-desc">Edit Pendapatan Kerjasama</h5>
							<hr>
						</div>
						<?= form_open(base_url('administrator/pendapatan_kerjasama/edit_save/' . $this->uri->segment(4)), [
							'name'    => 'form_pendapatan_kerjasama',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_pendapatan_kerjasama',
							'method'  => 'POST'
						]); ?>

						<div class="form-group ">
							<label for="kd_prodi" class="col-sm-2 control-label">Prodi/Unit
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="kd_prodi" id="kd_prodi" data-placeholder="Select Prodi/Unit">
									<option value=""></option>
									<?php foreach ($filter_prodi as $row) : ?>
										<option <?= $row->kode ==  $pendapatan_kerjasama->kd_prodi ? 'selected' : ''; ?> value="<?= $row->kode ?>"><?= $row->kode .' - '.$row->nama_prodi; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>


						<div class="form-group ">
							<label for="unit_ks" class="col-sm-2 control-label">MoA/LoA/IA
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="unit_ks" id="unit_ks" data-placeholder="Select MoA/LoA/IA">
									<option value=""></option>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>


						<div class="form-group ">
							<label for="nilai_kontrak" class="col-sm-2 control-label">Nominal Kontrak
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control money" name="nilai_kontrak" id="nilai_kontrak" placeholder="Nominal Kontrak" value="<?= set_value('nilai_kontrak', $pendapatan_kerjasama->nilai_kontrak); ?>">
								<small class="info help-block">
									<b>Format Nilai Kontrak must</b> Valid Number, <b>Input Nilai Kontrak</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nilai_diterima" class="col-sm-2 control-label">Nominal Diterima
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control money" name="nilai_diterima" id="nilai_diterima" placeholder="Nominal Diterima" value="<?= set_value('nilai_diterima', $pendapatan_kerjasama->nilai_diterima); ?>">
								<small class="info help-block">
									<b>Format Nilai Diterima must</b> Valid Number, <b>Input Nilai Diterima</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="kebutuhan_rab" class="col-sm-2 control-label">Nominal Kebutuhan RAB
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control money" name="kebutuhan_rab" id="kebutuhan_rab" placeholder="Nominal Kebutuhan RAB" value="<?= set_value('kebutuhan_rab', $pendapatan_kerjasama->kebutuhan_rab); ?>">
								<small class="info help-block">
									<b>Format Kebutuhan Rab must</b> Valid Number, <b>Input Kebutuhan Rab</b> Max Length : 255.</small>
							</div>
						</div>
						<div class="form-group ">
							<label for="bank_rekening" class="col-sm-2 control-label">Nomor Rekening
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="bank_rekening" id="bank_rekening" placeholder="Nomor Rekening" value="<?= set_value('bank_rekening', $pendapatan_kerjasama->bank_rekening); ?>">
								<small class="info help-block">
									<b>Input Bank Rekening</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="bank_nama" class="col-sm-2 control-label">Nama Bank
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="bank_nama" id="bank_nama" placeholder="Nama Bank" value="<?= set_value('bank_nama', $pendapatan_kerjasama->bank_nama); ?>">
								<small class="info help-block">
									<b>Input Bank Nama</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="bank_atasnama" class="col-sm-2 control-label">Nama Pemegang Rekening
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="bank_atasnama" id="bank_atasnama" placeholder="Nama Pemegang Rekening" value="<?= set_value('bank_atasnama', $pendapatan_kerjasama->bank_atasnama); ?>">
								<small class="info help-block">
									<b>Input Bank Atasnama</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="deskripsi" class="col-sm-2 control-label">Deskripsi
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<textarea id="deskripsi" name="deskripsi" rows="10" cols="80"> <?= set_value('deskripsi', $pendapatan_kerjasama->deskripsi); ?></textarea>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_pengantar" class="col-sm-2 control-label">File Pengantar
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="pendapatan_kerjasama_file_pengantar_galery"></div>
								<input class="data_file data_file_uuid" name="pendapatan_kerjasama_file_pengantar_uuid" id="pendapatan_kerjasama_file_pengantar_uuid" type="hidden" value="<?= set_value('pendapatan_kerjasama_file_pengantar_uuid'); ?>">
								<input class="data_file" name="pendapatan_kerjasama_file_pengantar_name" id="pendapatan_kerjasama_file_pengantar_name" type="hidden" value="<?= set_value('pendapatan_kerjasama_file_pengantar_name', $pendapatan_kerjasama->file_pengantar); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_sptjm" class="col-sm-2 control-label">File SPTJM
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="pendapatan_kerjasama_file_sptjm_galery"></div>
								<input class="data_file data_file_uuid" name="pendapatan_kerjasama_file_sptjm_uuid" id="pendapatan_kerjasama_file_sptjm_uuid" type="hidden" value="<?= set_value('pendapatan_kerjasama_file_sptjm_uuid'); ?>">
								<input class="data_file" name="pendapatan_kerjasama_file_sptjm_name" id="pendapatan_kerjasama_file_sptjm_name" type="hidden" value="<?= set_value('pendapatan_kerjasama_file_sptjm_name', $pendapatan_kerjasama->file_sptjm); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_rab" class="col-sm-2 control-label">File RAB
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="pendapatan_kerjasama_file_rab_galery"></div>
								<input class="data_file data_file_uuid" name="pendapatan_kerjasama_file_rab_uuid" id="pendapatan_kerjasama_file_rab_uuid" type="hidden" value="<?= set_value('pendapatan_kerjasama_file_rab_uuid'); ?>">
								<input class="data_file" name="pendapatan_kerjasama_file_rab_name" id="pendapatan_kerjasama_file_rab_name" type="hidden" value="<?= set_value('pendapatan_kerjasama_file_rab_name', $pendapatan_kerjasama->file_rab); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_pendukung" class="col-sm-2 control-label">File Pendukung
							</label>
							<div class="col-sm-8">
								<div id="pendapatan_kerjasama_file_pendukung_galery"></div>
								<input class="data_file data_file_uuid" name="pendapatan_kerjasama_file_pendukung_uuid" id="pendapatan_kerjasama_file_pendukung_uuid" type="hidden" value="<?= set_value('pendapatan_kerjasama_file_pendukung_uuid'); ?>">
								<input class="data_file" name="pendapatan_kerjasama_file_pendukung_name" id="pendapatan_kerjasama_file_pendukung_name" type="hidden" value="<?= set_value('pendapatan_kerjasama_file_pendukung_name', $pendapatan_kerjasama->file_pendukung); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="validasi" class="col-sm-2 control-label">Validasi
								<i class="required"></i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select" name="validasi" id="validasi" data-placeholder="Select Validasi">
									<option value=""></option>
									<option <?= $pendapatan_kerjasama->validasi == "Y" ? 'selected' : ''; ?> value="Y">Valid</option>
									<option <?= $pendapatan_kerjasama->validasi == "N" ? 'selected' : ''; ?> value="N">Invalid</option>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<script src="<?= BASE_ASSET; ?>ckeditor/ckeditor.js"></script>
<!-- Page script -->
<script>
	$(document).ready(function() {


		CKEDITOR.replace('deskripsi');
		var deskripsi = CKEDITOR.instances.deskripsi;

		$('#btn_cancel').click(function() {
			swal({
					title: "Are you sure?",
					text: "the data that you have created will be in the exhaust!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/pendapatan_kerjasama/index/' + "<?= $this->session->userdata('_page') ?>";
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();
			$('#deskripsi').val(deskripsi.getData());

			var form_pendapatan_kerjasama = $('#form_pendapatan_kerjasama');
			var data_post = form_pendapatan_kerjasama.serializeArray();
			var save_type = $(this).attr('data-stype');
			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: form_pendapatan_kerjasama.attr('action'),
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('form').find('.error-input').remove();
					$('.steps li').removeClass('error');
					if (res.success) {
						var id = $('#pendapatan_kerjasama_image_galery').find('li').attr('qq-file-id');
						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						$('.data_file_uuid').val('');

					} else {
						if (res.errors) {
							parseErrorField(res.errors);
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/

		var params = {};
		params[csrf] = token;

		$('#pendapatan_kerjasama_file_pengantar_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/pendapatan_kerjasama/upload_file_pengantar_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/pendapatan_kerjasama/delete_file_pengantar_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/pendapatan_kerjasama/get_file_pengantar_file/<?= $pendapatan_kerjasama->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#pendapatan_kerjasama_file_pengantar_galery').fineUploader('getUuid', id);
						$('#pendapatan_kerjasama_file_pengantar_uuid').val(uuid);
						$('#pendapatan_kerjasama_file_pengantar_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#pendapatan_kerjasama_file_pengantar_uuid').val();
					$.get(BASE_URL + '/administrator/pendapatan_kerjasama/delete_file_pengantar_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#pendapatan_kerjasama_file_pengantar_uuid').val('');
						$('#pendapatan_kerjasama_file_pengantar_name').val('');
					}
				}
			}
		}); /*end file_pengantar galey*/
		var params = {};
		params[csrf] = token;

		$('#pendapatan_kerjasama_file_sptjm_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/pendapatan_kerjasama/upload_file_sptjm_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/pendapatan_kerjasama/delete_file_sptjm_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/pendapatan_kerjasama/get_file_sptjm_file/<?= $pendapatan_kerjasama->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#pendapatan_kerjasama_file_sptjm_galery').fineUploader('getUuid', id);
						$('#pendapatan_kerjasama_file_sptjm_uuid').val(uuid);
						$('#pendapatan_kerjasama_file_sptjm_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#pendapatan_kerjasama_file_sptjm_uuid').val();
					$.get(BASE_URL + '/administrator/pendapatan_kerjasama/delete_file_sptjm_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#pendapatan_kerjasama_file_sptjm_uuid').val('');
						$('#pendapatan_kerjasama_file_sptjm_name').val('');
					}
				}
			}
		}); /*end file_sptjm galey*/
		var params = {};
		params[csrf] = token;

		$('#pendapatan_kerjasama_file_rab_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/pendapatan_kerjasama/upload_file_rab_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/pendapatan_kerjasama/delete_file_rab_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/pendapatan_kerjasama/get_file_rab_file/<?= $pendapatan_kerjasama->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#pendapatan_kerjasama_file_rab_galery').fineUploader('getUuid', id);
						$('#pendapatan_kerjasama_file_rab_uuid').val(uuid);
						$('#pendapatan_kerjasama_file_rab_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#pendapatan_kerjasama_file_rab_uuid').val();
					$.get(BASE_URL + '/administrator/pendapatan_kerjasama/delete_file_rab_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#pendapatan_kerjasama_file_rab_uuid').val('');
						$('#pendapatan_kerjasama_file_rab_name').val('');
					}
				}
			}
		}); /*end file_rab galey*/
		var params = {};
		params[csrf] = token;

		$('#pendapatan_kerjasama_file_pendukung_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/pendapatan_kerjasama/upload_file_pendukung_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/pendapatan_kerjasama/delete_file_pendukung_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/pendapatan_kerjasama/get_file_pendukung_file/<?= $pendapatan_kerjasama->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#pendapatan_kerjasama_file_pendukung_galery').fineUploader('getUuid', id);
						$('#pendapatan_kerjasama_file_pendukung_uuid').val(uuid);
						$('#pendapatan_kerjasama_file_pendukung_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#pendapatan_kerjasama_file_pendukung_uuid').val();
					$.get(BASE_URL + '/administrator/pendapatan_kerjasama/delete_file_pendukung_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#pendapatan_kerjasama_file_pendukung_uuid').val('');
						$('#pendapatan_kerjasama_file_pendukung_name').val('');
					}
				}
			}
		}); /*end file_pendukung galey*/

		function chained_unit_ks(selected, complete) {
			var val = $('#kd_prodi').val();
			$.LoadingOverlay('show')
			return $.ajax({
					url: BASE_URL + '/administrator/pendapatan_kerjasama/ajax_unit_ks/' + val,
					dataType: 'JSON',
					type: 'get',
					data:{
						unit_ks:'<?= $pendapatan_kerjasama->unit_ks ?>'
					}
				})
				.done(function(res) {
					var html = '<option value=""></option>';
					$.each(res, function(index, val) {
						html += '<option ' + (selected == val.id ? 'selected' : '') + ' value="' + val.id + '">' + val.nama_mitra + ' - ' + val.doc_nomor + '</option>'
					});
					$('#unit_ks').html(html);
					$('#unit_ks').trigger('chosen:updated');
					if (typeof complete != 'undefined') {
						complete();
					}

				})
				.fail(function() {
					toastr['error']('Error', 'Getting data fail')
				})
				.always(function() {
					$.LoadingOverlay('hide')
				});
		}

		$('#kd_prodi').change(function(event) {
			chained_unit_ks('')
		});
		async function chain() {
			await chained_unit_ks("<?= $pendapatan_kerjasama->unit_ks ?>");
		}
		chain();
		$(".money").maskMoney({
			thousands: '.',
			decimal: ',',
			allowZero: true,
			prefix: 'Rp ',
			precision: 0
		}).trigger('focus');
	}); /*end doc ready*/
</script>