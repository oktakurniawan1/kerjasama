<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
<form action="" method="POST" role="form" enctype="multipart/form-data" id='form-upload-sp2d'>
	<input type="hidden" name="id_pendapatan" value='<?= $pendapatan->id ?>'>
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name()?>" value="<?php echo $this->security->get_csrf_hash();?>"/>
	<div class="form-group">
		<label for="">File (pdf)</label>
		<input type="file" class="form-control" name="file_sp2d">
	</div>
	<div class="form-group">
		<label for="">Deskripsi</label>
		<input type="text" name="deskripsi" class="form-control" id="" placeholder="deskripsi singkat">
	</div>
	<button type="button" id='btn-upload' onclick="upload_sp2d()" class="btn btn-primary">Upload</button>
</form>
<div id="result" style="margin-top:5px;"></div>
<br>
<table class="table table-hover table-striped table-bordered" id='table1'>
	<thead>
		<tr>
			<th></th>
			<th></th>
			<th>Deskripsi</th>
			<th>File Sp2d</th>
			<th>Tanggal Upload</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
		</tr>
	</tbody>
</table>

<script>
	var oTable;
	$(function() {
		oTable = $('#table1').DataTable({
			processing: true,
			serverSide: true,
			pageLength: 5,
			lengthChange: false,
			//lengthMenu: [6, 10, 15, 25],
			ajax: {
				'url': '<?php echo site_url('administrator/pendapatan_kerjasama/browse_sp2d') ?>',
				'type': 'post',
				'data': {
					id_pendapatan: '<?=$pendapatan->id ?>',
					<?= csrf_token() ?>
				},
			},
			columns: [
				{data: 'id',visible: false,searchable: false,},
				{data: 'id_pendapatan',visible:false,searchable:false},
				{data: 'deskripsi'},
				{data: 'file_sp2d',render:function(data,row,type){
					return "<a target='_blank' href='<?=BASE_URL?>file/pendapatan_kerjasama-"+data+"'>"+data+"</a>"
				}},
				{data: 'tgl_upload'},
				{className: 'text-center',searchable: false,orderable: false,width: '30px'}
			],
			columnDefs: [{
				'targets': -1,
				'data': null,
				render: function(data, type, row) {
					return "<button class='btn btn-default btn-xs' onclick='hapus_sp2d(" + row.id + ")'><i class='fa fa-trash text-danger'></i></button> ";
				}
			}]
		})
		oTable.MakeCellsEditable({
			"onUpdate": update_data,
			"inputCss": 'form-control',
			'wrapperHtml': '<div class="alert alert-info">{content}</div>',
			"columns": [2],
			"confirmationButton": {
				"confirmCss": 'btn btn-success btn-xs',
				"cancelCss": 'btn btn-warning btn-xs'
			},
		});
	})

	function update_data(updatedCell, updatedRow, oldValue) {
		var url = '<?php echo site_url('administrator/pendapatan_kerjasama/update_sp2d') ?>';
		var data = updatedRow.data();
		data[csrf] = token;
		$.post(url, data, function(response) {
			oTable.draw(false);
		})
	}

	function upload_sp2d() {
		$.LoadingOverlay('show')
		var url = '<?php echo site_url('administrator/pendapatan_kerjasama/upload_file_sp2d') ?>';
		var form_data = new FormData($('#form-upload-sp2d')[0]);
		$.ajax({
			url: url,
			type: 'POST',
			data: form_data,
			success: function (response) {
				if (response.status) {
					$('#result').html(response.data)
					oTable.draw(false);
				}
				else{
					$('#result').html(response.data)
				}
				$.LoadingOverlay('hide');
			},
			cache: false,
			processData: false,
			contentType : false,
			dataType:'json'
		});
	}

	function hapus_sp2d(id) {
		if (confirm('Yakin akan dihapus?')) {
			var url = '<?php echo site_url('administrator/pendapatan_kerjasama/hapus_sp2d') ?>';
			$.get(url, {
				id: id
			}, function(response) {
				if (response.status > 0) {
					oTable.draw(false);
				}
			}, 'json')
		}
	}
</script>