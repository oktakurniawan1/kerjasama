<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<script type="text/javascript">
   //This page is a result of an autogenerated content made by running test.html with firefox.
   function domo() {

      // Binding keys
      $('*').bind('keydown', 'Ctrl+a', function assets() {
         window.location.href = BASE_URL + '/administrator/Karyawan/add';
         return false;
      });

      $('*').bind('keydown', 'Ctrl+f', function assets() {
         $('#sbtn').trigger('click');
         return false;
      });

      $('*').bind('keydown', 'Ctrl+x', function assets() {
         $('#reset').trigger('click');
         return false;
      });

      $('*').bind('keydown', 'Ctrl+b', function assets() {

         $('#reset').trigger('click');
         return false;
      });
   }

   jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
      <?= cclang('karyawan') ?><small><?= cclang('list_all'); ?></small>
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?= cclang('karyawan') ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">

      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                     <div class="row pull-right">
                     </div>
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username"><?= cclang('karyawan') ?></h3>
                     <h5 class="widget-user-desc"><?= cclang('list_all', [cclang('karyawan')]); ?> <i class="label bg-yellow"><?= $karyawan_counts; ?> <?= cclang('items'); ?></i></h5>
                  </div>

                  <form name="form_karyawan" id="form_karyawan" action="<?= base_url('administrator/karyawan/index'); ?>">
                     <div class="row">
                        <div class="col-md-8">
                           <?php is_allowed('karyawan_delete', function () { ?>
                              <div class="col-sm-2 padd-left-0 ">
                                 <select type="text" class="form-control chosen chosen-select" name="bulk" id="bulk" placeholder="Site Email">
                                    <option value="">Bulk</option>
                                    <option value="delete">Delete</option>
                                 </select>
                              </div>
                              <div class="col-sm-2 padd-left-0 ">
                                 <button type="button" class="btn btn-flat" name="apply" id="apply" title="<?= cclang('apply_bulk_action'); ?>"><?= cclang('apply_button'); ?></button>
                              </div>
                           <?php }) ?>
                           <div class="col-sm-3 padd-left-0  ">
                              <input type="text" class="form-control" name="q" id="filter" placeholder="<?= cclang('filter'); ?>" value="<?= $this->session->userdata('karyawan_filter'); ?>">
                           </div>
                           <div class="col-sm-3 padd-left-0 ">
                              <select type="text" class="form-control chosen chosen-select" name="f" id="field">
                                 <option value=""><?= cclang('all'); ?></option>
                                 <option <?= $this->session->userdata('karyawan_field') == 'nip' ? 'selected' : ''; ?> value="nip">NIP</option>
                                 <option <?= $this->session->userdata('karyawan_field') == 'nip_baru' ? 'selected' : ''; ?> value="nip_baru">Nip Baru</option>
                                 <option <?= $this->session->userdata('karyawan_field') == 'gelar_depan' ? 'selected' : ''; ?> value="gelar_depan">Gelar Depan</option>
                                 <option <?= $this->session->userdata('karyawan_field') == 'nama' ? 'selected' : ''; ?> value="nama">Nama</option>
                                 <option <?= $this->session->userdata('karyawan_field') == 'gelar_belakang' ? 'selected' : ''; ?> value="gelar_belakang">Gelar Belakang</option>
                                 <option <?= $this->session->userdata('karyawan_field') == 'dos_kar' ? 'selected' : ''; ?> value="dos_kar">Status Pegawai</option>
                                 <option <?= $this->session->userdata('karyawan_field') == 'email' ? 'selected' : ''; ?> value="email">Email</option>
                                 <option <?= $this->session->userdata('karyawan_field') == 'telp' ? 'selected' : ''; ?> value="telp">Telepon</option>
                                 <option <?= $this->session->userdata('karyawan_field') == 'nama_unit' ? 'selected' : ''; ?> value="nama_unit">Nama Unit</option>
                              </select>
                           </div>
                           <div class="col-sm-1 padd-left-0 ">
                              <button type="submit" class="btn btn-flat" name="sbtn" id="sbtn" value="Apply" title="<?= cclang('filter_search'); ?>">
                                 Filter
                              </button>
                           </div>
                           <div class="col-sm-1 padd-left-0 ">
                              <a class="btn btn-default btn-flat" name="reset" id="reset" value="Apply" href="<?= base_url('administrator/karyawan/?reset_filter='); ?>" title="<?= cclang('reset_filter'); ?>">
                                 <i class="fa fa-undo"></i>
                              </a>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="dataTables_paginate paging_simple_numbers pull-right" id="example2_paginate">
                              <?= $pagination; ?>
                           </div>
                        </div>
                     </div>
                     <div class="table-responsive">
                        <table class="table table-bordered table-striped dataTable">
                           <thead>
                              <tr class="">
                                 <th>
                                    <input type="checkbox" class="flat-red toltip" id="check_all" name="check_all" title="check all">
                                 </th>
                                 <th data-field="nip" data-sort="1" data-primary-key="0"> <?= cclang('nip') ?></th>
                                 <th data-field="nip_baru" data-sort="1" data-primary-key="0"> <?= cclang('nip_baru') ?></th>
                                 <th data-field="gelar_depan" data-sort="1" data-primary-key="0"> <?= cclang('gelar_depan') ?></th>
                                 <th data-field="nama" data-sort="1" data-primary-key="0"> <?= cclang('nama') ?></th>
                                 <th data-field="gelar_belakang" data-sort="1" data-primary-key="0"> <?= cclang('gelar_belakang') ?></th>
                                 <th data-field="dos_kar" data-sort="1" data-primary-key="0"> <?= cclang('dos_kar') ?></th>
                                 <th data-field="email" data-sort="1" data-primary-key="0"> <?= cclang('email') ?></th>
                                 <th data-field="telp" data-sort="1" data-primary-key="0"> <?= cclang('telp') ?></th>
                                 <th data-field="nama_unit" data-sort="1" data-primary-key="0"> <?= cclang('nama_unit') ?></th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody id="tbody_karyawan">
                              <?php foreach ($karyawans as $karyawan) : ?>
                                 <tr>
                                    <td width="5">
                                       <input type="checkbox" class="flat-red check" name="id[]" value="<?= $karyawan->id; ?>">
                                    </td>

                                    <td><?= _ent($karyawan->nip); ?></td>
                                    <td><?= _ent($karyawan->nip_baru); ?></td>
                                    <td><?= _ent($karyawan->gelar_depan); ?></td>
                                    <td><?= _ent($karyawan->nama); ?></td>
                                    <td><?= _ent($karyawan->gelar_belakang); ?></td>
                                    <td><?= _ent($karyawan->dos_kar); ?></td>
                                    <td><?= _ent($karyawan->email); ?></td>
                                    <td><?= _ent($karyawan->telp); ?></td>
                                    <td><?= _ent($karyawan->nama_unit); ?></td>
                                    <td width="120">

                                       <?php is_allowed('karyawan_view', function () use ($karyawan) { ?>
                                          <a href="<?= site_url('administrator/karyawan/view/' . $karyawan->id); ?>" class="btn btn-default btn-xs"><i class="fa fa-eye text-info"></i></a>
                                       <?php }) ?>
                                       <?php is_allowed('karyawan_delete', function () use ($karyawan) { ?>
                                          <a href="javascript:void(0);" data-href="<?= site_url('administrator/karyawan/delete/' . $karyawan->id); ?>" class="btn btn-default btn-xs remove-data"><i class="fa fa-trash fa-lg text-danger"></i></a>
                                       <?php }) ?>

                                    </td>
                                 </tr>
                              <?php endforeach; ?>
                              <?php if ($karyawan_counts == 0) : ?>
                                 <tr>
                                    <td colspan="100">
                                       Pegawai Unnes data is not available
                                    </td>
                                 </tr>
                              <?php endif; ?>

                           </tbody>
                        </table>
                     </div>
               </div>
               <!-- /.widget-user -->
               <hr>
               </form>
               <div class="row">
                  <div class="col-md-4 col-md-offset-8">
                     <div class="dataTables_paginate paging_simple_numbers pull-right" id="example2_paginate">
                        <?= $pagination; ?>
                     </div>
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->

<!-- Page script -->
<script>
   $(document).ready(function() {

      $('.remove-data').click(function() {

         var url = $(this).attr('data-href');

         swal({
               title: "<?= cclang('are_you_sure'); ?>",
               text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#DD6B55",
               confirmButtonText: "<?= cclang('yes_delete_it'); ?>",
               cancelButtonText: "<?= cclang('no_cancel_plx'); ?>",
               closeOnConfirm: true,
               closeOnCancel: true
            },
            function(isConfirm) {
               if (isConfirm) {
                  document.location.href = url;
               }
            });

         return false;
      });


      $('#apply').click(function() {

         var bulk = $('#bulk');
         var serialize_bulk = $('#form_karyawan').serialize();

         if (bulk.val() == 'delete') {
            swal({
                  title: "<?= cclang('are_you_sure'); ?>",
                  text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "<?= cclang('yes_delete_it'); ?>",
                  cancelButtonText: "<?= cclang('no_cancel_plx'); ?>",
                  closeOnConfirm: true,
                  closeOnCancel: true
               },
               function(isConfirm) {
                  if (isConfirm) {
                     document.location.href = BASE_URL + '/administrator/karyawan/delete?' + serialize_bulk;
                  }
               });

            return false;

         } else if (bulk.val() == '') {
            swal({
               title: "Upss",
               text: "<?= cclang('please_choose_bulk_action_first'); ?>",
               type: "warning",
               showCancelButton: false,
               confirmButtonColor: "#DD6B55",
               confirmButtonText: "Okay!",
               closeOnConfirm: true,
               closeOnCancel: true
            });

            return false;
         }

         return false;

      }); /*end appliy click*/


      //check all
      var checkAll = $('#check_all');
      var checkboxes = $('input.check');

      checkAll.on('ifChecked ifUnchecked', function(event) {
         if (event.type == 'ifChecked') {
            checkboxes.iCheck('check');
         } else {
            checkboxes.iCheck('uncheck');
         }
      });

      checkboxes.on('ifChanged', function(event) {
         if (checkboxes.filter(':checked').length == checkboxes.length) {
            checkAll.prop('checked', 'checked');
         } else {
            checkAll.removeProp('checked');
         }
         checkAll.iCheck('update');
      });
      initSortable('karyawan', $('table.dataTable'));
   }); /*end doc ready*/
</script>