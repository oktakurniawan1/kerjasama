<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['karyawan'] = 'Pegawai Unnes';
$lang['id'] = 'Id';
$lang['nip'] = 'NIP';
$lang['nip_baru'] = 'Nip Baru';
$lang['gelar_depan'] = 'Gelar Depan';
$lang['nama'] = 'Nama';
$lang['gelar_belakang'] = 'Gelar Belakang';
$lang['status_krj'] = 'Status Kerja';
$lang['dos_kar'] = 'Status Pegawai';
$lang['email'] = 'Email';
$lang['telp'] = 'Telepon';
$lang['kode_unit'] = 'Kode Unit';
$lang['nama_unit'] = 'Nama Unit';
$lang['lahir_tmp'] = 'Tempat Lahir';
$lang['lahir_tgl'] = 'Tanggal Lahir';
