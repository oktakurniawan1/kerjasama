<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 *| --------------------------------------------------------------------------
 *| Karyawan Controller
 *| --------------------------------------------------------------------------
 *| Karyawan site
 *|
 */
class Karyawan extends Admin
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_karyawan');
		$this->lang->load('web_lang', $this->current_lang);
	}
	/**
	 * show all Karyawans
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('karyawan_list');
		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('karyawan_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('karyawan_field', $f);
		$this->session->set_userdata('_page', $offset);
		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('karyawan_filter');
			$this->session->unset_userdata('karyawan_field');
		}
		$filter = $this->session->userdata('karyawan_filter');
		$field = $this->session->userdata('karyawan_field');
		$offset = $this->session->userdata('_page', $offset);
		$this->data['karyawans'] = $this->model_karyawan->get($filter, $field, $this->limit_page, $offset);
		$this->data['karyawan_counts'] = $this->model_karyawan->count_all($filter, $field);
		$config = [
			'base_url'     => 'administrator/karyawan/index/',
			'total_rows'   => $this->model_karyawan->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];
		$this->data['pagination'] = $this->pagination($config);
		$this->template->title('Karyawan List');
		$this->render('backend/standart/administrator/karyawan/karyawan_list', $this->data);
	}
	/**
	 * delete Karyawans
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('karyawan_delete');
		$this->load->helper('file');
		$arr_id = $this->input->get('id');
		$remove = false;
		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}
		if ($remove) {
			set_message(cclang('has_been_deleted', 'karyawan'), 'success');
		} else {
			set_message(cclang('error_delete', 'karyawan'), 'error');
		}
		redirect_back();
	}
	/**
	 * View view Karyawans
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('karyawan_view');
		$this->data['karyawan'] = $this->model_karyawan->join_avaiable()->filter_avaiable()->find($id);
		$this->template->title('Karyawan Detail');
		$this->render('backend/standart/administrator/karyawan/karyawan_view', $this->data);
	}
	/**
	 * delete Karyawans
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$karyawan = $this->model_karyawan->find($id);
		return $this->model_karyawan->remove($id);
	}
	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('karyawan_export');
		$this->model_karyawan->export(
			'karyawan',
			'karyawan',
			$this->model_karyawan->field_search
		);
	}
	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('karyawan_export');

		$this->model_karyawan->pdf('karyawan', 'karyawan');
	}

	public function single_pdf($id = null)
	{
		$this->is_allowed('karyawan_export');

		$table = $title = 'karyawan';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_karyawan->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
	public function form_cari_pegawai()
	{
		$data['pegawai'] = $this->model_karyawan->get_karyawan();
		$this->load->view('_cari_pegawai',$data);
	}
}


/* End of file karyawan.php */
/* Location: ./application/controllers/administrator/Karyawan.php */
