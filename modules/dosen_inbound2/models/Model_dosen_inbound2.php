<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_dosen_inbound2 extends MY_Model
{

    private $primary_key    = 'id';
    private $table_name     = 'dosen_inbound2';
    public $field_search   = ['scope', 'kd_prodi', 'keg_dosin_id', 'tgs_dosin_id', 'judul_kegiatan', 'nama_dosin', 'nomor_identitas', 'institusi_asal', 'unit_ks', 'negara', 'kode_dosen', 'validasi'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
        );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "dosen_inbound2." . $field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "dosen_inbound2." . $field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '(' . $where . ')';
        } else {
            $where .= "(" . "dosen_inbound2." . $field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "dosen_inbound2." . $field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "dosen_inbound2." . $field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '(' . $where . ')';
        } else {
            $where .= "(" . "dosen_inbound2." . $field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) and count($select_field)) {
            $this->db->select($select_field);
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);

        $this->sortable();

        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable()
    {
        $this->db->join('ref_prodi', 'ref_prodi.kode = dosen_inbound2.kd_prodi', 'LEFT');
        $this->db->join('dosin_keg', 'dosin_keg.id = dosen_inbound2.keg_dosin_id', 'LEFT');
        $this->db->join('dosin_tugas', 'dosin_tugas.id = dosen_inbound2.tgs_dosin_id', 'LEFT');
        $this->db->join('mou_kerjasama', 'mou_kerjasama.id = dosen_inbound2.mou_ks', 'LEFT');
        $this->db->join('si_kerjasama', 'si_kerjasama.id = dosen_inbound2.unit_ks', 'LEFT');
        $this->db->join('negara', 'negara.nama_negara = dosen_inbound2.negara', 'LEFT');

        $this->db->select('dosen_inbound2.*,ref_prodi.nama_prodi as ref_prodi_nama_prodi,dosin_keg.kegiatan as dosin_keg_kegiatan,dosin_tugas.tugas as dosin_tugas_tugas,mou_kerjasama.nama_mitra as mou_kerjasama_nama_mitra,si_kerjasama.nama_mitra as si_kerjasama_nama_mitra,negara.nama_negara as negara_nama_negara');


        return $this;
    }

    public function filter_avaiable()
    {

        /*
        if (!$this->aauth->is_admin()) {
            }

        return $this;
        */
        $prodi = [];
        if (is_member('Kaprodi')) {
            if (is_non_user()) $prodi[] = fromsess('kd_prodi');
            else {
                $prodi = get_user_data('prodi');
                $prodi = explode(',', $prodi);
            }
            $this->db->where_in('dosen_inbound2.kd_prodi', $prodi);
        } else if (is_member('Unit')) {
            if (is_non_user()) $prodi[] = fromsess('kd_prodi');
            else {
                $prodi = get_user_data('prodi');
                $prodi = explode(',', $prodi);
            }
            $this->db->where_in('dosen_inbound2.kd_prodi', $prodi);
        }
        return $this;
    }
}

/* End of file Model_dosen_inbound2.php */
/* Location: ./application/models/Model_dosen_inbound2.php */