<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Dosen Inbound <small>Edit Dosen Inbound</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/dosen_inbound2'); ?>">Dosen Inbound</a></li>
		<li class="active">Edit</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Dosen Inbound</h3>
							<h5 class="widget-user-desc">Edit Dosen Inbound</h5>
							<hr>
						</div>
						<?= form_open(base_url('administrator/dosen_inbound2/edit_save/' . $this->uri->segment(4)), [
							'name'    => 'form_dosen_inbound2',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_dosen_inbound2',
							'method'  => 'POST'
						]); ?>


						<div class="form-group ">
							<label for="aktivitas" class="col-sm-2 control-label">Jenis Aktivitas
								<i class="required"><?=(find_rules('aktivitas', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<input type="hidden" name="aktivitas" value=<?= $dosen_inbound2->aktivitas ?>>
								<select class="form-control chosen chosen-select" name="aktivitas" id="aktivitas" data-placeholder="Select Jenis Aktivitas" disabled>
									<option value=""></option>
									<option <?= $dosen_inbound2->aktivitas == "SA" ? 'selected' : ''; ?> value="SA">Short Activities</option>
									<option <?= $dosen_inbound2->aktivitas == "LA" ? 'selected' : ''; ?> value="LA">Long Activities</option>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="scope" class="col-sm-2 control-label">DN/LN
								<i class="required"><?=(find_rules('scope', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<input type="hidden" name="scope" value="<?= $dosen_inbound2->scope ?>">
								<select class="form-control chosen chosen-select" name="scope" id="scope" data-placeholder="Select DN/LN" disabled>
									<option value=""></option>
									<option <?= $dosen_inbound2->scope == "DN" ? 'selected' : ''; ?> value="DN">Dalam Negeri</option>
									<option <?= $dosen_inbound2->scope == "LN" ? 'selected' : ''; ?> value="LN">Luar Negeri</option>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="kd_prodi" class="col-sm-2 control-label">Program Studi/Unit
								<i class="required"><?=(find_rules('kd_prodi', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="kd_prodi" id="kd_prodi" data-placeholder="Select Program Studi/Unit">
									<option value=""></option>
									<?php foreach ($filter_prodi as $row) : ?>
										<option <?= $row->kode ==  $dosen_inbound2->kd_prodi ? 'selected' : ''; ?> value="<?= $row->kode ?>"><?= $row->kode .' - '.$row->nama_prodi; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="keg_dosin_id" class="col-sm-2 control-label">Jenis Kegiatan
								<i class="required"><?=(find_rules('keg_dosin_id', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="keg_dosin_id" id="keg_dosin_id" data-placeholder="Select Jenis Kegiatan">
									<option value=""></option>
									<?php foreach (db_get_all_data('dosin_keg', ['aktivitas' => 'SA']) as $row) : ?>
										<option <?= $row->id ==  $dosen_inbound2->keg_dosin_id ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->kegiatan; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgs_dosin_id" class="col-sm-2 control-label">Tugas Sebagai
								<i class="required"><?=(find_rules('tgs_dosin_id', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="tgs_dosin_id" id="tgs_dosin_id" data-placeholder="Select Tugas Sebagai">
									<option value=""></option>
									<?php foreach (db_get_all_data('dosin_tugas', ['aktivitas' => 'SA']) as $row) : ?>
										<option <?= $row->id ==  $dosen_inbound2->tgs_dosin_id ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->tugas; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="judul_kegiatan" class="col-sm-2 control-label">Judul Kegiatan
								<i class="required"><?=(find_rules('judul_kegiatan', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="judul_kegiatan" id="judul_kegiatan" placeholder="Judul Kegiatan" value="<?= set_value('judul_kegiatan', $dosen_inbound2->judul_kegiatan); ?>">
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_mulai" class="col-sm-2 control-label">Tanggal Mulai
								<i class="required"><?=(find_rules('tgl_mulai', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-8">
									<input type="text" class="form-control pull-right datepicker" name="tgl_mulai" placeholder="Tanggal Mulai" id="tgl_mulai" value="<?= set_value('dosen_inbound2_tgl_mulai_name', $dosen_inbound2->tgl_mulai); ?>">
								</div>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_selesai" class="col-sm-2 control-label">Tanggal Selesai
								<i class="required"><?=(find_rules('tgl_selesai', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-8">
									<input type="text" class="form-control pull-right datepicker" name="tgl_selesai" placeholder="Tanggal Selesai" id="tgl_selesai" value="<?= set_value('dosen_inbound2_tgl_selesai_name', $dosen_inbound2->tgl_selesai); ?>">
								</div>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nama_dosin" class="col-sm-2 control-label">Nama Dosen/Tamu
								<i class="required"><?=(find_rules('nama_dosin', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nama_dosin" id="nama_dosin" placeholder="Nama Dosen/Tamu" value="<?= set_value('nama_dosin', $dosen_inbound2->nama_dosin); ?>">
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nomor_identitas" class="col-sm-2 control-label">No Identitas (KTP/SIM/NIP/PASPOR)
								<i class="required"><?=(find_rules('nomor_identitas', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nomor_identitas" id="nomor_identitas" placeholder="No Identitas (KTP/SIM/NIP/PASPOR)" value="<?= set_value('nomor_identitas', $dosen_inbound2->nomor_identitas); ?>">
								<small class="info help-block">
									<b>Input Nomor Identitas</b> Max Length : 100.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nomor_identitas" class="col-sm-2 control-label">NIDN (Nomor Induk Dosen Nasional)
								<i class="required"><?=(find_rules('nidn', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nidn" id="nidn" placeholder="Nomor Induk Dosen Nasional" value="<?= set_value('nidn', $dosen_inbound2->nidn); ?>">
								<small class="info help-block">
									<b>Input NIDN</b> Max Length : 10.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="institusi_asal" class="col-sm-2 control-label">Institusi Asal
								<i class="required"><?=(find_rules('institusi_asal', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="institusi_asal" id="institusi_asal" placeholder="Institusi Asal" value="<?= set_value('institusi_asal', $dosen_inbound2->institusi_asal); ?>">
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="mou_ks" class="col-sm-2 control-label">Nota Kesepahaman (MoU)
								<i class="required"><?=(find_rules('mou_ks', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="mou_ks" id="mou_ks" data-placeholder="Select Nota Kesepahaman (MoU)">
									<option value=""></option>
									<?php foreach ($list_mou as $row) : ?>
										<option <?= $row->id ==  $dosen_inbound2->mou_ks ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->nama_mitra . ' - ' . $row->doc_nomor; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									Pilih MoU, jika DLB/tidak ada dalam pilihan silahkan pilih <b>PENGAJUAN BARU</b></small>
							</div>
						</div>

						<div class="form-group ">
							<label for="unit_ks" class="col-sm-2 control-label">MoA/IA/LoA (Unit) / SK DLB
								<i class="required"><?=(find_rules('unit_ks', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="unit_ks" id="unit_ks" data-placeholder="Select MoA/IA/LoA (Unit)">
									<option value=""></option>
									<?php foreach ($list_moa as $row) : ?>
										<option <?= $row->id ==  $dosen_inbound2->unit_ks ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->nama_mitra . ' - ' . $row->doc_nomor; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="negara" class="col-sm-2 control-label">Negara
								<i class="required"><?=(find_rules('negara', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="negara" id="negara" data-placeholder="Select Negara">
									<option value=""></option>
									<?php foreach (db_get_all_data('negara') as $row) : ?>
										<option <?= $row->nama_negara ==  $dosen_inbound2->negara ? 'selected' : ''; ?> value="<?= $row->nama_negara ?>"><?= $row->nama_negara; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_cv" class="col-sm-2 control-label">File CV
								<i class="required"><?=(find_rules('dosen_inbound2_file_cv_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound2_file_cv_galery"></div>
								<input class="data_file data_file_uuid" name="dosen_inbound2_file_cv_uuid" id="dosen_inbound2_file_cv_uuid" type="hidden" value="<?= set_value('dosen_inbound2_file_cv_uuid'); ?>">
								<input class="data_file" name="dosen_inbound2_file_cv_name" id="dosen_inbound2_file_cv_name" type="hidden" value="<?= set_value('dosen_inbound2_file_cv_name', $dosen_inbound2->file_cv); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_loa" class="col-sm-2 control-label">File LoA
								<i class="required"><?=(find_rules('dosen_inbound2_file_loa_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound2_file_loa_galery"></div>
								<input class="data_file data_file_uuid" name="dosen_inbound2_file_loa_uuid" id="dosen_inbound2_file_loa_uuid" type="hidden" value="<?= set_value('dosen_inbound2_file_loa_uuid'); ?>">
								<input class="data_file" name="dosen_inbound2_file_loa_name" id="dosen_inbound2_file_loa_name" type="hidden" value="<?= set_value('dosen_inbound2_file_loa_name', $dosen_inbound2->file_loa); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_foto" class="col-sm-2 control-label">File Foto
								<i class="required"><?=(find_rules('dosen_inbound2_file_foto_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound2_file_foto_galery"></div>
								<input class="data_file data_file_uuid" name="dosen_inbound2_file_foto_uuid" id="dosen_inbound2_file_foto_uuid" type="hidden" value="<?= set_value('dosen_inbound2_file_foto_uuid'); ?>">
								<input class="data_file" name="dosen_inbound2_file_foto_name" id="dosen_inbound2_file_foto_name" type="hidden" value="<?= set_value('dosen_inbound2_file_foto_name', $dosen_inbound2->file_foto); ?>">
								<small class="info help-block">
									<b>Extension file must</b> JPG,PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_jadwal_keg" class="col-sm-2 control-label">File Jadwal Kegiatan
								<i class="required"><?=(find_rules('dosen_inbound2_file_jadwal_keg_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound2_file_jadwal_keg_galery"></div>
								<input class="data_file data_file_uuid" name="dosen_inbound2_file_jadwal_keg_uuid" id="dosen_inbound2_file_jadwal_keg_uuid" type="hidden" value="<?= set_value('dosen_inbound2_file_jadwal_keg_uuid'); ?>">
								<input class="data_file" name="dosen_inbound2_file_jadwal_keg_name" id="dosen_inbound2_file_jadwal_keg_name" type="hidden" value="<?= set_value('dosen_inbound2_file_jadwal_keg_name', $dosen_inbound2->file_jadwal_keg); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_paspor" class="col-sm-2 control-label">File Paspor
								<i class="required"><?=(find_rules('dosen_inbound2_file_paspor_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound2_file_paspor_galery"></div>
								<input class="data_file data_file_uuid" name="dosen_inbound2_file_paspor_uuid" id="dosen_inbound2_file_paspor_uuid" type="hidden" value="<?= set_value('dosen_inbound2_file_paspor_uuid'); ?>">
								<input class="data_file" name="dosen_inbound2_file_paspor_name" id="dosen_inbound2_file_paspor_name" type="hidden" value="<?= set_value('dosen_inbound2_file_paspor_name', $dosen_inbound2->file_paspor); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_tiket_kepulangan" class="col-sm-2 control-label">File Tiket Kepulangan
								<i class="required"><?=(find_rules('dosen_inbound2_file_tiket_kepulangan_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound2_file_tiket_kepulangan_galery"></div>
								<input class="data_file data_file_uuid" name="dosen_inbound2_file_tiket_kepulangan_uuid" id="dosen_inbound2_file_tiket_kepulangan_uuid" type="hidden" value="<?= set_value('dosen_inbound2_file_tiket_kepulangan_uuid'); ?>">
								<input class="data_file" name="dosen_inbound2_file_tiket_kepulangan_name" id="dosen_inbound2_file_tiket_kepulangan_name" type="hidden" value="<?= set_value('dosen_inbound2_file_tiket_kepulangan_name', $dosen_inbound2->file_tiket_kepulangan); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_asuransi_kesehatan" class="col-sm-2 control-label">File Asuransi Kesehatan
								<i class="required"><?=(find_rules('dosen_inbound2_file_asuransi_kesehatan_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound2_file_asuransi_kesehatan_galery"></div>
								<input class="data_file data_file_uuid" name="dosen_inbound2_file_asuransi_kesehatan_uuid" id="dosen_inbound2_file_asuransi_kesehatan_uuid" type="hidden" value="<?= set_value('dosen_inbound2_file_asuransi_kesehatan_uuid'); ?>">
								<input class="data_file" name="dosen_inbound2_file_asuransi_kesehatan_name" id="dosen_inbound2_file_asuransi_kesehatan_name" type="hidden" value="<?= set_value('dosen_inbound2_file_asuransi_kesehatan_name', $dosen_inbound2->file_asuransi_kesehatan); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_bank_stantement" class="col-sm-2 control-label">File Bank Stantement
								<i class="required"><?=(find_rules('dosen_inbound2_file_bank_stantement_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound2_file_bank_stantement_galery"></div>
								<input class="data_file data_file_uuid" name="dosen_inbound2_file_bank_stantement_uuid" id="dosen_inbound2_file_bank_stantement_uuid" type="hidden" value="<?= set_value('dosen_inbound2_file_bank_stantement_uuid'); ?>">
								<input class="data_file" name="dosen_inbound2_file_bank_stantement_name" id="dosen_inbound2_file_bank_stantement_name" type="hidden" value="<?= set_value('dosen_inbound2_file_bank_stantement_name', $dosen_inbound2->file_bank_stantement); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_materi_paparan" class="col-sm-2 control-label">File Materi Paparan
								<i class="required"><?=(find_rules('dosen_inbound2_file_materi_paparan_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound2_file_materi_paparan_galery"></div>
								<input class="data_file data_file_uuid" name="dosen_inbound2_file_materi_paparan_uuid" id="dosen_inbound2_file_materi_paparan_uuid" type="hidden" value="<?= set_value('dosen_inbound2_file_materi_paparan_uuid'); ?>">
								<input class="data_file" name="dosen_inbound2_file_materi_paparan_name" id="dosen_inbound2_file_materi_paparan_name" type="hidden" value="<?= set_value('dosen_inbound2_file_materi_paparan_name', $dosen_inbound2->file_materi_paparan); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 10240 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="deskripsi" class="col-sm-2 control-label">Deskripsi
							</label>
							<div class="col-sm-8">
								<textarea id="deskripsi" name="deskripsi" rows="5" class="textarea form-control"><?= set_value('deskripsi', $dosen_inbound2->deskripsi); ?></textarea>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="kode_dosen" class="col-sm-2 control-label">Kode Dosen
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="kode_dosen" id="kode_dosen" placeholder="XXXX" value="<?= set_value('kode_dosen', $dosen_inbound2->kode_dosen); ?>" disabled>
								<small class="info help-block">
									<b>Kode Dosen akan update otomatis setelah data divalidasi.</b></small>
							</div>
						</div>

						<?php if (is_groups_in(['Admin', 'Kerjasama'])) : ?>
							<div class="form-group ">
								<label for="validasi" class="col-sm-2 control-label">Validasi
								</label>
								<div class="col-sm-8">
									<select class="form-control chosen chosen-select" name="validasi" id="validasi" data-placeholder="Select Validasi">
										<option value=""></option>
										<option <?= $dosen_inbound2->validasi == "Y" ? 'selected' : ''; ?> value="Y">Valid</option>
										<option <?= $dosen_inbound2->validasi == "T" ? 'selected' : ''; ?> value="T">Invalid</option>
									</select>
									<small class="info help-block">
									</small>
								</div>
							</div>
						<?php endif ?>

						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	$(document).ready(function() {

		$('#mou_ks').change(function(e) {
			var mou_ks = e.target.value;
			$.get('<?= site_url('administrator/si_kerjasama/get_option_moa') ?>', {
				mou_ks: mou_ks,
				kd_prodi: $('select[name=kd_prodi] option:selected').val()
			}, function(page) {
				$('#unit_ks').html(page).val('').trigger('chosen:updated');
			});
		});

		$('#kd_prodi').change(function() {
			$('#mou_ks').trigger('change');
		})

		$('#btn_cancel').click(function() {
			swal({
					title: "Are you sure?",
					text: "the data that you have created will be in the exhaust!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/dosen_inbound2/index/' + "<?= $this->session->userdata('_page') ?>";
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();

			var form_dosen_inbound2 = $('#form_dosen_inbound2');
			var data_post = form_dosen_inbound2.serializeArray();
			var save_type = $(this).attr('data-stype');
			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: form_dosen_inbound2.attr('action'),
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('form').find('.error-input').remove();
					$('.steps li').removeClass('error');
					if (res.success) {
						var id = $('#dosen_inbound2_image_galery').find('li').attr('qq-file-id');
						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						$('.data_file_uuid').val('');

					} else {
						if (res.errors) {
							parseErrorField(res.errors);
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/

		var params = {};
		params[csrf] = token;

		$('#dosen_inbound2_file_cv_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound2/upload_file_cv_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/dosen_inbound2/delete_file_cv_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/dosen_inbound2/get_file_cv_file/<?= $dosen_inbound2->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound2_file_cv_galery').fineUploader('getUuid', id);
						$('#dosen_inbound2_file_cv_uuid').val(uuid);
						$('#dosen_inbound2_file_cv_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound2_file_cv_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound2/delete_file_cv_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound2_file_cv_uuid').val('');
						$('#dosen_inbound2_file_cv_name').val('');
					}
				}
			}
		}); /*end file_cv galey*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound2_file_loa_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound2/upload_file_loa_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/dosen_inbound2/delete_file_loa_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/dosen_inbound2/get_file_loa_file/<?= $dosen_inbound2->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound2_file_loa_galery').fineUploader('getUuid', id);
						$('#dosen_inbound2_file_loa_uuid').val(uuid);
						$('#dosen_inbound2_file_loa_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound2_file_loa_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound2/delete_file_loa_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound2_file_loa_uuid').val('');
						$('#dosen_inbound2_file_loa_name').val('');
					}
				}
			}
		}); /*end file_loa galey*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound2_file_foto_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound2/upload_file_foto_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/dosen_inbound2/delete_file_foto_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/dosen_inbound2/get_file_foto_file/<?= $dosen_inbound2->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["jpg", "pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound2_file_foto_galery').fineUploader('getUuid', id);
						$('#dosen_inbound2_file_foto_uuid').val(uuid);
						$('#dosen_inbound2_file_foto_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound2_file_foto_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound2/delete_file_foto_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound2_file_foto_uuid').val('');
						$('#dosen_inbound2_file_foto_name').val('');
					}
				}
			}
		}); /*end file_foto galey*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound2_file_jadwal_keg_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound2/upload_file_jadwal_keg_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/dosen_inbound2/delete_file_jadwal_keg_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/dosen_inbound2/get_file_jadwal_keg_file/<?= $dosen_inbound2->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound2_file_jadwal_keg_galery').fineUploader('getUuid', id);
						$('#dosen_inbound2_file_jadwal_keg_uuid').val(uuid);
						$('#dosen_inbound2_file_jadwal_keg_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound2_file_jadwal_keg_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound2/delete_file_jadwal_keg_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound2_file_jadwal_keg_uuid').val('');
						$('#dosen_inbound2_file_jadwal_keg_name').val('');
					}
				}
			}
		}); /*end file_jadwal_keg galey*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound2_file_paspor_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound2/upload_file_paspor_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/dosen_inbound2/delete_file_paspor_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/dosen_inbound2/get_file_paspor_file/<?= $dosen_inbound2->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound2_file_paspor_galery').fineUploader('getUuid', id);
						$('#dosen_inbound2_file_paspor_uuid').val(uuid);
						$('#dosen_inbound2_file_paspor_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound2_file_paspor_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound2/delete_file_paspor_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound2_file_paspor_uuid').val('');
						$('#dosen_inbound2_file_paspor_name').val('');
					}
				}
			}
		}); /*end file_paspor galey*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound2_file_tiket_kepulangan_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound2/upload_file_tiket_kepulangan_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/dosen_inbound2/delete_file_tiket_kepulangan_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/dosen_inbound2/get_file_tiket_kepulangan_file/<?= $dosen_inbound2->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound2_file_tiket_kepulangan_galery').fineUploader('getUuid', id);
						$('#dosen_inbound2_file_tiket_kepulangan_uuid').val(uuid);
						$('#dosen_inbound2_file_tiket_kepulangan_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound2_file_tiket_kepulangan_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound2/delete_file_tiket_kepulangan_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound2_file_tiket_kepulangan_uuid').val('');
						$('#dosen_inbound2_file_tiket_kepulangan_name').val('');
					}
				}
			}
		}); /*end file_tiket_kepulangan galey*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound2_file_asuransi_kesehatan_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound2/upload_file_asuransi_kesehatan_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/dosen_inbound2/delete_file_asuransi_kesehatan_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/dosen_inbound2/get_file_asuransi_kesehatan_file/<?= $dosen_inbound2->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound2_file_asuransi_kesehatan_galery').fineUploader('getUuid', id);
						$('#dosen_inbound2_file_asuransi_kesehatan_uuid').val(uuid);
						$('#dosen_inbound2_file_asuransi_kesehatan_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound2_file_asuransi_kesehatan_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound2/delete_file_asuransi_kesehatan_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound2_file_asuransi_kesehatan_uuid').val('');
						$('#dosen_inbound2_file_asuransi_kesehatan_name').val('');
					}
				}
			}
		}); /*end file_asuransi_kesehatan galey*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound2_file_bank_stantement_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound2/upload_file_bank_stantement_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/dosen_inbound2/delete_file_bank_stantement_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/dosen_inbound2/get_file_bank_stantement_file/<?= $dosen_inbound2->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound2_file_bank_stantement_galery').fineUploader('getUuid', id);
						$('#dosen_inbound2_file_bank_stantement_uuid').val(uuid);
						$('#dosen_inbound2_file_bank_stantement_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound2_file_bank_stantement_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound2/delete_file_bank_stantement_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound2_file_bank_stantement_uuid').val('');
						$('#dosen_inbound2_file_bank_stantement_name').val('');
					}
				}
			}
		}); /*end file_bank_stantement galey*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound2_file_materi_paparan_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound2/upload_file_materi_paparan_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/dosen_inbound2/delete_file_materi_paparan_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/dosen_inbound2/get_file_materi_paparan_file/<?= $dosen_inbound2->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 10485760,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound2_file_materi_paparan_galery').fineUploader('getUuid', id);
						$('#dosen_inbound2_file_materi_paparan_uuid').val(uuid);
						$('#dosen_inbound2_file_materi_paparan_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound2_file_materi_paparan_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound2/delete_file_materi_paparan_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound2_file_materi_paparan_uuid').val('');
						$('#dosen_inbound2_file_materi_paparan_name').val('');
					}
				}
			}
		}); /*end file_materi_paparan galey*/
		async function chain() {}
		chain();

	}); /*end doc ready*/
</script>