<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Dosen Inbound2 Controller
 *| --------------------------------------------------------------------------
 *| Dosen Inbound2 site
 *|
 */
class Dosen_inbound2 extends Admin
{

	protected $validation_rules = [];

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_dosen_inbound2');
		$this->load->model('ref_prodi/Model_ref_prodi', 'model_ref_prodi');
		$this->lang->load('web_lang', $this->current_lang);

		$this->validation_rules = [
			['field' => 'scope', 'label' => 'DN/LN', 'rules' => 'trim|required'],
			['field' => 'aktivitas', 'label' => 'Jenis Aktivitas', 'rules' => 'trim|required'],
			['field' => 'kd_prodi', 'label' => 'Program Studi/Unit', 'rules' => 'trim|required'],
			['field' => 'keg_dosin_id', 'label' => 'Jenis Kegiatan', 'rules' => 'trim|required'],
			['field' => 'tgs_dosin_id', 'label' => 'Tugas Sebagai', 'rules' => 'trim|required'],
			['field' => 'judul_kegiatan', 'label' => 'Judul Kegiatan', 'rules' => 'trim|required'],
			['field' => 'tgl_mulai', 'label' => 'Tanggal Mulai', 'rules' => 'trim|required'],
			['field' => 'tgl_selesai', 'label' => 'Tanggal Selesai', 'rules' => 'trim|required'],
			['field' => 'nama_dosin', 'label' => 'Nama Dosen/Tamu', 'rules' => 'trim|required'],
			['field' => 'nomor_identitas', 'label' => 'No Identitas (KTP/SIM/NIP/PASPOR)', 'rules' => 'trim|required|max_length[100]'],
			['field' => 'nidn', 'label' => 'Nomor Induk Dosen Nasional', 'rules' => 'trim|required|max_length[10]'],
			['field' => 'institusi_asal', 'label' => 'Institusi Asal', 'rules' => 'trim|required'],
			['field' => 'mou_ks', 'label' => 'Nota Kesepahaman (MoU)', 'rules' => 'trim|required'],
			['field' => 'unit_ks', 'label' => 'MoA/IA/LoA (Unit)', 'rules' => 'trim|required'],
			['field' => 'negara', 'label' => 'Negara', 'rules' => 'trim|required'],
			['field' => 'dosen_inbound2_file_cv_name', 'label' => 'File CV', 'rules' => 'trim|required'],
			['field' => 'dosen_inbound2_file_loa_name', 'label' => 'File LoA', 'rules' => 'trim|required'],
			['field' => 'dosen_inbound2_file_foto_name', 'label' => 'File Foto', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound2_file_jadwal_keg_name', 'label' => 'File Jadwal Kegiatan', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound2_file_paspor_name', 'label' => 'File Paspor', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound2_file_tiket_kepulangan_name', 'label' => 'File Tiket Kepulangan', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound2_file_asuransi_kesehatan_name', 'label' => 'File Asuransi Kesehatan', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound2_file_bank_stantement_name', 'label' => 'File Bank Stantement', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound2_file_materi_paparan_name', 'label' => 'File Materi Paparan', 'rules' => 'trim|required'],
		];
		$this->form_validation->set_rules($this->validation_rules);
	}

	/**
	 * show all Dosen Inbound2s
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		redirect('administrator/dosen_inbound1');
	}

	/**
	 * Add new dosen_inbound2s
	 *
	 */
	public function add()
	{
		$this->is_allowed('dosen_inbound2_add');

		$this->data['filter_prodi'] = get_filter_prodi();
		$this->data['validation_rules'] = $this->validation_rules;
		$this->template->title('Dosen Inbound New');
		$this->render('backend/standart/administrator/dosen_inbound2/dosen_inbound2_add', $this->data);
	}

	/**
	 * Add New Dosen Inbound2s
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('dosen_inbound2_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}


		if ($this->form_validation->run()) {
			$dosen_inbound2_file_cv_uuid = $this->input->post('dosen_inbound2_file_cv_uuid');
			$dosen_inbound2_file_cv_name = $this->input->post('dosen_inbound2_file_cv_name');
			$dosen_inbound2_file_loa_uuid = $this->input->post('dosen_inbound2_file_loa_uuid');
			$dosen_inbound2_file_loa_name = $this->input->post('dosen_inbound2_file_loa_name');
			$dosen_inbound2_file_foto_uuid = $this->input->post('dosen_inbound2_file_foto_uuid');
			$dosen_inbound2_file_foto_name = $this->input->post('dosen_inbound2_file_foto_name');
			$dosen_inbound2_file_jadwal_keg_uuid = $this->input->post('dosen_inbound2_file_jadwal_keg_uuid');
			$dosen_inbound2_file_jadwal_keg_name = $this->input->post('dosen_inbound2_file_jadwal_keg_name');
			$dosen_inbound2_file_paspor_uuid = $this->input->post('dosen_inbound2_file_paspor_uuid');
			$dosen_inbound2_file_paspor_name = $this->input->post('dosen_inbound2_file_paspor_name');
			$dosen_inbound2_file_tiket_kepulangan_uuid = $this->input->post('dosen_inbound2_file_tiket_kepulangan_uuid');
			$dosen_inbound2_file_tiket_kepulangan_name = $this->input->post('dosen_inbound2_file_tiket_kepulangan_name');
			$dosen_inbound2_file_asuransi_kesehatan_uuid = $this->input->post('dosen_inbound2_file_asuransi_kesehatan_uuid');
			$dosen_inbound2_file_asuransi_kesehatan_name = $this->input->post('dosen_inbound2_file_asuransi_kesehatan_name');
			$dosen_inbound2_file_bank_stantement_uuid = $this->input->post('dosen_inbound2_file_bank_stantement_uuid');
			$dosen_inbound2_file_bank_stantement_name = $this->input->post('dosen_inbound2_file_bank_stantement_name');
			$dosen_inbound2_file_materi_paparan_uuid = $this->input->post('dosen_inbound2_file_materi_paparan_uuid');
			$dosen_inbound2_file_materi_paparan_name = $this->input->post('dosen_inbound2_file_materi_paparan_name');

			// simpan kode_fak
			$fak = $this->model_ref_prodi->get_single(['kode' => $this->input->post('kd_prodi')]);

			$save_data = [
				'scope'           => $this->input->post('scope'),
				'aktivitas'       => $this->input->post('aktivitas'),
				'kd_prodi'        => $this->input->post('kd_prodi'),
				'kode_fak'        => $fak->kode_fak,
				'keg_dosin_id'    => $this->input->post('keg_dosin_id'),
				'tgs_dosin_id'    => $this->input->post('tgs_dosin_id'),
				'judul_kegiatan'  => $this->input->post('judul_kegiatan'),
				'tgl_mulai'       => $this->input->post('tgl_mulai'),
				'tgl_selesai'     => $this->input->post('tgl_selesai'),
				'nama_dosin'      => $this->input->post('nama_dosin'),
				'nomor_identitas' => $this->input->post('nomor_identitas'),
				'nidn'            => $this->input->post('nidn'),
				'institusi_asal'  => $this->input->post('institusi_asal'),
				'mou_ks'          => $this->input->post('mou_ks'),
				'unit_ks'         => $this->input->post('unit_ks'),
				'negara'          => $this->input->post('negara'),
				'deskripsi'       => $this->input->post('deskripsi'),
				'kode_dosen'      => $this->input->post('kode_dosen'),
				'validasi'        => $this->input->post('validasi'),
			];

			if (!is_dir(FCPATH . '/uploads/dosen_inbound2/')) {
				mkdir(FCPATH . '/uploads/dosen_inbound2/');
			}

			if (!empty($dosen_inbound2_file_cv_name)) {
				$dosen_inbound2_file_cv_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_cv_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_cv_uuid . '/' . $dosen_inbound2_file_cv_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_cv_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_cv_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_cv'] = $dosen_inbound2_file_cv_name_copy;
			}

			if (!empty($dosen_inbound2_file_loa_name)) {
				$dosen_inbound2_file_loa_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_loa_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_loa_uuid . '/' . $dosen_inbound2_file_loa_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_loa_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_loa'] = $dosen_inbound2_file_loa_name_copy;
			}

			if (!empty($dosen_inbound2_file_foto_name)) {
				$dosen_inbound2_file_foto_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_foto_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_foto_uuid . '/' . $dosen_inbound2_file_foto_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_foto_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_foto_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_foto'] = $dosen_inbound2_file_foto_name_copy;
			}

			if (!empty($dosen_inbound2_file_jadwal_keg_name)) {
				$dosen_inbound2_file_jadwal_keg_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_jadwal_keg_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_jadwal_keg_uuid . '/' . $dosen_inbound2_file_jadwal_keg_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_jadwal_keg_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_jadwal_keg_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_jadwal_keg'] = $dosen_inbound2_file_jadwal_keg_name_copy;
			}

			if (!empty($dosen_inbound2_file_paspor_name)) {
				$dosen_inbound2_file_paspor_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_paspor_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_paspor_uuid . '/' . $dosen_inbound2_file_paspor_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_paspor_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_paspor_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_paspor'] = $dosen_inbound2_file_paspor_name_copy;
			}

			if (!empty($dosen_inbound2_file_tiket_kepulangan_name)) {
				$dosen_inbound2_file_tiket_kepulangan_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_tiket_kepulangan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_tiket_kepulangan_uuid . '/' . $dosen_inbound2_file_tiket_kepulangan_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_tiket_kepulangan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_tiket_kepulangan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_tiket_kepulangan'] = $dosen_inbound2_file_tiket_kepulangan_name_copy;
			}

			if (!empty($dosen_inbound2_file_asuransi_kesehatan_name)) {
				$dosen_inbound2_file_asuransi_kesehatan_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_asuransi_kesehatan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_asuransi_kesehatan_uuid . '/' . $dosen_inbound2_file_asuransi_kesehatan_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_asuransi_kesehatan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_asuransi_kesehatan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_asuransi_kesehatan'] = $dosen_inbound2_file_asuransi_kesehatan_name_copy;
			}

			if (!empty($dosen_inbound2_file_bank_stantement_name)) {
				$dosen_inbound2_file_bank_stantement_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_bank_stantement_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_bank_stantement_uuid . '/' . $dosen_inbound2_file_bank_stantement_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_bank_stantement_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_bank_stantement_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_bank_stantement'] = $dosen_inbound2_file_bank_stantement_name_copy;
			}

			if (!empty($dosen_inbound2_file_materi_paparan_name)) {
				$dosen_inbound2_file_materi_paparan_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_materi_paparan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_materi_paparan_uuid . '/' . $dosen_inbound2_file_materi_paparan_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_materi_paparan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_materi_paparan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_materi_paparan'] = $dosen_inbound2_file_materi_paparan_name_copy;
			}


			$save_dosen_inbound2 = $this->model_dosen_inbound2->store($save_data);


			if ($save_dosen_inbound2) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_dosen_inbound2;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/dosen_inbound2/edit/' . $save_dosen_inbound2, 'Edit Dosen Inbound2'),
						anchor('administrator/dosen_inbound2', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/dosen_inbound2/edit/' . $save_dosen_inbound2, 'Edit Dosen Inbound2')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dosen_inbound2');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dosen_inbound2');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Dosen Inbound2s
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('dosen_inbound2_update');

		if (is_groups_in(['Kaprodi', 'Unit'])) {
			$prodi = get_filter_prodi(true);
			$row = $this->model_dosen_inbound2->find($id);
			if (!in_array($row->kd_prodi, $prodi)) {
				set_message(cclang('sorry_you_do_not_have_permission_to_access', 'dosen_inbound1'), 'error');
				redirect_back();
			}
			/**tolak jika sudah divalidasi */
			if ($row->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa mengubah data, karena data sudah divalidasi', 'dosen_inbound1'), 'warning');
				redirect_back();
			}
		}

		$this->data['dosen_inbound2'] = $this->model_dosen_inbound2->find($id);

		$list_mou = db_get_all_data('mou_kerjasama', "scope='{$this->data['dosen_inbound2']->scope}' AND tgl_selesai > curdate() ");
		$cur_mou = db_get_all_data('mou_kerjasama', "id = '{$this->data['dosen_inbound2']->mou_ks}'", 'row');

		if (!is_null($cur_mou)) {
			$tgl = DateTime::createFromFormat('Y-m-d', $cur_mou->tgl_selesai);
			$now = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
			if ($tgl < $now) $list_mou[] = $cur_mou;
		}
		$list_mou[] = (object)['id' => '1', 'nama_mitra' => 'Pengajuan Baru', 'doc_nomor' => 'XXX'];
		$this->data['list_mou'] = $list_mou;

		//moa
		$list_moa = db_get_all_data('si_kerjasama', "mou_ks = '{$this->data['dosen_inbound2']->mou_ks}'  AND tgl_selesai > curdate() AND kd_prodi = '{$this->data['dosen_inbound2']->kd_prodi}'");
		$cur_moa = db_get_all_data('si_kerjasama', "id = '{$this->data['dosen_inbound2']->unit_ks}'", 'row');

		if (!is_null($cur_moa)) {
			$tgl = DateTime::createFromFormat('Y-m-d', $cur_moa->tgl_selesai);
			$now = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
			if ($tgl < $now) $list_moa[] = $cur_moa;
		}
		$this->data['list_moa'] = $list_moa;
		$this->data['filter_prodi'] = get_filter_prodi();
		$this->data['validation_rules'] = $this->validation_rules;
		$this->template->title('Dosen Inbound Update');
		$this->render('backend/standart/administrator/dosen_inbound2/dosen_inbound2_update', $this->data);
	}

	/**
	 * Update Dosen Inbound2s
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('dosen_inbound2_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}


		if ($this->form_validation->run()) {
			$dosen_inbound2_file_cv_uuid = $this->input->post('dosen_inbound2_file_cv_uuid');
			$dosen_inbound2_file_cv_name = $this->input->post('dosen_inbound2_file_cv_name');
			$dosen_inbound2_file_loa_uuid = $this->input->post('dosen_inbound2_file_loa_uuid');
			$dosen_inbound2_file_loa_name = $this->input->post('dosen_inbound2_file_loa_name');
			$dosen_inbound2_file_foto_uuid = $this->input->post('dosen_inbound2_file_foto_uuid');
			$dosen_inbound2_file_foto_name = $this->input->post('dosen_inbound2_file_foto_name');
			$dosen_inbound2_file_jadwal_keg_uuid = $this->input->post('dosen_inbound2_file_jadwal_keg_uuid');
			$dosen_inbound2_file_jadwal_keg_name = $this->input->post('dosen_inbound2_file_jadwal_keg_name');
			$dosen_inbound2_file_paspor_uuid = $this->input->post('dosen_inbound2_file_paspor_uuid');
			$dosen_inbound2_file_paspor_name = $this->input->post('dosen_inbound2_file_paspor_name');
			$dosen_inbound2_file_tiket_kepulangan_uuid = $this->input->post('dosen_inbound2_file_tiket_kepulangan_uuid');
			$dosen_inbound2_file_tiket_kepulangan_name = $this->input->post('dosen_inbound2_file_tiket_kepulangan_name');
			$dosen_inbound2_file_asuransi_kesehatan_uuid = $this->input->post('dosen_inbound2_file_asuransi_kesehatan_uuid');
			$dosen_inbound2_file_asuransi_kesehatan_name = $this->input->post('dosen_inbound2_file_asuransi_kesehatan_name');
			$dosen_inbound2_file_bank_stantement_uuid = $this->input->post('dosen_inbound2_file_bank_stantement_uuid');
			$dosen_inbound2_file_bank_stantement_name = $this->input->post('dosen_inbound2_file_bank_stantement_name');
			$dosen_inbound2_file_materi_paparan_uuid = $this->input->post('dosen_inbound2_file_materi_paparan_uuid');
			$dosen_inbound2_file_materi_paparan_name = $this->input->post('dosen_inbound2_file_materi_paparan_name');

			$fak = $this->model_ref_prodi->get_single(['kode' => $this->input->post('kd_prodi')]);

			$save_data = [
				'scope'           => $this->input->post('scope'),
				'aktivitas'       => $this->input->post('aktivitas'),
				'kode_fak'        => $fak->kode_fak,
				'kd_prodi'        => $this->input->post('kd_prodi'),
				'keg_dosin_id'    => $this->input->post('keg_dosin_id'),
				'tgs_dosin_id'    => $this->input->post('tgs_dosin_id'),
				'judul_kegiatan'  => $this->input->post('judul_kegiatan'),
				'tgl_mulai'       => $this->input->post('tgl_mulai'),
				'tgl_selesai'     => $this->input->post('tgl_selesai'),
				'nama_dosin'      => $this->input->post('nama_dosin'),
				'nomor_identitas' => $this->input->post('nomor_identitas'),
				'nidn'            => $this->input->post('nidn'),
				'institusi_asal'  => $this->input->post('institusi_asal'),
				'mou_ks'          => $this->input->post('mou_ks'),
				'unit_ks'         => $this->input->post('unit_ks'),
				'negara'          => $this->input->post('negara'),
				'deskripsi'       => $this->input->post('deskripsi'),
				'kode_dosen'      => $this->input->post('kode_dosen'),
				'validasi'        => $this->input->post('validasi'),
			];

			if (!is_dir(FCPATH . '/uploads/dosen_inbound2/')) {
				mkdir(FCPATH . '/uploads/dosen_inbound2/');
			}

			if (!empty($dosen_inbound2_file_cv_uuid)) {
				$dosen_inbound2_file_cv_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_cv_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_cv_uuid . '/' . $dosen_inbound2_file_cv_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_cv_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_cv_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_cv'] = $dosen_inbound2_file_cv_name_copy;
			}

			if (!empty($dosen_inbound2_file_loa_uuid)) {
				$dosen_inbound2_file_loa_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_loa_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_loa_uuid . '/' . $dosen_inbound2_file_loa_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_loa_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_loa'] = $dosen_inbound2_file_loa_name_copy;
			}

			if (!empty($dosen_inbound2_file_foto_uuid)) {
				$dosen_inbound2_file_foto_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_foto_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_foto_uuid . '/' . $dosen_inbound2_file_foto_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_foto_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_foto_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_foto'] = $dosen_inbound2_file_foto_name_copy;
			}

			if (!empty($dosen_inbound2_file_jadwal_keg_uuid)) {
				$dosen_inbound2_file_jadwal_keg_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_jadwal_keg_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_jadwal_keg_uuid . '/' . $dosen_inbound2_file_jadwal_keg_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_jadwal_keg_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_jadwal_keg_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_jadwal_keg'] = $dosen_inbound2_file_jadwal_keg_name_copy;
			}

			if (!empty($dosen_inbound2_file_paspor_uuid)) {
				$dosen_inbound2_file_paspor_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_paspor_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_paspor_uuid . '/' . $dosen_inbound2_file_paspor_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_paspor_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_paspor_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_paspor'] = $dosen_inbound2_file_paspor_name_copy;
			}

			if (!empty($dosen_inbound2_file_tiket_kepulangan_uuid)) {
				$dosen_inbound2_file_tiket_kepulangan_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_tiket_kepulangan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_tiket_kepulangan_uuid . '/' . $dosen_inbound2_file_tiket_kepulangan_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_tiket_kepulangan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_tiket_kepulangan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_tiket_kepulangan'] = $dosen_inbound2_file_tiket_kepulangan_name_copy;
			}

			if (!empty($dosen_inbound2_file_asuransi_kesehatan_uuid)) {
				$dosen_inbound2_file_asuransi_kesehatan_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_asuransi_kesehatan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_asuransi_kesehatan_uuid . '/' . $dosen_inbound2_file_asuransi_kesehatan_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_asuransi_kesehatan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_asuransi_kesehatan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_asuransi_kesehatan'] = $dosen_inbound2_file_asuransi_kesehatan_name_copy;
			}

			if (!empty($dosen_inbound2_file_bank_stantement_uuid)) {
				$dosen_inbound2_file_bank_stantement_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_bank_stantement_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_bank_stantement_uuid . '/' . $dosen_inbound2_file_bank_stantement_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_bank_stantement_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_bank_stantement_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_bank_stantement'] = $dosen_inbound2_file_bank_stantement_name_copy;
			}

			if (!empty($dosen_inbound2_file_materi_paparan_uuid)) {
				$dosen_inbound2_file_materi_paparan_name_copy = date('YmdHis') . '-' . $dosen_inbound2_file_materi_paparan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound2_file_materi_paparan_uuid . '/' . $dosen_inbound2_file_materi_paparan_name,
					FCPATH . 'uploads/dosen_inbound2/' . $dosen_inbound2_file_materi_paparan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2_file_materi_paparan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_materi_paparan'] = $dosen_inbound2_file_materi_paparan_name_copy;
			}


			$save_dosen_inbound2 = $this->model_dosen_inbound2->change($id, $save_data);

			if ($save_dosen_inbound2) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/dosen_inbound2', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dosen_inbound2/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dosen_inbound2/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Dosen Inbound2s
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('dosen_inbound2_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'dosen_inbound2'), 'success');
		} else {
			set_message(cclang('error_delete', 'dosen_inbound2'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Dosen Inbound2s
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('dosen_inbound2_view');

		$this->data['files'] = $this->config->item('dosin2_file');
		$this->data['dosen_inbound2'] = $this->model_dosen_inbound2->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Dosen Inbound Detail');
		$this->render('backend/standart/administrator/dosen_inbound2/dosen_inbound2_view', $this->data);
	}

	/**
	 * delete Dosen Inbound2s
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$dosen_inbound2 = $this->model_dosen_inbound2->find($id);

		if (!empty($dosen_inbound2->file_cv)) {
			$path = FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2->file_cv;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound2->file_loa)) {
			$path = FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2->file_loa;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound2->file_foto)) {
			$path = FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2->file_foto;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound2->file_jadwal_keg)) {
			$path = FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2->file_jadwal_keg;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound2->file_paspor)) {
			$path = FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2->file_paspor;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound2->file_tiket_kepulangan)) {
			$path = FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2->file_tiket_kepulangan;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound2->file_asuransi_kesehatan)) {
			$path = FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2->file_asuransi_kesehatan;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound2->file_bank_stantement)) {
			$path = FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2->file_bank_stantement;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound2->file_materi_paparan)) {
			$path = FCPATH . '/uploads/dosen_inbound2/' . $dosen_inbound2->file_materi_paparan;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}


		return $this->model_dosen_inbound2->remove($id);
	}

	/**
	 * Upload Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function upload_file_cv_file()
	{
		if (!$this->is_allowed('dosen_inbound2_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound2',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function delete_file_cv_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound2_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_cv',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/'
		]);
	}

	/**
	 * Get Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function get_file_cv_file($id)
	{
		if (!$this->is_allowed('dosen_inbound2_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound2 = $this->model_dosen_inbound2->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_cv',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/',
			'delete_endpoint'   => 'administrator/dosen_inbound2/delete_file_cv_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function upload_file_loa_file()
	{
		if (!$this->is_allowed('dosen_inbound2_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound2',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function delete_file_loa_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound2_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_loa',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/'
		]);
	}

	/**
	 * Get Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function get_file_loa_file($id)
	{
		if (!$this->is_allowed('dosen_inbound2_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound2 = $this->model_dosen_inbound2->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_loa',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/',
			'delete_endpoint'   => 'administrator/dosen_inbound2/delete_file_loa_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function upload_file_foto_file()
	{
		if (!$this->is_allowed('dosen_inbound2_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound2',
			'allowed_types' => 'jpg|pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function delete_file_foto_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound2_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_foto',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/'
		]);
	}

	/**
	 * Get Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function get_file_foto_file($id)
	{
		if (!$this->is_allowed('dosen_inbound2_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound2 = $this->model_dosen_inbound2->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_foto',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/',
			'delete_endpoint'   => 'administrator/dosen_inbound2/delete_file_foto_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function upload_file_jadwal_keg_file()
	{
		if (!$this->is_allowed('dosen_inbound2_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound2',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function delete_file_jadwal_keg_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound2_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_jadwal_keg',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/'
		]);
	}

	/**
	 * Get Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function get_file_jadwal_keg_file($id)
	{
		if (!$this->is_allowed('dosen_inbound2_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound2 = $this->model_dosen_inbound2->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_jadwal_keg',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/',
			'delete_endpoint'   => 'administrator/dosen_inbound2/delete_file_jadwal_keg_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function upload_file_paspor_file()
	{
		if (!$this->is_allowed('dosen_inbound2_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound2',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function delete_file_paspor_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound2_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_paspor',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/'
		]);
	}

	/**
	 * Get Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function get_file_paspor_file($id)
	{
		if (!$this->is_allowed('dosen_inbound2_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound2 = $this->model_dosen_inbound2->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_paspor',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/',
			'delete_endpoint'   => 'administrator/dosen_inbound2/delete_file_paspor_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function upload_file_tiket_kepulangan_file()
	{
		if (!$this->is_allowed('dosen_inbound2_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound2',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function delete_file_tiket_kepulangan_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound2_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_tiket_kepulangan',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/'
		]);
	}

	/**
	 * Get Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function get_file_tiket_kepulangan_file($id)
	{
		if (!$this->is_allowed('dosen_inbound2_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound2 = $this->model_dosen_inbound2->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_tiket_kepulangan',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/',
			'delete_endpoint'   => 'administrator/dosen_inbound2/delete_file_tiket_kepulangan_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function upload_file_asuransi_kesehatan_file()
	{
		if (!$this->is_allowed('dosen_inbound2_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound2',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function delete_file_asuransi_kesehatan_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound2_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_asuransi_kesehatan',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/'
		]);
	}

	/**
	 * Get Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function get_file_asuransi_kesehatan_file($id)
	{
		if (!$this->is_allowed('dosen_inbound2_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound2 = $this->model_dosen_inbound2->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_asuransi_kesehatan',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/',
			'delete_endpoint'   => 'administrator/dosen_inbound2/delete_file_asuransi_kesehatan_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function upload_file_bank_stantement_file()
	{
		if (!$this->is_allowed('dosen_inbound2_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound2',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function delete_file_bank_stantement_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound2_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_bank_stantement',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/'
		]);
	}

	/**
	 * Get Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function get_file_bank_stantement_file($id)
	{
		if (!$this->is_allowed('dosen_inbound2_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound2 = $this->model_dosen_inbound2->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_bank_stantement',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/',
			'delete_endpoint'   => 'administrator/dosen_inbound2/delete_file_bank_stantement_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function upload_file_materi_paparan_file()
	{
		if (!$this->is_allowed('dosen_inbound2_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound2',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 10240,
		]);
	}

	/**
	 * Delete Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function delete_file_materi_paparan_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound2_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_materi_paparan',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/'
		]);
	}

	/**
	 * Get Image Dosen Inbound2	* 
	 * @return JSON
	 */
	public function get_file_materi_paparan_file($id)
	{
		if (!$this->is_allowed('dosen_inbound2_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound2 = $this->model_dosen_inbound2->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_materi_paparan',
			'table_name'        => 'dosen_inbound2',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound2/',
			'delete_endpoint'   => 'administrator/dosen_inbound2/delete_file_materi_paparan_file'
		]);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('dosen_inbound2_export');

		$this->model_dosen_inbound2->export(
			'dosen_inbound2',
			'dosen_inbound2',
			$this->model_dosen_inbound2->field_search
		);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('dosen_inbound2_export');

		$this->model_dosen_inbound2->pdf('dosen_inbound2', 'dosen_inbound2');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('dosen_inbound2_export');

		$table = $title = 'dosen_inbound2';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_dosen_inbound2->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
}


/* End of file dosen_inbound2.php */
/* Location: ./application/controllers/administrator/Dosen Inbound2.php */