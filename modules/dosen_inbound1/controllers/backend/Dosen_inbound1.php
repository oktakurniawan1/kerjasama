<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Dosen Inbound1 Controller
 *| --------------------------------------------------------------------------
 *| Dosen Inbound1 site
 *|
 */

use GuzzleHttp\Client;

class Dosen_inbound1 extends Admin
{

	protected $validation_rules = [];

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_dosen_inbound1');
		$this->load->model('ref_prodi/Model_ref_prodi', 'model_ref_prodi');
		$this->lang->load('web_lang', $this->current_lang);
		//cek_isi_survey();
		$this->validation_rules = [
			['field' => 'scope', 'label' => 'DN/LN', 'rules' => 'trim|required'],
			['field' => 'aktivitas', 'label' => 'Jenis Aktivitas', 'rules' => 'trim|required'],
			['field' => 'kd_prodi', 'label' => 'Program Studi/Unit', 'rules' => 'trim|required|max_length[255]'],
			['field' => 'keg_dosin_id', 'label' => 'Jenis Kegiatan', 'rules' => 'trim|required|max_length[11]'],
			['field' => 'tgs_dosin_id', 'label' => 'Tugas Sebagai', 'rules' => 'trim|required|max_length[11]'],
			['field' => 'judul_kegiatan', 'label' => 'Judul Kegiatan', 'rules' => 'trim|required|max_length[255]'],
			['field' => 'tgl_mulai', 'label' => 'Tanggal Mulai', 'rules' => 'trim|required'],
			['field' => 'tgl_selesai', 'label' => 'Tanggal Selesai', 'rules' => 'trim|required'],
			['field' => 'nama_dosin', 'label' => 'Nama Dosen/Tamu', 'rules' => 'trim|required|max_length[255]'],
			['field' => 'nomor_identitas', 'label' => 'No Identitas (KTP/SIM/NIP/PASPOR)', 'rules' => 'trim|required|max_length[100]'],
			['field' => 'nidn', 'label' => 'Nomor Induk Dosen Nasional', 'rules' => 'trim|required|max_length[10]'],
			['field' => 'institusi_asal', 'label' => 'Institusi Asal', 'rules' => 'trim|required|max_length[255]'],
			['field' => 'mou_ks', 'label' => 'Nota Kesepahaman (MoU)', 'rules' => 'trim|required|max_length[255]'],
			['field' => 'unit_ks', 'label' => 'MoA/IA/LoA (Unit)', 'rules' => 'trim|required|max_length[255]'],
			['field' => 'negara', 'label' => 'Negara', 'rules' => 'trim|required|max_length[255]'],
			['field' => 'dosen_inbound1_file_cv_name', 'label' => 'File CV', 'rules' => 'trim|required'],
			['field' => 'dosen_inbound1_file_loa_name', 'label' => 'File LoA', 'rules' => 'trim|required'],
			['field' => 'dosen_inbound1_file_foto_name', 'label' => 'File Foto', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound1_file_jadwal_keg_name', 'label' => 'File Jadwal Kegiatan', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound1_file_ktp_name', 'label' => 'File KTP/NIK', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound1_file_asuransi_kesehatan_name', 'label' => 'File Asuransi Kesehatan', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound1_file_materi_paparan_name', 'label' => 'File Materi Paparan', 'rules' => 'trim|required'],
			['field' => 'deskripsi', 'label' => 'Deskripsi', 'rules' => 'trim|max_length[255]'],
			['field' => 'kode_dosen', 'label' => 'Kode Dosen', 'rules' => 'trim|max_length[8]'],
		];
	$this->form_validation->set_rules($this->validation_rules);
	}
	/**
	 * Filter data
	 */
	public function filter_session_data()
	{
		$fields['tahun'] = $this->input->post('tahun');
		$fields['negara'] = $this->input->post('negara');
		$fields['scope'] = $this->input->post('scope');
		$fields['kd_prodi'] = $this->input->post('kd_prodi');
		$fields['keg_dosin_id'] = $this->input->post('keg_dosin_id');
		$fields['tgs_dosin_id'] = $this->input->post('tgs_dosin_id');
		$fields['is_aktif'] = $this->input->post('is_aktif');
		$fields['validasi'] = $this->input->post('validasi');
		$this->session->set_userdata('filterdata_dosin', $fields);
		$this->session->set_userdata('is_filtered_dosin', $fields);
		echo json_encode(['url' => base_url('administrator/dosen_inbound1')]);
	}
	public function clear_filter_session_data()
	{
		$this->session->unset_userdata('filterdata_dosin');
		$this->session->unset_userdata('is_filtered_dosin');
		echo json_encode(['url' => base_url('administrator/dosen_inbound1')]);
	}
	public function form_filter_data()
	{
		if (!$this->session->has_userdata('filterdata_dosin')) {
			$fields = ['tahun' => 'all', 'negara' => 'all', 'is_aktif' => 'all', 'kd_prodi' => 'all', 'scope' => 'all', 'keg_dosin_id' => 'all', 'tgs_dosin_id' => 'all', 'validasi' => 'all'];
			$this->session->set_userdata('filterdata_dosin', $fields);
		}

		$negara = array_column(db_get_all_data('negara'), 'nama_negara', 'nama_negara');
		$tahun = array_combine(range(2015, date('Y')), range(2015, date('Y')));
		$prodi = array_column(get_filter_prodi(), 'nama_prodi', 'kode');
		$dosenkeg = array_column(db_get_all_data('dosin_keg'), 'kegiatan', 'id');
		$dosentgs = array_column(db_get_all_data('dosin_tugas'), 'tugas', 'id');
		$prodi['all'] = 'ALL';
		$negara['all'] = 'ALL';
		$tahun['all'] = 'ALL';
		$dosenkeg['all'] = 'ALL';
		$dosentgs['all'] = 'ALL';
		$data['tahun'] = $tahun;
		$data['negara'] = $negara;
		$data['prodi'] = $prodi;
		$data['dosenkeg'] = $dosenkeg;
		$data['dosentgs'] = $dosentgs;
		$data['status'] = array('1' => 'Aktif', '0' => 'Selesai', 'all' => 'ALL');
		$data['validasi'] = array('Y' => 'Y', 'T' => 'T', '' => '', 'all' => 'ALL');
		$data['scope'] = array('DN' => 'DN', 'LN' => 'LN', 'all' => 'ALL');
		$this->load->view('form-filter-data', $data);
	}
	/**
	 * Request kode dosen DLB
	 */
	public function form_request_kode_dlb()
	{
		$id = $this->input->get('id');
		$dos = $this->model_dosen_inbound1->find($id);
		if ($dos->validasi == 'Y') {
			$data['dos'] = $dos;
			$view = $this->load->view('form-request-kode-dlb', $data, true);
			echo json_encode(['valid' => 'Y', 'view' => $view]);
		} else {
			$view = html_alert('Sebelum request Kode dosen, data harus terlebih dahulu divalidasi.', 'Perhatian!', 'warning');
			echo json_encode(['valid' => 'T', 'view' => $view]);
		}
	}

	public function request_kode_dlb()
	{
		$url = (ENVIRONMENT == 'development' ? $this->config->item('url1_request_dlb') : $this->config->item('url2_request_dlb'));
		$data = $this->input->get();
		$ds = db_get_all_data('dosen_inbound', ['id' => $data['id']], 'row');
		$data['uuid'] = $ds->uuid;
		$data['token'] = $this->config->item('token_pagoda');
		// $result = callApiWithCurl($url, $data);
		$client = new Client();
		$response = $client->request('POST', $url, [
			'form_params' => $data
		]);
		$result = $response->getBody()->getContents();
		$dlb = json_decode($result);
		if (is_null($dlb)) die();
		else {
			if ($dlb->status == true)
				$this->model_dosen_inbound1->change($data['id'], ['kode_dosen' => $dlb->kodedos, 'uuid' => $dlb->uuid]);
			else
				$this->model_dosen_inbound1->change($data['id'], ['kode_dosen' => $dlb->kodedos]);
			echo $result;
		}
	}
	public function form_cari_kode_sebelumnya()
	{
		$data = db_get_select('kode_dosen,CONCAT_WS(" - ",kode_dosen,nomor_identitas,nama_dosin) AS nama', 'dosen_inbound', false);
		$data['dlb'] = array_column($data, 'nama', 'kode_dosen');
		$this->load->view('form-cari-kode-sebelumnya', $data);
	}
	public function update_kode_dosen()
	{
		$this->model_dosen_inbound1->change($this->input->post('id'), ['kode_dosen' => $this->input->post('kode_dosen')]);
		echo json_encode($this->input->post());
	}

	public function form_option_dosen_inbound()
	{
		$this->load->view('form-option-dosen-inbound');
	}
	public function pilih_option()
	{
		//print_r($_POST);die();
		$scope = $this->input->post('scope');
		$aktivitas = $this->input->post('jenis_aktivitas');
		$is_penelitian = $this->input->post('is_penelitian');
		$urls = [
			'SA' => [
				'DN' => base_url('administrator/dosen_inbound1/add'),
				'LN' => base_url('administrator/dosen_inbound2/add')
			],
			'LA' => [
				'DN' => base_url('administrator/dosen_inbound3/add'),
				'LN' => base_url('administrator/dosen_inbound4/add'),
			],
		];
		if ($scope == 'LN' && $aktivitas == 'LA' && $is_penelitian == 'true')
			$url = base_url('administrator/dosen_inbound5/add');
		else if ($aktivitas == 'PJ')
			$url = base_url('administrator/dosen_inbound6/add');
		else
			$url = $urls[$aktivitas][$scope];
		echo json_encode(['url' => $url]);
	}
	/**
	 * show all Dosen Inbound1s
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('dosen_inbound1_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('dosen_inbound1_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('dosen_inbound1_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('dosen_inbound1_filter');
			$this->session->unset_userdata('dosen_inbound1_field');
		}
		$filter = $this->session->userdata('dosen_inbound1_filter');
		$field = $this->session->userdata('dosen_inbound1_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['dosen_inbound1s'] = $this->model_dosen_inbound1->get($filter, $field, $this->limit_page, $offset);
		$this->data['dosen_inbound1_counts'] = $this->model_dosen_inbound1->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/dosen_inbound1/index/',
			'total_rows'   => $this->model_dosen_inbound1->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Dosen Inbound List');
		$this->render('backend/standart/administrator/dosen_inbound1/dosen_inbound1_list', $this->data);
	}

	/**
	 * Add new dosen_inbound1s
	 *
	 */
	public function add()
	{
		$this->is_allowed('dosen_inbound1_add');

		$this->data['filter_prodi'] = get_filter_prodi();

		$this->template->title('Dosen Inbound New');
		$this->data['validation_rules'] = $this->validation_rules;
		$this->render('backend/standart/administrator/dosen_inbound1/dosen_inbound1_add', $this->data);
	}

	/**
	 * Add New Dosen Inbound1s
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('dosen_inbound1_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		if ($this->form_validation->run()) {
			$dosen_inbound1_file_cv_uuid = $this->input->post('dosen_inbound1_file_cv_uuid');
			$dosen_inbound1_file_cv_name = $this->input->post('dosen_inbound1_file_cv_name');
			$dosen_inbound1_file_loa_uuid = $this->input->post('dosen_inbound1_file_loa_uuid');
			$dosen_inbound1_file_loa_name = $this->input->post('dosen_inbound1_file_loa_name');
			$dosen_inbound1_file_foto_uuid = $this->input->post('dosen_inbound1_file_foto_uuid');
			$dosen_inbound1_file_foto_name = $this->input->post('dosen_inbound1_file_foto_name');
			$dosen_inbound1_file_jadwal_keg_uuid = $this->input->post('dosen_inbound1_file_jadwal_keg_uuid');
			$dosen_inbound1_file_jadwal_keg_name = $this->input->post('dosen_inbound1_file_jadwal_keg_name');
			$dosen_inbound1_file_ktp_uuid = $this->input->post('dosen_inbound1_file_ktp_uuid');
			$dosen_inbound1_file_ktp_name = $this->input->post('dosen_inbound1_file_ktp_name');
			$dosen_inbound1_file_asuransi_kesehatan_uuid = $this->input->post('dosen_inbound1_file_asuransi_kesehatan_uuid');
			$dosen_inbound1_file_asuransi_kesehatan_name = $this->input->post('dosen_inbound1_file_asuransi_kesehatan_name');
			$dosen_inbound1_file_materi_paparan_uuid = $this->input->post('dosen_inbound1_file_materi_paparan_uuid');
			$dosen_inbound1_file_materi_paparan_name = $this->input->post('dosen_inbound1_file_materi_paparan_name');

			// simpan kode_fak
			$fak = $this->model_ref_prodi->get_single(['kode' => $this->input->post('kd_prodi')]);

			$save_data = [
				'scope'           => $this->input->post('scope'),
				'aktivitas'       => $this->input->post('aktivitas'),
				'kode_fak'        => $fak->kode_fak,
				'kd_prodi'        => $this->input->post('kd_prodi'),
				'keg_dosin_id'    => $this->input->post('keg_dosin_id'),
				'tgs_dosin_id'    => $this->input->post('tgs_dosin_id'),
				'judul_kegiatan'  => $this->input->post('judul_kegiatan'),
				'tgl_mulai'       => $this->input->post('tgl_mulai'),
				'tgl_selesai'     => $this->input->post('tgl_selesai'),
				'nama_dosin'      => $this->input->post('nama_dosin'),
				'nomor_identitas' => $this->input->post('nomor_identitas'),
				'nidn'            => $this->input->post('nidn'),
				'institusi_asal'  => $this->input->post('institusi_asal'),
				'mou_ks'          => $this->input->post('mou_ks'),
				'unit_ks'         => $this->input->post('unit_ks'),
				'negara'          => $this->input->post('negara'),
				'deskripsi'       => $this->input->post('deskripsi'),
				'kode_dosen'      => $this->input->post('kode_dosen'),
				'validasi'        => $this->input->post('validasi'),
			];

			if (!is_dir(FCPATH . '/uploads/dosen_inbound1/')) {
				mkdir(FCPATH . '/uploads/dosen_inbound1/');
			}

			if (!empty($dosen_inbound1_file_cv_name)) {
				$dosen_inbound1_file_cv_name_copy = date('YmdHis') . '-' . $dosen_inbound1_file_cv_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound1_file_cv_uuid . '/' . $dosen_inbound1_file_cv_name,
					FCPATH . 'uploads/dosen_inbound1/' . $dosen_inbound1_file_cv_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound1/' . $dosen_inbound1_file_cv_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_cv'] = $dosen_inbound1_file_cv_name_copy;
			}

			if (!empty($dosen_inbound1_file_loa_name)) {
				$dosen_inbound1_file_loa_name_copy = date('YmdHis') . '-' . $dosen_inbound1_file_loa_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound1_file_loa_uuid . '/' . $dosen_inbound1_file_loa_name,
					FCPATH . 'uploads/dosen_inbound1/' . $dosen_inbound1_file_loa_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound1/' . $dosen_inbound1_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_loa'] = $dosen_inbound1_file_loa_name_copy;
			}

			if (!empty($dosen_inbound1_file_foto_name)) {
				$dosen_inbound1_file_foto_name_copy = date('YmdHis') . '-' . $dosen_inbound1_file_foto_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound1_file_foto_uuid . '/' . $dosen_inbound1_file_foto_name,
					FCPATH . 'uploads/dosen_inbound1/' . $dosen_inbound1_file_foto_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound1/' . $dosen_inbound1_file_foto_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_foto'] = $dosen_inbound1_file_foto_name_copy;
			}

			if (!empty($dosen_inbound1_file_jadwal_keg_name)) {
				$dosen_inbound1_file_jadwal_keg_name_copy = date('YmdHis') . '-' . $dosen_inbound1_file_jadwal_keg_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound1_file_jadwal_keg_uuid . '/' . $dosen_inbound1_file_jadwal_keg_name,
					FCPATH . 'uploads/dosen_inbound1/' . $dosen_inbound1_file_jadwal_keg_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound1/' . $dosen_inbound1_file_jadwal_keg_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_jadwal_keg'] = $dosen_inbound1_file_jadwal_keg_name_copy;
			}

			if (!empty($dosen_inbound1_file_ktp_name)) {
				$dosen_inbound1_file_ktp_name_copy = date('YmdHis') . '-' . $dosen_inbound1_file_ktp_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound1_file_ktp_uuid . '/' . $dosen_inbound1_file_ktp_name,
					FCPATH . 'uploads/dosen_inbound1/' . $dosen_inbound1_file_ktp_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound1/' . $dosen_inbound1_file_ktp_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ktp'] = $dosen_inbound1_file_ktp_name_copy;
			}

			if (!empty($dosen_inbound1_file_asuransi_kesehatan_name)) {
				$dosen_inbound1_file_asuransi_kesehatan_name_copy = date('YmdHis') . '-' . $dosen_inbound1_file_asuransi_kesehatan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound1_file_asuransi_kesehatan_uuid . '/' . $dosen_inbound1_file_asuransi_kesehatan_name,
					FCPATH . 'uploads/dosen_inbound1/' . $dosen_inbound1_file_asuransi_kesehatan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound1/' . $dosen_inbound1_file_asuransi_kesehatan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_asuransi_kesehatan'] = $dosen_inbound1_file_asuransi_kesehatan_name_copy;
			}

			if (!empty($dosen_inbound1_file_materi_paparan_name)) {
				$dosen_inbound1_file_materi_paparan_name_copy = date('YmdHis') . '-' . $dosen_inbound1_file_materi_paparan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound1_file_materi_paparan_uuid . '/' . $dosen_inbound1_file_materi_paparan_name,
					FCPATH . 'uploads/dosen_inbound1/' . $dosen_inbound1_file_materi_paparan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound1/' . $dosen_inbound1_file_materi_paparan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_materi_paparan'] = $dosen_inbound1_file_materi_paparan_name_copy;
			}


			$save_dosen_inbound1 = $this->model_dosen_inbound1->store($save_data);


			if ($save_dosen_inbound1) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_dosen_inbound1;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/dosen_inbound1/edit/' . $save_dosen_inbound1, 'Edit Dosen Inbound1'),
						anchor('administrator/dosen_inbound1', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/dosen_inbound1/edit/' . $save_dosen_inbound1, 'Edit Dosen Inbound1')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dosen_inbound1');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dosen_inbound1');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Dosen Inbound1s
	 *
	 * @var $id String
	 */
	public function edit_dosin($id)
	{
		$this->is_allowed('dosen_inbound1_update');
		//case
		$urls = [
			'SA' => [
				'DN' => base_url('administrator/dosen_inbound1/edit/' . $id),
				'LN' => base_url('administrator/dosen_inbound2/edit/' . $id)
			],
			'LA' => [
				'DN' => base_url('administrator/dosen_inbound3/edit/' . $id),
				'LN' => base_url('administrator/dosen_inbound4/edit/' . $id)
			],
		];
		$row = $this->model_dosen_inbound1->find($id);
		if ($row->tgs_dosin_id == '6' && $row->scope == 'LN' && $row->aktivitas == 'LA')
			$url =  base_url('administrator/dosen_inbound5/edit/' . $id);
		else if ($row->keg_dosin_id == '10')
			$url =  base_url('administrator/dosen_inbound6/edit/' . $id);
		else
			$url = $urls[$row->aktivitas][$row->scope];
		echo json_encode(['url' => $url]);
	}

	public function view_dosin($id)
	{
		$this->is_allowed('dosen_inbound1_view');
		//case
		$urls = [
			'SA' => [
				'DN' => base_url('administrator/dosen_inbound1/view/' . $id),
				'LN' => base_url('administrator/dosen_inbound2/view/' . $id)
			],
			'LA' => [
				'DN' => base_url('administrator/dosen_inbound3/view/' . $id),
				'LN' => base_url('administrator/dosen_inbound4/view/' . $id)
			],
		];
		$row = $this->model_dosen_inbound1->find($id);
		if ($row->tgs_dosin_id == '6' && $row->scope == 'LN' && $row->aktivitas == 'LA')
			$url =  base_url('administrator/dosen_inbound5/view/' . $id);
		else
			$url = $urls[$row->aktivitas][$row->scope];
		echo json_encode(['url' => $url]);
	}

	public function edit($id)
	{
		$this->is_allowed('dosen_inbound1_update');

		if (is_groups_in(['Kaprodi', 'Unit'])) {
			$prodi = get_filter_prodi(true);
			$row = $this->model_dosen_inbound1->find($id);
			if (!in_array($row->kd_prodi, $prodi)) {
				set_message(cclang('sorry_you_do_not_have_permission_to_access', 'dosen_inbound1'), 'error');
				redirect_back();
			}
			/**tolak jika sudah divalidasi */
			if ($row->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa mengubah data, karena data sudah divalidasi', 'dosen_inbound1'), 'warning');
				redirect_back();
			}
		}

		$this->data['dosen_inbound1'] = $this->model_dosen_inbound1->find($id);

		//mou
		$list_mou = db_get_all_data('mou_kerjasama', "scope='{$this->data['dosen_inbound1']->scope}' AND tgl_selesai > curdate() ");
		$cur_mou = db_get_all_data('mou_kerjasama', "id = '{$this->data['dosen_inbound1']->mou_ks}'", 'row');
		if (!is_null($cur_mou)) {
			$tgl = DateTime::createFromFormat('Y-m-d', $cur_mou->tgl_selesai);
			$now = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
			if ($tgl < $now) $list_mou[] = $cur_mou;
		}
		$list_mou[] = (object)['id' => '1', 'nama_mitra' => 'Pengajuan Baru', 'doc_nomor' => 'XXX'];
		$this->data['list_mou'] = $list_mou;

		//moa
		$list_moa = db_get_all_data('si_kerjasama', "mou_ks = '{$this->data['dosen_inbound1']->mou_ks}'  AND tgl_selesai > curdate() AND kd_prodi = '{$this->data['dosen_inbound1']->kd_prodi}'");
		$cur_moa = db_get_all_data('si_kerjasama', "id = '{$this->data['dosen_inbound1']->unit_ks}'", 'row');

		if (!is_null($cur_moa)) {
			$tgl = DateTime::createFromFormat('Y-m-d', $cur_moa->tgl_selesai);
			$now = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
			if ($tgl < $now) $list_moa[] = $cur_moa;
		}
		$this->data['list_moa'] = $list_moa;
		$this->data['filter_prodi'] = get_filter_prodi();
		$this->data['validation_rules'] = $this->validation_rules;
		$this->template->title('Dosen Inbound Update');
		$this->render('backend/standart/administrator/dosen_inbound1/dosen_inbound1_update', $this->data);
	}

	/**
	 * Update Dosen Inbound1s
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('dosen_inbound1_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		if ($this->form_validation->run()) {
			$dosen_inbound1_file_cv_uuid = $this->input->post('dosen_inbound1_file_cv_uuid');
			$dosen_inbound1_file_cv_name = $this->input->post('dosen_inbound1_file_cv_name');
			$dosen_inbound1_file_loa_uuid = $this->input->post('dosen_inbound1_file_loa_uuid');
			$dosen_inbound1_file_loa_name = $this->input->post('dosen_inbound1_file_loa_name');
			$dosen_inbound1_file_foto_uuid = $this->input->post('dosen_inbound1_file_foto_uuid');
			$dosen_inbound1_file_foto_name = $this->input->post('dosen_inbound1_file_foto_name');
			$dosen_inbound1_file_jadwal_keg_uuid = $this->input->post('dosen_inbound1_file_jadwal_keg_uuid');
			$dosen_inbound1_file_jadwal_keg_name = $this->input->post('dosen_inbound1_file_jadwal_keg_name');
			$dosen_inbound1_file_ktp_uuid = $this->input->post('dosen_inbound1_file_ktp_uuid');
			$dosen_inbound1_file_ktp_name = $this->input->post('dosen_inbound1_file_ktp_name');
			$dosen_inbound1_file_asuransi_kesehatan_uuid = $this->input->post('dosen_inbound1_file_asuransi_kesehatan_uuid');
			$dosen_inbound1_file_asuransi_kesehatan_name = $this->input->post('dosen_inbound1_file_asuransi_kesehatan_name');
			$dosen_inbound1_file_materi_paparan_uuid = $this->input->post('dosen_inbound1_file_materi_paparan_uuid');
			$dosen_inbound1_file_materi_paparan_name = $this->input->post('dosen_inbound1_file_materi_paparan_name');

			$fak = $this->model_ref_prodi->get_single(['kode' => $this->input->post('kd_prodi')]);

			$save_data = [
				'scope'           => $this->input->post('scope'),
				'aktivitas'       => $this->input->post('aktivitas'),
				'kode_fak'        => $fak->kode_fak,
				'kd_prodi'        => $this->input->post('kd_prodi'),
				'keg_dosin_id'    => $this->input->post('keg_dosin_id'),
				'tgs_dosin_id'    => $this->input->post('tgs_dosin_id'),
				'judul_kegiatan'  => $this->input->post('judul_kegiatan'),
				'tgl_mulai'       => $this->input->post('tgl_mulai'),
				'tgl_selesai'     => $this->input->post('tgl_selesai'),
				'nama_dosin'      => $this->input->post('nama_dosin'),
				'nomor_identitas' => $this->input->post('nomor_identitas'),
				'nidn'            => $this->input->post('nidn'),
				'institusi_asal'  => $this->input->post('institusi_asal'),
				'mou_ks'          => $this->input->post('mou_ks'),
				'unit_ks'         => $this->input->post('unit_ks'),
				'negara'          => $this->input->post('negara'),
				'deskripsi'       => $this->input->post('deskripsi'),
				'kode_dosen'      => $this->input->post('kode_dosen'),
				'validasi'        => $this->input->post('validasi'),
			];

			if (!is_dir(FCPATH . '/uploads/dosen_inbound1/')) {
				mkdir(FCPATH . '/uploads/dosen_inbound1/');
			}

			if (!empty($dosen_inbound1_file_cv_uuid)) {
				$dosen_inbound1_file_cv_name_copy = date('YmdHis') . '-' . $dosen_inbound1_file_cv_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound1_file_cv_uuid . '/' . $dosen_inbound1_file_cv_name,
					FCPATH . 'uploads/dosen_inbound1/' . $dosen_inbound1_file_cv_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound1/' . $dosen_inbound1_file_cv_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_cv'] = $dosen_inbound1_file_cv_name_copy;
			}

			if (!empty($dosen_inbound1_file_loa_uuid)) {
				$dosen_inbound1_file_loa_name_copy = date('YmdHis') . '-' . $dosen_inbound1_file_loa_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound1_file_loa_uuid . '/' . $dosen_inbound1_file_loa_name,
					FCPATH . 'uploads/dosen_inbound1/' . $dosen_inbound1_file_loa_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound1/' . $dosen_inbound1_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_loa'] = $dosen_inbound1_file_loa_name_copy;
			}

			if (!empty($dosen_inbound1_file_foto_uuid)) {
				$dosen_inbound1_file_foto_name_copy = date('YmdHis') . '-' . $dosen_inbound1_file_foto_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound1_file_foto_uuid . '/' . $dosen_inbound1_file_foto_name,
					FCPATH . 'uploads/dosen_inbound1/' . $dosen_inbound1_file_foto_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound1/' . $dosen_inbound1_file_foto_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_foto'] = $dosen_inbound1_file_foto_name_copy;
			}

			if (!empty($dosen_inbound1_file_jadwal_keg_uuid)) {
				$dosen_inbound1_file_jadwal_keg_name_copy = date('YmdHis') . '-' . $dosen_inbound1_file_jadwal_keg_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound1_file_jadwal_keg_uuid . '/' . $dosen_inbound1_file_jadwal_keg_name,
					FCPATH . 'uploads/dosen_inbound1/' . $dosen_inbound1_file_jadwal_keg_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound1/' . $dosen_inbound1_file_jadwal_keg_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_jadwal_keg'] = $dosen_inbound1_file_jadwal_keg_name_copy;
			}

			if (!empty($dosen_inbound1_file_ktp_uuid)) {
				$dosen_inbound1_file_ktp_name_copy = date('YmdHis') . '-' . $dosen_inbound1_file_ktp_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound1_file_ktp_uuid . '/' . $dosen_inbound1_file_ktp_name,
					FCPATH . 'uploads/dosen_inbound1/' . $dosen_inbound1_file_ktp_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound1/' . $dosen_inbound1_file_ktp_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ktp'] = $dosen_inbound1_file_ktp_name_copy;
			}

			if (!empty($dosen_inbound1_file_asuransi_kesehatan_uuid)) {
				$dosen_inbound1_file_asuransi_kesehatan_name_copy = date('YmdHis') . '-' . $dosen_inbound1_file_asuransi_kesehatan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound1_file_asuransi_kesehatan_uuid . '/' . $dosen_inbound1_file_asuransi_kesehatan_name,
					FCPATH . 'uploads/dosen_inbound1/' . $dosen_inbound1_file_asuransi_kesehatan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound1/' . $dosen_inbound1_file_asuransi_kesehatan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_asuransi_kesehatan'] = $dosen_inbound1_file_asuransi_kesehatan_name_copy;
			}

			if (!empty($dosen_inbound1_file_materi_paparan_uuid)) {
				$dosen_inbound1_file_materi_paparan_name_copy = date('YmdHis') . '-' . $dosen_inbound1_file_materi_paparan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound1_file_materi_paparan_uuid . '/' . $dosen_inbound1_file_materi_paparan_name,
					FCPATH . 'uploads/dosen_inbound1/' . $dosen_inbound1_file_materi_paparan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound1/' . $dosen_inbound1_file_materi_paparan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_materi_paparan'] = $dosen_inbound1_file_materi_paparan_name_copy;
			}


			$save_dosen_inbound1 = $this->model_dosen_inbound1->change($id, $save_data);

			if ($save_dosen_inbound1) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/dosen_inbound1', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dosen_inbound1/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dosen_inbound1/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Dosen Inbound1s
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('dosen_inbound1_delete');

		if (is_groups_in(['Kaprodi', 'Unit'])) {
			$prodi = get_filter_prodi(true);
			$row = $this->model_dosen_inbound1->find($id);
			if (!in_array($row->kd_prodi, $prodi)) {
				set_message(cclang('sorry_you_do_not_have_permission_to_access', 'dosen_inbound1'), 'error');
				redirect_back();
			}
			/**tolak jika sudah divalidasi */
			if ($row->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa menghapus data, karena data sudah divalidasi', 'dosen_inbound1'), 'warning');
				redirect_back();
			}
		}

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'dosen_inbound1'), 'success');
		} else {
			set_message(cclang('error_delete', 'dosen_inbound1'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Dosen Inbound1s
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('dosen_inbound1_view');

		$this->data['files'] = $this->config->item('dosin1_file');
		$this->data['dosen_inbound1'] = $this->model_dosen_inbound1->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Dosen Inbound Detail');
		$this->render('backend/standart/administrator/dosen_inbound1/dosen_inbound1_view', $this->data);
	}

	/**
	 * delete Dosen Inbound1s
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$dosen_inbound1 = db_get_all_data('dosen_inbound', ['id' => $id], 'row');
		$files = $this->config->item('dosin_file_fields');
		for ($i = 1; $i < 5; $i++) {
			foreach ($files as $file) {
				if (!empty($dosen_inbound1->$file)) {
					$path = FCPATH . "uploads/dosen_inbound{$i}/" . $dosen_inbound1->$file;
					if (is_file($path)) {
						$delete_file = unlink($path);
					}
				}
			}
		}
		$url = (ENVIRONMENT == 'development' ? $this->config->item('url1_request_hapus_dlb') : $this->config->item('url2_request_hapus_dlb'));
		$data = $this->input->get();
		$data['token'] = $this->config->item('token_pagoda');
		$data['kode'] = $dosen_inbound1->kode_dosen;
		callApiWithCurl($url, $data);
		return $this->model_dosen_inbound1->remove($id);
	}

	/**
	 * Upload Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function upload_file_cv_file()
	{
		if (!$this->is_allowed('dosen_inbound1_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound1',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function delete_file_cv_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound1_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_cv',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound1',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound1/'
		]);
	}

	/**
	 * Get Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function get_file_cv_file($id)
	{
		if (!$this->is_allowed('dosen_inbound1_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound1 = $this->model_dosen_inbound1->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_cv',
			'table_name'        => 'dosen_inbound1',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound1/',
			'delete_endpoint'   => 'administrator/dosen_inbound1/delete_file_cv_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function upload_file_loa_file()
	{
		if (!$this->is_allowed('dosen_inbound1_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound1',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function delete_file_loa_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound1_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_loa',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound1',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound1/'
		]);
	}

	/**
	 * Get Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function get_file_loa_file($id)
	{
		if (!$this->is_allowed('dosen_inbound1_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound1 = $this->model_dosen_inbound1->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_loa',
			'table_name'        => 'dosen_inbound1',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound1/',
			'delete_endpoint'   => 'administrator/dosen_inbound1/delete_file_loa_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function upload_file_foto_file()
	{
		if (!$this->is_allowed('dosen_inbound1_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound1',
			'allowed_types' => 'pdf|jpg',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function delete_file_foto_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound1_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_foto',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound1',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound1/'
		]);
	}

	/**
	 * Get Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function get_file_foto_file($id)
	{
		if (!$this->is_allowed('dosen_inbound1_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound1 = $this->model_dosen_inbound1->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_foto',
			'table_name'        => 'dosen_inbound1',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound1/',
			'delete_endpoint'   => 'administrator/dosen_inbound1/delete_file_foto_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function upload_file_jadwal_keg_file()
	{
		if (!$this->is_allowed('dosen_inbound1_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound1',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function delete_file_jadwal_keg_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound1_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_jadwal_keg',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound1',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound1/'
		]);
	}

	/**
	 * Get Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function get_file_jadwal_keg_file($id)
	{
		if (!$this->is_allowed('dosen_inbound1_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound1 = $this->model_dosen_inbound1->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_jadwal_keg',
			'table_name'        => 'dosen_inbound1',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound1/',
			'delete_endpoint'   => 'administrator/dosen_inbound1/delete_file_jadwal_keg_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function upload_file_ktp_file()
	{
		if (!$this->is_allowed('dosen_inbound1_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound1',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function delete_file_ktp_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound1_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ktp',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound1',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound1/'
		]);
	}

	/**
	 * Get Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function get_file_ktp_file($id)
	{
		if (!$this->is_allowed('dosen_inbound1_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound1 = $this->model_dosen_inbound1->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ktp',
			'table_name'        => 'dosen_inbound1',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound1/',
			'delete_endpoint'   => 'administrator/dosen_inbound1/delete_file_ktp_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function upload_file_asuransi_kesehatan_file()
	{
		if (!$this->is_allowed('dosen_inbound1_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound1',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function delete_file_asuransi_kesehatan_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound1_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_asuransi_kesehatan',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound1',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound1/'
		]);
	}

	/**
	 * Get Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function get_file_asuransi_kesehatan_file($id)
	{
		if (!$this->is_allowed('dosen_inbound1_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound1 = $this->model_dosen_inbound1->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_asuransi_kesehatan',
			'table_name'        => 'dosen_inbound1',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound1/',
			'delete_endpoint'   => 'administrator/dosen_inbound1/delete_file_asuransi_kesehatan_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function upload_file_materi_paparan_file()
	{
		if (!$this->is_allowed('dosen_inbound1_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound1',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 10240,
		]);
	}

	/**
	 * Delete Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function delete_file_materi_paparan_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound1_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_materi_paparan',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound1',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound1/'
		]);
	}

	/**
	 * Get Image Dosen Inbound1	* 
	 * @return JSON
	 */
	public function get_file_materi_paparan_file($id)
	{
		if (!$this->is_allowed('dosen_inbound1_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound1 = $this->model_dosen_inbound1->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_materi_paparan',
			'table_name'        => 'dosen_inbound1',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound1/',
			'delete_endpoint'   => 'administrator/dosen_inbound1/delete_file_materi_paparan_file'
		]);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('dosen_inbound1_export');

		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
			'scope',
			'tahun',
			'tgl_mulai',
			'tgl_selesai',
			'negara',
			'kd_prodi',
			'ref_prodi_nama_prodi',
			'dosin_keg_kegiatan',
			'dosin_tugas_tugas',
			'nama_dosin',
			'kode_dosen',
			'mou_kerjasama_nama_mitra',
			'si_kerjasama_nama_mitra',
			'institusi_asal',
			'judul_kegiatan',
		];
		$data = $this->model_dosen_inbound1->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_dosen_inbound1->export2($data, 'file', $selected_field);
		// $this->model_dosen_inbound1->export(
		// 	'dosen_inbound1',
		// 	'dosen_inbound1',
		// 	$this->model_dosen_inbound1->field_search
		// );
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('dosen_inbound1_export');

		$this->model_dosen_inbound1->pdf('dosen_inbound1', 'dosen_inbound1');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('dosen_inbound1_export');

		$table = $title = 'dosen_inbound1';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_dosen_inbound1->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
}


/* End of file dosen_inbound1.php */
/* Location: ./application/controllers/administrator/Dosen Inbound1.php */
