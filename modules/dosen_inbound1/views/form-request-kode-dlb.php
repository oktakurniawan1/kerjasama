<form id="form-request-kode-dlb" role="form">
   <div class="form-group">
      <label for="">Nama Dosen</label>
      <input type="text" value="<?= $dos->nama_dosin ?>" class="form-control" disabled>
   </div>
   <div class="form-group">
      <label for="">Asal Institusi</label>
      <input type="text" class="form-control" value="<?= $dos->institusi_asal ?>" disabled>
   </div>
   <div class="form-group">
      <label for="">Kode Dosen</label>
		<div class="row">
			<div class="col-md-8">
				<input type="text" class="form-control" id="kode_dosen_dlb" placeholder="<?= $dos->kode_dosen ?>" disabled>
			</div>
			<div class="col-md-4">
				<button class="btn btn-default" id='btn1'>Kode dosen sebelumnya</button>
			</div>
		</div>
   </div>
   <?php echo html_alert('Untuk login sikadu menggunakan username dan password sama dengan kode dosen', '', 'success'); ?>
   <input type="hidden" name="id" value="<?= $dos->id ?>">
   <input type="hidden" name="nama" value="<?= $dos->nama_dosin ?>">
   <input type="hidden" name="kd_prodi" value="<?= $dos->kd_prodi ?>">
   <input type="hidden" name="nomor_identitas" value="<?= $dos->nomor_identitas ?>">
</form>
<?php echo html_alert('Tekan tombol request untuk mendapatkan kode dosen. Jangan tutup dahulu tunggu beberapa detik sampai muncul notifikasi.', 'Perhatian!', 'info'); ?>
<script>
	$('#btn1').click(function(e){
		e.preventDefault();
		var dialog = new BootstrapDialog({
			title: 'Cari kode sebelumnya',
			draggable: true,
			message: function(dialog) {
				var $message = $('<div></div>');
				$.get('<?= site_url('administrator/dosen_inbound1/form_cari_kode_sebelumnya') ?>', {}, function(response) {
					$message.html(response);
				})
				return $message;
			},
			buttons: [{
				label: 'Update',
				icon: 'glyphicon glyphicon-floppy-disk',
				cssClass: 'btn-primary',
				action: function(dialog) {
					var data = {};
					data[csrf] = token;
					data['id'] = $('input[name=id]').val();
					data['kode_dosen'] = $('select[name=kode_sebelumnya]').val();
					data['nama'] = $('select[name=kode_sebelumnya] option:selected').text();
					$.post('<?= site_url('administrator/dosen_inbound1/update_kode_dosen') ?>', data, function(respon) {
						$('#kode_dosen_dlb').val(respon.nama );
					}, 'json')
					dialog.close();
				}
			}, {
				label: 'Tutup',
				action: function(dialog) {
					dialog.close();
				}
			}],
		});
		dialog.realize(),
		dialog.getModalBody().prop('id','form-cari-kode-sebelumnya');
		dialog.open();
	})
</script>