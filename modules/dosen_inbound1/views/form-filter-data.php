<form action="" method="POST" role="form">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label for="">Program Studi</label>
				<?= form_dropdown('kd_prodi', $prodi, fromsess('filterdata_dosin')['kd_prodi'], ['style' => 'width:100%']) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label for="">Tahun</label>
				<?= form_dropdown('tahun', $tahun, fromsess('filterdata_dosin')['tahun'], ['class' => 'form-control']) ?>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="">Jenis Kegiatan</label>
				<?= form_dropdown('keg_dosin_id', $dosenkeg, fromsess('filterdata_dosin')['keg_dosin_id'], ['class' => 'form-control']) ?>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="">Tugas</label>
				<?= form_dropdown('tgs_dosin_id', $dosentgs, fromsess('filterdata_dosin')['tgs_dosin_id'], ['class' => 'form-control']) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-5">
			<div class="form-group">
				<label for="">Negara</label>
				<?= form_dropdown('negara', $negara, fromsess('filterdata_dosin')['negara'], ['style' => 'width:100%']) ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<label for="">Scope</label>
				<?= form_dropdown('scope', $scope, fromsess('filterdata_dosin')['scope'], ['class' => 'form-control']) ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<label for="">Validasi</label>
				<?= form_dropdown('validasi', $validasi, fromsess('filterdata_dosin')['validasi'], ['class' => 'form-control']) ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="">Status</label>
				<?= form_dropdown('is_aktif', $status, fromsess('filterdata_dosin')['is_aktif'], ['class' => 'form-control']) ?>
			</div>
		</div>
	</div>
</form>

<script>
	$(function() {
		$('select[name=kd_prodi],select[name=negara]').select2({
			theme: 'classic',
			dropdownParent: $('#filterdosin-dialog'),
		});
	})
</script>