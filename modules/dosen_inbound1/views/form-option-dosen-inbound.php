<form action="" method="POST" role="form">
	<div class="form-group">
		<label for="">Jenis Aktivitas</label>
		<?php echo form_dropdown('jenis_aktivitas', ['SA' => 'Short Activities', 'LA' => 'Long Activities','PJ'=>'Penguji Skripsi/Thesis/Disertasi'], 'SA', ['class' => 'form-control']) ?>
	</div>
	<div class="form-group">
		<label for="">DN/LN</label>
		<?php echo form_dropdown('scope', ['DN' => 'Dalam Negeri', 'LN' => 'Luar Negeri'], 'DN', ['class' => 'form-control']) ?>
	</div>
	<div class='form-group'>
		<div class="checkbox">
			<label style="display: none;">
				<input type="checkbox" name='is_penelitian'>
				Penelitian
			</label>
		</div>
	</div>
</form>
<script>
	$(function() {
		$('select[name="scope"]').change(function(e) {
			var aktivitas = $('select[name=jenis_aktivitas] option:selected').val();
			if (e.target.value == 'LN' && aktivitas == 'LA')
				$('input[type=checkbox]').parent().show();
			else
				$('label input[type=checkbox]').parent().hide();
		})
		$('select[name=jenis_aktivitas]').change(function(e) {
			$('select[name="scope"]').trigger('change');
			$('select[name="scope"]').prop('disabled',e.target.value == 'PJ');
		})
	})
</script>