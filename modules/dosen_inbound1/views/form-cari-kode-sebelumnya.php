<form role="form">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label for="">Cari Dosen</label>
				<?= form_dropdown('kode_sebelumnya', $dlb,null,['style' => 'width:100%']) ?>
			</div>
		</div>
	</div>
</form>
<script>
	$(function() {
		$('select[name=kode_sebelumnya]').select2({
			theme: 'classic',
			dropdownParent: $('#form-cari-kode-sebelumnya'),
		});
	})
</script>