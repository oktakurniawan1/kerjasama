<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['ref_prodi'] = 'Referensi Prodi/Unit';
$lang['id'] = 'Id';
$lang['kode'] = 'Kode';
$lang['nama_prodi'] = 'Nama Prodi/Unit';
$lang['kode_fak'] = 'Fakultas';
$lang['tingkat'] = 'Tingkat';
