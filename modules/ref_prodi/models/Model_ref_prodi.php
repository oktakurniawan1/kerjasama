<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_ref_prodi extends MY_Model
{

	private $primary_key    = 'id';
	private $table_name     = 'ref_prodi';
	public $field_search   = ['kode', 'nama_prodi', 'kode_fak', 'tingkat'];

	public function __construct()
	{
		$config = array(
			'primary_key'   => $this->primary_key,
			'table_name'    => $this->table_name,
			'field_search'  => $this->field_search,
		);

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "ref_prodi." . $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "ref_prodi." . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			$where .= "(" . "ref_prodi." . $field . " LIKE '%" . $q . "%' )";
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "ref_prodi." . $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "ref_prodi." . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			$where .= "(" . "ref_prodi." . $field . " LIKE '%" . $q . "%' )";
		}

		if (is_array($select_field) and count($select_field)) {
			$this->db->select($select_field);
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->select("
            (case kode_fak
                when '1' then 'FIPP'
                when '2' then 'FBS'
                when '3' then 'FISIP'
                when '4' then 'FMIPA'
                when '5' then 'FT'
                when '6' then 'FIK'
                when '7' then 'FEB'
                when '8' then 'FH'
                when '9' then 'FK'
                when '0' then 'Unit'
                when '99' then 'Sekolah Pascasarjana'
                else '-'
            end) AS nama_fakultas
        ", false);
		$this->db->select("
            (case tingkat
                when '1' then 'Universitas'
                when '2' then 'Fakultas'
                when '3' then 'Program Studi'
                when '4' then 'Badan/Biro/Lembaga'
                when '5' then 'UPT/Unit'
                else '-'
            end) AS nama_tingkat
        ", false);
		$this->db->where($where);
		$this->db->limit($limit, $offset);

		$this->sortable();

		$query = $this->db->get($this->table_name);

		return $query->result();
	}

	public function join_avaiable()
	{

		$this->db->select('ref_prodi.*');


		return $this;
	}

	public function filter_avaiable()
	{

		if (!$this->aauth->is_admin()) {
		}

		return $this;
	}
	public function update_mapping($data)
	{
		foreach ($data as $unit_id => $mapped_unit) {
			$this->db->where('id_unit', $unit_id);
			$this->db->update('unit', ['mapped_unit' => $mapped_unit]);
		}
		return $this->db->affected_rows() > 0;
	}
	public function get_map_unit_prodi($id_unit)
	{
		$this->db->select('u.id_unit, u.nm_unit, p.*');
		$this->db->from('unit u');
		$this->db->join('ref_prodi p', 'u.mapped_unit = p.id');
		$this->db->where('id_unit', $id_unit);
		$query = $this->db->get();
		return $query->row();
	}
}

/* End of file Model_ref_prodi.php */
/* Location: ./application/models/Model_ref_prodi.php */