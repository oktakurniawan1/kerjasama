<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<script type="text/javascript">
   //This page is a result of an autogenerated content made by running test.html with firefox.
   function domo() {

      // Binding keys
      $('*').bind('keydown', 'Ctrl+a', function assets() {
         window.location.href = BASE_URL + '/administrator/Ref_prodi/add';
         return false;
      });

      $('*').bind('keydown', 'Ctrl+f', function assets() {
         $('#sbtn').trigger('click');
         return false;
      });

      $('*').bind('keydown', 'Ctrl+x', function assets() {
         $('#reset').trigger('click');
         return false;
      });

      $('*').bind('keydown', 'Ctrl+b', function assets() {

         $('#reset').trigger('click');
         return false;
      });
   }

   jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
      <?= cclang('ref_prodi') ?><small><?= cclang('list_all'); ?></small>
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?= cclang('ref_prodi') ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">

      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                     <div class="row pull-right">
                        <?php is_allowed('ref_prodi_add', function () { ?>
                           <a class="btn btn-flat btn-success btn_add_new" id="btn_add_new" title="<?= cclang('add_new_button', [cclang('ref_prodi')]); ?>  (Ctrl+a)" href="<?= site_url('administrator/ref_prodi/add'); ?>"><i class="fa fa-plus-square-o"></i> <?= cclang('add_new_button', [cclang('ref_prodi')]); ?></a>
                           <?php }) ?><?php is_allowed('ref_prodi_export', function () { ?>
                           <a class="btn btn-flat btn-success" title="<?= cclang('export'); ?> <?= cclang('ref_prodi') ?> " href="<?= site_url('administrator/ref_prodi/export?q=' . $this->input->get('q') . '&f=' . $this->input->get('f')); ?>"><i class="fa fa-file-excel-o"></i> <?= cclang('export'); ?> XLS</a>
                        <?php }) ?> </div>
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username"><?= cclang('ref_prodi') ?></h3>
                     <h5 class="widget-user-desc"><?= cclang('list_all', [cclang('ref_prodi')]); ?> <i class="label bg-yellow"><?= $ref_prodi_counts; ?> <?= cclang('items'); ?></i></h5>
                  </div>

                  <form name="form_ref_prodi" id="form_ref_prodi" action="<?= base_url('administrator/ref_prodi/index'); ?>">
                     <div class="row">
                        <div class="col-md-8">
                           <?php is_allowed('ref_prodi_delete', function () { ?>
                              <div class="col-sm-2 padd-left-0 ">
                                 <select type="text" class="form-control chosen chosen-select" name="bulk" id="bulk" placeholder="Site Email">
                                    <option value="">Bulk</option>
                                    <option value="delete">Delete</option>
                                 </select>
                              </div>
                              <div class="col-sm-2 padd-left-0 ">
                                 <button type="button" class="btn btn-flat" name="apply" id="apply" title="<?= cclang('apply_bulk_action'); ?>"><?= cclang('apply_button'); ?></button>
                              </div>
                           <?php }) ?>
                           <div class="col-sm-3 padd-left-0  ">
                              <input type="text" class="form-control" name="q" id="filter" placeholder="<?= cclang('filter'); ?>" value="<?= $this->session->userdata('ref_prodi_filter'); ?>">
                           </div>
                           <div class="col-sm-3 padd-left-0 ">
                              <select type="text" class="form-control chosen chosen-select" name="f" id="field">
                                 <option value=""><?= cclang('all'); ?></option>
                                 <option <?= $this->session->userdata('ref_prodi_field') == 'kode' ? 'selected' : ''; ?> value="kode">Kode</option>
                                 <option <?= $this->session->userdata('ref_prodi_field') == 'nama_prodi' ? 'selected' : ''; ?> value="nama_prodi">Nama Prodi/Unit</option>
                                 <option <?= $this->session->userdata('ref_prodi_field') == 'kode_fak' ? 'selected' : ''; ?> value="kode_fak">Fakultas</option>
                              </select>
                           </div>
                           <div class="col-sm-1 padd-left-0 ">
                              <button type="submit" class="btn btn-flat" name="sbtn" id="sbtn" value="Apply" title="<?= cclang('filter_search'); ?>">
                                 Filter
                              </button>
                           </div>
                           <div class="col-sm-1 padd-left-0 ">
                              <a class="btn btn-default btn-flat" name="reset" id="reset" value="Apply" href="<?= base_url('administrator/ref_prodi/?reset_filter='); ?>" title="<?= cclang('reset_filter'); ?>">
                                 <i class="fa fa-undo"></i>
                              </a>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="dataTables_paginate paging_simple_numbers pull-right" id="example2_paginate">
                              <?= $pagination; ?>
                           </div>
                        </div>
                     </div>
                     <div class="table-responsive">
                        <table class="table table-bordered table-striped dataTable">
                           <thead>
                              <tr class="">
                                 <th>
                                    <input type="checkbox" class="flat-red toltip" id="check_all" name="check_all" title="check all">
                                 </th>
                                 <th data-field="kode" data-sort="1" data-primary-key="0"> <?= cclang('kode') ?></th>
                                 <th data-field="nama_prodi" data-sort="1" data-primary-key="0"> <?= cclang('nama_prodi') ?></th>
                                 <th data-field="kode_fak" data-sort="1" data-primary-key="0"> <?= cclang('kode_fak') ?></th>
                                 <th data-field="tingkat" data-sort="1" data-primary-key="0"> <?= cclang('tingkat') ?></th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody id="tbody_ref_prodi">
                              <?php foreach ($ref_prodis as $ref_prodi) : ?>
                                 <tr>
                                    <td width="5">
                                       <input type="checkbox" class="flat-red check" name="id[]" value="<?= $ref_prodi->id; ?>">
                                    </td>

                                    <td><?= _ent($ref_prodi->kode); ?></td>
                                    <td><?= _ent($ref_prodi->nama_prodi); ?></td>
                                    <td><?= _ent($ref_prodi->nama_fakultas); ?></td>
                                    <td><?= _ent($ref_prodi->nama_tingkat); ?></td>
                                    <td width="120">

                                       <?php is_allowed('ref_prodi_view', function () use ($ref_prodi) { ?>
                                          <a href="<?= site_url('administrator/ref_prodi/view/' . $ref_prodi->id); ?>" class="btn btn-default btn-xs"><i class="fa fa-eye text-info"></i></a>
                                       <?php }) ?>
                                       <?php is_allowed('ref_prodi_update', function () use ($ref_prodi) { ?>
                                          <a href="<?= site_url('administrator/ref_prodi/edit/' . $ref_prodi->id); ?>" class="btn btn-default btn-xs"><i class="fa fa-edit fa-lg text-warning"></i></a>
                                       <?php }) ?> <?php is_allowed('ref_prodi_delete', function () use ($ref_prodi) { ?>
                                          <a href="javascript:void(0);" data-href="<?= site_url('administrator/ref_prodi/delete/' . $ref_prodi->id); ?>" class="btn btn-default btn-xs remove-data"><i class="fa fa-trash fa-lg text-danger"></i></a>
                                       <?php }) ?>

                                    </td>
                                 </tr>
                              <?php endforeach; ?>
                              <?php if ($ref_prodi_counts == 0) : ?>
                                 <tr>
                                    <td colspan="100">
                                       Referensi Prodi/Unit data is not available
                                    </td>
                                 </tr>
                              <?php endif; ?>

                           </tbody>
                        </table>
                     </div>
               </div>
               <!-- /.widget-user -->
               <hr>
               </form>
               <div class="row">
                  <div class="col-md-4 col-md-offset-8">
                     <div class="dataTables_paginate paging_simple_numbers pull-right" id="example2_paginate">
                        <?= $pagination; ?>
                     </div>
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->

<!-- Page script -->
<script>
   $(document).ready(function() {

      $('.remove-data').click(function() {

         var url = $(this).attr('data-href');

         swal({
               title: "<?= cclang('are_you_sure'); ?>",
               text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#DD6B55",
               confirmButtonText: "<?= cclang('yes_delete_it'); ?>",
               cancelButtonText: "<?= cclang('no_cancel_plx'); ?>",
               closeOnConfirm: true,
               closeOnCancel: true
            },
            function(isConfirm) {
               if (isConfirm) {
                  document.location.href = url;
               }
            });

         return false;
      });


      $('#apply').click(function() {

         var bulk = $('#bulk');
         var serialize_bulk = $('#form_ref_prodi').serialize();

         if (bulk.val() == 'delete') {
            swal({
                  title: "<?= cclang('are_you_sure'); ?>",
                  text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "<?= cclang('yes_delete_it'); ?>",
                  cancelButtonText: "<?= cclang('no_cancel_plx'); ?>",
                  closeOnConfirm: true,
                  closeOnCancel: true
               },
               function(isConfirm) {
                  if (isConfirm) {
                     document.location.href = BASE_URL + '/administrator/ref_prodi/delete?' + serialize_bulk;
                  }
               });

            return false;

         } else if (bulk.val() == '') {
            swal({
               title: "Upss",
               text: "<?= cclang('please_choose_bulk_action_first'); ?>",
               type: "warning",
               showCancelButton: false,
               confirmButtonColor: "#DD6B55",
               confirmButtonText: "Okay!",
               closeOnConfirm: true,
               closeOnCancel: true
            });

            return false;
         }

         return false;

      }); /*end appliy click*/


      //check all
      var checkAll = $('#check_all');
      var checkboxes = $('input.check');

      checkAll.on('ifChecked ifUnchecked', function(event) {
         if (event.type == 'ifChecked') {
            checkboxes.iCheck('check');
         } else {
            checkboxes.iCheck('uncheck');
         }
      });

      checkboxes.on('ifChanged', function(event) {
         if (checkboxes.filter(':checked').length == checkboxes.length) {
            checkAll.prop('checked', 'checked');
         } else {
            checkAll.removeProp('checked');
         }
         checkAll.iCheck('update');
      });
      initSortable('ref_prodi', $('table.dataTable'));
   }); /*end doc ready*/
</script>