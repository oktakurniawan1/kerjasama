<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Unit Mapping</title>
    <style>
        .level-0 td:first-child {
            padding-left: 10px;
        }

        .level-1 td:first-child {
            padding-left: 30px;
        }

        .level-2 td:first-child {
            padding-left: 50px;
        }

        .level-3 td:first-child {
            padding-left: 70px;
        }

        /* Tambahkan lebih banyak gaya untuk level yang lebih dalam jika diperlukan */
        .hide {
            display: none;
        }
    </style>
</head>

<body>
    <section class="content-header">
        <h1>
            Unit Mapping
            <small>Manajemen Unit Mapping</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Mapping Unit</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-body ">
                        <!-- Widget: user widget style 1 -->
                        <div class="box box-widget widget-user-2">
                            <h4>Cari nama unit, kode unit, dan jenjang</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" id="search" placeholder="Search and Enter">
                            </div>
                            <form id="unitMappingForm">
                                <table class="table table-bordered table-hover" id="unitTable">
                                    <thead>
                                        <tr>
                                            <th>Nama Unit</th>
                                            <th>Kode</th>
                                            <th>Level</th>
                                            <th>Jenjang</th>
                                            <th>Maping Unit</th>
                                        </tr>
                                    </thead>
                                    <tbody id="treeTable">
                                        <?php foreach ($unit as $item) : ?>
                                            <tr class="level-<?= $item->level ?>" data-id="<?= $item->id_unit ?>" data-parent="<?= $item->induk ?>" data-level="<?= $item->level ?>">
                                                <td>
                                                    <?php if ($item->has_children) : ?>
                                                        <span class="toggle" style="cursor:pointer;"><i class="fa fa-plus"></i></span>
                                                    <?php endif; ?>
                                                    <?= $item->nm_unit ?>
                                                </td>
                                                <td><?= $item->id_akademik ?></td>
                                                <td><?= $item->level ?></td>
                                                <td><?= $item->jenjang ?></td>
                                                <td>
                                                    <select class="form-control maping-unit select2" name="maping_unit[<?= $item->id_unit ?>]">
                                                        <option value="">Pilih</option>
                                                        <?php foreach ($prodi as $p) : ?>
                                                            <option value="<?= $p->id ?>" <?= $p->id == $item->mapped_unit ? 'selected' : '' ?>><?= $p->kode . ' - ' . $p->nama_prodi ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <button type="submit" style="margin-bottom: 10px;" class="btn btn-primary" id="saveButton">Simpan</button>
                            </form>
                        </div>
                        <!-- /.widget-user -->
                    </div>
                    <!--/box body -->
                </div>
                <!--/box -->
            </div>
        </div>
    </section>
    <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
            $('.toggle').on('click', function() {
                var $this = $(this);
                var $icon = $this.find('i');
                var $row = $this.closest('tr');
                var id = $row.data('id');
                var level = $row.data('level');

                if ($icon.hasClass('fa-plus')) {
                    $icon.removeClass('fa-plus').addClass('fa-minus');
                    $row.nextAll('tr').each(function() {
                        var $nextRow = $(this);
                        if ($nextRow.data('parent') == id) {
                            $nextRow.removeClass('hide');
                            if ($nextRow.data('level') > level + 1) {
                                return false;
                            }
                        }
                    });
                } else {
                    $icon.removeClass('fa-minus').addClass('fa-plus');
                    collapseChildRows(id);
                }
            });

            function collapseChildRows(parentId) {
                $('#treeTable tr').each(function() {
                    var $row = $(this);
                    if ($row.data('parent') == parentId) {
                        $row.addClass('hide');
                        $row.find('.toggle i').removeClass('fa-minus').addClass('fa-plus');
                        collapseChildRows($row.data('id'));
                    }
                });
            }

            // Filter functionality
            $('#search').on('keypress', function(e) {
                if (e.which == 13) { // Enter key pressed
                    $.LoadingOverlay('show')
                    e.preventDefault();
                    var value = $(this).val().toLowerCase();
                    var rows = $('#treeTable tr');
                    rows.each(function() {
                        var $row = $(this);
                        var text = $row.find('td:nth-child(1), td:nth-child(2), td:nth-child(4)').text().toLowerCase();
                        var isVisible = text.indexOf(value) > -1;
                        $row.toggle(isVisible);
                        if (isVisible && $row.data('parent')) {
                            showParent($row.data('parent'));
                            showChildren($row.data('id'));
                        }
                    });
                    $.LoadingOverlay('hide')
                }
            });

            function showParent(parentId) {
                var $parentRow = $('#treeTable tr[data-id="' + parentId + '"]');
                if ($parentRow.css('display') === 'none') {
                    $parentRow.show();
                    if ($parentRow.data('parent')) {
                        showParent($parentRow.data('parent'));
                    }
                }
            }

            function showChildren(id) {
                $('#treeTable tr').each(function() {
                    var $row = $(this);
                    if ($row.data('parent') == id) {
                        $row.show();
                        if ($row.data('id')) {
                            showChildren($row.data('id'));
                        }
                    }
                });
            }

            // Save button functionality
            $('#unitMappingForm').on('submit', function(e) {
                e.preventDefault();
                var formData = $(this).serializeArray();
                var data = {};
                formData.forEach(function(item) {
                    var key = item.name.match(/\[(.*?)\]/)[1];
                    data[key] = item.value;
                });
                // console.log(data); // Debugging purpose
                $.ajax({
                    type: 'POST',
                    url: '<?= site_url('administrator/ref_prodi/save_map_unit') ?>',
                    data: {
                        data: data,
                        <?= csrf_token() ?>
                    },
                    success: function(response) {
                        alert('Data berhasil disimpan');
                    },
                    error: function() {
                        alert('Terjadi kesalahan saat menyimpan data');
                    }
                });
            });

            // Initialize all rows as hidden except level 0
            $('#treeTable tr').not('.level-0').addClass('hide');
            $('#treeTable tr.level-0').removeClass('hide');
            // Trigger click on level 0 to expand/collapse
            $('.level-0 .toggle').trigger('click');
        });
    </script>
</body>

</html>