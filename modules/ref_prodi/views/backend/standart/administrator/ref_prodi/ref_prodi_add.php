<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Referensi Prodi/Unit <small><?= cclang('new', ['Referensi Prodi/Unit']); ?> </small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/ref_prodi'); ?>">Referensi Prodi/Unit</a></li>
		<li class="active"><?= cclang('new'); ?></li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Referensi Prodi/Unit</h3>
							<h5 class="widget-user-desc"><?= cclang('new', ['Referensi Prodi/Unit']); ?></h5>
							<hr>
						</div>
						<?= form_open('', [
							'name'    => 'form_ref_prodi',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_ref_prodi',
							'enctype' => 'multipart/form-data',
							'method'  => 'POST'
						]); ?>

						<div class="form-group ">
							<label for="kode" class="col-sm-2 control-label">Kode
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="kode" id="kode" placeholder="Kode" value="<?= set_value('kode'); ?>">
								<small class="info help-block">
									<b>Input Kode</b> Max Length : 6.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nama_prodi" class="col-sm-2 control-label">Nama Prodi/Unit
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nama_prodi" id="nama_prodi" placeholder="Nama Prodi/Unit" value="<?= set_value('nama_prodi'); ?>">
								<small class="info help-block">
									<b>Input Nama Prodi</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="kode_fak" class="col-sm-2 control-label">Fakultas
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select" name="kode_fak" id="kode_fak" data-placeholder="Select Fakultas">
									<option value="">Not Set</option>
									<option value="1">FIPP</option>
									<option value="2">FBS</option>
									<option value="3">FISIP</option>
									<option value="4">FMIPA</option>
									<option value="5">FT</option>
									<option value="6">FIK</option>
									<option value="7">FEB</option>
									<option value="8">FH</option>
									<option value="9">FK</option>
									<option value="0">Unit</option>
									<option value="99">Sekolah Pascasarjana</option>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tingkat" class="col-sm-2 control-label">Tingkat
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select" name="tingkat" id="tingkat" data-placeholder="Select Tingkat">
									<option value="">Not Set</option>
									<option value="1">Universitas</option>
									<option value="2">Fakultas</option>
									<option value="3">Program Studi</option>
									<option value="4">Badan/Lembaga/Biro</option>
									<option value="5">UPT/Unit</option>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>


						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	$(document).ready(function() {


		$('#btn_cancel').click(function() {
			swal({
					title: "<?= cclang('are_you_sure'); ?>",
					text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/ref_prodi';
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();

			var form_ref_prodi = $('#form_ref_prodi');
			var data_post = form_ref_prodi.serializeArray();
			var save_type = $(this).attr('data-stype');

			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: BASE_URL + '/administrator/ref_prodi/add_save',
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('.steps li').removeClass('error');
					$('form').find('.error-input').remove();
					if (res.success) {

						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						resetForm();
						$('.chosen option').prop('selected', false).trigger('chosen:updated');

					} else {
						if (res.errors) {

							$.each(res.errors, function(index, val) {
								$('form #' + index).parents('.form-group').addClass('has-error');
								$('form #' + index).parents('.form-group').find('small').prepend(`<div class="error-input">` + val + `</div>`);
							});
							$('.steps li').removeClass('error');
							$('.content section').each(function(index, el) {
								if ($(this).find('.has-error').length) {
									$('.steps li:eq(' + index + ')').addClass('error').find('a').trigger('click');
								}
							});
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/

	}); /*end doc ready*/
</script>