<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Ref_prodi extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_ref_prodi');
	}

	/**
	 * @api {get} /ref_prodi/all Get all ref_prodis.
	 * @apiVersion 0.1.0
	 * @apiName AllRefprodi 
	 * @apiGroup ref_prodi
	 * @apiHeader {String} X-Api-Key Ref prodis unique access-key.
	 * @apiPermission Ref prodi Cant be Accessed permission name : api_ref_prodi_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Ref prodis.
	 * @apiParam {String} [Field="All Field"] Optional field of Ref prodis : id, kode, nama_prodi, kode_fak, tingkat.
	 * @apiParam {String} [Start=0] Optional start index of Ref prodis.
	 * @apiParam {String} [Limit=10] Optional limit data of Ref prodis.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of ref_prodi.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataRef prodi Ref prodi data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_ref_prodi_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'kode', 'nama_prodi', 'kode_fak', 'tingkat'];
		$ref_prodis = $this->model_api_ref_prodi->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_ref_prodi->count_all($filter, $field);
		$ref_prodis = array_map(function($row){
						
			return $row;
		}, $ref_prodis);

		$data['ref_prodi'] = $ref_prodis;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Ref prodi',
			'data'	 	=> $data,
			'total' 	=> $total,
		], API::HTTP_OK);
	}

		/**
	 * @api {get} /ref_prodi/detail Detail Ref prodi.
	 * @apiVersion 0.1.0
	 * @apiName DetailRef prodi
	 * @apiGroup ref_prodi
	 * @apiHeader {String} X-Api-Key Ref prodis unique access-key.
	 * @apiPermission Ref prodi Cant be Accessed permission name : api_ref_prodi_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Ref prodis.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of ref_prodi.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError Ref prodiNotFound Ref prodi data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_ref_prodi_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'kode', 'nama_prodi', 'kode_fak', 'tingkat'];
		$ref_prodi = $this->model_api_ref_prodi->find($id, $select_field);

		if (!$ref_prodi) {
			$this->response([
					'status' 	=> false,
					'message' 	=> 'Blog not found'
				], API::HTTP_NOT_FOUND);
		}

					
		$data['ref_prodi'] = $ref_prodi;
		if ($data['ref_prodi']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Ref prodi',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Ref prodi not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /ref_prodi/add Add Ref prodi.
	 * @apiVersion 0.1.0
	 * @apiName AddRef prodi
	 * @apiGroup ref_prodi
	 * @apiHeader {String} X-Api-Key Ref prodis unique access-key.
	 * @apiPermission Ref prodi Cant be Accessed permission name : api_ref_prodi_add
	 *
 	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_ref_prodi_add', false);

		
		if ($this->form_validation->run()) {

			$save_data = [
			];
			
			$save_ref_prodi = $this->model_api_ref_prodi->store($save_data);

			if ($save_ref_prodi) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /ref_prodi/update Update Ref prodi.
	 * @apiVersion 0.1.0
	 * @apiName UpdateRef prodi
	 * @apiGroup ref_prodi
	 * @apiHeader {String} X-Api-Key Ref prodis unique access-key.
	 * @apiPermission Ref prodi Cant be Accessed permission name : api_ref_prodi_update
	 *
	 * @apiParam {Integer} id Mandatory id of Ref Prodi.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_ref_prodi_update', false);

		
		
		if ($this->form_validation->run()) {

			$save_data = [
			];
			
			$save_ref_prodi = $this->model_api_ref_prodi->change($this->post('id'), $save_data);

			if ($save_ref_prodi) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /ref_prodi/delete Delete Ref prodi. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteRef prodi
	 * @apiGroup ref_prodi
	 * @apiHeader {String} X-Api-Key Ref prodis unique access-key.
	 	 * @apiPermission Ref prodi Cant be Accessed permission name : api_ref_prodi_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Ref prodis .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_ref_prodi_delete', false);

		$ref_prodi = $this->model_api_ref_prodi->find($this->post('id'));

		if (!$ref_prodi) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Ref prodi not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_ref_prodi->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Ref prodi deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Ref prodi not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
}

/* End of file Ref prodi.php */
/* Location: ./application/controllers/api/Ref prodi.php */