<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Ref Prodi Controller
 *| --------------------------------------------------------------------------
 *| Ref Prodi site
 *|
 */
class Ref_prodi extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_ref_prodi');
		$this->lang->load('web_lang', $this->current_lang);
		$this->load->library('Tree', ['table' => 'unit', 'id_column' => 'id_unit', 'parent_column' => 'induk'], 'unit');
	}

	/**
	 * show all Ref Prodis
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('ref_prodi_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('ref_prodi_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('ref_prodi_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('ref_prodi_filter');
			$this->session->unset_userdata('ref_prodi_field');
		}
		$filter = $this->session->userdata('ref_prodi_filter');
		$field = $this->session->userdata('ref_prodi_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['ref_prodis'] = $this->model_ref_prodi->get($filter, $field, $this->limit_page, $offset);
		$this->data['ref_prodi_counts'] = $this->model_ref_prodi->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/ref_prodi/index/',
			'total_rows'   => $this->model_ref_prodi->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Ref Prodi List');
		$this->render('backend/standart/administrator/ref_prodi/ref_prodi_list', $this->data);
	}

	/**
	 * Add new ref_prodis
	 *
	 */
	public function add()
	{
		$this->is_allowed('ref_prodi_add');

		$this->template->title('Ref Prodi New');
		$this->render('backend/standart/administrator/ref_prodi/ref_prodi_add', $this->data);
	}

	/**
	 * Add New Ref Prodis
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('ref_prodi_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('kode', 'Kode', 'trim|required|max_length[6]');
		$this->form_validation->set_rules('nama_prodi', 'Nama Prodi/Unit', 'trim|required|max_length[255]');


		if ($this->form_validation->run()) {

			$save_data = [
				'kode' => $this->input->post('kode'),
				'nama_prodi' => $this->input->post('nama_prodi'),
				'kode_fak' => $this->input->post('kode_fak'),
				'tingkat' => $this->input->post('tingkat'),
			];


			$save_ref_prodi = $this->model_ref_prodi->store($save_data);


			if ($save_ref_prodi) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_ref_prodi;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/ref_prodi/edit/' . $save_ref_prodi, 'Edit Ref Prodi'),
						anchor('administrator/ref_prodi', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/ref_prodi/edit/' . $save_ref_prodi, 'Edit Ref Prodi')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ref_prodi');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ref_prodi');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Ref Prodis
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('ref_prodi_update');

		$this->data['ref_prodi'] = $this->model_ref_prodi->find($id);

		$this->template->title('Ref Prodi Update');
		$this->render('backend/standart/administrator/ref_prodi/ref_prodi_update', $this->data);
	}

	/**
	 * Update Ref Prodis
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('ref_prodi_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('kode', 'Kode', 'trim|required|max_length[6]');
		$this->form_validation->set_rules('nama_prodi', 'Nama Prodi/Unit', 'trim|required|max_length[255]');

		if ($this->form_validation->run()) {

			$save_data = [
				'kode' => $this->input->post('kode'),
				'nama_prodi' => $this->input->post('nama_prodi'),
				'kode_fak' => $this->input->post('kode_fak'),
				'tingkat' => $this->input->post('tingkat'),
			];


			$save_ref_prodi = $this->model_ref_prodi->change($id, $save_data);

			if ($save_ref_prodi) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/ref_prodi', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ref_prodi/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ref_prodi/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Ref Prodis
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('ref_prodi_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'ref_prodi'), 'success');
		} else {
			set_message(cclang('error_delete', 'ref_prodi'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Ref Prodis
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('ref_prodi_view');

		$this->data['ref_prodi'] = $this->model_ref_prodi->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Ref Prodi Detail');
		$this->render('backend/standart/administrator/ref_prodi/ref_prodi_view', $this->data);
	}

	public function view_by_kode($kode)
	{
		$this->is_allowed('ref_prodi_view');
		$this->data['ref_prodi'] = $this->model_ref_prodi->get_single(['kode' => $kode]);
		$this->template->title('Ref Prodi Detail');
		$this->render('backend/standart/administrator/ref_prodi/ref_prodi_view', $this->data);
	}

	/**
	 * delete Ref Prodis
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$ref_prodi = $this->model_ref_prodi->find($id);



		return $this->model_ref_prodi->remove($id);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('ref_prodi_export');

		$this->model_ref_prodi->export(
			'ref_prodi',
			'ref_prodi',
			$this->model_ref_prodi->field_search
		);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('ref_prodi_export');

		$this->model_ref_prodi->pdf('ref_prodi', 'ref_prodi');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('ref_prodi_export');

		$table = $title = 'ref_prodi';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_ref_prodi->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
	public function unit_maping()
	{
		$this->data['unit']  = $this->unit->getAll();
		$this->data['prodi'] = $this->model_ref_prodi->find_all();
		$this->render('backend/standart/administrator/ref_prodi/unit_maping_view', $this->data);
	}
	public function save_map_unit()
	{
		$data = $this->input->post('data');
        $result = $this->model_ref_prodi->update_mapping($data);
        echo json_encode(['success' => $result]);
	}
}


/* End of file ref_prodi.php */
/* Location: ./application/controllers/administrator/Ref Prodi.php */