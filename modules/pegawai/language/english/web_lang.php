<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['pegawai'] = 'Pegawai';
$lang['id_pegawai'] = 'Id Pegawai';
$lang['nip'] = 'NIP';
$lang['nip_baru'] = 'NIP Baru';
$lang['kode_dosen'] = 'Kode Dosen';
$lang['nama_gelar'] = 'Nama Gelar';
$lang['nm_unit'] = 'Unit';
$lang['nm_person'] = 'Nama';
$lang['no_hp'] = 'HP';
$lang['email'] = 'Email';
$lang['nm_sts_pegawai'] = 'Status';
$lang['nm_kat_pegawai'] = 'Kategori';
$lang['jns_pegawai'] = 'Jenis';
$lang['nm_jabatan'] = 'Jabatan';
$lang['nm_homebase'] = 'Homebase';
$lang['nm_jabatan_struktural'] = 'Jabatan Struktural';
$lang['nm_unit_struktural'] = 'Unit Struktural';
$lang['nm_unit_fungsional'] = 'Unit Fungsional';
$lang['nm_jabatan_fungsional'] = 'Jabatan Fungsional';
$lang['nm_prodi'] = 'Prodi';
