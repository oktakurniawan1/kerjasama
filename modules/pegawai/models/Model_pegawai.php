<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_pegawai extends MY_Model
{

    private $primary_key  = 'id_pegawai';
    private $table_name   = 'pegawai';
    public  $field_search = ['nip', 'nip_baru', 'kode_dosen', 'nama_gelar', 'nm_unit', 'nm_person', 'no_hp', 'email', 'nm_sts_pegawai', 'nm_kat_pegawai', 'jns_pegawai', 'nm_jabatan', 'nm_homebase', 'nm_jabatan_struktural', 'nm_unit_struktural', 'nm_unit_fungsional', 'nm_jabatan_fungsional', 'nm_prodi'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
        );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "pegawai." . $field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "pegawai." . $field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '(' . $where . ')';
        } else {
            $where .= "(" . "pegawai." . $field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);

        /**form filter data */
        if ($this->session->has_userdata('filterdata_pegawai')) {
            foreach (fromsess('filterdata_pegawai') as $key => $row) {
                if ($row != 'all') $this->db->having($key, $row);
            }
        }
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [], $result = TRUE)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "pegawai." . $field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "pegawai." . $field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '(' . $where . ')';
        } else {
            $where .= "(" . "pegawai." . $field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) and count($select_field)) {
            $this->db->select($select_field);
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);

        /**form filter data */
        if ($this->session->has_userdata('filterdata_pegawai')) {
            foreach (fromsess('filterdata_pegawai') as $key => $row) {
                if ($row != 'all') $this->db->having($key, $row);
            }
        }
        $this->db->limit($limit, $offset);

        $this->sortable();

        $query = $this->db->get($this->table_name);

        if ($result)
            return $query->result();
        else return $query;
    }

    public function join_avaiable()
    {

        $this->db->select('pegawai.*');


        return $this;
    }

    public function filter_avaiable()
    {

        if (!$this->aauth->is_admin()) {
        }

        return $this;
    }
    public function get_pegawai()
    {
        return $this->db->select('nip,nip_baru,nama_gelar,jns_pegawai,nm_unit_fungsional,nm_unit_struktural,no_hp,email,tmpt_lahir,tgl_lahir')->get('pegawai')->result();
    }

    public function get_info_pegawai($nip)
    {
        $this->db->select('
            p.nip_baru, 
            p.id_unit, 
            p.nm_unit, 
            p.jns_pegawai, 
            p.nama_gelar, 
            p.id_homebase, 
            p.nm_homebase,
            u.id_akademik,
            p.nm_jabatan_fungsional,
            p.nm_jabatan_struktural,
            id_jabatan_struktural,
            nm_jabatan_struktural,
            id_unit_struktural,
            nm_unit_struktural
        ');
        $this->db->from('pegawai p');
        $this->db->join('unit u', 'p.id_homebase = u.id_unit', 'left');
        $this->db->where('p.nip_baru', $nip);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }
}

/* End of file Model_pegawai.php */
/* Location: ./application/models/Model_pegawai.php */