<script defer src="<?= BASE_ASSET; ?>/js/custom.js"></script>
<div class="row">
    <div class="form-group ">
        <label for="" class="col-sm-2 control-label">Cari Pegawai</label>
        <div class="col-sm-10">
            <select class="form-control chosen chosen-select-deselect" name="pegawai" id="pegawai" data-placeholder="cari nip,nama pegawai...">
                <option value=""></option>
                <?php foreach ($pegawai as $row) :
                    $data = "data-nip='$row->nip_baru' data-nama='{$row->nama_gelar}' data-lahir_tmp='$row->tmpt_lahir' data-lahir_tgl='$row->tgl_lahir' data-tlp='{$row->no_hp}' data-email='{$row->email}'"; ?>
                    <option <?= $data ?> value="<?= $row->nip_baru ?>"><?= $row->jns_pegawai . ' - ' . $row->nip_baru . ' - ' . $row->nama_gelar ?></option>
                <?php endforeach; ?>
            </select>
            <small class="info help-block">
                cari nama pegawai di simpeg
            </small>
        </div>
    </div>
</div>