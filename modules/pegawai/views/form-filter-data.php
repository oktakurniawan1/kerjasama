<form action="" method="POST" role="form">
   <div class="row">
      <div class="col-md-8">
         <div class="form-group">
            <label for="">Jenis pegawai</label>
            <?= form_dropdown('jns_pegawai', $jns_pegawai, fromsess('filterdata_pegawai')['jns_pegawai'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
         </div>
         <div class="form-group">
            <label for="">Kategori pegawai</label>
            <?= form_dropdown('nm_kat_pegawai', $nm_kat_pegawai, fromsess('filterdata_pegawai')['nm_kat_pegawai'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
         </div>
         <div class="form-group">
            <label for="">Kategori pegawai</label>
            <?= form_dropdown('nm_sts_pegawai', $status_kepegawaian, fromsess('filterdata_pegawai')['nm_sts_pegawai'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
         </div>
      </div>
   </div>
</form>

<script>
   $(function() {
      // $('select[name=name1],select[name=name2],select[name=name3]').select2({
      // 	theme: 'classic',
      // 	dropdownParent: $('#filterpegawai-dialog'),
      // });
      // $('select[name=name4]').select2({
      // 	tags:true
      // })
   })
</script>