

<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Pegawai        <small>Edit Pegawai</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/pegawai'); ?>">Pegawai</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Pegawai</h3>
                            <h5 class="widget-user-desc">Edit Pegawai</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/pegawai/edit_save/'.$this->uri->segment(4)), [
                            'name'    => 'form_pegawai', 
                            'class'   => 'form-horizontal form-step', 
                            'id'      => 'form_pegawai', 
                            'method'  => 'POST'
                            ]); ?>
                         
                                                <div class="form-group ">
                            <label for="nip" class="col-sm-2 control-label">NIP 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nip" id="nip" placeholder="NIP" value="<?= set_value('nip', $pegawai->nip); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="nip_baru" class="col-sm-2 control-label">NIP Baru 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nip_baru" id="nip_baru" placeholder="NIP Baru" value="<?= set_value('nip_baru', $pegawai->nip_baru); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kode_dosen" class="col-sm-2 control-label">Kode Dosen 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kode_dosen" id="kode_dosen" placeholder="Kode Dosen" value="<?= set_value('kode_dosen', $pegawai->kode_dosen); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="nama_gelar" class="col-sm-2 control-label">Nama Gelar 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nama_gelar" id="nama_gelar" placeholder="Nama Gelar" value="<?= set_value('nama_gelar', $pegawai->nama_gelar); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="nm_unit" class="col-sm-2 control-label">Unit 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nm_unit" id="nm_unit" placeholder="Unit" value="<?= set_value('nm_unit', $pegawai->nm_unit); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="nm_person" class="col-sm-2 control-label">Nama 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nm_person" id="nm_person" placeholder="Nama" value="<?= set_value('nm_person', $pegawai->nm_person); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="no_hp" class="col-sm-2 control-label">HP 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="no_hp" id="no_hp" placeholder="HP" value="<?= set_value('no_hp', $pegawai->no_hp); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="email" class="col-sm-2 control-label">Email 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?= set_value('email', $pegawai->email); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="nm_sts_pegawai" class="col-sm-2 control-label">Status 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nm_sts_pegawai" id="nm_sts_pegawai" placeholder="Status" value="<?= set_value('nm_sts_pegawai', $pegawai->nm_sts_pegawai); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="nm_kat_pegawai" class="col-sm-2 control-label">Kategori 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nm_kat_pegawai" id="nm_kat_pegawai" placeholder="Kategori" value="<?= set_value('nm_kat_pegawai', $pegawai->nm_kat_pegawai); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="jns_pegawai" class="col-sm-2 control-label">Jenis 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="jns_pegawai" id="jns_pegawai" placeholder="Jenis" value="<?= set_value('jns_pegawai', $pegawai->jns_pegawai); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="nm_jabatan" class="col-sm-2 control-label">Jabatan 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nm_jabatan" id="nm_jabatan" placeholder="Jabatan" value="<?= set_value('nm_jabatan', $pegawai->nm_jabatan); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="nm_homebase" class="col-sm-2 control-label">Homebase 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nm_homebase" id="nm_homebase" placeholder="Homebase" value="<?= set_value('nm_homebase', $pegawai->nm_homebase); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="nm_jabatan_struktural" class="col-sm-2 control-label">Jabatan Struktural 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nm_jabatan_struktural" id="nm_jabatan_struktural" placeholder="Jabatan Struktural" value="<?= set_value('nm_jabatan_struktural', $pegawai->nm_jabatan_struktural); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="nm_unit_struktural" class="col-sm-2 control-label">Unit Struktural 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nm_unit_struktural" id="nm_unit_struktural" placeholder="Unit Struktural" value="<?= set_value('nm_unit_struktural', $pegawai->nm_unit_struktural); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="nm_unit_fungsional" class="col-sm-2 control-label">Unit Fungsional 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nm_unit_fungsional" id="nm_unit_fungsional" placeholder="Unit Fungsional" value="<?= set_value('nm_unit_fungsional', $pegawai->nm_unit_fungsional); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="nm_jabatan_fungsional" class="col-sm-2 control-label">Jabatan Fungsional 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nm_jabatan_fungsional" id="nm_jabatan_fungsional" placeholder="Jabatan Fungsional" value="<?= set_value('nm_jabatan_fungsional', $pegawai->nm_jabatan_fungsional); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="nm_prodi" class="col-sm-2 control-label">Prodi 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nm_prodi" id="nm_prodi" placeholder="Prodi" value="<?= set_value('nm_prodi', $pegawai->nm_prodi); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                
                                                 <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                 <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){
       
      
             
      $('#btn_cancel').click(function(){
        swal({
            title: "Are you sure?",
            text: "the data that you have created will be in the exhaust!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/pegawai/index/'+"<?= $this->session->userdata('_page')?>";
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_pegawai = $('#form_pegawai');
        var data_post = form_pegawai.serializeArray();
        var save_type = $(this).attr('data-stype');
        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: form_pegawai.attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          $('form').find('.form-group').removeClass('has-error');
          $('form').find('.error-input').remove();
          $('.steps li').removeClass('error');
          if(res.success) {
            var id = $('#pegawai_image_galery').find('li').attr('qq-file-id');
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            $('.data_file_uuid').val('');
    
          } else {
            if (res.errors) {
               parseErrorField(res.errors);
            }
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
       
       
       

      async function chain(){
      }
       
      chain();


    
    
    }); /*end doc ready*/
</script>