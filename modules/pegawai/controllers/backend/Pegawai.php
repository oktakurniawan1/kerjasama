<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Pegawai Controller
 *| --------------------------------------------------------------------------
 *| Pegawai site
 *|
 */
class Pegawai extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_pegawai');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * form filter data 
	 */
	public function filter_session_data()
	{
		$fields['jns_pegawai']    = $this->input->post('jns_pegawai');
		$fields['nm_kat_pegawai'] = $this->input->post('nm_kat_pegawai');
		$fields['nm_sts_pegawai'] = $this->input->post('nm_sts_pegawai');
		$this->session->set_userdata('filterdata_pegawai', $fields);
		$this->session->set_userdata('is_filtered_pegawai', TRUE);
		echo json_encode(['url' => base_url('administrator/pegawai')]);
	}

	public function clear_filter_session_data()
	{
		$this->session->unset_userdata('filterdata_pegawai');
		$this->session->unset_userdata('is_filtered_pegawai');
		echo json_encode(['url' => base_url('administrator/pegawai')]);
	}

	public function form_filter_data()
	{
		if (!$this->session->has_userdata('filterdata_pegawai')) {
			$fields['jns_pegawai']    = 'all';
			$fields['nm_kat_pegawai'] = 'all';
			$fields['nm_sts_pegawai'] = 'all';
			$this->session->set_userdata('filterdata_pegawai', $fields);
		}
		$data['jns_pegawai'] = ['dosen' => "Dosen", 'karyawan' => "Karyawan", 'all' => "Semua"];
		$nm_kat_pegawai      = [
			"BLU"         => "BLU",
			"CPNS"        => "CPNS",
			"Harian"      => "Harian",
			"Kontrak"     => "Kontrak",
			"PNS"         => "PNS",
			"PPPK"        => "PPPK",
			"Tetap"       => "Tetap",
			"Tidak Tetap" => "Tidak Tetap",
			'all'         => 'Semua'
		];
		$status_kepegawaian = [
			"Aktif"             => "Aktif",
			"Diberhentikan"     => "Diberhentikan",
			"Kontrak Berakhir"  => "Kontrak Berakhir",
			"Mengundurkan Diri" => "Mengundurkan Diri",
			"Meninggal"         => "Meninggal",
			"Mutasi"            => "Mutasi",
			"Pensiun"           => "Pensiun",
			"Pensiun Dini"      => "Pensiun Dini",
			'all'               => 'Semua'
		];
		$data['status_kepegawaian'] = $status_kepegawaian;
		$data['nm_kat_pegawai'] = $nm_kat_pegawai;
		$this->load->view('form-filter-data', $data);
	}
	/**
	 * show all Pegawais
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('pegawai_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('pegawai_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('pegawai_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('pegawai_filter');
			$this->session->unset_userdata('pegawai_field');
		}
		$filter = $this->session->userdata('pegawai_filter');
		$field = $this->session->userdata('pegawai_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['pegawais'] = $this->model_pegawai->get($filter, $field, $this->limit_page, $offset);
		$this->data['pegawai_counts'] = $this->model_pegawai->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/pegawai/index/',
			'total_rows'   => $this->model_pegawai->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Pegawai List');
		$this->render('backend/standart/administrator/pegawai/pegawai_list', $this->data);
	}


	/**
	 * Update view Pegawais
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('pegawai_update');

		$this->data['pegawai'] = $this->model_pegawai->find($id);

		$this->template->title('Pegawai Update');
		$this->render('backend/standart/administrator/pegawai/pegawai_update', $this->data);
	}

	/**
	 * Update Pegawais
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('pegawai_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}


		if ($this->form_validation->run()) {

			$save_data = [
				'nip' => $this->input->post('nip'),
				'nip_baru' => $this->input->post('nip_baru'),
				'kode_dosen' => $this->input->post('kode_dosen'),
				'nama_gelar' => $this->input->post('nama_gelar'),
				'nm_unit' => $this->input->post('nm_unit'),
				'nm_person' => $this->input->post('nm_person'),
				'no_hp' => $this->input->post('no_hp'),
				'email' => $this->input->post('email'),
				'nm_sts_pegawai' => $this->input->post('nm_sts_pegawai'),
				'nm_kat_pegawai' => $this->input->post('nm_kat_pegawai'),
				'jns_pegawai' => $this->input->post('jns_pegawai'),
				'nm_jabatan' => $this->input->post('nm_jabatan'),
				'nm_homebase' => $this->input->post('nm_homebase'),
				'nm_jabatan_struktural' => $this->input->post('nm_jabatan_struktural'),
				'nm_unit_struktural' => $this->input->post('nm_unit_struktural'),
				'nm_unit_fungsional' => $this->input->post('nm_unit_fungsional'),
				'nm_jabatan_fungsional' => $this->input->post('nm_jabatan_fungsional'),
				'nm_prodi' => $this->input->post('nm_prodi'),
			];


			$save_pegawai = $this->model_pegawai->change($id, $save_data);

			if ($save_pegawai) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/pegawai', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/pegawai/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/pegawai/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Pegawais
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('pegawai_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'pegawai'), 'success');
		} else {
			set_message(cclang('error_delete', 'pegawai'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Pegawais
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('pegawai_view');

		$this->data['pegawai'] = $this->model_pegawai->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Pegawai Detail');
		$this->render('backend/standart/administrator/pegawai/pegawai_view', $this->data);
	}

	/**
	 * delete Pegawais
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$pegawai = $this->model_pegawai->find($id);



		return $this->model_pegawai->remove($id);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
			'nip',
			'nip_baru',
			'kode_dosen',
			'nama_gelar',
			'nm_unit',
			'nm_person',
			'no_hp',
			'email',
			'nm_sts_pegawai',
			'nm_kat_pegawai',
			'jns_pegawai',
			'nm_jabatan',
			'nm_homebase',
			'nm_jabatan_struktural',
			'nm_unit_struktural',
			'nm_unit_fungsional',
			'nm_jabatan_fungsional',
			'nm_prodi',
		];
		$data = $this->model_pegawai->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_pegawai->export2($data, 'file', $selected_field);

		/*
		$this->is_allowed('pegawai_export');

		$this->model_pegawai->export(
			'pegawai', 
			'pegawai',
			$this->model_pegawai->field_search
		);
		*/
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('pegawai_export');

		$this->model_pegawai->pdf('pegawai', 'pegawai');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('pegawai_export');

		$table = $title = 'pegawai';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_pegawai->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
	public function form_cari_pegawai()
	{
		$data['pegawai'] = $this->model_pegawai->get_pegawai();
		$this->load->view('_cari_pegawai', $data);
	}
}


/* End of file pegawai.php */
/* Location: ./application/controllers/administrator/Pegawai.php */