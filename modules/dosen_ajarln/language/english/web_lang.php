<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['dosen_ajarln']    = 'Dosen Mengajar LN';
$lang['id']              = 'Id';
$lang['tgl_pelaksanaan'] = 'Tanggal Pelaksanaan';
$lang['tahun']           = 'Tahun';
$lang['nip']             = 'Nip';
$lang['kode_fak']        = 'Fakultas';
$lang['kd_prodi']        = 'Program Studi';
$lang['dest_kd_univ']    = 'Tujuan Universitas';
$lang['dest_kd_prodi']   = 'Tujuan Prodi';
$lang['nama_mk']         = 'Nama Mata Kuliah';
$lang['sks']             = 'Sks';
$lang['bobot']           = 'Bobot';
