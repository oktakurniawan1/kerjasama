<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Dosen Ajarln Controller
 *| --------------------------------------------------------------------------
 *| Dosen Ajarln site
 *|
 */
class Dosen_ajarln extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_dosen_ajarln');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * form filter data 
	 */
	public function filter_session_data()
	{
		$fields['tahun']         = $this->input->post('tahun');
		$fields['kode_fak']      = $this->input->post('kode_fak');
		$fields['kd_prodi']      = $this->input->post('kd_prodi');
		$fields['dest_kd_univ']  = $this->input->post('dest_kd_univ');
		$fields['dest_kd_prodi'] = $this->input->post('dest_kd_prodi');
		$this->session->set_userdata('filterdata_dosen_ajarln', $fields);
		$this->session->set_userdata('is_filtered_dosen_ajarln', TRUE);
		echo json_encode(['url' => base_url('administrator/dosen_ajarln')]);
	}

	public function clear_filter_session_data()
	{
		$this->session->unset_userdata('filterdata_dosen_ajarln');
		$this->session->unset_userdata('is_filtered_dosen_ajarln');
		echo json_encode(['url' => base_url('administrator/dosen_ajarln')]);
	}

	public function form_filter_data()
	{
		if (!$this->session->has_userdata('filterdata_dosen_ajarln')) {
			$fields['tahun']         = 'all';
			$fields['nip']           = 'all';
			$fields['kode_fak']      = 'all';
			$fields['kd_prodi']      = 'all';
			$fields['dest_kd_univ']  = 'all';
			$fields['dest_kd_prodi'] = 'all';
			$this->session->set_userdata('filterdata_dosen_ajarln', $fields);
		}
		$tahun        = array_combine(range(2023, date('Y')), range(2023, date('Y')));
		$tahun['all'] = 'All';

		$fakultas         = db_get_all_data('fakultas',['kode !=' => '0']);
		$fakultas         = array_column($fakultas, 'nama_singkat', 'kode');
		$fakultas['all']  = 'All';
		$data['kode_fak'] = $fakultas;
		$prodi            = db_get_all_data('ref_prodi', ['kode_fak' => $this->session->userdata('filterdata_dosen_ajarln')['kode_fak']]);
		$prodi            = array_column($prodi, 'nama_prodi', 'kode');
		$prodi['all']     = 'All';
		
		$dest_univ         = db_get_all_data('dikti_univ');
		$dest_univ         = array_column($dest_univ, 'nama_univ', 'kd_univ');
		$dest_univ['all']  = 'All';
		$dest_prodi['all'] = 'All';

		$data['tahun']         = $tahun;
		$data['kode_fak']      = $fakultas;
		$data['kd_prodi']      = $prodi;
		$data['dest_kd_univ']  = $dest_univ;
		$data['dest_kd_prodi'] = $dest_prodi;
		$this->load->view('form-filter-data', $data);
	}
	/**
	 * show all Dosen Ajarlns
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('dosen_ajarln_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('dosen_ajarln_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('dosen_ajarln_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('dosen_ajarln_filter');
			$this->session->unset_userdata('dosen_ajarln_field');
		}
		$filter = $this->session->userdata('dosen_ajarln_filter');
		$field = $this->session->userdata('dosen_ajarln_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['dosen_ajarlns'] = $this->model_dosen_ajarln->get($filter, $field, $this->limit_page, $offset);
		$this->data['dosen_ajarln_counts'] = $this->model_dosen_ajarln->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/dosen_ajarln/index/',
			'total_rows'   => $this->model_dosen_ajarln->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Dosen Ajarln List');
		$this->render('backend/standart/administrator/dosen_ajarln/dosen_ajarln_list', $this->data);
	}

	/**
	 * Add new dosen_ajarlns
	 *
	 */
	public function add()
	{
		$this->is_allowed('dosen_ajarln_add');

		$this->template->title('Dosen Ajarln New');
		$this->render('backend/standart/administrator/dosen_ajarln/dosen_ajarln_add', $this->data);
	}

	/**
	 * Add New Dosen Ajarlns
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('dosen_ajarln_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('tgl_pelaksanaan', 'Tgl Pelaksanaan', 'trim|required');
		$this->form_validation->set_rules('nip', 'Nip', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim');


		if ($this->form_validation->run()) {

			$save_data = [
				'tgl_pelaksanaan' => $this->input->post('tgl_pelaksanaan'),
				'tahun'           => date('Y', strtotime($this->input->post('tgl_pelaksanaan'))),
				'nip'             => $this->input->post('nip'),
				'kode_fak'        => $this->input->post('kode_fak'),
				'kd_prodi'        => $this->input->post('kd_prodi'),
				'dest_kd_univ'    => $this->input->post('dest_kd_univ'),
				'dest_kd_prodi'   => $this->input->post('dest_kd_prodi'),
				'nama_mk'         => $this->input->post('nama_mk'),
				'sks'             => $this->input->post('sks'),
				'bobot'           => $this->input->post('bobot'),
				'keterangan'      => $this->input->post('keterangan'),
			];


			$save_dosen_ajarln = $this->model_dosen_ajarln->store($save_data);


			if ($save_dosen_ajarln) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_dosen_ajarln;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/dosen_ajarln/edit/' . $save_dosen_ajarln, 'Edit Dosen Ajarln'),
						anchor('administrator/dosen_ajarln', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/dosen_ajarln/edit/' . $save_dosen_ajarln, 'Edit Dosen Ajarln')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dosen_ajarln');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dosen_ajarln');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Dosen Ajarlns
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('dosen_ajarln_update');

		$this->data['dosen_ajarln'] = $this->model_dosen_ajarln->find($id);

		$this->template->title('Dosen Ajarln Update');
		$this->render('backend/standart/administrator/dosen_ajarln/dosen_ajarln_update', $this->data);
	}

	/**
	 * Update Dosen Ajarlns
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('dosen_ajarln_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('tgl_pelaksanaan', 'Tgl Pelaksanaan', 'trim|required');
		$this->form_validation->set_rules('nip', 'Nip', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim');

		if ($this->form_validation->run()) {

			$save_data = [
				'tgl_pelaksanaan' => $this->input->post('tgl_pelaksanaan'),
				'tahun'           => date('Y', strtotime($this->input->post('tgl_pelaksanaan'))),
				'nip'             => $this->input->post('nip'),
				'kode_fak'        => $this->input->post('kode_fak'),
				'kd_prodi'        => $this->input->post('kd_prodi'),
				'dest_kd_univ'    => $this->input->post('dest_kd_univ'),
				'dest_kd_prodi'   => $this->input->post('dest_kd_prodi'),
				'nama_mk'         => $this->input->post('nama_mk'),
				'sks'             => $this->input->post('sks'),
				'bobot'           => $this->input->post('bobot'),
				'keterangan'      => $this->input->post('keterangan'),
			];


			$save_dosen_ajarln = $this->model_dosen_ajarln->change($id, $save_data);

			if ($save_dosen_ajarln) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/dosen_ajarln', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dosen_ajarln/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dosen_ajarln/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Dosen Ajarlns
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('dosen_ajarln_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'dosen_ajarln'), 'success');
		} else {
			set_message(cclang('error_delete', 'dosen_ajarln'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Dosen Ajarlns
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('dosen_ajarln_view');

		$this->data['dosen_ajarln'] = $this->model_dosen_ajarln->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Dosen Ajarln Detail');
		$this->render('backend/standart/administrator/dosen_ajarln/dosen_ajarln_view', $this->data);
	}

	/**
	 * delete Dosen Ajarlns
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$dosen_ajarln = $this->model_dosen_ajarln->find($id);



		return $this->model_dosen_ajarln->remove($id);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
			'tgl_pelaksanaan',
			'nip',
			'kode_fak',
			'kd_prodi',
			'dest_kd_univ',
			'dest_kd_prodi',
			'nama_mk',
			'sks',
			'bobot',
		];
		$data = $this->model_dosen_ajarln->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_dosen_ajarln->export2($data, 'file', $selected_field);

		/*
		$this->is_allowed('dosen_ajarln_export');

		$this->model_dosen_ajarln->export(
			'dosen_ajarln', 
			'dosen_ajarln',
			$this->model_dosen_ajarln->field_search
		);
		*/
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('dosen_ajarln_export');

		$this->model_dosen_ajarln->pdf('dosen_ajarln', 'dosen_ajarln');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('dosen_ajarln_export');

		$table = $title = 'dosen_ajarln';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_dosen_ajarln->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}

	public function ajax_kd_prodi($id = null)
	{
		if (!$this->is_allowed('dosen_ajarln_list', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}
		$results = db_get_all_data('ref_prodi', ['kode_fak' => $id]);
		$this->response($results);
	}

	public function ajax_dest_kd_prodi($id = null)
	{
		if (!$this->is_allowed('dosen_ajarln_list', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}
		$results = db_get_all_data('dikti_prodi', ['kd_univ' => $id]);
		$this->response($results);
	}
}


/* End of file dosen_ajarln.php */
/* Location: ./application/controllers/administrator/Dosen Ajarln.php */