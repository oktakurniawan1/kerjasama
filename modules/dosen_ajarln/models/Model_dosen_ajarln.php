<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_dosen_ajarln extends MY_Model
{

    private $primary_key  = 'id';
    private $table_name   = 'dosen_ajarln';
    public  $field_search = ['tgl_pelaksanaan', 'tahun', 'nip', 'kode_fak', 'kd_prodi', 'dest_kd_univ', 'dest_kd_prodi', 'nama_mk', 'sks', 'bobot', 'keterangan'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
        );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "dosen_ajarln." . $field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "dosen_ajarln." . $field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '(' . $where . ')';
        } else {
            $where .= "(" . "dosen_ajarln." . $field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);

        /**form filter data */
        if ($this->session->has_userdata('filterdata_dosen_ajarln')) {
            foreach (fromsess('filterdata_dosen_ajarln') as $key => $row) {
                if ($row != 'all') $this->db->having($key, $row);
            }
        }
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [], $result = TRUE)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "dosen_ajarln." . $field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "dosen_ajarln." . $field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '(' . $where . ')';
        } else {
            $where .= "(" . "dosen_ajarln." . $field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) and count($select_field)) {
            $this->db->select($select_field);
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);

        /**form filter data */
        if ($this->session->has_userdata('filterdata_dosen_ajarln')) {
            foreach (fromsess('filterdata_dosen_ajarln') as $key => $row) {
                if ($row != 'all') $this->db->having($key, $row);
            }
        }
        $this->db->limit($limit, $offset);

        $this->sortable();

        $query = $this->db->get($this->table_name);

        if ($result)
            return $query->result();
        else return $query;
    }

    public function join_avaiable()
    {
        $this->db->join('pegawai', 'pegawai.nip = dosen_ajarln.nip', 'LEFT');
        $this->db->join('fakultas', 'fakultas.kode = dosen_ajarln.kode_fak', 'LEFT');
        $this->db->join('ref_prodi', 'ref_prodi.kode = dosen_ajarln.kd_prodi', 'LEFT');
        $this->db->join('dikti_univ', 'dikti_univ.kd_univ = dosen_ajarln.dest_kd_univ', 'LEFT');
        $this->db->join('dikti_prodi', 'dikti_prodi.kd_prodi = dosen_ajarln.dest_kd_prodi AND dikti_prodi.kd_univ = dikti_univ.kd_univ', 'LEFT');
        $this->db->select('dosen_ajarln.*,pegawai.nama_gelar as pegawai_nama_gelar,fakultas.nama_singkat as fakultas_nama_singkat,ref_prodi.nama_prodi as ref_prodi_nama_prodi,dikti_univ.nama_univ as dikti_univ_nama_univ,dikti_prodi.nama_prodi as dikti_prodi_nama_prodi');


        return $this;
    }

    public function filter_avaiable()
    {

        if (!$this->aauth->is_admin()) {
        }

        return $this;
    }

    public function add_save_methods()
    {
        $save_data = [
            // ...existing fields...
            'keterangan' => $this->input->post('keterangan'),
        ];

        // ...existing code...
    }

    public function edit_save_methods()
    {
        $save_data = [
            // ...existing fields...
            'keterangan' => $this->input->post('keterangan'),
        ];

        // ...existing code...
    }
}

/* End of file Model_dosen_ajarln.php */
/* Location: ./application/models/Model_dosen_ajarln.php */