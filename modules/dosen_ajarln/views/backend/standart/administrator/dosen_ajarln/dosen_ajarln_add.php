<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dosen Ajarln <small><?= cclang('new', ['Dosen Ajarln']); ?> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="<?= site_url('administrator/dosen_ajarln'); ?>">Dosen Ajarln</a></li>
        <li class="active"><?= cclang('new'); ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Dosen Ajarln</h3>
                            <h5 class="widget-user-desc"><?= cclang('new', ['Dosen Ajarln']); ?></h5>
                            <hr>
                        </div>
                        <?= form_open('', [
                            'name'    => 'form_dosen_ajarln',
                            'class'   => 'form-horizontal form-step',
                            'id'      => 'form_dosen_ajarln',
                            'enctype' => 'multipart/form-data',
                            'method'  => 'POST'
                        ]); ?>

                        <div class="form-group ">
                            <label for="tgl_pelaksanaan" class="col-sm-2 control-label">Tanggal Pelaksanaan
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                                <div class="input-group date col-sm-8">
                                    <input type="text" class="form-control pull-right datepicker" name="tgl_pelaksanaan" placeholder="Tgl Pelaksanaan" id="tgl_pelaksanaan">
                                </div>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="nip" class="col-sm-2 control-label">Nip
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control chosen chosen-select-deselect" name="nip" id="nip" data-placeholder="Select Nip">
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('pegawai') as $row): ?>
                                        <option value="<?= $row->nip ?>"><?= $row->nip . ' - ' . $row->nama_gelar; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>


                        <div class="form-group ">
                            <label for="kode_fak" class="col-sm-2 control-label">Fakultas
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control chosen chosen-select-deselect" name="kode_fak" id="kode_fak" data-placeholder="Select Kode Fak">
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('fakultas') as $row): ?>
                                        <option value="<?= $row->kode ?>"><?= $row->nama_singkat; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>


                        <div class="form-group ">
                            <label for="kd_prodi" class="col-sm-2 control-label">Program Studi
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control chosen chosen-select-deselect" name="kd_prodi" id="kd_prodi" data-placeholder="Select Kd Prodi">
                                    <option value=""></option>
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>


                        <div class="form-group ">
                            <label for="dest_kd_univ" class="col-sm-2 control-label">Universitas Tujuan
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control chosen chosen-select-deselect" name="dest_kd_univ" id="dest_kd_univ" data-placeholder="Select Dest Kd Univ">
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('dikti_univ') as $row): ?>
                                        <option value="<?= $row->kd_univ ?>"><?= $row->nama_univ; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>


                        <div class="form-group ">
                            <label for="dest_kd_prodi" class="col-sm-2 control-label">Program Studi Tujuan
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control chosen chosen-select-deselect" name="dest_kd_prodi" id="dest_kd_prodi" data-placeholder="Select Dest Kd Prodi">
                                    <option value=""></option>
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>


                        <div class="form-group ">
                            <label for="nama_mk" class="col-sm-2 control-label">Nama Matakuiah
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nama_mk" id="nama_mk" placeholder="Nama Mk" value="<?= set_value('nama_mk'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="sks" class="col-sm-2 control-label">Sks
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="sks" id="sks" placeholder="Sks" value="<?= set_value('sks'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="bobot" class="col-sm-2 control-label">Bobot
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="bobot" id="bobot" placeholder="Bobot" value="<?= set_value('bobot'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="keterangan" class="col-sm-2 control-label">Keterangan
                            </label>
                            <div class="col-sm-8">
                                <textarea class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan" rows="2"><?= set_value('keterangan'); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                        <div class="message"></div>
                        <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                                <i class="fa fa-save"></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                                <i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                                <i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                                <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
                                <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function() {


        $('#btn_cancel').click(function() {
            swal({
                    title: "<?= cclang('are_you_sure'); ?>",
                    text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes!",
                    cancelButtonText: "No!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                        window.location.href = BASE_URL + 'administrator/dosen_ajarln';
                    }
                });

            return false;
        }); /*end btn cancel*/

        $('.btn_save').click(function() {
            $('.message').fadeOut();

            var form_dosen_ajarln = $('#form_dosen_ajarln');
            var data_post = form_dosen_ajarln.serializeArray();
            var save_type = $(this).attr('data-stype');

            data_post.push({
                name: 'save_type',
                value: save_type
            });

            $('.loading').show();

            $.ajax({
                    url: BASE_URL + '/administrator/dosen_ajarln/add_save',
                    type: 'POST',
                    dataType: 'json',
                    data: data_post,
                })
                .done(function(res) {
                    $('form').find('.form-group').removeClass('has-error');
                    $('.steps li').removeClass('error');
                    $('form').find('.error-input').remove();
                    if (res.success) {

                        if (save_type == 'back') {
                            window.location.href = res.redirect;
                            return;
                        }

                        $('.message').printMessage({
                            message: res.message
                        });
                        $('.message').fadeIn();
                        resetForm();
                        $('.chosen option').prop('selected', false).trigger('chosen:updated');

                    } else {
                        if (res.errors) {

                            $.each(res.errors, function(index, val) {
                                $('form #' + index).parents('.form-group').addClass('has-error');
                                $('form #' + index).parents('.form-group').find('small').prepend(`
                      <div class="error-input">` + val + `</div>
                      `);
                            });
                            $('.steps li').removeClass('error');
                            $('.content section').each(function(index, el) {
                                if ($(this).find('.has-error').length) {
                                    $('.steps li:eq(' + index + ')').addClass('error').find('a').trigger('click');
                                }
                            });
                        }
                        $('.message').printMessage({
                            message: res.message,
                            type: 'warning'
                        });
                    }

                })
                .fail(function() {
                    $('.message').printMessage({
                        message: 'Error save data',
                        type: 'warning'
                    });
                })
                .always(function() {
                    $('.loading').hide();
                    $('html, body').animate({
                        scrollTop: $(document).height()
                    }, 2000);
                });

            return false;
        }); /*end btn save*/





        $('#kode_fak').change(function(event) {
            var val = $(this).val();
            $.LoadingOverlay('show')
            $.ajax({
                    url: BASE_URL + '/administrator/dosen_ajarln/ajax_kd_prodi/' + val,
                    dataType: 'JSON',
                })
                .done(function(res) {
                    var html = '<option value=""></option>';
                    $.each(res, function(index, val) {
                        html += '<option value="' + val.kode + '">' + val.nama_prodi + '</option>'
                    });
                    $('#kd_prodi').html(html);
                    $('#kd_prodi').trigger('chosen:updated');

                })
                .fail(function() {
                    toastr['error']('Error', 'Getting data fail')
                })
                .always(function() {
                    $.LoadingOverlay('hide')
                });

        });

        $('#dest_kd_univ').change(function(event) {
            var val = $(this).val();
            $.LoadingOverlay('show')
            $.ajax({
                    url: BASE_URL + '/administrator/dosen_ajarln/ajax_dest_kd_prodi/' + val,
                    dataType: 'JSON',
                })
                .done(function(res) {
                    var html = '<option value=""></option>';
                    $.each(res, function(index, val) {
                        html += '<option value="' + val.kd_prodi + '">' + val.nama_prodi + ' - ' + val.jenjang + '</option>'
                    });
                    $('#dest_kd_prodi').html(html);
                    $('#dest_kd_prodi').trigger('chosen:updated');

                })
                .fail(function() {
                    toastr['error']('Error', 'Getting data fail')
                })
                .always(function() {
                    $.LoadingOverlay('hide')
                });

        });




    }); /*end doc ready*/
</script>