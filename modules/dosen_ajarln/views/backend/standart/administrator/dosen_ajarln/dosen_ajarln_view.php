<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
      Dosen Ajarln <small><?= cclang('new', ['Dosen Ajarln']); ?> </small>
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class=""><a href="<?= site_url('administrator/dosen_ajarln'); ?>">Dosen Ajarln</a></li>
      <li class="active"><?= cclang('detail'); ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">

      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">

               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">

                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/view.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Dosen Ajarln</h3>
                     <h5 class="widget-user-desc"><?= cclang('new', ['Dosen Ajarln']); ?></h5>
                     <hr>
                  </div>


                  <div class="form-horizontal form-step" name="form_dosen_ajarln" id="form_dosen_ajarln">

                     <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Id </label>

                        <div class="col-sm-8">
                           <?= _ent($dosen_ajarln->id); ?>
                        </div>
                     </div>

                     <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Tanggal Pelaksanaan </label>

                        <div class="col-sm-8">
                           <?= _ent($dosen_ajarln->tgl_pelaksanaan); ?>
                        </div>
                     </div>

                     <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Tahun </label>

                        <div class="col-sm-8">
                           <?= _ent($dosen_ajarln->tahun); ?>
                        </div>
                     </div>

                     <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Nip </label>

                        <div class="col-sm-8">
                           <?= _ent($dosen_ajarln->pegawai_nama_gelar); ?>
                        </div>
                     </div>

                     <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Fakultas </label>

                        <div class="col-sm-8">
                           <?= _ent($dosen_ajarln->fakultas_nama_singkat); ?>
                        </div>
                     </div>

                     <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Program Studi </label>

                        <div class="col-sm-8">
                           <?= _ent($dosen_ajarln->ref_prodi_nama_prodi); ?>
                        </div>
                     </div>

                     <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Tujuan Universitas </label>

                        <div class="col-sm-8">
                           <?= _ent($dosen_ajarln->dikti_univ_nama_univ); ?>
                        </div>
                     </div>

                     <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Tujuan Program Prodi </label>

                        <div class="col-sm-8">
                           <?= _ent($dosen_ajarln->dikti_prodi_nama_prodi); ?>
                        </div>
                     </div>

                     <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Nama Matakuliah </label>

                        <div class="col-sm-8">
                           <?= _ent($dosen_ajarln->nama_mk); ?>
                        </div>
                     </div>

                     <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Sks </label>

                        <div class="col-sm-8">
                           <?= _ent($dosen_ajarln->sks); ?>
                        </div>
                     </div>

                     <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Bobot </label>

                        <div class="col-sm-8">
                           <?= _ent($dosen_ajarln->bobot); ?>
                        </div>
                     </div>

                     <div class="form-group ">
                        <label for="keterangan" class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-8">
                           <?= _ent($dosen_ajarln->keterangan); ?>
                        </div>
                     </div>

                     <br>
                     <br>
                     <div class="view-nav">
                        <?php is_allowed('dosen_ajarln_update', function () use ($dosen_ajarln) { ?>
                           <a class="btn btn-flat btn-info btn_edit btn_action" id="btn_edit" data-stype='back' title="edit dosen_ajarln (Ctrl+e)" href="<?= site_url('administrator/dosen_ajarln/edit/' . $dosen_ajarln->id); ?>"><i class="fa fa-edit"></i> <?= cclang('update', ['Dosen Ajarln']); ?> </a>
                        <?php }) ?>
                        <a class="btn btn-flat btn-default btn_action" id="btn_back" title="back (Ctrl+x)" href="<?= site_url('administrator/dosen_ajarln/index/' . $this->session->userdata('_page')); ?>"><i class="fa fa-undo"></i> <?= cclang('go_list_button', ['Dosen Ajarln']); ?></a>
                     </div>

                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->

      </div>
   </div>
</section>
<!-- /.content -->
<script>
   $(document).ready(function() {

   });
</script>