<form action="" method="POST" role="form">
   <div class="row">
      <div class="col-md-6">
         <div class="form-group">
            <label for="">Tahun</label>
            <?= form_dropdown('tahun', $tahun, fromsess('filterdata_dosen_ajarln')['tahun'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
         </div>
         <div class="form-group">
            <label for="">Fakultas</label>
            <?= form_dropdown('kode_fak', $kode_fak, fromsess('filterdata_dosen_ajarln')['kode_fak'], ['class' => 'form-control', 'style' => 'width:100%', 'onchange' => 'updateProdi()']) ?>
         </div>
         <div class="form-group">
            <label for="">Program Studi</label>
            <?= form_dropdown('kd_prodi', $kd_prodi, fromsess('filterdata_dosen_ajarln')['kd_prodi'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
         </div>
      </div>
      <div class="col-md-6">
         <div class="form-group">
            <label for="">Universitas Tujuan</label>
            <?= form_dropdown('dest_kd_univ', $dest_kd_univ, fromsess('filterdata_dosen_ajarln')['dest_kd_univ'], ['class' => 'form-control', 'style' => 'width:100%', 'onchange' => 'updateDestProdi()']) ?>
         </div>
         <div class="form-group">
            <label for="">Program Studi Tujuan</label>
            <?= form_dropdown('dest_kd_prodi', $dest_kd_prodi, fromsess('filterdata_dosen_ajarln')['dest_kd_prodi'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
         </div>
      </div>
   </div>
</form>

<script>
   $(document).ready(function() {
      $('select[name="kode_fak"]').select2({
         theme: 'classic',
         dropdownParent: $('#filterdosen_ajarln-dialog'),
      });
      $('select[name="kd_prodi"]').select2({
         theme: 'classic',
         dropdownParent: $('#filterdosen_ajarln-dialog'),
      });
      $('select[name="dest_kd_univ"]').select2({
         theme: 'classic',
         dropdownParent: $('#filterdosen_ajarln-dialog'),
      });
      $('select[name="dest_kd_prodi"]').select2({
         theme: 'classic',
         dropdownParent: $('#filterdosen_ajarln-dialog'),
      });
   });

   function updateProdi() {
      var kode_fak = $('select[name="kode_fak"]').val();
      $.ajax({
         url: '<?= base_url("administrator/dosen_ajarln/ajax_kd_prodi/") ?>' + kode_fak,
         type: 'GET',
         dataType: 'json',
         success: function(data) {
            var options = '<option value="all">All</option>';
            $.each(data, function(index, element) {
               options += '<option value="' + element.kode + '">' + element.kode + ' - ' + element.nama_prodi + '</option>';
            });
            $('select[name="kd_prodi"]').html(options);
         }
      });
   }

   function updateDestProdi() {
      var dest_kd_univ = $('select[name="dest_kd_univ"]').val();
      $.ajax({
         url: '<?= base_url("administrator/dosen_ajarln/ajax_dest_kd_prodi/") ?>' + dest_kd_univ,
         type: 'GET',
         dataType: 'json',
         success: function(data) {
            var options = '<option value="all">All</option>';
            console.log(data);
            $.each(data, function(index, element) {
               options += '<option value="' + element.kd_prodi + '">' + element.nama_prodi + ' - ' + element.jenjang + '</option>';
            });
            $('select[name="dest_kd_prodi"]').html(options);
         }
      });
   }
</script>