<section class="content-header">
    <h1>
        <?= cclang('user') ?>
        <small><?= cclang('Login as', cclang('user')) ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> <?= cclang('home') ?></a></li>
        <li class=""><a href="<?= site_url('administrator/user'); ?>"><?= cclang('user') ?></a></li>
        <li class="active"><?= cclang('login as') ?></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-10">
            <div class="box box-warning">
                <div class="box-body ">
                    <form class="form-horizontal" method="GET" action="<?= site_url('administrator/user/login_as_user'); ?>">
                        <div class="form-group ">
                            <label for="" class="col-sm-2 control-label">Cari Pegawai</label>
                            <div class="col-sm-10">
                                <select class="form-control chosen chosen-select-deselect" name="pegawai" id="pegawai" data-placeholder="cari nip,nama pegawai...">
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('pegawai') as $row) :
                                        $data = "data-nip='$row->nip_baru' data-nama='$row->nama_gelar'"; ?>
                                        <option <?= $data ?> value="<?= $row->nip_baru ?>"><?= $row->jns_pegawai . ' - ' . $row->nip_baru . ' - ' . $row->nama_gelar; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="prodi" class="col-sm-2 control-label">Prodi</label>
                            <div class="col-sm-10">
                                <select class="form-control chosen chosen-select-deselect" name="kd_prodi" id="kd_prodi" data-placeholder="Select Program Studi / Unit">
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('ref_prodi') as $row) : ?>
                                        <option value="<?= $row->kode ?>"><?= $row->kode . ' - ' . $row->nama_prodi; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <small class="info help-block">Isian prodi untuk session user.</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary"><?= cclang('submit') ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>