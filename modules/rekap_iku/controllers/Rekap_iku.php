<?php

use GuzzleHttp\Promise\Is;

defined('BASEPATH') or exit('No direct script access allowed');

class Rekap_iku extends Admin
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Model_rekap_iku');
	}
	public function index()
	{
	}

	public function unit_jml_doc()
	{
		$from = $this->input->get('from');
		$to   = $this->input->get('to');
		$unit = $this->input->get('unit');
		$data = $this->Model_rekap_iku->get_jml_doc_unit($from, $to, $unit);
		$tmp  = [];
		foreach ($data as $row) {
			//jika key prodi blm ada, buat key kd_prodi,kat,nama prodi
			if (!isset($tmp[$row->kd_prodi])) {
				$tmp[$row->kd_prodi]['nama_prodi'] = $row->nama_prodi;
				$tmp[$row->kd_prodi]['jml_thn'] = $row->jml_thn;
				//buat key kategori (kat)
				$tmp[$row->kd_prodi]['kat'] = [
					$row->maping_id_iku => [
						'jml_doc' => $row->jml_doc,
						'skor_doc' => ($row->jml_doc * $row->bobot) / 10, //bobot dokumen
						'skor_kategori' => $row->jml_thn * $row->bobot_kategori * $row->jml_prodi,
					]
				];
			} else {
				//jika key kd_prodi sudah ada, insert data kategorinya
				$tmp[$row->kd_prodi]['kat'][$row->maping_id_iku] = [
					'jml_doc' => $row->jml_doc,
					'skor_doc' => ($row->jml_doc * $row->bobot) / 10,
					'skor_kategori' => $row->jml_thn * $row->bobot_kategori * $row->jml_prodi,
				];
			}
		}
		// dd($tmp);

		//tambahkan kategori yg tidak ada dan rekap hasil
		$refkat = db_get_all_data('iku_ks');
		$hasil = [];
		$i = 0;
		foreach ($tmp as $p => $row) {
			$hasil[$i] = [
				'Kode' => $p,
				($unit == 'prodi' ? 'Program Studi' : 'Fakultas') => $row['nama_prodi'],
			];
			$total_doc = 0;
			$total_skor_doc = 0;
			$total_skor_kategori = 0;
			foreach ($refkat as $k) {
				//jika tidak memiliki kategori
				if (!array_key_exists($k->id, $tmp[$p]['kat'])) {
					$hasil[$i][$k->nama_iku_ks] = 0;
					$skor_doc[$i][$k->id] = 0;
					$skor_kategori[$i][$k->id] = 0;
				} else {
					$hasil[$i][$k->nama_iku_ks] = $row['kat'][$k->id]['jml_doc'];
					$skor_doc[$i][$k->id] = $row['kat'][$k->id]['skor_doc'];
					$skor_kategori[$i][$k->id]  = $row['kat'][$k->id]['skor_kategori'];
				}

				$total_doc += $hasil[$i][$k->nama_iku_ks];
				$total_skor_doc += $skor_doc[$i][$k->id];
				$total_skor_kategori += $skor_kategori[$i][$k->id];
			}
			$hasil[$i]['Total Dokumen'] = $total_doc;
			$hasil[$i]['Skor Dokumen'] = $total_skor_doc;
			$hasil[$i]['Jml Tahun'] = $row['jml_thn'];
			$hasil[$i]['Skor Kategori'] = $total_skor_kategori;
			$hasil[$i]['Total Skor'] = $total_skor_doc + $total_skor_kategori;
			$i++;
		}
		if(empty($hasil)) die('tidak ada data');
		$selected_field = array_keys($hasil[0]);
		//format = ['doc_file'=>'link','doc_mou'=>'link'];
		export_excel($hasil, 'file', $selected_field, []);
	}
	public function unit_detail_mitra()
	{
		$from = $this->input->get('from');
		$to = $this->input->get('to');
		$unit = $this->input->get('unit');
		$data = $this->Model_rekap_iku->get_detail_mitra($from, $to, $unit);
		$tmp = [];
		foreach ($data as $row) {
			//jika key prodi blm ada, buat key kd_prodi,kat,nama prodi
			if (!isset($tmp[$row->kd_prodi])) {
				$tmp[$row->kd_prodi]['nama_prodi'] = $row->nama_prodi;
				//buat key kategori (kat)
				$tmp[$row->kd_prodi]['kat'] = [
					$row->maping_id_iku => [
						'daftar_mitra' => [$row->nama_mitra]
					]
				];
			} else {
				//jika key kd_prodi sudah ada, insert data kategorinya
				$tmp[$row->kd_prodi]['kat'][$row->maping_id_iku]['daftar_mitra'][] = $row->nama_mitra;
			}
		}
		// dd($tmp);

		//tambahkan kategori yg tidak ada dan rekap hasil
		$refkat = db_get_all_data('iku_ks');
		$hasil = [];
		$i = 0;
		foreach ($tmp as $p => $row) {
			$hasil[$i] = [
				'Kode' => $p,
				($unit == 'prodi' ? 'Program Studi' : 'Fakultas') => $row['nama_prodi'],
			];
			foreach ($refkat as $k) {
				//jika tidak memiliki kategori
				if (!array_key_exists($k->id, $tmp[$p]['kat'])) {
					$hasil[$i][$k->nama_iku_ks] = '';
				} else {
					$hasil[$i][$k->nama_iku_ks] = $row['kat'][$k->id]['daftar_mitra'];
				}
			}
			$i++;
		}

		// Load PHPExcel library
		$this->load->library('excel');

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set active sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$huruf = 'ABCDEFGHIJKLMOPQRSTUVWXYZ';

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Your name")
			->setLastModifiedBy("Your name")
			->setTitle("Export Data")
			->setSubject("Export Data")
			->setDescription("Export Data")
			->setKeywords("export, data");

		// Add header style
		$header_style = array(
			'font' => array(
				'bold' => true,
				'color' => array('rgb' => 'FFFFFF'),
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array('rgb' => 'DA3232'),
			),
		);

		// Add header
		// Set header value
		if(empty($hasil)) die('tidak ada data');
		$header = array_keys($hasil[0]);
		$objPHPExcel->getActiveSheet()->fromArray($header, null, 'A1');

		// Apply header style to cell
		$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->applyFromArray($header_style);

		// Add data
		$row = 2;
		$end_rows = [];
		foreach ($hasil as $h => $row_data) {
			$col = 0;
			foreach ($row_data as $i => $field) {
				if (is_array($field)) {
					$val_row = $row;
					foreach ($field as $m => $mitra) {
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $val_row, ($m + 1) . '. ' . $mitra);
						$val_row++;
					}
					$end_rows[] = $val_row;
				} else {
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $row_data[$i]);
				}
				$col++;
			}
			$row = max($end_rows);
			$row++;
		}
		//set border
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		$objPHPExcel->getActiveSheet()->getStyle("A1:" . $huruf[count($header) - 1] . ($row - 2))->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:' . $huruf[count($header) - 1] . ($row - 2))->getAlignment()->setWrapText(true);
		// set widht
		foreach ($header as $i => $h) {
			if ($i > 0) $objPHPExcel->getActiveSheet()->getColumnDimension($huruf[$i])->setWidth(50);
		}

		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Data');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Redirect output to a client's web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="data.xls"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}
	public function generate_excel()
	{
		$this->load->library('excel');
		$data = [
			0 => [
				'nim' => '01',
				'nama' => 'imam',
				'alamat' => 'semarang',
				'pendidikan' => ['SD', 'SMP', 'SMA']
			],
			1 => [
				'nim' => '02',
				'nama' => 'agung',
				'alamat' => 'semarang',
				'pendidikan' => ['SD', 'SMP']
			],
			2 => [
				'nim' => '03',
				'nama' => 'budi',
				'alamat' => 'semarang',
				'pendidikan' => ['SD', 'SMP', 'SMA', 'D3']
			],
			3 => [
				'nim' => '03',
				'nama' => 'budi',
				'alamat' => 'semarang',
				'pendidikan' => ['SD', 'SMP', 'SMA', 'S1']
			],
		];

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Your name")
			->setLastModifiedBy("Your name")
			->setTitle("Export Data")
			->setSubject("Export Data")
			->setDescription("Export Data")
			->setKeywords("export, data");

		// Add header style
		$header_style = array(
			'font' => array(
				'bold' => true,
				'color' => array('rgb' => 'FFFFFF'),
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array('rgb' => '808080'),
			),
		);

		// Add header
		// Set header value
		$header = array_keys($data[0]);
		$objPHPExcel->getActiveSheet()->fromArray($header, null, 'A1');

		// Apply header style to cell
		$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($header_style);

		// Add data
		$row = 2;
		foreach ($data as $item) {
			$col = 0;
			foreach ($item as $key => $value) {
				if (is_array($value) && !empty($value)) {
					$val_row = $row;
					foreach ($value as $val) {
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $val_row, $val);
						$val_row++;
					}
					$col++;
				} else {
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
					$col++;
				}
			}
			$row = $val_row;
		}
		//set border
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D' . $val_row)->applyFromArray($styleArray);

		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Data');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Redirect output to a client's web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="data.xls"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}
}
