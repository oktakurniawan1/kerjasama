<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_rekap_iku extends CI_Model {

	public function __construct(){
		parent::__construct();
	}
	
	public function get_jml_doc_unit($from,$to,$unit = 'prodi'){
		$from = date('Y-m-d',strtotime($from));
		$to = date('Y-m-t',strtotime($to));
		$sql_prodi ="SELECT
					U.kd_prodi,
					p.nama_prodi,
					m.id_iku AS maping_id_iku,
					w.nama_iku_ks AS maping_ks_iku,
					count(U.id) as jml_doc,
					count(DISTINCT U.tahun) as jml_thn,
					count(DISTINCT U.kd_prodi) AS jml_prodi,
					w.bobot,
					w.bobot_kategori
				FROM
					si_kerjasama AS U
					INNER JOIN ( si_jenis_ks AS j, ref_prodi AS p, iku_map_ks m, iku_ks w ) ON (
						U.jenis_ks = j.id 
						AND U.kd_prodi = p.kode 
						AND m.id_jenis_ks = j.id 
						AND w.id = m.id_iku 
						AND ( U.doc_tanggal BETWEEN ? AND ? ) 
						AND p.tingkat = 3
					)
				GROUP BY U.kd_prodi,m.id_iku";

		$sql_fak ="SELECT
					f.nama_singkat AS kd_prodi,
					f.nama AS nama_prodi,
					m.id_iku AS maping_id_iku,
					w.nama_iku_ks AS maping_ks_iku,
					count(U.id) as jml_doc,
					count(DISTINCT U.tahun) as jml_thn,
					count(DISTINCT U.kd_prodi) AS jml_prodi,
					w.bobot,
					w.bobot_kategori
				FROM
					si_kerjasama AS U
					INNER JOIN ( si_jenis_ks AS j, ref_prodi AS p, iku_map_ks m, iku_ks w, fakultas f ) ON (
						U.jenis_ks = j.id 
						AND U.kd_prodi = p.kode
						AND p.kode_fak = f.kode
						AND m.id_jenis_ks = j.id 
						AND w.id = m.id_iku 
						AND ( U.doc_tanggal BETWEEN ? AND ? ) 
						AND p.tingkat IN (3)
					)
				GROUP BY f.kode,m.id_iku";

		return $this->db->query(($unit == 'prodi' ? $sql_prodi : $sql_fak),[$from,$to])->result();
	}
	public function get_detail_mitra($from,$to,$unit = 'prodi'){
		$from = date('Y-m-d',strtotime($from));
		$to = date('Y-m-t',strtotime($to));
		$sql_prodi ="SELECT
					U.kd_prodi,
					p.nama_prodi,
					concat(U.nama_mitra,' - ',U.doc_nomor) AS nama_mitra,
					m.id_iku AS maping_id_iku,
					w.nama_iku_ks AS maping_ks_iku
				FROM
					si_kerjasama AS U
					INNER JOIN ( si_jenis_ks AS j, ref_prodi AS p, iku_map_ks m, iku_ks w ) ON (
						U.jenis_ks = j.id 
						AND U.kd_prodi = p.kode 
						AND m.id_jenis_ks = j.id 
						AND w.id = m.id_iku 
						AND ( U.doc_tanggal BETWEEN ? AND ? ) 
						AND p.tingkat = 3
					) ORDER BY U.nama_mitra";

		$sql_fak ="SELECT
					f.nama_singkat AS kd_prodi,
					f.nama AS nama_prodi,
					concat(U.nama_mitra,' - ',U.doc_nomor) AS nama_mitra,
					m.id_iku AS maping_id_iku,
					w.nama_iku_ks AS maping_ks_iku
				FROM
					si_kerjasama AS U
					INNER JOIN ( si_jenis_ks AS j, ref_prodi AS p, iku_map_ks m, iku_ks w, fakultas f ) ON (
						U.jenis_ks = j.id 
						AND U.kd_prodi = p.kode
						AND p.kode_fak = f.kode
						AND m.id_jenis_ks = j.id 
						AND w.id = m.id_iku 
						AND ( U.doc_tanggal BETWEEN ? AND ? ) 
						AND p.tingkat = 3
					) ORDER BY U.nama_mitra";
		return $this->db->query(($unit == 'prodi' ? $sql_prodi : $sql_fak),[$from,$to])->result();
	}
}
?>