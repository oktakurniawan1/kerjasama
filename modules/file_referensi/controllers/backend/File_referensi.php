<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| File Referensi Controller
*| --------------------------------------------------------------------------
*| File Referensi site
*|
*/
class File_referensi extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_file_referensi');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all File Referensis
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('file_referensi_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if(!is_null($q)) $this->session->set_userdata('file_referensi_filter',$q);
		if(!is_null($f)) $this->session->set_userdata('file_referensi_field',$f);
		$this->session->set_userdata('_page',$offset);

		if(!is_null($this->input->get('reset_filter'))){
			$this->session->unset_userdata('file_referensi_filter');
			$this->session->unset_userdata('file_referensi_field');
		}
		$filter = $this->session->userdata('file_referensi_filter');
		$field = $this->session->userdata('file_referensi_field');
		$offset = $this->session->userdata('_page',$offset);

		$this->data['file_referensis'] = $this->model_file_referensi->get($filter, $field, $this->limit_page, $offset);
		$this->data['file_referensi_counts'] = $this->model_file_referensi->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/file_referensi/index/',
			'total_rows'   => $this->model_file_referensi->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('File Referensi List');
		$this->render('backend/standart/administrator/file_referensi/file_referensi_list', $this->data);
	}
	
	/**
	* Add new file_referensis
	*
	*/
	public function add()
	{
		$this->is_allowed('file_referensi_add');

		$this->template->title('File Referensi New');
		$this->render('backend/standart/administrator/file_referensi/file_referensi_add', $this->data);
	}

	/**
	* Add New File Referensis
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('file_referensi_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('file_referensi_file_ref_name', 'File Ref', 'trim|required');
		

		if ($this->form_validation->run()) {
			$file_referensi_file_ref_uuid = $this->input->post('file_referensi_file_ref_uuid');
			$file_referensi_file_ref_name = $this->input->post('file_referensi_file_ref_name');
		
			$save_data = [
				'deskripsi' => $this->input->post('deskripsi'),
				'tgl_upload' => date('Y-m-d'),
				'uploader' => get_user_data('full_name'),
			];

			if (!is_dir(FCPATH . '/uploads/file_referensi/')) {
				mkdir(FCPATH . '/uploads/file_referensi/');
			}

			if (!empty($file_referensi_file_ref_name)) {
				$file_referensi_file_ref_name_copy = date('YmdHis') . '-' . $file_referensi_file_ref_name;

				rename(FCPATH . 'uploads/tmp/' . $file_referensi_file_ref_uuid . '/' . $file_referensi_file_ref_name, 
						FCPATH . 'uploads/file_referensi/' . $file_referensi_file_ref_name_copy);

				if (!is_file(FCPATH . '/uploads/file_referensi/' . $file_referensi_file_ref_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_ref'] = $file_referensi_file_ref_name_copy;
			}
		
			
			$save_file_referensi = $this->model_file_referensi->store($save_data);
            

			if ($save_file_referensi) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_file_referensi;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/file_referensi/edit/' . $save_file_referensi, 'Edit File Referensi'),
						anchor('administrator/file_referensi', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/file_referensi/edit/' . $save_file_referensi, 'Edit File Referensi')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/file_referensi');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/file_referensi');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view File Referensis
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('file_referensi_update');

		$this->data['file_referensi'] = $this->model_file_referensi->find($id);

		$this->template->title('File Referensi Update');
		$this->render('backend/standart/administrator/file_referensi/file_referensi_update', $this->data);
	}

	/**
	* Update File Referensis
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('file_referensi_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tgl_upload', 'Tgl Upload', 'trim|required');
		$this->form_validation->set_rules('file_referensi_file_ref_name', 'File Ref', 'trim|required');
		
		if ($this->form_validation->run()) {
			$file_referensi_file_ref_uuid = $this->input->post('file_referensi_file_ref_uuid');
			$file_referensi_file_ref_name = $this->input->post('file_referensi_file_ref_name');
		
			$save_data = [
				'deskripsi' => $this->input->post('deskripsi'),
				'tgl_upload' => $this->input->post('tgl_upload'),
				'uploader' => get_user_data('username'),
			];

			if (!is_dir(FCPATH . '/uploads/file_referensi/')) {
				mkdir(FCPATH . '/uploads/file_referensi/');
			}

			if (!empty($file_referensi_file_ref_uuid)) {
				$file_referensi_file_ref_name_copy = date('YmdHis') . '-' . $file_referensi_file_ref_name;

				rename(FCPATH . 'uploads/tmp/' . $file_referensi_file_ref_uuid . '/' . $file_referensi_file_ref_name, 
						FCPATH . 'uploads/file_referensi/' . $file_referensi_file_ref_name_copy);

				if (!is_file(FCPATH . '/uploads/file_referensi/' . $file_referensi_file_ref_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_ref'] = $file_referensi_file_ref_name_copy;
			}
		
			
			$save_file_referensi = $this->model_file_referensi->change($id, $save_data);

			if ($save_file_referensi) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/file_referensi', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/file_referensi/index/'.$this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/file_referensi/index/'.$this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete File Referensis
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('file_referensi_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'file_referensi'), 'success');
        } else {
            set_message(cclang('error_delete', 'file_referensi'), 'error');
        }

		redirect_back();
	}

		/**
	* View view File Referensis
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('file_referensi_view');

		$this->data['file_referensi'] = $this->model_file_referensi->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('File Referensi Detail');
		$this->render('backend/standart/administrator/file_referensi/file_referensi_view', $this->data);
	}
	
	/**
	* delete File Referensis
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$file_referensi = $this->model_file_referensi->find($id);

		if (!empty($file_referensi->file_ref)) {
			$path = FCPATH . '/uploads/file_referensi/' . $file_referensi->file_ref;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_file_referensi->remove($id);
	}
	
	/**
	* Upload Image File Referensi	* 
	* @return JSON
	*/
	public function upload_file_ref_file()
	{
		if (!$this->is_allowed('file_referensi_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'file_referensi',
			'allowed_types' => 'pdf|jpg|jpeg|doc|docx|xls|xlsx',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	* Delete Image File Referensi	* 
	* @return JSON
	*/
	public function delete_file_ref_file($uuid)
	{
		if (!$this->is_allowed('file_referensi_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_ref', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'file_referensi',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/file_referensi/'
        ]);
	}

	/**
	* Get Image File Referensi	* 
	* @return JSON
	*/
	public function get_file_ref_file($id)
	{
		if (!$this->is_allowed('file_referensi_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$file_referensi = $this->model_file_referensi->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_ref', 
            'table_name'        => 'file_referensi',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/file_referensi/',
            'delete_endpoint'   => 'administrator/file_referensi/delete_file_ref_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('file_referensi_export');

		$this->model_file_referensi->export(
			'file_referensi', 
			'file_referensi',
			$this->model_file_referensi->field_search
		);
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('file_referensi_export');

		$this->model_file_referensi->pdf('file_referensi', 'file_referensi');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('file_referensi_export');

		$table = $title = 'file_referensi';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_file_referensi->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file file_referensi.php */
/* Location: ./application/controllers/administrator/File Referensi.php */