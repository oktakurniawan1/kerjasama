<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Dosen Inbound <small><?= cclang('new', ['Dosen Inbound']); ?> </small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/dosen_inbound4'); ?>">Dosen Inbound</a></li>
		<li class="active"><?= cclang('new'); ?></li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Dosen Inbound</h3>
							<h5 class="widget-user-desc"><?= cclang('new', ['Dosen Inbound']); ?></h5>
							<hr>
						</div>
						<?= form_open('', [
							'name'    => 'form_dosen_inbound4',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_dosen_inbound4',
							'enctype' => 'multipart/form-data',
							'method'  => 'POST'
						]); ?>

						<div class="form-group ">
							<label for="aktivitas" class="col-sm-2 control-label">Jenis Aktivitas
								<i class="required"><?=(find_rules('aktivitas', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<input type="hidden" name="aktivitas" value="LA">
								<select class="form-control chosen chosen-select" name="aktivitas" id="aktivitas" data-placeholder="Select Jenis Aktivitas" disabled>
									<option value=""></option>
									<option value="SA">Short Activities</option>
									<option value="LA" selected>Long Activities</option>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="scope" class="col-sm-2 control-label">DN/LN
								<i class="required"><?=(find_rules('scope', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<input type="hidden" name="scope" value='LN'>
								<select class="form-control chosen chosen-select" name="scope" id="scope" data-placeholder="Select DN/LN" disabled>
									<option value=""></option>
									<option value="DN">Dalam Negeri</option>
									<option value="LN" selected>Luar Negeri</option>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="kd_prodi" class="col-sm-2 control-label">Program Studi / Unit
								<i class="required"><?=(find_rules('kd_prodi', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="kd_prodi" id="kd_prodi" data-placeholder="Select Program Studi / Unit">
									<option value=""></option>
									<?php foreach ($filter_prodi as $row) : ?>
										<option value="<?= $row->kode ?>"><?= $row->kode .' - '.$row->nama_prodi; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="keg_dosin_id" class="col-sm-2 control-label">Jenis Kegiatan
								<i class="required"><?=(find_rules('keg_dosin_id', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="keg_dosin_id" id="keg_dosin_id" data-placeholder="Select Jenis Kegiatan">
									<option value=""></option>
									<?php foreach (db_get_all_data('dosin_keg', ['aktivitas' => 'LA']) as $row) : ?>
										<option value="<?= $row->id ?>"><?= $row->kegiatan; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgs_dosin_id" class="col-sm-2 control-label">Tugas Sebagai
								<i class="required"><?=(find_rules('tgs_dosin_id', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="tgs_dosin_id" id="tgs_dosin_id" data-placeholder="Select Tugas Sebagai">
									<option value=""></option>
									<?php foreach (db_get_all_data('dosin_tugas', ['id' => 5]) as $row) : ?>
										<option value="<?= $row->id ?>" selected><?= $row->tugas; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>


						<div class="form-group ">
							<label for="judul_kegiatan" class="col-sm-2 control-label">Judul Kegiatan
								<i class="required"><?=(find_rules('judul_kegiatan', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="judul_kegiatan" id="judul_kegiatan" placeholder="Judul Kegiatan" value="<?= set_value('judul_kegiatan'); ?>">
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_mulai" class="col-sm-2 control-label">Tanggal Mulai
								<i class="required"><?=(find_rules('tgl_mulai', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-8">
									<input type="text" class="form-control pull-right datepicker" name="tgl_mulai" placeholder="Tanggal Mulai" id="tgl_mulai">
								</div>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_selesai" class="col-sm-2 control-label">Tanggal Selesai
								<i class="required"><?=(find_rules('tgl_selesai', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-8">
									<input type="text" class="form-control pull-right datepicker" name="tgl_selesai" placeholder="Tanggal Selesai" id="tgl_selesai">
								</div>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nama_dosin" class="col-sm-2 control-label">Nama Dosen/Tamu
								<i class="required"><?=(find_rules('nama_dosin', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nama_dosin" id="nama_dosin" placeholder="Nama Dosen/Tamu" value="<?= set_value('nama_dosin'); ?>">
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nomor_identitas" class="col-sm-2 control-label">No Identitas (KTP/SIM/NIP/PASPOR)
								<i class="required"><?=(find_rules('nomor_identitas', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nomor_identitas" id="nomor_identitas" placeholder="No Identitas (KTP/SIM/NIP/PASPOR)" value="<?= set_value('nomor_identitas'); ?>">
								<small class="info help-block">
									<b>Input Nomor Identitas</b> Max Length : 100.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nomor_identitas" class="col-sm-2 control-label">NIDN (Nomor Induk Dosen Nasional)
								<i class="required"><?=(find_rules('nidn', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nidn" id="nidn" placeholder="Nomor Induk Dosen Nasional" value="<?= set_value('nidn'); ?>">
								<small class="info help-block">
									<b>Input NIDN</b> Max Length : 10</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="institusi_asal" class="col-sm-2 control-label">Institusi Asal
								<i class="required"><?=(find_rules('institusi_asal', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="institusi_asal" id="institusi_asal" placeholder="Institusi Asal" value="<?= set_value('institusi_asal'); ?>">
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="" class="col-sm-2 control-label">Nota Kesepahaman (MoU)
								<i class="required"><?=(find_rules('mou_ks', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="mou_ks" id="mou_ks" data-placeholder="Select MoU Kerjasama">
								</select>
								<small class="info help-block">
									Pilih MoU, jika DLB/tidak ada dalam pilihan silahkan pilih <b>PENGAJUAN BARU</b></small>
							</div>
						</div>

						<div class="form-group ">
							<label for="unit_ks" class="col-sm-2 control-label">MoA/IA/LoA (Unit) / SK DLB
								<i class="required"><?=(find_rules('unit_ks', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="unit_ks" id="unit_ks" data-placeholder="Select MoA/IA/LoA (Unit)">
								</select>
								<small class="info help-block">
									Jika tidak ada, silahkan inputkan terlebih dahulu di input kerjasama Unit.
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="negara" class="col-sm-2 control-label">Negara
								<i class="required"><?=(find_rules('negara', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="negara" id="negara" data-placeholder="Select Negara">
									<option value=""></option>
									<?php foreach (db_get_all_data('negara') as $row) : ?>
										<option value="<?= $row->nama_negara ?>"><?= $row->nama_negara; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>


						<div class="form-group ">
							<label for="file_cv" class="col-sm-2 control-label">File CV
								<i class="required"><?=(find_rules('dosen_inbound4_file_cv_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound4_file_cv_galery"></div>
								<input class="data_file" name="dosen_inbound4_file_cv_uuid" id="dosen_inbound4_file_cv_uuid" type="hidden" value="<?= set_value('dosen_inbound4_file_cv_uuid'); ?>">
								<input class="data_file" name="dosen_inbound4_file_cv_name" id="dosen_inbound4_file_cv_name" type="hidden" value="<?= set_value('dosen_inbound4_file_cv_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_loa" class="col-sm-2 control-label">File LoA
								<i class="required"><?=(find_rules('dosen_inbound4_file_loa_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound4_file_loa_galery"></div>
								<input class="data_file" name="dosen_inbound4_file_loa_uuid" id="dosen_inbound4_file_loa_uuid" type="hidden" value="<?= set_value('dosen_inbound4_file_loa_uuid'); ?>">
								<input class="data_file" name="dosen_inbound4_file_loa_name" id="dosen_inbound4_file_loa_name" type="hidden" value="<?= set_value('dosen_inbound4_file_loa_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_foto" class="col-sm-2 control-label">File Foto
								<i class="required"><?=(find_rules('dosen_inbound4_file_foto_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound4_file_foto_galery"></div>
								<input class="data_file" name="dosen_inbound4_file_foto_uuid" id="dosen_inbound4_file_foto_uuid" type="hidden" value="<?= set_value('dosen_inbound4_file_foto_uuid'); ?>">
								<input class="data_file" name="dosen_inbound4_file_foto_name" id="dosen_inbound4_file_foto_name" type="hidden" value="<?= set_value('dosen_inbound4_file_foto_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF,JPG, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_paspor" class="col-sm-2 control-label">File Paspor
								<i class="required"><?=(find_rules('dosen_inbound4_file_paspor_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound4_file_paspor_galery"></div>
								<input class="data_file" name="dosen_inbound4_file_paspor_uuid" id="dosen_inbound4_file_paspor_uuid" type="hidden" value="<?= set_value('dosen_inbound4_file_paspor_uuid'); ?>">
								<input class="data_file" name="dosen_inbound4_file_paspor_name" id="dosen_inbound4_file_paspor_name" type="hidden" value="<?= set_value('dosen_inbound4_file_paspor_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_ket_sehat" class="col-sm-2 control-label">File Keterangan Sehat
								<i class="required"><?=(find_rules('dosen_inbound4_file_ket_sehat_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound4_file_ket_sehat_galery"></div>
								<input class="data_file" name="dosen_inbound4_file_ket_sehat_uuid" id="dosen_inbound4_file_ket_sehat_uuid" type="hidden" value="<?= set_value('dosen_inbound4_file_ket_sehat_uuid'); ?>">
								<input class="data_file" name="dosen_inbound4_file_ket_sehat_name" id="dosen_inbound4_file_ket_sehat_name" type="hidden" value="<?= set_value('dosen_inbound4_file_ket_sehat_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_ijasah" class="col-sm-2 control-label">File Ijasah
								<i class="required"><?=(find_rules('dosen_inbound4_file_ijasah_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound4_file_ijasah_galery"></div>
								<input class="data_file" name="dosen_inbound4_file_ijasah_uuid" id="dosen_inbound4_file_ijasah_uuid" type="hidden" value="<?= set_value('dosen_inbound4_file_ijasah_uuid'); ?>">
								<input class="data_file" name="dosen_inbound4_file_ijasah_name" id="dosen_inbound4_file_ijasah_name" type="hidden" value="<?= set_value('dosen_inbound4_file_ijasah_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_bank_stantement" class="col-sm-2 control-label">File Bank Stantement $1500
								<i class="required"><?=(find_rules('dosen_inbound4_file_bank_stantement_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound4_file_bank_stantement_galery"></div>
								<input class="data_file" name="dosen_inbound4_file_bank_stantement_uuid" id="dosen_inbound4_file_bank_stantement_uuid" type="hidden" value="<?= set_value('dosen_inbound4_file_bank_stantement_uuid'); ?>">
								<input class="data_file" name="dosen_inbound4_file_bank_stantement_name" id="dosen_inbound4_file_bank_stantement_name" type="hidden" value="<?= set_value('dosen_inbound4_file_bank_stantement_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_asuransi_kesehatan" class="col-sm-2 control-label">File Asuransi Kesehatan
								<i class="required"><?=(find_rules('dosen_inbound4_file_asuransi_kesehatan_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound4_file_asuransi_kesehatan_galery"></div>
								<input class="data_file" name="dosen_inbound4_file_asuransi_kesehatan_uuid" id="dosen_inbound4_file_asuransi_kesehatan_uuid" type="hidden" value="<?= set_value('dosen_inbound4_file_asuransi_kesehatan_uuid'); ?>">
								<input class="data_file" name="dosen_inbound4_file_asuransi_kesehatan_name" id="dosen_inbound4_file_asuransi_kesehatan_name" type="hidden" value="<?= set_value('dosen_inbound4_file_asuransi_kesehatan_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_kontrak_kerja" class="col-sm-2 control-label">File Kontrak Kerja
								<i class="required"><?=(find_rules('dosen_inbound4_file_kontrak_kerja_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound4_file_kontrak_kerja_galery"></div>
								<input class="data_file" name="dosen_inbound4_file_kontrak_kerja_uuid" id="dosen_inbound4_file_kontrak_kerja_uuid" type="hidden" value="<?= set_value('dosen_inbound4_file_kontrak_kerja_uuid'); ?>">
								<input class="data_file" name="dosen_inbound4_file_kontrak_kerja_name" id="dosen_inbound4_file_kontrak_kerja_name" type="hidden" value="<?= set_value('dosen_inbound4_file_kontrak_kerja_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_sertifikat_profesor" class="col-sm-2 control-label">File Sertifikat Profesor
								<i class="required"><?=(find_rules('dosen_inbound4_file_sertifikat_profesor_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound4_file_sertifikat_profesor_galery"></div>
								<input class="data_file" name="dosen_inbound4_file_sertifikat_profesor_uuid" id="dosen_inbound4_file_sertifikat_profesor_uuid" type="hidden" value="<?= set_value('dosen_inbound4_file_sertifikat_profesor_uuid'); ?>">
								<input class="data_file" name="dosen_inbound4_file_sertifikat_profesor_name" id="dosen_inbound4_file_sertifikat_profesor_name" type="hidden" value="<?= set_value('dosen_inbound4_file_sertifikat_profesor_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_sertifikat_keahlian" class="col-sm-2 control-label">File Sertifikat Keahlian
								<i class="required"><?=(find_rules('dosen_inbound4_file_sertifikat_keahlian_name', $validation_rules) ? '*' :'') ?></i>
							</label>
							<div class="col-sm-8">
								<div id="dosen_inbound4_file_sertifikat_keahlian_galery"></div>
								<input class="data_file" name="dosen_inbound4_file_sertifikat_keahlian_uuid" id="dosen_inbound4_file_sertifikat_keahlian_uuid" type="hidden" value="<?= set_value('dosen_inbound4_file_sertifikat_keahlian_uuid'); ?>">
								<input class="data_file" name="dosen_inbound4_file_sertifikat_keahlian_name" id="dosen_inbound4_file_sertifikat_keahlian_name" type="hidden" value="<?= set_value('dosen_inbound4_file_sertifikat_keahlian_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="deskripsi" class="col-sm-2 control-label">Deskripsi
							</label>
							<div class="col-sm-8">
								<textarea id="deskripsi" name="deskripsi" rows="5" class="textarea form-control"><?= set_value('deskripsi'); ?></textarea>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="kode_dosen" class="col-sm-2 control-label">Kode Dosen
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="kode_dosen" id="kode_dosen" placeholder="XXXX" value="<?= set_value('kode_dosen'); ?>" disabled>
								<small class="info help-block">
									<b>Kode Dosen akan update otomatis setelah data divalidasi.</b></small>
							</div>
						</div>

						<?php if (is_groups_in(['Admin', 'Kerjasama'])) : ?>
							<div class="form-group ">
								<label for="validasi" class="col-sm-2 control-label">Validasi
								</label>
								<div class="col-sm-8">
									<select class="form-control chosen chosen-select" name="validasi" id="validasi" data-placeholder="Select Validasi">
										<option value=""></option>
										<option value="Y">Valid</option>
										<option value="T">Invalid</option>
									</select>
									<small class="info help-block">
									</small>
								</div>
							</div>
						<?php endif ?>

						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	$(document).ready(function() {
		$.get('<?= site_url('administrator/si_kerjasama/get_option_mou') ?>', {
			scope: 'LN'
		}, function(page) {
			$('#mou_ks').html(page).val('').trigger('chosen:updated');
		});
		$('#mou_ks').change(function(e) {
			var mou_ks = e.target.value;
			$.get('<?= site_url('administrator/si_kerjasama/get_option_moa') ?>', {
				mou_ks: mou_ks,
				kd_prodi: $('select[name=kd_prodi] option:selected').val()
			}, function(page) {
				$('#unit_ks').html(page).val('').trigger('chosen:updated');
			});
		}).trigger('change');

		$('#kd_prodi').change(function() {
			$('#mou_ks').trigger('change');
		})

		$('#btn_cancel').click(function() {
			swal({
					title: "<?= cclang('are_you_sure'); ?>",
					text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/dosen_inbound4';
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();

			var form_dosen_inbound4 = $('#form_dosen_inbound4');
			var data_post = form_dosen_inbound4.serializeArray();
			var save_type = $(this).attr('data-stype');

			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: BASE_URL + '/administrator/dosen_inbound4/add_save',
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('.steps li').removeClass('error');
					$('form').find('.error-input').remove();
					if (res.success) {
						var id_file_cv = $('#dosen_inbound4_file_cv_galery').find('li').attr('qq-file-id');
						var id_file_loa = $('#dosen_inbound4_file_loa_galery').find('li').attr('qq-file-id');
						var id_file_foto = $('#dosen_inbound4_file_foto_galery').find('li').attr('qq-file-id');
						var id_file_paspor = $('#dosen_inbound4_file_paspor_galery').find('li').attr('qq-file-id');
						var id_file_ket_sehat = $('#dosen_inbound4_file_ket_sehat_galery').find('li').attr('qq-file-id');
						var id_file_ijasah = $('#dosen_inbound4_file_ijasah_galery').find('li').attr('qq-file-id');
						var id_file_bank_stantement = $('#dosen_inbound4_file_bank_stantement_galery').find('li').attr('qq-file-id');
						var id_file_asuransi_kesehatan = $('#dosen_inbound4_file_asuransi_kesehatan_galery').find('li').attr('qq-file-id');
						var id_file_kontrak_kerja = $('#dosen_inbound4_file_kontrak_kerja_galery').find('li').attr('qq-file-id');
						var id_file_sertifikat_profesor = $('#dosen_inbound4_file_sertifikat_profesor_galery').find('li').attr('qq-file-id');
						var id_file_sertifikat_keahlian = $('#dosen_inbound4_file_sertifikat_keahlian_galery').find('li').attr('qq-file-id');

						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						resetForm();
						if (typeof id_file_cv !== 'undefined') {
							$('#dosen_inbound4_file_cv_galery').fineUploader('deleteFile', id_file_cv);
						}
						if (typeof id_file_loa !== 'undefined') {
							$('#dosen_inbound4_file_loa_galery').fineUploader('deleteFile', id_file_loa);
						}
						if (typeof id_file_foto !== 'undefined') {
							$('#dosen_inbound4_file_foto_galery').fineUploader('deleteFile', id_file_foto);
						}
						if (typeof id_file_paspor !== 'undefined') {
							$('#dosen_inbound4_file_paspor_galery').fineUploader('deleteFile', id_file_paspor);
						}
						if (typeof id_file_ket_sehat !== 'undefined') {
							$('#dosen_inbound4_file_ket_sehat_galery').fineUploader('deleteFile', id_file_ket_sehat);
						}
						if (typeof id_file_ijasah !== 'undefined') {
							$('#dosen_inbound4_file_ijasah_galery').fineUploader('deleteFile', id_file_ijasah);
						}
						if (typeof id_file_bank_stantement !== 'undefined') {
							$('#dosen_inbound4_file_bank_stantement_galery').fineUploader('deleteFile', id_file_bank_stantement);
						}
						if (typeof id_file_asuransi_kesehatan !== 'undefined') {
							$('#dosen_inbound4_file_asuransi_kesehatan_galery').fineUploader('deleteFile', id_file_asuransi_kesehatan);
						}
						if (typeof id_file_kontrak_kerja !== 'undefined') {
							$('#dosen_inbound4_file_kontrak_kerja_galery').fineUploader('deleteFile', id_file_kontrak_kerja);
						}
						if (typeof id_file_sertifikat_profesor !== 'undefined') {
							$('#dosen_inbound4_file_sertifikat_profesor_galery').fineUploader('deleteFile', id_file_sertifikat_profesor);
						}
						if (typeof id_file_sertifikat_keahlian !== 'undefined') {
							$('#dosen_inbound4_file_sertifikat_keahlian_galery').fineUploader('deleteFile', id_file_sertifikat_keahlian);
						}
						$('.chosen option').prop('selected', false).trigger('chosen:updated');

					} else {
						if (res.errors) {

							$.each(res.errors, function(index, val) {
								$('form #' + index).parents('.form-group').addClass('has-error');
								$('form #' + index).parents('.form-group').find('small').prepend(`
                      <div class="error-input">` + val + `</div>
                      `);
							});
							$('.steps li').removeClass('error');
							$('.content section').each(function(index, el) {
								if ($(this).find('.has-error').length) {
									$('.steps li:eq(' + index + ')').addClass('error').find('a').trigger('click');
								}
							});
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/

		var params = {};
		params[csrf] = token;

		$('#dosen_inbound4_file_cv_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound4/upload_file_cv_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/dosen_inbound4/delete_file_cv_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound4_file_cv_galery').fineUploader('getUuid', id);
						$('#dosen_inbound4_file_cv_uuid').val(uuid);
						$('#dosen_inbound4_file_cv_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound4_file_cv_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound4/delete_file_cv_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound4_file_cv_uuid').val('');
						$('#dosen_inbound4_file_cv_name').val('');
					}
				}
			}
		}); /*end file_cv galery*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound4_file_loa_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound4/upload_file_loa_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/dosen_inbound4/delete_file_loa_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound4_file_loa_galery').fineUploader('getUuid', id);
						$('#dosen_inbound4_file_loa_uuid').val(uuid);
						$('#dosen_inbound4_file_loa_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound4_file_loa_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound4/delete_file_loa_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound4_file_loa_uuid').val('');
						$('#dosen_inbound4_file_loa_name').val('');
					}
				}
			}
		}); /*end file_loa galery*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound4_file_foto_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound4/upload_file_foto_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/dosen_inbound4/delete_file_foto_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf", "jpg"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound4_file_foto_galery').fineUploader('getUuid', id);
						$('#dosen_inbound4_file_foto_uuid').val(uuid);
						$('#dosen_inbound4_file_foto_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound4_file_foto_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound4/delete_file_foto_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound4_file_foto_uuid').val('');
						$('#dosen_inbound4_file_foto_name').val('');
					}
				}
			}
		}); /*end file_foto galery*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound4_file_paspor_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound4/upload_file_paspor_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/dosen_inbound4/delete_file_paspor_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound4_file_paspor_galery').fineUploader('getUuid', id);
						$('#dosen_inbound4_file_paspor_uuid').val(uuid);
						$('#dosen_inbound4_file_paspor_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound4_file_paspor_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound4/delete_file_paspor_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound4_file_paspor_uuid').val('');
						$('#dosen_inbound4_file_paspor_name').val('');
					}
				}
			}
		}); /*end file_paspor galery*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound4_file_ket_sehat_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound4/upload_file_ket_sehat_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/dosen_inbound4/delete_file_ket_sehat_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound4_file_ket_sehat_galery').fineUploader('getUuid', id);
						$('#dosen_inbound4_file_ket_sehat_uuid').val(uuid);
						$('#dosen_inbound4_file_ket_sehat_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound4_file_ket_sehat_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound4/delete_file_ket_sehat_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound4_file_ket_sehat_uuid').val('');
						$('#dosen_inbound4_file_ket_sehat_name').val('');
					}
				}
			}
		}); /*end file_ket_sehat galery*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound4_file_ijasah_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound4/upload_file_ijasah_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/dosen_inbound4/delete_file_ijasah_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound4_file_ijasah_galery').fineUploader('getUuid', id);
						$('#dosen_inbound4_file_ijasah_uuid').val(uuid);
						$('#dosen_inbound4_file_ijasah_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound4_file_ijasah_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound4/delete_file_ijasah_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound4_file_ijasah_uuid').val('');
						$('#dosen_inbound4_file_ijasah_name').val('');
					}
				}
			}
		}); /*end file_ijasah galery*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound4_file_bank_stantement_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound4/upload_file_bank_stantement_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/dosen_inbound4/delete_file_bank_stantement_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound4_file_bank_stantement_galery').fineUploader('getUuid', id);
						$('#dosen_inbound4_file_bank_stantement_uuid').val(uuid);
						$('#dosen_inbound4_file_bank_stantement_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound4_file_bank_stantement_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound4/delete_file_bank_stantement_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound4_file_bank_stantement_uuid').val('');
						$('#dosen_inbound4_file_bank_stantement_name').val('');
					}
				}
			}
		}); /*end file_bank_stantement galery*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound4_file_asuransi_kesehatan_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound4/upload_file_asuransi_kesehatan_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/dosen_inbound4/delete_file_asuransi_kesehatan_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound4_file_asuransi_kesehatan_galery').fineUploader('getUuid', id);
						$('#dosen_inbound4_file_asuransi_kesehatan_uuid').val(uuid);
						$('#dosen_inbound4_file_asuransi_kesehatan_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound4_file_asuransi_kesehatan_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound4/delete_file_asuransi_kesehatan_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound4_file_asuransi_kesehatan_uuid').val('');
						$('#dosen_inbound4_file_asuransi_kesehatan_name').val('');
					}
				}
			}
		}); /*end file_asuransi_kesehatan galery*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound4_file_kontrak_kerja_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound4/upload_file_kontrak_kerja_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/dosen_inbound4/delete_file_kontrak_kerja_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound4_file_kontrak_kerja_galery').fineUploader('getUuid', id);
						$('#dosen_inbound4_file_kontrak_kerja_uuid').val(uuid);
						$('#dosen_inbound4_file_kontrak_kerja_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound4_file_kontrak_kerja_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound4/delete_file_kontrak_kerja_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound4_file_kontrak_kerja_uuid').val('');
						$('#dosen_inbound4_file_kontrak_kerja_name').val('');
					}
				}
			}
		}); /*end file_kontrak_kerja galery*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound4_file_sertifikat_profesor_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound4/upload_file_sertifikat_profesor_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/dosen_inbound4/delete_file_sertifikat_profesor_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound4_file_sertifikat_profesor_galery').fineUploader('getUuid', id);
						$('#dosen_inbound4_file_sertifikat_profesor_uuid').val(uuid);
						$('#dosen_inbound4_file_sertifikat_profesor_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound4_file_sertifikat_profesor_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound4/delete_file_sertifikat_profesor_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound4_file_sertifikat_profesor_uuid').val('');
						$('#dosen_inbound4_file_sertifikat_profesor_name').val('');
					}
				}
			}
		}); /*end file_sertifikat_profesor galery*/
		var params = {};
		params[csrf] = token;

		$('#dosen_inbound4_file_sertifikat_keahlian_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/dosen_inbound4/upload_file_sertifikat_keahlian_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/dosen_inbound4/delete_file_sertifikat_keahlian_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#dosen_inbound4_file_sertifikat_keahlian_galery').fineUploader('getUuid', id);
						$('#dosen_inbound4_file_sertifikat_keahlian_uuid').val(uuid);
						$('#dosen_inbound4_file_sertifikat_keahlian_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#dosen_inbound4_file_sertifikat_keahlian_uuid').val();
					$.get(BASE_URL + '/administrator/dosen_inbound4/delete_file_sertifikat_keahlian_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#dosen_inbound4_file_sertifikat_keahlian_uuid').val('');
						$('#dosen_inbound4_file_sertifikat_keahlian_name').val('');
					}
				}
			}
		}); /*end file_sertifikat_keahlian galery*/

	}); /*end doc ready*/
</script>