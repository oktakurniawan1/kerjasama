<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Dosen Inbound4 Controller
 *| --------------------------------------------------------------------------
 *| Dosen Inbound4 site
 *|
 */
class Dosen_inbound4 extends Admin
{

	protected $validation_rules = [];

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_dosen_inbound4');
		$this->load->model('ref_prodi/Model_ref_prodi', 'model_ref_prodi');
		$this->lang->load('web_lang', $this->current_lang);

		$this->validation_rules = [
			['field' => 'scope', 'label' => 'DN/LN', 'rules' => 'trim|required'],
			['field' => 'aktivitas', 'label' => 'Jenis Aktivitas', 'rules' => 'trim|required'],
			['field' => 'kd_prodi', 'label' => 'Program Studi/Unit', 'rules' => 'trim|required'],
			['field' => 'keg_dosin_id', 'label' => 'Jenis Kegiatan', 'rules' => 'trim|required'],
			['field' => 'tgs_dosin_id', 'label' => 'Tugas Sebagai', 'rules' => 'trim|required'],
			['field' => 'judul_kegiatan', 'label' => 'Judul Kegiatan', 'rules' => 'trim|required'],
			['field' => 'tgl_mulai', 'label' => 'Tanggal Mulai', 'rules' => 'trim|required'],
			['field' => 'tgl_selesai', 'label' => 'Tanggal Selesai', 'rules' => 'trim|required'],
			['field' => 'nama_dosin', 'label' => 'Nama Dosen/Tamu', 'rules' => 'trim|required'],
			['field' => 'nomor_identitas', 'label' => 'No Identitas (KTP/SIM/NIP/PASPOR)', 'rules' => 'trim|required|max_length[100]'],
			['field' => 'nidn', 'label' => 'Nomor Induk Dosen Nasional', 'rules' => 'trim|required|max_length[10]'],
			['field' => 'institusi_asal', 'label' => 'Institusi Asal', 'rules' => 'trim|required'],
			['field' => 'mou_ks', 'label' => 'Nota Kesepahaman (MoU)', 'rules' => 'trim|required'],
			['field' => 'unit_ks', 'label' => 'MoA/IA/LoA (Unit)', 'rules' => 'trim|required'],
			['field' => 'negara', 'label' => 'Negara', 'rules' => 'trim|required'],
			['field' => 'dosen_inbound4_file_cv_name', 'label' => 'File CV', 'rules' => 'trim|required'],
			['field' => 'dosen_inbound4_file_loa_name', 'label' => 'File LoA', 'rules' => 'trim|required'],
			['field' => 'dosen_inbound4_file_foto_name', 'label' => 'File Foto', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound4_file_paspor_name', 'label' => 'File Paspor', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound4_file_ket_sehat_name', 'label' => 'File Keterangan Sehat', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound4_file_ijasah_name', 'label' => 'File Ijasah', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound4_file_bank_stantement_name', 'label' => 'File Bank Stantement $1500', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound4_file_asuransi_kesehatan_name', 'label' => 'File Asuransi Kesehatan', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound4_file_kontrak_kerja_name', 'label' => 'File Kontrak Kerja', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound4_file_sertifikat_profesor_name', 'label' => 'File Sertifikat Profesor', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound4_file_sertifikat_keahlian_name', 'label' => 'File Sertifikat Keahlian', 'rules' => 'trim|required'],
		];
		
		$this->form_validation->set_rules($this->validation_rules);
	}

	/**
	 * show all Dosen Inbound4s
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		redirect('administrator/dosen_inbound1');
	}

	/**
	 * Add new dosen_inbound4s
	 *
	 */
	public function add()
	{
		$this->is_allowed('dosen_inbound4_add');

		$this->data['filter_prodi'] = get_filter_prodi();
		$this->data['validation_rules'] = $this->validation_rules;
		$this->template->title('Dosen Inbound New');
		$this->render('backend/standart/administrator/dosen_inbound4/dosen_inbound4_add', $this->data);
	}

	/**
	 * Add New Dosen Inbound4s
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('dosen_inbound4_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}


		if ($this->form_validation->run()) {
			$dosen_inbound4_file_cv_uuid = $this->input->post('dosen_inbound4_file_cv_uuid');
			$dosen_inbound4_file_cv_name = $this->input->post('dosen_inbound4_file_cv_name');
			$dosen_inbound4_file_loa_uuid = $this->input->post('dosen_inbound4_file_loa_uuid');
			$dosen_inbound4_file_loa_name = $this->input->post('dosen_inbound4_file_loa_name');
			$dosen_inbound4_file_foto_uuid = $this->input->post('dosen_inbound4_file_foto_uuid');
			$dosen_inbound4_file_foto_name = $this->input->post('dosen_inbound4_file_foto_name');
			$dosen_inbound4_file_paspor_uuid = $this->input->post('dosen_inbound4_file_paspor_uuid');
			$dosen_inbound4_file_paspor_name = $this->input->post('dosen_inbound4_file_paspor_name');
			$dosen_inbound4_file_ket_sehat_uuid = $this->input->post('dosen_inbound4_file_ket_sehat_uuid');
			$dosen_inbound4_file_ket_sehat_name = $this->input->post('dosen_inbound4_file_ket_sehat_name');
			$dosen_inbound4_file_ijasah_uuid = $this->input->post('dosen_inbound4_file_ijasah_uuid');
			$dosen_inbound4_file_ijasah_name = $this->input->post('dosen_inbound4_file_ijasah_name');
			$dosen_inbound4_file_bank_stantement_uuid = $this->input->post('dosen_inbound4_file_bank_stantement_uuid');
			$dosen_inbound4_file_bank_stantement_name = $this->input->post('dosen_inbound4_file_bank_stantement_name');
			$dosen_inbound4_file_asuransi_kesehatan_uuid = $this->input->post('dosen_inbound4_file_asuransi_kesehatan_uuid');
			$dosen_inbound4_file_asuransi_kesehatan_name = $this->input->post('dosen_inbound4_file_asuransi_kesehatan_name');
			$dosen_inbound4_file_kontrak_kerja_uuid = $this->input->post('dosen_inbound4_file_kontrak_kerja_uuid');
			$dosen_inbound4_file_kontrak_kerja_name = $this->input->post('dosen_inbound4_file_kontrak_kerja_name');
			$dosen_inbound4_file_sertifikat_profesor_uuid = $this->input->post('dosen_inbound4_file_sertifikat_profesor_uuid');
			$dosen_inbound4_file_sertifikat_profesor_name = $this->input->post('dosen_inbound4_file_sertifikat_profesor_name');
			$dosen_inbound4_file_sertifikat_keahlian_uuid = $this->input->post('dosen_inbound4_file_sertifikat_keahlian_uuid');
			$dosen_inbound4_file_sertifikat_keahlian_name = $this->input->post('dosen_inbound4_file_sertifikat_keahlian_name');

			// simpan kode_fak
			$fak = $this->model_ref_prodi->get_single(['kode' => $this->input->post('kd_prodi')]);

			$save_data = [
				'scope'           => $this->input->post('scope'),
				'aktivitas'       => $this->input->post('aktivitas'),
				'kode_fak'        => $fak->kode_fak,
				'kd_prodi'        => $this->input->post('kd_prodi'),
				'keg_dosin_id'    => $this->input->post('keg_dosin_id'),
				'tgs_dosin_id'    => $this->input->post('tgs_dosin_id'),
				'judul_kegiatan'  => $this->input->post('judul_kegiatan'),
				'tgl_mulai'       => $this->input->post('tgl_mulai'),
				'tgl_selesai'     => $this->input->post('tgl_selesai'),
				'nama_dosin'      => $this->input->post('nama_dosin'),
				'nomor_identitas' => $this->input->post('nomor_identitas'),
				'nidn'            => $this->input->post('nidn'),
				'institusi_asal'  => $this->input->post('institusi_asal'),
				'mou_ks'          => $this->input->post('mou_ks'),
				'unit_ks'         => $this->input->post('unit_ks'),
				'negara'          => $this->input->post('negara'),
				'deskripsi'       => $this->input->post('deskripsi'),
				'kode_dosen'      => $this->input->post('kode_dosen'),
				'validasi'        => $this->input->post('validasi'),
			];

			if (!is_dir(FCPATH . '/uploads/dosen_inbound4/')) {
				mkdir(FCPATH . '/uploads/dosen_inbound4/');
			}

			if (!empty($dosen_inbound4_file_cv_name)) {
				$dosen_inbound4_file_cv_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_cv_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_cv_uuid . '/' . $dosen_inbound4_file_cv_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_cv_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_cv_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_cv'] = $dosen_inbound4_file_cv_name_copy;
			}

			if (!empty($dosen_inbound4_file_loa_name)) {
				$dosen_inbound4_file_loa_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_loa_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_loa_uuid . '/' . $dosen_inbound4_file_loa_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_loa_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_loa'] = $dosen_inbound4_file_loa_name_copy;
			}

			if (!empty($dosen_inbound4_file_foto_name)) {
				$dosen_inbound4_file_foto_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_foto_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_foto_uuid . '/' . $dosen_inbound4_file_foto_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_foto_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_foto_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_foto'] = $dosen_inbound4_file_foto_name_copy;
			}

			if (!empty($dosen_inbound4_file_paspor_name)) {
				$dosen_inbound4_file_paspor_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_paspor_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_paspor_uuid . '/' . $dosen_inbound4_file_paspor_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_paspor_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_paspor_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_paspor'] = $dosen_inbound4_file_paspor_name_copy;
			}

			if (!empty($dosen_inbound4_file_ket_sehat_name)) {
				$dosen_inbound4_file_ket_sehat_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_ket_sehat_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_ket_sehat_uuid . '/' . $dosen_inbound4_file_ket_sehat_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_ket_sehat_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_ket_sehat_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ket_sehat'] = $dosen_inbound4_file_ket_sehat_name_copy;
			}

			if (!empty($dosen_inbound4_file_ijasah_name)) {
				$dosen_inbound4_file_ijasah_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_ijasah_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_ijasah_uuid . '/' . $dosen_inbound4_file_ijasah_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_ijasah_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_ijasah_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ijasah'] = $dosen_inbound4_file_ijasah_name_copy;
			}

			if (!empty($dosen_inbound4_file_bank_stantement_name)) {
				$dosen_inbound4_file_bank_stantement_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_bank_stantement_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_bank_stantement_uuid . '/' . $dosen_inbound4_file_bank_stantement_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_bank_stantement_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_bank_stantement_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_bank_stantement'] = $dosen_inbound4_file_bank_stantement_name_copy;
			}

			if (!empty($dosen_inbound4_file_asuransi_kesehatan_name)) {
				$dosen_inbound4_file_asuransi_kesehatan_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_asuransi_kesehatan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_asuransi_kesehatan_uuid . '/' . $dosen_inbound4_file_asuransi_kesehatan_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_asuransi_kesehatan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_asuransi_kesehatan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_asuransi_kesehatan'] = $dosen_inbound4_file_asuransi_kesehatan_name_copy;
			}

			if (!empty($dosen_inbound4_file_kontrak_kerja_name)) {
				$dosen_inbound4_file_kontrak_kerja_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_kontrak_kerja_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_kontrak_kerja_uuid . '/' . $dosen_inbound4_file_kontrak_kerja_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_kontrak_kerja_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_kontrak_kerja_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_kontrak_kerja'] = $dosen_inbound4_file_kontrak_kerja_name_copy;
			}

			if (!empty($dosen_inbound4_file_sertifikat_profesor_name)) {
				$dosen_inbound4_file_sertifikat_profesor_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_sertifikat_profesor_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_sertifikat_profesor_uuid . '/' . $dosen_inbound4_file_sertifikat_profesor_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_sertifikat_profesor_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_sertifikat_profesor_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_sertifikat_profesor'] = $dosen_inbound4_file_sertifikat_profesor_name_copy;
			}

			if (!empty($dosen_inbound4_file_sertifikat_keahlian_name)) {
				$dosen_inbound4_file_sertifikat_keahlian_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_sertifikat_keahlian_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_sertifikat_keahlian_uuid . '/' . $dosen_inbound4_file_sertifikat_keahlian_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_sertifikat_keahlian_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_sertifikat_keahlian_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_sertifikat_keahlian'] = $dosen_inbound4_file_sertifikat_keahlian_name_copy;
			}


			$save_dosen_inbound4 = $this->model_dosen_inbound4->store($save_data);


			if ($save_dosen_inbound4) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_dosen_inbound4;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/dosen_inbound4/edit/' . $save_dosen_inbound4, 'Edit Dosen Inbound4'),
						anchor('administrator/dosen_inbound4', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/dosen_inbound4/edit/' . $save_dosen_inbound4, 'Edit Dosen Inbound4')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dosen_inbound4');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dosen_inbound4');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Dosen Inbound4s
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('dosen_inbound4_update');

		if (is_groups_in(['Kaprodi', 'Unit'])) {
			$prodi = get_filter_prodi(true);
			$row = $this->model_dosen_inbound4->find($id);
			if (!in_array($row->kd_prodi, $prodi)) {
				set_message(cclang('sorry_you_do_not_have_permission_to_access', 'dosen_inbound1'), 'error');
				redirect_back();
			}
			/**tolak jika sudah divalidasi */
			if ($row->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa mengubah data, karena data sudah divalidasi', 'dosen_inbound1'), 'warning');
				redirect_back();
			}
		}

		$this->data['dosen_inbound4'] = $this->model_dosen_inbound4->find($id);

		$list_mou = db_get_all_data('mou_kerjasama', "scope='{$this->data['dosen_inbound4']->scope}' AND tgl_selesai > curdate() ");
		$cur_mou = db_get_all_data('mou_kerjasama', "id = '{$this->data['dosen_inbound4']->mou_ks}'", 'row');

		if (!is_null($cur_mou)) {
			$tgl = DateTime::createFromFormat('Y-m-d', $cur_mou->tgl_selesai);
			$now = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
			if ($tgl < $now) $list_mou[] = $cur_mou;
		}
		$list_mou[] = (object)['id' => '1', 'nama_mitra' => 'Pengajuan Baru', 'doc_nomor' => 'XXX'];
		$this->data['list_mou'] = $list_mou;

		//moa
		$list_moa = db_get_all_data('si_kerjasama', "mou_ks = '{$this->data['dosen_inbound4']->mou_ks}'  AND tgl_selesai > curdate() AND kd_prodi = '{$this->data['dosen_inbound4']->kd_prodi}' ");
		$cur_moa = db_get_all_data('si_kerjasama', "id = '{$this->data['dosen_inbound4']->unit_ks}'", 'row');

		if (!is_null($cur_moa)) {
			$tgl = DateTime::createFromFormat('Y-m-d', $cur_moa->tgl_selesai);
			$now = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
			if ($tgl < $now) $list_moa[] = $cur_moa;
		}
		$this->data['list_moa'] = $list_moa;
		$this->data['filter_prodi'] = get_filter_prodi();
		$this->data['validation_rules'] = $this->validation_rules;
		$this->template->title('Dosen Inbound Update');
		$this->render('backend/standart/administrator/dosen_inbound4/dosen_inbound4_update', $this->data);
	}

	/**
	 * Update Dosen Inbound4s
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('dosen_inbound4_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		if ($this->form_validation->run()) {
			$dosen_inbound4_file_cv_uuid = $this->input->post('dosen_inbound4_file_cv_uuid');
			$dosen_inbound4_file_cv_name = $this->input->post('dosen_inbound4_file_cv_name');
			$dosen_inbound4_file_loa_uuid = $this->input->post('dosen_inbound4_file_loa_uuid');
			$dosen_inbound4_file_loa_name = $this->input->post('dosen_inbound4_file_loa_name');
			$dosen_inbound4_file_foto_uuid = $this->input->post('dosen_inbound4_file_foto_uuid');
			$dosen_inbound4_file_foto_name = $this->input->post('dosen_inbound4_file_foto_name');
			$dosen_inbound4_file_paspor_uuid = $this->input->post('dosen_inbound4_file_paspor_uuid');
			$dosen_inbound4_file_paspor_name = $this->input->post('dosen_inbound4_file_paspor_name');
			$dosen_inbound4_file_ket_sehat_uuid = $this->input->post('dosen_inbound4_file_ket_sehat_uuid');
			$dosen_inbound4_file_ket_sehat_name = $this->input->post('dosen_inbound4_file_ket_sehat_name');
			$dosen_inbound4_file_ijasah_uuid = $this->input->post('dosen_inbound4_file_ijasah_uuid');
			$dosen_inbound4_file_ijasah_name = $this->input->post('dosen_inbound4_file_ijasah_name');
			$dosen_inbound4_file_bank_stantement_uuid = $this->input->post('dosen_inbound4_file_bank_stantement_uuid');
			$dosen_inbound4_file_bank_stantement_name = $this->input->post('dosen_inbound4_file_bank_stantement_name');
			$dosen_inbound4_file_asuransi_kesehatan_uuid = $this->input->post('dosen_inbound4_file_asuransi_kesehatan_uuid');
			$dosen_inbound4_file_asuransi_kesehatan_name = $this->input->post('dosen_inbound4_file_asuransi_kesehatan_name');
			$dosen_inbound4_file_kontrak_kerja_uuid = $this->input->post('dosen_inbound4_file_kontrak_kerja_uuid');
			$dosen_inbound4_file_kontrak_kerja_name = $this->input->post('dosen_inbound4_file_kontrak_kerja_name');
			$dosen_inbound4_file_sertifikat_profesor_uuid = $this->input->post('dosen_inbound4_file_sertifikat_profesor_uuid');
			$dosen_inbound4_file_sertifikat_profesor_name = $this->input->post('dosen_inbound4_file_sertifikat_profesor_name');
			$dosen_inbound4_file_sertifikat_keahlian_uuid = $this->input->post('dosen_inbound4_file_sertifikat_keahlian_uuid');
			$dosen_inbound4_file_sertifikat_keahlian_name = $this->input->post('dosen_inbound4_file_sertifikat_keahlian_name');

			$fak = $this->model_ref_prodi->get_single(['kode' => $this->input->post('kd_prodi')]);

			$save_data = [
				'scope'           => $this->input->post('scope'),
				'aktivitas'       => $this->input->post('aktivitas'),
				'kode_fak'        => $fak->kode_fak,
				'kd_prodi'        => $this->input->post('kd_prodi'),
				'keg_dosin_id'    => $this->input->post('keg_dosin_id'),
				'tgs_dosin_id'    => $this->input->post('tgs_dosin_id'),
				'judul_kegiatan'  => $this->input->post('judul_kegiatan'),
				'tgl_mulai'       => $this->input->post('tgl_mulai'),
				'tgl_selesai'     => $this->input->post('tgl_selesai'),
				'nama_dosin'      => $this->input->post('nama_dosin'),
				'nomor_identitas' => $this->input->post('nomor_identitas'),
				'nidn'            => $this->input->post('nidn'),
				'institusi_asal'  => $this->input->post('institusi_asal'),
				'mou_ks'          => $this->input->post('mou_ks'),
				'unit_ks'         => $this->input->post('unit_ks'),
				'negara'          => $this->input->post('negara'),
				'deskripsi'       => $this->input->post('deskripsi'),
				'kode_dosen'      => $this->input->post('kode_dosen'),
				'validasi'        => $this->input->post('validasi'),
			];

			if (!is_dir(FCPATH . '/uploads/dosen_inbound4/')) {
				mkdir(FCPATH . '/uploads/dosen_inbound4/');
			}

			if (!empty($dosen_inbound4_file_cv_uuid)) {
				$dosen_inbound4_file_cv_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_cv_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_cv_uuid . '/' . $dosen_inbound4_file_cv_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_cv_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_cv_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_cv'] = $dosen_inbound4_file_cv_name_copy;
			}

			if (!empty($dosen_inbound4_file_loa_uuid)) {
				$dosen_inbound4_file_loa_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_loa_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_loa_uuid . '/' . $dosen_inbound4_file_loa_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_loa_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_loa'] = $dosen_inbound4_file_loa_name_copy;
			}

			if (!empty($dosen_inbound4_file_foto_uuid)) {
				$dosen_inbound4_file_foto_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_foto_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_foto_uuid . '/' . $dosen_inbound4_file_foto_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_foto_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_foto_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_foto'] = $dosen_inbound4_file_foto_name_copy;
			}

			if (!empty($dosen_inbound4_file_paspor_uuid)) {
				$dosen_inbound4_file_paspor_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_paspor_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_paspor_uuid . '/' . $dosen_inbound4_file_paspor_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_paspor_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_paspor_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_paspor'] = $dosen_inbound4_file_paspor_name_copy;
			}

			if (!empty($dosen_inbound4_file_ket_sehat_uuid)) {
				$dosen_inbound4_file_ket_sehat_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_ket_sehat_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_ket_sehat_uuid . '/' . $dosen_inbound4_file_ket_sehat_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_ket_sehat_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_ket_sehat_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ket_sehat'] = $dosen_inbound4_file_ket_sehat_name_copy;
			}

			if (!empty($dosen_inbound4_file_ijasah_uuid)) {
				$dosen_inbound4_file_ijasah_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_ijasah_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_ijasah_uuid . '/' . $dosen_inbound4_file_ijasah_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_ijasah_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_ijasah_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ijasah'] = $dosen_inbound4_file_ijasah_name_copy;
			}

			if (!empty($dosen_inbound4_file_bank_stantement_uuid)) {
				$dosen_inbound4_file_bank_stantement_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_bank_stantement_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_bank_stantement_uuid . '/' . $dosen_inbound4_file_bank_stantement_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_bank_stantement_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_bank_stantement_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_bank_stantement'] = $dosen_inbound4_file_bank_stantement_name_copy;
			}

			if (!empty($dosen_inbound4_file_asuransi_kesehatan_uuid)) {
				$dosen_inbound4_file_asuransi_kesehatan_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_asuransi_kesehatan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_asuransi_kesehatan_uuid . '/' . $dosen_inbound4_file_asuransi_kesehatan_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_asuransi_kesehatan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_asuransi_kesehatan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_asuransi_kesehatan'] = $dosen_inbound4_file_asuransi_kesehatan_name_copy;
			}

			if (!empty($dosen_inbound4_file_kontrak_kerja_uuid)) {
				$dosen_inbound4_file_kontrak_kerja_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_kontrak_kerja_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_kontrak_kerja_uuid . '/' . $dosen_inbound4_file_kontrak_kerja_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_kontrak_kerja_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_kontrak_kerja_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_kontrak_kerja'] = $dosen_inbound4_file_kontrak_kerja_name_copy;
			}

			if (!empty($dosen_inbound4_file_sertifikat_profesor_uuid)) {
				$dosen_inbound4_file_sertifikat_profesor_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_sertifikat_profesor_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_sertifikat_profesor_uuid . '/' . $dosen_inbound4_file_sertifikat_profesor_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_sertifikat_profesor_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_sertifikat_profesor_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_sertifikat_profesor'] = $dosen_inbound4_file_sertifikat_profesor_name_copy;
			}

			if (!empty($dosen_inbound4_file_sertifikat_keahlian_uuid)) {
				$dosen_inbound4_file_sertifikat_keahlian_name_copy = date('YmdHis') . '-' . $dosen_inbound4_file_sertifikat_keahlian_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound4_file_sertifikat_keahlian_uuid . '/' . $dosen_inbound4_file_sertifikat_keahlian_name,
					FCPATH . 'uploads/dosen_inbound4/' . $dosen_inbound4_file_sertifikat_keahlian_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4_file_sertifikat_keahlian_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_sertifikat_keahlian'] = $dosen_inbound4_file_sertifikat_keahlian_name_copy;
			}


			$save_dosen_inbound4 = $this->model_dosen_inbound4->change($id, $save_data);

			if ($save_dosen_inbound4) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/dosen_inbound4', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dosen_inbound4/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dosen_inbound4/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Dosen Inbound4s
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('dosen_inbound4_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'dosen_inbound4'), 'success');
		} else {
			set_message(cclang('error_delete', 'dosen_inbound4'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Dosen Inbound4s
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('dosen_inbound4_view');

		$this->data['files'] = $this->config->item('dosin4_file');
		$this->data['dosen_inbound4'] = $this->model_dosen_inbound4->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Dosen Inbound Detail');
		$this->render('backend/standart/administrator/dosen_inbound4/dosen_inbound4_view', $this->data);
	}

	/**
	 * delete Dosen Inbound4s
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$dosen_inbound4 = $this->model_dosen_inbound4->find($id);

		if (!empty($dosen_inbound4->file_cv)) {
			$path = FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4->file_cv;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound4->file_loa)) {
			$path = FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4->file_loa;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound4->file_foto)) {
			$path = FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4->file_foto;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound4->file_paspor)) {
			$path = FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4->file_paspor;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound4->file_ket_sehat)) {
			$path = FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4->file_ket_sehat;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound4->file_ijasah)) {
			$path = FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4->file_ijasah;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound4->file_bank_stantement)) {
			$path = FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4->file_bank_stantement;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound4->file_asuransi_kesehatan)) {
			$path = FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4->file_asuransi_kesehatan;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound4->file_kontrak_kerja)) {
			$path = FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4->file_kontrak_kerja;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound4->file_sertifikat_profesor)) {
			$path = FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4->file_sertifikat_profesor;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound4->file_sertifikat_keahlian)) {
			$path = FCPATH . '/uploads/dosen_inbound4/' . $dosen_inbound4->file_sertifikat_keahlian;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}


		return $this->model_dosen_inbound4->remove($id);
	}

	/**
	 * Upload Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function upload_file_cv_file()
	{
		if (!$this->is_allowed('dosen_inbound4_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound4',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function delete_file_cv_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound4_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_cv',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/'
		]);
	}

	/**
	 * Get Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function get_file_cv_file($id)
	{
		if (!$this->is_allowed('dosen_inbound4_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound4 = $this->model_dosen_inbound4->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_cv',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/',
			'delete_endpoint'   => 'administrator/dosen_inbound4/delete_file_cv_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function upload_file_loa_file()
	{
		if (!$this->is_allowed('dosen_inbound4_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound4',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function delete_file_loa_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound4_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_loa',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/'
		]);
	}

	/**
	 * Get Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function get_file_loa_file($id)
	{
		if (!$this->is_allowed('dosen_inbound4_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound4 = $this->model_dosen_inbound4->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_loa',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/',
			'delete_endpoint'   => 'administrator/dosen_inbound4/delete_file_loa_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function upload_file_foto_file()
	{
		if (!$this->is_allowed('dosen_inbound4_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound4',
			'allowed_types' => 'pdf|jpg',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function delete_file_foto_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound4_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_foto',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/'
		]);
	}

	/**
	 * Get Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function get_file_foto_file($id)
	{
		if (!$this->is_allowed('dosen_inbound4_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound4 = $this->model_dosen_inbound4->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_foto',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/',
			'delete_endpoint'   => 'administrator/dosen_inbound4/delete_file_foto_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function upload_file_paspor_file()
	{
		if (!$this->is_allowed('dosen_inbound4_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound4',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function delete_file_paspor_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound4_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_paspor',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/'
		]);
	}

	/**
	 * Get Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function get_file_paspor_file($id)
	{
		if (!$this->is_allowed('dosen_inbound4_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound4 = $this->model_dosen_inbound4->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_paspor',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/',
			'delete_endpoint'   => 'administrator/dosen_inbound4/delete_file_paspor_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function upload_file_ket_sehat_file()
	{
		if (!$this->is_allowed('dosen_inbound4_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound4',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function delete_file_ket_sehat_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound4_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ket_sehat',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/'
		]);
	}

	/**
	 * Get Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function get_file_ket_sehat_file($id)
	{
		if (!$this->is_allowed('dosen_inbound4_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound4 = $this->model_dosen_inbound4->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ket_sehat',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/',
			'delete_endpoint'   => 'administrator/dosen_inbound4/delete_file_ket_sehat_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function upload_file_ijasah_file()
	{
		if (!$this->is_allowed('dosen_inbound4_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound4',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function delete_file_ijasah_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound4_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ijasah',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/'
		]);
	}

	/**
	 * Get Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function get_file_ijasah_file($id)
	{
		if (!$this->is_allowed('dosen_inbound4_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound4 = $this->model_dosen_inbound4->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ijasah',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/',
			'delete_endpoint'   => 'administrator/dosen_inbound4/delete_file_ijasah_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function upload_file_bank_stantement_file()
	{
		if (!$this->is_allowed('dosen_inbound4_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound4',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function delete_file_bank_stantement_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound4_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_bank_stantement',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/'
		]);
	}

	/**
	 * Get Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function get_file_bank_stantement_file($id)
	{
		if (!$this->is_allowed('dosen_inbound4_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound4 = $this->model_dosen_inbound4->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_bank_stantement',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/',
			'delete_endpoint'   => 'administrator/dosen_inbound4/delete_file_bank_stantement_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function upload_file_asuransi_kesehatan_file()
	{
		if (!$this->is_allowed('dosen_inbound4_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound4',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function delete_file_asuransi_kesehatan_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound4_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_asuransi_kesehatan',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/'
		]);
	}

	/**
	 * Get Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function get_file_asuransi_kesehatan_file($id)
	{
		if (!$this->is_allowed('dosen_inbound4_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound4 = $this->model_dosen_inbound4->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_asuransi_kesehatan',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/',
			'delete_endpoint'   => 'administrator/dosen_inbound4/delete_file_asuransi_kesehatan_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function upload_file_kontrak_kerja_file()
	{
		if (!$this->is_allowed('dosen_inbound4_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound4',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function delete_file_kontrak_kerja_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound4_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_kontrak_kerja',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/'
		]);
	}

	/**
	 * Get Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function get_file_kontrak_kerja_file($id)
	{
		if (!$this->is_allowed('dosen_inbound4_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound4 = $this->model_dosen_inbound4->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_kontrak_kerja',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/',
			'delete_endpoint'   => 'administrator/dosen_inbound4/delete_file_kontrak_kerja_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function upload_file_sertifikat_profesor_file()
	{
		if (!$this->is_allowed('dosen_inbound4_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound4',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function delete_file_sertifikat_profesor_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound4_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_sertifikat_profesor',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/'
		]);
	}

	/**
	 * Get Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function get_file_sertifikat_profesor_file($id)
	{
		if (!$this->is_allowed('dosen_inbound4_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound4 = $this->model_dosen_inbound4->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_sertifikat_profesor',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/',
			'delete_endpoint'   => 'administrator/dosen_inbound4/delete_file_sertifikat_profesor_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function upload_file_sertifikat_keahlian_file()
	{
		if (!$this->is_allowed('dosen_inbound4_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound4',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function delete_file_sertifikat_keahlian_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound4_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_sertifikat_keahlian',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/'
		]);
	}

	/**
	 * Get Image Dosen Inbound4	* 
	 * @return JSON
	 */
	public function get_file_sertifikat_keahlian_file($id)
	{
		if (!$this->is_allowed('dosen_inbound4_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound4 = $this->model_dosen_inbound4->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_sertifikat_keahlian',
			'table_name'        => 'dosen_inbound4',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound4/',
			'delete_endpoint'   => 'administrator/dosen_inbound4/delete_file_sertifikat_keahlian_file'
		]);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('dosen_inbound4_export');

		$this->model_dosen_inbound4->export(
			'dosen_inbound4',
			'dosen_inbound4',
			$this->model_dosen_inbound4->field_search
		);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('dosen_inbound4_export');

		$this->model_dosen_inbound4->pdf('dosen_inbound4', 'dosen_inbound4');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('dosen_inbound4_export');

		$table = $title = 'dosen_inbound4';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_dosen_inbound4->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
}


/* End of file dosen_inbound4.php */
/* Location: ./application/controllers/administrator/Dosen Inbound4.php */