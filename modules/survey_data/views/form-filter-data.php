<form action="" method="POST" role="form">
   <div class="row">
	      <div class="col-md-8">
         <div class="form-group">
            <label for="">fullname</label>
            <?= form_dropdown('fullname', $fullname, fromsess('filterdata_survey_data')['fullname'], ['class' => 'form-control','style' => 'width:100%']) ?>
         </div>
      </div>
	      <div class="col-md-8">
         <div class="form-group">
            <label for="">question_id</label>
            <?= form_dropdown('question_id', $question_id, fromsess('filterdata_survey_data')['question_id'], ['class' => 'form-control','style' => 'width:100%']) ?>
         </div>
      </div>
	      <div class="col-md-8">
         <div class="form-group">
            <label for="">answer</label>
            <?= form_dropdown('answer', $answer, fromsess('filterdata_survey_data')['answer'], ['class' => 'form-control','style' => 'width:100%']) ?>
         </div>
      </div>
	   </div>
</form>

<script>
	$(function() {
		// $('select[name=name1],select[name=name2],select[name=name3]').select2({
		// 	theme: 'classic',
		// 	dropdownParent: $('#filtersurvey_data-dialog'),
		// });
		// $('select[name=name4]').select2({
		// 	tags:true
		// })
	})
</script>