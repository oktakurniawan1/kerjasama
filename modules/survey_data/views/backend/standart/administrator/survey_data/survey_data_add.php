<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Survey SiMKS <small><?= cclang('new', ['Survey SiMKS']); ?> </small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/survey_data'); ?>">Survey SiMKS</a></li>
		<li class="active"><?= cclang('new'); ?></li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">KUESIONER PENELITIAN SiMKS</h3>
							<h5 class="widget-user-desc">OPTIMALISASI SISTEM INFORMASI MANAJEMEN KERJA SAMA TERHADAP EFEKTIFITAS PELAYANAN INFORMASI PUBLIK DI UNIVERSITAS NEGERI SEMARANG</h5>
							<hr>
						</div>
						<?= form_open('', [
							'name'    => 'form_survey_data',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_survey_data',
							'enctype' => 'multipart/form-data',
							'method'  => 'POST'
						]); ?>

						<?php foreach (db_get_all_data('survey') as $i => $row) : ?>
							<div class="form-group ">
								<label for="question_id" class="col-sm-2 control-label"><?php echo $i + 1 ?>
								</label>
								<div class="col-sm-8">
									<select class="form-control" name="question_id[<?= $i + 1 ?>]" readonly>
										<option value="<?= $row->id ?>" selected><?= $row->question; ?></option>
									</select>
									<small class="info help-block">
									</small>
								</div>
							</div>

							<div class="form-group">
								<label for="answer" class="col-sm-2 control-label">
									<i class="required">*</i>
								</label>
								<div class="col-sm-8">
									<div class="col-md-3 padding-left-0">
										<label>
											<input type="radio" class="flat-red" name="answer<?= $i + 1 ?>[]" value="STS"> Sangat Tidak Setuju </label>
									</div>
									<div class="col-md-3 padding-left-0">
										<label>
											<input type="radio" class="flat-red" name="answer<?= $i + 1 ?>[]" value="TS"> Tidak Setuju </label>
									</div>
									<div class="col-md-3 padding-left-0">
										<label>
											<input type="radio" class="flat-red" name="answer<?= $i + 1 ?>[]" value="KS"> Kurang Setuju </label>
									</div>
									<div class="col-md-3 padding-left-0">
										<label>
											<input type="radio" class="flat-red" name="answer<?= $i + 1 ?>[]" value="S"> Setuju </label>
									</div>
									<div class="col-md-3 padding-left-0">
										<label>
											<input type="radio" class="flat-red" name="answer<?= $i + 1 ?>[]" value="SS"> Sangat Setuju </label>
									</div>
									<small class="info help-block"></small>
								</div>
							</div>
						<?php endforeach; ?>


						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	$(document).ready(function() {
		$('.btn_save').click(function() {
			//$('.message').fadeOut();

			var form_survey_data = $('#form_survey_data');
			var data_post = form_survey_data.serializeArray();
			var save_type = $(this).attr('data-stype');

			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: BASE_URL + '/administrator/survey_data/add_save',
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('.steps li').removeClass('error');
					$('form').find('.error-input').remove();
					if (res.success) {

						// if (save_type == 'back') {
						// 	window.location.href = res.redirect;
						// 	return;
						// }

						$('.message').printMessage({
							message: res.message
						});
						// $('.message').fadeIn();
						// resetForm();
						// $('.chosen option').prop('selected', false).trigger('chosen:updated');

					} else {
						if (res.errors) {

							$.each(res.errors, function(index, val) {
								$("input[name='"+index+"']").parents('.form-group').addClass('has-error');
								$("input[name='"+index+"']").parents('.form-group').find('small').prepend(`
                      <div class="error-input">` + val + `</div>
                      `);
							});
							$('.steps li').removeClass('error');
							$('.content section').each(function(index, el) {
								if ($(this).find('.has-error').length) {
									$('.steps li:eq(' + index + ')').addClass('error').find('a').trigger('click');
								}
							});
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/
	}); /*end doc ready*/
</script>