<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Survey Data Controller
 *| --------------------------------------------------------------------------
 *| Survey Data site
 *|
 */
class Survey_data extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_survey_data');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * form filter data 
	 */
	public function filter_session_data()
	{
		$fields['fullname'] = $this->input->post('fullname');
		$fields['question_id'] = $this->input->post('question_id');
		$fields['answer'] = $this->input->post('answer');
		$this->session->set_userdata('filterdata_survey_data', $fields);
		$this->session->set_userdata('is_filtered_survey_data', TRUE);
		echo json_encode(['url' => base_url('administrator/survey_data')]);
	}

	public function clear_filter_session_data()
	{
		$this->session->unset_userdata('filterdata_survey_data');
		$this->session->unset_userdata('is_filtered_survey_data');
		echo json_encode(['url' => base_url('administrator/survey_data')]);
	}

	public function form_filter_data()
	{
		if (!$this->session->has_userdata('filterdata_survey_data')) {
			$fields['fullname'] = 'all';
			$fields['question_id'] = 'all';
			$fields['answer'] = 'all';
			$this->session->set_userdata('filterdata_survey_data', $fields);
		}
		$data['fullname'] = [];
		$data['question_id'] = [];
		$data['answer'] = [];
		$this->load->view('form-filter-data', $data);
	}
	/**
	 * show all Survey Datas
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('survey_data_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('survey_data_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('survey_data_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('survey_data_filter');
			$this->session->unset_userdata('survey_data_field');
		}
		$filter = $this->session->userdata('survey_data_filter');
		$field = $this->session->userdata('survey_data_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['survey_datas'] = $this->model_survey_data->get($filter, $field, $this->limit_page, $offset);
		$this->data['survey_data_counts'] = $this->model_survey_data->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/survey_data/index/',
			'total_rows'   => $this->model_survey_data->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Survey Data List');
		$this->render('backend/standart/administrator/survey_data/survey_data_list', $this->data);
	}

	/**
	 * Add new survey_datas
	 *
	 */
	public function add()
	{
		// $this->is_allowed('survey_data_add');

		$this->template->title('Survey Data New');
		$this->render('backend/standart/administrator/survey_data/survey_data_add', $this->data);
	}

	/**
	 * Add New Survey Datas
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		// dd($this->input->post());
		// if (!$this->is_allowed('survey_data_add', false)) {
		// 	echo json_encode([
		// 		'success' => false,
		// 		'message' => cclang('sorry_you_do_not_have_permission_to_access')
		// 	]);
		// 	exit;
		// }
		foreach ($this->input->post('question_id') as $id) {
			$this->form_validation->set_rules('answer' . $id . '[]', 'Answer', 'trim|required');
		}
		$user = get_user_name();
		$cek = db_get_all_data('survey_data',['username'=>$user->username]);
		if(!empty($cek)){
			$this->data['success'] = false;
			$this->data['message'] = 'Anda sudah mengisi survey';
			echo json_encode($this->data);
			exit;
		}
		if ($this->form_validation->run()) {
			foreach($this->input->post('question_id') as $id){
				$save_data['user_id'] = get_user_data('id');
				$save_data['username'] = $user->username;
				$save_data['fullname'] = $user->full_name;
				$save_data['question_id'] = $id;
				$save_data['answer'] = $this->input->post('answer'.$id)[0];
				$this->model_survey_data->store($save_data);
			}
			$this->data['success'] = true;
			$this->data['message'] = 'Data survey sudah berhasil disimpan. Terimakasih. '.anchor('administrator/dashboard', ' Go Back to Dashboard');
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}
		echo json_encode($this->data);
	}
	/**
	 * Update view Survey Datas
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('survey_data_update');
		$this->data['survey_data'] = $this->model_survey_data->find($id);
		$this->template->title('Survey Data Update');
		$this->render('backend/standart/administrator/survey_data/survey_data_update', $this->data);
	}

	/**
	 * Update Survey Datas
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('survey_data_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('answer', 'Answer', 'trim|required');

		if ($this->form_validation->run()) {

			$save_data = [
				'question_id' => $this->input->post('question_id'),
				'answer' => $this->input->post('answer'),
			];


			$save_survey_data = $this->model_survey_data->change($id, $save_data);

			if ($save_survey_data) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/survey_data', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/survey_data/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/survey_data/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Survey Datas
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('survey_data_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'survey_data'), 'success');
		} else {
			set_message(cclang('error_delete', 'survey_data'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Survey Datas
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('survey_data_view');

		$this->data['survey_data'] = $this->model_survey_data->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Survey Data Detail');
		$this->render('backend/standart/administrator/survey_data/survey_data_view', $this->data);
	}

	/**
	 * delete Survey Datas
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$survey_data = $this->model_survey_data->find($id);



		return $this->model_survey_data->remove($id);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
			'question_id',
			'answer',
		];
		$data = $this->model_survey_data->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_survey_data->export2($data, 'file', $selected_field);

		/*
		$this->is_allowed('survey_data_export');

		$this->model_survey_data->export(
			'survey_data', 
			'survey_data',
			$this->model_survey_data->field_search
		);
		*/
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('survey_data_export');

		$this->model_survey_data->pdf('survey_data', 'survey_data');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('survey_data_export');

		$table = $title = 'survey_data';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_survey_data->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
}


/* End of file survey_data.php */
/* Location: ./application/controllers/administrator/Survey Data.php */