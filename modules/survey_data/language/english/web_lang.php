<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['survey_data'] = 'Survey SiMKS';
$lang['id'] = 'Id';
$lang['username'] = 'Username';
$lang['fullname'] = 'Fullname';
$lang['question_id'] = 'Question';
$lang['answer'] = 'Answer';
