<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_survey_data extends MY_Model {

    private $primary_key    = 'id';
    private $table_name     = 'survey_data';
    public $field_search   = ['fullname', 'question_id', 'answer'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "survey_data.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "survey_data.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "survey_data.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);

		/**form filter data */
		if ($this->session->has_userdata('filterdata_survey_data')) {
			foreach (fromsess('filterdata_survey_data') as $key => $row) {
				if ($row != 'all') $this->db->having($key, $row);
			}
		}
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [], $result = TRUE)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "survey_data.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "survey_data.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "survey_data.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);

		/**form filter data */
		if ($this->session->has_userdata('filterdata_survey_data')) {
			foreach (fromsess('filterdata_survey_data') as $key => $row) {
				if ($row != 'all') $this->db->having($key, $row);
			}
		}
        $this->db->limit($limit, $offset);
        
        $this->sortable();
        
        $query = $this->db->get($this->table_name);

        if ($result)
			return $query->result();
			else return $query;
    }

    public function join_avaiable() {
        $this->db->join('survey', 'survey.id = survey_data.question_id', 'LEFT');
        
        $this->db->select('survey_data.*,survey.question as survey_question');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            $this->db->where($this->table_name.'.user_id', get_user_data('id'));
        }

        return $this;
    }

}

/* End of file Model_survey_data.php */
/* Location: ./application/models/Model_survey_data.php */