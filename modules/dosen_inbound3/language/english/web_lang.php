<?php
defined('BASEPATH') or exit('No direct script access allowed');

$lang['dosen_inbound3'] = 'Dosen Inbound';
$lang['scope'] = 'DN/LN';
$lang['aktivitas'] = 'Jenis Aktivitas';
$lang['kd_prodi'] = 'Program Studi/Unit';
$lang['keg_dosin_id'] = 'Jenis Kegiatan';
$lang['tgs_dosin_id'] = 'Tugas Sebagai';
$lang['judul_kegiatan'] = 'Judul Kegiatan';
$lang['tgl_mulai'] = 'Tanggal Mulai';
$lang['tgl_selesai'] = 'Tanggal Selesai';
$lang['nama_dosin'] = 'Nama Dosen/Tamu';
$lang['nomor_identitas'] = 'No Identitas (KTP/SIM/NIP/PASPOR)';
$lang['institusi_asal'] = 'Institusi Asal';
$lang['mou_ks'] = 'Nota Kesepahaman (MoU)';
$lang['unit_ks'] = 'MoA/IA/LoA (Unit)';
$lang['negara'] = 'Negara';
$lang['file_cv'] = 'File CV';
$lang['file_ktp'] = 'File KTP/NIK';
$lang['file_loa'] = 'File LoA';
$lang['file_sertifikat_keahlian'] = 'File Sertifikat Keahlian';
$lang['file_foto'] = 'File Foto';
$lang['file_asuransi_kesehatan'] = 'File Asuransi Kesehatan';
$lang['file_materi_paparan'] = 'File Materi Paparan';
$lang['deskripsi'] = 'Deskripsi';
$lang['kode_dosen'] = 'Kode Dosen';
$lang['validasi'] = 'Validasi';
