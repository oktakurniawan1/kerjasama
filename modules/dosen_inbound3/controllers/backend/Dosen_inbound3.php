<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Dosen Inbound3 Controller
 *| --------------------------------------------------------------------------
 *| Dosen Inbound3 site
 *|
 */
class Dosen_inbound3 extends Admin
{

	protected $validation_rules = [];

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_dosen_inbound3');
		$this->load->model('ref_prodi/Model_ref_prodi', 'model_ref_prodi');
		$this->lang->load('web_lang', $this->current_lang);

		$this->validation_rules = [
			['field' => 'scope', 'label' => 'DN/LN', 'rules' => 'trim|required'],
			['field' => 'aktivitas', 'label' => 'Jenis Aktivitas', 'rules' => 'trim|required'],
			['field' => 'kd_prodi', 'label' => 'Program Studi/Unit', 'rules' => 'trim|required'],
			['field' => 'keg_dosin_id', 'label' => 'Jenis Kegiatan', 'rules' => 'trim|required'],
			['field' => 'tgs_dosin_id', 'label' => 'Tugas Sebagai', 'rules' => 'trim|required'],
			['field' => 'judul_kegiatan', 'label' => 'Judul Kegiatan', 'rules' => 'trim|required'],
			['field' => 'tgl_mulai', 'label' => 'Tanggal Mulai', 'rules' => 'trim|required'],
			['field' => 'tgl_selesai', 'label' => 'Tanggal Selesai', 'rules' => 'trim|required'],
			['field' => 'nama_dosin', 'label' => 'Nama Dosen/Tamu', 'rules' => 'trim|required'],
			['field' => 'nomor_identitas', 'label' => 'No Identitas (KTP/SIM/NIP/PASPOR)', 'rules' => 'trim|required|max_length[100]'],
			['field' => 'nidn', 'label' => 'Nomor Induk Dosen Nasional', 'rules' => 'trim|required|max_length[10]'],
			['field' => 'institusi_asal', 'label' => 'Institusi Asal', 'rules' => 'trim|required'],
			['field' => 'mou_ks', 'label' => 'Nota Kesepahaman (MoU)', 'rules' => 'trim|required'],
			['field' => 'unit_ks', 'label' => 'MoA/IA/LoA (Unit)', 'rules' => 'trim|required'],
			['field' => 'negara', 'label' => 'Negara', 'rules' => 'trim|required'],
			['field' => 'dosen_inbound3_file_cv_name', 'label' => 'File CV', 'rules' => 'trim|required'],
			['field' => 'dosen_inbound3_file_ktp_name', 'label' => 'File KTP/NIK', 'rules' => 'trim|required'],
			['field' => 'dosen_inbound3_file_loa_name', 'label' => 'File LoA', 'rules' => 'trim|required'],
			['field' => 'dosen_inbound3_file_foto_name', 'label' => 'File Foto', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound3_file_sertifikat_keahlian_name', 'label' => 'File Sertifikat Keahlian', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound3_file_asuransi_kesehatan_name', 'label' => 'File Asuransi Kesehatan', 'rules' => 'trim|required'],
			// ['field' => 'dosen_inbound3_file_materi_paparan_name', 'label' => 'File Materi Paparan', 'rules' => 'trim|required'],
		];

		$this->form_validation->set_rules($this->validation_rules);
	}

	/**
	 * show all Dosen Inbound3s
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		redirect('administrator/dosen_inbound1');
	}

	/**
	 * Add new dosen_inbound3s
	 *
	 */
	public function add()
	{
		$this->is_allowed('dosen_inbound3_add');

		$this->data['filter_prodi'] = get_filter_prodi();

		$this->template->title('Dosen Inbound New');
		$this->data['validation_rules'] = $this->validation_rules;
		$this->render('backend/standart/administrator/dosen_inbound3/dosen_inbound3_add', $this->data);
	}

	/**
	 * Add New Dosen Inbound3s
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('dosen_inbound3_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		if ($this->form_validation->run()) {
			$dosen_inbound3_file_cv_uuid = $this->input->post('dosen_inbound3_file_cv_uuid');
			$dosen_inbound3_file_cv_name = $this->input->post('dosen_inbound3_file_cv_name');
			$dosen_inbound3_file_ktp_uuid = $this->input->post('dosen_inbound3_file_ktp_uuid');
			$dosen_inbound3_file_ktp_name = $this->input->post('dosen_inbound3_file_ktp_name');
			$dosen_inbound3_file_loa_uuid = $this->input->post('dosen_inbound3_file_loa_uuid');
			$dosen_inbound3_file_loa_name = $this->input->post('dosen_inbound3_file_loa_name');
			$dosen_inbound3_file_sertifikat_keahlian_uuid = $this->input->post('dosen_inbound3_file_sertifikat_keahlian_uuid');
			$dosen_inbound3_file_sertifikat_keahlian_name = $this->input->post('dosen_inbound3_file_sertifikat_keahlian_name');
			$dosen_inbound3_file_foto_uuid = $this->input->post('dosen_inbound3_file_foto_uuid');
			$dosen_inbound3_file_foto_name = $this->input->post('dosen_inbound3_file_foto_name');
			$dosen_inbound3_file_asuransi_kesehatan_uuid = $this->input->post('dosen_inbound3_file_asuransi_kesehatan_uuid');
			$dosen_inbound3_file_asuransi_kesehatan_name = $this->input->post('dosen_inbound3_file_asuransi_kesehatan_name');
			// $dosen_inbound3_file_materi_paparan_uuid = $this->input->post('dosen_inbound3_file_materi_paparan_uuid');
			// $dosen_inbound3_file_materi_paparan_name = $this->input->post('dosen_inbound3_file_materi_paparan_name');

			// simpan kode_fak
			$fak = $this->model_ref_prodi->get_single(['kode' => $this->input->post('kd_prodi')]);

			$save_data = [
				'scope'           => $this->input->post('scope'),
				'aktivitas'       => $this->input->post('aktivitas'),
				'kode_fak'        => $fak->kode_fak,
				'kd_prodi'        => $this->input->post('kd_prodi'),
				'keg_dosin_id'    => $this->input->post('keg_dosin_id'),
				'tgs_dosin_id'    => $this->input->post('tgs_dosin_id'),
				'judul_kegiatan'  => $this->input->post('judul_kegiatan'),
				'tgl_mulai'       => $this->input->post('tgl_mulai'),
				'tgl_selesai'     => $this->input->post('tgl_selesai'),
				'nama_dosin'      => $this->input->post('nama_dosin'),
				'nomor_identitas' => $this->input->post('nomor_identitas'),
				'nidn'            => $this->input->post('nidn'),
				'institusi_asal'  => $this->input->post('institusi_asal'),
				'mou_ks'          => $this->input->post('mou_ks'),
				'unit_ks'         => $this->input->post('unit_ks'),
				'negara'          => $this->input->post('negara'),
				'deskripsi'       => $this->input->post('deskripsi'),
				'kode_dosen'      => $this->input->post('kode_dosen'),
				'validasi'        => $this->input->post('validasi'),
			];

			if (!is_dir(FCPATH . '/uploads/dosen_inbound3/')) {
				mkdir(FCPATH . '/uploads/dosen_inbound3/');
			}

			if (!empty($dosen_inbound3_file_cv_name)) {
				$dosen_inbound3_file_cv_name_copy = date('YmdHis') . '-' . $dosen_inbound3_file_cv_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound3_file_cv_uuid . '/' . $dosen_inbound3_file_cv_name,
					FCPATH . 'uploads/dosen_inbound3/' . $dosen_inbound3_file_cv_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3_file_cv_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_cv'] = $dosen_inbound3_file_cv_name_copy;
			}

			if (!empty($dosen_inbound3_file_ktp_name)) {
				$dosen_inbound3_file_ktp_name_copy = date('YmdHis') . '-' . $dosen_inbound3_file_ktp_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound3_file_ktp_uuid . '/' . $dosen_inbound3_file_ktp_name,
					FCPATH . 'uploads/dosen_inbound3/' . $dosen_inbound3_file_ktp_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3_file_ktp_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ktp'] = $dosen_inbound3_file_ktp_name_copy;
			}

			if (!empty($dosen_inbound3_file_loa_name)) {
				$dosen_inbound3_file_loa_name_copy = date('YmdHis') . '-' . $dosen_inbound3_file_loa_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound3_file_loa_uuid . '/' . $dosen_inbound3_file_loa_name,
					FCPATH . 'uploads/dosen_inbound3/' . $dosen_inbound3_file_loa_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_loa'] = $dosen_inbound3_file_loa_name_copy;
			}

			if (!empty($dosen_inbound3_file_sertifikat_keahlian_name)) {
				$dosen_inbound3_file_sertifikat_keahlian_name_copy = date('YmdHis') . '-' . $dosen_inbound3_file_sertifikat_keahlian_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound3_file_sertifikat_keahlian_uuid . '/' . $dosen_inbound3_file_sertifikat_keahlian_name,
					FCPATH . 'uploads/dosen_inbound3/' . $dosen_inbound3_file_sertifikat_keahlian_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3_file_sertifikat_keahlian_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_sertifikat_keahlian'] = $dosen_inbound3_file_sertifikat_keahlian_name_copy;
			}

			if (!empty($dosen_inbound3_file_foto_name)) {
				$dosen_inbound3_file_foto_name_copy = date('YmdHis') . '-' . $dosen_inbound3_file_foto_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound3_file_foto_uuid . '/' . $dosen_inbound3_file_foto_name,
					FCPATH . 'uploads/dosen_inbound3/' . $dosen_inbound3_file_foto_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3_file_foto_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_foto'] = $dosen_inbound3_file_foto_name_copy;
			}

			if (!empty($dosen_inbound3_file_asuransi_kesehatan_name)) {
				$dosen_inbound3_file_asuransi_kesehatan_name_copy = date('YmdHis') . '-' . $dosen_inbound3_file_asuransi_kesehatan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound3_file_asuransi_kesehatan_uuid . '/' . $dosen_inbound3_file_asuransi_kesehatan_name,
					FCPATH . 'uploads/dosen_inbound3/' . $dosen_inbound3_file_asuransi_kesehatan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3_file_asuransi_kesehatan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_asuransi_kesehatan'] = $dosen_inbound3_file_asuransi_kesehatan_name_copy;
			}

			// if (!empty($dosen_inbound3_file_materi_paparan_name)) {
			// 	$dosen_inbound3_file_materi_paparan_name_copy = date('YmdHis') . '-' . $dosen_inbound3_file_materi_paparan_name;

			// 	rename(
			// 		FCPATH . 'uploads/tmp/' . $dosen_inbound3_file_materi_paparan_uuid . '/' . $dosen_inbound3_file_materi_paparan_name,
			// 		FCPATH . 'uploads/dosen_inbound3/' . $dosen_inbound3_file_materi_paparan_name_copy
			// 	);

			// 	if (!is_file(FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3_file_materi_paparan_name_copy)) {
			// 		echo json_encode([
			// 			'success' => false,
			// 			'message' => 'Error uploading file'
			// 		]);
			// 		exit;
			// 	}

			// 	$save_data['file_materi_paparan'] = $dosen_inbound3_file_materi_paparan_name_copy;
			// }


			$save_dosen_inbound3 = $this->model_dosen_inbound3->store($save_data);


			if ($save_dosen_inbound3) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_dosen_inbound3;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/dosen_inbound3/edit/' . $save_dosen_inbound3, 'Edit Dosen Inbound3'),
						anchor('administrator/dosen_inbound3', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/dosen_inbound3/edit/' . $save_dosen_inbound3, 'Edit Dosen Inbound3')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dosen_inbound3');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dosen_inbound3');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Dosen Inbound3s
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('dosen_inbound3_update');

		if (is_groups_in(['Kaprodi', 'Unit'])) {
			$prodi = get_filter_prodi(true);
			$row = $this->model_dosen_inbound3->find($id);
			if (!in_array($row->kd_prodi, $prodi)) {
				set_message(cclang('sorry_you_do_not_have_permission_to_access', 'dosen_inbound1'), 'error');
				redirect_back();
			}
			/**tolak jika sudah divalidasi */
			if ($row->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa mengubah data, karena data sudah divalidasi', 'dosen_inbound1'), 'warning');
				redirect_back();
			}
		}

		$this->data['dosen_inbound3'] = $this->model_dosen_inbound3->find($id);

		$list_mou = db_get_all_data('mou_kerjasama', "scope='{$this->data['dosen_inbound3']->scope}' AND tgl_selesai > curdate() ");
		$cur_mou = db_get_all_data('mou_kerjasama', "id = '{$this->data['dosen_inbound3']->mou_ks}'", 'row');

		if (!is_null($cur_mou)) {
			$tgl = DateTime::createFromFormat('Y-m-d', $cur_mou->tgl_selesai);
			$now = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
			if ($tgl < $now) $list_mou[] = $cur_mou;
		}
		$list_mou[] = (object)['id' => '1', 'nama_mitra' => 'Pengajuan Baru', 'doc_nomor' => 'XXX'];
		$this->data['list_mou'] = $list_mou;

		//moa
		$list_moa = db_get_all_data('si_kerjasama', "mou_ks = '{$this->data['dosen_inbound3']->mou_ks}'  AND tgl_selesai > curdate() AND kd_prodi = '{$this->data['dosen_inbound3']->kd_prodi}' ");
		$cur_moa = db_get_all_data('si_kerjasama', "id = '{$this->data['dosen_inbound3']->unit_ks}'", 'row');

		if (!is_null($cur_moa)) {
			$tgl = DateTime::createFromFormat('Y-m-d', $cur_moa->tgl_selesai);
			$now = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
			if ($tgl < $now) $list_moa[] = $cur_moa;
		}
		//print_r($list_moa);
		//die();
		$this->data['list_moa'] = $list_moa;
		$this->data['filter_prodi'] = get_filter_prodi();
		$this->data['validation_rules'] = $this->validation_rules;
		$this->template->title('Dosen Inbound Update');
		$this->render('backend/standart/administrator/dosen_inbound3/dosen_inbound3_update', $this->data);
	}

	/**
	 * Update Dosen Inbound3s
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('dosen_inbound3_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		if ($this->form_validation->run()) {
			$dosen_inbound3_file_cv_uuid = $this->input->post('dosen_inbound3_file_cv_uuid');
			$dosen_inbound3_file_cv_name = $this->input->post('dosen_inbound3_file_cv_name');
			$dosen_inbound3_file_ktp_uuid = $this->input->post('dosen_inbound3_file_ktp_uuid');
			$dosen_inbound3_file_ktp_name = $this->input->post('dosen_inbound3_file_ktp_name');
			$dosen_inbound3_file_loa_uuid = $this->input->post('dosen_inbound3_file_loa_uuid');
			$dosen_inbound3_file_loa_name = $this->input->post('dosen_inbound3_file_loa_name');
			$dosen_inbound3_file_sertifikat_keahlian_uuid = $this->input->post('dosen_inbound3_file_sertifikat_keahlian_uuid');
			$dosen_inbound3_file_sertifikat_keahlian_name = $this->input->post('dosen_inbound3_file_sertifikat_keahlian_name');
			$dosen_inbound3_file_foto_uuid = $this->input->post('dosen_inbound3_file_foto_uuid');
			$dosen_inbound3_file_foto_name = $this->input->post('dosen_inbound3_file_foto_name');
			$dosen_inbound3_file_asuransi_kesehatan_uuid = $this->input->post('dosen_inbound3_file_asuransi_kesehatan_uuid');
			$dosen_inbound3_file_asuransi_kesehatan_name = $this->input->post('dosen_inbound3_file_asuransi_kesehatan_name');
			// $dosen_inbound3_file_materi_paparan_uuid = $this->input->post('dosen_inbound3_file_materi_paparan_uuid');
			// $dosen_inbound3_file_materi_paparan_name = $this->input->post('dosen_inbound3_file_materi_paparan_name');

			$fak = $this->model_ref_prodi->get_single(['kode' => $this->input->post('kd_prodi')]);

			$save_data = [
				'scope'           => $this->input->post('scope'),
				'aktivitas'       => $this->input->post('aktivitas'),
				'kode_fak'        => $fak->kode_fak,
				'kd_prodi'        => $this->input->post('kd_prodi'),
				'keg_dosin_id'    => $this->input->post('keg_dosin_id'),
				'tgs_dosin_id'    => $this->input->post('tgs_dosin_id'),
				'judul_kegiatan'  => $this->input->post('judul_kegiatan'),
				'tgl_mulai'       => $this->input->post('tgl_mulai'),
				'tgl_selesai'     => $this->input->post('tgl_selesai'),
				'nama_dosin'      => $this->input->post('nama_dosin'),
				'nomor_identitas' => $this->input->post('nomor_identitas'),
				'nidn'            => $this->input->post('nidn'),
				'institusi_asal'  => $this->input->post('institusi_asal'),
				'mou_ks'          => $this->input->post('mou_ks'),
				'unit_ks'         => $this->input->post('unit_ks'),
				'negara'          => $this->input->post('negara'),
				'deskripsi'       => $this->input->post('deskripsi'),
				'kode_dosen'      => $this->input->post('kode_dosen'),
				'validasi'        => $this->input->post('validasi'),
			];

			if (!is_dir(FCPATH . '/uploads/dosen_inbound3/')) {
				mkdir(FCPATH . '/uploads/dosen_inbound3/');
			}

			if (!empty($dosen_inbound3_file_cv_uuid)) {
				$dosen_inbound3_file_cv_name_copy = date('YmdHis') . '-' . $dosen_inbound3_file_cv_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound3_file_cv_uuid . '/' . $dosen_inbound3_file_cv_name,
					FCPATH . 'uploads/dosen_inbound3/' . $dosen_inbound3_file_cv_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3_file_cv_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_cv'] = $dosen_inbound3_file_cv_name_copy;
			}

			if (!empty($dosen_inbound3_file_ktp_uuid)) {
				$dosen_inbound3_file_ktp_name_copy = date('YmdHis') . '-' . $dosen_inbound3_file_ktp_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound3_file_ktp_uuid . '/' . $dosen_inbound3_file_ktp_name,
					FCPATH . 'uploads/dosen_inbound3/' . $dosen_inbound3_file_ktp_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3_file_ktp_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ktp'] = $dosen_inbound3_file_ktp_name_copy;
			}

			if (!empty($dosen_inbound3_file_loa_uuid)) {
				$dosen_inbound3_file_loa_name_copy = date('YmdHis') . '-' . $dosen_inbound3_file_loa_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound3_file_loa_uuid . '/' . $dosen_inbound3_file_loa_name,
					FCPATH . 'uploads/dosen_inbound3/' . $dosen_inbound3_file_loa_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_loa'] = $dosen_inbound3_file_loa_name_copy;
			}

			if (!empty($dosen_inbound3_file_sertifikat_keahlian_uuid)) {
				$dosen_inbound3_file_sertifikat_keahlian_name_copy = date('YmdHis') . '-' . $dosen_inbound3_file_sertifikat_keahlian_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound3_file_sertifikat_keahlian_uuid . '/' . $dosen_inbound3_file_sertifikat_keahlian_name,
					FCPATH . 'uploads/dosen_inbound3/' . $dosen_inbound3_file_sertifikat_keahlian_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3_file_sertifikat_keahlian_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_sertifikat_keahlian'] = $dosen_inbound3_file_sertifikat_keahlian_name_copy;
			}

			if (!empty($dosen_inbound3_file_foto_uuid)) {
				$dosen_inbound3_file_foto_name_copy = date('YmdHis') . '-' . $dosen_inbound3_file_foto_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound3_file_foto_uuid . '/' . $dosen_inbound3_file_foto_name,
					FCPATH . 'uploads/dosen_inbound3/' . $dosen_inbound3_file_foto_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3_file_foto_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_foto'] = $dosen_inbound3_file_foto_name_copy;
			}

			if (!empty($dosen_inbound3_file_asuransi_kesehatan_uuid)) {
				$dosen_inbound3_file_asuransi_kesehatan_name_copy = date('YmdHis') . '-' . $dosen_inbound3_file_asuransi_kesehatan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $dosen_inbound3_file_asuransi_kesehatan_uuid . '/' . $dosen_inbound3_file_asuransi_kesehatan_name,
					FCPATH . 'uploads/dosen_inbound3/' . $dosen_inbound3_file_asuransi_kesehatan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3_file_asuransi_kesehatan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_asuransi_kesehatan'] = $dosen_inbound3_file_asuransi_kesehatan_name_copy;
			}

			// if (!empty($dosen_inbound3_file_materi_paparan_uuid)) {
			// 	$dosen_inbound3_file_materi_paparan_name_copy = date('YmdHis') . '-' . $dosen_inbound3_file_materi_paparan_name;

			// 	rename(
			// 		FCPATH . 'uploads/tmp/' . $dosen_inbound3_file_materi_paparan_uuid . '/' . $dosen_inbound3_file_materi_paparan_name,
			// 		FCPATH . 'uploads/dosen_inbound3/' . $dosen_inbound3_file_materi_paparan_name_copy
			// 	);

			// 	if (!is_file(FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3_file_materi_paparan_name_copy)) {
			// 		echo json_encode([
			// 			'success' => false,
			// 			'message' => 'Error uploading file'
			// 		]);
			// 		exit;
			// 	}

			// 	$save_data['file_materi_paparan'] = $dosen_inbound3_file_materi_paparan_name_copy;
			// }


			$save_dosen_inbound3 = $this->model_dosen_inbound3->change($id, $save_data);

			if ($save_dosen_inbound3) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/dosen_inbound3', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dosen_inbound3/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dosen_inbound3/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Dosen Inbound3s
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('dosen_inbound3_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'dosen_inbound3'), 'success');
		} else {
			set_message(cclang('error_delete', 'dosen_inbound3'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Dosen Inbound3s
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('dosen_inbound3_view');

		$this->data['files'] = $this->config->item('dosin3_file');
		$this->data['dosen_inbound3'] = $this->model_dosen_inbound3->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Dosen Inbound Detail');
		$this->render('backend/standart/administrator/dosen_inbound3/dosen_inbound3_view', $this->data);
	}

	/**
	 * delete Dosen Inbound3s
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$dosen_inbound3 = $this->model_dosen_inbound3->find($id);

		if (!empty($dosen_inbound3->file_cv)) {
			$path = FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3->file_cv;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound3->file_ktp)) {
			$path = FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3->file_ktp;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound3->file_loa)) {
			$path = FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3->file_loa;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound3->file_sertifikat_keahlian)) {
			$path = FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3->file_sertifikat_keahlian;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound3->file_foto)) {
			$path = FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3->file_foto;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($dosen_inbound3->file_asuransi_kesehatan)) {
			$path = FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3->file_asuransi_kesehatan;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}

		// if (!empty($dosen_inbound3->file_materi_paparan)) {
		// 	$path = FCPATH . '/uploads/dosen_inbound3/' . $dosen_inbound3->file_materi_paparan;

		// 	if (is_file($path)) {
		// 		$delete_file = unlink($path);
		// 	}
		// }


		return $this->model_dosen_inbound3->remove($id);
	}

	/**
	 * Upload Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function upload_file_cv_file()
	{
		if (!$this->is_allowed('dosen_inbound3_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound3',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function delete_file_cv_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound3_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_cv',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound3',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound3/'
		]);
	}

	/**
	 * Get Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function get_file_cv_file($id)
	{
		if (!$this->is_allowed('dosen_inbound3_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound3 = $this->model_dosen_inbound3->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_cv',
			'table_name'        => 'dosen_inbound3',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound3/',
			'delete_endpoint'   => 'administrator/dosen_inbound3/delete_file_cv_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function upload_file_ktp_file()
	{
		if (!$this->is_allowed('dosen_inbound3_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound3',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function delete_file_ktp_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound3_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ktp',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound3',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound3/'
		]);
	}

	/**
	 * Get Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function get_file_ktp_file($id)
	{
		if (!$this->is_allowed('dosen_inbound3_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound3 = $this->model_dosen_inbound3->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ktp',
			'table_name'        => 'dosen_inbound3',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound3/',
			'delete_endpoint'   => 'administrator/dosen_inbound3/delete_file_ktp_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function upload_file_loa_file()
	{
		if (!$this->is_allowed('dosen_inbound3_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound3',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function delete_file_loa_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound3_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_loa',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound3',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound3/'
		]);
	}

	/**
	 * Get Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function get_file_loa_file($id)
	{
		if (!$this->is_allowed('dosen_inbound3_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound3 = $this->model_dosen_inbound3->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_loa',
			'table_name'        => 'dosen_inbound3',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound3/',
			'delete_endpoint'   => 'administrator/dosen_inbound3/delete_file_loa_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function upload_file_sertifikat_keahlian_file()
	{
		if (!$this->is_allowed('dosen_inbound3_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound3',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function delete_file_sertifikat_keahlian_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound3_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_sertifikat_keahlian',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound3',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound3/'
		]);
	}

	/**
	 * Get Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function get_file_sertifikat_keahlian_file($id)
	{
		if (!$this->is_allowed('dosen_inbound3_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound3 = $this->model_dosen_inbound3->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_sertifikat_keahlian',
			'table_name'        => 'dosen_inbound3',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound3/',
			'delete_endpoint'   => 'administrator/dosen_inbound3/delete_file_sertifikat_keahlian_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function upload_file_foto_file()
	{
		if (!$this->is_allowed('dosen_inbound3_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound3',
			'allowed_types' => 'pdf|jpg',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function delete_file_foto_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound3_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_foto',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound3',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound3/'
		]);
	}

	/**
	 * Get Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function get_file_foto_file($id)
	{
		if (!$this->is_allowed('dosen_inbound3_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound3 = $this->model_dosen_inbound3->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_foto',
			'table_name'        => 'dosen_inbound3',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound3/',
			'delete_endpoint'   => 'administrator/dosen_inbound3/delete_file_foto_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function upload_file_asuransi_kesehatan_file()
	{
		if (!$this->is_allowed('dosen_inbound3_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'dosen_inbound3',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function delete_file_asuransi_kesehatan_file($uuid)
	{
		if (!$this->is_allowed('dosen_inbound3_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_asuransi_kesehatan',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'dosen_inbound3',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound3/'
		]);
	}

	/**
	 * Get Image Dosen Inbound3	* 
	 * @return JSON
	 */
	public function get_file_asuransi_kesehatan_file($id)
	{
		if (!$this->is_allowed('dosen_inbound3_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$dosen_inbound3 = $this->model_dosen_inbound3->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_asuransi_kesehatan',
			'table_name'        => 'dosen_inbound3',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/dosen_inbound3/',
			'delete_endpoint'   => 'administrator/dosen_inbound3/delete_file_asuransi_kesehatan_file'
		]);
	}

	/**
	 * Upload Image Dosen Inbound3	* 
	 * @return JSON
	 */

	// public function upload_file_materi_paparan_file()
	// {
	// 	if (!$this->is_allowed('dosen_inbound3_add', false)) {
	// 		echo json_encode([
	// 			'success' => false,
	// 			'message' => cclang('sorry_you_do_not_have_permission_to_access')
	// 		]);
	// 		exit;
	// 	}

	// 	$uuid = $this->input->post('qquuid');

	// 	echo $this->upload_file([
	// 		'uuid' 		 	=> $uuid,
	// 		'table_name' 	=> 'dosen_inbound3',
	// 		'allowed_types' => 'pdf',
	// 		'max_size' 	 	=> 10240,
	// 	]);
	// }

	/**
	 * Delete Image Dosen Inbound3	* 
	 * @return JSON
	 */

	// public function delete_file_materi_paparan_file($uuid)
	// {
	// 	if (!$this->is_allowed('dosen_inbound3_delete', false)) {
	// 		echo json_encode([
	// 			'success' => false,
	// 			'error' => cclang('sorry_you_do_not_have_permission_to_access')
	// 		]);
	// 		exit;
	// 	}

	// 	echo $this->delete_file([
	// 		'uuid'              => $uuid,
	// 		'delete_by'         => $this->input->get('by'),
	// 		'field_name'        => 'file_materi_paparan',
	// 		'upload_path_tmp'   => './uploads/tmp/',
	// 		'table_name'        => 'dosen_inbound3',
	// 		'primary_key'       => 'id',
	// 		'upload_path'       => 'uploads/dosen_inbound3/'
	// 	]);
	// }

	/**
	 * Get Image Dosen Inbound3	* 
	 * @return JSON
	 */

	// public function get_file_materi_paparan_file($id)
	// {
	// 	if (!$this->is_allowed('dosen_inbound3_update', false)) {
	// 		echo json_encode([
	// 			'success' => false,
	// 			'message' => 'Image not loaded, you do not have permission to access'
	// 		]);
	// 		exit;
	// 	}

	// 	$dosen_inbound3 = $this->model_dosen_inbound3->find($id);

	// 	echo $this->get_file([
	// 		'uuid'              => $id,
	// 		'delete_by'         => 'id',
	// 		'field_name'        => 'file_materi_paparan',
	// 		'table_name'        => 'dosen_inbound3',
	// 		'primary_key'       => 'id',
	// 		'upload_path'       => 'uploads/dosen_inbound3/',
	// 		'delete_endpoint'   => 'administrator/dosen_inbound3/delete_file_materi_paparan_file'
	// 	]);
	// }


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('dosen_inbound3_export');

		$this->model_dosen_inbound3->export(
			'dosen_inbound3',
			'dosen_inbound3',
			$this->model_dosen_inbound3->field_search
		);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('dosen_inbound3_export');

		$this->model_dosen_inbound3->pdf('dosen_inbound3', 'dosen_inbound3');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('dosen_inbound3_export');

		$table = $title = 'dosen_inbound3';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_dosen_inbound3->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
}


/* End of file dosen_inbound3.php */
/* Location: ./application/controllers/administrator/Dosen Inbound3.php */