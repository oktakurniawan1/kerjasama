<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['karyawan_ttd'] = 'Tandatangan Pegawai';
$lang['id'] = 'Id';
$lang['nip'] = 'NIP';
$lang['nama'] = 'Nama';
$lang['jabatan'] = 'Jabatan';
$lang['file_ttd'] = 'File Tanda Tangan';
