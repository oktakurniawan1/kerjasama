<form action="" method="POST" role="form">
   <div class="row">
	      <div class="col-md-8">
         <div class="form-group">
            <label for="">nip</label>
            <?= form_dropdown('nip', $nip, fromsess('filterdata_karyawan_ttd')['nip'], ['class' => 'form-control','style' => 'width:100%']) ?>
         </div>
      </div>
	      <div class="col-md-8">
         <div class="form-group">
            <label for="">nama</label>
            <?= form_dropdown('nama', $nama, fromsess('filterdata_karyawan_ttd')['nama'], ['class' => 'form-control','style' => 'width:100%']) ?>
         </div>
      </div>
	      <div class="col-md-8">
         <div class="form-group">
            <label for="">jabatan</label>
            <?= form_dropdown('jabatan', $jabatan, fromsess('filterdata_karyawan_ttd')['jabatan'], ['class' => 'form-control','style' => 'width:100%']) ?>
         </div>
      </div>
	      <div class="col-md-8">
         <div class="form-group">
            <label for="">file_ttd</label>
            <?= form_dropdown('file_ttd', $file_ttd, fromsess('filterdata_karyawan_ttd')['file_ttd'], ['class' => 'form-control','style' => 'width:100%']) ?>
         </div>
      </div>
	   </div>
</form>

<script>
	$(function() {
		// $('select[name=name1],select[name=name2],select[name=name3]').select2({
		// 	theme: 'classic',
		// 	dropdownParent: $('#filterkaryawan_ttd-dialog'),
		// });
		// $('select[name=name4]').select2({
		// 	tags:true
		// })
	})
</script>