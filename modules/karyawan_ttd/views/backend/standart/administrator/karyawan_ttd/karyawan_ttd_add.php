<!-- Fine Uploader Gallery CSS file
====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Tandatangan Pegawai <small><?= cclang('new', ['Tandatangan Pegawai']); ?> </small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/karyawan_ttd'); ?>">Tandatangan Pegawai</a></li>
		<li class="active"><?= cclang('new'); ?></li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Tandatangan Pegawai</h3>
							<h5 class="widget-user-desc"><?= cclang('new', ['Tandatangan Pegawai']); ?></h5>
							<hr>
						</div>
						<?= form_open('', [
							'name'    => 'form_karyawan_ttd',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_karyawan_ttd',
							'enctype' => 'multipart/form-data',
							'method'  => 'POST'
						]); ?>

						<div class="form-group ">
							<label for="nip" class="col-sm-2 control-label sr-only">
							</label>
							<div class="col-sm-2">
								<button type="button" class="btn btn-sm btn-primary" onclick="cari_pegawai()"><i class='fa fa-search'></i> Cari di simpeg</button>
							</div>
						</div>

						<div class="form-group ">
							<label for="nip" class="col-sm-2 control-label">NIP
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nip" id="nip" placeholder="NIP" value="<?= set_value('nip'); ?>">
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nama" class="col-sm-2 control-label">Nama
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?= set_value('nama'); ?>">
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="jabatan" class="col-sm-2 control-label">Jabatan
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="jabatan" id="jabatan" placeholder="Jabatan" value="<?= set_value('jabatan'); ?>">
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_ttd" class="col-sm-2 control-label">File Tanda Tangan
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="karyawan_ttd_file_ttd_galery"></div>
								<input class="data_file" name="karyawan_ttd_file_ttd_uuid" id="karyawan_ttd_file_ttd_uuid" type="hidden" value="<?= set_value('karyawan_ttd_file_ttd_uuid'); ?>">
								<input class="data_file" name="karyawan_ttd_file_ttd_name" id="karyawan_ttd_file_ttd_name" type="hidden" value="<?= set_value('karyawan_ttd_file_ttd_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PNG.</small>
							</div>
						</div>

						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	$(document).ready(function() {


		$('#btn_cancel').click(function() {
			swal({
					title: "<?= cclang('are_you_sure'); ?>",
					text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/karyawan_ttd';
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();

			var form_karyawan_ttd = $('#form_karyawan_ttd');
			var data_post = form_karyawan_ttd.serializeArray();
			var save_type = $(this).attr('data-stype');

			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: BASE_URL + '/administrator/karyawan_ttd/add_save',
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('.steps li').removeClass('error');
					$('form').find('.error-input').remove();
					if (res.success) {
						var id_file_ttd = $('#karyawan_ttd_file_ttd_galery').find('li').attr('qq-file-id');

						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						resetForm();
						if (typeof id_file_ttd !== 'undefined') {
							$('#karyawan_ttd_file_ttd_galery').fineUploader('deleteFile', id_file_ttd);
						}
						$('.chosen option').prop('selected', false).trigger('chosen:updated');

					} else {
						if (res.errors) {

							$.each(res.errors, function(index, val) {
								$('form #' + index).parents('.form-group').addClass('has-error');
								$('form #' + index).parents('.form-group').find('small').prepend(`<div class="error-input">` + val + `</div>`);
							});
							$('.steps li').removeClass('error');
							$('.content section').each(function(index, el) {
								if ($(this).find('.has-error').length) {
									$('.steps li:eq(' + index + ')').addClass('error').find('a').trigger('click');
								}
							});
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/

		var params = {};
		params[csrf] = token;

		$('#karyawan_ttd_file_ttd_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/karyawan_ttd/upload_file_ttd_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/karyawan_ttd/delete_file_ttd_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["png"],
				sizeLimit: 0,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#karyawan_ttd_file_ttd_galery').fineUploader('getUuid', id);
						$('#karyawan_ttd_file_ttd_uuid').val(uuid);
						$('#karyawan_ttd_file_ttd_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#karyawan_ttd_file_ttd_uuid').val();
					$.get(BASE_URL + '/administrator/karyawan_ttd/delete_file_ttd_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#karyawan_ttd_file_ttd_uuid').val('');
						$('#karyawan_ttd_file_ttd_name').val('');
					}
				}
			}
		}); /*end file_ttd galery*/
	}); /*end doc ready*/
	function cari_pegawai() {
		BootstrapDialog.show({
			title: 'Cari data Simpeg',
			draggable: true,
			message: function(dialog) {
				var $message = $('<div></div>');
				$.get('<?= site_url('administrator/karyawan_ttd/get_pegawai') ?>', {}, function(response) {
					$message.html(response);
				})
				return $message;
			},
			buttons: [{
				label: 'Ambil',
				icon: 'glyphicon glyphicon-plus',
				cssClass: 'btn-primary',
				action: function(dialog) {
					$('#nip').val($('#pegawai option:selected').data('nip'));
					$('#nama').val($('#pegawai option:selected').data('nama'));
					$('#jabatan').val($('#pegawai option:selected').data('jabatan'));

					dialog.close();
				}
			}, {
				label: 'Tutup',
				action: function(dialog) {
					dialog.close();
				}
			}],
		});
	}
</script>