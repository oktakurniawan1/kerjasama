<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Karyawan Ttd Controller
 *| --------------------------------------------------------------------------
 *| Karyawan Ttd site
 *|
 */
class Karyawan_ttd extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_karyawan_ttd');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * form filter data 
	 */
	public function filter_session_data()
	{
		$fields['nip'] = $this->input->post('nip');
		$fields['nama'] = $this->input->post('nama');
		$fields['jabatan'] = $this->input->post('jabatan');
		$fields['file_ttd'] = $this->input->post('file_ttd');
		$this->session->set_userdata('filterdata_karyawan_ttd', $fields);
		$this->session->set_userdata('is_filtered_karyawan_ttd', TRUE);
		echo json_encode(['url' => base_url('administrator/karyawan_ttd')]);
	}

	public function clear_filter_session_data()
	{
		$this->session->unset_userdata('filterdata_karyawan_ttd');
		$this->session->unset_userdata('is_filtered_karyawan_ttd');
		echo json_encode(['url' => base_url('administrator/karyawan_ttd')]);
	}

	public function form_filter_data()
	{
		if (!$this->session->has_userdata('filterdata_karyawan_ttd')) {
			$fields['nip'] = 'all';
			$fields['nama'] = 'all';
			$fields['jabatan'] = 'all';
			$fields['file_ttd'] = 'all';
			$this->session->set_userdata('filterdata_karyawan_ttd', $fields);
		}
		$data['nip'] = [];
		$data['nama'] = [];
		$data['jabatan'] = [];
		$data['file_ttd'] = [];
		$this->load->view('form-filter-data', $data);
	}
	/**
	 * show all Karyawan Ttds
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('karyawan_ttd_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('karyawan_ttd_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('karyawan_ttd_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('karyawan_ttd_filter');
			$this->session->unset_userdata('karyawan_ttd_field');
		}
		$filter = $this->session->userdata('karyawan_ttd_filter');
		$field = $this->session->userdata('karyawan_ttd_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['karyawan_ttds'] = $this->model_karyawan_ttd->get($filter, $field, $this->limit_page, $offset);
		$this->data['karyawan_ttd_counts'] = $this->model_karyawan_ttd->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/karyawan_ttd/index/',
			'total_rows'   => $this->model_karyawan_ttd->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Tandatangan List');
		$this->render('backend/standart/administrator/karyawan_ttd/karyawan_ttd_list', $this->data);
	}

	/**
	 * Add new karyawan_ttds
	 *
	 */
	public function add()
	{
		$this->is_allowed('karyawan_ttd_add');

		$this->template->title('Tandatangan New');
		$this->render('backend/standart/administrator/karyawan_ttd/karyawan_ttd_add', $this->data);
	}

	/**
	 * Add New Karyawan Ttds
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('karyawan_ttd_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('karyawan_ttd_file_ttd_name', 'File Tanda Tangan', 'trim|required');


		if ($this->form_validation->run()) {
			$karyawan_ttd_file_ttd_uuid = $this->input->post('karyawan_ttd_file_ttd_uuid');
			$karyawan_ttd_file_ttd_name = $this->input->post('karyawan_ttd_file_ttd_name');

			$save_data = [
				'nip'        => $this->input->post('nip'),
				'nama'       => $this->input->post('nama'),
				'jabatan'    => $this->input->post('jabatan'),
				'updated_at' => date('Y-m-d H:i:s')
			];

			if (!is_dir(FCPATH . '/uploads/karyawan_ttd/')) {
				mkdir(FCPATH . '/uploads/karyawan_ttd/');
			}

			if (!empty($karyawan_ttd_file_ttd_name)) {
				$karyawan_ttd_file_ttd_name_copy = date('YmdHis') . '-' . $karyawan_ttd_file_ttd_name;

				rename(
					FCPATH . 'uploads/tmp/' . $karyawan_ttd_file_ttd_uuid . '/' . $karyawan_ttd_file_ttd_name,
					FCPATH . 'uploads/karyawan_ttd/' . $karyawan_ttd_file_ttd_name_copy
				);

				if (!is_file(FCPATH . '/uploads/karyawan_ttd/' . $karyawan_ttd_file_ttd_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ttd'] = $karyawan_ttd_file_ttd_name_copy;
			}


			$save_karyawan_ttd = $this->model_karyawan_ttd->store($save_data);


			if ($save_karyawan_ttd) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_karyawan_ttd;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/karyawan_ttd/edit/' . $save_karyawan_ttd, 'Edit Karyawan Ttd'),
						anchor('administrator/karyawan_ttd', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/karyawan_ttd/edit/' . $save_karyawan_ttd, 'Edit Karyawan Ttd')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/karyawan_ttd');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/karyawan_ttd');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Karyawan Ttds
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('karyawan_ttd_update');

		$this->data['karyawan_ttd'] = $this->model_karyawan_ttd->find($id);

		$this->template->title('Tandatangan Update');
		$this->render('backend/standart/administrator/karyawan_ttd/karyawan_ttd_update', $this->data);
	}

	/**
	 * Update Karyawan Ttds
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('karyawan_ttd_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('karyawan_ttd_file_ttd_name', 'File Tanda Tangan', 'trim|required');

		if ($this->form_validation->run()) {
			$karyawan_ttd_file_ttd_uuid = $this->input->post('karyawan_ttd_file_ttd_uuid');
			$karyawan_ttd_file_ttd_name = $this->input->post('karyawan_ttd_file_ttd_name');

			$save_data = [
				'nip'        => $this->input->post('nip'),
				'nama'       => $this->input->post('nama'),
				'jabatan'    => $this->input->post('jabatan'),
				'updated_at' => date('Y-m-d H:i:s')
			];

			if (!is_dir(FCPATH . '/uploads/karyawan_ttd/')) {
				mkdir(FCPATH . '/uploads/karyawan_ttd/');
			}

			if (!empty($karyawan_ttd_file_ttd_uuid)) {
				$karyawan_ttd_file_ttd_name_copy = date('YmdHis') . '-' . $karyawan_ttd_file_ttd_name;

				rename(
					FCPATH . 'uploads/tmp/' . $karyawan_ttd_file_ttd_uuid . '/' . $karyawan_ttd_file_ttd_name,
					FCPATH . 'uploads/karyawan_ttd/' . $karyawan_ttd_file_ttd_name_copy
				);

				if (!is_file(FCPATH . '/uploads/karyawan_ttd/' . $karyawan_ttd_file_ttd_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ttd'] = $karyawan_ttd_file_ttd_name_copy;
			}


			$save_karyawan_ttd = $this->model_karyawan_ttd->change($id, $save_data);

			if ($save_karyawan_ttd) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/karyawan_ttd', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/karyawan_ttd/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/karyawan_ttd/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Karyawan Ttds
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('karyawan_ttd_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'karyawan_ttd'), 'success');
		} else {
			set_message(cclang('error_delete', 'karyawan_ttd'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Karyawan Ttds
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('karyawan_ttd_view');

		$this->data['karyawan_ttd'] = $this->model_karyawan_ttd->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Tandatangan Detail');
		$this->render('backend/standart/administrator/karyawan_ttd/karyawan_ttd_view', $this->data);
	}

	/**
	 * delete Karyawan Ttds
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$karyawan_ttd = $this->model_karyawan_ttd->find($id);

		if (!empty($karyawan_ttd->file_ttd)) {
			$path = FCPATH . '/uploads/karyawan_ttd/' . $karyawan_ttd->file_ttd;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}


		return $this->model_karyawan_ttd->remove($id);
	}

	/**
	 * Upload Image Karyawan Ttd	* 
	 * @return JSON
	 */
	public function upload_file_ttd_file()
	{
		if (!$this->is_allowed('karyawan_ttd_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'karyawan_ttd',
			'allowed_types' => 'png',
		],true);
	}

	/**
	 * Delete Image Karyawan Ttd	* 
	 * @return JSON
	 */
	public function delete_file_ttd_file($uuid)
	{
		if (!$this->is_allowed('karyawan_ttd_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ttd',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'karyawan_ttd',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/karyawan_ttd/'
		]);
	}

	/**
	 * Get Image Karyawan Ttd	* 
	 * @return JSON
	 */
	public function get_file_ttd_file($id)
	{
		if (!$this->is_allowed('karyawan_ttd_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$karyawan_ttd = $this->model_karyawan_ttd->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ttd',
			'table_name'        => 'karyawan_ttd',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/karyawan_ttd/',
			'delete_endpoint'   => 'administrator/karyawan_ttd/delete_file_ttd_file'
		]);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
			'nip',
			'nama',
			'jabatan',
		];
		$data = $this->model_karyawan_ttd->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_karyawan_ttd->export2($data, 'file', $selected_field);

		/*
		$this->is_allowed('karyawan_ttd_export');

		$this->model_karyawan_ttd->export(
			'karyawan_ttd', 
			'karyawan_ttd',
			$this->model_karyawan_ttd->field_search
		);
		*/
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('karyawan_ttd_export');

		$this->model_karyawan_ttd->pdf('karyawan_ttd', 'karyawan_ttd');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('karyawan_ttd_export');

		$table = $title = 'karyawan_ttd';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_karyawan_ttd->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}

	public function get_pegawai(){
		$data['pegawai'] = $this->model_karyawan_ttd->get_pegawai();
		$this->load->view('_cari_pegawai', $data);
	}
}


/* End of file karyawan_ttd.php */
/* Location: ./application/controllers/administrator/Karyawan Ttd.php */