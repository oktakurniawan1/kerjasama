<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| File Controller
 *| --------------------------------------------------------------------------
 *| user site
 *|
 */
class File extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('user/model_user');
	}

	/**
	 * download file
	 *
	 * @var $file_path String
	 * @var $file_name String
	 */
	public function download($file_path = null, $file_name = null)
	{
		$file_name = $this->uri->segment(5, $file_name);
		$ext = explode('.',$file_name);
		$ext = end($ext);
		$mime = false;
		$mode = 'attachment';
		if($ext == 'pdf'){
			$mode = 'inline';
			$mime = 'application/pdf';
		}
		$this->load->helper('download');
		$path = FCPATH . 'uploads/' . $file_path . '/' . $file_name;
		force_download($path, NULL, $mime, $mode);
	}
}
