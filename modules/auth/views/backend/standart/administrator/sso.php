<?= form_open(site_url('administrator/auth/post_sso'), [
	'name'    => 'formsso', 
	'method'  => 'POST'
]); ?>
<!-- jangan lupa sertakan CSRF token jika framework Anda mewajibkan -->
<input type="hidden" name="sso_token" id="sso_token" />
</form>

<script src="<?= BASE_ASSET; ?>js/sso/jquery.js"></script>
<script src="<?= BASE_ASSET; ?>js/sso/xcomponent.frame.min.js"></script>
<!-- sesuaikan http/https sesuai dg apps masing-masing -->
<script src="https://apps.unnes.ac.id/js/46/1"></script>