<?php

use GuzzleHttp\Client;

defined('BASEPATH') or exit('No direct script access allowed');

/**
 *| --------------------------------------------------------------------------
 *| Auth Controller
 *| --------------------------------------------------------------------------
 *| For authentication
 *|
 */
class Auth extends Admin
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('pegawai/model_pegawai');
		$this->load->model('ref_prodi/model_ref_prodi');
	}

	/**
	 * Login user
	 *
	 */
	public function login()
	{
		if ($this->aauth->is_loggedin()) {
			redirect('administrator/user/profile', 'refresh');
		}
		$data = [];
		$this->config->load('site');

		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');


		if ($this->form_validation->run()) {
			if ($this->aauth->login($this->input->post('username'), $this->input->post('password'), $this->input->post('remember'))) {
				$ref = $this->session->userdata('redirect');

				if ($ref) {
					redirect($ref, 'refresh');
				} else {
					redirect('/administrator/user/profile', 'refresh');
				}
			} else {
				$data['error'] = $this->aauth->print_errors(TRUE);
			}
		} else {
			$data['error'] = validation_errors();
		}
		$this->template->build('backend/standart/administrator/login', $data);
	}
	/**
	 * Login SSO
	 */
	public function post_sso()
	{
		if ($this->aauth->is_loggedin()) {
			redirect('administrator/user/profile', 'refresh');
		}
		$data['sso_token'] = $this->input->post('sso_token');

		// TODO - Ganti Dengan apps
		// URL API to check whether the token submitted is still valid
		$url = "http://apps.unnes.ac.id/auth";
		# $url = "http://localhost/unnes/unnes-app-gateway/public/auth";

		// username and password (this is real) for accessing API
		$username = "sso@mail.unnes.ac.id";
		$password = "unnes2018";

		// call API with CURL method
		$result = callApiWithURL($url, $data, $username, $password);

		// decode result
		$result = json_decode($result);
		//die(print_r($result));
		if ($result->success) {
			$data = (array) $result->data->data;
			//print_r($data);die();
			// login dengan session
			if ($data['status'] == 'mhs') {
				// id = 10 : user pancingan di tabel user dengan group yg sudah diset sebagai mahasiswa, 
				// agar bisa masuk sistem
				$login = [
					'id'        => '10',
					'status'    => 'mhs',
					'username'  => $data['usernm'],   //berisi nim
					'email'     => $data['email'],
					'loggedin'  => true,
					'non_user'  => true,
					'identitas' => $data['usernm'],
					'nama'      => $data['nama'],
					'kd_prodi'  => $data['prodi'],
				];
				$this->session->set_userdata($login);
				redirect('administrator/user/profile', 'refresh');
			} else if (($data['status'] == 'dosen') || ($data['status'] == 'dosens')) {
				if (!$this->aauth->user_exist_by_email($data['email'])) {
					// login dengan session, meminjam dengan meminjam akun dosen id 212
					
					$infopeg = $this->model_pegawai->get_info_pegawai($data['nip_baru']);
					
					$login = [
						'id'        => '212',
						'status'    => 'dosen',
						'username'  => $data['usernm'],     //berisi nip
						'email'     => $data['email'],
						'loggedin'  => true,
						'non_user'  => true,
						'identitas' => $data['usernm'],
						'nama'      => $infopeg ? $infopeg->nama_gelar : $data['nama'],
						'kd_prodi'  => $infopeg ? $infopeg->id_akademik : $data['prodi'],
						'nip_baru'  => $data['nip_baru'],
					];

					// get jabatan
					// $peg = callApiWithURL('https://simpeg.unnes.ac.id/index.php/api_simpeg/get_pegawai/' . $data['usernm']);
					// $peg = json_decode($peg);
					// $client   = new Client();
					// $response = $client->request('POST', 'https://simpeg.unnes.ac.id/index.php/api_simpeg/get_pegawai/' . $data['usernm'], ['verify' => false]);
					// $result   = $response->getBody();
					// $peg      = json_decode($result);

					// $kaprodi = ['37', '68', '104', '105', '69', '5'];
					// $wr      = ['54','12','10','11'];

					$kaprodi = $this->config->item('id_kaprodi');
					$pejabat = $this->config->item('id_pejabat');

					if ($infopeg) {
						switch (true) {
							case array_key_exists($infopeg->id_jabatan_struktural, $pejabat):
								$login['id'] = '296'; // pejabat
								break;
							case array_key_exists($infopeg->id_jabatan_struktural, $kaprodi):
								$login['id'] = '213';
								break;
							default:
								break;
						}
					}
					$this->session->set_userdata($login);
					redirect('administrator/user/profile', 'refresh');
				} else {
					//login dengan tabel user sebagai akun kerjasama
					$id = $this->aauth->get_user_id($data['email']);
					$user = $this->aauth->get_user($id);
					if ($this->aauth->login($data['email'], enkrip_dekrip($user->pass2, false), true)) {
						$ref = $this->session->userdata('redirect');

						if ($ref) {
							redirect($ref, 'refresh');
						} else {
							redirect('/administrator/user/profile', 'refresh');
						}
					} else {
						$this->data['view_email'] = true;
						$this->data['email'] = $data['email'];
						$this->data['pesan'] = $this->aauth->print_errors(TRUE);
						$this->template->build('backend/standart/administrator/message_sso', $this->data);
					}
				}
			} else if ($data['status'] == 'karyawan') {
				if (!$this->aauth->user_exist_by_email($data['email'])) {
					$peg   = $this->model_pegawai->get_info_pegawai($data['nip_baru']);
					$unit  = $this->load->model_ref_prodi->get_map_unit_prodi($peg->id_unit);
					$login = [
						'id'        => '215',
						'status'    => 'karyawan',
						'username'  => $data['usernm'],     //berisi nip
						'email'     => $data['email'],
						'loggedin'  => true,
						'non_user'  => true,
						'identitas' => $data['usernm'],
						'nama'      => $peg ? $peg->nama_gelar : $data['nama'],
						'kd_prodi'  => $unit ? $unit->kode : $data['prodi'],
						'nip_baru'  => $data['nip_baru'],
					];
					//$this->_redirect_emailnotfound($data['email']);
					$this->session->set_userdata($login);
					redirect('administrator/user/profile', 'refresh');
				} else
				//login dengan tabel user sebagai akun kerjasama
				{
					$id = $this->aauth->get_user_id($data['email']);
					$user = $this->aauth->get_user($id);
					if ($this->aauth->login($data['email'], enkrip_dekrip($user->pass2, false), true)) {
						$ref = $this->session->userdata('redirect');

						if ($ref) {
							redirect($ref, 'refresh');
						} else {
							redirect('/administrator/user/profile', 'refresh');
						}
					} else {
						$this->data['view_email'] = true;
						$this->data['email']      = $data['email'];
						$this->data['pesan']      = $this->aauth->print_errors(TRUE);
						$this->template->build('backend/standart/administrator/message_sso', $this->data);
					}
				}
			} else if ($data['status'] == 'pejabat') {
				if (!$this->aauth->user_exist_by_email($data['email'])) {
					$peg   = $this->model_pegawai->get_info_pegawai($data['nip_baru']);
					$unit  = $this->load->model_ref_prodi->get_map_unit_prodi($peg->id_unit_struktural);
					$login = [
						'id'        => '296',
						'status'    => 'pejabat',
						'username'  => $data['usernm'],     //berisi nip
						'email'     => $data['email'],
						'loggedin'  => true,
						'non_user'  => true,
						'identitas' => $data['usernm'],
						'nama'      => $peg ? $peg->nama_gelar : $data['nama'],
						'kd_prodi'  => $unit ? $unit->kode : $data['prodi'],
						'nip_baru'  => $data['nip_baru'],
					];
					//$this->_redirect_emailnotfound($data['email']);
					$this->session->set_userdata($login);
					redirect('administrator/user/profile', 'refresh');
				} else
				//login dengan tabel user sebagai akun kerjasama
				{
					$id = $this->aauth->get_user_id($data['email']);
					$user = $this->aauth->get_user($id);
					if ($this->aauth->login($data['email'], enkrip_dekrip($user->pass2, false), true)) {
						$ref = $this->session->userdata('redirect');

						if ($ref) {
							redirect($ref, 'refresh');
						} else {
							redirect('/administrator/user/profile', 'refresh');
						}
					} else {
						$this->data['view_email'] = true;
						$this->data['email'] = $data['email'];
						$this->data['pesan'] = $this->aauth->print_errors(TRUE);
						$this->template->build('backend/standart/administrator/message_sso', $this->data);
					}
				}
			}
		} else {
			$this->data['pesan'] = 'Token Invalid';
			$this->data['view_email'] = false;
			$this->template->build('backend/standart/administrator/message_sso', $this->data);
		}
	}
	/*
	private function _redirect_emailnotfound($email)
	{
		$this->data['view_email'] = true;
		$this->data['email'] = $email;
		$this->data['pesan'] = 'Email tidak terdaftar';
		$this->template->build('backend/standart/administrator/message_sso', $this->data);
		return;
	}
*/
	public function formsso()
	{
		if ($this->aauth->is_loggedin()) {
			redirect('administrator/dashboard', 'refresh');
		}
		$this->load->view('backend/standart/administrator/sso');
	}
	public function login_sso()
	{
		$this->template->build('backend/standart/administrator/scriptsso');
	}
	/**
	 * Register user member
	 *
	 */
	public function register()
	{
		$data = [];

		$this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[aauth_users.username]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[aauth_users.email]');
		$this->form_validation->set_rules('agree', 'Agree', 'trim|required');
		$this->form_validation->set_rules('captcha', 'Captcha', 'trim|required|callback_valid_captcha');

		$this->form_validation->set_message('is_unique', 'User already used');

		if ($this->form_validation->run()) {
			$save_data = [
				'full_name' => $this->input->post('full_name')
			];
			$save_user = $this->aauth->create_user($this->input->post('email'), $this->input->post('password'), $this->input->post('username'), $save_data);

			if ($save_user) {
				set_message('Your account sucessfully created');
				$this->aauth->add_member($save_user, 4);
				redirect('administrator/login', 'refresh');
			} else {
				$data['error'] = $this->aauth->print_errors();
			}
		} else {
			$data['error'] = validation_errors();
		}

		$this->template->build('backend/standart/administrator/register_member', $data);
	}

	/**
	 * User forgot password
	 *
	 * @var String $id 
	 */
	public function forgot_password()
	{
		$data = [];

		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('captcha', 'Captcha', 'trim|required|callback_valid_captcha');

		$this->form_validation->set_message('is_unique', 'User already used');

		if ($this->form_validation->run()) {
			//custom your action
			$reset = $this->aauth->remind_password($this->input->post('email'));
			if ($reset) {
				set_message('Your password reset link send to your mail');
			} else {
				set_message('Failed to send password reminder', 'danger');
			}
			redirect('administrator/login', 'refresh');
		} else {
			$data['error'] = validation_errors();
		}

		$this->template->build('backend/standart/administrator/forgot_password', $data);
	}

	/**
	 * User session logout
	 *
	 */
	public function logout()
	{
		$this->aauth->logout();
		redirect('/');
	}
	public function sso_logout()
	{
		$this->aauth->logout();
		$this->template->build('backend/standart/administrator/scriptsso');
	}
}

/* End of file Auth.php */
/* Location: ./application/controllers/administrator/Auth.php */
