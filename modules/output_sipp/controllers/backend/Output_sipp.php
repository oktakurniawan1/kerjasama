<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Output Sipp Controller
*| --------------------------------------------------------------------------
*| Output Sipp site
*|
*/
class Output_sipp extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_output_sipp');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Output Sipps
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('output_sipp_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if(!is_null($q)) $this->session->set_userdata('output_sipp_filter',$q);
		if(!is_null($f)) $this->session->set_userdata('output_sipp_field',$f);
		$this->session->set_userdata('_page',$offset);

		if(!is_null($this->input->get('reset_filter'))){
			$this->session->unset_userdata('output_sipp_filter');
			$this->session->unset_userdata('output_sipp_field');
		}
		$filter = $this->session->userdata('output_sipp_filter');
		$field = $this->session->userdata('output_sipp_field');
		$offset = $this->session->userdata('_page',$offset);

		$this->data['output_sipps'] = $this->model_output_sipp->get($filter, $field, $this->limit_page, $offset);
		$this->data['output_sipp_counts'] = $this->model_output_sipp->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/output_sipp/index/',
			'total_rows'   => $this->model_output_sipp->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Output Sipp List');
		$this->render('backend/standart/administrator/output_sipp/output_sipp_list', $this->data);
	}
	
	/**
	* Add new output_sipps
	*
	*/
	public function add()
	{
		$this->is_allowed('output_sipp_add');

		$this->template->title('Output Sipp New');
		$this->render('backend/standart/administrator/output_sipp/output_sipp_add', $this->data);
	}

	/**
	* Add New Output Sipps
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('output_sipp_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('output_name', 'Output Name', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'output_name' => $this->input->post('output_name'),
			];

			
			$save_output_sipp = $this->model_output_sipp->store($save_data);
            

			if ($save_output_sipp) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_output_sipp;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/output_sipp/edit/' . $save_output_sipp, 'Edit Output Sipp'),
						anchor('administrator/output_sipp', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/output_sipp/edit/' . $save_output_sipp, 'Edit Output Sipp')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/output_sipp');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/output_sipp');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Output Sipps
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('output_sipp_update');

		$this->data['output_sipp'] = $this->model_output_sipp->find($id);

		$this->template->title('Output Sipp Update');
		$this->render('backend/standart/administrator/output_sipp/output_sipp_update', $this->data);
	}

	/**
	* Update Output Sipps
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('output_sipp_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('output_name', 'Output Name', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'output_name' => $this->input->post('output_name'),
			];

			
			$save_output_sipp = $this->model_output_sipp->change($id, $save_data);

			if ($save_output_sipp) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/output_sipp', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/output_sipp/index/'.$this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/output_sipp/index/'.$this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Output Sipps
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('output_sipp_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'output_sipp'), 'success');
        } else {
            set_message(cclang('error_delete', 'output_sipp'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Output Sipps
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('output_sipp_view');

		$this->data['output_sipp'] = $this->model_output_sipp->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Output Sipp Detail');
		$this->render('backend/standart/administrator/output_sipp/output_sipp_view', $this->data);
	}
	
	/**
	* delete Output Sipps
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$output_sipp = $this->model_output_sipp->find($id);

		
		
		return $this->model_output_sipp->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('output_sipp_export');

		$this->model_output_sipp->export(
			'output_sipp', 
			'output_sipp',
			$this->model_output_sipp->field_search
		);
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('output_sipp_export');

		$this->model_output_sipp->pdf('output_sipp', 'output_sipp');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('output_sipp_export');

		$table = $title = 'output_sipp';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_output_sipp->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file output_sipp.php */
/* Location: ./application/controllers/administrator/Output Sipp.php */