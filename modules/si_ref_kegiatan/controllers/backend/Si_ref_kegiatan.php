<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Si Ref Kegiatan Controller
*| --------------------------------------------------------------------------
*| Si Ref Kegiatan site
*|
*/
class Si_ref_kegiatan extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_si_ref_kegiatan');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Si Ref Kegiatans
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('si_ref_kegiatan_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if(!is_null($q)) $this->session->set_userdata('si_ref_kegiatan_filter',$q);
		if(!is_null($f)) $this->session->set_userdata('si_ref_kegiatan_field',$f);
		$this->session->set_userdata('_page',$offset);

		if(!is_null($this->input->get('reset_filter'))){
			$this->session->unset_userdata('si_ref_kegiatan_filter');
			$this->session->unset_userdata('si_ref_kegiatan_field');
		}
		$filter = $this->session->userdata('si_ref_kegiatan_filter');
		$field = $this->session->userdata('si_ref_kegiatan_field');
		$offset = $this->session->userdata('_page',$offset);

		$this->data['si_ref_kegiatans'] = $this->model_si_ref_kegiatan->get($filter, $field, $this->limit_page, $offset);
		$this->data['si_ref_kegiatan_counts'] = $this->model_si_ref_kegiatan->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/si_ref_kegiatan/index/',
			'total_rows'   => $this->model_si_ref_kegiatan->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Tabel Jenis Kegiatan List');
		$this->render('backend/standart/administrator/si_ref_kegiatan/si_ref_kegiatan_list', $this->data);
	}
	
	/**
	* Add new si_ref_kegiatans
	*
	*/
	public function add()
	{
		$this->is_allowed('si_ref_kegiatan_add');

		$this->template->title('Tabel Jenis Kegiatan New');
		$this->render('backend/standart/administrator/si_ref_kegiatan/si_ref_kegiatan_add', $this->data);
	}

	/**
	* Add New Si Ref Kegiatans
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('si_ref_kegiatan_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('bound_name', 'Nama Kegiatan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('bound_to', 'DN/LN', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'bound_name' => $this->input->post('bound_name'),
				'bound_to' => $this->input->post('bound_to'),
				'deskripsi' => $this->input->post('deskripsi'),
			];

			
			$save_si_ref_kegiatan = $this->model_si_ref_kegiatan->store($save_data);
            

			if ($save_si_ref_kegiatan) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_si_ref_kegiatan;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/si_ref_kegiatan/edit/' . $save_si_ref_kegiatan, 'Edit Si Ref Kegiatan'),
						anchor('administrator/si_ref_kegiatan', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/si_ref_kegiatan/edit/' . $save_si_ref_kegiatan, 'Edit Si Ref Kegiatan')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/si_ref_kegiatan');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/si_ref_kegiatan');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Si Ref Kegiatans
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('si_ref_kegiatan_update');

		$this->data['si_ref_kegiatan'] = $this->model_si_ref_kegiatan->find($id);

		$this->template->title('Tabel Jenis Kegiatan Update');
		$this->render('backend/standart/administrator/si_ref_kegiatan/si_ref_kegiatan_update', $this->data);
	}

	/**
	* Update Si Ref Kegiatans
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('si_ref_kegiatan_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('bound_name', 'Nama Kegiatan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('bound_to', 'DN/LN', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'bound_name' => $this->input->post('bound_name'),
				'bound_to' => $this->input->post('bound_to'),
				'deskripsi' => $this->input->post('deskripsi'),
			];

			
			$save_si_ref_kegiatan = $this->model_si_ref_kegiatan->change($id, $save_data);

			if ($save_si_ref_kegiatan) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/si_ref_kegiatan', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/si_ref_kegiatan/index/'.$this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/si_ref_kegiatan/index/'.$this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Si Ref Kegiatans
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('si_ref_kegiatan_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'si_ref_kegiatan'), 'success');
        } else {
            set_message(cclang('error_delete', 'si_ref_kegiatan'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Si Ref Kegiatans
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('si_ref_kegiatan_view');

		$this->data['si_ref_kegiatan'] = $this->model_si_ref_kegiatan->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Tabel Jenis Kegiatan Detail');
		$this->render('backend/standart/administrator/si_ref_kegiatan/si_ref_kegiatan_view', $this->data);
	}
	
	/**
	* delete Si Ref Kegiatans
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$si_ref_kegiatan = $this->model_si_ref_kegiatan->find($id);

		
		
		return $this->model_si_ref_kegiatan->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('si_ref_kegiatan_export');

		$this->model_si_ref_kegiatan->export(
			'si_ref_kegiatan', 
			'si_ref_kegiatan',
			$this->model_si_ref_kegiatan->field_search
		);
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('si_ref_kegiatan_export');

		$this->model_si_ref_kegiatan->pdf('si_ref_kegiatan', 'si_ref_kegiatan');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('si_ref_kegiatan_export');

		$table = $title = 'si_ref_kegiatan';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_si_ref_kegiatan->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file si_ref_kegiatan.php */
/* Location: ./application/controllers/administrator/Si Ref Kegiatan.php */