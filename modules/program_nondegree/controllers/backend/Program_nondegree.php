<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Program Nondegree Controller
*| --------------------------------------------------------------------------
*| Program Nondegree site
*|
*/
class Program_nondegree extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_program_nondegree');
		$this->lang->load('web_lang', $this->current_lang);
	}

		/**
	* show all Program Nondegrees
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('program_nondegree_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if(!is_null($q)) $this->session->set_userdata('program_nondegree_filter',$q);
		if(!is_null($f)) $this->session->set_userdata('program_nondegree_field',$f);
		$this->session->set_userdata('_page',$offset);

		if(!is_null($this->input->get('reset_filter'))){
			$this->session->unset_userdata('program_nondegree_filter');
			$this->session->unset_userdata('program_nondegree_field');
		}
		$filter = $this->session->userdata('program_nondegree_filter');
		$field = $this->session->userdata('program_nondegree_field');
		$offset = $this->session->userdata('_page',$offset);

		$this->data['program_nondegrees'] = $this->model_program_nondegree->get($filter, $field, $this->limit_page, $offset);
		$this->data['program_nondegree_counts'] = $this->model_program_nondegree->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/program_nondegree/index/',
			'total_rows'   => $this->model_program_nondegree->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Program Nondegree List');
		$this->render('backend/standart/administrator/program_nondegree/program_nondegree_list', $this->data);
	}
	
	/**
	* Add new program_nondegrees
	*
	*/
	public function add()
	{
		$this->is_allowed('program_nondegree_add');

		$this->template->title('Program Nondegree New');
		$this->render('backend/standart/administrator/program_nondegree/program_nondegree_add', $this->data);
	}

	/**
	* Add New Program Nondegrees
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('program_nondegree_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('nama_program', 'Nama Program', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'nama_program' => $this->input->post('nama_program'),
			];

			
			$save_program_nondegree = $this->model_program_nondegree->store($save_data);
            

			if ($save_program_nondegree) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_program_nondegree;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/program_nondegree/edit/' . $save_program_nondegree, 'Edit Program Nondegree'),
						anchor('administrator/program_nondegree', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/program_nondegree/edit/' . $save_program_nondegree, 'Edit Program Nondegree')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/program_nondegree');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/program_nondegree');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Program Nondegrees
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('program_nondegree_update');

		$this->data['program_nondegree'] = $this->model_program_nondegree->find($id);

		$this->template->title('Program Nondegree Update');
		$this->render('backend/standart/administrator/program_nondegree/program_nondegree_update', $this->data);
	}

	/**
	* Update Program Nondegrees
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('program_nondegree_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('nama_program', 'Nama Program', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'nama_program' => $this->input->post('nama_program'),
			];

			
			$save_program_nondegree = $this->model_program_nondegree->change($id, $save_data);

			if ($save_program_nondegree) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/program_nondegree', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/program_nondegree/index/'.$this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/program_nondegree/index/'.$this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Program Nondegrees
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('program_nondegree_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'program_nondegree'), 'success');
        } else {
            set_message(cclang('error_delete', 'program_nondegree'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Program Nondegrees
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('program_nondegree_view');

		$this->data['program_nondegree'] = $this->model_program_nondegree->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Program Nondegree Detail');
		$this->render('backend/standart/administrator/program_nondegree/program_nondegree_view', $this->data);
	}
	
	/**
	* delete Program Nondegrees
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$program_nondegree = $this->model_program_nondegree->find($id);

		
		
		return $this->model_program_nondegree->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
				'nama_program',
				];
		$data = $this->model_program_nondegree->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_program_nondegree->export2($data, 'file', $selected_field);

		/*
		$this->is_allowed('program_nondegree_export');

		$this->model_program_nondegree->export(
			'program_nondegree', 
			'program_nondegree',
			$this->model_program_nondegree->field_search
		);
		*/
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('program_nondegree_export');

		$this->model_program_nondegree->pdf('program_nondegree', 'program_nondegree');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('program_nondegree_export');

		$table = $title = 'program_nondegree';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_program_nondegree->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file program_nondegree.php */
/* Location: ./application/controllers/administrator/Program Nondegree.php */