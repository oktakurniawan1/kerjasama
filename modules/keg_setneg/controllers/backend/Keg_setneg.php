<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Keg Setneg Controller
*| --------------------------------------------------------------------------
*| Keg Setneg site
*|
*/
class Keg_setneg extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_keg_setneg');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Keg Setnegs
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('keg_setneg_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if(!is_null($q)) $this->session->set_userdata('keg_setneg_filter',$q);
		if(!is_null($f)) $this->session->set_userdata('keg_setneg_field',$f);
		$this->session->set_userdata('_page',$offset);

		if(!is_null($this->input->get('reset_filter'))){
			$this->session->unset_userdata('keg_setneg_filter');
			$this->session->unset_userdata('keg_setneg_field');
		}
		$filter = $this->session->userdata('keg_setneg_filter');
		$field = $this->session->userdata('keg_setneg_field');
		$offset = $this->session->userdata('_page',$offset);

		$this->data['keg_setnegs'] = $this->model_keg_setneg->get($filter, $field, $this->limit_page, $offset);
		$this->data['keg_setneg_counts'] = $this->model_keg_setneg->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/keg_setneg/index/',
			'total_rows'   => $this->model_keg_setneg->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Keg Setneg List');
		$this->render('backend/standart/administrator/keg_setneg/keg_setneg_list', $this->data);
	}
	
	/**
	* Add new keg_setnegs
	*
	*/
	public function add()
	{
		$this->is_allowed('keg_setneg_add');

		$this->template->title('Keg Setneg New');
		$this->render('backend/standart/administrator/keg_setneg/keg_setneg_add', $this->data);
	}

	/**
	* Add New Keg Setnegs
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('keg_setneg_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('keg_setneg', 'Kegiatan Setneg', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'keg_setneg' => $this->input->post('keg_setneg'),
				'deskripsi' => $this->input->post('deskripsi'),
			];

			
			$save_keg_setneg = $this->model_keg_setneg->store($save_data);
            

			if ($save_keg_setneg) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_keg_setneg;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/keg_setneg/edit/' . $save_keg_setneg, 'Edit Keg Setneg'),
						anchor('administrator/keg_setneg', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/keg_setneg/edit/' . $save_keg_setneg, 'Edit Keg Setneg')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/keg_setneg');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/keg_setneg');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Keg Setnegs
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('keg_setneg_update');

		$this->data['keg_setneg'] = $this->model_keg_setneg->find($id);

		$this->template->title('Keg Setneg Update');
		$this->render('backend/standart/administrator/keg_setneg/keg_setneg_update', $this->data);
	}

	/**
	* Update Keg Setnegs
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('keg_setneg_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('keg_setneg', 'Kegiatan Setneg', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'keg_setneg' => $this->input->post('keg_setneg'),
				'deskripsi' => $this->input->post('deskripsi'),
			];

			
			$save_keg_setneg = $this->model_keg_setneg->change($id, $save_data);

			if ($save_keg_setneg) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/keg_setneg', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/keg_setneg/index/'.$this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/keg_setneg/index/'.$this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Keg Setnegs
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('keg_setneg_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'keg_setneg'), 'success');
        } else {
            set_message(cclang('error_delete', 'keg_setneg'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Keg Setnegs
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('keg_setneg_view');

		$this->data['keg_setneg'] = $this->model_keg_setneg->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Keg Setneg Detail');
		$this->render('backend/standart/administrator/keg_setneg/keg_setneg_view', $this->data);
	}
	
	/**
	* delete Keg Setnegs
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$keg_setneg = $this->model_keg_setneg->find($id);

		
		
		return $this->model_keg_setneg->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('keg_setneg_export');

		$this->model_keg_setneg->export(
			'keg_setneg', 
			'keg_setneg',
			$this->model_keg_setneg->field_search
		);
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('keg_setneg_export');

		$this->model_keg_setneg->pdf('keg_setneg', 'keg_setneg');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('keg_setneg_export');

		$table = $title = 'keg_setneg';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_keg_setneg->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file keg_setneg.php */
/* Location: ./application/controllers/administrator/Keg Setneg.php */