<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Dosin Tugas Controller
*| --------------------------------------------------------------------------
*| Dosin Tugas site
*|
*/
class Dosin_tugas extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_dosin_tugas');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Dosin Tugass
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('dosin_tugas_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if(!is_null($q)) $this->session->set_userdata('dosin_tugas_filter',$q);
		if(!is_null($f)) $this->session->set_userdata('dosin_tugas_field',$f);
		$this->session->set_userdata('_page',$offset);

		if(!is_null($this->input->get('reset_filter'))){
			$this->session->unset_userdata('dosin_tugas_filter');
			$this->session->unset_userdata('dosin_tugas_field');
		}
		$filter = $this->session->userdata('dosin_tugas_filter');
		$field = $this->session->userdata('dosin_tugas_field');
		$offset = $this->session->userdata('_page',$offset);

		$this->data['dosin_tugass'] = $this->model_dosin_tugas->get($filter, $field, $this->limit_page, $offset);
		$this->data['dosin_tugas_counts'] = $this->model_dosin_tugas->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/dosin_tugas/index/',
			'total_rows'   => $this->model_dosin_tugas->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Dosin Tugas List');
		$this->render('backend/standart/administrator/dosin_tugas/dosin_tugas_list', $this->data);
	}
	
	/**
	* Add new dosin_tugass
	*
	*/
	public function add()
	{
		$this->is_allowed('dosin_tugas_add');

		$this->template->title('Dosin Tugas New');
		$this->render('backend/standart/administrator/dosin_tugas/dosin_tugas_add', $this->data);
	}

	/**
	* Add New Dosin Tugass
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('dosin_tugas_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('tugas', 'Nama Tugas', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('aktivitas', 'Tipe Aktivitas', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'tugas' => $this->input->post('tugas'),
				'aktivitas' => $this->input->post('aktivitas'),
			];

			
			$save_dosin_tugas = $this->model_dosin_tugas->store($save_data);
            

			if ($save_dosin_tugas) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_dosin_tugas;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/dosin_tugas/edit/' . $save_dosin_tugas, 'Edit Dosin Tugas'),
						anchor('administrator/dosin_tugas', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/dosin_tugas/edit/' . $save_dosin_tugas, 'Edit Dosin Tugas')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dosin_tugas');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dosin_tugas');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Dosin Tugass
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('dosin_tugas_update');

		$this->data['dosin_tugas'] = $this->model_dosin_tugas->find($id);

		$this->template->title('Dosin Tugas Update');
		$this->render('backend/standart/administrator/dosin_tugas/dosin_tugas_update', $this->data);
	}

	/**
	* Update Dosin Tugass
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('dosin_tugas_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('tugas', 'Nama Tugas', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('aktivitas', 'Tipe Aktivitas', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'tugas' => $this->input->post('tugas'),
				'aktivitas' => $this->input->post('aktivitas'),
			];

			
			$save_dosin_tugas = $this->model_dosin_tugas->change($id, $save_data);

			if ($save_dosin_tugas) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/dosin_tugas', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dosin_tugas/index/'.$this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dosin_tugas/index/'.$this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Dosin Tugass
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('dosin_tugas_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'dosin_tugas'), 'success');
        } else {
            set_message(cclang('error_delete', 'dosin_tugas'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Dosin Tugass
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('dosin_tugas_view');

		$this->data['dosin_tugas'] = $this->model_dosin_tugas->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Dosin Tugas Detail');
		$this->render('backend/standart/administrator/dosin_tugas/dosin_tugas_view', $this->data);
	}
	
	/**
	* delete Dosin Tugass
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$dosin_tugas = $this->model_dosin_tugas->find($id);

		
		
		return $this->model_dosin_tugas->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('dosin_tugas_export');

		$this->model_dosin_tugas->export(
			'dosin_tugas', 
			'dosin_tugas',
			$this->model_dosin_tugas->field_search
		);
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('dosin_tugas_export');

		$this->model_dosin_tugas->pdf('dosin_tugas', 'dosin_tugas');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('dosin_tugas_export');

		$table = $title = 'dosin_tugas';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_dosin_tugas->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file dosin_tugas.php */
/* Location: ./application/controllers/administrator/Dosin Tugas.php */