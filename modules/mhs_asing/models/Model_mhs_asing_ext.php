<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_mhs_asing_ext extends MY_Model
{
	private $primary_key    = 'id';
	private $table_name     = 'mhs_asing_ext';
	public $field_search   = [];
	public function __construct()
	{
		$config = array(
			'primary_key'   => $this->primary_key,
			'table_name'    => $this->table_name,
			'field_search'  => $this->field_search,
		);

		parent::__construct($config);
	}
}