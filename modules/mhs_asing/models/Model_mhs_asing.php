<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_mhs_asing extends MY_Model
{

	private $primary_key  = 'id';
	private $table_name   = 'mhs_asing';
	public  $field_search = ['nim', 'tgl_pasport','tgl_ijin_belajar', 'tgl_kitas', 'negara', 'tlp','lls'];

	public function __construct()
	{
		$config = array(
			'primary_key'   => $this->primary_key,
			'table_name'    => $this->table_name,
			'field_search'  => $this->field_search,
		);

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "mhs_asing." . $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "mhs_asing." . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			$where .= "(" . "mhs_asing." . $field . " LIKE '%" . $q . "%' )";
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);

		/**form filter data */
		if ($this->session->has_userdata('filterdata_mhs_asing')) {
			foreach (fromsess('filterdata_mhs_asing') as $key => $row) {
				if ($row != 'all') $this->db->having($key, $row);
			}
		}
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [], $result = TRUE)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "mhs_asing." . $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "mhs_asing." . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			$where .= "(" . "mhs_asing." . $field . " LIKE '%" . $q . "%' )";
		}

		if (is_array($select_field) and count($select_field)) {
			$this->db->select($select_field);
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);

		/**form filter data */
		if ($this->session->has_userdata('filterdata_mhs_asing')) {
			foreach (fromsess('filterdata_mhs_asing') as $key => $row) {
				if ($row != 'all') $this->db->having($key, $row);
			}
		}
		$this->db->limit($limit, $offset);

		$this->sortable();

		$query = $this->db->get($this->table_name);

		if ($result)
			return $query->result();
		else return $query;
	}

	public function join_avaiable()
	{

		$this->db->select('mhs_asing.*');
		$this->db->select('mhs.nama,ref_prodi.nama_prodi,mhs.ta,mhs.program_studi');
		$this->db->join('mhs', 'mhs.nim = mhs_asing.nim', 'LEFT');
		$this->db->join('ref_prodi', 'ref_prodi.kode = mhs.program_studi', 'LEFT');
		return $this;
	}

	public function filter_avaiable()
	{

		// if (!$this->aauth->is_admin()) {
		// }
		return $this;
	}
	public function gethistoryfiles($table,$id){
		$this->load->library('Datatables', null, 'datatable');
		$this->datatable->set_database('default');
		$this->datatable->select('*')->where(['mhs_asing_id' => $id])->from($table);
		return $this->datatable->generate();
	}
	public function add_to_history($table,$mhs_asing_id){
		$row = db_get_all_data('mhs_asing',['id'=>$mhs_asing_id],'row_array');
		$kolom = [
			'mhs_asing_kitas' => ['file_kitas','tgl_kitas'],
			'mhs_asing_pasports' => ['file_pasport','tgl_pasport'],
			'mhs_asing_permits' => ['file_ijin_belajar','tgl_ijin_belajar'],
		];
		$data['mhs_asing_id'] = $row['id'];
		$data['file'] = $row[$kolom[$table][0]];
		$data['tgl_file'] = $row[$kolom[$table][1]];
		$this->db->insert($table,$data);
	}
	public function hapus_history_file($table,$id){
		$this->db->from($table)->where('id',$id)->delete();
	}
	public function update_history_file($post){
		$this->db->where('id',$post['id'])->update($post['table'],['deskripsi'=>$post['deskripsi']]);
	}
}

/* End of file Model_mhs_asing.php */
/* Location: ./application/models/Model_mhs_asing.php */