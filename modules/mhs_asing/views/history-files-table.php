<table class="table table-hover table-striped table-bordered" id='<?=$table?>'>
	<thead>
		<tr>
			<th>id</th>
			<th>id_mhs</th>
			<th>Files</th>
			<th>Expired Date</th>
			<th>Desc</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
		</tr>
	</tbody>
</table>
<script>
	$(function(){
		oTable[<?=$otb?>] = $('#<?=$table?>').DataTable({
				processing: true,
				serverSide: true,
				searching:false,
				pageLength: 5,
				order: [[3, 'desc']],
				lengthChange: false,
				//lengthMenu: [6, 10, 15, 25],
				ajax: {
					'url': '<?php echo site_url('administrator/mhs_asing/browse_history_files') ?>',
					'type': 'post',
					'data': {
						table: '<?=$table ?>',
						mhs_asing_id: '<?=$mhs_asing_id ?>',
						<?= csrf_token() ?>
						
					},
				},
				columns: [
					{data: 'id',visible: false,searchable: false,},
					{data: 'mhs_asing_id',visible:false,searchable:false},
					{data: 'file',render:function(data,row,type){
						return "<a target='_blank' href='<?=BASE_URL?>file/"+data+"'>"+data+"</a>"
					}},
					{data: 'tgl_file'},
					{data: 'deskripsi'},
					// {data: 'tgl_upload'},
					{className: 'text-center',searchable: false,orderable: false,width: '30px'}
				],
				columnDefs: [{
					'targets': -1,
					'data': null,
					render: function(data, type, row) {
						var otb = '<?=$otb?>';
						return '<button type="button" class="btn btn-default btn-xs" onclick="hapus_history_file('+otb+',\''+'<?=$table?>'+'\','+row.id+')"><i class="fa fa-trash text-danger"></i></button> ' +
							'<button type="button" class="btn btn-default btn-xs" onclick="update_history_file('+otb+',\''+'<?=$table?>'+'\','+row.id+')"><i class="fa fa-edit text-primary"></i></button>';
					}
				}]
			})
	})
</script>