<form action="" method="POST" role="form">
	<div class="row">
		<div class="col-md-8">
			<div class="form-group">
				<label for="">Program Studi</label>
				<?= form_dropdown('program_studi', $program_studi, fromsess('filterdata_mhs_asing')['program_studi'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label for="">Negara</label>
				<?= form_dropdown('negara', $negara, fromsess('filterdata_mhs_asing')['negara'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
			</div>
		</div>
	</div>
</form>

<script>
	$(function() {
		$('select[name=negara],select[name=program_studi]').select2({
			theme: 'classic',
			dropdownParent: $('#filtermhs_asing-dialog'),
		});
	})
</script>