<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Mahasiswa Internasional <small>Edit Mahasiswa Internasional</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/mhs_asing'); ?>">Mahasiswa Internasional</a></li>
		<li class="active">Edit</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Mahasiswa Internasional</h3>
							<h5 class="widget-user-desc">Edit Mahasiswa Internasional</h5>
							<hr>
						</div>
						<?= form_open(base_url('administrator/mhs_asing/edit_save/' . $this->uri->segment(4)), [
							'name'    => 'form_mhs_asing',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_mhs_asing',
							'method'  => 'POST'
						]); ?>

						<div class="form-group ">
							<label for="nim" class="col-sm-2 control-label">Student ID
							</label>
							<div class="col-sm-8">
								<?php if (is_member('Mahasiswa')) : ?>
									<input type="text" class="form-control" name="nim" id="nim" placeholder="Student ID" value="<?= $this->session->userdata('identitas'); ?>" readonly>
								<?php else : ?>
									<input type="text" class="form-control" name="nim" id="nim" placeholder="Student ID" value="<?= set_value('nim', $mhs_asing->nim); ?>" readonly>
									<div class='row' style="margin-top:10px">
										<div class="col-sm-2" style="padding-right: 5px;">
											<?= form_dropdown('ta', array_combine(array_reverse(range(2000, date('Y'))), array_reverse(range(2000, date('Y')))), date('Y'), ['class' => 'form-control input-sm']) ?>
										</div>
										<div class="col-sm-4" style="padding-left: 0px;">
											<button class="btn btn-primary btn-sm" type="button" onclick="cari_mahasiswa()">Search student</button>
										</div>
									</div>
								<?php endif; ?>
								<small class="info help-block">
									<b>Input Nim</b> Max Length : 10, <b>klik tombol cari mahasiswa untuk bantuan cari data mahasiswa dengan filter angkatan.</b></small>
							</div>
						</div>

						<?php if (!empty($ket_mhs)) : ?>
							<div class="form-group ">
								<label for="nama" class="col-sm-2 control-label">Name
								</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id='nama' disabled value="<?= $ket_mhs[0]['nama']; ?>">
								</div>
							</div>

							<div class="form-group ">
								<label for="nama" class="col-sm-2 control-label">Day of Birth
								</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="tlhr" disabled value="<?= $ket_mhs[0]['tlhr']; ?>">
								</div>
							</div>

							<div class="form-group ">
								<label for="nama" class="col-sm-2 control-label">Semester
								</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="smt" disabled value="<?= $ket_mhs[0]['smstr_int'] . ' | Last Registration : ' . $ket_mhs[0]['last_smt']; ?>">
								</div>
							</div>

							<div class="form-group ">
								<label for="nama_prodi" class="col-sm-2 control-label">Study Program
								</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="nama_prodi" disabled value="<?= $ket_mhs[0]['prodi_concat']; ?>">
								</div>
							</div>

							<div class="form-group ">
								<label for="ta" class="col-sm-2 control-label">Class Year
								</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="ta" disabled value="<?= $ket_mhs[0]['ta']; ?>">
								</div>
							</div>

							<div class="form-group ">
								<label for="ta" class="col-sm-2 control-label">Supervisor
								</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="doswal" disabled value="<?= $ket_mhs[0]['nama_doswal']; ?>">
								</div>
							</div>

							<div class="form-group ">
								<label for="ta" class="col-sm-2 control-label">Number Of Course
								</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="skstotal" disabled value="<?= $ket_mhs[0]['sks_total'] . " ( Until Periode {$ket_mhs[0]['smstr_int']} )"; ?>">
								</div>
							</div>

							<div class="form-group ">
								<label for="ta" class="col-sm-2 control-label">GPA
								</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="ipk" disabled value="<?= $ket_mhs[0]['ipk']; ?>">
								</div>
							</div>

							<div class="form-group ">
								<label for="ta" class="col-sm-2 control-label">Status
								</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="status_mhs" disabled value="<?= $ket_mhs[0]['status_mhs'] . ' | ' . 'Current Registration Status : ' . $ket_mhs[0]['nama_status_smt']; ?>">
								</div>
							</div>

						<?php endif; ?>

						<div class="form-group ">
							<label for="program" class="col-sm-2 control-label">Program
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<?= form_dropdown('program', ['d' => 'Degree', 'nd' => 'Non Degree', '' => ''], $mhs_asing->program, ['class' => 'form-control chosen chosen-select', 'id' => 'program']) ?>
							</div>
						</div>

						<div class="non-degree-form">
							<div class="form-group">
								<label for="program_type" class="col-sm-2 control-label">Program Type
									<i class="required">*</i>
								</label>
								<div class="col-sm-8">
									<?= form_dropdown('program_type', array_column(db_get_all_data('program_nondegree'), 'nama_program', 'id'), $mhs_asing->program_type, ['class' => 'form-control chosen chosen-select']) ?>
									<small class="info help-block">
										<b>Select program type</b>
									</small>
								</div>
							</div>

							<div class="form-group">
								<label for="nd_start_date" class="col-sm-2 control-label">Start Date
									<i class="required">*</i>
								</label>
								<div class="col-sm-8">
									<input type="text" class="form-control datepicker" id="nd_start_date" name="nd_start_date" placeholder="Start Date" value="<?= $mhs_asing->nd_start_date ?>">
									<small class="info help-block">
										<b>Start date format must</b> Valid Date, <b>Input start date</b> valid date format : yyyy-mm-dd.
									</small>
								</div>
							</div>

							<div class="form-group">
								<label for="nd_finish_date" class="col-sm-2 control-label">Finish Date
									<i class="required">*</i>
								</label>
								<div class="col-sm-8">
									<input type="text" class="form-control datepicker" id="nd_finish_date" name="nd_finish_date" placeholder="Finish Date" value="<?= $mhs_asing->nd_finish_date ?>">
									<small class="info help-block">
										<b>Finish date format must</b> Valid Date, <b>Input finish date</b> valid date format : yyyy-mm-dd.
									</small>
								</div>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_ijin_belajar" class="col-sm-2 control-label">Study Permit
								<i class="required">*</i>
							</label>
							<div class="col-sm-3">
								<div id="mhs_asing_file_ijin_belajar_galery"></div>
								<input class="data_file data_file_uuid" name="mhs_asing_file_ijin_belajar_uuid" id="mhs_asing_file_ijin_belajar_uuid" type="hidden" value="<?= set_value('mhs_asing_file_ijin_belajar_uuid'); ?>">
								<input class="data_file" name="mhs_asing_file_ijin_belajar_name" id="mhs_asing_file_ijin_belajar_name" type="hidden" value="<?= set_value('mhs_asing_file_ijin_belajar_name', $mhs_asing->file_ijin_belajar); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
							<div class="col-md-1" style="margin-right: 15px;">
								<button type="button" onclick="add_to_history(0,'mhs_asing_permits','<?= $mhs_asing->id ?>')" class="btn btn-primary">Add to Archive<span class='glyphicon glyphicon-chevron-right'></span></button>
							</div>
							<div class="col-md-4">
								<div id='hist-permits'></div>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_ijin_belajar" class="col-sm-2 control-label">Study Permit Date
								<i class="required">*</i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-4">
									<input type="text" class="form-control pull-right datepicker" name="tgl_ijin_belajar" placeholder="Study Permit Date" id="tgl_ijin_belajar" value="<?= set_value('mhs_asing_tgl_ijin_belajar_name', $mhs_asing->tgl_ijin_belajar); ?>">
								</div>
								<small class="info help-block">
									<b>Study permit date format must</b> Valid Date.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_pasport" class="col-sm-2 control-label">Pasport ID
								<i class="required">*</i>
							</label>
							<div class="col-sm-3">
								<div id="mhs_asing_file_pasport_galery"></div>
								<input class="data_file data_file_uuid" name="mhs_asing_file_pasport_uuid" id="mhs_asing_file_pasport_uuid" type="hidden" value="<?= set_value('mhs_asing_file_pasport_uuid'); ?>">
								<input class="data_file" name="mhs_asing_file_pasport_name" id="mhs_asing_file_pasport_name" type="hidden" value="<?= set_value('mhs_asing_file_pasport_name', $mhs_asing->file_pasport); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
							<div class="col-md-1" style="margin-right: 15px;">
								<button type="button" onclick="add_to_history(1,'mhs_asing_pasports','<?= $mhs_asing->id ?>')" class="btn btn-primary">Add to Archive<span class='glyphicon glyphicon-chevron-right'></span></button>
							</div>
							<div class="col-md-4">
								<div id='hist-pasports'></div>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_pasport" class="col-sm-2 control-label">Pasport Date
								<i class="required">*</i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-4">
									<input type="text" class="form-control pull-right datepicker" name="tgl_pasport" placeholder="Pasport Date" id="tgl_pasport" value="<?= set_value('mhs_asing_tgl_pasport_name', $mhs_asing->tgl_pasport); ?>">
								</div>
								<small class="info help-block">
									<b>Date pasport format must</b> Valid Date.</small>
							</div>
						</div>


						<div class="form-group ">
							<label for="file_kitas" class="col-sm-2 control-label">KITAS File
								<i class="required">*</i>
							</label>
							<div class="col-sm-3">
								<div id="mhs_asing_file_kitas_galery"></div>
								<input class="data_file data_file_uuid" name="mhs_asing_file_kitas_uuid" id="mhs_asing_file_kitas_uuid" type="hidden" value="<?= set_value('mhs_asing_file_kitas_uuid'); ?>">
								<input class="data_file" name="mhs_asing_file_kitas_name" id="mhs_asing_file_kitas_name" type="hidden" value="<?= set_value('mhs_asing_file_kitas_name', $mhs_asing->file_kitas); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
							<div class="col-md-1" style="margin-right: 15px;">
								<button type="button" onclick="add_to_history(2,'mhs_asing_kitas','<?= $mhs_asing->id ?>')" class="btn btn-primary">Add to Archive<span class='glyphicon glyphicon-chevron-right'></span></button>
							</div>
							<div class="col-md-4">
								<div id='hist-kitas'></div>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_kitas" class="col-sm-2 control-label">KITAS Date
								<i class="required">*</i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-4">
									<input type="text" class="form-control pull-right datepicker" name="tgl_kitas" placeholder="Kitas Date" id="tgl_kitas" value="<?= set_value('mhs_asing_tgl_kitas_name', $mhs_asing->tgl_kitas); ?>">
								</div>
								<small class="info help-block">
									<b>Date kitas format must</b> Valid Date, <b>Input Tgl Kitas</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="negara" class="col-sm-2 control-label">Country
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="negara" id="negara" data-placeholder="Select Country">
									<option value=""></option>
									<?php foreach (db_get_all_data('negara') as $row) : ?>
										<option <?= $row->nama_negara ==  $mhs_asing->negara ? 'selected' : ''; ?> value="<?= $row->nama_negara ?>"><?= $row->nama_negara; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Select Country</b></small>
							</div>
						</div>

						<div class="form-group ">
							<label for="alamat_lokal" class="col-sm-2 control-label">Residence At Indonesia
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="alamat_lokal" id="alamat_lokal" placeholder="Residence At Indonesia" value="<?= set_value('alamat_lokal', $mhs_asing->alamat_lokal); ?>">
								<small class="info help-block">
									<b>Input residence at Indonesia</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tlp" class="col-sm-2 control-label">Telephone
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="tlp" id="tlp" placeholder="Telephone" value="<?= set_value('tlp', $mhs_asing->tlp); ?>">
								<small class="info help-block">
									<b>Input Telephone</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tlp_darurat" class="col-sm-2 control-label">Emergency Contact
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="tlp_darurat" id="tlp_darurat" placeholder="Emergency Contact" value="<?= set_value('tlp_darurat', $mhs_asing->tlp_darurat); ?>">
								<small class="info help-block">
									<b>Input emergency contact</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="email" class="col-sm-2 control-label">Email
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?= set_value('email', $mhs_asing->email); ?>">
								<small class="info help-block">
									<b>Email format must</b> Valid Email, <b>Input Email</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	var oTable = {};
	$(document).ready(function() {
		$.when(
			$.get('<?= site_url('administrator/mhs_asing/history_files') ?>', {
				otb: 0,
				table: 'mhs_asing_permits',
				mhs_asing_id: '<?= $mhs_asing->id ?>'
			}, function(table) {
				$('#hist-permits').html(table);
			}),
			$.get('<?= site_url('administrator/mhs_asing/history_files') ?>', {
				otb: 1,
				table: 'mhs_asing_pasports',
				mhs_asing_id: '<?= $mhs_asing->id ?>'
			}, function(table) {
				$('#hist-pasports').html(table);
			}),
			$.get('<?= site_url('administrator/mhs_asing/history_files') ?>', {
				otb: 2,
				table: 'mhs_asing_kitas',
				mhs_asing_id: '<?= $mhs_asing->id ?>'
			}, function(table) {
				$('#hist-kitas').html(table);
			})
		)
		$('#btn_cancel').click(function() {
			swal({
					title: "Are you sure?",
					text: "the data that you have created will be in the exhaust!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/mhs_asing/index/' + "<?= $this->session->userdata('_page') ?>";
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();

			var form_mhs_asing = $('#form_mhs_asing');
			var data_post = form_mhs_asing.serializeArray();
			var save_type = $(this).attr('data-stype');
			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: form_mhs_asing.attr('action'),
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('form').find('.error-input').remove();
					$('.steps li').removeClass('error');
					if (res.success) {
						var id = $('#mhs_asing_image_galery').find('li').attr('qq-file-id');
						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						$('.data_file_uuid').val('');

					} else {
						if (res.errors) {
							parseErrorField(res.errors);
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/

		var params = {};
		params[csrf] = token;

		$('#mhs_asing_file_ijin_belajar_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_asing/upload_file_ijin_belajar_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/mhs_asing/delete_file_ijin_belajar_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/mhs_asing/get_file_ijin_belajar_file/<?= $mhs_asing->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_asing_file_ijin_belajar_galery').fineUploader('getUuid', id);
						$('#mhs_asing_file_ijin_belajar_uuid').val(uuid);
						$('#mhs_asing_file_ijin_belajar_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_asing_file_ijin_belajar_uuid').val();
					$.get(BASE_URL + '/administrator/mhs_asing/delete_file_ijin_belajar_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_asing_file_ijin_belajar_uuid').val('');
						$('#mhs_asing_file_ijin_belajar_name').val('');
					}
				}
			}
		}); /*end file_ijin_belajar galey*/
		var params = {};
		params[csrf] = token;

		$('#mhs_asing_file_pasport_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_asing/upload_file_pasport_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/mhs_asing/delete_file_pasport_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/mhs_asing/get_file_pasport_file/<?= $mhs_asing->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_asing_file_pasport_galery').fineUploader('getUuid', id);
						$('#mhs_asing_file_pasport_uuid').val(uuid);
						$('#mhs_asing_file_pasport_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_asing_file_pasport_uuid').val();
					$.get(BASE_URL + '/administrator/mhs_asing/delete_file_pasport_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_asing_file_pasport_uuid').val('');
						$('#mhs_asing_file_pasport_name').val('');
					}
				}
			}
		}); /*end file_pasport galey*/
		var params = {};
		params[csrf] = token;

		$('#mhs_asing_file_kitas_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_asing/upload_file_kitas_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/mhs_asing/delete_file_kitas_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/mhs_asing/get_file_kitas_file/<?= $mhs_asing->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_asing_file_kitas_galery').fineUploader('getUuid', id);
						$('#mhs_asing_file_kitas_uuid').val(uuid);
						$('#mhs_asing_file_kitas_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_asing_file_kitas_uuid').val();
					$.get(BASE_URL + '/administrator/mhs_asing/delete_file_kitas_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_asing_file_kitas_uuid').val('');
						$('#mhs_asing_file_kitas_name').val('');
					}
				}
			}
		}); /*end file_kitas galey*/
		async function chain() {}

		chain();

		$('#program').change(function() {
			$.LoadingOverlay('show');
			if ($(this).val() == 'nd') {
				$('.non-degree-form').show();
			} else {
				$('.non-degree-form').hide();
			}
			$.LoadingOverlay('hide');
		}).trigger('choosen:updated');

		$('#program').trigger('change');

	}); /*end doc ready*/
	function cari_mahasiswa() {
		BootstrapDialog.show({
			title: 'Cari data Mahasiswa',
			draggable: true,
			message: function(dialog) {
				var $message = $('<div></div>');
				$.get('<?= site_url('administrator/mhs_mobility/form_cari_mahasiswa') ?>', {
					ta: $('select[name=ta] option:selected').val()
				}, function(response) {
					$message.html(response);
				})
				return $message;
			},
			buttons: [{
				label: 'Ambil',
				icon: 'glyphicon glyphicon-plus',
				cssClass: 'btn-primary',
				action: function(dialog) {
					$('#nim').val($('#mahasiswa option:selected').data('nim'));
					$.LoadingOverlay('show');
					$.get('<?= site_url('administrator/mhs_asing/getInfoMhs') ?>', {
						nim: $('#mahasiswa option:selected').data('nim')
					}, function(response) {
						$('#nama').val(response[0].nama);
						$('#tlhr').val(response[0].tlhr);
						$('#smt').val(response[0].smstr_int + '| Last Registration : ' + response[0].last_smt);
						$('#nama_prodi').val(response[0].prodi_concat);
						$('#ta').val(response[0].ta);
						$('#doswal').val(response[0].nama_doswal);
						$('#skstotal').val(response[0].sks_total + ' ( Until Periode ' + response[0].smstr_int + ' )');
						$('#ipk').val(response[0].ipk);
						$('#status_mhs').val(response[0].status_mhs + ' | Current Registration Status : ' + response[0].nama_status_smt);
						$.LoadingOverlay('hide');
					}, 'json')
					dialog.close();
				}
			}, {
				label: 'Tutup',
				action: function(dialog) {
					dialog.close();
				}
			}],
		});
	}

	function add_to_history(otb, table, mhs_id) {
		var url = '<?php echo site_url('administrator/mhs_asing/add_to_history') ?>';
		var data = {};
		data[csrf] = token;
		data['table'] = table;
		data['mhs_asing_id'] = mhs_id;
		$.post(url, data, function(response) {
			oTable[otb].draw(false);
		})
	}

	function hapus_history_file(otb, table, id) {
		if (confirm('Yakin akan dihapus?')) {
			var url = '<?php echo site_url('administrator/mhs_asing/hapus_history_file') ?>';
			$.get(url, {
				table: table,
				id: id
			}, function(response) {
				oTable[otb].draw(false);
			})
		}
	}

	function update_history_file(otb, table, id) {
		BootstrapDialog.show({
			title: 'Deskripsi',
			cssClass: 'mydialog',
			message: function(dialog) {
				var $message = $('<div><input type"text" name="deskripsi" class="form-control"></div>');
				return $message;
			},
			buttons: [{
					label: 'Simpan',
					cssClass: 'btn-success',
					action: function(dialog) {
						var data = {};
						data[csrf] = token;
						data['table'] = table;
						data['id'] = id;
						data['deskripsi'] = $('input[name=deskripsi]').val();
						$.post('<?= site_url('administrator/mhs_asing/update_history_file') ?>', data, function() {
							oTable[otb].draw(false);
						})
						dialog.close();
					}
				},
				{
					label: 'Tutup',
					action: function(dialog) {
						dialog.close();
					}
				}
			],
		});
	}
</script>