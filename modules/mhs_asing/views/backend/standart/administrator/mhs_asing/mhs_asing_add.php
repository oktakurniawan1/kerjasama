<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Mahasiswa Internasional <small><?= cclang('new', ['Mahasiswa Internasional']); ?> </small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/mhs_asing'); ?>">Mahasiswa Internasional</a></li>
		<li class="active"><?= cclang('new'); ?></li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Mahasiswa Internasional</h3>
							<h5 class="widget-user-desc"><?= cclang('new', ['Mahasiswa Internasional']); ?></h5>
							<hr>
						</div>
						<?= form_open('', [
							'name'    => 'form_mhs_asing',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_mhs_asing',
							'enctype' => 'multipart/form-data',
							'method'  => 'POST'
						]); ?>

						<div class="form-group ">
							<label for="nim" class="col-sm-2 control-label">Student ID
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<?php if (is_member('Mahasiswa')) : ?>
									<input type="text" class="form-control" name="nim" id="nim" placeholder="Student ID" value="<?= $this->session->userdata('identitas'); ?>" readonly>
								<?php else : ?>
									<input type="text" class="form-control" name="nim" id="nim" placeholder="Student ID" value="<?= set_value('nim'); ?>" readonly>
									<div class='row' style="margin-top:10px">
										<div class="col-sm-2" style="padding-right: 5px;">
											<?= form_dropdown('ta', array_combine(array_reverse(range(2000, date('Y'))), array_reverse(range(2000, date('Y')))), date('Y'), ['class' => 'form-control input-sm chosen chosen-select']) ?>
										</div>
										<div class="col-sm-4" style="padding-left: 0px;">
											<button class="btn btn-primary btn-sm" type="button" onclick="cari_mahasiswa()">Cari Mahasiswa</button>
										</div>
									</div>
								<?php endif; ?>
								<small class="info help-block">
									<b>Input Nim</b> Max Length : 10, <b>klik tombol cari mahasiswa untuk bantuan cari data mahasiswa dengan filter angkatan.</b></small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nama" class="col-sm-2 control-label">Name
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id='nama' disabled value="">
							</div>
						</div>

						<div class="form-group ">
							<label for="nama" class="col-sm-2 control-label">Day of Birth
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="tlhr" disabled value="">
							</div>
						</div>

						<div class="form-group ">
							<label for="nama" class="col-sm-2 control-label">Semester
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="smt" disabled value="">
							</div>
						</div>

						<div class="form-group ">
							<label for="nama_prodi" class="col-sm-2 control-label">Study Program
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="nama_prodi" disabled value="">
							</div>
						</div>

						<div class="form-group ">
							<label for="ta" class="col-sm-2 control-label">Class Year
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="ta" disabled value="">
							</div>
						</div>

						<div class="form-group ">
							<label for="ta" class="col-sm-2 control-label">Supervisor
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="doswal" disabled value="">
							</div>
						</div>

						<div class="form-group ">
							<label for="ta" class="col-sm-2 control-label">Number Of Course
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="skstotal" disabled value="">
							</div>
						</div>

						<div class="form-group ">
							<label for="ta" class="col-sm-2 control-label">GPA
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="ipk" disabled value="">
							</div>
						</div>

						<div class="form-group ">
							<label for="ta" class="col-sm-2 control-label">Status
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="status_mhs" disabled value="">
							</div>
						</div>

						<div class="form-group ">
							<label for="program" class="col-sm-2 control-label">Program
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select" name="program" id="program" data-placeholder="Select program">
									<option value=""></option>
									<option value="d">Degree</option>
									<option value="nd">Non Degree</option>
								</select>
							</div>
						</div>

						<div class="non-degree-form">
							<div class="form-group">
								<label for="program_type" class="col-sm-2 control-label">Program Type
									<i class="required">*</i>
								</label>
								<div class="col-sm-8">
									<select class="form-control chosen chosen-select" name="program_type" id="program_type" data-placeholder="Select type program">
										<option value=""></option>
										<?php foreach (db_get_all_data('program_nondegree') as $row) : ?>
											<option value="<?= $row->id ?>"><?= $row->nama_program; ?></option>
										<?php endforeach; ?>
									</select>
									<small class="info help-block">
										<b>Select program type</b>
									</small>
								</div>
							</div>

							<div class="form-group">
								<label for="nd_start_date" class="col-sm-2 control-label">Start Date
									<i class="required">*</i>
								</label>
								<div class="col-sm-8">
									<input type="text" class="form-control datepicker" id="nd_start_date" name="nd_start_date" placeholder="Start Date" value="<?= set_value('nd_start_date'); ?>">
									<small class="info help-block">
										<b>Start date format must</b> Valid Date, <b>Input start date</b> valid date format : yyyy-mm-dd.
									</small>
								</div>
							</div>

							<div class="form-group">
								<label for="nd_finish_date" class="col-sm-2 control-label">Finish Date
									<i class="required">*</i>
								</label>
								<div class="col-sm-8">
									<input type="text" class="form-control datepicker" id="nd_finish_date" name="nd_finish_date" placeholder="Finish Date" value="<?= set_value('nd_finish_date'); ?>">
									<small class="info help-block">
										<b>Finish date format must</b> Valid Date, <b>Input finish date</b> valid date format : yyyy-mm-dd.
									</small>
								</div>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_ijin_belajar" class="col-sm-2 control-label">Study Permit
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="mhs_asing_file_ijin_belajar_galery"></div>
								<input class="data_file" name="mhs_asing_file_ijin_belajar_uuid" id="mhs_asing_file_ijin_belajar_uuid" type="hidden" value="<?= set_value('mhs_asing_file_ijin_belajar_uuid'); ?>">
								<input class="data_file" name="mhs_asing_file_ijin_belajar_name" id="mhs_asing_file_ijin_belajar_name" type="hidden" value="<?= set_value('mhs_asing_file_ijin_belajar_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_ijin_belajar" class="col-sm-2 control-label">Study Permit Date
								<i class="required">*</i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-8">
									<input type="text" class="form-control pull-right datepicker" name="tgl_ijin_belajar" id="tgl_ijin_belajar" placeholder="Study Permit Date" value="<?= set_value('tgl_ijin_belajar'); ?>">
								</div>
								<small class="info help-block">
									<b>Study permit date format must</b> Valid Date, <b>Input study permit</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_pasport" class="col-sm-2 control-label">Pasport ID
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="mhs_asing_file_pasport_galery"></div>
								<input class="data_file" name="mhs_asing_file_pasport_uuid" id="mhs_asing_file_pasport_uuid" type="hidden" value="<?= set_value('mhs_asing_file_pasport_uuid'); ?>">
								<input class="data_file" name="mhs_asing_file_pasport_name" id="mhs_asing_file_pasport_name" type="hidden" value="<?= set_value('mhs_asing_file_pasport_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_pasport" class="col-sm-2 control-label">Pasport Date
								<i class="required">*</i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-8">
									<input type="text" class="form-control pull-right datepicker" name="tgl_pasport" placeholder="Pasport Date" id="tgl_pasport">
								</div>
								<small class="info help-block">
									<b>Date pasport format must</b> Valid Date.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_kitas" class="col-sm-2 control-label">KITAS File
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="mhs_asing_file_kitas_galery"></div>
								<input class="data_file" name="mhs_asing_file_kitas_uuid" id="mhs_asing_file_kitas_uuid" type="hidden" value="<?= set_value('mhs_asing_file_kitas_uuid'); ?>">
								<input class="data_file" name="mhs_asing_file_kitas_name" id="mhs_asing_file_kitas_name" type="hidden" value="<?= set_value('mhs_asing_file_kitas_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_kitas" class="col-sm-2 control-label">KITAS Date
								<i class="required">*</i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-8">
									<input type="text" class="form-control pull-right datepicker" name="tgl_kitas" id="tgl_kitas" placeholder="KITAS Date" value="<?= set_value('tgl_kitas'); ?>">
								</div>
								<small class="info help-block">
									<b>Date kitas format must</b> Valid Date, <b>Input kitas date</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="negara" class="col-sm-2 control-label">Country
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="negara" id="negara" data-placeholder="Country">
									<option value=""></option>
									<?php foreach (db_get_all_data('negara') as $row) : ?>
										<option value="<?= $row->nama_negara ?>"><?= $row->nama_negara; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Select Country</b></small>
							</div>
						</div>

						<div class="form-group ">
							<label for="alamat_lokal" class="col-sm-2 control-label">Residence At Indonesia
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="alamat_lokal" id="alamat_lokal" placeholder="Residence At Indonesia" value="<?= set_value('alamat_lokal'); ?>">
								<small class="info help-block">
									<b>Input residence at Indonesia</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tlp" class="col-sm-2 control-label">Telephone
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="tlp" id="tlp" placeholder="Telephone" value="<?= set_value('tlp'); ?>">
								<small class="info help-block">
									<b>Input telephone</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tlp_darurat" class="col-sm-2 control-label">Emergency Contact
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="tlp_darurat" id="tlp_darurat" placeholder="Emergency Contact" value="<?= set_value('tlp_darurat'); ?>">
								<small class="info help-block">
									<b>Input emergency contact</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="email" class="col-sm-2 control-label">Email
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?= set_value('email'); ?>">
								<small class="info help-block">
									<b>Email format must</b> Valid Email, <b>Input Email</b> Max Length : 255.</small>
							</div>
						</div>


						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	$(document).ready(function() {


		$('#btn_cancel').click(function() {
			swal({
					title: "<?= cclang('are_you_sure'); ?>",
					text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/mhs_asing';
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();

			var form_mhs_asing = $('#form_mhs_asing');
			var data_post = form_mhs_asing.serializeArray();
			var save_type = $(this).attr('data-stype');

			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: BASE_URL + '/administrator/mhs_asing/add_save',
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('.steps li').removeClass('error');
					$('form').find('.error-input').remove();
					if (res.success) {
						var id_file_ijin_belajar = $('#mhs_asing_file_ijin_belajar_galery').find('li').attr('qq-file-id');
						var id_file_pasport = $('#mhs_asing_file_pasport_galery').find('li').attr('qq-file-id');
						var id_file_kitas = $('#mhs_asing_file_kitas_galery').find('li').attr('qq-file-id');

						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						resetForm();
						if (typeof id_file_ijin_belajar !== 'undefined') {
							$('#mhs_asing_file_ijin_belajar_galery').fineUploader('deleteFile', id_file_ijin_belajar);
						}
						if (typeof id_file_pasport !== 'undefined') {
							$('#mhs_asing_file_pasport_galery').fineUploader('deleteFile', id_file_pasport);
						}
						if (typeof id_file_kitas !== 'undefined') {
							$('#mhs_asing_file_kitas_galery').fineUploader('deleteFile', id_file_kitas);
						}
						$('.chosen option').prop('selected', false).trigger('chosen:updated');

					} else {
						if (res.errors) {

							$.each(res.errors, function(index, val) {
								$('form #' + index).parents('.form-group').addClass('has-error');
								$('form #' + index).parents('.form-group').find('small').prepend(`
                      <div class="error-input">` + val + `</div>
                      `);
							});
							$('.steps li').removeClass('error');
							$('.content section').each(function(index, el) {
								if ($(this).find('.has-error').length) {
									$('.steps li:eq(' + index + ')').addClass('error').find('a').trigger('click');
								}
							});
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/

		var params = {};
		params[csrf] = token;

		$('#mhs_asing_file_ijin_belajar_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_asing/upload_file_ijin_belajar_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mhs_asing/delete_file_ijin_belajar_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_asing_file_ijin_belajar_galery').fineUploader('getUuid', id);
						$('#mhs_asing_file_ijin_belajar_uuid').val(uuid);
						$('#mhs_asing_file_ijin_belajar_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_asing_file_ijin_belajar_uuid').val();
					$.get(BASE_URL + '/administrator/mhs_asing/delete_file_ijin_belajar_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_asing_file_ijin_belajar_uuid').val('');
						$('#mhs_asing_file_ijin_belajar_name').val('');
					}
				}
			}
		}); /*end file_ijin_belajar galery*/
		var params = {};
		params[csrf] = token;

		$('#mhs_asing_file_pasport_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_asing/upload_file_pasport_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mhs_asing/delete_file_pasport_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_asing_file_pasport_galery').fineUploader('getUuid', id);
						$('#mhs_asing_file_pasport_uuid').val(uuid);
						$('#mhs_asing_file_pasport_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_asing_file_pasport_uuid').val();
					$.get(BASE_URL + '/administrator/mhs_asing/delete_file_pasport_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_asing_file_pasport_uuid').val('');
						$('#mhs_asing_file_pasport_name').val('');
					}
				}
			}
		}); /*end file_pasport galery*/
		var params = {};
		params[csrf] = token;

		$('#mhs_asing_file_kitas_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_asing/upload_file_kitas_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mhs_asing/delete_file_kitas_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_asing_file_kitas_galery').fineUploader('getUuid', id);
						$('#mhs_asing_file_kitas_uuid').val(uuid);
						$('#mhs_asing_file_kitas_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_asing_file_kitas_uuid').val();
					$.get(BASE_URL + '/administrator/mhs_asing/delete_file_kitas_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_asing_file_kitas_uuid').val('');
						$('#mhs_asing_file_kitas_name').val('');
					}
				}
			}
		}); /*end file_kitas galery*/
		$('.non-degree-form').hide()
		$('#program').change(function() {
			$.LoadingOverlay('show');
			if ($(this).val() == 'nd') {
				$('.non-degree-form').show();
			} else {
				$('.non-degree-form').hide();
			}
			$.LoadingOverlay('hide');
		})
		
	}); /*end doc ready*/
	function cari_mahasiswa() {
		BootstrapDialog.show({
			title: 'Cari data Mahasiswa',
			draggable: true,
			message: function(dialog) {
				var $message = $('<div></div>');
				$.get('<?= site_url('administrator/mhs_mobility/form_cari_mahasiswa') ?>', {
					ta: $('select[name=ta] option:selected').val()
				}, function(response) {
					$message.html(response);
				})
				return $message;
			},
			buttons: [{
				label: 'Ambil',
				icon: 'glyphicon glyphicon-plus',
				cssClass: 'btn-primary',
				action: function(dialog) {
					$('#nim').val($('#mahasiswa option:selected').data('nim'));
					$.LoadingOverlay('show');
					$.get('<?= site_url('administrator/mhs_asing/getInfoMhs') ?>', {
						nim: $('#mahasiswa option:selected').data('nim')
					}, function(response) {
						$('#nama').val(response[0].nama);
						$('#tlhr').val(response[0].tlhr);
						$('#smt').val(response[0].smstr_int + '| Last Registration : ' + response[0].last_smt);
						$('#nama_prodi').val(response[0].prodi_concat);
						$('#ta').val(response[0].ta);
						$('#doswal').val(response[0].nama_doswal);
						$('#skstotal').val(response[0].sks_total + ' ( Until Periode ' + response[0].smstr_int + ' )');
						$('#ipk').val(response[0].ipk);
						$('#status_mhs').val(response[0].status_mhs + ' | Current Registration Status : ' + response[0].nama_status_smt);
						$.LoadingOverlay('hide');
					}, 'json')
					dialog.close();
				}
			}, {
				label: 'Tutup',
				action: function(dialog) {
					dialog.close();
				}
			}],
		});
	}
	
	function addToHistory(id) {

	}
</script>