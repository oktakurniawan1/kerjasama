<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['mhs_asing'] = 'Mahasiswa Luar Negeri';
$lang['id'] = 'Id';
$lang['nim'] = 'Student ID';
$lang['file_ijin_belajar'] = 'Studi Permit';
$lang['tgl_ijin_belajar'] = 'Permit Date';
$lang['file_pasport'] = 'Pasport ID';
$lang['tgl_pasport'] = 'Pasport Date';
$lang['file_kitas'] = 'KITAS File';
$lang['tgl_kitas'] = 'KITAS Date';
$lang['negara'] = 'Country';
$lang['alamat_lokal'] = 'Residence At Indonesia';
$lang['tlp'] = 'Telephone';
$lang['tlp_darurat'] = 'Emergency Contact';
$lang['email'] = 'Email';
