<?php
defined('BASEPATH') or exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Mhs_asing extends API
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_mhs_asing');
	}

	/**
	 * @api {get} /mhs_asing/all Get all mhs_asings.
	 * @apiVersion 0.1.0
	 * @apiName AllMhsasing 
	 * @apiGroup mhs_asing
	 * @apiHeader {String} X-Api-Key Mhs asings unique access-key.
	 * @apiHeader {String} X-Token Mhs asings unique token.
	 * @apiPermission Mhs asing Cant be Accessed permission name : api_mhs_asing_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Mhs asings.
	 * @apiParam {String} [Field="All Field"] Optional field of Mhs asings : id, nim, program, program_type, nd_start_date, nd_finish_date, file_ijin_belajar, tgl_ijin_belajar, file_pasport, tgl_pasport, file_kitas, tgl_kitas, negara, alamat_lokal, tlp, tlp_darurat, email, ipk, lls, tgl_lulus.
	 * @apiParam {String} [Start=0] Optional start index of Mhs asings.
	 * @apiParam {String} [Limit=10] Optional limit data of Mhs asings.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of mhs_asing.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataMhs asing Mhs asing data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		// $this->is_allowed('api_mhs_asing_all');

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'nim', 'program', 'program_type', 'nd_start_date', 'nd_finish_date', 'file_ijin_belajar', 'tgl_ijin_belajar', 'file_pasport', 'tgl_pasport', 'file_kitas', 'tgl_kitas', 'negara', 'alamat_lokal', 'tlp', 'tlp_darurat', 'email', 'ipk', 'lls', 'tgl_lulus'];
		$mhs_asings = $this->model_api_mhs_asing->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_mhs_asing->count_all($filter, $field);
		$mhs_asings = array_map(function ($row) {

			return $row;
		}, $mhs_asings);

		$data['mhs_asing'] = $mhs_asings;

		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Mhs asing',
			'data'	 	=> $data,
			'total' 	=> $total,
		], API::HTTP_OK);
	}
	public function mhs_get()
	{
		ini_set('memory_limit', '512M');
		ini_set('output_buffering', 'off');
		ini_set('zlib.output_compression', false);
		
		ob_start(); // Memulai output buffering di awal

		header('Content-Type: application/json');
		echo '[';
		$first = true;
		$result = $this->model_api_mhs_asing->getmhs();
		foreach ($result as $row) {
			if (!$first) {
				echo ',';
			}
			echo json_encode($row);
			$first = false;
		}
		echo ']';
		ob_end_flush(); 
	}
}

/* End of file Mhs asing.php */
/* Location: ./application/controllers/api/Mhs asing.php */