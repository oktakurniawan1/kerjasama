<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Mhs Asing Controller
 *| --------------------------------------------------------------------------
 *| Mhs Asing site
 *|
 */
class Mhs_asing extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_mhs_asing');
		$this->lang->load('web_lang', $this->current_lang);
	}

	public function history_files()
	{
		$data['otb'] = $this->input->get('otb');
		$data['table'] = $this->input->get('table');
		$data['mhs_asing_id'] = $this->input->get('mhs_asing_id');
		$this->load->view('history-files-table', $data);
	}
	public function browse_history_files()
	{
		echo $this->model_mhs_asing->gethistoryfiles($this->input->post('table'), $this->input->post('mhs_asing_id'));
	}
	public function add_to_history()
	{
		$table = $this->input->post('table');
		$mhs_asing_id = $this->input->post('mhs_asing_id');
		$this->model_mhs_asing->add_to_history($table, $mhs_asing_id);
	}
	public function hapus_history_file()
	{
		$this->model_mhs_asing->hapus_history_file($this->input->get('table'), $this->input->get('id'));
	}
	public function update_history_file()
	{
		$this->model_mhs_asing->update_history_file($this->input->post());
	}

	/**
	 * form filter data 
	 */
	public function filter_session_data()
	{
		$fields['program_studi'] = $this->input->post('program_studi');
		$fields['negara'] = $this->input->post('negara');
		$this->session->set_userdata('filterdata_mhs_asing', $fields);
		$this->session->set_userdata('is_filtered_mhs_asing', TRUE);
		echo json_encode(['url' => base_url('administrator/mhs_asing')]);
	}

	public function clear_filter_session_data()
	{
		$this->session->unset_userdata('filterdata_mhs_asing');
		$this->session->unset_userdata('is_filtered_mhs_asing');
		echo json_encode(['url' => base_url('administrator/mhs_asing')]);
	}

	public function form_filter_data()
	{
		if (!$this->session->has_userdata('filterdata_mhs_asing')) {
			$fields['program_studi'] = 'all';
			$fields['negara'] = 'all';
			$this->session->set_userdata('filterdata_mhs_asing', $fields);
		}
		$tmp = get_filter_prodi();
		$prodi['all'] = 'ALL';
		foreach ($tmp as $p) {
			$prodi[$p->kode] = $p->kode . ' - ' . $p->nama_prodi;
		}
		$negara = array_column(db_get_all_data('negara'), 'nama_negara', 'nama_negara');
		$negara['all'] = 'ALL';
		$data['negara'] = $negara;
		$data['program_studi'] = $prodi;
		$this->load->view('form-filter-data', $data);
	}
	/**
	 * show all Mhs Asings
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('mhs_asing_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('mhs_asing_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('mhs_asing_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('mhs_asing_filter');
			$this->session->unset_userdata('mhs_asing_field');
		}
		$filter = $this->session->userdata('mhs_asing_filter');
		$field  = $this->session->userdata('mhs_asing_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['mhs_asings'] = $this->model_mhs_asing->get($filter, $field, $this->limit_page, $offset);
		$this->data['mhs_asing_counts'] = $this->model_mhs_asing->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/mhs_asing/index/',
			'total_rows'   => $this->model_mhs_asing->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Mahasiswa Internasional List');
		$this->render('backend/standart/administrator/mhs_asing/mhs_asing_list', $this->data);
	}

	/**
	 * Add new mhs_asings
	 *
	 */
	public function add()
	{
		$this->is_allowed('mhs_asing_add');

		$this->template->title('Mahasiswa Internasional New');
		$this->render('backend/standart/administrator/mhs_asing/mhs_asing_add', $this->data);
	}

	/**
	 * Add New Mhs Asings
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('mhs_asing_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('nim', 'Student ID', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('program', 'Program', 'trim|required');
		if($this->input->post('program') == 'nd'){
			$this->form_validation->set_rules('program_type', 'Program Type', 'trim|required');
			$this->form_validation->set_rules('nd_start_date', 'Start Date', 'required|callback_valid_date');
			$this->form_validation->set_rules('nd_finish_date', 'Finish Date', 'required|callback_valid_date');
		}
		if (is_member('Mahasiswa')) {
			$this->form_validation->set_rules('mhs_asing_file_ijin_belajar_name', 'Studi Permit', 'trim|required');
			$this->form_validation->set_rules('mhs_asing_file_pasport_name', 'Pasport ID', 'trim|required');
			$this->form_validation->set_rules('tgl_pasport', 'Pasport Date', 'trim|required|callback_valid_date');
			$this->form_validation->set_rules('mhs_asing_file_kitas_name', 'KITAS File', 'trim|required');
			$this->form_validation->set_rules('tgl_kitas', 'KITAS Date', 'trim|required|max_length[255]|callback_valid_date');
			$this->form_validation->set_rules('tgl_ijin_belajar', 'Permit Date', 'trim|required|max_length[255]|callback_valid_date');
			$this->form_validation->set_rules('negara', 'Country', 'trim|required|max_length[255]');
			$this->form_validation->set_rules('alamat_lokal', 'Residence At Indonesia', 'trim|required|max_length[255]');
			$this->form_validation->set_rules('tlp', 'Telephone', 'trim|required|max_length[255]');
			$this->form_validation->set_rules('tlp_darurat', 'Emergency Contact', 'trim|required|max_length[255]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[255]|valid_email');
			$this->form_validation->set_rules('program', 'Program', 'trim|required');
		}


		if ($this->form_validation->run()) {
			$mhs_asing_file_ijin_belajar_uuid = $this->input->post('mhs_asing_file_ijin_belajar_uuid');
			$mhs_asing_file_ijin_belajar_name = $this->input->post('mhs_asing_file_ijin_belajar_name');
			$mhs_asing_file_pasport_uuid      = $this->input->post('mhs_asing_file_pasport_uuid');
			$mhs_asing_file_pasport_name      = $this->input->post('mhs_asing_file_pasport_name');
			$mhs_asing_file_kitas_uuid        = $this->input->post('mhs_asing_file_kitas_uuid');
			$mhs_asing_file_kitas_name        = $this->input->post('mhs_asing_file_kitas_name');

			$save_data = [
				'nim'              => $this->input->post('nim'),
				'tgl_pasport'      => $this->input->post('tgl_pasport'),
				'tgl_kitas'        => $this->input->post('tgl_kitas'),
				'tgl_ijin_belajar' => $this->input->post('tgl_ijin_belajar'),
				'negara'           => $this->input->post('negara'),
				'alamat_lokal'     => $this->input->post('alamat_lokal'),
				'tlp'              => $this->input->post('tlp'),
				'tlp_darurat'      => $this->input->post('tlp_darurat'),
				'email'            => $this->input->post('email'),
				'program'          => $this->input->post('program'),
				'program_type'     => $this->input->post('program_type'),
				'nd_start_date'    => $this->input->post('nd_start_date'),
				'nd_finish_date'   => $this->input->post('nd_finish_date'),
			];

			if (!is_dir(FCPATH . '/uploads/mhs_asing/')) {
				mkdir(FCPATH . '/uploads/mhs_asing/');
			}

			if (!empty($mhs_asing_file_ijin_belajar_name)) {
				$mhs_asing_file_ijin_belajar_name_copy = date('YmdHis') . '-' . $mhs_asing_file_ijin_belajar_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_asing_file_ijin_belajar_uuid . '/' . $mhs_asing_file_ijin_belajar_name,
					FCPATH . 'uploads/mhs_asing/' . $mhs_asing_file_ijin_belajar_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_asing/' . $mhs_asing_file_ijin_belajar_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ijin_belajar'] = $mhs_asing_file_ijin_belajar_name_copy;
			}

			if (!empty($mhs_asing_file_pasport_name)) {
				$mhs_asing_file_pasport_name_copy = date('YmdHis') . '-' . $mhs_asing_file_pasport_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_asing_file_pasport_uuid . '/' . $mhs_asing_file_pasport_name,
					FCPATH . 'uploads/mhs_asing/' . $mhs_asing_file_pasport_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_asing/' . $mhs_asing_file_pasport_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_pasport'] = $mhs_asing_file_pasport_name_copy;
			}

			if (!empty($mhs_asing_file_kitas_name)) {
				$mhs_asing_file_kitas_name_copy = date('YmdHis') . '-' . $mhs_asing_file_kitas_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_asing_file_kitas_uuid . '/' . $mhs_asing_file_kitas_name,
					FCPATH . 'uploads/mhs_asing/' . $mhs_asing_file_kitas_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_asing/' . $mhs_asing_file_kitas_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_kitas'] = $mhs_asing_file_kitas_name_copy;
			}

			$save_mhs_asing = $this->model_mhs_asing->store($save_data);

			if ($save_mhs_asing) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_mhs_asing;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/mhs_asing/edit/' . $save_mhs_asing, 'Edit Mhs Asing'),
						anchor('administrator/mhs_asing', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/mhs_asing/edit/' . $save_mhs_asing, 'Edit Mhs Asing')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/mhs_asing');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success']  = false;
					$this->data['message']  = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/mhs_asing');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Mhs Asings
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('mhs_asing_update');
		$this->data['mhs_asing'] = $this->model_mhs_asing->join_avaiable()->find($id);
		$this->data['ket_mhs'] = getInfoMhs($this->data['mhs_asing']->nim);
		$this->template->title('Mahasiswa Internasional Update');
		$this->render('backend/standart/administrator/mhs_asing/mhs_asing_update', $this->data);
	}
	public function getInfoMhs()
	{
		echo json_encode(getInfoMhs($this->input->get('nim')));
	}
	/**
	 * Update Mhs Asings
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('mhs_asing_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('nim', 'Student ID', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('program', 'Program', 'trim|required');
		if($this->input->post('program') == 'nd'){
			$this->form_validation->set_rules('program_type', 'Program Type', 'trim|required');
			$this->form_validation->set_rules('nd_start_date', 'Start Date', 'required|callback_valid_date');
			$this->form_validation->set_rules('nd_finish_date', 'Finish Date', 'required|callback_valid_date');
		}
		if (is_member('Mahasiswa')) {
			$this->form_validation->set_rules('mhs_asing_file_ijin_belajar_name', 'Studi Permit', 'trim|required');
			$this->form_validation->set_rules('mhs_asing_file_pasport_name', 'Pasport ID', 'trim|required');
			$this->form_validation->set_rules('tgl_pasport', 'Pasport Date', 'trim|required|callback_valid_date');
			$this->form_validation->set_rules('mhs_asing_file_kitas_name', 'KITAS File', 'trim|required');
			$this->form_validation->set_rules('tgl_kitas', 'KITAS Date', 'trim|required|max_length[255]|callback_valid_date');
			$this->form_validation->set_rules('tgl_ijin_belajar', 'Permit Date', 'trim|required|max_length[255]|callback_valid_date');
			$this->form_validation->set_rules('negara', 'Country', 'trim|required|max_length[255]');
			$this->form_validation->set_rules('alamat_lokal', 'Residence At Indonesia', 'trim|required|max_length[255]');
			$this->form_validation->set_rules('tlp', 'Telephone', 'trim|required|max_length[255]');
			$this->form_validation->set_rules('tlp_darurat', 'Emergency Contact', 'trim|required|max_length[255]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[255]|valid_email');
		}

		if ($this->form_validation->run()) {
			$mhs_asing_file_ijin_belajar_uuid = $this->input->post('mhs_asing_file_ijin_belajar_uuid');
			$mhs_asing_file_ijin_belajar_name = $this->input->post('mhs_asing_file_ijin_belajar_name');
			$mhs_asing_file_pasport_uuid = $this->input->post('mhs_asing_file_pasport_uuid');
			$mhs_asing_file_pasport_name = $this->input->post('mhs_asing_file_pasport_name');
			$mhs_asing_file_kitas_uuid = $this->input->post('mhs_asing_file_kitas_uuid');
			$mhs_asing_file_kitas_name = $this->input->post('mhs_asing_file_kitas_name');

			$save_data = [
				'nim'              => $this->input->post('nim'),
				'tgl_pasport'      => $this->input->post('tgl_pasport'),
				'tgl_kitas'        => $this->input->post('tgl_kitas'),
				'tgl_ijin_belajar' => $this->input->post('tgl_ijin_belajar'),
				'negara'           => $this->input->post('negara'),
				'alamat_lokal'     => $this->input->post('alamat_lokal'),
				'tlp'              => $this->input->post('tlp'),
				'tlp_darurat'      => $this->input->post('tlp_darurat'),
				'email'            => $this->input->post('email'),
				'program'          => $this->input->post('program'),
				'program_type'     => $this->input->post('program_type'),
				'nd_start_date'    => $this->input->post('nd_start_date'),
				'nd_finish_date'   => $this->input->post('nd_finish_date'),
			];

			if (!is_dir(FCPATH . '/uploads/mhs_asing/')) {
				mkdir(FCPATH . '/uploads/mhs_asing/');
			}

			if (!empty($mhs_asing_file_ijin_belajar_uuid)) {
				$mhs_asing_file_ijin_belajar_name_copy = date('YmdHis') . '-' . $mhs_asing_file_ijin_belajar_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_asing_file_ijin_belajar_uuid . '/' . $mhs_asing_file_ijin_belajar_name,
					FCPATH . 'uploads/mhs_asing/' . $mhs_asing_file_ijin_belajar_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_asing/' . $mhs_asing_file_ijin_belajar_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ijin_belajar'] = $mhs_asing_file_ijin_belajar_name_copy;
			}

			if (!empty($mhs_asing_file_pasport_uuid)) {
				$mhs_asing_file_pasport_name_copy = date('YmdHis') . '-' . $mhs_asing_file_pasport_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_asing_file_pasport_uuid . '/' . $mhs_asing_file_pasport_name,
					FCPATH . 'uploads/mhs_asing/' . $mhs_asing_file_pasport_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_asing/' . $mhs_asing_file_pasport_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_pasport'] = $mhs_asing_file_pasport_name_copy;
			}

			if (!empty($mhs_asing_file_kitas_uuid)) {
				$mhs_asing_file_kitas_name_copy = date('YmdHis') . '-' . $mhs_asing_file_kitas_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_asing_file_kitas_uuid . '/' . $mhs_asing_file_kitas_name,
					FCPATH . 'uploads/mhs_asing/' . $mhs_asing_file_kitas_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_asing/' . $mhs_asing_file_kitas_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_kitas'] = $mhs_asing_file_kitas_name_copy;
			}


			$save_mhs_asing = $this->model_mhs_asing->change($id, $save_data);

			if ($save_mhs_asing) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/mhs_asing', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/mhs_asing/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/mhs_asing/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Mhs Asings
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('mhs_asing_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'mhs_asing'), 'success');
		} else {
			set_message(cclang('error_delete', 'mhs_asing'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Mhs Asings
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('mhs_asing_view');
		$this->data['mhs_asing'] = $this->model_mhs_asing->join_avaiable()->filter_avaiable()->find($id);
		$this->data['permits'] = db_get_all_data('mhs_asing_permits', ['mhs_asing_id' => $id], 'result');
		$this->data['kitas'] = db_get_all_data('mhs_asing_kitas', ['mhs_asing_id' => $id], 'result');
		$this->data['pasports'] = db_get_all_data('mhs_asing_pasports', ['mhs_asing_id' => $id], 'result');
		$this->data['ket_mhs'] = getInfoMhs($this->data['mhs_asing']->nim);
		$this->template->title('Mahasiswa Internasional Detail');
		$this->render('backend/standart/administrator/mhs_asing/mhs_asing_view', $this->data);
	}

	/**
	 * delete Mhs Asings
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$mhs_asing = $this->model_mhs_asing->find($id);

		if (!empty($mhs_asing->file_ijin_belajar)) {
			$path = FCPATH . '/uploads/mhs_asing/' . $mhs_asing->file_ijin_belajar;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($mhs_asing->file_pasport)) {
			$path = FCPATH . '/uploads/mhs_asing/' . $mhs_asing->file_pasport;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($mhs_asing->file_kitas)) {
			$path = FCPATH . '/uploads/mhs_asing/' . $mhs_asing->file_kitas;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}


		return $this->model_mhs_asing->remove($id);
	}

	/**
	 * Upload Image Mhs Asing	* 
	 * @return JSON
	 */
	public function upload_file_ijin_belajar_file()
	{
		if (!$this->is_allowed('mhs_asing_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mhs_asing',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Mhs Asing	* 
	 * @return JSON
	 */
	public function delete_file_ijin_belajar_file($uuid)
	{
		if (!$this->is_allowed('mhs_asing_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ijin_belajar',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mhs_asing',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_asing/'
		]);
	}

	/**
	 * Get Image Mhs Asing	* 
	 * @return JSON
	 */
	public function get_file_ijin_belajar_file($id)
	{
		if (!$this->is_allowed('mhs_asing_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mhs_asing = $this->model_mhs_asing->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ijin_belajar',
			'table_name'        => 'mhs_asing',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_asing/',
			'delete_endpoint'   => 'administrator/mhs_asing/delete_file_ijin_belajar_file'
		]);
	}

	/**
	 * Upload Image Mhs Asing	* 
	 * @return JSON
	 */
	public function upload_file_pasport_file()
	{
		if (!$this->is_allowed('mhs_asing_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mhs_asing',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Mhs Asing	* 
	 * @return JSON
	 */
	public function delete_file_pasport_file($uuid)
	{
		if (!$this->is_allowed('mhs_asing_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_pasport',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mhs_asing',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_asing/'
		]);
	}

	/**
	 * Get Image Mhs Asing	* 
	 * @return JSON
	 */
	public function get_file_pasport_file($id)
	{
		if (!$this->is_allowed('mhs_asing_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mhs_asing = $this->model_mhs_asing->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_pasport',
			'table_name'        => 'mhs_asing',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_asing/',
			'delete_endpoint'   => 'administrator/mhs_asing/delete_file_pasport_file'
		]);
	}

	/**
	 * Upload Image Mhs Asing	* 
	 * @return JSON
	 */
	public function upload_file_kitas_file()
	{
		if (!$this->is_allowed('mhs_asing_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mhs_asing',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Mhs Asing	* 
	 * @return JSON
	 */
	public function delete_file_kitas_file($uuid)
	{
		if (!$this->is_allowed('mhs_asing_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_kitas',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mhs_asing',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_asing/'
		]);
	}

	/**
	 * Get Image Mhs Asing	* 
	 * @return JSON
	 */
	public function get_file_kitas_file($id)
	{
		if (!$this->is_allowed('mhs_asing_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mhs_asing = $this->model_mhs_asing->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_kitas',
			'table_name'        => 'mhs_asing',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_asing/',
			'delete_endpoint'   => 'administrator/mhs_asing/delete_file_kitas_file'
		]);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$data = $this->model_mhs_asing->get($filter, $field, 0, $offset, [], FALSE)->result_array();
		$data = keyBy($data, 'nim');
		$nims = [];
		foreach ($data as $row) {
			$nims[] = $row['nim'];
		}
		$akm = getAKMMahasiswa($nims);
		$akm = keyBy($akm, 'nim');

		$hasil = [];
		foreach ($data as $key => $value) {
			if (isset($akm[$key])) {
				$hasil[$key] = array_merge($data[$key], $akm[$key]);
			}
		}
		$hasil = array_values($hasil);
		if(count($hasil) == 0)
			$selected_field = ['nim'];
		else
			$selected_field = array_keys($hasil[0]);
		export_excel($hasil, 'file', $selected_field, ['file_ijin_belajar'=>'link','file_pasport'=>'link','file_kitas'=>'link']);
		// $this->model_mhs_asing->export2($data, 'file', $selected_field);

		/*
		$this->is_allowed('mhs_asing_export');

		$this->model_mhs_asing->export(
			'mhs_asing', 
			'mhs_asing',
			$this->model_mhs_asing->field_search
		);
		*/
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('mhs_asing_export');

		$this->model_mhs_asing->pdf('mhs_asing', 'mhs_asing');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('mhs_asing_export');

		$table = $title = 'mhs_asing';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_mhs_asing->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
}
/* End of file mhs_asing.php */
/* Location: ./application/controllers/administrator/Mhs Asing.php */