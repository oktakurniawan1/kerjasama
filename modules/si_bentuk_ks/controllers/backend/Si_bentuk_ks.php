<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Si Bentuk Ks Controller
 *| --------------------------------------------------------------------------
 *| Si Bentuk Ks site
 *|
 */
class Si_bentuk_ks extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_si_bentuk_ks');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * show all Si Bentuk Kss
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('si_bentuk_ks_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('si_bentuk_ks_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('si_bentuk_ks_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('si_bentuk_ks_filter');
			$this->session->unset_userdata('si_bentuk_ks_field');
		}
		$filter = $this->session->userdata('si_bentuk_ks_filter');
		$field = $this->session->userdata('si_bentuk_ks_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['si_bentuk_kss'] = $this->model_si_bentuk_ks->get($filter, $field, $this->limit_page, $offset);
		$this->data['si_bentuk_ks_counts'] = $this->model_si_bentuk_ks->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/si_bentuk_ks/index/',
			'total_rows'   => $this->model_si_bentuk_ks->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Si Bentuk Ks List');
		$this->render('backend/standart/administrator/si_bentuk_ks/si_bentuk_ks_list', $this->data);
	}

	/**
	 * Add new si_bentuk_kss
	 *
	 */
	public function add()
	{
		$this->is_allowed('si_bentuk_ks_add');

		$this->template->title('Si Bentuk Ks New');
		$this->render('backend/standart/administrator/si_bentuk_ks/si_bentuk_ks_add', $this->data);
	}

	/**
	 * Add New Si Bentuk Kss
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('si_bentuk_ks_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('bentuk_ks', 'Bentuk Kerjasama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');


		if ($this->form_validation->run()) {

			$save_data = [
				'bentuk_ks' => $this->input->post('bentuk_ks'),
				'deskripsi' => $this->input->post('deskripsi'),
			];


			$save_si_bentuk_ks = $this->model_si_bentuk_ks->store($save_data);


			if ($save_si_bentuk_ks) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_si_bentuk_ks;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/si_bentuk_ks/edit/' . $save_si_bentuk_ks, 'Edit Si Bentuk Ks'),
						anchor('administrator/si_bentuk_ks', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/si_bentuk_ks/edit/' . $save_si_bentuk_ks, 'Edit Si Bentuk Ks')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/si_bentuk_ks');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/si_bentuk_ks');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Si Bentuk Kss
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('si_bentuk_ks_update');

		$this->data['si_bentuk_ks'] = $this->model_si_bentuk_ks->find($id);

		$this->template->title('Si Bentuk Ks Update');
		$this->render('backend/standart/administrator/si_bentuk_ks/si_bentuk_ks_update', $this->data);
	}

	/**
	 * Update Si Bentuk Kss
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('si_bentuk_ks_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('bentuk_ks', 'Bentuk Kerjasama', 'trim|required|max_length[255]');

		if ($this->form_validation->run()) {

			$save_data = [
				'bentuk_ks' => $this->input->post('bentuk_ks'),
				'deskripsi' => $this->input->post('deskripsi'),
			];


			$save_si_bentuk_ks = $this->model_si_bentuk_ks->change($id, $save_data);

			if ($save_si_bentuk_ks) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/si_bentuk_ks', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/si_bentuk_ks/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/si_bentuk_ks/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Si Bentuk Kss
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->load->model('si_kerjasama/model_si_kerjasama');
		$check = $this->model_si_kerjasama->check_bentuk_ks($id);
		if ($check) {
			set_message('Data ini sudah digunakan di kerjasama Unit atau sudah memiliki sasaran, tidak bisa dihapus', 'error');
			redirect_back();
		}
		$this->is_allowed('si_bentuk_ks_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'si_bentuk_ks'), 'success');
		} else {
			set_message(cclang('error_delete', 'si_bentuk_ks'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Si Bentuk Kss
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('si_bentuk_ks_view');

		$this->data['si_bentuk_ks'] = $this->model_si_bentuk_ks->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Si Bentuk Ks Detail');
		$this->render('backend/standart/administrator/si_bentuk_ks/si_bentuk_ks_view', $this->data);
	}

	/**
	 * delete Si Bentuk Kss
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$si_bentuk_ks = $this->model_si_bentuk_ks->find($id);
		return $this->model_si_bentuk_ks->remove($id);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('si_bentuk_ks_export');

		$this->model_si_bentuk_ks->export(
			'si_bentuk_ks',
			'si_bentuk_ks',
			$this->model_si_bentuk_ks->field_search
		);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('si_bentuk_ks_export');

		$this->model_si_bentuk_ks->pdf('si_bentuk_ks', 'si_bentuk_ks');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('si_bentuk_ks_export');

		$table = $title = 'si_bentuk_ks';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_si_bentuk_ks->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
}


/* End of file si_bentuk_ks.php */
/* Location: ./application/controllers/administrator/Si Bentuk Ks.php */