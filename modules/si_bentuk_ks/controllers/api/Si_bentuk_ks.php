<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Si_bentuk_ks extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_si_bentuk_ks');
	}

	/**
	 * @api {get} /si_bentuk_ks/all Get all si_bentuk_kss.
	 * @apiVersion 0.1.0
	 * @apiName AllSibentukks 
	 * @apiGroup si_bentuk_ks
	 * @apiHeader {String} X-Api-Key Si bentuk kss unique access-key.
	 * @apiPermission Si bentuk ks Cant be Accessed permission name : api_si_bentuk_ks_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Si bentuk kss.
	 * @apiParam {String} [Field="All Field"] Optional field of Si bentuk kss : id, bentuk_ks, deskripsi.
	 * @apiParam {String} [Start=0] Optional start index of Si bentuk kss.
	 * @apiParam {String} [Limit=10] Optional limit data of Si bentuk kss.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of si_bentuk_ks.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataSi bentuk ks Si bentuk ks data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_si_bentuk_ks_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'bentuk_ks', 'deskripsi'];
		$si_bentuk_kss = $this->model_api_si_bentuk_ks->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_si_bentuk_ks->count_all($filter, $field);
		$si_bentuk_kss = array_map(function($row){
						
			return $row;
		}, $si_bentuk_kss);

		$data['si_bentuk_ks'] = $si_bentuk_kss;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Si bentuk ks',
			'data'	 	=> $data,
			'total' 	=> $total,
		], API::HTTP_OK);
	}

		/**
	 * @api {get} /si_bentuk_ks/detail Detail Si bentuk ks.
	 * @apiVersion 0.1.0
	 * @apiName DetailSi bentuk ks
	 * @apiGroup si_bentuk_ks
	 * @apiHeader {String} X-Api-Key Si bentuk kss unique access-key.
	 * @apiPermission Si bentuk ks Cant be Accessed permission name : api_si_bentuk_ks_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Si bentuk kss.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of si_bentuk_ks.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError Si bentuk ksNotFound Si bentuk ks data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_si_bentuk_ks_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'bentuk_ks', 'deskripsi'];
		$si_bentuk_ks = $this->model_api_si_bentuk_ks->find($id, $select_field);

		if (!$si_bentuk_ks) {
			$this->response([
					'status' 	=> false,
					'message' 	=> 'Blog not found'
				], API::HTTP_NOT_FOUND);
		}

					
		$data['si_bentuk_ks'] = $si_bentuk_ks;
		if ($data['si_bentuk_ks']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Si bentuk ks',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Si bentuk ks not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /si_bentuk_ks/add Add Si bentuk ks.
	 * @apiVersion 0.1.0
	 * @apiName AddSi bentuk ks
	 * @apiGroup si_bentuk_ks
	 * @apiHeader {String} X-Api-Key Si bentuk kss unique access-key.
	 * @apiPermission Si bentuk ks Cant be Accessed permission name : api_si_bentuk_ks_add
	 *
 	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_si_bentuk_ks_add', false);

		
		if ($this->form_validation->run()) {

			$save_data = [
			];
			
			$save_si_bentuk_ks = $this->model_api_si_bentuk_ks->store($save_data);

			if ($save_si_bentuk_ks) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /si_bentuk_ks/update Update Si bentuk ks.
	 * @apiVersion 0.1.0
	 * @apiName UpdateSi bentuk ks
	 * @apiGroup si_bentuk_ks
	 * @apiHeader {String} X-Api-Key Si bentuk kss unique access-key.
	 * @apiPermission Si bentuk ks Cant be Accessed permission name : api_si_bentuk_ks_update
	 *
	 * @apiParam {Integer} id Mandatory id of Si Bentuk Ks.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_si_bentuk_ks_update', false);

		
		
		if ($this->form_validation->run()) {

			$save_data = [
			];
			
			$save_si_bentuk_ks = $this->model_api_si_bentuk_ks->change($this->post('id'), $save_data);

			if ($save_si_bentuk_ks) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /si_bentuk_ks/delete Delete Si bentuk ks. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteSi bentuk ks
	 * @apiGroup si_bentuk_ks
	 * @apiHeader {String} X-Api-Key Si bentuk kss unique access-key.
	 	 * @apiPermission Si bentuk ks Cant be Accessed permission name : api_si_bentuk_ks_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Si bentuk kss .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_si_bentuk_ks_delete', false);

		$si_bentuk_ks = $this->model_api_si_bentuk_ks->find($this->post('id'));

		if (!$si_bentuk_ks) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Si bentuk ks not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_si_bentuk_ks->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Si bentuk ks deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Si bentuk ks not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
}

/* End of file Si bentuk ks.php */
/* Location: ./application/controllers/api/Si bentuk ks.php */