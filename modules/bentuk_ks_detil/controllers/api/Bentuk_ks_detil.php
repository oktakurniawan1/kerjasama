<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Bentuk_ks_detil extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_bentuk_ks_detil');
	}

	/**
	 * @api {get} /bentuk_ks_detil/all Get all bentuk_ks_detils.
	 * @apiVersion 0.1.0
	 * @apiName AllBentukksdetil 
	 * @apiGroup bentuk_ks_detil
	 * @apiHeader {String} X-Api-Key Bentuk ks detils unique access-key.
	 * @apiPermission Bentuk ks detil Cant be Accessed permission name : api_bentuk_ks_detil_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Bentuk ks detils.
	 * @apiParam {String} [Field="All Field"] Optional field of Bentuk ks detils : id, id_moa, id_bentuk_ks, deskripsi, nilai.
	 * @apiParam {String} [Start=0] Optional start index of Bentuk ks detils.
	 * @apiParam {String} [Limit=10] Optional limit data of Bentuk ks detils.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of bentuk_ks_detil.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataBentuk ks detil Bentuk ks detil data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_bentuk_ks_detil_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'id_moa', 'id_bentuk_ks', 'deskripsi', 'nilai'];
		$bentuk_ks_detils = $this->model_api_bentuk_ks_detil->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_bentuk_ks_detil->count_all($filter, $field);
		$bentuk_ks_detils = array_map(function($row){
						
			return $row;
		}, $bentuk_ks_detils);

		$data['bentuk_ks_detil'] = $bentuk_ks_detils;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Bentuk ks detil',
			'data'	 	=> $data,
			'total' 	=> $total,
		], API::HTTP_OK);
	}

		/**
	 * @api {get} /bentuk_ks_detil/detail Detail Bentuk ks detil.
	 * @apiVersion 0.1.0
	 * @apiName DetailBentuk ks detil
	 * @apiGroup bentuk_ks_detil
	 * @apiHeader {String} X-Api-Key Bentuk ks detils unique access-key.
	 * @apiPermission Bentuk ks detil Cant be Accessed permission name : api_bentuk_ks_detil_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Bentuk ks detils.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of bentuk_ks_detil.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError Bentuk ks detilNotFound Bentuk ks detil data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_bentuk_ks_detil_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'id_moa', 'id_bentuk_ks', 'deskripsi', 'nilai'];
		$bentuk_ks_detil = $this->model_api_bentuk_ks_detil->find($id, $select_field);

		if (!$bentuk_ks_detil) {
			$this->response([
					'status' 	=> false,
					'message' 	=> 'Blog not found'
				], API::HTTP_NOT_FOUND);
		}

					
		$data['bentuk_ks_detil'] = $bentuk_ks_detil;
		if ($data['bentuk_ks_detil']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Bentuk ks detil',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Bentuk ks detil not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /bentuk_ks_detil/add Add Bentuk ks detil.
	 * @apiVersion 0.1.0
	 * @apiName AddBentuk ks detil
	 * @apiGroup bentuk_ks_detil
	 * @apiHeader {String} X-Api-Key Bentuk ks detils unique access-key.
	 * @apiPermission Bentuk ks detil Cant be Accessed permission name : api_bentuk_ks_detil_add
	 *
 	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_bentuk_ks_detil_add', false);

		
		if ($this->form_validation->run()) {

			$save_data = [
			];
			
			$save_bentuk_ks_detil = $this->model_api_bentuk_ks_detil->store($save_data);

			if ($save_bentuk_ks_detil) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /bentuk_ks_detil/update Update Bentuk ks detil.
	 * @apiVersion 0.1.0
	 * @apiName UpdateBentuk ks detil
	 * @apiGroup bentuk_ks_detil
	 * @apiHeader {String} X-Api-Key Bentuk ks detils unique access-key.
	 * @apiPermission Bentuk ks detil Cant be Accessed permission name : api_bentuk_ks_detil_update
	 *
	 * @apiParam {Integer} id Mandatory id of Bentuk Ks Detil.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_bentuk_ks_detil_update', false);

		
		
		if ($this->form_validation->run()) {

			$save_data = [
			];
			
			$save_bentuk_ks_detil = $this->model_api_bentuk_ks_detil->change($this->post('id'), $save_data);

			if ($save_bentuk_ks_detil) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /bentuk_ks_detil/delete Delete Bentuk ks detil. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteBentuk ks detil
	 * @apiGroup bentuk_ks_detil
	 * @apiHeader {String} X-Api-Key Bentuk ks detils unique access-key.
	 	 * @apiPermission Bentuk ks detil Cant be Accessed permission name : api_bentuk_ks_detil_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Bentuk ks detils .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_bentuk_ks_detil_delete', false);

		$bentuk_ks_detil = $this->model_api_bentuk_ks_detil->find($this->post('id'));

		if (!$bentuk_ks_detil) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Bentuk ks detil not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_bentuk_ks_detil->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Bentuk ks detil deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Bentuk ks detil not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
}

/* End of file Bentuk ks detil.php */
/* Location: ./application/controllers/api/Bentuk ks detil.php */