<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['dosen_inbound6'] = 'Dosen Penguji';
$lang['scope'] = 'Scope';
$lang['aktivitas'] = 'Aktivitas';
$lang['judul_kegiatan'] = 'Judul Thesis/Disertasi';
$lang['tgl_mulai'] = 'Tanggal Mulai';
$lang['tgl_selesai'] = 'Tanggal Selesai';
$lang['nama_dosin'] = 'Nama Dosen';
$lang['nidn'] = 'NIDN';
$lang['nomor_identitas'] = 'NRP/NIP';
$lang['institusi_asal'] = 'Institusi Asal';
$lang['negara'] = 'Negara';
$lang['deskripsi'] = 'Deskripsi';
