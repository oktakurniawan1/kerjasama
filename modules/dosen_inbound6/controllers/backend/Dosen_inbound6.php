<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Dosen Inbound6 Controller
 *| --------------------------------------------------------------------------
 *| Dosen Inbound6 site
 *|
 */
class Dosen_inbound6 extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_dosen_inbound6');
		$this->lang->load('web_lang', $this->current_lang);
	}

	public function add()
	{
		$this->is_allowed('dosen_inbound6_add');
		$this->data['filter_prodi'] = get_filter_prodi();
		$this->template->title('Dosen Inbound6 New');
		$this->render('backend/standart/administrator/dosen_inbound6/dosen_inbound6_add', $this->data);
	}

	/**
	 * Add New Dosen Inbound6s
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('dosen_inbound6_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('scope', 'Scope', 'trim|required');
		$this->form_validation->set_rules('keg_dosin_id', 'Jenis Kegiatan', 'trim|required');
		$this->form_validation->set_rules('kd_prodi', 'Program Studi/Unit', 'trim|required');
		$this->form_validation->set_rules('nama_dosin', 'Nama Dosen', 'trim|required');
		$this->form_validation->set_rules('nidn', 'NIDN', 'trim|required');
		$this->form_validation->set_rules('nomor_identitas', 'NRP/NIP', 'trim|required');
		$this->form_validation->set_rules('institusi_asal', 'Institusi Asal', 'trim|required');
		$this->form_validation->set_rules('negara', 'Negara', 'trim|required');


		if ($this->form_validation->run()) {

			$save_data = [
				'scope'           => $this->input->post('scope'),
				'kd_prodi'        => $this->input->post('kd_prodi'),
				'keg_dosin_id'    => $this->input->post('keg_dosin_id'),
				'tgl_mulai'       => $this->input->post('tgl_mulai'),
				'tgl_selesai'     => $this->input->post('tgl_selesai'),
				'nama_dosin'      => $this->input->post('nama_dosin'),
				'nidn'            => $this->input->post('nidn'),
				'nomor_identitas' => $this->input->post('nomor_identitas'),
				'institusi_asal'  => $this->input->post('institusi_asal'),	
				'negara'          => $this->input->post('negara'),
				'deskripsi'       => $this->input->post('deskripsi'),
				'aktivitas'       => 'SA',
			];


			$save_dosen_inbound6 = $this->model_dosen_inbound6->store($save_data);
			$save_dosen_inbound6 = true;


			if ($save_dosen_inbound6) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_dosen_inbound6;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/dosen_inbound1/edit/' . $save_dosen_inbound6, 'Edit Dosen Inbound6'),
						anchor('administrator/dosen_inbound1', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/dosen_inbound1/edit/' . $save_dosen_inbound6, 'Edit Dosen Inbound6')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dosen_inbound1');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dosen_inbound1');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Dosen Inbound6s
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('dosen_inbound6_update');

		if (is_groups_in(['Kaprodi', 'Unit'])) {
			$prodi = get_filter_prodi(true);
			$row = $this->model_dosen_inbound2->find($id);
			if (!in_array($row->kd_prodi, $prodi)) {
				set_message(cclang('sorry_you_do_not_have_permission_to_access', 'dosen_inbound1'), 'error');
				redirect_back();
			}
			/**tolak jika sudah divalidasi */
			if ($row->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa mengubah data, karena data sudah divalidasi', 'dosen_inbound1'), 'warning');
				redirect_back();
			}
		}

		if (is_groups_in(['Kaprodi', 'Unit'])) {
			$prodi = get_filter_prodi(true);
			$row = $this->model_dosen_inbound6->find($id);
			if (!in_array($row->kd_prodi, $prodi)) {
				set_message(cclang('sorry_you_do_not_have_permission_to_access', 'dosen_inbound1'), 'error');
				redirect_back();
			}
			/**tolak jika sudah divalidasi */
			if ($row->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa mengubah data, karena data sudah divalidasi', 'dosen_inbound1'), 'warning');
				redirect_back();
			}
		}

		$this->data['dosen_inbound6'] = $this->model_dosen_inbound6->find($id);
		$this->data['filter_prodi'] = get_filter_prodi();
		$this->template->title('Dosen Penguji Update');
		$this->render('backend/standart/administrator/dosen_inbound6/dosen_inbound6_update', $this->data);
	}

	/**
	 * Update Dosen Inbound6s
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('dosen_inbound6_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('scope', 'Scope', 'trim|required');
		$this->form_validation->set_rules('keg_dosin_id', 'Jenis Kegiatan', 'trim|required');
		$this->form_validation->set_rules('nama_dosin', 'Nama Dosen', 'trim|required');
		$this->form_validation->set_rules('nidn', 'NIDN', 'trim|required');
		$this->form_validation->set_rules('nomor_identitas', 'NRP/NIP', 'trim|required');
		$this->form_validation->set_rules('institusi_asal', 'Institusi Asal', 'trim|required');
		$this->form_validation->set_rules('negara', 'Negara', 'trim|required');

		if ($this->form_validation->run()) {

			$save_data = [
				'scope'           => $this->input->post('scope'),
				'kd_prodi'        => $this->input->post('kd_prodi'),
				'keg_dosin_id'    => $this->input->post('keg_dosin_id'),
				'judul_kegiatan'  => $this->input->post('judul_kegiatan'),
				'tgl_mulai'       => $this->input->post('tgl_mulai'),
				'tgl_selesai'     => $this->input->post('tgl_selesai'),
				'nama_dosin'      => $this->input->post('nama_dosin'),
				'nidn'            => $this->input->post('nidn'),
				'nomor_identitas' => $this->input->post('nomor_identitas'),
				'institusi_asal'  => $this->input->post('institusi_asal'),
				'negara'          => $this->input->post('negara'),
				'deskripsi'       => $this->input->post('deskripsi'),
				'validasi'        => $this->input->post('validasi'),
			];


			$save_dosen_inbound6 = $this->model_dosen_inbound6->change($id, $save_data);

			if ($save_dosen_inbound6) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/dosen_inbound1', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dosen_inbound1/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dosen_inbound1/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}
		echo json_encode($this->data);
	}
}


/* End of file dosen_inbound6.php */
/* Location: ./application/controllers/administrator/Dosen Inbound6.php */