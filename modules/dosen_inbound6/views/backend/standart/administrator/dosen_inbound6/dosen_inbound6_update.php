<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Dosen Inbound6 <small>Edit Dosen Penguji</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/dosen_inbound6'); ?>">Dosen Penguji</a></li>
		<li class="active">Edit</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Dosen Penguji</h3>
							<h5 class="widget-user-desc">Edit Dosen Penguji</h5>
							<hr>
						</div>
						<?= form_open(base_url('administrator/dosen_inbound6/edit_save/' . $this->uri->segment(4)), [
							'name'    => 'form_dosen_inbound6',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_dosen_inbound6',
							'method'  => 'POST'
						]); ?>

						<div class="form-group ">
							<label for="scope" class="col-sm-2 control-label">Scope
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select" name="scope" id="scope" data-placeholder="Select Scope">
									<option value=""></option>
									<option <?= $dosen_inbound6->scope == "DN" ? 'selected' : ''; ?> value="DN">DN</option>
									<option <?= $dosen_inbound6->scope == "LN" ? 'selected' : ''; ?> value="LN">LN</option>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="kd_prodi" class="col-sm-2 control-label">Program Studi/Unit
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="kd_prodi" id="kd_prodi" data-placeholder="Select Program Studi/Unit">
									<option value=""></option>
									<?php foreach ($filter_prodi as $row) : ?>
										<option <?= $row->kode ==  $dosen_inbound6->kd_prodi ? 'selected' : ''; ?> value="<?= $row->kode ?>"><?= $row->kode .' - '.$row->nama_prodi; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="keg_dosin_id" class="col-sm-2 control-label">Jenis Kegiatan
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="keg_dosin_id" id="keg_dosin_id" data-placeholder="Select Jenis Kegiatan">
									<option value=""></option>
									<?php foreach (db_get_all_data('dosin_keg', ['id' => '10']) as $row) : ?>
										<option <?= $row->id ==  $dosen_inbound6->keg_dosin_id ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->kegiatan; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_mulai" class="col-sm-2 control-label">Tanggal Mulai
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-8">
									<input type="text" class="form-control pull-right datepicker" name="tgl_mulai" placeholder="Tanggal Mulai" id="tgl_mulai" value="<?= set_value('dosen_inbound6_tgl_mulai_name', $dosen_inbound6->tgl_mulai); ?>">
								</div>
								<small class="info help-block">
								</small>
							</div>
						</div>


						<div class="form-group ">
							<label for="tgl_selesai" class="col-sm-2 control-label">Tanggal Selesai
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-8">
									<input type="text" class="form-control pull-right datepicker" name="tgl_selesai" placeholder="Tanggal Selesai" id="tgl_selesai" value="<?= set_value('dosen_inbound6_tgl_selesai_name', $dosen_inbound6->tgl_selesai); ?>">
								</div>
								<small class="info help-block">
								</small>
							</div>
						</div>


						<div class="form-group ">
							<label for="nama_dosin" class="col-sm-2 control-label">Nama Dosen
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nama_dosin" id="nama_dosin" placeholder="Nama Dosen" value="<?= set_value('nama_dosin', $dosen_inbound6->nama_dosin); ?>">
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nidn" class="col-sm-2 control-label">NIDN
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nidn" id="nidn" placeholder="NIDN" value="<?= set_value('nidn', $dosen_inbound6->nidn); ?>">
								<small class="info help-block">
									isikan <code>-</code> jika tidak memiliki NIDN
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nomor_identitas" class="col-sm-2 control-label">NRP/NIP
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nomor_identitas" id="nomor_identitas" placeholder="NRP/NIP" value="<?= set_value('nomor_identitas', $dosen_inbound6->nomor_identitas); ?>">
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="institusi_asal" class="col-sm-2 control-label">Institusi Asal
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="institusi_asal" id="institusi_asal" placeholder="Institusi Asal" value="<?= set_value('institusi_asal', $dosen_inbound6->institusi_asal); ?>">
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="negara" class="col-sm-2 control-label">Negara
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="negara" id="negara" data-placeholder="Select Negara">
									<option value=""></option>
									<?php foreach (db_get_all_data('negara') as $row) : ?>
										<option <?= $row->nama_negara ==  $dosen_inbound6->negara ? 'selected' : ''; ?> value="<?= $row->nama_negara ?>"><?= $row->nama_negara; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="deskripsi" class="col-sm-2 control-label">Deskripsi
							</label>
							<div class="col-sm-8">
								<textarea id="deskripsi" name="deskripsi" rows="5" class="textarea form-control"><?= set_value('deskripsi', $dosen_inbound6->deskripsi); ?></textarea>
								<small class="info help-block">
									Judul thesis/disertasi
								</small>
							</div>
						</div>

						<?php if (is_groups_in(['Admin', 'Kerjasama'])) : ?>
							<div class="form-group ">
								<label for="validasi" class="col-sm-2 control-label">Validasi
								</label>
								<div class="col-sm-8">
									<select class="form-control chosen chosen-select" name="validasi" id="validasi" data-placeholder="Select Validasi">
										<option value=""></option>
										<option <?= $dosen_inbound6->validasi == "Y" ? 'selected' : ''; ?> value="Y">Valid</option>
										<option <?= $dosen_inbound6->validasi == "T" ? 'selected' : ''; ?> value="T">Invalid</option>
									</select>
									<small class="info help-block">
									</small>
								</div>
							</div>
						<?php endif ?>

						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	$(document).ready(function() {



		$('#btn_cancel').click(function() {
			swal({
					title: "Are you sure?",
					text: "the data that you have created will be in the exhaust!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/dosen_inbound1/index/' + "<?= $this->session->userdata('_page') ?>";
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();

			var form_dosen_inbound6 = $('#form_dosen_inbound6');
			var data_post = form_dosen_inbound6.serializeArray();
			var save_type = $(this).attr('data-stype');
			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: form_dosen_inbound6.attr('action'),
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('form').find('.error-input').remove();
					$('.steps li').removeClass('error');
					if (res.success) {
						var id = $('#dosen_inbound6_image_galery').find('li').attr('qq-file-id');
						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						$('.data_file_uuid').val('');

					} else {
						if (res.errors) {
							parseErrorField(res.errors);
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/
		async function chain() {}
		chain();
	}); /*end doc ready*/
</script>