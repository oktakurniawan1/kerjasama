<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 3/1/2018
 * Time: 3:15 AM
 */

class Demo_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->db->reconnect();
        $this->table = "sales";
    }

    public function get_sales(){
        $query = $this->db->get($this->table);

        return ($query->num_rows() > 0)?$query->result_array():FALSE;

    }

}