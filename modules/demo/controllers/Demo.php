<?php
/**
 * @property Stats $stats
 * @property Functions $functions
 * @property Files $files
 * @property Analytics_model $analytics_model
 * */

if (!defined('BASEPATH')) exit('No direct script access allowed');

Class Demo extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('demo_model');
        $this->load->driver('charts');
    }

    function index(){

        $chart_data['chart_data_name'] = "Sales";
        $chart_data['x-axis_title'] = "Tesla";
        $chart_data['y-axis_title'] = "Sales";

        $products = array();
        $values = array();
        $pie_data = array();

        $sales = $this->demo_model->get_sales();

        foreach($sales as $sale){
            array_push($products, $sale['products']);
            array_push($values, $sale['sales']);
            array_push($pie_data, array("name" => $sale['products'], "value" => $sale['sales']));
        }

        $chart_data['x-data'] = $products;
        $chart_data['y-data'] = $values;

        //$data['sales_chart'] = $this->charts->line->show_chart($chart_data);

        $data['sales_bar_chart'] = $this->bar_chart($chart_data);
        $data['sales_line_chart'] = $this->line_chart($chart_data);
        $data['sales_pie_chart'] = $this->pie_chart($pie_data,$products);
        $data['sales_multiple_chart'] = $this->multiple_chart($chart_data);

        $this->load->view('demo',$data);
    }


    function pie_chart($data,$items){
        $chart_data['chart_data_name'] = "Sales";
        $chart_data['p-data'] = $data;
        $chart_data['legend'] = $items;
        $chart_data['div_id'] = "pie-sales";

        return $this->charts->pie->show_chart($chart_data);
    }

    function bar_chart($chart_data){
        $chart_data['div_id'] = "bar-sales";
        return $this->charts->bar->show_chart($chart_data);
    }

    function line_chart($chart_data){
        $chart_data['div_id'] = "line-sales";
        return $this->charts->line->show_chart($chart_data);
    }

    function multiple_chart($chart_data){
        $chart_data['div_id'] = "multiple-sales";
        $chart_data['multiple'] = "multiple";
        $products = array();
        $values = array();

        $chart = $this->charts->bar->show_chart($chart_data);

        $sales = $this->demo_model->get_sales();

        foreach($sales as $sale){
            array_push($products, $sale['products']." forecast");
            array_push($values, $sale['forecast']);
        }

        $chart_data['x-data'] = $products;
        $chart_data['y-data'] = $values;
        $chart_data['y-axis_title'] = "Forecast";
        $chart_data['chart_data_name'] = "Sales zForecast";

        $chart =  $chart . $this->charts->line->show_chart($chart_data);

        return $chart;
    }
}
