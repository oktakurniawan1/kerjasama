<?php echo $this->load->view('header') ?>
<div class="container">
    <div class="row">
            <h1>Codeigniter Charts with Database Demo</h1>

            <p>Codeigniter Charts is a simple charting script that helps developers create charts using the codeigniter framework quickly. It's a wrapper for echarts js.</p>
    </div>

    <div class="col-12">
        <h4>Bar Chart Demo</h4>
        <div class="p-t-30" >One can easily create a bar chart by using the code below in the controller.</div>
        <pre>
            <code class="php">
                $chart_data['div_id'] = "bar-sales";
                return $this->charts->bar->show_chart($chart_data);
            </code>
        </pre>

        <div class="card">
            <div class="card-header"> <h4>Example Bar Chart<h4> </div>
            <?=$sales_bar_chart?>
        </div>

    </div>

    <div class="col-12">
        <h4>Line Chart Demo</h4>
        <div class="p-t-30" >One can easily create a line chart by using the code below in the controller.</div>
        <pre>
            <code class="php">
                $chart_data['div_id'] = "bar-sales";
                return $this->charts->line->show_chart($chart_data);
            </code>
        </pre>

        <div class="card">
            <div class="card-header"> <h4>Example Line Chart<h4> </div>
            <?=$sales_line_chart?>
        </div>

    </div>

    <div class="col-12">
        <h4>Pie Chart Demo</h4>
        <div class="p-t-30" >One can easily create a pie chart by using the code below in the controller.</div>
        <pre>
            <code class="php">
                $chart_data['chart_data_name'] = "Sales";
                $chart_data['p-data'] = $data;
                $chart_data['legend'] = $items;
                $chart_data['div_id'] = "pie-sales";

                return $this->charts->pie->show_chart($chart_data);
            </code>
        </pre>

        <div class="card">
            <div class="card-header"> <h4>Example Pie Chart<h4> </div>
            <?=$sales_pie_chart?>
        </div>

    </div>

    <div class="col-12">
        <h4>Multiple Chart Demo</h4>
        <div class="p-t-30" >One can easily create a bar and line chart by using the code below in the controller. This will require abit more code</div>
        <pre>
            <code class="php">
                $chart_data['div_id'] = "multiple-sales";
                $chart_data['multiple'] = "multiple";
                $products = array();
                $values = array();

                $chart = $this->charts->bar->show_chart($chart_data);
                ///get the data from the database for the second graph
                $sales = $this->demo_model->get_sales();

                foreach($sales as $sale){
                array_push($products, $sale['products']);
                array_push($values, $sale['alt_sales']);
                }

                $chart_data['x-data'] = $products;
                $chart_data['y-data'] = $values;

                $chart =  $chart . $this->charts->line->show_chart($chart_data);

                return $chart;
            </code>
        </pre>

        <div class="card">
            <div class="card-header"> <h4>Example Multiple Chart<h4> </div>
            <?=$sales_multiple_chart?>
        </div>

    </div>


</div>

<?php echo $this->load->view('footer') ?>