<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_rekap extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->thn_a = $this->session->userdata('thn_a');
		$this->thn_b = $this->session->userdata('thn_b');
	}
	public function getRekapSipp($tahun)
	{
		$sql = "SELECT t.thn_pelaksanaan,o.output_name,count(t.nip) as jml,s.output_ks
               FROM tridarma t
                  INNER JOIN (skim_sipp s,output_sipp o) ON ( t.kategori_kegiatan = s.id_skim and s.output_ks = o.id and s.used = 'Y')
               WHERE t.used != 'N'
               GROUP BY t.thn_pelaksanaan,o.id 
               ORDER BY t.thn_pelaksanaan,o.id";
		$res = $this->db->query($sql)->result();
		$data = [];
		foreach ($res as $row) {
			$data[$row->thn_pelaksanaan][$row->output_name] = array($row->jml, $row->output_ks);
		}
		$output = $this->db->order_by('id', 'ASC')->get('output_sipp')->result();
		$sipp = array();
		$i = 0;
		foreach ($output as $o) {
			$sipp[$i]['name'] = $o->output_name;
			foreach ($tahun as $t) {
				$jml = (isset($data[$t][$o->output_name]) ? $data[$t][$o->output_name][0] : 0);
				$o_ks = (isset($data[$t][$o->output_name]) ? $data[$t][$o->output_name][1] : null);
				$sipp[$i]['data'][] = array(intval($jml), $o_ks);
			}
			$i++;
		}
		return $sipp;
	}
	public function get_sipp_by_tahun($tahun, $o_ks)
	{
		$sql = "SELECT t.nip,t.skim_kegiatan,t.judul
               FROM tridarma t
                  INNER JOIN (skim_sipp s,output_sipp o) ON ( t.kategori_kegiatan = s.id_skim and s.output_ks = o.id and s.used = 'Y' and o.id = $o_ks)
               WHERE t.used != 'N' AND t.thn_pelaksanaan = $tahun ";
		return $this->db->query($sql)->result_array();
	}
	/**
	 * Rekap MoU
	 * @return mixed 
	 */
	public function get_mou_group_by_negara()
	{
		return $this->db->from('mou_kerjasama')
			->select('negara')
			->select('sum(IF(DATEDIFF(tgl_selesai,CURDATE())>0,1,0)) as aktif')
			->select('sum(IF((DATEDIFF(tgl_selesai,CURDATE())<=0) OR (DAYNAME(tgl_selesai) is null),1,0)) as selesai')
			->where("tahun BETWEEN $this->thn_a AND $this->thn_b ", null, false)
			->group_by('negara')->get()->result();
	}
	public function get_mou_by_jenis($where = [])
	{
		$this->db->where($where);
		$res =  $this->db->from('mou_kerjasama as m')
			->select('j.jenis_ks,count(j.id) as jml,j.id as jid')
			->select('sum(IF(DATEDIFF(tgl_selesai,CURDATE())>0,1,0)) as aktif')
			->select('sum(IF((DATEDIFF(tgl_selesai,CURDATE())<=0) OR (DAYNAME(tgl_selesai) is null),1,0)) as selesai')
			->where("tahun BETWEEN $this->thn_a AND $this->thn_b ", null, false)
			->join('si_jenis_ks as j', 'm.jenis_ks=j.id')->group_by('j.id')->get()->result();

		$data = [];
		foreach ($res as $k => $row) {
			$a = new stdClass;
			$a->y = $row->jml;
			$a->name = $row->jenis_ks;
			if ($k == 0) {
				$a->sliced = TRUE;
				$a->selected = TRUE;
			}
			$data[] = $a;
		}
		return [$data, $res];
	}
	public function get_rekap_mou_where($where)
	{
		return $this->db->select('nama_mitra,tgl_mulai,tgl_selesai')
			->select('IF(DATEDIFF(tgl_selesai,CURDATE())>0,"aktif","selesai") as status')
			->where(" (tahun BETWEEN $this->thn_a AND $this->thn_b) ", null, false)
			->get_where('mou_kerjasama', $where)->result_array();
	}
	public function get_sebaran_pengguna_mou()
	{
		$sql = "SELECT 'unit' as pengguna,concat(m.nama_mitra,' - ',m.doc_nomor) as nama_mitra,m.id,count(a.id) as jml,
					sum(IF(DATEDIFF(a.tgl_selesai,CURDATE())>0,1,0)) as jml_aktif, 
					sum(IF(DATEDIFF(a.tgl_selesai,CURDATE())<0,1,0)) as jml_selesai 
				FROM mou_kerjasama m inner join si_kerjasama a on (m.id = a.mou_ks)
				WHERE a.tahun BETWEEN $this->thn_a AND $this->thn_b
				GROUP BY m.id UNION ALL
				
				SELECT 'dosen_inb' as pengguna,concat(m.nama_mitra,' - ',m.doc_nomor) as nama_mitra,m.id,count(a.id) as jml,
					sum(IF(DATEDIFF(a.tgl_selesai,CURDATE())>0,1,0)) as jml_aktif,
					sum(IF(DATEDIFF(a.tgl_selesai,CURDATE())<0,1,0)) as jml_selesai 
				FROM mou_kerjasama m inner join dosen_inbound a on (m.id = a.mou_ks)
				WHERE a.tahun BETWEEN $this->thn_a AND $this->thn_b
				GROUP BY m.id UNION ALL

				SELECT 'mbkm' as pengguna,concat(m.nama_mitra,' - ',m.doc_nomor) as nama_mitra,m.id,count(a.id) as jml,
					sum(IF(DATEDIFF(b.tgl_selesai,CURDATE())>0,1,0)) as jml_aktif,
					sum(IF(DATEDIFF(b.tgl_selesai,CURDATE())<0,1,0)) as jml_selesai 
				FROM mou_kerjasama m inner join (mbkm a,si_kerjasama b) on (m.id = a.mou_ks and a.unit_ks = b.id and a.validasi = 'Y')
				WHERE a.tahun BETWEEN $this->thn_a AND $this->thn_b
				GROUP BY m.id UNION ALL
				
				SELECT 'perjalun' as pengguna,concat(m.nama_mitra,' - ',m.doc_nomor) as nama_mitra,m.id,count(a.id) as jml,
					sum(IF(DATEDIFF(a.tgl_selesai,CURDATE())>0,1,0)) as jml_aktif,
					sum(IF(DATEDIFF(a.tgl_selesai,CURDATE())<0,1,0)) as jml_selesai
				FROM mou_kerjasama m inner join perjalun a on (m.id = a.mou_ks)
				WHERE a.tahun BETWEEN $this->thn_a AND $this->thn_b
				GROUP BY m.id";

		$data = $this->db->query($sql)->result();
		$mou = [];
		foreach ($data as $row) {
			$mou[$row->id]['name'] = $row->nama_mitra;
			$mou[$row->id][$row->pengguna] = ['jml_aktif' => $row->jml_aktif, 'jml_selesai' => $row->jml_selesai, 'jml' => $row->jml];
		}
		$kol = ['unit', 'dosen_inb', 'mbkm', 'perjalun'];
		$result = [];
		foreach ($mou as $id => $dt) {
			$tmp = ['mitra' => $dt['name'], 'id' => $id];
			foreach ($kol as $n) {
				if (isset($dt[$n])) $tmp['dt'][$n] = $dt[$n];
				else $tmp['dt'][$n] = ['jml_aktif' => 0, 'jml_selesai' => 0, 'jml' => 0];
			}
			$result[] = $tmp;
		}
		return $result;
	}
	/**
	 * REKAP UNIT
	 */
	public function get_rekap_unit($thn1 = null, $thn2 = null)
	{
		$result =  $this->db->from('si_kerjasama')
			->select('ref_prodi.nama_prodi,si_kerjasama.kd_prodi')
			->select('sum(IF(DATEDIFF(tgl_selesai,CURDATE())>0,1,0)) as aktif')
			->select('sum(IF((DATEDIFF(tgl_selesai,CURDATE())<=0) OR (DAYNAME(tgl_selesai) is null),1,0)) as selesai')
			->select("sum(if(scope='DN',1,0)) as dn")
			->select("sum(if(scope='LN',1,0)) as ln")
			->select("sum(if(mou_ks=1,1,0)) as no_mou")
			->select("sum(if(mou_ks!=1,1,0)) as with_mou")
			->join('ref_prodi', 'ref_prodi.kode = si_kerjasama.kd_prodi')
			//where('mou_ks !=','1')
			->where("tahun BETWEEN $this->thn_a AND $this->thn_b ", null, false)
			->group_by('si_kerjasama.kd_prodi')->get()->result();
		//2 series name = aktif,selesai
		$aktif['name'] = 'Aktif';
		$selesai['name'] = 'Selesai';
		$dn['name'] = 'DN';
		$ln['name'] = 'LN';
		$no_mou['name'] = 'Proses MoU';
		$with_mou['name'] = 'Dengan MoU';
		$categori = [];
		foreach ($result as $row) {
			$aktif['data'][] = $row->aktif;
			$selesai['data'][] = $row->selesai;
			$dn['data'][] = $row->dn;
			$ln['data'][] = $row->ln;
			$no_mou['data'][] = $row->no_mou;
			$with_mou['data'][] = $row->with_mou;
			$categori[] = $row->nama_prodi;
		}
		return array('chart' => [$aktif, $selesai, $dn, $ln, $no_mou, $with_mou, $categori], 'tabel' => $result);
	}

	public function get_rekap_unit_fakultas()
	{
		$result =  $this->db->from('si_kerjasama')
			->select('if(fakultas.kode is not null,fakultas.nama,"Unit/Lembaga") as nama_prodi')
			->select('ref_prodi.kode_fak')
			->select('sum(IF(DATEDIFF(tgl_selesai,CURDATE())>0,1,0)) as aktif')
			->select('sum(IF((DATEDIFF(tgl_selesai,CURDATE())<=0) OR (DAYNAME(tgl_selesai) is null),1,0)) as selesai')
			->select("sum(if(scope='DN',1,0)) as dn")
			->select("sum(if(scope='LN',1,0)) as ln")
			->select("sum(if(mou_ks=1,1,0)) as no_mou")
			->select("sum(if(mou_ks!=1,1,0)) as with_mou")
			->join('ref_prodi', 'ref_prodi.kode = si_kerjasama.kd_prodi')
			->join('fakultas', 'ref_prodi.kode_fak = fakultas.kode', 'left')
			//where('mou_ks !=','1')
			->where("tahun BETWEEN $this->thn_a AND $this->thn_b ", null, false)
			->group_by('ref_prodi.kode_fak')->order_by('fakultas.kode')->get()->result();
		//2 series name = aktif,selesai
		$aktif['name'] = 'Aktif';
		$selesai['name'] = 'Selesai';
		$dn['name'] = 'DN';
		$ln['name'] = 'LN';
		$no_mou['name'] = 'Proses MoU';
		$with_mou['name'] = 'Dengan MoU';
		$categori = [];
		foreach ($result as $row) {
			$aktif['data'][] = $row->aktif;
			$selesai['data'][] = $row->selesai;
			$dn['data'][] = $row->dn;
			$ln['data'][] = $row->ln;
			$no_mou['data'][] = $row->no_mou;
			$with_mou['data'][] = $row->with_mou;
			$categori[] = $row->nama_prodi;
		}
		return array('chart' => [$aktif, $selesai, $dn, $ln, $no_mou, $with_mou, $categori], 'tabel' => $result);
	}

	public function get_rekap_tahun()
	{
		$result =  $this->db->from('si_kerjasama')
			->select('fakultas.kode,fakultas.nama,tahun,count(si_kerjasama.id) as jml_moa')
			->join('ref_prodi', 'ref_prodi.kode = si_kerjasama.kd_prodi')
			->join('fakultas', 'ref_prodi.kode_fak = fakultas.kode', 'left')
			//where('mou_ks !=','1')
			->where("tahun BETWEEN $this->thn_a AND $this->thn_b ", null, false)
			->group_by('tahun,fakultas.kode')->get()->result();
	}

	public function get_unit_group_by_negara()
	{
		return $this->db->from('si_kerjasama')
			->select('if(negara  regexp"[[:alnum:]]+",negara,"Tidak Terdefinisi") as negara', false)
			->select('sum(IF(DATEDIFF(tgl_selesai,CURDATE())>0,1,0)) as aktif')
			->select('sum(IF((DATEDIFF(tgl_selesai,CURDATE())<=0) OR (DAYNAME(tgl_selesai) is null),1,0)) as selesai')
			->where("tahun BETWEEN $this->thn_a AND $this->thn_b ", null, false)
			->group_by('negara')->get()->result();
	}

	public function get_rekap_moa_where($where)
	{
		return $this->db->select('nama_mitra,tgl_mulai,tgl_selesai')
			->select('IF(DATEDIFF(tgl_selesai,CURDATE())>0,"aktif","selesai") as status') //->where('mou_ks !=','1')
			->join('ref_prodi', 'ref_prodi.kode = si_kerjasama.kd_prodi')
			->join('fakultas', 'ref_prodi.kode_fak = fakultas.kode', 'left')
			->where(" (tahun BETWEEN $this->thn_a AND $this->thn_b ) ", null, false)
			->get_where('si_kerjasama', $where)->result_array();
	}

	/**
	 * Student mobility
	 */
	public function get_studentmob_by_prodi($where)
	{
		return $this->db->select('nim,nama,k.nama_mitra,m.tgl_mulai,m.tgl_selesai')
			->select('IF(DATEDIFF(m.tgl_selesai,CURDATE())>0,"aktif","selesai") as status')
			->join('mou_kerjasama k', 'k.id = m.mou_ks', 'left')
			//->where('mou_ks !=', '1')->order_by('nim')
			->where(" (m.tahun BETWEEN $this->thn_a AND $this->thn_b) ", null, false)
			->get_where('mhs_mobility m', $where)->result_array();
	}

	public function get_rekap_student_mob()
	{
		$result =  $this->db->from('mhs_mobility')
			->select('ref_prodi.nama_prodi,mhs_mobility.kd_prodi')
			->select('sum(IF(DATEDIFF(tgl_selesai,CURDATE())>0,1,0)) as aktif')
			->select('sum(IF((DATEDIFF(tgl_selesai,CURDATE())<=0) OR (DAYNAME(tgl_selesai) is null),1,0)) as selesai')
			->select('count(DISTINCT nim) as jml_mhs')
			->select("sum(if(scope='DN',1,0)) as dn")
			->select("sum(if(scope='LN',1,0)) as ln")
			->select("sum(if(mou_ks=1,1,0)) as no_mou")
			->select("sum(if(mou_ks!=1,1,0)) as with_mou")
			->join('ref_prodi', 'ref_prodi.kode = mhs_mobility.kd_prodi')
			->where("tahun BETWEEN $this->thn_a AND $this->thn_b ", null, false)
			->group_by('mhs_mobility.kd_prodi')->get()->result(); //->where('mou_ks !=','1')
		//2 series name = aktif,selesai
		$aktif['name'] = 'Aktif';
		$aktif['tooltip'] = ['valueSuffix' => ' Keg'];
		$selesai['name'] = 'Selesai';
		$selesai['tooltip'] = ['valueSuffix' => ' Keg'];
		$jmlmhs['name'] = 'Mahasiswa';
		$jmlmhs['tooltip'] = ['valueSuffix' => ' Mhs'];
		$dn['name'] = 'DN';
		$dn['tooltip'] = ['valueSuffix' => ' Keg'];
		$ln['name'] = 'LN';
		$ln['tooltip'] = ['valueSuffix' => ' Keg'];
		$with_mou['name'] = 'Dengan MoU';
		$with_mou['tooltip'] = ['valueSuffix' => ' Keg'];
		$no_mou['name'] = 'Proses MoU';
		$no_mou['tooltip'] = ['valueSuffix' => ' Keg'];
		$categori = [];
		foreach ($result as $row) {
			$aktif['data'][] = $row->aktif;
			$selesai['data'][] = $row->selesai;
			$jmlmhs['data'][] = $row->jml_mhs;
			$categori[] = $row->nama_prodi;
			$dn['data'][] = $row->dn;
			$ln['data'][] = $row->ln;
			$with_mou['data'][] = $row->with_mou;
			$no_mou['data'][] = $row->no_mou;
		}
		return array('chart' => [$aktif, $selesai, $jmlmhs, $dn, $ln, $with_mou, $no_mou, $categori], 'tabel' => $result);
	}

	/**
	 * MBKM
	 */
	public function get_mbkm_by_fakultas()
	{
		return $this->db->select('f.nama_singkat,count(a.id) as jml')->from('mbkm a')
			->join('(ref_prodi p, fakultas f)', 'p.kode = a.kd_prodi and p.kode_fak = f.kode')->group_by('f.kode')->get()->result();
	}
}
