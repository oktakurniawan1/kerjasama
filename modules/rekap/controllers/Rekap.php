<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 *| --------------------------------------------------------------------------
 *| Rekap Controller
 *| --------------------------------------------------------------------------
 *| Rekap
 *|
 */

use Ghunti\HighchartsPHP\Highchart;
use Ghunti\HighchartsPHP\HighchartJsExpr;

class Rekap extends Admin
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_rekap');
		$this->load->library('table');

		$this->thn_a = $this->input->get('thn_a');
		$this->thn_b = $this->input->get('thn_b');
		if (!is_null($this->thn_a) && !is_null($this->thn_b)) {
			$this->session->set_userdata('thn_a', $this->thn_a);
			$this->session->set_userdata('thn_b', $this->thn_b);
		} else {
			if (!$this->session->has_userdata('thn_a')) $this->session->set_userdata('thn_a', 2015);
			if (!$this->session->has_userdata('thn_b')) $this->session->set_userdata('thn_b', date('Y'));
		}
	}
	public function index()
	{
	}
	public function rekap_sipp()
	{
		$this->template->title('Rekap');
		$this->data['date_sipp'] = (date('Y') - 10) . '/01/01 - ' . date('Y/m/d');
		$this->render('rekap_sipp', $this->data);
	}
	public function chart_sipp()
	{
		$date = $this->input->get('date_sipp');
		$date = array_map('trim', explode('-', $date));
		$thn1 = DateTime::createFromFormat('Y/m/d', $date[0])->format('Y');
		$thn2 = DateTime::createFromFormat('Y/m/d', $date[1])->format('Y');
		$tahun = range($thn1, $thn2);
		$chart = new Highchart();
		$chart->chart->renderTo = "chart-sipp";
		$chart->chart->type = "column";
		$chart->title->text = "Output SIPP Kerjasama";
		$chart->subtitle->text = "Sumber: sipp.unnes.ac.id : " . date('Y-m-d H:i:s');

		$chart->xAxis->categories = $tahun;
		$chart->yAxis->min = 0;
		$chart->yAxis->title->text = "Jumlah kegiatan";
		$chart->xAxis->title->text = "Tahun";
		$chart->legend->layout = "horizontal";
		$chart->legend->backgroundColor = "#FFFFFF";
		$chart->legend->align = "left";
		$chart->legend->verticalAlign = "top";
		$chart->legend->x = 100;
		$chart->legend->y = 50;
		$chart->legend->floating = 1;
		$chart->legend->shadow = 1;

		$chart->tooltip->formatter = new HighchartJsExpr("function() {
        return '' + this.x +': '+ this.y +' kegiatan';}");

		$chart->plotOptions->column->pointPadding = 0.1;
		$chart->plotOptions->column->borderWidth = 0;
		$sipp = $this->model_rekap->getRekapSipp($tahun);
		foreach ($sipp as $val) {
			$dt = [];
			foreach ($val['data'] as $d) {
				$dt[] = $d[0];
			}
			$chart->series[] = array(
				'name' => $val['name'],
				'data' => $dt
			);
		}
		$data['chart'] = $chart;
		$data['sipp'] = $sipp;
		$data['tahun'] = $tahun;
		echo  $this->load->view('_chart_sipp', $data, TRUE);
	}
	public function list_sipp_by_tahun()
	{
		$data = $this->model_rekap->get_sipp_by_tahun($this->input->get('tahun'), $this->input->get('o_ks'));
		$this->table->set_template(['table_open' => '<table class="table table-bordered table-responsive table-striped" id="table1">', 'table-close' => '</table>']);
		$this->table->set_heading('NIP', 'SKIM', 'Judul');
		echo $this->table->generate($data);
	}

	public function rekap_mou()
	{
		$this->data['thn_a'] = $this->session->userdata('thn_a');
		$this->data['thn_b'] = $this->session->userdata('thn_b');
		$thn = range(2015, date('Y'));
		$thns = array_combine($thn, $thn);
		$this->data['thns'] = $thns;
		$this->template->title('Rekap Mou');
		$this->data['sebaran'] = $this->model_rekap->get_sebaran_pengguna_mou();
		$this->render('rekap_mou', $this->data);
	}
	public function chart_mou()
	{
		$data['negara'] = $this->model_rekap->get_mou_group_by_negara();
		$dt = $this->model_rekap->get_mou_by_jenis();
		$data['piechart_mou_jenis'] = json_encode($dt[0], JSON_NUMERIC_CHECK);
		$data['table'] = $dt[1];
		echo $this->load->view('_chart_mou', $data, TRUE);
	}
	public function filter_mou_scope()
	{
		if ($this->input->get('scope') != 'ALL') $filter = ['scope' => $this->input->get('scope')];
		else $filter = [];
		$mou = $this->model_rekap->get_mou_by_jenis($filter);

		$this->table->set_template(['table_open' => '<table class="table table-bordered table-responsive table-striped">', 'table-close' => '</table>']);
		$this->table->set_heading('Jenis Kerjasama', 'Aktif', 'Selesai', 'Jml');
		$tb = [];
		foreach ($mou[1] as $row) {
			$tb[] = [
				'jenis_ks' => $row->jenis_ks, 'aktif' => $row->aktif, 'selesai' => $row->selesai,
				'jml' => "<a href='javascript:void(0)' onclick='tampil_mou_jenis(\"$row->jid\")'>{$row->jml}</a>"
			];
		}
		$table =  $this->table->generate($tb);

		$data['pie'] = $mou[0];
		$data['table'] = $table;
		echo json_encode($data, JSON_NUMERIC_CHECK);
	}
	public function list_mou_by_negara()
	{
		$data = $this->model_rekap->get_rekap_mou_where(['negara' => $this->input->get('negara')]);
		$this->load->library('table');
		$this->table->set_template(['table_open' => '<table class="table table-bordered table-striped" id="table1">', 'table-close' => '</table>']);
		$this->table->set_heading('Nama mitra', 'tgl mulai', 'tgl selesai', 'status');
		echo $this->table->generate($data);
	}
	public function list_mou_by_jenis()
	{
		$filter = ['jenis_ks' => $this->input->get('jid')];
		if ($this->input->get('scope') != 'ALL') $filter['scope'] = $this->input->get('scope');
		$data = $this->model_rekap->get_rekap_mou_where($filter);
		$this->load->library('table');
		$this->table->set_template(['table_open' => '<table class="table table-bordered table-striped" id="table2">', 'table-close' => '</table>']);
		$this->table->set_heading('Nama mitra', 'tgl mulai', 'tgl selesai', 'status');
		echo $this->table->generate($data);
	}

	/**
	 * UNIT
	 */
	public function rekap_unit()
	{
		$this->data['thn_a'] = $this->session->userdata('thn_a');
		$this->data['thn_b'] = $this->session->userdata('thn_b');
		$thn = range(2015, date('Y'));
		$thns = array_combine($thn, $thn);
		$this->data['thns'] = $thns;
		$this->template->title('Rekap Unit');
		$this->render('rekap_unit', $this->data);
	}
	public function chart_unit()
	{
		$rekap = $this->model_rekap->get_rekap_unit();
		$fakultas = $this->model_rekap->get_rekap_unit_fakultas();
		$data['negara'] = $this->model_rekap->get_unit_group_by_negara();
		$data['rekap_unit'] = json_encode($rekap['chart'], JSON_NUMERIC_CHECK);
		$data['tabel'] = $rekap['tabel'];
		$data['rekap_fakultas']  = json_encode($fakultas['chart'], JSON_NUMERIC_CHECK);
		$data['tabel_fakultas'] = $fakultas['tabel'];
		// $data['test'] = $test;
		echo $this->load->view('_chart_unit', $data, TRUE);
	}
	public function list_moa_by_negara()
	{
		$data = $this->model_rekap->get_rekap_moa_where(['negara' => $this->input->get('negara')]);
		$this->load->library('table');
		$this->table->set_template(['table_open' => '<table class="table table-bordered table-striped" id="table3">', 'table-close' => '</table>']);
		$this->table->set_heading('Nama mitra', 'tgl mulai', 'tgl selesai', 'status');
		echo $this->table->generate($data);
	}
	public function list_moa_by_unit()
	{
		$filter = ['kd_prodi' => $this->input->get('unit')];
		//if($this->input->get('scope')!='ALL') $filter['scope'] = $this->input->get('scope');
		$data = $this->model_rekap->get_rekap_moa_where($filter);
		$this->load->library('table');
		$this->table->set_template(['table_open' => '<table class="table table-bordered table-striped" id="table1">', 'table-close' => '</table>']);
		$this->table->set_heading('Nama mitra', 'tgl mulai', 'tgl selesai', 'status');
		echo $this->table->generate($data);
	}
	public function list_moa_by_fakultas()
	{
		$filter = ['kode_fak' => $this->input->get('fak')];
		//if($this->input->get('scope')!='ALL') $filter['scope'] = $this->input->get('scope');
		$data = $this->model_rekap->get_rekap_moa_where($filter);
		$this->load->library('table');
		$this->table->set_template(['table_open' => '<table class="table table-bordered table-striped" id="table1">', 'table-close' => '</table>']);
		$this->table->set_heading('Nama mitra', 'tgl mulai', 'tgl selesai', 'status');
		echo $this->table->generate($data);
	}
	/**
	 * 
	 * @return void rekap mahasiswa
	 */
	public function rekap_mhs()
	{
		$this->data['thn_a'] = $this->session->userdata('thn_a');
		$this->data['thn_b'] = $this->session->userdata('thn_b');
		$thn = range(2015, date('Y'));
		$thns = array_combine($thn, $thn);
		$this->data['thns'] = $thns;
		$this->template->title('Rekap Student Mobility');
		$this->render('rekap_mhs', $this->data);
	}
	public function chart_student_mob()
	{
		$rekap = $this->model_rekap->get_rekap_student_mob();
		$data['rekap'] = json_encode($rekap['chart'], JSON_NUMERIC_CHECK);
		$data['tabel'] = $rekap['tabel'];
		echo $this->load->view('_chart_mhs', $data, TRUE);
	}
	public function list_studentmob_by_prodi()
	{
		$filter = ['kd_prodi' => $this->input->get('unit')];
		$data = $this->model_rekap->get_studentmob_by_prodi($filter);
		$this->load->library('table');
		$this->table->set_template(['table_open' => '<table class="table table-bordered table-striped" id="table1">', 'table-close' => '</table>']);
		$this->table->set_heading('Nim', 'Nama', 'Nama Mitra (MoU)', 'tgl mulai', 'tgl selesai', 'status');
		echo $this->table->generate($data);
	}
	/**
	 * MBKM
	 */
	public function rekap_mbkm()
	{
		$this->data['thn_a'] = $this->session->userdata('thn_a');
		$this->data['thn_b'] = $this->session->userdata('thn_b');
		$thn = range(2015, date('Y'));
		$thns = array_combine($thn, $thn);
		$this->data['thns'] = $thns;
		$this->template->title('Rekap Merdeka Belajar');
		$this->render('rekap_mbkm', $this->data);
	}
	public function chart_mbkm()
	{
		$dt = $this->model_rekap->get_mbkm_by_fakultas();
		$data['piechart_mbkm'] = json_encode($dt, JSON_NUMERIC_CHECK);
		echo $this->load->view('_chart_mbkm', $data, TRUE);
	}
	/**
	 * IKU
	 */
	public function rekap_unit_iku(){
		$this->render('rekap-unit-iku');
	}
}
