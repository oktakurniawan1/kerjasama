<div class="row">
	<div class="col-md-5">
		<h4>Tabel Kegiatan Student Mobility</h4>
		<div class='table-responsive' style="padding:10px">
			<table class="table table-responsive table-striped table-bordered" id='tbmhs'>
				<thead>
					<tr>
						<th rowspan="2">Prodi/Unit</th>
						<th rowspan="2">Mhs</th>
						<th colspan="7" style="text-align: center;">Kegiatan</th>
					</tr>
					<tr>
						<th>Aktif</th>
						<th>Selesai</th>
						<th>Jml</th>
						<th>MoU</th>
						<th>Proses MoU</th>
						<th>DN</th>
						<th>LN</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$t1 = 0;
					$t2 = 0;
					$t3 = 0;
					$t4 = 0;
					$t5 = 0;
					$t6 = 0;
					$t7 = 0;
					foreach ($tabel as $row) {
						$jml = $row->aktif + $row->selesai;
						$t1 = $t1 + $row->aktif;
						$t2 = $t2 + $row->selesai;
						$t3 = $t3 + $row->jml_mhs;
						$t4 = $t4 + $row->with_mou;
						$t5 = $t5 + $row->no_mou;
						$t6 = $t6 + $row->dn;
						$t7 = $t7 + $row->ln;
						echo "<tr>
						<td>{$row->nama_prodi}</td>
						<td>{$row->jml_mhs}</td>
						<td>{$row->aktif}</td>
						<td>{$row->selesai}</td>
						<td><b><a href='javascript:void(0)' onclick='tampil_rekap(\"$row->kd_prodi\",\"$row->nama_prodi\")'>{$jml}</a></b></td>
						<td>{$row->with_mou}</td>
						<td>{$row->no_mou}</td>
						<td>{$row->dn}</td>
						<td>{$row->ln}</td>
					</tr>";
					}
					?>
				</tbody>
				<tfoot>
					<tr>
						<td><b>Total</b></td>
						<td><?= $t3 ?></td>
						<td><?= $t1 ?></td>
						<td><?= $t2 ?></td>
						<td><b><?= $t2+$t1 ?></b></td>
						<td><?= $t4 ?></td>
						<td><?= $t5 ?></td>
						<td><?= $t6 ?></td>
						<td><?= $t7 ?></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<div class="col-md-7">
		<div id='div-student-mob' style="padding-right: 7px;"></div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#tbmhs').DataTable({
			dom: "<'row'<'col-sm-6'B><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'i><'col-sm-7'<'pull-right'p>>>",
			buttons: [
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5'
			],
			lengthChange: false,
			pagingType:'simple',
		})
		var data = <?= $rekap?>;
		var chart_student_mob = {
			chart: {
				renderTo: 'div-student-mob',
				type: 'bar',
				height: 85 + '%'
			},
			title: {
				text: 'Chart kerjasama kegiatan student mobility'
			},
			subtitle: {
				text: 'Source: <a href="simkerjasama.unnes.ac.id">simkerjasama.unnes.ac.id pada <?=date('Y-m-d H:i:s')?></a>'
			},
			xAxis: {
				categories: data[7],
				title: {
					text: null
				}
			},
			yAxis: {
				min: 0,
				allowDecimals:false,
				title: {
					text: 'Jumlah Kegiatan (Keg)',
					align: 'high'
				},
				labels: {
					overflow: 'justify'
				}
			},
			tooltip: {
				//valueSuffix: ' keg'
				crosshairs: true,
				split: true
			},
			plotOptions: {
				series :{
					pointWidth:10,
					pointPadding: 0,
					groupPadding: 0,
				},
				bar: {
					dataLabels: {
						enabled: true
					}
				}
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'top',
				x: -40,
				y: 80,
				floating: true,
				borderWidth: 1,
				backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
				shadow: true
			},
			credits: {
				enabled: false
			},
			series: [
				data[0],data[1],data[5],data[6]
			],
		}
		// memanggil data dengan jquery
		/*
		$.getJSON("https://laporankerma.kemdikbud.go.id/rest-api/dashboard/aktif-mou", function(json) {
			universitas_aktif.series[0].data = json;
			chart = new Highcharts.Chart(universitas_aktif);
		});
		*/
		chart = new Highcharts.Chart(chart_student_mob);
	})
</script>