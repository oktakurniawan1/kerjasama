<div class="row">
	<div class="col-md-4">
		<h4>Tabel Rekap MoU Kerjasama Berdasarkan Negara</h4>
		<div class='table-responsive' style="padding:10px">
			<table class="table table-responsive table-striped table-bordered" id='mou-negara'>
				<thead>
					<tr>
						<th>Negara</th>
						<th>Aktif</th>
						<th>Selesai</th>
						<th>Jml</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$t1 = 0;
					$t2 = 0;
					$t3 = 0;
					foreach ($negara as $row) {
						$jml = $row->aktif + $row->selesai;
						$t1 = $t1 + $row->aktif;
						$t2 = $t2 + $row->selesai;
						$t3 = $t3 + $jml;
						echo "<tr>
								<td>{$row->negara}</td>
								<td>{$row->aktif}</td>
								<td>{$row->selesai}</td>
								<td><a href='javascript:void(0)' onclick='tampil_mou(\"$row->negara\")'>{$jml}</a></td>
						</tr>";
					}
					?>
				</tbody>
				<tfoot>
					<tr>
						<td><b>Total</b></td>
						<td><?= $t1 ?></td>
						<td><?= $t2 ?></td>
						<td><?= $t3 ?></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<div class="col-md-8">
		<div id='div-mou-negara' style="padding-right: 7px;"></div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-4">
		<h4>Tabel rekap MoU kerjasama berdasarkan jenis kerjasama</h4>
		<div class='table-responsive' style="padding:10px">
			<div class="form-group">
				<label for="pwd">Filter DN/LN :</label>
				<select name="filter-scope" id="filter-scope" class="form-control" required="required">
					<option value="ALL" selected>Semua</option>
					<option value="DN">Dalam Negeri</option>
					<option value="LN">Luar Negeri</option>
				</select>
			</div>
			<div id='div-table-mou-jenis'>
				<table class="table table-responsive table-striped table-bordered">
					<thead>
						<tr>
							<th>Jenis Kerjasama</th>
							<th>Aktif</th>
							<th>Selesai</th>
							<th>Jml</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($table as $row) {
							echo "<tr>
									<td>{$row->jenis_ks}</td>
									<td>{$row->aktif}</td>
									<td>{$row->selesai}</td>
									<td><a href='javascript:void(0)' onclick='tampil_mou_jenis(\"$row->jid\")'>{$row->jml}</a></td>
								</tr>";
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div id='div-mou-jenis'></div>
	</div>
</div>

<script type="text/javascript">
	var pie;
	$(function() {
		// by negara
		Highcharts.chart('div-mou-negara', {
			data: {
				table: 'mou-negara',
				endColumn: 2,
				endRow: $('#mou-negara tr').length - 2,
			},
			chart: {
				type: 'column',
			},
			colors: ['#058dc7', '#ff9655'],
			title: {
				text: 'Chart MoU Kerjasama Berdasarkan Negara'
			},
			yAxis: {
				allowDecimals: false,
				title: {
					text: 'Jumlah dokumen'
				},
				stackLabels: { // tampilkan jml di bar
					enabled: true
				}
			},
			plotOptions: {
				column: {
					stacking: 'normal', // menampilkan jml di bar
					pointPadding: 0.1,
					borderWidth: 0,
					dataLabels: {
						enabled: true,
						formatter: function() { //skip nilai 0
							if (this.y) {
								return this.y;
							}
						}
					},
				},
				stacking: 'normal'
			},
			tooltip: {
				formatter: function() {
					return '<b>' + this.series.name + '</b><br/>' +
						this.point.y + ' ' + this.point.name.toLowerCase();
				}
			}
		});
		// by jenis
		pie = new Highcharts.chart('div-mou-jenis', {
			chart: {
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false,
				height: 55 + '%',
				type: 'pie'
			},
			title: {
				text: 'Chart MoU Berdasarkan Jenis Kerjasama',
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b> | {point.y}'
			},
			accessibility: {
				point: {
					valueSuffix: '%'
				}
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						//alignTo: 'plotEdges',
						format: '<b>{point.name}</b>: {point.percentage:.1f} % | {point.y}'
					},
					showInLegend: true
				}
			},
			series: [{
				name: 'Dokumen',
				colorByPoint: true,
				data: <?php echo $piechart_mou_jenis ?>
			}]
		});
	})
	$('#mou-negara').DataTable({
		dom: "<'row'<'col-sm-5'B><'col-sm-7'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'i><'col-sm-7'<'pull-right'p>>>",
		buttons: [
			'copyHtml5',
			'excelHtml5',
			'pdfHtml5'
		],
		lengthChange: false,
		pagingType: 'simple',
	})
	$('#filter-scope').change(function(e) {
		$.get('<?= site_url('rekap/filter_mou_scope') ?>', {
			scope: e.target.value
		}, function(data) {
			//pie.series[0].setData(data.pie); atau
			pie.series[0].update({
				data: data.pie
			});
			$('#div-table-mou-jenis').html(data.table);
		}, 'json')
	})
</script>