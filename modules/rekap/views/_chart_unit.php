<?php

// use \koolreport\widgets\koolphp\Table;
// use \koolreport\widgets\google\ColumnChart;
?>
<div class="row">
    <div class="col-md-4">
        <h4>Tabel Rekap Kerjasama Unit Berdasarkan Negara</h4>
        <div class='table-responsive' style="padding:10px">
            <table class="table table-responsive table-striped table-bordered" id='unit-negara'>
                <thead>
                    <tr>
                        <th>Negara</th>
                        <th>Aktif</th>
                        <th>Selesai</th>
                        <th>Jml</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $t1 = 0;
                    $t2 = 0;
                    $t3 = 0;
                    foreach ($negara as $row) {
                        $jml = $row->aktif + $row->selesai;
                        $t1 = $t1 + $row->aktif;
                        $t2 = $t2 + $row->selesai;
                        $t3 = $t3 + $jml;
                        echo "<tr>
                        <td>{$row->negara}</td>
                        <td>{$row->aktif}</td>
                        <td>{$row->selesai}</td>
                        <td><a href='javascript:void(0)' onclick='tampil_moa(\"$row->negara\")'>{$jml}</a></td>
                    </tr>";
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td><b>Total</b></td>
                        <td><?= $t1 ?></td>
                        <td><?= $t2 ?></td>
                        <td><?= $t3 ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="col-md-8">
        <div id='div-unit-negara' style="padding-right: 7px;"></div>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <h4>Tabel Kerjasama Unit Per Fakultas</h4>
        <div class='table-responsive' style="padding:10px">
            <table class="table table-responsive table-striped table-bordered" id='tbunit-fakultas'>
                <thead>
                    <tr>
                        <th>Fakultas/Unit</th>
                        <th>Aktif</th>
                        <th>Selesai</th>
                        <th>Jml</th>
                        <th>DN</th>
                        <th>LN</th>
                        <th>Dengan MoU</th>
                        <th>Proses MoU</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $t1 = 0;
                    $t2 = 0;
                    $t3 = 0;
                    $t4 = 0;
                    $t5 = 0;
                    $t6 = 0;
                    foreach ($tabel_fakultas as $row) {
                        $jml = $row->aktif + $row->selesai;
                        $t1 = $t1 + $row->aktif;
                        $t2 = $t2 + $row->selesai;
                        $t3 = $t3 + $row->dn;
                        $t4 = $t4 + $row->ln;
                        $t5 = $t5 + $row->with_mou;
                        $t6 = $t6 + $row->no_mou;
                        echo "<tr>
                        <td>{$row->nama_prodi}</td>
                        <td>{$row->aktif}</td>
                        <td>{$row->selesai}</td>
                        <td><b><a href='javascript:void(0)' onclick='tampil_rekap_fakultas(\"$row->kode_fak\",\"$row->nama_prodi\")'>{$jml}</a></b></td>
                        <td>{$row->dn}</td>
                        <td>{$row->ln}</td>
                        <td>{$row->with_mou}</td>
                        <td>{$row->no_mou}</td>
                    </tr>";
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td><b>Total</b></td>
                        <td><?= $t1 ?></td>
                        <td><?= $t2 ?></td>
                        <td><b><?= $t2 + $t1 ?></b></td>
                        <td><?= $t3 ?></td>
                        <td><?= $t4 ?></td>
                        <td><?= $t5 ?></td>
                        <td><?= $t6 ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="col-md-7">
        <div id='div-moa-unit-fakultas' style="padding-right: 7px;"></div>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <h4>Tabel Kerjasama Unit</h4>
        <div class='table-responsive' style="padding:10px">
            <table class="table table-responsive table-striped table-bordered" id='tbunit'>
                <thead>
                    <tr>
                        <th>Prodi/Unit</th>
                        <th>Aktif</th>
                        <th>Selesai</th>
                        <th>Jml</th>
                        <th>DN</th>
                        <th>LN</th>
                        <th>Dengan MoU</th>
                        <th>Proses MoU</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $t1 = 0;
                    $t2 = 0;
                    $t3 = 0;
                    $t4 = 0;
                    $t5 = 0;
                    $t6 = 0;
                    foreach ($tabel as $row) {
                        $jml = $row->aktif + $row->selesai;
                        $t1 = $t1 + $row->aktif;
                        $t2 = $t2 + $row->selesai;
                        $t3 = $t3 + $row->dn;
                        $t4 = $t4 + $row->ln;
                        $t5 = $t5 + $row->with_mou;
                        $t6 = $t6 + $row->no_mou;
                        echo "<tr>
                        <td>{$row->nama_prodi}</td>
                        <td>{$row->aktif}</td>
                        <td>{$row->selesai}</td>
                        <td><b><a href='javascript:void(0)' onclick='tampil_rekap_unit(\"$row->kd_prodi\",\"$row->nama_prodi\")'>{$jml}</a></b></td>
                        <td>{$row->dn}</td>
                        <td>{$row->ln}</td>
                        <td>{$row->with_mou}</td>
                        <td>{$row->no_mou}</td>
                    </tr>";
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td><b>Total</b></td>
                        <td><?= $t1 ?></td>
                        <td><?= $t2 ?></td>
                        <td><b><?= $t2 + $t1 ?></b></td>
                        <td><?= $t3 ?></td>
                        <td><?= $t4 ?></td>
                        <td><?= $t5 ?></td>
                        <td><?= $t6 ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="col-md-7">
        <div id='div-moa-unit' style="padding-right: 7px;"></div>
    </div>
</div>
<div class='row'>
    <div class='col-md-12'>
        <?php
        // Table::create(array(
        //     "dataSource" => $test
        // ));
        ?>
        <?php
        // ColumnChart::create(array(
        //     "dataSource" => $test
        // ));
        ?>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        // rekap kerjasama unit by negara
        Highcharts.chart('div-unit-negara', {
            data: {
                table: 'unit-negara',
                endColumn: 2,
                endRow: $('#unit-negara tr').length - 2,
            },
            chart: {
                type: 'bar',
                height: 75 + '%'
            },
            colors: ['#058dc7', '#ff9655'],
            title: {
                text: 'Chart Kerjasama Unit Berdasarkan Negara'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Jumlah dokumen'
                },
                stackLabels: { // tampilkan jml di bar
                    enabled: true
                }
            },
            plotOptions: {
                series: {
                    stacking: 'normal', // menampilkan jml di bar
                    pointPadding: 0.1,
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        formatter: function() { //skip nilai 0
                            if (this.y) {
                                return this.y;
                            }
                        }
                    },
                },
                //stacking: 'normal'
            },
            tooltip: {
                formatter: function() {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.point.y + ' ' + this.point.name.toLowerCase();
                }
            }
        });
        $('#unit-negara').DataTable({
            dom: "<'row'<'col-sm-5'B><'col-sm-7'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'i><'col-sm-7'<'pull-right'p>>>",
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'pdfHtml5'
            ],
            lengthChange: false,
            pagingType: 'simple',
        })

        //rekap by prodi
        $('#tbunit').DataTable({
            dom: "<'row'<'col-sm-6'B><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'i><'col-sm-7'<'pull-right'p>>>",
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'pdfHtml5'
            ],
            lengthChange: false,
            pagingType: 'simple',
        })
        var data = <?= $rekap_unit ?>;
        var chart_unit = {
            chart: {
                renderTo: 'div-moa-unit',
                type: 'bar',
                height: 300 + '%'
            },
            title: {
                text: 'Chart kegiatan kerjasama Unit'
            },
            subtitle: {
                text: 'Source: <a href="simkerjasama.unnes.ac.id">simkerjasama.unnes.ac.id pada <?= date('Y-m-d H:i:s') ?></a>'
            },
            xAxis: {
                categories: data[6],
                title: {
                    text: null
                },
            },
            yAxis: {
                min: 0,
                allowDecimals: false,
                title: {
                    text: 'Jumlah kegiatan (dokumen)',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ' dok'
            },
            plotOptions: {
                series: {
                    pointWidth: 10,
                    pointPadding: 0,
                    groupPadding: 0,
                },
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 240,
                floating: true,
                borderWidth: 1,
                backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [
                data[0], data[1], data[2], data[3], data[4], data[5]
            ],
        }

        //rekap by fakultas
        $('#tbunit-fakultas').DataTable({
            dom: "<'row'<'col-sm-6'B><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'i><'col-sm-7'<'pull-right'p>>>",
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'pdfHtml5'
            ],
            lengthChange: false,
            pagingType: 'simple',
        })
        var data_fak = <?= $rekap_fakultas ?>;
        var chart_unit_fak = {
            chart: {
                renderTo: 'div-moa-unit-fakultas',
                type: 'bar',
                height: 125 + '%'
            },
            title: {
                text: 'Chart Kegiatan Kerjasama Per Fakultas/Unit'
            },
            subtitle: {
                text: 'Source: <a href="simkerjasama.unnes.ac.id">simkerjasama.unnes.ac.id pada <?= date('Y-m-d H:i:s') ?></a>'
            },
            xAxis: {
                categories: data_fak[6],
                title: {
                    text: null
                },
            },
            yAxis: {
                min: 0,
                allowDecimals: false,
                title: {
                    text: 'Jumlah kegiatan (dokumen)',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ' dok'
            },
            plotOptions: {
                series: {
                    pointWidth: 10,
                    pointPadding: 0,
                    groupPadding: 0,
                },
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 240,
                floating: true,
                borderWidth: 1,
                backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [
                data_fak[0], data_fak[1], data_fak[2], data_fak[3], data_fak[4], data_fak[5]
            ],
        }
        // memanggil data dengan jquery
        /*
        $.getJSON("https://laporankerma.kemdikbud.go.id/rest-api/dashboard/aktif-mou", function(json) {
            universitas_aktif.series[0].data = json;
            chart = new Highcharts.Chart(universitas_aktif);
        });
        */
        chart_fak = new Highcharts.Chart(chart_unit_fak);
        chart = new Highcharts.Chart(chart_unit);
    })
</script>