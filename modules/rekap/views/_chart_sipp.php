<style>
.mydialog 
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    max-height: 500px;
    overflow-y: auto;
}
</style>
<div id="chart-sipp" style="padding-right:5px"></div>
<div class="box box-default box-solid" style="margin-top: 10px;">
    <div class="box-header with-border">
        <h3 class="box-title">Tabel Output SIPP Kerjasama</h3>
    </div>
    <div class="box-body">
        <div class="table table-responsive">
            <table class='table table-responsive table-bordered table-striped table-hover'>
                <thead>
                    <tr>
                        <th>Output Name</th>
                        <?php foreach ($tahun as $t) echo "<th>$t</th>" ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($sipp as $val) {
                        echo "<tr><td>{$val['name']}</td>";
                        foreach ($val['data'] as $i => $j) {
                            if($j[0] == 0)
                                echo "<td>{$j[0]}";
                            else
                                echo "<td><a href='javascript:void(0)' onclick='lihat_data_sipp(\"{$j[1]}\",\"{$tahun[$i]}\")'>{$j[0]}</td>";
                        }
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer"></div>
</div>
<script>
    <?php echo $chart->render(); ?>
    async function lihat_data_sipp(o_ks,tahun)
    {
        BootstrapDialog.show({
            title: 'Data SIPP tahun '+tahun,
            size: 'size-wide',
            cssClass : 'mydialog',
            message: await function(dialog) {
                var $message = $('<div class="table table-responsive"></div>');
                $.get('<?= site_url('rekap/list_sipp_by_tahun') ?>', {tahun:tahun,o_ks:o_ks},function(page){
                    $message.html(page);
                    $('#table1',$message).DataTable();
                });
                return $message;
            },
            buttons: [{
                label: 'Tutup',
                action: function(dialog) {
                    dialog.close();
                }
            }],
        });
    }
</script>