<section class="content-header">
	<h1>
		Rekap <small><?= cclang('detail', ['Rekap']); ?> </small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/rekap'); ?>">Rekap Kerjasama Unit</a></li>
		<li class="active"><?= cclang('detail'); ?></li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/chart.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Rekap</h3>
							<h5 class="widget-user-desc">Rekap Kerjasama Unit</h5>
							<div class='row' style="margin-top: 30px;">
								<div class='col-md-12'>
									<form action="<?= BASE_URL . 'rekap/rekap_unit' ?>" method="get" class="form-inline" role="form">
										<div class="form-group">
											<div class="input-group">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<?= form_dropdown('thn_a', $thns, $thn_a, ['class' => 'form-control']) ?>
											</div>
										</div>
										<div class="form-group">
											<label class="" for=""> sampai </label>
											<?= form_dropdown('thn_b', $thns, $thn_b, ['class' => 'form-control']) ?>
										</div>
										<button type="submit" class="btn btn-primary">Tampilkan</button>
									</form>
								</div>
							</div>
							<hr>
						</div>
						<div id='div-chart-unit'></div>
					</div>
					<!-- <div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item"
							frameborder="0" 
							allowFullScreen="true"
							src="https://app.powerbi.com/view?r=eyJrIjoiNjAwOGQ2MTQtZjQwYi00MGRkLWJlZDQtNTI2NDZhM2IxNDQ2IiwidCI6IjI5ZTVlYTIwLWRhYWEtNDI5MS1hMmFlLTg3NGU2M2MxZjI5MiIsImMiOjEwfQ%3D%3D&pageName=ReportSection"></iframe>
					</div> -->
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/highcharts.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/data.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/exporting.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/export-data.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/accessibility.js"></script>

<script type="text/javascript">
	$(function() {
		$.get('<?= site_url('rekap/chart_unit') ?>', {}, function(page) {
			$('#div-chart-unit').html(page);
		})
	})
	async function tampil_moa(negara) {
		BootstrapDialog.show({
			title: 'Informasi kerjasama Unit dengan Negara ' + negara,
			size: 'size-wide',
			message: await
			function(dialog) {
				var $message = $('<div class="table-responsive"></div>');
				$.get('<?= site_url('rekap/list_moa_by_negara') ?>', {
					negara: negara
				}, function(page) {
					$message.html(page);
					$('#table3', $message).DataTable();
				});
				return $message;
			},
			buttons: [{
				label: 'Tutup',
				action: function(dialog) {
					dialog.close();
				}
			}],
		});
	}
	async function tampil_rekap_unit(unit, name) {
		BootstrapDialog.show({
			title: 'Kerjasama Unit : ' + name,
			size: 'size-wide',
			message: await
			function(dialog) {
				var $message = $('<div class="table-responsive"></div>');
				$.get('<?= site_url('rekap/list_moa_by_unit') ?>', {
					unit: unit
				}, function(page) {
					$message.html(page);
					$('#table1', $message).DataTable();
				});
				return $message;
			},
			buttons: [{
				label: 'Tutup',
				action: function(dialog) {
					dialog.close();
				}
			}],
		});
	}
	async function tampil_rekap_fakultas(fak, name) {
		BootstrapDialog.show({
			title: 'Kerjasama Unit : ' + name,
			size: 'size-wide',
			message: await
			function(dialog) {
				var $message = $('<div class="table-responsive"></div>');
				$.get('<?= site_url('rekap/list_moa_by_fakultas') ?>', {
					fak: fak
				}, function(page) {
					$message.html(page);
					$('#table1', $message).DataTable();
				});
				return $message;
			},
			buttons: [{
				label: 'Tutup',
				action: function(dialog) {
					dialog.close();
				}
			}],
		});
	}
</script>
<!-- /.content -->