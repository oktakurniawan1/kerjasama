<script>
   $(function() {
      pie = new Highcharts.chart('div-mbkm-fak', {
         chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            height: 55 + '%',
            type: 'pie'
         },
         title: {
            text: 'Chart MoU Berdasarkan Jenis Kerjasama',
         },
         tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b> | {point.y}'
         },
         accessibility: {
            point: {
               valueSuffix: '%'
            }
         },
         plotOptions: {
            pie: {
               allowPointSelect: true,
               cursor: 'pointer',
               dataLabels: {
                  enabled: true,
                  //alignTo: 'plotEdges',
                  format: '<b>{point.name}</b>: {point.percentage:.1f} % | {point.y}'
               },
               showInLegend: true
            }
         },
         series: [{
            name: 'Dokumen',
            colorByPoint: true,
            data: <?php echo $piechart_mbkm_fak ?>
         }]
      });
   })
</script>