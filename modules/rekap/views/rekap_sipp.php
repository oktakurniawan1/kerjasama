<section class="content-header">
    <h1>
        Rekap <small><?= cclang('detail', ['Rekap']); ?> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="<?= site_url('administrator/rekap'); ?>">Rekap SIPP</a></li>
        <li class="active"><?= cclang('detail'); ?></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/chart.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Rekap</h3>
                            <h5 class="widget-user-desc">Rekap implementasi SIPP kerjasama</h5>
                            <hr>
                        </div>
                        <div class='row'>
                            <div class='col-md-4'>
                                <div class="form-group">
                                    <label>Date range:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="range_sipp">
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-flat" type="button" id='sipp-btn'>Tampil</button>
                                        </span>
                                    </div><!-- /.input group -->
                                </div><!-- /.form group -->
                            </div>
                        </div>
                        <div id='div-chart-sipp'></div>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
        <div class='col-md-12'>
        </div>
    </div>
</section>
<script src="<?= BASE_ASSET; ?>/daterangepicker/moment.min.js"></script>
<script src="<?= BASE_ASSET; ?>/daterangepicker/daterangepicker.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/highcharts.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/data.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/exporting.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/export-data.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/accessibility.js"></script>

<script type="text/javascript">
    $(function() {
        var d = new Date();
        $('#range_sipp').daterangepicker({
            opens: "right",
            showDropdowns: true,
            minYear: d.getFullYear() - 10,
            maxYear: d.getFullYear(),
            linkedCalendars: false,
            locale: {
                format: 'YYYY/MM/DD'
            }
        });
        $.get('<?= site_url('rekap/chart_sipp') ?>', {
            date_sipp: '<?= $date_sipp ?>'
        }, function(page) {
            $('#div-chart-sipp').html(page);
        })
        $('#sipp-btn').click(function(e) {
            $.get('<?= site_url('rekap/chart_sipp') ?>', {
                date_sipp: $('#range_sipp').val()
            }, function(page) {
                $('#div-chart-sipp').html(page);
            })
        })
    })
</script>
<!-- /.content -->