<section class="content-header">
	<h1>
		Rekap <small><?= cclang('detail', ['Rekap']); ?> </small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/rekap'); ?>">Skoring</a></li>
		<li class="active"><?= cclang('detail'); ?></li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/chart.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Rekap </h3>
							<h5 class="widget-user-desc">Rekap Kerjasama Unit Kebutuhan IKU 6</h5>
							<div class='row' style="margin-top: 30px;">
								<div class="col-md-12">
									<div class="embed-responsive embed-responsive-16by9">
										<iframe title="rekap_iku" width="800" height="836" src="https://app.powerbi.com/view?r=eyJrIjoiMjEwMDhjYzYtYWNkNi00MmMwLTljY2QtYTcyNDc4MzY0OWJiIiwidCI6IjI5ZTVlYTIwLWRhYWEtNDI5MS1hMmFlLTg3NGU2M2MxZjI5MiIsImMiOjEwfQ%3D%3D" frameborder="0" allowFullScreen="true"></iframe>
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="row" style="padding: 10px;">
							<div class="col-md-4">
								<form class="form-horizontal" id='myForm' method="GET" target="_blank">
									<legend>Download rekap (excel)</legend>
									<div class="form-group">
										<label for="" class="col-sm-2 control-label">Unit</label>
										<div class="col-sm-10">
											<? //= form_dropdown('unit', ['prodi' => 'Program studi', 'fak' => 'Fakultas'], 'kd_prodi', ['class' => 'form-control']) 
											?>
										</div>
									</div>
									<div class="form-group">
										<label for="" class="col-sm-2 control-label">Mulai</label>
										<div class="col-sm-10">
											<input type="month" name="from" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label for="" class="col-sm-2 control-label">Sampai</label>
										<div class="col-sm-10">
											<input type="month" name="to" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-2">
											<button type="button" id="btn1" class="btn btn-primary">Download</button>
										</div>
										<div class="col-sm-2" id="btn2" style="margin-left: 15px;">
											<button type="button" class="btn btn-primary">Download Detail</button>
										</div>
									</div>
								</form>
							</div>
						</div> -->
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<script>
	$(document).ready(function() {
		$("#btn1").click(function() {
			$("#myForm").attr("action", "<?= site_url('/rekap_iku/unit_jml_doc') ?>");
			$("#myForm").submit();
		});

		$("#btn2").click(function() {
			$("#myForm").attr("action", "<?= site_url('/rekap_iku/unit_detail_mitra') ?>");
			// $("#myForm").append('<input type="hidden" name="submit_type" value="2">');
			$("#myForm").submit();
		});
	});
</script>