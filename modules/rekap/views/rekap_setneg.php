<section class="content-header">
	<h1>
		Rekap <small><?= cclang('detail', ['Rekap']); ?> </small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/rekap'); ?>">Rekap Kerjasama Unit</a></li>
		<li class="active"><?= cclang('detail'); ?></li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/chart.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Rekap</h3>
							<h5 class="widget-user-desc">Rekap Kerjasama Unit</h5>
							<hr>
						</div>
						<div id='div-chart-unit'></div>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/highcharts.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/data.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/exporting.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/export-data.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/accessibility.js"></script>
<script type="text/javascript">
	$(function() {
		$.get('<?= site_url('rekap/chart_unit') ?>', {}, function(page) {
			$('#div-chart-unit').html(page);
		})
	})
</script>
<!-- /.content -->