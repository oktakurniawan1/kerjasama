<section class="content-header">
	<h1>
		Rekap <small><?= cclang('detail', ['Rekap']); ?> </small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/rekap'); ?>">Rekap Student Mobility</a></li>
		<li class="active"><?= cclang('detail'); ?></li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/chart.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Rekap</h3>
							<h5 class="widget-user-desc">Rekap Student Mobility</h5>
							<div class='row' style="margin-top: 30px;">
								<div class='col-md-12'>
									<form action="<?= BASE_URL . 'rekap/rekap_mhs' ?>" method="get" class="form-inline" role="form">
										<div class="form-group">
											<div class="input-group">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<?= form_dropdown('thn_a', $thns, $thn_a, ['class' => 'form-control']) ?>
											</div>
										</div>
										<div class="form-group">
											<label class="" for=""> sampai </label>
											<?= form_dropdown('thn_b', $thns, $thn_b, ['class' => 'form-control']) ?>
										</div>
										<button type="submit" class="btn btn-primary">Tampilkan</button>
									</form>
								</div>
							</div>
							<hr>
						</div>
						<div id='div-chart-student-mob'></div>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/highcharts.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/data.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/exporting.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/export-data.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/accessibility.js"></script>

<style>
	.mydialog .modal-dialog {
		overflow-y: initial !important
	}

	.modal-body {
		max-height: 450px;
		overflow-y: auto;
	}
</style>

<script type="text/javascript">
	$(function() {
		$.get('<?= site_url('rekap/chart_student_mob') ?>', {}, function(page) {
			$('#div-chart-student-mob').html(page);
		})
	})
	async function tampil_rekap(unit, name) {
		BootstrapDialog.show({
			title: 'Program Studi' + name,
			size: 'size-wide',
			cssClass: 'mydialog',
			message: await
			function(dialog) {
				var $message = $('<div class="table-responsive"></div>');
				$.get('<?= site_url('rekap/list_studentmob_by_prodi') ?>', {
					unit: unit
				}, function(page) {
					$message.html(page);
					$('#table1', $message).DataTable();
				});
				return $message;
			},
			buttons: [{
				label: 'Tutup',
				action: function(dialog) {
					dialog.close();
				}
			}],
		});
	}
</script>
<!-- /.content -->