<section class="content-header">
	<h1>
		Rekap <small><?= cclang('detail', ['Rekap']); ?> </small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/rekap'); ?>">Rekap MoU Kerjasama</a></li>
		<li class="active"><?= cclang('detail'); ?></li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/chart.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Rekap</h3>
							<h5 class="widget-user-desc">Rekap Mou Kerjasama</h5>
							<div class='row' style="margin-top: 30px;">
								<div class='col-md-12'>
									<form action="<?= BASE_URL . 'rekap/rekap_mou' ?>" method="get" class="form-inline" role="form">
										<div class="form-group">
											<div class="input-group">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<?= form_dropdown('thn_a', $thns, $thn_a, ['class' => 'form-control']) ?>
											</div>
										</div>
										<div class="form-group">
											<label class="" for=""> sampai </label>
											<?= form_dropdown('thn_b', $thns, $thn_b, ['class' => 'form-control']) ?>
										</div>
										<button type="submit" class="btn btn-primary">Tampilkan</button>
									</form>
								</div>
							</div>
							<hr>
						</div>
						<div id='div-chart-mou'></div>
						<div class="row">
							<div class="col-md-12">
								<h4 class='title'>Rekap Sebaran Penggunaan MoU</h4>
								<div class='table-responsive'>
									<table class="table table-hover table-bordered table-striped table-responsive" id='tb-sebaran-mou'>
										<thead>
											<tr>
												<th rowspan="3" style="text-align: center;">Nama Mitra MoU</th>
											</tr>
											<tr>
												<th colspan="3" style="text-align: center;">Kerjasama Unit/Prodi</th>
												<th colspan="3" style="text-align: center;">Dosen Inbound</th>
												<th colspan="3" style="text-align: center;">MBKM</th>
												<th colspan="3" style="text-align: center;">Perjalanan LN</th>
											</tr>
											<tr>
												<th>Aktif</th>
												<th>Selesai</th>
												<th>Jml</th>
												<th>Aktif</th>
												<th>Selesai</th>
												<th>Jml</th>
												<th>Aktif</th>
												<th>Selesai</th>
												<th>Jml</th>
												<th>Aktif</th>
												<th>Selesai</th>
												<th>Jml</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$tot = [];
											foreach ($sebaran as $row) {
												echo "<tr>
													<td>{$row['mitra']}</td>";
												foreach ($row['dt'] as $t => $n) {
													$tot[$t][1][] = $n['jml_aktif'];
													$tot[$t][2][] = $n['jml_selesai'];
													$tot[$t][3][] = $n['jml'];
													echo "<td>{$n['jml_aktif']}</td>";
													echo "<td>{$n['jml_selesai']}</td>";
													echo "<td>{$n['jml']}</td>";
												}

												echo "</tr>";
											}
											?>
										</tbody>
										<tfoot>
											<tr>
												<td><b>Total</b></td>
												<td><b><?php echo array_sum($tot['unit'][1]) ?></b></td>
												<td><b><?php echo array_sum($tot['unit'][2]) ?></b></td>
												<td><b><?php echo array_sum($tot['unit'][3]) ?></b></td>

												<td><b><?php echo array_sum($tot['dosen_inb'][1]) ?></b></td>
												<td><b><?php echo array_sum($tot['dosen_inb'][2]) ?></b></td>
												<td><b><?php echo array_sum($tot['dosen_inb'][3]) ?></b></td>

												<td><b><?php echo array_sum($tot['mbkm'][1]) ?></b></td>
												<td><b><?php echo array_sum($tot['mbkm'][2]) ?></b></td>
												<td><b><?php echo array_sum($tot['mbkm'][3]) ?></b></td>

												<td><b><?php echo array_sum($tot['perjalun'][1]) ?></b></td>
												<td><b><?php echo array_sum($tot['perjalun'][2]) ?></b></td>
												<td><b><?php echo array_sum($tot['perjalun'][3]) ?></b></td>

											</tr>
										</tfoot>
									</table>
								</div>
							</div>

						</div>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/highcharts.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/data.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/exporting.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/export-data.js"></script>
<script src="<?= BASE_ASSET; ?>/Highcharts-8.1.0/code/modules/accessibility.js"></script>

<script type="text/javascript">
	$(function() {
		$.get('<?= site_url('rekap/chart_mou') ?>', {}, function(page) {
			$('#div-chart-mou').html(page);
		})
		$('#tb-sebaran-mou').DataTable({
			dom: "<'row'<'col-sm-2'l><'col-sm-4'B><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'i><'col-sm-7'<'pull-right'p>>>",
			buttons: [
				'copyHtml5',
				'excelHtml5',
				'pdfHtml5'
			]
		});
	})
	async function tampil_mou(negara) {
		BootstrapDialog.show({
			title: 'Informasi MoU kerjasama Negara ' + negara,
			size: 'size-wide',
			message: await
			function(dialog) {
				var $message = $('<div class="table-responsive"></div>');
				$.get('<?= site_url('rekap/list_mou_by_negara') ?>', {
					negara: negara
				}, function(page) {
					$message.html(page);
					$('#table1', $message).DataTable();
				});
				return $message;
			},
			buttons: [{
				label: 'Tutup',
				action: function(dialog) {
					dialog.close();
				}
			}],
		});
	}
	async function tampil_mou_jenis(jid) {
		BootstrapDialog.show({
			title: 'Informasi MoU Jenis Kerjasama',
			size: 'size-wide',
			message: await
			function(dialog) {
				var $message = $('<div class="table-responsive"></div>');
				var scope = $('select[name=filter-scope] option:selected').val();
				$.get('<?= site_url('rekap/list_mou_by_jenis') ?>', {
					jid: jid,
					scope: scope
				}, function(page) {
					$message.html(page);
					$('#table2', $message).DataTable();
				});
				return $message;
			},
			buttons: [{
				label: 'Tutup',
				action: function(dialog) {
					dialog.close();
				}
			}],
		});
	}
</script>