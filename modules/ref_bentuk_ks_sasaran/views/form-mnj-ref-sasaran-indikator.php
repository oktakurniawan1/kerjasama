<form action="" method="POST" role="form">
	<div class="row">
		<div class="form-group">
			<div class="col-md-9">
				<label class="sr-only" for=""></label>
				<input type="text" name="nama_<?=$table?>" class="form-control" id="" placeholder="Nama <?=$table?>">
			</div>
			<button type="button" class="btn btn-default" onclick="tambah_data()">Tambah <?=$table?></button>
		</div>
	</div>
</form>
<table class="table table-hover table-striped table-bordered" id='table1'>
	<thead>
		<tr>
			<th>_id</th>
			<th>Nama <?=$table?></th>
			<th>Option</th>
		</tr>
	</thead>
	<tbody>
		<tr>
		</tr>
	</tbody>
</table>

<script>
	var oTable;
	$(function() {
		oTable = $('#table1').DataTable({
			processing: true,
			serverSide: true,
			pageLength: 5,
			lengthChange: false,
			//lengthMenu: [6, 10, 15, 25],
			ajax: {
				'url': '<?php echo site_url('administrator/ref_bentuk_ks_sasaran/browse_reff_sasaran_indikator') ?>',
				'type': 'post',
				'data': {table : '<?=$table?>',<?= csrf_token() ?>
				},
			},
			columns: [{
					data: 'id',
					visible: false,
					searchable: false,
				},
				{
					data: 'nama_'+ '<?=$table?>'
				},
				{
					className: 'text-center',
					searchable: false,
					orderable: false,
					width: '30px'
				}
			],
			columnDefs: [{
				'targets': -1,
				'data': null,
				render: function(data, type, row) {
					return "<button class='btn btn-default btn-xs' onclick='hapus_data(" + row.id + ")'><i class='fa fa-trash text-danger'></i></button> ";
				}
			}]
		})
		oTable.MakeCellsEditable({
			"onUpdate": update_data,
			"inputCss": 'form-control',
			'wrapperHtml': '<div class="alert alert-info" style="padding:5px">{content}</div>',
			"columns": [1],
			"confirmationButton": {
				"confirmCss": 'btn btn-success btn-xs',
				"cancelCss": 'btn btn-warning btn-xs'
			},
		});
	})

	function update_data(updatedCell, updatedRow, oldValue) {
		var url = '<?php echo site_url('administrator/ref_bentuk_ks_sasaran/update_reff_sasaran_indikator') ?>';
		var data = updatedRow.data();
		data[csrf] = token;
		data['table'] = '<?=$table?>';
		$.post(url, data, function(response) {
			oTable.draw(false);
		})
	}

	function tambah_data() {
		var data = {};
		var url = '<?php echo site_url('administrator/ref_bentuk_ks_sasaran/tambah_reff_sasaran_indikator') ?>';
		var nama = $('input[name=nama_'+'<?=$table?>'+']').val();
		if (nama.length == 0) {
			alert('Nama '+'<?=$table?>'+' harus diisi!');
			return;
		}
		data[csrf] = token;
		data['nama_'+'<?=$table?>'] = nama;
		data['table'] = '<?=$table?>';
		$.post(url, data, function(response) {
			if (response.status > 0) {
				oTable.draw(false);
			}
		}, 'json')
	}

	function hapus_data(id) {
		if (confirm('Yakin akan dihapus?')) {
			var url = '<?php echo site_url('administrator/ref_bentuk_ks_sasaran/hapus_reff_sasaran_indikator') ?>';
			$.get(url, {
				table:'<?=$table?>',
				id: id
			}, function(response) {
				if (response.status > 0) {
					oTable.draw(false);
				}
				else {
					alert('Data ini sudah digunakan di kerjasama Unit atau sudah memiliki sasaran, tidak bisa dihapus');
				}
			}, 'json')
		}
	}
</script>