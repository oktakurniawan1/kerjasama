<form action="" method="POST" role="form">
	<input type="hidden" name="id_sasaran" value='<?= $sasaran->id ?>'>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<label for="">Bentuk Kerjasama :</label>
				<input type="text" class='form-control' value="<?= $sasaran->bentuk_ks ?>" disabled>
			</div>
		</div>
		<div class="col-md-9">
			<div class="form-group">
				<label for="">Sasaran :</label>
				<input type="text" class='form-control' value="<?= $sasaran->nama_sasaran ?>" disabled>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group">
			<div class="col-md-9">
				<label class="sr-only" for=""></label>
				<?= form_dropdown('id_ref_indikator', $ref_indikator,null, ['style' => 'width:100%']) ?>
			</div>
			<button type="button" class="btn btn-success" onclick="tambah_indikator()">Tambah Indikator</button>
		</div>
	</div>
</form>
<table class="table table-hover table-striped table-bordered" id='table1'>
	<thead>
		<tr>
			<th>_id</th>
			<th>Nama Indikator</th>
			<th>Option</th>
		</tr>
	</thead>
	<tbody>
		<tr>
		</tr>
	</tbody>
</table>

<script>
	var oTable;
	$(function() {
		oTable = $('#table1').DataTable({
			processing: true,
			serverSide: true,
			pageLength: 5,
			lengthChange: false,
			//lengthMenu: [6, 10, 15, 25],
			ajax: {
				'url': '<?php echo site_url('administrator/ref_bentuk_ks_sasaran/browse_indikator') ?>',
				'type': 'post',
				'data': {
					id_sasaran: '<?= $sasaran->id ?>',
					<?= csrf_token() ?>
				},
			},
			columns: [{
					data: 'id',
					visible: false,
					searchable: false,
				},
				{
					data: 'nama_indikator'
				},
				{
					className: 'text-center',
					searchable: false,
					orderable: false,
					width: '30px'
				}
			],
			columnDefs: [{
				'targets': -1,
				'data': null,
				render: function(data, type, row) {
					return "<button class='btn btn-default btn-xs' onclick='hapus_indikator(" + row.id + ")'><i class='fa fa-trash text-danger'></i></button> ";
				}
			}]
		})
		oTable.MakeCellsEditable({
			"onUpdate": update_data,
			"inputCss": 'form-control',
			'wrapperHtml': '<div class="alert alert-info">{content}</div>',
			"columns": [1],
			"confirmationButton": {
				"confirmCss": 'btn btn-success btn-xs',
				"cancelCss": 'btn btn-warning btn-xs'
			},
			"inputTypes": [
				{
					"column":1, 
					"type":"list", 
					"options":<?=$indikator?>
				},
			]
		});
		$('select[name=id_ref_indikator]').select2({
			theme: 'classic',
			dropdownParent: $('#form_mnj_indikator'),
		});
	})

	function update_data(updatedCell, updatedRow, oldValue) {
		var url = '<?php echo site_url('administrator/ref_bentuk_ks_sasaran/update_indikator') ?>';
		var data = updatedRow.data();
		data[csrf] = token;
		$.post(url, data, function(response) {
			oTable.draw(false);
		})
	}

	function tambah_indikator() {
		var data = {};
		var url = '<?php echo site_url('administrator/ref_bentuk_ks_sasaran/tambah_indikator') ?>';
		var id_ref_indikator = $('select[name=id_ref_indikator]').val();
		if (id_ref_indikator.length == 0) {
			alert('Nama Indikator harus diisi!');
			return;
		}
		data[csrf] = token;
		data['id_sasaran'] = $('input[name=id_sasaran]').val();
		data['id_ref_indikator'] = id_ref_indikator;
		$.post(url, data, function(response) {
			if (response.status > 0) {
				oTable.draw(false);
			}
		}, 'json')
	}

	function hapus_indikator(id) {
		if (confirm('Yakin akan dihapus?')) {
			var url = '<?php echo site_url('administrator/ref_bentuk_ks_sasaran/hapus_indikator') ?>';
			$.get(url, {
				id: id
			}, function(response) {
				if (response.status > 0) {
					oTable.draw(false);
				}else{
					alert('Data ini sudah digunakan di kerjasama Unit atau sudah memiliki sasaran, tidak bisa dihapus');
				}
			}, 'json')
		}
	}
</script>