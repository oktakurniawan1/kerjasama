<form action="" method="POST" role="form">
	<div class="row">
		<div class="col-md-8">
			<div class="form-group">
				<label for="">Bentuk Kerjasama</label>
				<?= form_dropdown('id_bentuk_ks', $id_bentuk_ks, fromsess('filterdata_ref_bentuk_ks_sasaran')['id_bentuk_ks'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label for="">Sasaran</label>
				<?= form_dropdown('id_ref_sasaran', $id_ref_sasaran, fromsess('filterdata_ref_bentuk_ks_sasaran')['id_ref_sasaran'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
			</div>
		</div>
	</div>
</form>

<script>
	$(function() {
		$('select[name=id_bentuk_ks],select[name=id_ref_sasaran]').select2({
			theme: 'classic',
			dropdownParent: $('#filterref_bentuk_ks_sasaran-dialog')
		});
		// $('select[name=name4]').select2({
		// 	tags:true
		// })
	})
</script>