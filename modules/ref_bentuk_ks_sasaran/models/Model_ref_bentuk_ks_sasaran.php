<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_ref_bentuk_ks_sasaran extends MY_Model
{

	private $primary_key    = 'id';
	private $table_name     = 'ref_bentuk_ks_sasaran';
	public $field_search   = ['id_bentuk_ks', 'id_ref_sasaran'];

	public function __construct()
	{
		$config = array(
			'primary_key'   => $this->primary_key,
			'table_name'    => $this->table_name,
			'field_search'  => $this->field_search,
		);

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "ref_bentuk_ks_sasaran." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'id_ref_sasaran') $where .= "OR " . "ref_sasaran.nama_sasaran LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "ref_bentuk_ks_sasaran." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'id_ref_sasaran') $where .= "OR " . "ref_sasaran.nama_sasaran LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			if ($field == 'id_ref_sasaran') $where .= "(" . "ref_sasaran.nama_sasaran  LIKE '%" . $q . "%' )";
			else $where .= "(" . "ref_bentuk_ks_sasaran." . $field . " LIKE '%" . $q . "%' )";
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);
		$this->db->group_by('ref_bentuk_ks_sasaran.id_bentuk_ks,ref_bentuk_ks_sasaran.id');

		/**form filter data */
		if ($this->session->has_userdata('filterdata_ref_bentuk_ks_sasaran')) {
			foreach (fromsess('filterdata_ref_bentuk_ks_sasaran') as $key => $row) {
				if ($row != 'all') $this->db->having($key, $row);
			}
		}
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [], $result = TRUE)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "ref_bentuk_ks_sasaran." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'id_ref_sasaran') $where .= "OR " . "ref_sasaran.nama_sasaran LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "ref_bentuk_ks_sasaran." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'id_ref_sasaran') $where .= "OR " . "ref_sasaran.nama_sasaran LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			if ($field == 'id_ref_sasaran') $where .= "(" . "ref_sasaran.nama_sasaran  LIKE '%" . $q . "%' )";
			else $where .= "(" . "ref_bentuk_ks_sasaran." . $field . " LIKE '%" . $q . "%' )";
		}

		if (is_array($select_field) and count($select_field)) {
			$this->db->select($select_field);
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);

		/**form filter data */
		if ($this->session->has_userdata('filterdata_ref_bentuk_ks_sasaran')) {
			foreach (fromsess('filterdata_ref_bentuk_ks_sasaran') as $key => $row) {
				if ($row != 'all') $this->db->having($key, $row);
			}
		}
		// $this->db->having('id_bentuk_ks','15');
		$this->db->group_by('ref_bentuk_ks_sasaran.id_bentuk_ks,ref_bentuk_ks_sasaran.id');
		$this->db->limit($limit, $offset);
		$this->sortable();
		$query = $this->db->get($this->table_name);
		if ($result)
			return $query->result();
		else return $query;
	}

	public function join_avaiable()
	{
		$this->db->join('si_bentuk_ks', 'si_bentuk_ks.id = ref_bentuk_ks_sasaran.id_bentuk_ks', 'LEFT');
		$this->db->join('ref_bentuk_ks_indikator', 'ref_bentuk_ks_indikator.id_sasaran = ref_bentuk_ks_sasaran.id', 'LEFT');
		$this->db->join('ref_sasaran', 'ref_sasaran.id = ref_bentuk_ks_sasaran.id_ref_sasaran', 'LEFT');
		$this->db->join('ref_indikator', 'ref_indikator.id = ref_bentuk_ks_indikator.id_ref_indikator', 'LEFT');
		$this->db->select('ref_bentuk_ks_sasaran.*,si_bentuk_ks.bentuk_ks as si_bentuk_ks_bentuk_ks');
		$this->db->select('ref_sasaran.nama_sasaran as ref_sasaran_nama_sasaran');
		$this->db->select('GROUP_CONCAT(ref_indikator.nama_indikator SEPARATOR "#") AS indikator');
		return $this;
	}

	public function filter_avaiable()
	{
		if (!$this->aauth->is_admin()) {
		}
		return $this;
	}
	/**
	 * Input indikator
	 */
	public function getIndikator($id_sasaran)
	{
		$this->load->library('Datatables', null, 'dt');
		$this->dt->set_database('default');
		$this->dt->select('a.*,f.nama_indikator')
			->from('ref_bentuk_ks_indikator AS a')
			->join('ref_indikator AS f', 'f.id = a.id_ref_indikator')
			->where(['id_sasaran' => $id_sasaran]);
		return $this->dt->generate();
	}
	public function getInfo($id)
	{
		return $this->db->select('a.*,b.bentuk_ks,c.nama_sasaran')
			->from('ref_bentuk_ks_sasaran AS a')
			->join('si_bentuk_ks AS b', 'a.id_bentuk_ks = b.id', 'LEFT')
			->join('ref_sasaran AS c', 'c.id = a.id_ref_sasaran', 'LEFT')
			->where('a.id', $id)->get()->row();
	}
	public function tambah_indikator($post)
	{
		$this->db->insert('ref_bentuk_ks_indikator', $post);
		return $this->db->affected_rows();
	}
	public function hapus_indikator($id)
	{
		$this->db->where('id', $id)->delete('ref_bentuk_ks_indikator');
		return $this->db->affected_rows();
	}
	public function update_indikator($data)
	{
		$data['id_ref_indikator'] = $data['nama_indikator'];
		unset($data['nama_indikator']);
		$this->db->where('id', $data['id'])->update('ref_bentuk_ks_indikator', $data);
	}

	/**
	 * ref sasaran dan indikator
	 */
	public function get_ref_sasaran_indikator($table)
	{
		$this->load->library('Datatables', null, 'dt');
		$this->dt->set_database('default');
		$this->dt->select("id,nama_$table AS nama_$table")->from('ref_' . $table);
		return $this->dt->generate();
	}
	public function tambah_reff_sasaran_indikator($table, $post)
	{
		$data['nama_' . $table] = $post['nama_' . $table];
		$this->db->insert('ref_' . $table, $data);
		return $this->db->affected_rows();
	}
	public function hapus_reff_sasaran_indikator($table, $id)
	{
		$count = $this->db->where('id_ref_' . $table, $id)->count_all_results('ref_bentuk_ks_' . $table);
		// get id sasaran or bentuk
		$id_tmp = $this->db->where('id_ref_' . $table, $id)->get('ref_bentuk_ks_' . $table)->row();
		$id_var = $id_tmp ? $id_tmp->id : -1;

		// check if id sasaran or bentuk is used in bentuk_ks_detil
		$count_detil =  $this->db->where('id_' . $table, $id_var)->get('bentuk_ks_detil')->num_rows();
		if ($count_detil > 0) return 0;
		return $count > 0 ? 0 : $this->db->where('id', $id)->delete('ref_' . $table);
		// $this->db->where('id', $id)->delete('ref_'.$table);
		// return $this->db->affected_rows();
	}
	public function update_reff_sasaran_indikator($table, $post)
	{
		unset($post['table']);
		$this->db->where('id', $post['id'])->update('ref_' . $table, $post);
	}
	public function checkIndikatorUsed($id_sasaran)
	{
		$this->db->where('id_sasaran', $id_sasaran);
		$this->db->from('ref_bentuk_ks_indikator');
		$count = $this->db->count_all_results();
		return $count > 0;
	}
}

/* End of file Model_ref_bentuk_ks_sasaran.php */
/* Location: ./application/models/Model_ref_bentuk_ks_sasaran.php */