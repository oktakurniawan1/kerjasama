<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Ref Bentuk Ks Sasaran Controller
 *| --------------------------------------------------------------------------
 *| Ref Bentuk Ks Sasaran site
 *|
 */
class Ref_bentuk_ks_sasaran extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_ref_bentuk_ks_sasaran');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * reff Sasaran dan Indikator
	 */
	public function ref_sasaran_indikator(){
		$val = $this->input->get('val');
		$table = ['0'=>'sasaran','indikator'];
		$data['table'] = $table[$val];
		$this->load->view('form-mnj-ref-sasaran-indikator',$data);
	}

	public function browse_reff_sasaran_indikator(){
		echo $this->model_ref_bentuk_ks_sasaran->get_ref_sasaran_indikator($this->input->post('table'));
	}
	public function tambah_reff_sasaran_indikator(){
		$table = $this->input->post('table');
		$status = $this->model_ref_bentuk_ks_sasaran->tambah_reff_sasaran_indikator($table,$this->input->post());
		echo json_encode(['status' => $status]);
	}
	public function hapus_reff_sasaran_indikator(){
		$table = $this->input->get('table');
		$status = $this->model_ref_bentuk_ks_sasaran->hapus_reff_sasaran_indikator($table,$this->input->get('id'));
		echo json_encode(['status' => $status]);
	}

	/**
	 * Input Indikator
	 */
	public function update_reff_sasaran_indikator(){
		$table = $this->input->post('table');
		$this->model_ref_bentuk_ks_sasaran->update_reff_sasaran_indikator($table,$this->input->post());
	}

	public function form_mnj_indikator(){
		$id = $this->input->get('id_sasaran');
		$data['sasaran'] = $this->model_ref_bentuk_ks_sasaran->getInfo($id);
		$tmp = db_get_all_data('ref_indikator');
		$indikator[] = ['value'=>null,'display'=>'Pilih Indikator'];
		foreach($tmp as $row){
			$indikator[] = ['value'=>$row->id,'display'=>$row->nama_indikator];
		}
		$data['indikator'] = json_encode($indikator);
		$data['ref_indikator'] = array_column(obj2arr($tmp),'nama_indikator','id');
		$this->load->view('form-mnj-indikator', $data);
	}

	public function browse_indikator()
	{
		echo $this->model_ref_bentuk_ks_sasaran->getIndikator($this->input->post('id_sasaran'));
	}

	public function update_indikator()
	{
		$this->model_ref_bentuk_ks_sasaran->update_indikator($this->input->post());
	}
	public function tambah_indikator()
	{
		$status = $this->model_ref_bentuk_ks_sasaran->tambah_indikator($this->input->post());
		echo json_encode(['status' => $status]);
	}
	public function hapus_indikator()
	{
		$status = $this->model_ref_bentuk_ks_sasaran->hapus_indikator($this->input->get('id'));
		echo json_encode(['status' => $status]);
	}
	/**
	 * form filter data 
	 */
	public function filter_session_data()
	{
		$fields['id_bentuk_ks'] = $this->input->post('id_bentuk_ks');
		$fields['id_ref_sasaran'] = $this->input->post('id_ref_sasaran');
		$this->session->set_userdata('filterdata_ref_bentuk_ks_sasaran', $fields);
		$this->session->set_userdata('is_filtered_ref_bentuk_ks_sasaran', TRUE);
		echo json_encode(['url' => base_url('administrator/ref_bentuk_ks_sasaran')]);
	}

	public function clear_filter_session_data()
	{
		$this->session->unset_userdata('filterdata_ref_bentuk_ks_sasaran');
		$this->session->unset_userdata('is_filtered_ref_bentuk_ks_sasaran');
		echo json_encode(['url' => base_url('administrator/ref_bentuk_ks_sasaran')]);
	}

	public function form_filter_data()
	{
		if (!$this->session->has_userdata('filterdata_ref_bentuk_ks_sasaran')) {
			$fields['id_bentuk_ks'] = 'all';
			$fields['id_ref_sasaran'] = 'all';
			$this->session->set_userdata('filterdata_ref_bentuk_ks_sasaran', $fields);
		}
		$data['id_bentuk_ks'] = get_data_option('si_bentuk_ks', 'id', 'bentuk_ks', false, ['all', 'ALL']);
		$data['id_ref_sasaran'] = get_data_option('ref_sasaran', 'id', 'nama_sasaran', false, ['all', 'ALL']);
		$this->load->view('form-filter-data', $data);
	}
	/**
	 * show all Ref Bentuk Ks Sasarans
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('ref_bentuk_ks_sasaran_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('ref_bentuk_ks_sasaran_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('ref_bentuk_ks_sasaran_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('ref_bentuk_ks_sasaran_filter');
			$this->session->unset_userdata('ref_bentuk_ks_sasaran_field');
		}
		$filter = $this->session->userdata('ref_bentuk_ks_sasaran_filter');
		$field = $this->session->userdata('ref_bentuk_ks_sasaran_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['ref_bentuk_ks_sasarans'] = $this->model_ref_bentuk_ks_sasaran->get($filter, $field, $this->limit_page, $offset);
		// dd($this->data['ref_bentuk_ks_sasarans']);
		$this->data['ref_bentuk_ks_sasaran_counts'] = $this->model_ref_bentuk_ks_sasaran->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/ref_bentuk_ks_sasaran/index/',
			'total_rows'   => $this->model_ref_bentuk_ks_sasaran->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Ref Bentuk Ks Sasaran List');
		$this->render('backend/standart/administrator/ref_bentuk_ks_sasaran/ref_bentuk_ks_sasaran_list', $this->data);
	}

	/**
	 * Add new ref_bentuk_ks_sasarans
	 *
	 */
	public function add()
	{
		$this->is_allowed('ref_bentuk_ks_sasaran_add');

		$this->template->title('Ref Bentuk Ks Sasaran New');
		$this->render('backend/standart/administrator/ref_bentuk_ks_sasaran/ref_bentuk_ks_sasaran_add', $this->data);
	}

	/**
	 * Add New Ref Bentuk Ks Sasarans
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('ref_bentuk_ks_sasaran_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('id_bentuk_ks', 'Bentuk Kerjasama', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('id_ref_sasaran', 'Nama Sasaran', 'trim|required|max_length[255]');


		if ($this->form_validation->run()) {
			$save_data = [
				'id_bentuk_ks' => $this->input->post('id_bentuk_ks'),
				'id_ref_sasaran' => $this->input->post('id_ref_sasaran'),
			];

			$save_ref_bentuk_ks_sasaran = $this->model_ref_bentuk_ks_sasaran->store($save_data);


			if ($save_ref_bentuk_ks_sasaran) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_ref_bentuk_ks_sasaran;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/ref_bentuk_ks_sasaran/edit/' . $save_ref_bentuk_ks_sasaran, 'Edit Ref Bentuk Ks Sasaran'),
						anchor('administrator/ref_bentuk_ks_sasaran', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/ref_bentuk_ks_sasaran/edit/' . $save_ref_bentuk_ks_sasaran, 'Edit Ref Bentuk Ks Sasaran')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ref_bentuk_ks_sasaran');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ref_bentuk_ks_sasaran');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Ref Bentuk Ks Sasarans
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('ref_bentuk_ks_sasaran_update');

		$this->data['ref_bentuk_ks_sasaran'] = $this->model_ref_bentuk_ks_sasaran->find($id);

		$this->template->title('Ref Bentuk Ks Sasaran Update');
		$this->render('backend/standart/administrator/ref_bentuk_ks_sasaran/ref_bentuk_ks_sasaran_update', $this->data);
	}

	/**
	 * Update Ref Bentuk Ks Sasarans
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('ref_bentuk_ks_sasaran_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('id_bentuk_ks', 'Bentuk Kerjasama', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('id_ref_sasaran', 'Nama Sasaran', 'trim|required|max_length[255]');

		if ($this->form_validation->run()) {

			$save_data = [
				'id_bentuk_ks' => $this->input->post('id_bentuk_ks'),
				'id_ref_sasaran' => $this->input->post('id_ref_sasaran'),
			];


			$save_ref_bentuk_ks_sasaran = $this->model_ref_bentuk_ks_sasaran->change($id, $save_data);

			if ($save_ref_bentuk_ks_sasaran) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/ref_bentuk_ks_sasaran', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ref_bentuk_ks_sasaran/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ref_bentuk_ks_sasaran/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Ref Bentuk Ks Sasarans
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('ref_bentuk_ks_sasaran_delete');
		// cek indikator used
		$check = $this->model_ref_bentuk_ks_sasaran->checkIndikatorUsed($id);
		if ($check) {
			set_message('Data ini sudah digunakan di Indikator, tidak bisa dihapus', 'error');
			redirect_back();
		}

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'ref_bentuk_ks_sasaran'), 'success');
		} else {
			set_message(cclang('error_delete', 'ref_bentuk_ks_sasaran'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Ref Bentuk Ks Sasarans
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('ref_bentuk_ks_sasaran_view');

		$this->data['ref_bentuk_ks_sasaran'] = $this->model_ref_bentuk_ks_sasaran->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Ref Bentuk Ks Sasaran Detail');
		$this->render('backend/standart/administrator/ref_bentuk_ks_sasaran/ref_bentuk_ks_sasaran_view', $this->data);
	}

	/**
	 * delete Ref Bentuk Ks Sasarans
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$ref_bentuk_ks_sasaran = $this->model_ref_bentuk_ks_sasaran->find($id);



		return $this->model_ref_bentuk_ks_sasaran->remove($id);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
			'id_bentuk_ks',
			'id_ref_sasaran',
		];
		$data = $this->model_ref_bentuk_ks_sasaran->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_ref_bentuk_ks_sasaran->export2($data, 'file', $selected_field);

		/*
		$this->is_allowed('ref_bentuk_ks_sasaran_export');

		$this->model_ref_bentuk_ks_sasaran->export(
			'ref_bentuk_ks_sasaran', 
			'ref_bentuk_ks_sasaran',
			$this->model_ref_bentuk_ks_sasaran->field_search
		);
		*/
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('ref_bentuk_ks_sasaran_export');

		$this->model_ref_bentuk_ks_sasaran->pdf('ref_bentuk_ks_sasaran', 'ref_bentuk_ks_sasaran');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('ref_bentuk_ks_sasaran_export');

		$table = $title = 'ref_bentuk_ks_sasaran';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_ref_bentuk_ks_sasaran->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
}


/* End of file ref_bentuk_ks_sasaran.php */
/* Location: ./application/controllers/administrator/Ref Bentuk Ks Sasaran.php */