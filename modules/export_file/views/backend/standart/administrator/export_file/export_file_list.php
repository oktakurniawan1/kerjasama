<section class="content-header">
   <h1>
      <?= cclang('export_file') ?><small><?= cclang('list_all'); ?></small>
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?= cclang('export_file') ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">

      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                     <div class="row pull-right">
                     </div>
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username"><?= cclang('export_file') ?></h3>
                     <h5 class="widget-user-desc"><?= cclang('list_all', [cclang('export_file')]); ?> <i class="label bg-yellow"><?= $export_file_counts; ?> <?= cclang('items'); ?></i></h5>
                  </div>
                  
                  <div class="alert alert-warning">
                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                     <strong>Perhatian</strong> File akan terhapus secara otomatis setelah 1 jam.
                  </div>
                  
                  <form name="form_export_file" id="form_export_file" action="<?= base_url('administrator/export_file/index'); ?>">
                     <div class="row">
                        <div class="col-md-8">
                           <div class="col-sm-3 padd-left-0  ">
                              <input type="text" class="form-control" name="q" id="filter" placeholder="<?= cclang('filter'); ?>" value="<?= $this->session->userdata('export_file_filter'); ?>">
                           </div>
                           <div class="col-sm-3 padd-left-0 ">
                              <select type="text" class="form-control chosen chosen-select" name="f" id="field">
                                 <option value=""><?= cclang('all'); ?></option>
                                 <option <?= $this->session->userdata('export_file_field') == 'title' ? 'selected' : ''; ?> value="title">Sumber data</option>
                              </select>
                           </div>
                           <div class="col-sm-1 padd-left-0 ">
                              <button type="submit" class="btn btn-flat" name="sbtn" id="sbtn" value="Apply" title="<?= cclang('filter_search'); ?>">
                                 Filter
                              </button>
                           </div>
                           <div class="col-sm-1 padd-left-0 ">
                              <a class="btn btn-default btn-flat" name="reset" id="reset" value="Apply" href="<?= base_url('administrator/export_file/?reset_filter='); ?>" title="<?= cclang('reset_filter'); ?>">
                                 <i class="fa fa-undo"></i>
                              </a>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="dataTables_paginate paging_simple_numbers pull-right" id="example2_paginate">
                              <?= $pagination; ?>
                           </div>
                        </div>
                     </div>
                     <div class="table-responsive">
                        <table class="table table-bordered table-striped dataTable">
                           <thead>
                              <tr class="">
                                 <th data-field="title" data-sort="1" data-primary-key="0">ID</th>
                                 <th data-field="title" data-sort="1" data-primary-key="0">Sumber data</th>
                                 <th data-field="is_finish" data-sort="1" data-primary-key="0"> <?= cclang('is_finish') ?></th>
                                 <th data-field="file_name" data-sort="0" data-primary-key="0"> <?= cclang('file_name') ?></th>
                                 <th data-field="created_at" data-sort="1" data-primary-key="0"> <?= cclang('created_at') ?></th>
                              </tr>
                           </thead>
                           <tbody id="tbody_export_file">
                              <?php foreach ($export_files as $export_file) : ?>
                                 <tr>
                                    <td><code><?= _ent($export_file->id); ?></code></td>
                                    <td><?= _ent($export_file->title); ?></td>
                                    <td>
                                       <span class="label <?= $export_file->is_finish ? 'label-success' : 'label-warning' ?>">
                                          <?= $export_file->is_finish == 1 ? 'selesai' : 'proses' ?>
                                       </span>
                                    </td>
                                    <td>
                                       <?php if ($export_file->is_finish == 1) : ?>
                                          <?php if (is_image($export_file->file_name)) : ?>
                                             <a class="fancybox" rel="group" href="<?= BASE_URL . 'uploads/export_file/' . $export_file->file_name; ?>">
                                                <img src="<?= BASE_URL . 'uploads/excel/' . $export_file->file_name; ?>" class="image-responsive" alt="image export_file" title="file_name export_file" width="40px">
                                             </a>
                                          <?php else : ?>
                                             <a href="<?= BASE_URL . 'uploads/excel/' . $export_file->file_name; ?>">
                                                <img src="<?= get_icon_file($export_file->file_name); ?>" class="image-responsive image-icon" alt="image export_file" title="file_name <?= $export_file->file_name; ?>" width="40px">
                                             </a>
                                          <?php endif; ?>
                                       <?php endif; ?>
                                    </td>

                                    <td><code><?= time_ago($export_file->created_at); ?></code></td>
                                 </tr>
                              <?php endforeach; ?>
                              <?php if ($export_file_counts == 0) : ?>
                                 <tr>
                                    <td colspan="100">
                                       Download Export Excel data is not available
                                    </td>
                                 </tr>
                              <?php endif; ?>

                           </tbody>
                        </table>
                     </div>
               </div>
               <!-- /.widget-user -->
               <hr>
               </form>
               <div class="row">
                  <div class="col-md-4 col-md-offset-8">
                     <div class="dataTables_paginate paging_simple_numbers pull-right" id="example2_paginate">
                        <?= $pagination; ?>
                     </div>
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>