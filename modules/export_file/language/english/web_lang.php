<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['export_file']    = 'Download Export Excel';
$lang['id']             = 'Id';
$lang['user_id']        = 'User Id';
$lang['title']          = 'Sumber data';
$lang['format']         = 'Format';
$lang['selected_field'] = 'Selected Field';
$lang['sql_query']      = 'Sql Query';
$lang['is_finish']      = 'Status Proses';
$lang['file_name']      = 'File Hasil';
$lang['created_at']     = 'Created At';
$lang['deleted_at']     = 'Deleted At';
