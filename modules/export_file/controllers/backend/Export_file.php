<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Export File Controller
*| --------------------------------------------------------------------------
*| Export File site
*|
*/
class Export_file extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_export_file');
		$this->lang->load('web_lang', $this->current_lang);
	}

		/**
	* show all Export Files
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if(!is_null($q)) $this->session->set_userdata('export_file_filter',$q);
		if(!is_null($f)) $this->session->set_userdata('export_file_field',$f);
		$this->session->set_userdata('_page',$offset);

		if(!is_null($this->input->get('reset_filter'))){
			$this->session->unset_userdata('export_file_filter');
			$this->session->unset_userdata('export_file_field');
		}
		$filter = $this->session->userdata('export_file_filter');
		$field = $this->session->userdata('export_file_field');
		$offset = $this->session->userdata('_page',$offset);

		$this->data['export_files'] = $this->model_export_file->get($filter, $field, $this->limit_page, $offset);
		$this->data['export_file_counts'] = $this->model_export_file->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/export_file/index/',
			'total_rows'   => $this->model_export_file->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Download Export Excel List');
		$this->render('backend/standart/administrator/export_file/export_file_list', $this->data);
	}
	
}
