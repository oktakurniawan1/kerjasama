<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Telegram Notif Controller
 *| --------------------------------------------------------------------------
 *| Telegram Notif site
 *|
 */
class Telegram_notif extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_telegram_notif');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * form filter data 
	 */
	public function filter_session_data()
	{
		$fields['nip'] = $this->input->post('nip');
		$fields['notif_1'] = $this->input->post('notif_1');
		$fields['notif_2'] = $this->input->post('notif_2');
		$fields['notif_3'] = $this->input->post('notif_3');
		$fields['notif_4'] = $this->input->post('notif_4');
		$fields['notif_5'] = $this->input->post('notif_5');
		$fields['notif_6'] = $this->input->post('notif_6');
		$this->session->set_userdata('filterdata_telegram_notif', $fields);
		$this->session->set_userdata('is_filtered_telegram_notif', TRUE);
		echo json_encode(['url' => base_url('administrator/telegram_notif')]);
	}

	public function clear_filter_session_data()
	{
		$this->session->unset_userdata('filterdata_telegram_notif');
		$this->session->unset_userdata('is_filtered_telegram_notif');
		echo json_encode(['url' => base_url('administrator/telegram_notif')]);
	}

	public function form_filter_data()
	{
		if (!$this->session->has_userdata('filterdata_telegram_notif')) {
			$fields['nip'] = 'all';
			$fields['notif_1'] = 'all';
			$fields['notif_2'] = 'all';
			$fields['notif_3'] = 'all';
			$fields['notif_4'] = 'all';
			$fields['notif_5'] = 'all';
			$fields['notif_6'] = 'all';
			$this->session->set_userdata('filterdata_telegram_notif', $fields);
		}
		$data['nip'] = [];
		$data['notif_1'] = [];
		$data['notif_2'] = [];
		$data['notif_3'] = [];
		$data['notif_4'] = [];
		$data['notif_5'] = [];
		$data['notif_6'] = [];
		$this->load->view('form-filter-data', $data);
	}
	/**
	 * show all Telegram Notifs
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('telegram_notif_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('telegram_notif_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('telegram_notif_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('telegram_notif_filter');
			$this->session->unset_userdata('telegram_notif_field');
		}
		$filter = $this->session->userdata('telegram_notif_filter');
		$field = $this->session->userdata('telegram_notif_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['telegram_notifs'] = $this->model_telegram_notif->get($filter, $field, $this->limit_page, $offset);
		$this->data['telegram_notif_counts'] = $this->model_telegram_notif->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/telegram_notif/index/',
			'total_rows'   => $this->model_telegram_notif->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];
		$this->data['pagination'] = $this->pagination($config);
		$this->data['notif'] = $this->config->item('notif_telegram');
		$this->template->title('Telegram Notif List');
		$this->render('backend/standart/administrator/telegram_notif/telegram_notif_list', $this->data);
	}

	/**
	 * Add new telegram_notifs
	 *
	 */
	public function add()
	{
		$this->is_allowed('telegram_notif_add');
		$this->data['notif'] = $this->config->item('notif_telegram');
		$this->template->title('Telegram Notif New');
		$this->render('backend/standart/administrator/telegram_notif/telegram_notif_add', $this->data);
	}

	/**
	 * Add New Telegram Notifs
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('telegram_notif_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('nip', 'Karyawan', 'trim|required|max_length[50]');


		if ($this->form_validation->run()) {

			$save_data = [
				'nip' => $this->input->post('nip'),
				'notif_1' => $this->input->post('notif_1'),
				'notif_2' => $this->input->post('notif_2'),
				'notif_3' => $this->input->post('notif_3'),
				'notif_4' => $this->input->post('notif_4'),
				'notif_5' => $this->input->post('notif_5'),
				'notif_6' => $this->input->post('notif_6'),
			];


			$save_telegram_notif = $this->model_telegram_notif->store($save_data);


			if ($save_telegram_notif) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_telegram_notif;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/telegram_notif/edit/' . $save_telegram_notif, 'Edit Telegram Notif'),
						anchor('administrator/telegram_notif', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/telegram_notif/edit/' . $save_telegram_notif, 'Edit Telegram Notif')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/telegram_notif');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/telegram_notif');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Telegram Notifs
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('telegram_notif_update');

		$this->data['telegram_notif'] = $this->model_telegram_notif->find($id);
		$this->data['notif'] = $this->config->item('notif_telegram');
		$this->template->title('Telegram Notif Update');
		$this->render('backend/standart/administrator/telegram_notif/telegram_notif_update', $this->data);
	}

	/**
	 * Update Telegram Notifs
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('telegram_notif_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('nip', 'Karyawan', 'trim|required|max_length[50]');

		if ($this->form_validation->run()) {

			$save_data = [
				'nip' => $this->input->post('nip'),
				'notif_1' => $this->input->post('notif_1'),
				'notif_2' => $this->input->post('notif_2'),
				'notif_3' => $this->input->post('notif_3'),
				'notif_4' => $this->input->post('notif_4'),
				'notif_5' => $this->input->post('notif_5'),
				'notif_6' => $this->input->post('notif_6'),
			];


			$save_telegram_notif = $this->model_telegram_notif->change($id, $save_data);

			if ($save_telegram_notif) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/telegram_notif', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/telegram_notif/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/telegram_notif/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Telegram Notifs
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('telegram_notif_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'telegram_notif'), 'success');
		} else {
			set_message(cclang('error_delete', 'telegram_notif'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Telegram Notifs
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('telegram_notif_view');

		$this->data['telegram_notif'] = $this->model_telegram_notif->join_avaiable()->filter_avaiable()->find($id);
		$this->data['notif'] = $this->config->item('notif_telegram');
		$this->template->title('Telegram Notif Detail');
		$this->render('backend/standart/administrator/telegram_notif/telegram_notif_view', $this->data);
	}

	/**
	 * delete Telegram Notifs
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$telegram_notif = $this->model_telegram_notif->find($id);



		return $this->model_telegram_notif->remove($id);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
			'nip',
			'notif_1',
			'notif_2',
			'notif_3',
			'notif_4',
			'notif_5',
			'notif_6',
		];
		$data = $this->model_telegram_notif->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_telegram_notif->export2($data, 'file', $selected_field);

		/*
		$this->is_allowed('telegram_notif_export');

		$this->model_telegram_notif->export(
			'telegram_notif', 
			'telegram_notif',
			$this->model_telegram_notif->field_search
		);
		*/
	}
	public function kirim_telegram()
	{
		$table = $this->input->get('modul');
		$id = $this->input->get('id');
		$rowdata = $this->db->select('*')->where('id',$id)->get($table)->row();
		$nips = $this->db->select('username,full_name')
			->where("INSTR(prodi,$rowdata->kd_prodi)>",0)
			->where("username REGEXP '^[[:digit:]]+$'")
			->where_not_in('id',[10,220,212,213,215,296])
			->get('aauth_users')->result_array();
		$nips = array_column($nips,'full_name','username');
		$data['nips'] = $nips;
		// $data['nip'] = array_keys($nips);
		$data['doc_nomor'] = isset($rowdata->doc_nomor) ? $rowdata->doc_nomor : '';
		$this->load->view('form-send-telegram',$data);
		
	}
	public function antrian_telegram(){
		$username = $this->input->post('username');
		if(is_array($username)){
			$data['pesan'] = $this->input->post('pesan');
			foreach($username as $nip){
				$data['identitas'] = $nip;
				$data['waktu'] = date('Y-m-d H:i:s');
				$this->db->insert('antrian_telegram',$data);
			}
		}
	}
	
}


/* End of file telegram_notif.php */
/* Location: ./application/controllers/administrator/Telegram Notif.php */