<form action="" method="POST" role="form">
	<div class="form-group">
		<label for="">Daftar Prodi Pengelola</label>
		<?= form_dropdown('username[]',$nips,null, ['multiple' => 'multiple','style' => 'width:100%']) ?>
	</div>
	<div class="form-group">
		<label for="">Pesan</label>
		<textarea name="pesan" id="pesan" rows="5" class='form-control'><?= $doc_nomor?></textarea>
	</div>

</form>
<script>
	$(function() {
		$("select[name='username[]']").select2({
			theme: 'classic',
			dropdownParent: $('#form-kirim-telegram-dialog'),
			// tags: true
		})
	})
</script>