<div class="row">
    <div class="col-12">
        <div class="card-box">

            <div class="row">
                <div class="col-lg-6">

                    <div class="demo-box">
                        <h4 class="header-title">Line Chart</h4>
                        <p class="sub-header">
                            A line chart is a way of plotting data points on a line. Often, it is used to show trend data, and the comparison of two data sets.
                        </p>

                        <canvas id="lineChart" height="300"></canvas>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="demo-box mt-5 mt-lg-0">
                        <h4 class="header-title">Bar Chart</h4>
                        <p class="sub-header">
                            A bar chart is a way of showing data as bars. It is sometimes used to show trend data, and the comparison of multiple data sets side by side.
                        </p>

                        <canvas id="bar" height="300"></canvas>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row mt-5">
                <div class="col-lg-6 mt-2">
                    <div class="demo-box">
                        <h4 class="header-title">Pie Chart</h4>
                        <p class="sub-header">
                            Pie and doughnut charts are probably the most commonly used chart there are. They are divided into segments, the arc of each segment shows the proportional value of each piece of data.
                        </p>

                        <canvas id="pie" height="260"></canvas>

                    </div>
                </div>

                <div class="col-lg-6 mt-2">

                    <div class="demo-box mt-5 mt-lg-0">
                        <h4 class="header-title">Donut Chart</h4>
                        <p class="sub-header">
                            Pie and doughnut charts are probably the most commonly used chart there are. They are divided into segments, the arc of each segment shows the proportional value of each piece of data.
                        </p>

                        <canvas id="doughnut" height="260"></canvas>

                    </div>
                </div>

            </div>
            <!-- end row -->

            <div class="row mt-5">

                <div class="col-lg-6 mt-2">

                    <div class="demo-box">
                        <h4 class="header-title">Polar area Chart</h4>
                        <p class="sub-header">
                            Polar area charts are similar to pie charts, but each segment has the same angle - the radius of the segment differs depending on the value.
                        </p>

                        <canvas id="polarArea" height="300"> </canvas>

                    </div>
                </div>

                <div class="col-lg-6 mt-2">
                    <div class="demo-box mt-5 mt-lg-0">
                        <h4 class="header-title">Radar Chart</h4>
                        <p class="sub-header">
                            A radar chart is a way of showing multiple data points and the variation between them. They are often useful for comparing the points of two or more different data sets.
                        </p>

                        <canvas id="radar" height="300"></canvas>

                    </div>
                </div>

            </div>
            <!-- end row -->

        </div>
        <!-- card-box -->
    </div>
    <!-- end col-->

</div>
<!-- end row -->

<!-- Chart JS -->
<script src="<?= theme_zircos(); ?>libs/chart-js/Chart.bundle.min.js"></script>
<script src="<?= theme_zircos(); ?>js/pages/chartjs.init.js"></script>
