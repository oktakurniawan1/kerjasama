<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*| --------------------------------------------------------------------------
*| Page Controller
*| --------------------------------------------------------------------------
*| For default controller
*|
*/
class Beranda extends Front
{

	public function __construct()
	{
		parent::__construct();
	}
    public function statistik_fakultas()
    {
		$this->data['content'] = $this->load->view('statistik-fakultas',[],true);
		$this->template->build('template',$this->data);
	}
    public function regstudent_mob()
    {
		$this->data['form'] = form_builder(1);
		$this->data['title'] = 'Registrasi Student Mobility';
		$this->template->build('form-template',$this->data);
	}
	public function form_regstudent_mob()
	{
		$this->data['content'] = '<div class="embed-responsive embed-responsive-16by9">
				<iframe  class="embed-responsive-item" src="'.site_url('front/regstudent_mob').'" allowfullscreen></iframe>
				  </div>';
		$this->template->build('template',$this->data);
	}
}
