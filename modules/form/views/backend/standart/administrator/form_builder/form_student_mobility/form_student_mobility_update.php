
<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Student Mobility        <small><?= cclang('update'); ?> Student Mobility</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/manage-form/form_student_mobility'); ?>">Student Mobility</a></li>
        <li class="active"><?= cclang('update'); ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Student Mobility</h3>
                            <h5 class="widget-user-desc">Edit Student Mobility</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/manage-form/form_student_mobility/edit_save/'.$this->uri->segment(4)), [
                            'name'    => 'form_form_student_mobility', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_form_student_mobility', 
                            'method'  => 'POST'
                            ]); ?>
                         
                                                <div class="form-group ">
                            <label for="nim" class="col-sm-2 control-label">NIM 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nim" value="<?= set_value('nim', $form_student_mobility->nim); ?>" id="nim" placeholder=""  >
                                <small class="info help-block">
                                <b>Format NIM must</b> Integer,  <b>Input NIM</b> Max Length : 10.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="nama" class="col-sm-2 control-label">Nama 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nama" value="<?= set_value('nama', $form_student_mobility->nama); ?>" id="nama" placeholder=""  >
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="dn_ln" class="col-sm-2 control-label">DN/LN 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="dn_ln" id="dn_ln" data-placeholder="Select DN/LN" >
                                    <option value=""></option>
                                    <option <?= $form_student_mobility->dn_ln == "DL" ? 'selected' :''; ?> value="DL">Dalam negeri</option>
                                    <option <?= $form_student_mobility->dn_ln == "LN" ? 'selected' :''; ?> value="LN">Luar negeri</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="id_outbound" class="col-sm-2 control-label">Jenis Kegiatan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="id_outbound" id="id_outbound" data-placeholder="Select Jenis Kegiatan"  >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('si_ref_kegiatan') as $row): ?>
                                    <option <?=  $row->id ==  $form_student_mobility->id_outbound ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->keg_outbound; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="email" class="col-sm-2 control-label">Email 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" name="email" value="<?= set_value('email', $form_student_mobility->email); ?>" id="email" placeholder=""  >
                                <small class="info help-block">
                                <b>Format Email must</b> Valid Email.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="no_tlp" class="col-sm-2 control-label">Nomer Telepon (HP) 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="no_tlp" value="<?= set_value('no_tlp', $form_student_mobility->no_tlp); ?>" id="no_tlp" placeholder=""  >
                                <small class="info help-block">
                                <b>Format Nomer Telepon (HP) must</b> Alpha Numeric.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="no_tlp2" class="col-sm-2 control-label">Nomor Telepon Darurat 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="no_tlp2" value="<?= set_value('no_tlp2', $form_student_mobility->no_tlp2); ?>" id="no_tlp2" placeholder=""  >
                                <small class="info help-block">
                                <b>Format Nomor Telepon Darurat must</b> Integer.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="file_ktp" class="col-sm-2 control-label">File KTP 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="form_student_mobility_file_ktp_galery" ></div>
                                <input class="data_file data_file_uuid" name="form_student_mobility_file_ktp_uuid" id="form_student_mobility_file_ktp_uuid" type="hidden" value="<?= set_value('form_student_mobility_file_ktp_uuid'); ?>">
                                <input class="data_file" name="form_student_mobility_file_ktp_name" id="form_student_mobility_file_ktp_name" type="hidden" value="<?= set_value('form_student_mobility_file_ktp_name', $form_student_mobility->file_ktp); ?>">
                                <small class="info help-block">
                                <b>Extension file must</b> JPG,PDF,  <b>Max size file</b>  100 kb.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="file_loa" class="col-sm-2 control-label">File Loa 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="form_student_mobility_file_loa_galery" ></div>
                                <input class="data_file data_file_uuid" name="form_student_mobility_file_loa_uuid" id="form_student_mobility_file_loa_uuid" type="hidden" value="<?= set_value('form_student_mobility_file_loa_uuid'); ?>">
                                <input class="data_file" name="form_student_mobility_file_loa_name" id="form_student_mobility_file_loa_name" type="hidden" value="<?= set_value('form_student_mobility_file_loa_name', $form_student_mobility->file_loa); ?>">
                                <small class="info help-block">
                                <b>Extension file must</b> PDF,JPG,  <b>Max size file</b>  100 kb.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="tgl_mulai" class="col-sm-2 control-label">Tanggal Mulai 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datepicker" value="<?= set_value('tgl_mulai', $form_student_mobility->tgl_mulai); ?>"  name="tgl_mulai"  placeholder="" id="tgl_mulai" >
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="tgl_selesai" class="col-sm-2 control-label">Tanggal Selesai 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datepicker" value="<?= set_value('tgl_selesai', $form_student_mobility->tgl_selesai); ?>"  name="tgl_selesai"  placeholder="" id="tgl_selesai" >
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="negara_tujuan" class="col-sm-2 control-label">Negara Tujuan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="negara_tujuan" value="<?= set_value('negara_tujuan', $form_student_mobility->negara_tujuan); ?>" id="negara_tujuan" placeholder=""  >
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="ket_pembiayaan" class="col-sm-2 control-label">Keterangan Pembiayaan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="ket_pembiayaan" value="<?= set_value('ket_pembiayaan', $form_student_mobility->ket_pembiayaan); ?>" id="ket_pembiayaan" placeholder=""  >
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="cancel (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_field_data'); ?></i>
                            </span>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){
      
             
      $('#btn_cancel').click(function(){
        swal({
            title: "Are you sure?",
            text: "the data that you have created will be in the exhaust!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/manage-form/form_student_mobility';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_form_student_mobility = $('#form_form_student_mobility');
        var data_post = form_form_student_mobility.serializeArray();
        var save_type = $(this).attr('data-stype');
        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: form_form_student_mobility.attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          if(res.success) {
            var id = $('#form_student_mobility_image_galery').find('li').attr('qq-file-id');
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            $('.data_file_uuid').val('');
    
          } else {
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
              var params = {};
       params[csrf] = token;

       $('#form_student_mobility_file_ktp_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/manage-form/form_student_mobility/upload_file_ktp_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + '/administrator/manage-form/form_student_mobility/delete_file_ktp_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/manage-form/form_student_mobility/get_file_ktp_file/<?= $form_student_mobility->id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["jpg","pdf"],
              sizeLimit : 102400,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#form_student_mobility_file_ktp_galery').fineUploader('getUuid', id);
                   $('#form_student_mobility_file_ktp_uuid').val(uuid);
                   $('#form_student_mobility_file_ktp_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#form_student_mobility_file_ktp_uuid').val();
                  $.get(BASE_URL + '/administrator/manage-form/form_student_mobility/delete_file_ktp_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#form_student_mobility_file_ktp_uuid').val('');
                  $('#form_student_mobility_file_ktp_name').val('');
                }
              }
          }
      }); /*end file_ktp galey*/
              var params = {};
       params[csrf] = token;

       $('#form_student_mobility_file_loa_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/manage-form/form_student_mobility/upload_file_loa_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + '/administrator/manage-form/form_student_mobility/delete_file_loa_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/manage-form/form_student_mobility/get_file_loa_file/<?= $form_student_mobility->id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["pdf","jpg"],
              sizeLimit : 102400,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#form_student_mobility_file_loa_galery').fineUploader('getUuid', id);
                   $('#form_student_mobility_file_loa_uuid').val(uuid);
                   $('#form_student_mobility_file_loa_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#form_student_mobility_file_loa_uuid').val();
                  $.get(BASE_URL + '/administrator/manage-form/form_student_mobility/delete_file_loa_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#form_student_mobility_file_loa_uuid').val('');
                  $('#form_student_mobility_file_loa_name').val('');
                }
              }
          }
      }); /*end file_loa galey*/
           
    
    }); /*end doc ready*/
</script>