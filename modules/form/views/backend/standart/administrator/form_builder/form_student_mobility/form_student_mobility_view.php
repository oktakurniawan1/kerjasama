
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
//This page is a result of an autogenerated content made by running test.html with firefox.
function domo(){
 
   // Binding keys
   $('*').bind('keydown', 'Ctrl+e', function assets() {
      $('#btn_edit').trigger('click');
       return false;
   });

   $('*').bind('keydown', 'Ctrl+x', function assets() {
      $('#btn_back').trigger('click');
       return false;
   });
    
}


jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
      Student Mobility      <small><?= cclang('detail', 'Student Mobility'); ?></small>
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class=""><a  href="<?= site_url('administrator/manage-form/form_student_mobility'); ?>">Student Mobility</a></li>
      <li class="active"><?= cclang('detail'); ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
     
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">

               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                    
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/view.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Student Mobility</h3>
                     <h5 class="widget-user-desc">Detail Student Mobility</h5>
                     <hr>
                  </div>

                 
                  <div class="form-horizontal" name="form_form_student_mobility" id="form_form_student_mobility" >
                   
                    <div class="form-group option">               
                  <label for="content" class="col-sm-2 control-label">NIM </label>

                        <div class="col-sm-8">
                           <?= _ent($form_student_mobility->nim); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group option">               
                  <label for="content" class="col-sm-2 control-label">Nama </label>

                        <div class="col-sm-8">
                           <?= _ent($form_student_mobility->nama); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group option">               
                  <label for="content" class="col-sm-2 control-label">DN/LN </label>

                        <div class="col-sm-8">
                           <?= _ent($form_student_mobility->dn_ln); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group option">               
                  <label for="content" class="col-sm-2 control-label">Jenis Kegiatan </label>

                        <div class="col-sm-8">
                           <?= _ent($form_student_mobility->id_outbound); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group option">               
                  <label for="content" class="col-sm-2 control-label">Email </label>

                        <div class="col-sm-8">
                           <?= _ent($form_student_mobility->email); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group option">               
                  <label for="content" class="col-sm-2 control-label">Nomer Telepon (HP) </label>

                        <div class="col-sm-8">
                           <?= _ent($form_student_mobility->no_tlp); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group option">               
                  <label for="content" class="col-sm-2 control-label">Nomor Telepon Darurat </label>

                        <div class="col-sm-8">
                           <?= _ent($form_student_mobility->no_tlp2); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label"> File KTP </label>
                        <div class="col-sm-8">
                             <?php if (is_image($form_student_mobility->file_ktp)): ?>
                              <a class="fancybox" rel="group" href="<?= BASE_URL . 'uploads/form_student_mobility/' . $form_student_mobility->file_ktp; ?>">
                                <img src="<?= BASE_URL . 'uploads/form_student_mobility/' . $form_student_mobility->file_ktp; ?>" class="image-responsive" alt="image form_student_mobility" title="File KTP form_student_mobility" width="40px">
                              </a>
                              <?php else: ?>
                              <label>
                                <a href="<?= BASE_URL . 'administrator/manage-form/file/download/form_student_mobility/' . $form_student_mobility->file_ktp; ?>">
                                 <img src="<?= get_icon_file($form_student_mobility->file_ktp); ?>" class="image-responsive" alt="image form_student_mobility" title="File KTP <?= $form_student_mobility->file_ktp; ?>" width="40px"> 
                               <?= $form_student_mobility->file_ktp ?>
                               </a>
                               </label>
                              <?php endif; ?>
                        </div>
                    </div>
                                       
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label"> File Loa </label>
                        <div class="col-sm-8">
                             <?php if (is_image($form_student_mobility->file_loa)): ?>
                              <a class="fancybox" rel="group" href="<?= BASE_URL . 'uploads/form_student_mobility/' . $form_student_mobility->file_loa; ?>">
                                <img src="<?= BASE_URL . 'uploads/form_student_mobility/' . $form_student_mobility->file_loa; ?>" class="image-responsive" alt="image form_student_mobility" title="File_loa form_student_mobility" width="40px">
                              </a>
                              <?php else: ?>
                              <label>
                                <a href="<?= BASE_URL . 'administrator/manage-form/file/download/form_student_mobility/' . $form_student_mobility->file_loa; ?>">
                                 <img src="<?= get_icon_file($form_student_mobility->file_loa); ?>" class="image-responsive" alt="image form_student_mobility" title="File_loa <?= $form_student_mobility->file_loa; ?>" width="40px"> 
                               <?= $form_student_mobility->file_loa ?>
                               </a>
                               </label>
                              <?php endif; ?>
                        </div>
                    </div>
                                       
                    <div class="form-group option">               
                  <label for="content" class="col-sm-2 control-label">Tanggal Mulai </label>

                        <div class="col-sm-8">
                           <?= _ent($form_student_mobility->tgl_mulai); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group option">               
                  <label for="content" class="col-sm-2 control-label">Tanggal Selesai </label>

                        <div class="col-sm-8">
                           <?= _ent($form_student_mobility->tgl_selesai); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group option">               
                  <label for="content" class="col-sm-2 control-label">Negara Tujuan </label>

                        <div class="col-sm-8">
                           <?= _ent($form_student_mobility->negara_tujuan); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group option">               
                  <label for="content" class="col-sm-2 control-label">Keterangan Pembiayaan </label>

                        <div class="col-sm-8">
                           <?= _ent($form_student_mobility->ket_pembiayaan); ?>
                        </div>
                    </div>
                                        
                    <br>
                    <br>

                    <div class="view-nav">
                        <?php is_allowed('form_student_mobility_update', function() use ($form_student_mobility){?>
                        <a class="btn btn-flat btn-info btn_edit btn_action" id="btn_edit" data-stype='back' title="<?= cclang('update', 'form_student_mobility'); ?> (Ctrl+e)" href="<?= site_url('administrator/manage-form/form_student_mobility/edit/'.$form_student_mobility->id); ?>"><i class="fa fa-edit" ></i> <?= cclang('update', 'Form Student Mobility'); ?></a>
                        <?php }) ?>
                        <a class="btn btn-flat btn-default btn_action" id="btn_back" title="back (Ctrl+x)" href="<?= site_url('administrator/manage-form/form_student_mobility/'); ?>"><i class="fa fa-undo" ></i> <?= cclang('go_list', 'Form Student Mobility'); ?></a>
                     </div>
                    
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->

      </div>
   </div>
</section>
<!-- /.content -->
