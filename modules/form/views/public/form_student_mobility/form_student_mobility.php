
<script src="<?= BASE_ASSET; ?>js/custom.js"></script>

<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>

<?php $this->load->view('core_template/fine_upload'); ?>

<?= form_open('', [
    'name'    => 'form_form_student_mobility', 
    'class'   => 'form-horizontal form_form_student_mobility', 
    'id'      => 'form_form_student_mobility',
    'enctype' => 'multipart/form-data', 
    'method'  => 'POST'
]); ?>
 
<div class="form-group ">
    <label for="nim" class="col-sm-2 control-label">NIM 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="nim" id="nim" placeholder=""  >
        <small class="info help-block">
        <b>Format NIM must</b> Integer,  <b>Input NIM</b> Max Length : 10.</small>
    </div>
</div>
 
<div class="form-group ">
    <label for="nama" class="col-sm-2 control-label">Nama 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="nama" id="nama" placeholder=""  >
        <small class="info help-block">
        </small>
    </div>
</div>
 
<div class="form-group ">
    <label for="dn_ln" class="col-sm-2 control-label">DN/LN 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <select  class="form-control chosen chosen-select" name="dn_ln" id="dn_ln" data-placeholder="Select DN/LN" >
            <option value=""></option>
            <option value="DL">Dalam negeri</option>
            <option value="LN">Luar negeri</option>
            </select>
        <small class="info help-block">
        </small>
    </div>
</div>
 
<div class="form-group ">
    <label for="id_outbound" class="col-sm-2 control-label">Jenis Kegiatan 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <select  class="form-control chosen chosen-select-deselect" name="id_outbound" id="id_outbound" data-placeholder="Select Jenis Kegiatan"  >
            <option value=""></option>
            <?php foreach (db_get_all_data('si_ref_kegiatan') as $row): ?>
            <option value="<?= $row->id ?>"><?= $row->keg_outbound; ?></option>
            <?php endforeach; ?>  
        </select>
        <small class="info help-block">
        </small>
    </div>
</div>

 
<div class="form-group ">
    <label for="email" class="col-sm-2 control-label">Email 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <input type="email" class="form-control" name="email" id="email" placeholder=""  >
        <small class="info help-block">
        <b>Format Email must</b> Valid Email.</small>
    </div>
</div>
 
<div class="form-group ">
    <label for="no_tlp" class="col-sm-2 control-label">Nomer Telepon (HP) 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="no_tlp" id="no_tlp" placeholder=""  >
        <small class="info help-block">
        <b>Format Nomer Telepon (HP) must</b> Alpha Numeric.</small>
    </div>
</div>
 
<div class="form-group ">
    <label for="no_tlp2" class="col-sm-2 control-label">Nomor Telepon Darurat 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <input type="number" class="form-control" name="no_tlp2" id="no_tlp2" placeholder=""  >
        <small class="info help-block">
        <b>Format Nomor Telepon Darurat must</b> Integer.</small>
    </div>
</div>
 
<div class="form-group ">
    <label for="file_ktp" class="col-sm-2 control-label">File KTP 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <div id="form_student_mobility_file_ktp_galery" ></div>
        <input class="data_file" name="form_student_mobility_file_ktp_uuid" id="form_student_mobility_file_ktp_uuid" type="hidden" >
        <input class="data_file" name="form_student_mobility_file_ktp_name" id="form_student_mobility_file_ktp_name" type="hidden" >
        <small class="info help-block">
        <b>Extension file must</b> JPG,PDF,  <b>Max size file</b>  100 kb.</small>
    </div>
</div>
 
<div class="form-group ">
    <label for="file_loa" class="col-sm-2 control-label">File Loa 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <div id="form_student_mobility_file_loa_galery" ></div>
        <input class="data_file" name="form_student_mobility_file_loa_uuid" id="form_student_mobility_file_loa_uuid" type="hidden" >
        <input class="data_file" name="form_student_mobility_file_loa_name" id="form_student_mobility_file_loa_name" type="hidden" >
        <small class="info help-block">
        <b>Extension file must</b> PDF,JPG,  <b>Max size file</b>  100 kb.</small>
    </div>
</div>
 
<div class="form-group ">
    <label for="tgl_mulai" class="col-sm-2 control-label">Tanggal Mulai 
    <i class="required">*</i>
    </label>
    <div class="col-sm-6">
    <div class="input-group date col-sm-8">
      <input type="text" class="form-control pull-right datepicker" name="tgl_mulai"  placeholder="" id="tgl_mulai" >
    </div>
    <small class="info help-block">
    </small>
    </div>
</div>
 
<div class="form-group ">
    <label for="tgl_selesai" class="col-sm-2 control-label">Tanggal Selesai 
    <i class="required">*</i>
    </label>
    <div class="col-sm-6">
    <div class="input-group date col-sm-8">
      <input type="text" class="form-control pull-right datepicker" name="tgl_selesai"  placeholder="" id="tgl_selesai" >
    </div>
    <small class="info help-block">
    </small>
    </div>
</div>
 
<div class="form-group ">
    <label for="negara_tujuan" class="col-sm-2 control-label">Negara Tujuan 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="negara_tujuan" id="negara_tujuan" placeholder=""  >
        <small class="info help-block">
        </small>
    </div>
</div>
 
<div class="form-group ">
    <label for="ket_pembiayaan" class="col-sm-2 control-label">Keterangan Pembiayaan 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="ket_pembiayaan" id="ket_pembiayaan" placeholder=""  >
        <small class="info help-block">
        </small>
    </div>
</div>


<div class="row col-sm-12 message">
</div>
<div class="col-sm-2">
</div>
<div class="col-sm-8 padding-left-0">
    <button class="btn btn-flat btn-primary btn_save" id="btn_save" data-stype='stay'>
    Submit
    </button>
    <span class="loading loading-hide">
    <img src="http://local.kerjasama.net:80/asset//img/loading-spin-primary.svg"> 
    <i>Loading, Submitting data</i>
    </span>
</div>
</form></div>


<!-- Page script -->
<script>
    $(document).ready(function(){
          $('.form-preview').submit(function(){
        return false;
     });

     $('input[type="checkbox"].flat-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
     });


    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_form_student_mobility = $('#form_form_student_mobility');
        var data_post = form_form_student_mobility.serializeArray();
        var save_type = $(this).attr('data-stype');
    
        $('.loading').show();
    
        $.ajax({
          url: BASE_URL + 'form/form_student_mobility/submit',
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          if(res.success) {
            var id_file_ktp = $('#form_student_mobility_file_ktp_galery').find('li').attr('qq-file-id');
            var id_file_loa = $('#form_student_mobility_file_loa_galery').find('li').attr('qq-file-id');
            
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            resetForm();
            if (typeof id_file_ktp !== 'undefined') {
                    $('#form_student_mobility_file_ktp_galery').fineUploader('deleteFile', id_file_ktp);
                }
            if (typeof id_file_loa !== 'undefined') {
                    $('#form_student_mobility_file_loa_galery').fineUploader('deleteFile', id_file_loa);
                }
            $('.chosen option').prop('selected', false).trigger('chosen:updated');
                
          } else {
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 1000);
        });
    
        return false;
      }); /*end btn save*/


      
             
       var params = {};
       params[csrf] = token;

       $('#form_student_mobility_file_ktp_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + 'form/form_student_mobility/upload_file_ktp_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + 'form/form_student_mobility/delete_file_ktp_file',
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
          multiple : false,
          validation: {
              allowedExtensions: ["jpg","pdf"],
              sizeLimit : 102400,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#form_student_mobility_file_ktp_galery').fineUploader('getUuid', id);
                   $('#form_student_mobility_file_ktp_uuid').val(uuid);
                   $('#form_student_mobility_file_ktp_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#form_student_mobility_file_ktp_uuid').val();
                  $.get(BASE_URL + 'form/form_student_mobility/delete_file_ktp_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#form_student_mobility_file_ktp_uuid').val('');
                  $('#form_student_mobility_file_ktp_name').val('');
                }
              }
          }
      }); /*end file_ktp galey*/
       var params = {};
       params[csrf] = token;

       $('#form_student_mobility_file_loa_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + 'form/form_student_mobility/upload_file_loa_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + 'form/form_student_mobility/delete_file_loa_file',
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
          multiple : false,
          validation: {
              allowedExtensions: ["pdf","jpg"],
              sizeLimit : 102400,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#form_student_mobility_file_loa_galery').fineUploader('getUuid', id);
                   $('#form_student_mobility_file_loa_uuid').val(uuid);
                   $('#form_student_mobility_file_loa_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#form_student_mobility_file_loa_uuid').val();
                  $.get(BASE_URL + 'form/form_student_mobility/delete_file_loa_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#form_student_mobility_file_loa_uuid').val('');
                  $('#form_student_mobility_file_loa_name').val('');
                }
              }
          }
      }); /*end file_loa galey*/
           
    }); /*end doc ready*/
</script>