<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Form Student Mobility Controller
*| --------------------------------------------------------------------------
*| Form Student Mobility site
*|
*/
class Form_student_mobility extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_form_student_mobility');
	}

	/**
	* Submit Form Student Mobilitys
	*
	*/
	public function submit()
	{
		$this->form_validation->set_rules('nim', 'NIM', 'trim|required|max_length[10]|integer');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('dn_ln', 'DN/LN', 'trim|required');
		$this->form_validation->set_rules('id_outbound', 'Jenis Kegiatan', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('no_tlp', 'Nomer Telepon (HP)', 'trim|alpha_numeric|required');
		$this->form_validation->set_rules('no_tlp2', 'Nomor Telepon Darurat', 'trim|required|integer');
		$this->form_validation->set_rules('form_student_mobility_file_ktp_name', 'File KTP', 'trim|required');
		$this->form_validation->set_rules('form_student_mobility_file_loa_name', 'File Loa', 'trim|required');
		$this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'trim|required');
		$this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'trim|required');
		$this->form_validation->set_rules('negara_tujuan', 'Negara Tujuan', 'trim|required');
		$this->form_validation->set_rules('ket_pembiayaan', 'Keterangan Pembiayaan', 'trim|required');
		
		if ($this->form_validation->run()) {
			$form_student_mobility_file_ktp_uuid = $this->input->post('form_student_mobility_file_ktp_uuid');
			$form_student_mobility_file_ktp_name = $this->input->post('form_student_mobility_file_ktp_name');
			$form_student_mobility_file_loa_uuid = $this->input->post('form_student_mobility_file_loa_uuid');
			$form_student_mobility_file_loa_name = $this->input->post('form_student_mobility_file_loa_name');
		
			$save_data = [
				'nim' => $this->input->post('nim'),
				'nama' => $this->input->post('nama'),
				'dn_ln' => $this->input->post('dn_ln'),
				'id_outbound' => $this->input->post('id_outbound'),
				'email' => $this->input->post('email'),
				'no_tlp' => $this->input->post('no_tlp'),
				'no_tlp2' => $this->input->post('no_tlp2'),
				'file_ktp' => $this->input->post('file_ktp'),
				'file_loa' => $this->input->post('file_loa'),
				'tgl_mulai' => $this->input->post('tgl_mulai'),
				'tgl_selesai' => $this->input->post('tgl_selesai'),
				'negara_tujuan' => $this->input->post('negara_tujuan'),
				'ket_pembiayaan' => $this->input->post('ket_pembiayaan'),
			];

			if (!is_dir(FCPATH . '/uploads/form_student_mobility/')) {
				mkdir(FCPATH . '/uploads/form_student_mobility/');
			}

			if (!empty($form_student_mobility_file_ktp_uuid)) {
				$form_student_mobility_file_ktp_name_copy = date('YmdHis') . '-' . $form_student_mobility_file_ktp_name;

				rename(FCPATH . 'uploads/tmp/' . $form_student_mobility_file_ktp_uuid . '/' . $form_student_mobility_file_ktp_name, 
						FCPATH . 'uploads/form_student_mobility/' . $form_student_mobility_file_ktp_name_copy);

				if (!is_file(FCPATH . '/uploads/form_student_mobility/' . $form_student_mobility_file_ktp_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_ktp'] = $form_student_mobility_file_ktp_name_copy;
			}
		
			if (!empty($form_student_mobility_file_loa_uuid)) {
				$form_student_mobility_file_loa_name_copy = date('YmdHis') . '-' . $form_student_mobility_file_loa_name;

				rename(FCPATH . 'uploads/tmp/' . $form_student_mobility_file_loa_uuid . '/' . $form_student_mobility_file_loa_name, 
						FCPATH . 'uploads/form_student_mobility/' . $form_student_mobility_file_loa_name_copy);

				if (!is_file(FCPATH . '/uploads/form_student_mobility/' . $form_student_mobility_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_loa'] = $form_student_mobility_file_loa_name_copy;
			}
		
			
			$save_form_student_mobility = $this->model_form_student_mobility->store($save_data);

			$this->data['success'] = true;
			$this->data['id'] 	   = $save_form_student_mobility;
			$this->data['message'] = cclang('your_data_has_been_successfully_submitted');
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}

	
	/**
	* Upload Image Form Student Mobility	* 
	* @return JSON
	*/
	public function upload_file_ktp_file()
	{
		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'form_student_mobility',
			'allowed_types' => 'jpg|pdf',
			'max_size' 	 	=> 100,
		]);
	}

	/**
	* Delete Image Form Student Mobility	* 
	* @return JSON
	*/
	public function delete_file_ktp_file($uuid)
	{
		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_ktp', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'form_student_mobility',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_student_mobility/'
        ]);
	}

	/**
	* Get Image Form Student Mobility	* 
	* @return JSON
	*/
	public function get_file_ktp_file($id)
	{
		$form_student_mobility = $this->model_form_student_mobility->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_ktp', 
            'table_name'        => 'form_student_mobility',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_student_mobility/',
            'delete_endpoint'   => 'administrator/form_student_mobility/delete_file_ktp_file'
        ]);
	}
	
	/**
	* Upload Image Form Student Mobility	* 
	* @return JSON
	*/
	public function upload_file_loa_file()
	{
		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'form_student_mobility',
			'allowed_types' => 'pdf|jpg',
			'max_size' 	 	=> 100,
		]);
	}

	/**
	* Delete Image Form Student Mobility	* 
	* @return JSON
	*/
	public function delete_file_loa_file($uuid)
	{
		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_loa', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'form_student_mobility',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_student_mobility/'
        ]);
	}

	/**
	* Get Image Form Student Mobility	* 
	* @return JSON
	*/
	public function get_file_loa_file($id)
	{
		$form_student_mobility = $this->model_form_student_mobility->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_loa', 
            'table_name'        => 'form_student_mobility',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_student_mobility/',
            'delete_endpoint'   => 'administrator/form_student_mobility/delete_file_loa_file'
        ]);
	}
	
}


/* End of file form_student_mobility.php */
/* Location: ./application/controllers/administrator/Form Student Mobility.php */