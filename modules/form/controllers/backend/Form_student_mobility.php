<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Form Student Mobility Controller
*| --------------------------------------------------------------------------
*| Form Student Mobility site
*|
*/
class Form_student_mobility extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_form_student_mobility');
	}

	/**
	* show all Form Student Mobilitys
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('form_student_mobility_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['form_student_mobilitys'] = $this->model_form_student_mobility->get($filter, $field, $this->limit_page, $offset);
		$this->data['form_student_mobility_counts'] = $this->model_form_student_mobility->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/manage-form/form_student_mobility/index/',
			'total_rows'   => $this->model_form_student_mobility->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 5,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Registrasi Student Mobility List');
		$this->render('backend/standart/administrator/form_builder/form_student_mobility/form_student_mobility_list', $this->data);
	}

	/**
	* Update view Form Student Mobilitys
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('form_student_mobility_update');

		$this->data['form_student_mobility'] = $this->model_form_student_mobility->find($id);

		$this->template->title('Registrasi Student Mobility Update');
		$this->render('backend/standart/administrator/form_builder/form_student_mobility/form_student_mobility_update', $this->data);
	}

	/**
	* Update Form Student Mobilitys
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('form_student_mobility_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('nim', 'NIM', 'trim|required|max_length[10]|integer');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('dn_ln', 'DN/LN', 'trim|required');
		$this->form_validation->set_rules('id_outbound', 'Jenis Kegiatan', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('no_tlp', 'Nomer Telepon (HP)', 'trim|alpha_numeric|required');
		$this->form_validation->set_rules('no_tlp2', 'Nomor Telepon Darurat', 'trim|required|integer');
		$this->form_validation->set_rules('form_student_mobility_file_ktp_name', 'File KTP', 'trim|required');
		$this->form_validation->set_rules('form_student_mobility_file_loa_name', 'File Loa', 'trim|required');
		$this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'trim|required');
		$this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'trim|required');
		$this->form_validation->set_rules('negara_tujuan', 'Negara Tujuan', 'trim|required');
		$this->form_validation->set_rules('ket_pembiayaan', 'Keterangan Pembiayaan', 'trim|required');
		
		if ($this->form_validation->run()) {
			$form_student_mobility_file_ktp_uuid = $this->input->post('form_student_mobility_file_ktp_uuid');
			$form_student_mobility_file_ktp_name = $this->input->post('form_student_mobility_file_ktp_name');
			$form_student_mobility_file_loa_uuid = $this->input->post('form_student_mobility_file_loa_uuid');
			$form_student_mobility_file_loa_name = $this->input->post('form_student_mobility_file_loa_name');
		
			$save_data = [
				'nim' => $this->input->post('nim'),
				'nama' => $this->input->post('nama'),
				'dn_ln' => $this->input->post('dn_ln'),
				'id_outbound' => $this->input->post('id_outbound'),
				'email' => $this->input->post('email'),
				'no_tlp' => $this->input->post('no_tlp'),
				'no_tlp2' => $this->input->post('no_tlp2'),
				'tgl_mulai' => $this->input->post('tgl_mulai'),
				'tgl_selesai' => $this->input->post('tgl_selesai'),
				'negara_tujuan' => $this->input->post('negara_tujuan'),
				'ket_pembiayaan' => $this->input->post('ket_pembiayaan'),
			];

			if (!is_dir(FCPATH . '/uploads/form_student_mobility/')) {
				mkdir(FCPATH . '/uploads/form_student_mobility/');
			}

			if (!empty($form_student_mobility_file_ktp_uuid)) {
				$form_student_mobility_file_ktp_name_copy = date('YmdHis') . '-' . $form_student_mobility_file_ktp_name;

				rename(FCPATH . 'uploads/tmp/' . $form_student_mobility_file_ktp_uuid . '/' . $form_student_mobility_file_ktp_name, 
						FCPATH . 'uploads/form_student_mobility/' . $form_student_mobility_file_ktp_name_copy);

				if (!is_file(FCPATH . '/uploads/form_student_mobility/' . $form_student_mobility_file_ktp_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_ktp'] = $form_student_mobility_file_ktp_name_copy;
			}
		
			if (!empty($form_student_mobility_file_loa_uuid)) {
				$form_student_mobility_file_loa_name_copy = date('YmdHis') . '-' . $form_student_mobility_file_loa_name;

				rename(FCPATH . 'uploads/tmp/' . $form_student_mobility_file_loa_uuid . '/' . $form_student_mobility_file_loa_name, 
						FCPATH . 'uploads/form_student_mobility/' . $form_student_mobility_file_loa_name_copy);

				if (!is_file(FCPATH . '/uploads/form_student_mobility/' . $form_student_mobility_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_loa'] = $form_student_mobility_file_loa_name_copy;
			}
		
			
			$save_form_student_mobility = $this->model_form_student_mobility->change($id, $save_data);

			if ($save_form_student_mobility) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/form_student_mobility', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/form_student_mobility');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					set_message('Your data not change.', 'error');
					
            		$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/form_student_mobility');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}

	/**
	* delete Form Student Mobilitys
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('form_student_mobility_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'Form Student Mobility'), 'success');
        } else {
            set_message(cclang('error_delete', 'Form Student Mobility'), 'error');
        }

		redirect_back();
	}

	/**
	* View view Form Student Mobilitys
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('form_student_mobility_view');

		$this->data['form_student_mobility'] = $this->model_form_student_mobility->find($id);

		$this->template->title('Registrasi Student Mobility Detail');
		$this->render('backend/standart/administrator/form_builder/form_student_mobility/form_student_mobility_view', $this->data);
	}

	/**
	* delete Form Student Mobilitys
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$form_student_mobility = $this->model_form_student_mobility->find($id);

		if (!empty($form_student_mobility->file_ktp)) {
			$path = FCPATH . '/uploads/form_student_mobility/' . $form_student_mobility->file_ktp;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}

		if (!empty($form_student_mobility->file_loa)) {
			$path = FCPATH . '/uploads/form_student_mobility/' . $form_student_mobility->file_loa;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}

		
		return $this->model_form_student_mobility->remove($id);
	}
	
	/**
	* Upload Image Form Student Mobility	* 
	* @return JSON
	*/
	public function upload_file_ktp_file()
	{
		if (!$this->is_allowed('form_student_mobility_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'form_student_mobility',
			'allowed_types' => 'jpg|pdf',
			'max_size' 	 	=> 100,
		]);
	}

	/**
	* Delete Image Form Student Mobility	* 
	* @return JSON
	*/
	public function delete_file_ktp_file($uuid)
	{
		if (!$this->is_allowed('form_student_mobility_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_ktp', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'form_student_mobility',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_student_mobility/'
        ]);
	}

	/**
	* Get Image Form Student Mobility	* 
	* @return JSON
	*/
	public function get_file_ktp_file($id)
	{
		if (!$this->is_allowed('form_student_mobility_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$form_student_mobility = $this->model_form_student_mobility->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_ktp', 
            'table_name'        => 'form_student_mobility',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_student_mobility/',
            'delete_endpoint'   => 'administrator/form_student_mobility/delete_file_ktp_file'
        ]);
	}
	
	/**
	* Upload Image Form Student Mobility	* 
	* @return JSON
	*/
	public function upload_file_loa_file()
	{
		if (!$this->is_allowed('form_student_mobility_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'form_student_mobility',
			'allowed_types' => 'pdf|jpg',
			'max_size' 	 	=> 100,
		]);
	}

	/**
	* Delete Image Form Student Mobility	* 
	* @return JSON
	*/
	public function delete_file_loa_file($uuid)
	{
		if (!$this->is_allowed('form_student_mobility_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_loa', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'form_student_mobility',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_student_mobility/'
        ]);
	}

	/**
	* Get Image Form Student Mobility	* 
	* @return JSON
	*/
	public function get_file_loa_file($id)
	{
		if (!$this->is_allowed('form_student_mobility_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$form_student_mobility = $this->model_form_student_mobility->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_loa', 
            'table_name'        => 'form_student_mobility',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_student_mobility/',
            'delete_endpoint'   => 'administrator/form_student_mobility/delete_file_loa_file'
        ]);
	}
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('form_student_mobility_export');

		$this->model_form_student_mobility->export('form_student_mobility', 'form_student_mobility');
	}
}


/* End of file form_student_mobility.php */
/* Location: ./application/controllers/administrator/Form Student Mobility.php */