<?php
defined('BASEPATH') or exit('No direct script access allowed');

use GuzzleHttp\Client;

class Model_api_data extends MY_Model
{

	private $sikadu;

	public function __construct()
	{
		parent::__construct();
		$this->load->database(); // Load the default database
		$this->sikadu = $this->load->database('sikadu', TRUE); // Load the sikadu database
	}
	public function getDosenInbound($year)
	{
		$batas_tgl = [$year . '-03-02', $year + 1 . '-03-01'];
		$smts = [$year - 1 . '2', $year . '1'];

		// Query utama untuk getDosenInbound
		$this->db->select('
				d.id,
				d.scope,
				concat("https://simkerjasama.unnes.ac.id/file/",s.doc_file) as link_IA,
				d.aktivitas,
				d.kd_prodi,
				p.tingkat,
				d.keg_dosin_id AS id_kegiatan,
				k.kegiatan AS nama_kegiatan,
				d.tgs_dosin_id AS id_tugas,
				t.tugas AS nama_tugas,
				d.judul_kegiatan,
				d.tgl_mulai,
				d.tgl_selesai,
				d.tahun,
				d.nama_dosin,
				d.nomor_identitas,
				d.nidn,
				d.institusi_asal,
				d.negara,
				d.kode_dosen,
				d.validasi,
				d.is_aktif
			');
		$this->db->from('dosen_inbound d');
		$this->db->join('dosin_keg k', 'd.keg_dosin_id = k.id', 'left');
		$this->db->join('dosin_tugas t', 'd.tgs_dosin_id = t.id', 'left');
		$this->db->join('si_kerjasama s', 'd.unit_ks = s.id', 'left');
		$this->db->join('ref_prodi p', 'd.kd_prodi = p.kode', 'left');
		$this->db->where('d.tgl_mulai <=', $batas_tgl[1]);
		$this->db->where('d.tgl_selesai >=', $batas_tgl[0]);

		$query = $this->db->get();
		$result = $query->result_array();

		// Ambil kode_dosen unik dari hasil query yang sudah difilter
		$uniqueKodeDosen = array_unique(array_column($result, 'kode_dosen'));

		// Ambil data praktisi untuk setiap kode_dosen unik
		$praktisiData = [];
		foreach ($uniqueKodeDosen as $kodeDosen) {
			$praktisiData[$kodeDosen] = $this->getDosenPraktisi($kodeDosen, $smts);
		}
		// dd($praktisiData);

		// Iterate through each result to add praktisi data in 'semester' format and program_studi_child
		foreach ($result as &$dosen) {
			// Set default values for each semester in 'semester' array
			$dosen['semester'] = [
				$smts[0] => 0,
				$smts[1] => 0,
			];

			// Jika ada data praktisi, perbarui nilai sesuai
			$kodeDosen = $dosen['kode_dosen'];
			if (isset($praktisiData[$kodeDosen])) {
				foreach ($praktisiData[$kodeDosen] as $praktisi) {
					$semesterKey = $praktisi->smstr_int;
					$dosen['semester'][$semesterKey] = $praktisi->ket;
				}
			}

			// Menambahkan data program_studi_child jika tingkat != 3 dan kode_dosen memenuhi kriteria
			if ($dosen['tingkat'] != 3 && preg_match('/^[0-9]{5,}$/', $dosen['kode_dosen'])) {
				$child_data = $this->getChildData($dosen['kode_dosen'], $smts);
				$dosen['program_studi_child'] = $child_data;
			} else {
				$dosen['program_studi_child'] = [];
			}
		}

		return $result;
	}

	private function getChildData($kode_dosen, $smts)
	{
		$subquery = "
				SELECT id
				FROM v2_tu_jadwal
				WHERE soft_delete = '0'
					AND (
						kode_dosen1 = '$kode_dosen'
						OR kode_dosen2 = '$kode_dosen'
						OR kode_dosen3 = '$kode_dosen'
						OR kode_dosen4 = '$kode_dosen'
					)
					AND smstr_int IN ('" . implode("','", $smts) . "')
				GROUP BY id
			";

		$this->sikadu->select('m.program_studi');
		$this->sikadu->from('v2_tu_krs k');
		$this->sikadu->join('mhs m', 'k.nim = m.nim');
		$this->sikadu->where('k.soft_delete', '0');
		$this->sikadu->where("k.id_jadwal IN ($subquery)", NULL, FALSE);
		$this->sikadu->group_by('m.program_studi');

		$query = $this->sikadu->get();
		return $query->result_array();
	}

	public function getDosenPraktisi($kode, $smstr_int)
	{
		$smstrPlaceholders = implode(',', array_fill(0, count($smstr_int), '?'));

		$sql = "
				SELECT 
					CASE 
						WHEN kode_dosen1 = ? THEN kode_dosen1
						WHEN kode_dosen2 = ? THEN kode_dosen2
						WHEN kode_dosen3 = ? THEN kode_dosen3
						WHEN kode_dosen4 = ? THEN kode_dosen4
					END AS kode_dosen,
					smstr_int,
					1 AS ket  -- Menambahkan kolom ket dengan nilai default true (1)
				FROM v2_tu_jadwal
				WHERE 
					(kode_dosen1 = ? 
					OR kode_dosen2 = ? 
					OR kode_dosen3 = ? 
					OR kode_dosen4 = ?)
				AND smstr_int IN ($smstrPlaceholders)
				AND soft_delete = '0'
				GROUP BY kode_dosen, smstr_int
			";

		$params = array_merge([$kode, $kode, $kode, $kode, $kode, $kode, $kode, $kode], $smstr_int);

		$query = $this->sikadu->query($sql, $params);
		$result = $query->result();

		// Pastikan setiap item dalam $result memiliki properti 'ket'
		foreach ($result as &$row) {
			if (!isset($row->ket)) {
				$row->ket = 0;  // Tambahkan ket bernilai 0 jika tidak ada
			} else {
				$row->ket = (int)$row->ket;  // Pastikan ket bernilai integer
			}
		}
		return $result;
	}
}

/* End of file Model_data.php */
/* Location: ./application/models/Model_data.php */