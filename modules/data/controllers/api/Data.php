<?php
defined('BASEPATH') or exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Data extends API
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_data');
	}
	public function dosen_inbound_get()
	{
		$year = $this->get('year');
		$datas = $this->model_api_data->getDosenInbound($year);

		$datas = array_map(function ($row) {
			return $row;
		}, $datas);

		$data['data'] = $datas;
		$this->response([
			'data'  => $data,
			'count' => count($datas),
		], API::HTTP_OK);
	}
}
