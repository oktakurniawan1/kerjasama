<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Dikti Univ Controller
 *| --------------------------------------------------------------------------
 *| Dikti Univ site
 *|
 */
class Dikti_univ extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_dikti_univ');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * show all Dikti Univs
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('dikti_univ_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('dikti_univ_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('dikti_univ_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('dikti_univ_filter');
			$this->session->unset_userdata('dikti_univ_field');
		}
		$filter = $this->session->userdata('dikti_univ_filter');
		$field = $this->session->userdata('dikti_univ_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['dikti_univs'] = $this->model_dikti_univ->get($filter, $field, $this->limit_page, $offset);
		$this->data['dikti_univ_counts'] = $this->model_dikti_univ->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/dikti_univ/index/',
			'total_rows'   => $this->model_dikti_univ->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Dikti Univ List');
		$this->render('backend/standart/administrator/dikti_univ/dikti_univ_list', $this->data);
	}

	/**
	 * Add new dikti_univs
	 *
	 */
	public function add()
	{
		$this->is_allowed('dikti_univ_add');

		$this->template->title('Dikti Univ New');
		$this->render('backend/standart/administrator/dikti_univ/dikti_univ_add', $this->data);
	}

	/**
	 * Add New Dikti Univs
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('dikti_univ_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('kd_univ', 'Kode Universitas', 'trim|required|max_length[8]');
		$this->form_validation->set_rules('nama_univ', 'Nama Universitas', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('lembaga', 'Lembaga', 'trim|max_length[255]');
		$this->form_validation->set_rules('bentuk_pendidikan', 'Bentuk Pendidikan', 'trim|max_length[255]');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|max_length[255]');
		$this->form_validation->set_rules('kd_propinsi', 'Kode Propinsi', 'trim|max_length[255]');
		$this->form_validation->set_rules('kabupaten', 'Kabupaten', 'trim|max_length[255]');
		$this->form_validation->set_rules('kecataman', 'Kecamatan', 'trim|max_length[255]');
		$this->form_validation->set_rules('kodepos', 'Kodepos', 'trim|max_length[10]|callback_valid_number');
		$this->form_validation->set_rules('email', 'Email', 'trim|max_length[255]|valid_email');
		$this->form_validation->set_rules('telepon', 'Telepon', 'trim|required');

		if ($this->form_validation->run()) {
			$propinsi = db_get_all_data('ref_propinsi', ['kd_propinsi' => $this->input->kd_propinsi], 'row');
			$save_data = [
				'kd_univ' => $this->input->post('kd_univ'),
				'nama_univ' => $this->input->post('nama_univ'),
				'lembaga' => $this->input->post('lembaga'),
				'bentuk_pendidikan' => $this->input->post('bentuk_pendidikan'),
				'alamat' => $this->input->post('alamat'),
				'kd_propinsi' => $this->input->post('kd_propinsi'),
				'propinsi' => $propinsi->propinsi,
				'kabupaten' => $this->input->post('kabupaten'),
				'kecataman' => $this->input->post('kecataman'),
				'kodepos' => $this->input->post('kodepos'),
				'email' => $this->input->post('email'),
				'telepon' => $this->input->post('telepon'),
			];

			$save_dikti_univ = $this->model_dikti_univ->store($save_data);

			if ($save_dikti_univ) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_dikti_univ;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/dikti_univ/edit/' . $save_dikti_univ, 'Edit Dikti Univ'),
						anchor('administrator/dikti_univ', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/dikti_univ/edit/' . $save_dikti_univ, 'Edit Dikti Univ')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dikti_univ');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dikti_univ');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Dikti Univs
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('dikti_univ_update');

		$this->data['dikti_univ'] = $this->model_dikti_univ->find($id);

		$this->template->title('Dikti Univ Update');
		$this->render('backend/standart/administrator/dikti_univ/dikti_univ_update', $this->data);
	}

	/**
	 * Update Dikti Univs
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('dikti_univ_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('kd_univ', 'Kode Universitas', 'trim|required|max_length[8]');
		$this->form_validation->set_rules('nama_univ', 'Nama Universitas', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('lembaga', 'Lembaga', 'trim|max_length[255]');
		$this->form_validation->set_rules('bentuk_pendidikan', 'Bentuk Pendidikan', 'trim|max_length[255]');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|max_length[255]');
		$this->form_validation->set_rules('kd_propinsi', 'Kode Propinsi', 'trim|max_length[255]');
		$this->form_validation->set_rules('kabupaten', 'Kabupaten', 'trim|max_length[255]');
		$this->form_validation->set_rules('kecataman', 'Kecamatan', 'trim|max_length[255]');
		$this->form_validation->set_rules('kodepos', 'Kodepos', 'trim|max_length[10]|callback_valid_number');
		$this->form_validation->set_rules('email', 'Email', 'trim|max_length[255]|valid_email');
		$this->form_validation->set_rules('telepon', 'Telepon', 'trim|required');

		if ($this->form_validation->run()) {
			$propinsi = db_get_all_data('ref_propinsi', ['kd_propinsi' => $this->input->kd_propinsi], 'row');
			$save_data = [
				'kd_univ' => $this->input->post('kd_univ'),
				'nama_univ' => $this->input->post('nama_univ'),
				'lembaga' => $this->input->post('lembaga'),
				'bentuk_pendidikan' => $this->input->post('bentuk_pendidikan'),
				'alamat' => $this->input->post('alamat'),
				'kd_propinsi' => $this->input->post('kd_propinsi'),
				'propinsi' => $propinsi->propinsi,
				'kabupaten' => $this->input->post('kabupaten'),
				'kecataman' => $this->input->post('kecataman'),
				'kodepos' => $this->input->post('kodepos'),
				'email' => $this->input->post('email'),
				'telepon' => $this->input->post('telepon'),
			];

			$save_dikti_univ = $this->model_dikti_univ->change($id, $save_data);

			if ($save_dikti_univ) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/dikti_univ', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dikti_univ/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dikti_univ/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Dikti Univs
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('dikti_univ_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'dikti_univ'), 'success');
		} else {
			set_message(cclang('error_delete', 'dikti_univ'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Dikti Univs
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('dikti_univ_view');
		$this->data['dikti_univ'] = $this->model_dikti_univ->join_avaiable()->filter_avaiable()->find($id);
		$this->template->title('Dikti Univ Detail');
		$this->render('backend/standart/administrator/dikti_univ/dikti_univ_view', $this->data);
	}
	public function view_by_kode($kode)
	{
		$this->is_allowed('dikti_univ_view');
		$this->data['dikti_univ'] = $this->model_dikti_univ->join_avaiable()->filter_avaiable()->get_single(['kd_univ' => $kode]);
		$this->template->title('Dikti Univ Detail');
		$this->render('backend/standart/administrator/dikti_univ/dikti_univ_view', $this->data);
	}

	/**
	 * delete Dikti Univs
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$dikti_univ = $this->model_dikti_univ->find($id);
		return $this->model_dikti_univ->remove($id);
	}

	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('dikti_univ_export');
		$this->model_dikti_univ->export(
			'dikti_univ',
			'dikti_univ',
			$this->model_dikti_univ->field_search
		);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('dikti_univ_export');
		$this->model_dikti_univ->pdf('dikti_univ', 'dikti_univ');
	}

	public function single_pdf($id = null)
	{
		$this->is_allowed('dikti_univ_export');
		$table = $title = 'dikti_univ';
		$this->load->library('HtmlPdf');
		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);
		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');
		$result = $this->db->get($table);
		$data = $this->model_dikti_univ->find($id);
		$fields = $result->list_fields();
		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);
		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
	public function form_mnj_prodi_dikti()
	{
		$data['jenjang'] = get_data_option('dikti_jjg', 'kdjjg', 'jenjang',false);
		$data['kd_univ'] = $this->input->get('kd_univ');
		$this->load->view('form-mnj-prodi-dikti', $data);
	}
	public function browse_prodi()
	{
		echo $this->model_dikti_univ->getProdi($this->input->post('kd_univ'));
	}
	public function update_prodi_dikti()
	{
		$this->model_dikti_univ->update_prodi($this->input->post());
	}
	public function tambah_data_prodi()
	{
		$status = $this->model_dikti_univ->tambah_prodi($this->input->post());
		echo json_encode(['status' => $status]);
	}
	public function hapus_data_prodi()
	{
		$status = $this->model_dikti_univ->hapus_prodi($this->input->get('id'));
		echo json_encode(['status' => $status]);
	}
}


/* End of file dikti_univ.php */
/* Location: ./application/controllers/administrator/Dikti Univ.php */