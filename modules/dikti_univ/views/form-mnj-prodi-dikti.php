<div class="alert alert-warning">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<strong>Perhatian</strong>
	<p>
	<ul>
		<li>Kosongkan kode prodi untuk Universitas Luar Negeri kode akan digenerate sistem otomatis.</li>
		<li>Inputkan kode prodi secara manual untuk Universitas dalam Negeri, kode prodi Universitas dalam negeri dapat dilihat di laman https://forlap.kemdikbud.go.id di pencarian data.</li>
	</ul>
	</p>
</div>

<form action="" method="POST" class="form-inline" role="form">
	<input type="hidden" name="kd_univ" value='<?= $kd_univ ?>'>
	<div class="form-group">
		<label class="sr-only" for=""></label>
		<input type="text" name="kd_prodi" class="form-control" id="" placeholder="kode prodi" style="width:100px;">
	</div>
	<div class="form-group">
		<label class="sr-only" for=""></label>
		<input type="text" name="nama_prodi" class="form-control" id="" placeholder="Nama Program Studi" style="width:400px;">
	</div>
	<div class="form-group">
		<label class="sr-only" for=""></label>
		<?= form_dropdown('kdjjg', $jenjang, '30', ['class' => 'form-control']) ?>
	</div>
	<button type="button" class="btn btn-success" onclick="tambah_prodi()">Tambah Prodi</button>
</form>
<table class="table table-hover table-striped table-bordered" id='table1'>
	<thead>
		<tr>
			<th>_id</th>
			<th>Kode Prodi</th>
			<th>Nama Prodi</th>
			<th>Jenjang</th>
			<th>Option</th>
		</tr>
	</thead>
	<tbody>
		<tr>
		</tr>
	</tbody>
</table>

<script>
	var oTable;
	$(function() {
		oTable = $('#table1').DataTable({
			processing: true,
			serverSide: true,
			pageLength: 5,
			lengthChange: false,
			//lengthMenu: [6, 10, 15, 25],
			ajax: {
				'url': '<?php echo site_url('administrator/dikti_univ/browse_prodi') ?>',
				'type': 'post',
				'data': {
					kd_univ: '<?= $kd_univ ?>',
					<?= csrf_token() ?>
				},
			},
			columns: [{
					data: 'id',
					visible: false,
					searchable: false,
				},
				{
					data: 'kd_prodi'
				},
				{
					data: 'nama_prodi'
				},
				{
					data: 'jenjang'
				},
				{
					className: 'text-center',
					searchable: false,
					orderable: false,
					width: '30px'
				}
			],
			columnDefs: [{
				'targets': -1,
				'data': null,
				render: function(data, type, row) {
					return "<button class='btn btn-default btn-xs' onclick='hapus_prodi(" + row.id + ")'><i class='fa fa-trash text-danger'></i></button> ";
				}
			}]
		})
		oTable.MakeCellsEditable({
			"onUpdate": update_data,
			"inputCss": 'form-control',
			'wrapperHtml': '<div class="alert alert-info">{content}</div>',
			"columns": [1, 2, 3],
			"confirmationButton": {
				"confirmCss": 'btn btn-success btn-xs',
				"cancelCss": 'btn btn-warning btn-xs'
			},
			'inputTypes': [{
				'column': 3,
				'type': 'list',
				'options': [{
						'value': 'D-I',
						'display': 'D-I'
					},
					{
						'value': 'D-II',
						'display': 'D-II'
					},
					{
						'value': 'D-III',
						'display': 'D-III'
					},
					{
						'value': 'D-IV',
						'display': 'D-IV'
					},
					{
						'value': 'Profesi',
						'display': 'Profesi'
					},
					{
						'value': 'S1',
						'display': 'S1'
					},
					{
						'value': 'S2',
						'display': 'S2'
					},
					{
						'value': 'S3',
						'display': 'S3'
					},
					{
						'value': 'Spesialis',
						'display': 'Spesialis'
					},
				]
			}, ],
		});
	})

	function update_data(updatedCell, updatedRow, oldValue) {
		var url = '<?php echo site_url('administrator/dikti_univ/update_prodi_dikti') ?>';
		var data = updatedRow.data();
		data[csrf] = token;
		$.post(url, data, function(response) {
			oTable.draw(false);
		})
	}

	function tambah_prodi() {
		var data = {};
		var url = '<?php echo site_url('administrator/dikti_univ/tambah_data_prodi') ?>';
		var nama_prodi = $('input[name=nama_prodi]').val();
		if (nama_prodi.length == 0) {
			alert('Nama program studi harus diisi!');
			return;
		}
		data[csrf] = token;
		data['kd_prodi'] = $('input[name=kd_prodi]').val();
		data['kd_univ'] = $('input[name=kd_univ]').val();
		data['nama_prodi'] = nama_prodi;
		data['kdjjg'] = $('select[name=kdjjg] option:selected').val();
		$.post(url, data, function(response) {
			if (response.status > 0) {
				alert('prodi baru berhasil ditambahkan')
				oTable.draw(false);
			}
		}, 'json')
	}

	function hapus_prodi(id) {
		if (confirm('Yakin akan dihapus?')) {
			var url = '<?php echo site_url('administrator/dikti_univ/hapus_data_prodi') ?>';
			$.get(url, {
				id: id
			}, function(response) {
				if (response.status > 0) {
					oTable.draw(false);
				}
			}, 'json')
		}
	}
</script>