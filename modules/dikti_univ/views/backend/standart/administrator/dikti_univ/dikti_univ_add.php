<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Dikti Univ <small><?= cclang('new', ['Dikti Univ']); ?> </small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/dikti_univ'); ?>">Dikti Univ</a></li>
		<li class="active"><?= cclang('new'); ?></li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Dikti Univ</h3>
							<h5 class="widget-user-desc"><?= cclang('new', ['Dikti Univ']); ?></h5>
							<hr>
						</div>
						<?= form_open('', [
							'name'    => 'form_dikti_univ',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_dikti_univ',
							'enctype' => 'multipart/form-data',
							'method'  => 'POST'
						]); ?>

						<div class="form-group ">
							<label for="kd_univ" class="col-sm-2 control-label">Kode Universitas
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="kd_univ" id="kd_univ" placeholder="Kode Universitas" value="<?= set_value('kd_univ'); ?>">
								<small class="info help-block">
									<b>Input Kd Univ</b> Max Length : 8.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nama_univ" class="col-sm-2 control-label">Nama Universitas
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nama_univ" id="nama_univ" placeholder="Nama Universitas" value="<?= set_value('nama_univ'); ?>">
								<small class="info help-block">
									<b>Input Nama Univ</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="lembaga" class="col-sm-2 control-label">Lembaga
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="lembaga" id="lembaga" data-placeholder="Select Lembaga">
									<option value=""></option>
									<?php foreach (db_get_all_data('ref_lembaga_univ') as $row) : ?>
										<option value="<?= $row->lembaga ?>"><?= $row->lembaga; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Input Lembaga</b> Max Length : 255.</small>
							</div>
						</div>


						<div class="form-group ">
							<label for="bentuk_pendidikan" class="col-sm-2 control-label">Bentuk Pendidikan
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="bentuk_pendidikan" id="bentuk_pendidikan" data-placeholder="Select Bentuk Pendidikan">
									<option value=""></option>
									<?php foreach (db_get_all_data('ref_bentuk_univ') as $row) : ?>
										<option value="<?= $row->bentuk_pendidikan ?>"><?= $row->bentuk_pendidikan; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Input Bentuk Pendidikan</b> Max Length : 255.</small>
							</div>
						</div>


						<div class="form-group ">
							<label for="alamat" class="col-sm-2 control-label">Alamat
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" value="<?= set_value('alamat'); ?>">
								<small class="info help-block">
									<b>Input Alamat</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="kd_propinsi" class="col-sm-2 control-label">Kode Propinsi
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="kd_propinsi" id="kd_propinsi" data-placeholder="Select Kode Propinsi">
									<option value=""></option>
									<?php foreach (db_get_all_data('ref_propinsi') as $row) : ?>
										<option value="<?= $row->kd_propinsi ?>"><?= $row->propinsi; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Input Kd Propinsi</b> Max Length : 255.</small>
							</div>
						</div>


						<div class="form-group ">
							<label for="kabupaten" class="col-sm-2 control-label">Kabupaten
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="kabupaten" id="kabupaten" placeholder="Kabupaten" value="<?= set_value('kabupaten'); ?>">
								<small class="info help-block">
									<b>Input Kabupaten</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="kecataman" class="col-sm-2 control-label">Kecamatan
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="kecataman" id="kecataman" placeholder="Kecamatan" value="<?= set_value('kecataman'); ?>">
								<small class="info help-block">
									<b>Input Kecataman</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="kodepos" class="col-sm-2 control-label">Kodepos
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="kodepos" id="kodepos" placeholder="Kodepos" value="<?= set_value('kodepos'); ?>">
								<small class="info help-block">
									<b>Format Kodepos must</b> Valid Number, <b>Input Kodepos</b> Max Length : 10.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="email" class="col-sm-2 control-label">Email
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?= set_value('email'); ?>">
								<small class="info help-block">
									<b>Format Email must</b> Valid Email, <b>Input Email</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="telepon" class="col-sm-2 control-label">Telepon
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="telepon" id="telepon" placeholder="Telepon" value="<?= set_value('telepon'); ?>">
								<small class="info help-block">
								</small>
							</div>
						</div>


						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	$(document).ready(function() {


		$('#btn_cancel').click(function() {
			swal({
					title: "<?= cclang('are_you_sure'); ?>",
					text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/dikti_univ';
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();

			var form_dikti_univ = $('#form_dikti_univ');
			var data_post = form_dikti_univ.serializeArray();
			var save_type = $(this).attr('data-stype');

			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: BASE_URL + '/administrator/dikti_univ/add_save',
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('.steps li').removeClass('error');
					$('form').find('.error-input').remove();
					if (res.success) {

						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						resetForm();
						$('.chosen option').prop('selected', false).trigger('chosen:updated');

					} else {
						if (res.errors) {

							$.each(res.errors, function(index, val) {
								$('form #' + index).parents('.form-group').addClass('has-error');
								$('form #' + index).parents('.form-group').find('small').prepend(`
                      <div class="error-input">` + val + `</div>
                      `);
							});
							$('.steps li').removeClass('error');
							$('.content section').each(function(index, el) {
								if ($(this).find('.has-error').length) {
									$('.steps li:eq(' + index + ')').addClass('error').find('a').trigger('click');
								}
							});
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/








	}); /*end doc ready*/
</script>