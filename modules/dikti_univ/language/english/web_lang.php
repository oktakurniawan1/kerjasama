<?php
defined('BASEPATH') or exit('No direct script access allowed');

$lang['dikti_univ'] = 'Data Universitas';
$lang['id'] = 'Id';
$lang['kd_univ'] = 'Kode Universitas';
$lang['nama_univ'] = 'Nama Universitas';
$lang['lembaga'] = 'Lembaga';
$lang['bentuk_pendidikan'] = 'Bentuk Pendidikan';
$lang['alamat'] = 'Alamat';
$lang['kd_propinsi'] = 'Kode Propinsi';
$lang['kabupaten'] = 'Kabupaten';
$lang['kecataman'] = 'Kecamatan';
$lang['kodepos'] = 'Kodepos';
$lang['email'] = 'Email';
$lang['telepon'] = 'Telepon';
