<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_dikti_univ extends MY_Model
{

	private $primary_key    = 'id';
	private $table_name     = 'dikti_univ';
	public $field_search   = ['kd_univ', 'nama_univ', 'lembaga', 'bentuk_pendidikan', 'email', 'telepon'];

	public function __construct()
	{
		$config = array(
			'primary_key'   => $this->primary_key,
			'table_name'    => $this->table_name,
			'field_search'  => $this->field_search,
		);

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "dikti_univ." . $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "dikti_univ." . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			$where .= "(" . "dikti_univ." . $field . " LIKE '%" . $q . "%' )";
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "dikti_univ." . $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "dikti_univ." . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			$where .= "(" . "dikti_univ." . $field . " LIKE '%" . $q . "%' )";
		}

		if (is_array($select_field) and count($select_field)) {
			$this->db->select($select_field);
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);
		$this->db->limit($limit, $offset);

		$this->sortable();

		$query = $this->db->get($this->table_name);

		return $query->result();
	}

	public function join_avaiable()
	{
		$this->db->join('ref_lembaga_univ', 'ref_lembaga_univ.lembaga = dikti_univ.lembaga', 'LEFT');
		$this->db->join('ref_bentuk_univ', 'ref_bentuk_univ.bentuk_pendidikan = dikti_univ.bentuk_pendidikan', 'LEFT');
		$this->db->join('ref_propinsi', 'ref_propinsi.kd_propinsi = dikti_univ.kd_propinsi', 'LEFT');

		$this->db->select('dikti_univ.*,ref_lembaga_univ.lembaga as ref_lembaga_univ_lembaga,ref_bentuk_univ.bentuk_pendidikan as ref_bentuk_univ_bentuk_pendidikan,ref_propinsi.propinsi as ref_propinsi_propinsi');

		return $this;
	}

	public function filter_avaiable()
	{

		if (!$this->aauth->is_admin()) {
		}

		return $this;
	}
	public function getProdi($kd_univ)
	{
		$this->load->library('Datatables', null, 'dt');
		$this->dt->set_database('default');
		$this->dt->select('id,kd_prodi,nama_prodi,jenjang')->where(['kd_univ' => $kd_univ])->from('dikti_prodi');
		return $this->dt->generate();
	}
	public function tambah_prodi($post)
	{
		$kd_prodi_dl = $post['kd_prodi'];
		if($kd_prodi_dl == '')
		{
			//generate kode prodi Univ Luar
			$nums = $this->db->query("SELECT RIGHT(kd_prodi,2) + 0 as num FROM dikti_prodi WHERE kd_univ = '{$post['kd_univ']}' AND LENGTH(kd_prodi)=8 ")->result_array();
			$numkodes[] = 0;
			foreach ($nums as $row) {
				$numkodes[] = $row['num'];
			}
			$range = range(1, max($numkodes) + 1);
			$nomk = min(array_diff($range, $numkodes));
			$kd_prodi = $post['kd_univ'] . str_pad($nomk, 2, '0', STR_PAD_LEFT);
		}
		else{
			$kd_prodi = $kd_prodi_dl;
		}
		$jjg = db_get_all_data('dikti_jjg', ['kdjjg' => $post['kdjjg']], 'row');
		$post['kd_prodi'] = $kd_prodi;
		$post['jenjang'] = $jjg->jenjang;
		$sql = $this->db->set($post)->get_compiled_insert('dikti_prodi');
		$sql = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $sql);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}
	public function hapus_prodi($id)
	{
		$this->db->where('id', $id)->delete('dikti_prodi');
		return $this->db->affected_rows();
	}
	public function update_prodi($data)
	{
		$jjg = db_get_all_data('dikti_jjg', ['jenjang' => $data['jenjang']], 'row');
		$data['kdjjg'] = $jjg->kdjjg;
		$this->db->where('id', $data['id'])->update('dikti_prodi', $data);
	}
}

/* End of file Model_dikti_univ.php */
/* Location: ./application/models/Model_dikti_univ.php */