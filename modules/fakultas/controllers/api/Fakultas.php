<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Fakultas extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_fakultas');
	}

	/**
	 * @api {get} /fakultas/all Get all fakultass.
	 * @apiVersion 0.1.0
	 * @apiName AllFakultas 
	 * @apiGroup fakultas
	 * @apiHeader {String} X-Api-Key Fakultass unique access-key.
	 * @apiPermission Fakultas Cant be Accessed permission name : api_fakultas_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Fakultass.
	 * @apiParam {String} [Field="All Field"] Optional field of Fakultass : kode, nama, nama_singkat, nama_ktm, nama_en.
	 * @apiParam {String} [Start=0] Optional start index of Fakultass.
	 * @apiParam {String} [Limit=10] Optional limit data of Fakultass.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of fakultas.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataFakultas Fakultas data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_fakultas_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['kode', 'nama', 'nama_singkat', 'nama_ktm', 'nama_en'];
		$fakultass = $this->model_api_fakultas->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_fakultas->count_all($filter, $field);
		$fakultass = array_map(function($row){
						
			return $row;
		}, $fakultass);

		$data['fakultas'] = $fakultass;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Fakultas',
			'data'	 	=> $data,
			'total' 	=> $total,
		], API::HTTP_OK);
	}

		/**
	 * @api {get} /fakultas/detail Detail Fakultas.
	 * @apiVersion 0.1.0
	 * @apiName DetailFakultas
	 * @apiGroup fakultas
	 * @apiHeader {String} X-Api-Key Fakultass unique access-key.
	 * @apiPermission Fakultas Cant be Accessed permission name : api_fakultas_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Fakultass.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of fakultas.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError FakultasNotFound Fakultas data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_fakultas_detail', false);

		$this->requiredInput(['kode']);

		$id = $this->get('kode');

		$select_field = ['kode', 'nama', 'nama_singkat', 'nama_ktm', 'nama_en'];
		$fakultas = $this->model_api_fakultas->find($id, $select_field);

		if (!$fakultas) {
			$this->response([
					'status' 	=> false,
					'message' 	=> 'Blog not found'
				], API::HTTP_NOT_FOUND);
		}

					
		$data['fakultas'] = $fakultas;
		if ($data['fakultas']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Fakultas',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Fakultas not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /fakultas/add Add Fakultas.
	 * @apiVersion 0.1.0
	 * @apiName AddFakultas
	 * @apiGroup fakultas
	 * @apiHeader {String} X-Api-Key Fakultass unique access-key.
	 * @apiPermission Fakultas Cant be Accessed permission name : api_fakultas_add
	 *
 	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_fakultas_add', false);

		
		if ($this->form_validation->run()) {

			$save_data = [
			];
			
			$save_fakultas = $this->model_api_fakultas->store($save_data);

			if ($save_fakultas) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /fakultas/update Update Fakultas.
	 * @apiVersion 0.1.0
	 * @apiName UpdateFakultas
	 * @apiGroup fakultas
	 * @apiHeader {String} X-Api-Key Fakultass unique access-key.
	 * @apiPermission Fakultas Cant be Accessed permission name : api_fakultas_update
	 *
	 * @apiParam {Integer} kode Mandatory kode of Fakultas.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_fakultas_update', false);

		
		
		if ($this->form_validation->run()) {

			$save_data = [
			];
			
			$save_fakultas = $this->model_api_fakultas->change($this->post('kode'), $save_data);

			if ($save_fakultas) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /fakultas/delete Delete Fakultas. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteFakultas
	 * @apiGroup fakultas
	 * @apiHeader {String} X-Api-Key Fakultass unique access-key.
	 	 * @apiPermission Fakultas Cant be Accessed permission name : api_fakultas_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Fakultass .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_fakultas_delete', false);

		$fakultas = $this->model_api_fakultas->find($this->post('kode'));

		if (!$fakultas) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Fakultas not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_fakultas->remove($this->post('kode'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Fakultas deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Fakultas not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
}

/* End of file Fakultas.php */
/* Location: ./application/controllers/api/Fakultas.php */