<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 *| --------------------------------------------------------------------------
 *| Si Jenis Ks Controller
 *| --------------------------------------------------------------------------
 *| Si Jenis Ks site
 *|
 */
class Si_jenis_ks extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_si_jenis_ks');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * show all Si Jenis Kss
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('si_jenis_ks_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('si_jenis_ks_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('si_jenis_ks_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('si_jenis_ks_filter');
			$this->session->unset_userdata('si_jenis_ks_field');
		}
		$filter = $this->session->userdata('si_jenis_ks_filter');
		$field = $this->session->userdata('si_jenis_ks_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['si_jenis_kss'] = $this->model_si_jenis_ks->get($filter, $field, $this->limit_page, $offset);
		$this->data['si_jenis_ks_counts'] = $this->model_si_jenis_ks->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/si_jenis_ks/index/',
			'total_rows'   => $this->model_si_jenis_ks->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Tabel Jenis Kerjasama List');
		$this->render('backend/standart/administrator/si_jenis_ks/si_jenis_ks_list', $this->data);
	}

	/**
	 * Add new si_jenis_kss
	 *
	 */
	public function add()
	{
		$this->is_allowed('si_jenis_ks_add');

		$this->template->title('Tabel Jenis Kerjasama New');
		$this->render('backend/standart/administrator/si_jenis_ks/si_jenis_ks_add', $this->data);
	}

	/**
	 * Add New Si Jenis Kss
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('si_jenis_ks_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('jenis_ks', 'Jenis Kerjasama', 'trim|required|max_length[255]');


		if ($this->form_validation->run()) {

			$save_data = [
				'jenis_ks' => $this->input->post('jenis_ks'),
				'deskripsi' => $this->input->post('deskripsi'),
			];

			$save_si_jenis_ks = $this->model_si_jenis_ks->store($save_data);

			if ($save_si_jenis_ks) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_si_jenis_ks;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/si_jenis_ks/edit/' . $save_si_jenis_ks, 'Edit Si Jenis Ks'),
						anchor('administrator/si_jenis_ks', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/si_jenis_ks/edit/' . $save_si_jenis_ks, 'Edit Si Jenis Ks')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/si_jenis_ks');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/si_jenis_ks');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Si Jenis Kss
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('si_jenis_ks_update');

		$this->data['si_jenis_ks'] = $this->model_si_jenis_ks->find($id);

		$this->template->title('Tabel Jenis Kerjasama Update');
		$this->render('backend/standart/administrator/si_jenis_ks/si_jenis_ks_update', $this->data);
	}

	/**
	 * Update Si Jenis Kss
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('si_jenis_ks_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('jenis_ks', 'Jenis Kerjasama', 'trim|required|max_length[255]');

		if ($this->form_validation->run()) {

			$save_data = [
				'jenis_ks' => $this->input->post('jenis_ks'),
				'deskripsi' => $this->input->post('deskripsi'),
			];


			$save_si_jenis_ks = $this->model_si_jenis_ks->change($id, $save_data);

			if ($save_si_jenis_ks) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/si_jenis_ks', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/si_jenis_ks/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/si_jenis_ks/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Si Jenis Kss
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('si_jenis_ks_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'si_jenis_ks'), 'success');
		} else {
			set_message(cclang('error_delete', 'si_jenis_ks'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Si Jenis Kss
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('si_jenis_ks_view');

		$this->data['si_jenis_ks'] = $this->model_si_jenis_ks->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Tabel Jenis Kerjasama Detail');
		$this->render('backend/standart/administrator/si_jenis_ks/si_jenis_ks_view', $this->data);
	}

	/**
	 * delete Si Jenis Kss
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$cek = $this->model_si_jenis_ks->is_used($id);
		if ($cek) {
			set_message('Data ini sedang digunakan, tidak bisa dihapus', 'error');
			redirect_back();
		}
		$si_jenis_ks = $this->model_si_jenis_ks->find($id);
		return $this->model_si_jenis_ks->remove($id);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('si_jenis_ks_export');

		$this->model_si_jenis_ks->export(
			'si_jenis_ks',
			'si_jenis_ks',
			$this->model_si_jenis_ks->field_search
		);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('si_jenis_ks_export');

		$this->model_si_jenis_ks->pdf('si_jenis_ks', 'si_jenis_ks');
	}

	public function single_pdf($id = null)
	{
		$this->is_allowed('si_jenis_ks_export');

		$table = $title = 'si_jenis_ks';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_si_jenis_ks->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
	public function form_ubah_map_iku()
	{

		$iku = db_get_all_data('iku_ks');

		$data['iku'] = array_column($iku, 'nama_iku_ks', 'id');
		$data['id']  = $this->input->get('id');
		$data['val'] = $this->input->get('idiku');
		$this->load->view('form-map-iku', $data);
	}
	public function save_map_iku()
	{
		$id_iku = $this->input->post('idiku');
		$id_jenis_ks = $this->input->post('id');

		// Cek apakah data sudah ada
		$this->db->where('id_iku', $id_iku);
		$this->db->where('id_jenis_ks', $id_jenis_ks);
		$query = $this->db->get('iku_map_ks');

		if ($query->num_rows() > 0) {
			// Data sudah ada, lakukan update
			$data = array('id_iku' => $id_iku, 'id_jenis_ks' => $id_jenis_ks);
			$this->db->where('id_iku', $id_iku);
			$this->db->where('id_jenis_ks', $id_jenis_ks);
			if ($this->db->update('iku_map_ks', $data)) {
				$response = array('success' => true, 'message' => 'Data berhasil diupdate.');
			} else {
				$response = array('success' => false, 'message' => 'Gagal mengupdate data.');
			}
		} else {
			// Data belum ada, lakukan insert
			$data = array('id_iku' => $id_iku, 'id_jenis_ks' => $id_jenis_ks);
			if ($this->db->insert('iku_map_ks', $data)) {
				$response = array('success' => true, 'message' => 'Data berhasil ditambahkan.');
			} else {
				$response = array('success' => false, 'message' => 'Gagal menambahkan data.');
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}
	public function hapus_map_iku()
	{
		$id_iku = $this->input->post('idiku');
		$id_jenis_ks = $this->input->post('id');

		$this->db->where('id_iku', $id_iku);
		$this->db->where('id_jenis_ks', $id_jenis_ks);
		if ($this->db->delete('iku_map_ks')) {
			$response = array('success' => true, 'message' => 'Data berhasil dihapus.');
		} else {
			$response = array('success' => false, 'message' => 'Gagal menghapus data.');
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}
}


/* End of file si_jenis_ks.php */
/* Location: ./application/controllers/administrator/Si Jenis Ks.php */