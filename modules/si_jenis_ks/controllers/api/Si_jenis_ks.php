<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Si_jenis_ks extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_si_jenis_ks');
	}

	/**
	 * @api {get} /si_jenis_ks/all Get all si_jenis_kss.
	 * @apiVersion 0.1.0
	 * @apiName AllSijenisks 
	 * @apiGroup si_jenis_ks
	 * @apiHeader {String} X-Api-Key Si jenis kss unique access-key.
	 * @apiPermission Si jenis ks Cant be Accessed permission name : api_si_jenis_ks_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Si jenis kss.
	 * @apiParam {String} [Field="All Field"] Optional field of Si jenis kss : id, jenis_ks, deskripsi.
	 * @apiParam {String} [Start=0] Optional start index of Si jenis kss.
	 * @apiParam {String} [Limit=10] Optional limit data of Si jenis kss.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of si_jenis_ks.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataSi jenis ks Si jenis ks data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_si_jenis_ks_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'jenis_ks', 'deskripsi'];
		$si_jenis_kss = $this->model_api_si_jenis_ks->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_si_jenis_ks->count_all($filter, $field);
		$si_jenis_kss = array_map(function($row){
						
			return $row;
		}, $si_jenis_kss);

		$data['si_jenis_ks'] = $si_jenis_kss;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Si jenis ks',
			'data'	 	=> $data,
			'total' 	=> $total,
		], API::HTTP_OK);
	}

		/**
	 * @api {get} /si_jenis_ks/detail Detail Si jenis ks.
	 * @apiVersion 0.1.0
	 * @apiName DetailSi jenis ks
	 * @apiGroup si_jenis_ks
	 * @apiHeader {String} X-Api-Key Si jenis kss unique access-key.
	 * @apiPermission Si jenis ks Cant be Accessed permission name : api_si_jenis_ks_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Si jenis kss.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of si_jenis_ks.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError Si jenis ksNotFound Si jenis ks data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_si_jenis_ks_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'jenis_ks', 'deskripsi'];
		$si_jenis_ks = $this->model_api_si_jenis_ks->find($id, $select_field);

		if (!$si_jenis_ks) {
			$this->response([
					'status' 	=> false,
					'message' 	=> 'Blog not found'
				], API::HTTP_NOT_FOUND);
		}

					
		$data['si_jenis_ks'] = $si_jenis_ks;
		if ($data['si_jenis_ks']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Si jenis ks',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Si jenis ks not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /si_jenis_ks/add Add Si jenis ks.
	 * @apiVersion 0.1.0
	 * @apiName AddSi jenis ks
	 * @apiGroup si_jenis_ks
	 * @apiHeader {String} X-Api-Key Si jenis kss unique access-key.
	 * @apiPermission Si jenis ks Cant be Accessed permission name : api_si_jenis_ks_add
	 *
 	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_si_jenis_ks_add', false);

		
		if ($this->form_validation->run()) {

			$save_data = [
			];
			
			$save_si_jenis_ks = $this->model_api_si_jenis_ks->store($save_data);

			if ($save_si_jenis_ks) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /si_jenis_ks/update Update Si jenis ks.
	 * @apiVersion 0.1.0
	 * @apiName UpdateSi jenis ks
	 * @apiGroup si_jenis_ks
	 * @apiHeader {String} X-Api-Key Si jenis kss unique access-key.
	 * @apiPermission Si jenis ks Cant be Accessed permission name : api_si_jenis_ks_update
	 *
	 * @apiParam {Integer} id Mandatory id of Si Jenis Ks.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_si_jenis_ks_update', false);

		
		
		if ($this->form_validation->run()) {

			$save_data = [
			];
			
			$save_si_jenis_ks = $this->model_api_si_jenis_ks->change($this->post('id'), $save_data);

			if ($save_si_jenis_ks) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /si_jenis_ks/delete Delete Si jenis ks. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteSi jenis ks
	 * @apiGroup si_jenis_ks
	 * @apiHeader {String} X-Api-Key Si jenis kss unique access-key.
	 	 * @apiPermission Si jenis ks Cant be Accessed permission name : api_si_jenis_ks_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Si jenis kss .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_si_jenis_ks_delete', false);

		$si_jenis_ks = $this->model_api_si_jenis_ks->find($this->post('id'));

		if (!$si_jenis_ks) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Si jenis ks not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_si_jenis_ks->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Si jenis ks deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Si jenis ks not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
}

/* End of file Si jenis ks.php */
/* Location: ./application/controllers/api/Si jenis ks.php */