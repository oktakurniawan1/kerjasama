<form method="post" action="<?=site_url('administrator/si_jenis_ks/save_map_iku')?>" id="form-map-iku">
    <div class="contaner">
        <div class="row">
            <div class="col-md-12">
                <input type="hidden" name="id" value="<?=$id?>">
                <input type="hidden" name="val" value="<?=$val?>">
                <?php
                echo form_dropdown('idiku', $iku, $val, 'style=width:100%; id="iku"');
                ?>
            </div>
        </div>
    </div>
</form>
<script>
    $(function(){
        $('#iku').select2({
            dropdownParent: $('#my-modal'),
            theme: 'classic'
        });
    })
</script>