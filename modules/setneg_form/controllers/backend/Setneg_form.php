<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Setneg Form Controller
 *| --------------------------------------------------------------------------
 *| Setneg Form site
 *|
 */
class Setneg_form extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_setneg_form');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * show all Setneg Forms
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('setneg_form_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('setneg_form_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('setneg_form_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('setneg_form_filter');
			$this->session->unset_userdata('setneg_form_field');
		}
		$filter = $this->session->userdata('setneg_form_filter');
		$field = $this->session->userdata('setneg_form_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['setneg_forms'] = $this->model_setneg_form->get($filter, $field, $this->limit_page, $offset);
		$this->data['setneg_form_counts'] = $this->model_setneg_form->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/setneg_form/index/',
			'total_rows'   => $this->model_setneg_form->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Setneg Form List');
		$this->render('backend/standart/administrator/setneg_form/setneg_form_list', $this->data);
	}

	/**
	 * Add new setneg_forms
	 *
	 */
	public function add()
	{
		$this->is_allowed('setneg_form_add');

		$this->template->title('Setneg Form New');
		$this->render('backend/standart/administrator/setneg_form/setneg_form_add', $this->data);
	}

	/**
	 * Add New Setneg Forms
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('setneg_form_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('identitas', 'NIP', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('kd_prodi', 'Program Studi', 'trim|required');
		$this->form_validation->set_rules('tmp_lahir', 'Tempat Lahir', 'trim|required|max_length[40]');
		$this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'trim|required');
		$this->form_validation->set_rules('golru', 'Pangkat Golongan', 'trim|required|max_length[15]');
		$this->form_validation->set_rules('instansi', 'Asal Instansi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('almt_instansi', 'Alamat Instansi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tlp_instansi', 'Telepon Instansi', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('almt_rumah', 'Alamat Rumah', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('status_kawin', 'Status Perkawinan', 'trim|required');
		$this->form_validation->set_rules('almt_klg', 'Alamat Keluarga', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tlp_klg', 'Telepon Keluarga', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('bahasa_asing', 'Bahasa Asing', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('pend_s1', 'Pendidikan S1', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('pend_s2', 'Pendidikan S2', 'trim|max_length[255]');
		$this->form_validation->set_rules('pend_s3', 'Pendidikan S3', 'trim|max_length[255]');
		$this->form_validation->set_rules('negara_tujuan', 'Negara Tujuan', 'trim|required');
		$this->form_validation->set_rules('mob_setneg', 'Kegiatan', 'trim|required|max_length[4]');
		$this->form_validation->set_rules('lama_tinggal', 'Lama Tinggal', 'trim|required|max_length[3]|callback_valid_number');
		$this->form_validation->set_rules('setneg_form_file_ttd_name', 'File Tanda Tangan', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_loa_name', 'File LoA', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_rab_name', 'File RAB', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_drh_name', 'File DRH', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_ktp_name', 'File KTP', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_jdwkeg_name', 'File Kegiatan', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_relevansi_name', 'File Relevansi', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_kak_name', 'File Kerangka Keg.', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_ijin_name', 'File Ijin', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_sk_name', 'File SK', 'trim');


		if ($this->form_validation->run()) {
			$setneg_form_file_ttd_uuid = $this->input->post('setneg_form_file_ttd_uuid');
			$setneg_form_file_ttd_name = $this->input->post('setneg_form_file_ttd_name');
			$setneg_form_file_loa_uuid = $this->input->post('setneg_form_file_loa_uuid');
			$setneg_form_file_loa_name = $this->input->post('setneg_form_file_loa_name');
			$setneg_form_file_rab_uuid = $this->input->post('setneg_form_file_rab_uuid');
			$setneg_form_file_rab_name = $this->input->post('setneg_form_file_rab_name');
			$setneg_form_file_drh_uuid = $this->input->post('setneg_form_file_drh_uuid');
			$setneg_form_file_drh_name = $this->input->post('setneg_form_file_drh_name');
			$setneg_form_file_ktp_uuid = $this->input->post('setneg_form_file_ktp_uuid');
			$setneg_form_file_ktp_name = $this->input->post('setneg_form_file_ktp_name');
			$setneg_form_file_jdwkeg_uuid = $this->input->post('setneg_form_file_jdwkeg_uuid');
			$setneg_form_file_jdwkeg_name = $this->input->post('setneg_form_file_jdwkeg_name');
			$setneg_form_file_relevansi_uuid = $this->input->post('setneg_form_file_relevansi_uuid');
			$setneg_form_file_relevansi_name = $this->input->post('setneg_form_file_relevansi_name');
			$setneg_form_file_kak_uuid = $this->input->post('setneg_form_file_kak_uuid');
			$setneg_form_file_kak_name = $this->input->post('setneg_form_file_kak_name');
			$setneg_form_file_ijin_uuid = $this->input->post('setneg_form_file_ijin_uuid');
			$setneg_form_file_ijin_name = $this->input->post('setneg_form_file_ijin_name');
			$setneg_form_file_sk_uuid = $this->input->post('setneg_form_file_sk_uuid');
			$setneg_form_file_sk_name = $this->input->post('setneg_form_file_sk_name');

			$save_data = [
				'identitas' => $this->input->post('identitas'),
				'nama' => $this->input->post('nama'),
				'kd_prodi' => $this->input->post('kd_prodi'),
				'tmp_lahir' => $this->input->post('tmp_lahir'),
				'tgl_lahir' => $this->input->post('tgl_lahir'),
				'golru' => $this->input->post('golru'),
				'instansi' => $this->input->post('instansi'),
				'almt_instansi' => $this->input->post('almt_instansi'),
				'tlp_instansi' => $this->input->post('tlp_instansi'),
				'almt_rumah' => $this->input->post('almt_rumah'),
				'status_kawin' => $this->input->post('status_kawin'),
				'almt_klg' => $this->input->post('almt_klg'),
				'tlp_klg' => $this->input->post('tlp_klg'),
				'bahasa_asing' => $this->input->post('bahasa_asing'),
				'pend_s1' => $this->input->post('pend_s1'),
				'pend_s2' => $this->input->post('pend_s2'),
				'pend_s3' => $this->input->post('pend_s3'),
				'negara_tujuan' => $this->input->post('negara_tujuan'),
				'mob_setneg' => $this->input->post('mob_setneg'),
				'lama_tinggal' => $this->input->post('lama_tinggal'),
				'validasi' => $this->input->post('validasi'),
				'status_pengajuan' => $this->input->post('status_pengajuan'),
			];

			if (!is_dir(FCPATH . '/uploads/setneg_form/')) {
				mkdir(FCPATH . '/uploads/setneg_form/');
			}

			if (!empty($setneg_form_file_ttd_name)) {
				$setneg_form_file_ttd_name_copy = date('YmdHis') . '-' . $setneg_form_file_ttd_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_ttd_uuid . '/' . $setneg_form_file_ttd_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_ttd_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_ttd_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ttd'] = $setneg_form_file_ttd_name_copy;
			}

			if (!empty($setneg_form_file_loa_name)) {
				$setneg_form_file_loa_name_copy = date('YmdHis') . '-' . $setneg_form_file_loa_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_loa_uuid . '/' . $setneg_form_file_loa_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_loa_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_loa'] = $setneg_form_file_loa_name_copy;
			}

			if (!empty($setneg_form_file_rab_name)) {
				$setneg_form_file_rab_name_copy = date('YmdHis') . '-' . $setneg_form_file_rab_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_rab_uuid . '/' . $setneg_form_file_rab_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_rab_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_rab_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_rab'] = $setneg_form_file_rab_name_copy;
			}

			if (!empty($setneg_form_file_drh_name)) {
				$setneg_form_file_drh_name_copy = date('YmdHis') . '-' . $setneg_form_file_drh_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_drh_uuid . '/' . $setneg_form_file_drh_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_drh_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_drh_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_drh'] = $setneg_form_file_drh_name_copy;
			}

			if (!empty($setneg_form_file_ktp_name)) {
				$setneg_form_file_ktp_name_copy = date('YmdHis') . '-' . $setneg_form_file_ktp_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_ktp_uuid . '/' . $setneg_form_file_ktp_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_ktp_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_ktp_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ktp'] = $setneg_form_file_ktp_name_copy;
			}

			if (!empty($setneg_form_file_jdwkeg_name)) {
				$setneg_form_file_jdwkeg_name_copy = date('YmdHis') . '-' . $setneg_form_file_jdwkeg_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_jdwkeg_uuid . '/' . $setneg_form_file_jdwkeg_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_jdwkeg_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_jdwkeg_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_jdwkeg'] = $setneg_form_file_jdwkeg_name_copy;
			}

			if (!empty($setneg_form_file_relevansi_name)) {
				$setneg_form_file_relevansi_name_copy = date('YmdHis') . '-' . $setneg_form_file_relevansi_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_relevansi_uuid . '/' . $setneg_form_file_relevansi_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_relevansi_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_relevansi_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_relevansi'] = $setneg_form_file_relevansi_name_copy;
			}

			if (!empty($setneg_form_file_kak_name)) {
				$setneg_form_file_kak_name_copy = date('YmdHis') . '-' . $setneg_form_file_kak_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_kak_uuid . '/' . $setneg_form_file_kak_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_kak_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_kak_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_kak'] = $setneg_form_file_kak_name_copy;
			}

			if (!empty($setneg_form_file_ijin_name)) {
				$setneg_form_file_ijin_name_copy = date('YmdHis') . '-' . $setneg_form_file_ijin_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_ijin_uuid . '/' . $setneg_form_file_ijin_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_ijin_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_ijin_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ijin'] = $setneg_form_file_ijin_name_copy;
			}

			if (!empty($setneg_form_file_sk_name)) {
				$setneg_form_file_sk_name_copy = date('YmdHis') . '-' . $setneg_form_file_sk_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_sk_uuid . '/' . $setneg_form_file_sk_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_sk_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_sk_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_sk'] = $setneg_form_file_sk_name_copy;
			}


			$save_setneg_form = $this->model_setneg_form->store($save_data);


			if ($save_setneg_form) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_setneg_form;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/setneg_form/edit/' . $save_setneg_form, 'Edit Setneg Form'),
						anchor('administrator/setneg_form', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/setneg_form/edit/' . $save_setneg_form, 'Edit Setneg Form')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/setneg_form');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/setneg_form');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Setneg Forms
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('setneg_form_update');

		$this->data['setneg_form'] = $this->model_setneg_form->find($id);
		if (is_member('Dosen')) {
			if ($this->data['setneg_form']->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa mengubah, karena data sudah divalidasi', 'setneg_form'), 'warning');
				redirect_back();
			}
		}

		$this->template->title('Setneg Form Update');
		$this->render('backend/standart/administrator/setneg_form/setneg_form_update', $this->data);
	}

	/**
	 * Update Setneg Forms
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('setneg_form_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('identitas', 'NIP', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('kd_prodi', 'Program Studi', 'trim|required');
		$this->form_validation->set_rules('tmp_lahir', 'Tempat Lahir', 'trim|required|max_length[40]');
		$this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'trim|required');
		$this->form_validation->set_rules('golru', 'Pangkat Golongan', 'trim|required|max_length[15]');
		$this->form_validation->set_rules('instansi', 'Asal Instansi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('almt_instansi', 'Alamat Instansi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tlp_instansi', 'Telepon Instansi', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('almt_rumah', 'Alamat Rumah', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('status_kawin', 'Status Perkawinan', 'trim|required');
		$this->form_validation->set_rules('almt_klg', 'Alamat Keluarga', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tlp_klg', 'Telepon Keluarga', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('bahasa_asing', 'Bahasa Asing', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('pend_s1', 'Pendidikan S1', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('pend_s2', 'Pendidikan S2', 'trim|max_length[255]');
		$this->form_validation->set_rules('pend_s3', 'Pendidikan S3', 'trim|max_length[255]');
		$this->form_validation->set_rules('negara_tujuan', 'Negara Tujuan', 'trim|required');
		$this->form_validation->set_rules('mob_setneg', 'Kegiatan', 'trim|required|max_length[4]');
		$this->form_validation->set_rules('lama_tinggal', 'Lama Tinggal', 'trim|required|max_length[3]|callback_valid_number');
		$this->form_validation->set_rules('setneg_form_file_ttd_name', 'File Tanda Tangan', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_loa_name', 'File LoA', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_rab_name', 'File RAB', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_drh_name', 'File DRH', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_ktp_name', 'File KTP', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_jdwkeg_name', 'File Kegiatan', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_relevansi_name', 'File Relevansi', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_kak_name', 'File Kerangka Keg.', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_ijin_name', 'File Ijin', 'trim|required');
		$this->form_validation->set_rules('setneg_form_file_sk_name', 'File SK', 'trim');

		if ($this->form_validation->run()) {
			$setneg_form_file_ttd_uuid = $this->input->post('setneg_form_file_ttd_uuid');
			$setneg_form_file_ttd_name = $this->input->post('setneg_form_file_ttd_name');
			$setneg_form_file_loa_uuid = $this->input->post('setneg_form_file_loa_uuid');
			$setneg_form_file_loa_name = $this->input->post('setneg_form_file_loa_name');
			$setneg_form_file_rab_uuid = $this->input->post('setneg_form_file_rab_uuid');
			$setneg_form_file_rab_name = $this->input->post('setneg_form_file_rab_name');
			$setneg_form_file_drh_uuid = $this->input->post('setneg_form_file_drh_uuid');
			$setneg_form_file_drh_name = $this->input->post('setneg_form_file_drh_name');
			$setneg_form_file_ktp_uuid = $this->input->post('setneg_form_file_ktp_uuid');
			$setneg_form_file_ktp_name = $this->input->post('setneg_form_file_ktp_name');
			$setneg_form_file_jdwkeg_uuid = $this->input->post('setneg_form_file_jdwkeg_uuid');
			$setneg_form_file_jdwkeg_name = $this->input->post('setneg_form_file_jdwkeg_name');
			$setneg_form_file_relevansi_uuid = $this->input->post('setneg_form_file_relevansi_uuid');
			$setneg_form_file_relevansi_name = $this->input->post('setneg_form_file_relevansi_name');
			$setneg_form_file_kak_uuid = $this->input->post('setneg_form_file_kak_uuid');
			$setneg_form_file_kak_name = $this->input->post('setneg_form_file_kak_name');
			$setneg_form_file_ijin_uuid = $this->input->post('setneg_form_file_ijin_uuid');
			$setneg_form_file_ijin_name = $this->input->post('setneg_form_file_ijin_name');
			$setneg_form_file_sk_uuid = $this->input->post('setneg_form_file_sk_uuid');
			$setneg_form_file_sk_name = $this->input->post('setneg_form_file_sk_name');

			$save_data = [
				'identitas' => $this->input->post('identitas'),
				'nama' => $this->input->post('nama'),
				'kd_prodi' => $this->input->post('kd_prodi'),
				'tmp_lahir' => $this->input->post('tmp_lahir'),
				'tgl_lahir' => $this->input->post('tgl_lahir'),
				'golru' => $this->input->post('golru'),
				'instansi' => $this->input->post('instansi'),
				'almt_instansi' => $this->input->post('almt_instansi'),
				'tlp_instansi' => $this->input->post('tlp_instansi'),
				'almt_rumah' => $this->input->post('almt_rumah'),
				'status_kawin' => $this->input->post('status_kawin'),
				'almt_klg' => $this->input->post('almt_klg'),
				'tlp_klg' => $this->input->post('tlp_klg'),
				'bahasa_asing' => $this->input->post('bahasa_asing'),
				'pend_s1' => $this->input->post('pend_s1'),
				'pend_s2' => $this->input->post('pend_s2'),
				'pend_s3' => $this->input->post('pend_s3'),
				'negara_tujuan' => $this->input->post('negara_tujuan'),
				'mob_setneg' => $this->input->post('mob_setneg'),
				'lama_tinggal' => $this->input->post('lama_tinggal'),
				'validasi' => $this->input->post('validasi'),
				'status_pengajuan' => $this->input->post('status_pengajuan'),
			];

			if (!is_dir(FCPATH . '/uploads/setneg_form/')) {
				mkdir(FCPATH . '/uploads/setneg_form/');
			}

			if (!empty($setneg_form_file_ttd_uuid)) {
				$setneg_form_file_ttd_name_copy = date('YmdHis') . '-' . $setneg_form_file_ttd_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_ttd_uuid . '/' . $setneg_form_file_ttd_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_ttd_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_ttd_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ttd'] = $setneg_form_file_ttd_name_copy;
			}

			if (!empty($setneg_form_file_loa_uuid)) {
				$setneg_form_file_loa_name_copy = date('YmdHis') . '-' . $setneg_form_file_loa_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_loa_uuid . '/' . $setneg_form_file_loa_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_loa_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_loa'] = $setneg_form_file_loa_name_copy;
			}

			if (!empty($setneg_form_file_rab_uuid)) {
				$setneg_form_file_rab_name_copy = date('YmdHis') . '-' . $setneg_form_file_rab_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_rab_uuid . '/' . $setneg_form_file_rab_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_rab_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_rab_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_rab'] = $setneg_form_file_rab_name_copy;
			}

			if (!empty($setneg_form_file_drh_uuid)) {
				$setneg_form_file_drh_name_copy = date('YmdHis') . '-' . $setneg_form_file_drh_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_drh_uuid . '/' . $setneg_form_file_drh_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_drh_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_drh_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_drh'] = $setneg_form_file_drh_name_copy;
			}

			if (!empty($setneg_form_file_ktp_uuid)) {
				$setneg_form_file_ktp_name_copy = date('YmdHis') . '-' . $setneg_form_file_ktp_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_ktp_uuid . '/' . $setneg_form_file_ktp_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_ktp_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_ktp_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ktp'] = $setneg_form_file_ktp_name_copy;
			}

			if (!empty($setneg_form_file_jdwkeg_uuid)) {
				$setneg_form_file_jdwkeg_name_copy = date('YmdHis') . '-' . $setneg_form_file_jdwkeg_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_jdwkeg_uuid . '/' . $setneg_form_file_jdwkeg_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_jdwkeg_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_jdwkeg_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_jdwkeg'] = $setneg_form_file_jdwkeg_name_copy;
			}

			if (!empty($setneg_form_file_relevansi_uuid)) {
				$setneg_form_file_relevansi_name_copy = date('YmdHis') . '-' . $setneg_form_file_relevansi_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_relevansi_uuid . '/' . $setneg_form_file_relevansi_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_relevansi_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_relevansi_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_relevansi'] = $setneg_form_file_relevansi_name_copy;
			}

			if (!empty($setneg_form_file_kak_uuid)) {
				$setneg_form_file_kak_name_copy = date('YmdHis') . '-' . $setneg_form_file_kak_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_kak_uuid . '/' . $setneg_form_file_kak_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_kak_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_kak_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_kak'] = $setneg_form_file_kak_name_copy;
			}

			if (!empty($setneg_form_file_ijin_uuid)) {
				$setneg_form_file_ijin_name_copy = date('YmdHis') . '-' . $setneg_form_file_ijin_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_ijin_uuid . '/' . $setneg_form_file_ijin_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_ijin_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_ijin_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ijin'] = $setneg_form_file_ijin_name_copy;
			}

			if (!empty($setneg_form_file_sk_uuid)) {
				$setneg_form_file_sk_name_copy = date('YmdHis') . '-' . $setneg_form_file_sk_name;

				rename(
					FCPATH . 'uploads/tmp/' . $setneg_form_file_sk_uuid . '/' . $setneg_form_file_sk_name,
					FCPATH . 'uploads/setneg_form/' . $setneg_form_file_sk_name_copy
				);

				if (!is_file(FCPATH . '/uploads/setneg_form/' . $setneg_form_file_sk_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_sk'] = $setneg_form_file_sk_name_copy;
			}


			$save_setneg_form = $this->model_setneg_form->change($id, $save_data);

			if ($save_setneg_form) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/setneg_form', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/setneg_form/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/setneg_form/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Setneg Forms
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('setneg_form_delete');

		if (is_groups_in(['Dosen', 'Tendik'])) {
			$setneg	= $this->model_setneg_form->find($id);
			if ($setneg->identitas == fromsess('identitas')) {
				if ($setneg->validasi == 'Y') {
					set_message(cclang('Gagal hapus, karena data sudah divalidasi', 'setneg_form'), 'warning');
					redirect_back();
				}
			} else {
				set_message(cclang('sorry_you_do_not_have_permission_to_access', 'setneg_form'), 'error');
				redirect_back();
			}
		}

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'setneg_form'), 'success');
		} else {
			set_message(cclang('error_delete', 'setneg_form'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Setneg Forms
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('setneg_form_view');

		$this->data['setneg_form'] = $this->model_setneg_form->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Setneg Form Detail');
		$this->render('backend/standart/administrator/setneg_form/setneg_form_view', $this->data);
	}

	/**
	 * delete Setneg Forms
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$setneg_form = $this->model_setneg_form->find($id);

		if (!empty($setneg_form->file_ttd)) {
			$path = FCPATH . '/uploads/setneg_form/' . $setneg_form->file_ttd;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($setneg_form->file_loa)) {
			$path = FCPATH . '/uploads/setneg_form/' . $setneg_form->file_loa;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($setneg_form->file_rab)) {
			$path = FCPATH . '/uploads/setneg_form/' . $setneg_form->file_rab;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($setneg_form->file_drh)) {
			$path = FCPATH . '/uploads/setneg_form/' . $setneg_form->file_drh;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($setneg_form->file_ktp)) {
			$path = FCPATH . '/uploads/setneg_form/' . $setneg_form->file_ktp;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($setneg_form->file_jdwkeg)) {
			$path = FCPATH . '/uploads/setneg_form/' . $setneg_form->file_jdwkeg;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($setneg_form->file_relevansi)) {
			$path = FCPATH . '/uploads/setneg_form/' . $setneg_form->file_relevansi;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($setneg_form->file_kak)) {
			$path = FCPATH . '/uploads/setneg_form/' . $setneg_form->file_kak;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($setneg_form->file_ijin)) {
			$path = FCPATH . '/uploads/setneg_form/' . $setneg_form->file_ijin;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($setneg_form->file_sk)) {
			$path = FCPATH . '/uploads/setneg_form/' . $setneg_form->file_sk;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}


		return $this->model_setneg_form->remove($id);
	}

	/**
	 * Upload Image Setneg Form	* 
	 * @return JSON
	 */
	public function upload_file_ttd_file()
	{
		if (!$this->is_allowed('setneg_form_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'setneg_form',
			'allowed_types' => 'jpg|jpeg',
			'max_size' 	 	=> 200,
		]);
	}

	/**
	 * Delete Image Setneg Form	* 
	 * @return JSON
	 */
	public function delete_file_ttd_file($uuid)
	{
		if (!$this->is_allowed('setneg_form_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ttd',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/'
		]);
	}

	/**
	 * Get Image Setneg Form	* 
	 * @return JSON
	 */
	public function get_file_ttd_file($id)
	{
		if (!$this->is_allowed('setneg_form_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$setneg_form = $this->model_setneg_form->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ttd',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/',
			'delete_endpoint'   => 'administrator/setneg_form/delete_file_ttd_file'
		]);
	}

	/**
	 * Upload Image Setneg Form	* 
	 * @return JSON
	 */
	public function upload_file_loa_file()
	{
		if (!$this->is_allowed('setneg_form_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'setneg_form',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Setneg Form	* 
	 * @return JSON
	 */
	public function delete_file_loa_file($uuid)
	{
		if (!$this->is_allowed('setneg_form_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_loa',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/'
		]);
	}

	/**
	 * Get Image Setneg Form	* 
	 * @return JSON
	 */
	public function get_file_loa_file($id)
	{
		if (!$this->is_allowed('setneg_form_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$setneg_form = $this->model_setneg_form->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_loa',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/',
			'delete_endpoint'   => 'administrator/setneg_form/delete_file_loa_file'
		]);
	}

	/**
	 * Upload Image Setneg Form	* 
	 * @return JSON
	 */
	public function upload_file_rab_file()
	{
		if (!$this->is_allowed('setneg_form_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'setneg_form',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Setneg Form	* 
	 * @return JSON
	 */
	public function delete_file_rab_file($uuid)
	{
		if (!$this->is_allowed('setneg_form_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_rab',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/'
		]);
	}

	/**
	 * Get Image Setneg Form	* 
	 * @return JSON
	 */
	public function get_file_rab_file($id)
	{
		if (!$this->is_allowed('setneg_form_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$setneg_form = $this->model_setneg_form->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_rab',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/',
			'delete_endpoint'   => 'administrator/setneg_form/delete_file_rab_file'
		]);
	}

	/**
	 * Upload Image Setneg Form	* 
	 * @return JSON
	 */
	public function upload_file_drh_file()
	{
		if (!$this->is_allowed('setneg_form_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'setneg_form',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Setneg Form	* 
	 * @return JSON
	 */
	public function delete_file_drh_file($uuid)
	{
		if (!$this->is_allowed('setneg_form_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_drh',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/'
		]);
	}

	/**
	 * Get Image Setneg Form	* 
	 * @return JSON
	 */
	public function get_file_drh_file($id)
	{
		if (!$this->is_allowed('setneg_form_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$setneg_form = $this->model_setneg_form->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_drh',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/',
			'delete_endpoint'   => 'administrator/setneg_form/delete_file_drh_file'
		]);
	}

	/**
	 * Upload Image Setneg Form	* 
	 * @return JSON
	 */
	public function upload_file_ktp_file()
	{
		if (!$this->is_allowed('setneg_form_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'setneg_form',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Setneg Form	* 
	 * @return JSON
	 */
	public function delete_file_ktp_file($uuid)
	{
		if (!$this->is_allowed('setneg_form_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ktp',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/'
		]);
	}

	/**
	 * Get Image Setneg Form	* 
	 * @return JSON
	 */
	public function get_file_ktp_file($id)
	{
		if (!$this->is_allowed('setneg_form_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$setneg_form = $this->model_setneg_form->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ktp',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/',
			'delete_endpoint'   => 'administrator/setneg_form/delete_file_ktp_file'
		]);
	}

	/**
	 * Upload Image Setneg Form	* 
	 * @return JSON
	 */
	public function upload_file_jdwkeg_file()
	{
		if (!$this->is_allowed('setneg_form_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'setneg_form',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Setneg Form	* 
	 * @return JSON
	 */
	public function delete_file_jdwkeg_file($uuid)
	{
		if (!$this->is_allowed('setneg_form_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_jdwkeg',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/'
		]);
	}

	/**
	 * Get Image Setneg Form	* 
	 * @return JSON
	 */
	public function get_file_jdwkeg_file($id)
	{
		if (!$this->is_allowed('setneg_form_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$setneg_form = $this->model_setneg_form->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_jdwkeg',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/',
			'delete_endpoint'   => 'administrator/setneg_form/delete_file_jdwkeg_file'
		]);
	}

	/**
	 * Upload Image Setneg Form	* 
	 * @return JSON
	 */
	public function upload_file_relevansi_file()
	{
		if (!$this->is_allowed('setneg_form_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'setneg_form',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Setneg Form	* 
	 * @return JSON
	 */
	public function delete_file_relevansi_file($uuid)
	{
		if (!$this->is_allowed('setneg_form_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_relevansi',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/'
		]);
	}

	/**
	 * Get Image Setneg Form	* 
	 * @return JSON
	 */
	public function get_file_relevansi_file($id)
	{
		if (!$this->is_allowed('setneg_form_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$setneg_form = $this->model_setneg_form->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_relevansi',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/',
			'delete_endpoint'   => 'administrator/setneg_form/delete_file_relevansi_file'
		]);
	}

	/**
	 * Upload Image Setneg Form	* 
	 * @return JSON
	 */
	public function upload_file_kak_file()
	{
		if (!$this->is_allowed('setneg_form_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'setneg_form',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Setneg Form	* 
	 * @return JSON
	 */
	public function delete_file_kak_file($uuid)
	{
		if (!$this->is_allowed('setneg_form_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_kak',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/'
		]);
	}

	/**
	 * Get Image Setneg Form	* 
	 * @return JSON
	 */
	public function get_file_kak_file($id)
	{
		if (!$this->is_allowed('setneg_form_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$setneg_form = $this->model_setneg_form->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_kak',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/',
			'delete_endpoint'   => 'administrator/setneg_form/delete_file_kak_file'
		]);
	}

	/**
	 * Upload Image Setneg Form	* 
	 * @return JSON
	 */
	public function upload_file_ijin_file()
	{
		if (!$this->is_allowed('setneg_form_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'setneg_form',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Setneg Form	* 
	 * @return JSON
	 */
	public function delete_file_ijin_file($uuid)
	{
		if (!$this->is_allowed('setneg_form_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ijin',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/'
		]);
	}

	/**
	 * Get Image Setneg Form	* 
	 * @return JSON
	 */
	public function get_file_ijin_file($id)
	{
		if (!$this->is_allowed('setneg_form_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$setneg_form = $this->model_setneg_form->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ijin',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/',
			'delete_endpoint'   => 'administrator/setneg_form/delete_file_ijin_file'
		]);
	}

	/**
	 * Upload Image Setneg Form	* 
	 * @return JSON
	 */
	public function upload_file_sk_file()
	{
		if (!$this->is_allowed('setneg_form_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'setneg_form',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Setneg Form	* 
	 * @return JSON
	 */
	public function delete_file_sk_file($uuid)
	{
		if (!$this->is_allowed('setneg_form_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_sk',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/'
		]);
	}

	/**
	 * Get Image Setneg Form	* 
	 * @return JSON
	 */
	public function get_file_sk_file($id)
	{
		if (!$this->is_allowed('setneg_form_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$setneg_form = $this->model_setneg_form->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_sk',
			'table_name'        => 'setneg_form',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/setneg_form/',
			'delete_endpoint'   => 'administrator/setneg_form/delete_file_sk_file'
		]);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('setneg_form_export');

		$this->model_setneg_form->export(
			'setneg_form',
			'setneg_form',
			$this->model_setneg_form->field_search
		);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('setneg_form_export');

		$this->model_setneg_form->pdf('setneg_form', 'setneg_form');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('setneg_form_export');

		$table = $title = 'setneg_form';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_setneg_form->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
	public function form_setneg_pdf($id)
	{
		$this->is_allowed('setneg_form_view');

		$this->load->helper('string');
		$user = $this->model_setneg_form->join_avaiable()->filter_avaiable()->find($id);

		$template = new \PhpOffice\PhpWord\TemplateProcessor(FCPATH . 'uploads/word/form_setneg.docx');

		$user = obj2arr($user);
		foreach ($user as $i => $val) {
			$template->setValue($i, $val);
		}
		$template->setImageValue('ttd', array('path' => FCPATH . 'uploads/setneg_form/' . $user['file_ttd'], 'width' => 100, 'height' => 100, 'ratio' => true));

		$strname	= random_string('alnum', 8);
		$file_doc   = FCPATH . 'uploads/word/' . $strname . '.docx';
		$template->saveAS($file_doc);
		$file_pdf	= FCPATH . 'uploads/word/' . $strname . '.pdf';
		$unoconv 	= \Unoconv\Unoconv::create();
		$unoconv->transcode($file_doc, 'pdf', $file_pdf);

		/*  Run Backgroud
            shell_exec("/usr/bin/unoconv -f pdf ".base_path('storage/files/'.$file_doc)." /dev/null 2>&1 &");
        */
		/*  This method to execute a php script within a php page without making the user wait 
            until it completes executing.
            shell_exec("php Server.php > /dev/null 2>/dev/null &")
        */

		//  wait until finish dont add ">/dev/null 2>&1 &"
		//  shell_exec("/usr/bin/unoconv -f pdf ".base_path('storage/files/'.$file_doc));
		ob_start();
		readfile($file_pdf);
		$content = ob_get_contents();
		ob_get_clean();

		@unlink($file_doc); //hapus doc
		@unlink($file_pdf); //hapus pdf

		header('Content-type: application/pdf');
		header('Content-Disposition: inline; filename=filename.pdf');
		echo $content;
	}
	public function form_cari_pegawai()
	{
		$data['pegawai'] = $this->model_setneg_form->get_karyawan();
		$this->load->view('_cari_pegawai', $data);
	}
}


/* End of file setneg_form.php */
/* Location: ./application/controllers/administrator/Setneg Form.php */
