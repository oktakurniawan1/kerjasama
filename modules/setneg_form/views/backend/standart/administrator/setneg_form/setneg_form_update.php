<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>

<link href="<?= BASE_ASSET ?>/nakupanda/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?= BASE_ASSET ?>/nakupanda/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="<?= BASE_ASSET ?>/nakupanda/bootstrap.min.js"></script>
<script src="<?= BASE_ASSET ?>/nakupanda/bootstrap-dialog.min.js"></script>

<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo() {

        // Binding keys
        $('*').bind('keydown', 'Ctrl+s', function assets() {
            $('#btn_save').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+x', function assets() {
            $('#btn_cancel').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+d', function assets() {
            $('.btn_save_back').trigger('click');
            return false;
        });

    }

    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Form Pengajuan Setneg <small>Edit Form Pengajuan Setneg</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="<?= site_url('administrator/setneg_form'); ?>">Form Pengajuan Setneg</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Form Pengajuan Setneg</h3>
                            <h5 class="widget-user-desc">Edit Form Pengajuan Setneg</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/setneg_form/edit_save/' . $this->uri->segment(4)), [
                            'name'    => 'form_setneg_form',
                            'class'   => 'form-horizontal form-step',
                            'id'      => 'form_setneg_form',
                            'method'  => 'POST'
                        ]); ?>

                        <div class="form-group ">
                            <label for="identitas" class="col-sm-2 control-label">NIP
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <?php if(!is_groups_in(['Admin','Kerjasama'])) : ?>
                                    <input type="text" class="form-control" name="identitas" id="identitas" placeholder="NIP" value="<?= set_value('identitas', $setneg_form->identitas); ?>" readonly>
                                <?php else:?>
                                    <input type="text" class="form-control" name="identitas" id="identitas" placeholder="NIP" value="<?= set_value('identitas', $setneg_form->identitas); ?>">
                                    <button style='margin-top:10px' class="btn btn-primary btn-sm" type="button" onclick="cari_pegawai()">Cari di Simpeg</button>
                                <?php endif;?>
                                <small class="info help-block">
                                    <b>Input Identitas</b> Max Length : 20.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="nama" class="col-sm-2 control-label">Nama
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                            <?php if(!is_groups_in(['Admin','Kerjasama'])) : ?>
                                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?= set_value('nama', $setneg_form->nama); ?>" readonly>
                            <?php else:?>
                                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?= set_value('nama', $setneg_form->nama); ?>">
                            <?php endif;?>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="kd_prodi" class="col-sm-2 control-label">Program studi/Unit
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control chosen chosen-select-deselect" name="kd_prodi" id="kd_prodi" data-placeholder="Program studi">
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('ref_prodi') as $row) : ?>
                                        <option <?= $row->kode ==  $setneg_form->kd_prodi ? 'selected' : ''; ?> value="<?= $row->kode ?>"><?= $row->kode .' - '.$row->nama_prodi; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <small class="info help-block">
                                    Pilih<b> Program studi</b></small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="tmp_lahir" class="col-sm-2 control-label">Tempat Lahir
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="tmp_lahir" id="tmp_lahir" placeholder="Tempat Lahir" value="<?= set_value('tmp_lahir', $setneg_form->tmp_lahir); ?>">
                                <small class="info help-block">
                                    <b>Input Tmp Lahir</b> Max Length : 40.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="tgl_lahir" class="col-sm-2 control-label">Tanggal Lahir
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                                <div class="input-group date col-sm-8">
                                    <input type="text" class="form-control pull-right datepicker" name="tgl_lahir" placeholder="Tanggal Lahir" id="tgl_lahir" value="<?= set_value('tgl_lahir', $setneg_form->tgl_lahir); ?>">
                                </div>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>


                        <div class="form-group ">
                            <label for="golru" class="col-sm-2 control-label">Pangkat Golongan
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="golru" id="golru" placeholder="Pangkat Golongan" value="<?= set_value('golru', $setneg_form->golru); ?>">
                                <small class="info help-block">
                                    <b>Input Golru</b> Max Length : 15.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="instansi" class="col-sm-2 control-label">Asal Instansi
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="instansi" id="instansi" placeholder="Asal Instansi" value="<?= set_value('instansi', $setneg_form->instansi); ?>">
                                <small class="info help-block">
                                    <b>Input Instansi</b> Max Length : 255.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="almt_instansi" class="col-sm-2 control-label">Alamat Instansi
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="almt_instansi" id="almt_instansi" placeholder="Alamat Instansi" value="<?= set_value('almt_instansi', $setneg_form->almt_instansi); ?>">
                                <small class="info help-block">
                                    <b>Input Almt Instansi</b> Max Length : 255.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="tlp_instansi" class="col-sm-2 control-label">Telepon Instansi
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="tlp_instansi" id="tlp_instansi" placeholder="Telepon Instansi" value="<?= set_value('tlp_instansi', $setneg_form->tlp_instansi); ?>">
                                <small class="info help-block">
                                    <b>Input Tlp Instansi</b> Max Length : 20.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="almt_rumah" class="col-sm-2 control-label">Alamat Rumah
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="almt_rumah" id="almt_rumah" placeholder="Alamat Rumah" value="<?= set_value('almt_rumah', $setneg_form->almt_rumah); ?>">
                                <small class="info help-block">
                                    <b>Input Almt Rumah</b> Max Length : 255.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="status_kawin" class="col-sm-2 control-label">Status Perkawinan
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control chosen chosen-select" name="status_kawin" id="status_kawin" data-placeholder="Select Status Perkawinan">
                                    <option value=""></option>
                                    <option <?= $setneg_form->status_kawin == "K" ? 'selected' : ''; ?> value="K">Kawin</option>
                                    <option <?= $setneg_form->status_kawin == "D" ? 'selected' : ''; ?> value="D">Duda</option>
                                    <option <?= $setneg_form->status_kawin == "J" ? 'selected' : ''; ?> value="J">Janda</option>
                                    <option <?= $setneg_form->status_kawin == "BK" ? 'selected' : ''; ?> value="BK">Belum Kawin</option>
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="almt_klg" class="col-sm-2 control-label">Alamat Keluarga
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="almt_klg" id="almt_klg" placeholder="Alamat Keluarga" value="<?= set_value('almt_klg', $setneg_form->almt_klg); ?>">
                                <small class="info help-block">
                                    <b>Input Almt Klg</b> Max Length : 255.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="tlp_klg" class="col-sm-2 control-label">Telepon Keluarga
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="tlp_klg" id="tlp_klg" placeholder="Telepon Keluarga" value="<?= set_value('tlp_klg', $setneg_form->tlp_klg); ?>">
                                <small class="info help-block">
                                    <b>Input Tlp Klg</b> Max Length : 255.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="bahasa_asing" class="col-sm-2 control-label">Bahasa Asing
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="bahasa_asing" id="bahasa_asing" placeholder="Bahasa Asing" value="<?= set_value('bahasa_asing', $setneg_form->bahasa_asing); ?>">
                                <small class="info help-block">
                                    <b>Input Bahasa Asing</b> Max Length : 255.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="pend_s1" class="col-sm-2 control-label">Pendidikan S1
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="pend_s1" id="pend_s1" placeholder="Pendidikan S1" value="<?= set_value('pend_s1', $setneg_form->pend_s1); ?>">
                                <small class="info help-block">
                                    <b>Input Pend S1</b> Max Length : 255.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="pend_s2" class="col-sm-2 control-label">Pendidikan S2
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="pend_s2" id="pend_s2" placeholder="Pendidikan S2" value="<?= set_value('pend_s2', $setneg_form->pend_s2); ?>">
                                <small class="info help-block">
                                    <b>Input Pend S2</b> Max Length : 255.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="pend_s3" class="col-sm-2 control-label">Pendidikan S3
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="pend_s3" id="pend_s3" placeholder="Pendidikan S3" value="<?= set_value('pend_s3', $setneg_form->pend_s3); ?>">
                                <small class="info help-block">
                                    <b>Input Pend S3</b> Max Length : 255.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="negara_tujuan" class="col-sm-2 control-label">Negara Tujuan
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control chosen chosen-select-deselect" name="negara_tujuan" id="negara_tujuan" data-placeholder="Select Negara Tujuan">
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('negara') as $row) : ?>
                                        <option <?= $row->nama_negara ==  $setneg_form->negara_tujuan ? 'selected' : ''; ?> value="<?= $row->nama_negara ?>"><?= $row->nama_negara; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <small class="info help-block">
                                    <b>Input Negara Tujuan</b></small>
                            </div>
                        </div>



                        <div class="form-group ">
                            <label for="mob_setneg" class="col-sm-2 control-label">Kegiatan
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control chosen chosen-select-deselect" name="mob_setneg" id="mob_setneg" data-placeholder="Select Kegiatan">
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('keg_setneg') as $row) : ?>
                                        <option <?= $row->id ==  $setneg_form->mob_setneg ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->keg_setneg; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <small class="info help-block">
                                    <b>Input Mob Setneg</b> Max Length : 4.</small>
                            </div>
                        </div>



                        <div class="form-group ">
                            <label for="lama_tinggal" class="col-sm-2 control-label">Lama Tinggal
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="lama_tinggal" id="lama_tinggal" placeholder="Lama Tinggal" value="<?= set_value('lama_tinggal', $setneg_form->lama_tinggal); ?>">
                                <small class="info help-block">
                                    <b>Format Lama Tinggal must</b> Valid Number, <b>Input Lama Tinggal</b> Max Length : 3.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="file_ttd" class="col-sm-2 control-label">File Tanda Tangan
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="setneg_form_file_ttd_galery"></div>
                                <input class="data_file data_file_uuid" name="setneg_form_file_ttd_uuid" id="setneg_form_file_ttd_uuid" type="hidden" value="<?= set_value('setneg_form_file_ttd_uuid'); ?>">
                                <input class="data_file" name="setneg_form_file_ttd_name" id="setneg_form_file_ttd_name" type="hidden" value="<?= set_value('setneg_form_file_ttd_name', $setneg_form->file_ttd); ?>">
                                <small class="info help-block">
                                    <b>Extension file must</b> JPG,JPEG, <b>Max size file</b> 200 kb.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="file_loa" class="col-sm-2 control-label">File LoA
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="setneg_form_file_loa_galery"></div>
                                <input class="data_file data_file_uuid" name="setneg_form_file_loa_uuid" id="setneg_form_file_loa_uuid" type="hidden" value="<?= set_value('setneg_form_file_loa_uuid'); ?>">
                                <input class="data_file" name="setneg_form_file_loa_name" id="setneg_form_file_loa_name" type="hidden" value="<?= set_value('setneg_form_file_loa_name', $setneg_form->file_loa); ?>">
                                <small class="info help-block">
                                    <b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="file_rab" class="col-sm-2 control-label">File RAB
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="setneg_form_file_rab_galery"></div>
                                <input class="data_file data_file_uuid" name="setneg_form_file_rab_uuid" id="setneg_form_file_rab_uuid" type="hidden" value="<?= set_value('setneg_form_file_rab_uuid'); ?>">
                                <input class="data_file" name="setneg_form_file_rab_name" id="setneg_form_file_rab_name" type="hidden" value="<?= set_value('setneg_form_file_rab_name', $setneg_form->file_rab); ?>">
                                <small class="info help-block">
                                    <b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="file_drh" class="col-sm-2 control-label">File DRH
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="setneg_form_file_drh_galery"></div>
                                <input class="data_file data_file_uuid" name="setneg_form_file_drh_uuid" id="setneg_form_file_drh_uuid" type="hidden" value="<?= set_value('setneg_form_file_drh_uuid'); ?>">
                                <input class="data_file" name="setneg_form_file_drh_name" id="setneg_form_file_drh_name" type="hidden" value="<?= set_value('setneg_form_file_drh_name', $setneg_form->file_drh); ?>">
                                <small class="info help-block">
                                    <b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="file_ktp" class="col-sm-2 control-label">File KTP
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="setneg_form_file_ktp_galery"></div>
                                <input class="data_file data_file_uuid" name="setneg_form_file_ktp_uuid" id="setneg_form_file_ktp_uuid" type="hidden" value="<?= set_value('setneg_form_file_ktp_uuid'); ?>">
                                <input class="data_file" name="setneg_form_file_ktp_name" id="setneg_form_file_ktp_name" type="hidden" value="<?= set_value('setneg_form_file_ktp_name', $setneg_form->file_ktp); ?>">
                                <small class="info help-block">
                                    <b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="file_jdwkeg" class="col-sm-2 control-label">File Kegiatan
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="setneg_form_file_jdwkeg_galery"></div>
                                <input class="data_file data_file_uuid" name="setneg_form_file_jdwkeg_uuid" id="setneg_form_file_jdwkeg_uuid" type="hidden" value="<?= set_value('setneg_form_file_jdwkeg_uuid'); ?>">
                                <input class="data_file" name="setneg_form_file_jdwkeg_name" id="setneg_form_file_jdwkeg_name" type="hidden" value="<?= set_value('setneg_form_file_jdwkeg_name', $setneg_form->file_jdwkeg); ?>">
                                <small class="info help-block">
                                    <b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="file_relevansi" class="col-sm-2 control-label">File Relevansi
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="setneg_form_file_relevansi_galery"></div>
                                <input class="data_file data_file_uuid" name="setneg_form_file_relevansi_uuid" id="setneg_form_file_relevansi_uuid" type="hidden" value="<?= set_value('setneg_form_file_relevansi_uuid'); ?>">
                                <input class="data_file" name="setneg_form_file_relevansi_name" id="setneg_form_file_relevansi_name" type="hidden" value="<?= set_value('setneg_form_file_relevansi_name', $setneg_form->file_relevansi); ?>">
                                <small class="info help-block">
                                    <b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="file_kak" class="col-sm-2 control-label">File Kerangka Keg.
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="setneg_form_file_kak_galery"></div>
                                <input class="data_file data_file_uuid" name="setneg_form_file_kak_uuid" id="setneg_form_file_kak_uuid" type="hidden" value="<?= set_value('setneg_form_file_kak_uuid'); ?>">
                                <input class="data_file" name="setneg_form_file_kak_name" id="setneg_form_file_kak_name" type="hidden" value="<?= set_value('setneg_form_file_kak_name', $setneg_form->file_kak); ?>">
                                <small class="info help-block">
                                    <b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="file_ijin" class="col-sm-2 control-label">File Ijin
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="setneg_form_file_ijin_galery"></div>
                                <input class="data_file data_file_uuid" name="setneg_form_file_ijin_uuid" id="setneg_form_file_ijin_uuid" type="hidden" value="<?= set_value('setneg_form_file_ijin_uuid'); ?>">
                                <input class="data_file" name="setneg_form_file_ijin_name" id="setneg_form_file_ijin_name" type="hidden" value="<?= set_value('setneg_form_file_ijin_name', $setneg_form->file_ijin); ?>">
                                <small class="info help-block">
                                    <b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
                            </div>
                        </div>
                        <?php if(is_groups_in(['Admin','Kerjasama'])): ?> 
                        <div class="form-group ">
                            <label for="validasi" class="col-sm-2 control-label">Validasi
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control chosen chosen-select" name="validasi" id="validasi" data-placeholder="Select Validasi">
                                    <option value=""></option>
                                    <option <?= $setneg_form->validasi == "Y" ? 'selected' : ''; ?> value="Y">Ya</option>
                                    <option <?= $setneg_form->validasi == "T" ? 'selected' : ''; ?> value="T">Tidak</option>
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="status_pengajuan" class="col-sm-2 control-label">Status
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control chosen chosen-select" name="status_pengajuan" id="status_pengajuan" data-placeholder="Select Status">
                                    <option value=""></option>
                                    <option <?= $setneg_form->status_pengajuan == "Y" ? 'selected' : ''; ?> value="Y">Diterima</option>
                                    <option <?= $setneg_form->status_pengajuan == "T" ? 'selected' : ''; ?> value="T">Ditolak</option>
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="file_sk" class="col-sm-2 control-label">File SK
                            </label>
                            <div class="col-sm-8">
                                <div id="setneg_form_file_sk_galery"></div>
                                <input class="data_file data_file_uuid" name="setneg_form_file_sk_uuid" id="setneg_form_file_sk_uuid" type="hidden" value="<?= set_value('setneg_form_file_sk_uuid'); ?>">
                                <input class="data_file" name="setneg_form_file_sk_name" id="setneg_form_file_sk_name" type="hidden" value="<?= set_value('setneg_form_file_sk_name', $setneg_form->file_sk); ?>">
                                <small class="info help-block">
                                    <b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
                            </div>
                        </div>
                        <?php endif;?>

                        <div class="message"></div>
                        <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                                <i class="fa fa-save"></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                                <i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                                <i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                                <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
                                <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function() {



        $('#btn_cancel').click(function() {
            swal({
                    title: "Are you sure?",
                    text: "the data that you have created will be in the exhaust!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes!",
                    cancelButtonText: "No!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                        window.location.href = BASE_URL + 'administrator/setneg_form/index/' + "<?= $this->session->userdata('_page') ?>";
                    }
                });

            return false;
        }); /*end btn cancel*/

        $('.btn_save').click(function() {
            $('.message').fadeOut();

            var form_setneg_form = $('#form_setneg_form');
            var data_post = form_setneg_form.serializeArray();
            var save_type = $(this).attr('data-stype');
            data_post.push({
                name: 'save_type',
                value: save_type
            });

            $('.loading').show();

            $.ajax({
                    url: form_setneg_form.attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: data_post,
                })
                .done(function(res) {
                    $('form').find('.form-group').removeClass('has-error');
                    $('form').find('.error-input').remove();
                    $('.steps li').removeClass('error');
                    if (res.success) {
                        var id = $('#setneg_form_image_galery').find('li').attr('qq-file-id');
                        if (save_type == 'back') {
                            window.location.href = res.redirect;
                            return;
                        }

                        $('.message').printMessage({
                            message: res.message
                        });
                        $('.message').fadeIn();
                        $('.data_file_uuid').val('');

                    } else {
                        if (res.errors) {
                            parseErrorField(res.errors);
                        }
                        $('.message').printMessage({
                            message: res.message,
                            type: 'warning'
                        });
                    }

                })
                .fail(function() {
                    $('.message').printMessage({
                        message: 'Error save data',
                        type: 'warning'
                    });
                })
                .always(function() {
                    $('.loading').hide();
                    $('html, body').animate({
                        scrollTop: $(document).height()
                    }, 2000);
                });

            return false;
        }); /*end btn save*/

        var params = {};
        params[csrf] = token;

        $('#setneg_form_file_ttd_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/setneg_form/upload_file_ttd_file',
                params: params
            },
            deleteFile: {
                enabled: true, // defaults to false
                endpoint: BASE_URL + '/administrator/setneg_form/delete_file_ttd_file'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            session: {
                endpoint: BASE_URL + 'administrator/setneg_form/get_file_ttd_file/<?= $setneg_form->id; ?>',
                refreshOnRequest: true
            },
            multiple: false,
            validation: {
                allowedExtensions: ["jpg", "jpeg"],
                sizeLimit: 204800,
            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#setneg_form_file_ttd_galery').fineUploader('getUuid', id);
                        $('#setneg_form_file_ttd_uuid').val(uuid);
                        $('#setneg_form_file_ttd_name').val(xhr.uploadName);
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onSubmit: function(id, name) {
                    var uuid = $('#setneg_form_file_ttd_uuid').val();
                    $.get(BASE_URL + '/administrator/setneg_form/delete_file_ttd_file/' + uuid);
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#setneg_form_file_ttd_uuid').val('');
                        $('#setneg_form_file_ttd_name').val('');
                    }
                }
            }
        }); /*end file_ttd galey*/
        var params = {};
        params[csrf] = token;

        $('#setneg_form_file_loa_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/setneg_form/upload_file_loa_file',
                params: params
            },
            deleteFile: {
                enabled: true, // defaults to false
                endpoint: BASE_URL + '/administrator/setneg_form/delete_file_loa_file'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            session: {
                endpoint: BASE_URL + 'administrator/setneg_form/get_file_loa_file/<?= $setneg_form->id; ?>',
                refreshOnRequest: true
            },
            multiple: false,
            validation: {
                allowedExtensions: ["pdf"],
                sizeLimit: 5242880,
            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#setneg_form_file_loa_galery').fineUploader('getUuid', id);
                        $('#setneg_form_file_loa_uuid').val(uuid);
                        $('#setneg_form_file_loa_name').val(xhr.uploadName);
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onSubmit: function(id, name) {
                    var uuid = $('#setneg_form_file_loa_uuid').val();
                    $.get(BASE_URL + '/administrator/setneg_form/delete_file_loa_file/' + uuid);
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#setneg_form_file_loa_uuid').val('');
                        $('#setneg_form_file_loa_name').val('');
                    }
                }
            }
        }); /*end file_loa galey*/
        var params = {};
        params[csrf] = token;

        $('#setneg_form_file_rab_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/setneg_form/upload_file_rab_file',
                params: params
            },
            deleteFile: {
                enabled: true, // defaults to false
                endpoint: BASE_URL + '/administrator/setneg_form/delete_file_rab_file'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            session: {
                endpoint: BASE_URL + 'administrator/setneg_form/get_file_rab_file/<?= $setneg_form->id; ?>',
                refreshOnRequest: true
            },
            multiple: false,
            validation: {
                allowedExtensions: ["pdf"],
                sizeLimit: 5242880,
            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#setneg_form_file_rab_galery').fineUploader('getUuid', id);
                        $('#setneg_form_file_rab_uuid').val(uuid);
                        $('#setneg_form_file_rab_name').val(xhr.uploadName);
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onSubmit: function(id, name) {
                    var uuid = $('#setneg_form_file_rab_uuid').val();
                    $.get(BASE_URL + '/administrator/setneg_form/delete_file_rab_file/' + uuid);
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#setneg_form_file_rab_uuid').val('');
                        $('#setneg_form_file_rab_name').val('');
                    }
                }
            }
        }); /*end file_rab galey*/
        var params = {};
        params[csrf] = token;

        $('#setneg_form_file_drh_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/setneg_form/upload_file_drh_file',
                params: params
            },
            deleteFile: {
                enabled: true, // defaults to false
                endpoint: BASE_URL + '/administrator/setneg_form/delete_file_drh_file'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            session: {
                endpoint: BASE_URL + 'administrator/setneg_form/get_file_drh_file/<?= $setneg_form->id; ?>',
                refreshOnRequest: true
            },
            multiple: false,
            validation: {
                allowedExtensions: ["pdf"],
                sizeLimit: 5242880,
            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#setneg_form_file_drh_galery').fineUploader('getUuid', id);
                        $('#setneg_form_file_drh_uuid').val(uuid);
                        $('#setneg_form_file_drh_name').val(xhr.uploadName);
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onSubmit: function(id, name) {
                    var uuid = $('#setneg_form_file_drh_uuid').val();
                    $.get(BASE_URL + '/administrator/setneg_form/delete_file_drh_file/' + uuid);
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#setneg_form_file_drh_uuid').val('');
                        $('#setneg_form_file_drh_name').val('');
                    }
                }
            }
        }); /*end file_drh galey*/
        var params = {};
        params[csrf] = token;

        $('#setneg_form_file_ktp_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/setneg_form/upload_file_ktp_file',
                params: params
            },
            deleteFile: {
                enabled: true, // defaults to false
                endpoint: BASE_URL + '/administrator/setneg_form/delete_file_ktp_file'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            session: {
                endpoint: BASE_URL + 'administrator/setneg_form/get_file_ktp_file/<?= $setneg_form->id; ?>',
                refreshOnRequest: true
            },
            multiple: false,
            validation: {
                allowedExtensions: ["pdf"],
                sizeLimit: 5242880,
            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#setneg_form_file_ktp_galery').fineUploader('getUuid', id);
                        $('#setneg_form_file_ktp_uuid').val(uuid);
                        $('#setneg_form_file_ktp_name').val(xhr.uploadName);
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onSubmit: function(id, name) {
                    var uuid = $('#setneg_form_file_ktp_uuid').val();
                    $.get(BASE_URL + '/administrator/setneg_form/delete_file_ktp_file/' + uuid);
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#setneg_form_file_ktp_uuid').val('');
                        $('#setneg_form_file_ktp_name').val('');
                    }
                }
            }
        }); /*end file_ktp galey*/
        var params = {};
        params[csrf] = token;

        $('#setneg_form_file_jdwkeg_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/setneg_form/upload_file_jdwkeg_file',
                params: params
            },
            deleteFile: {
                enabled: true, // defaults to false
                endpoint: BASE_URL + '/administrator/setneg_form/delete_file_jdwkeg_file'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            session: {
                endpoint: BASE_URL + 'administrator/setneg_form/get_file_jdwkeg_file/<?= $setneg_form->id; ?>',
                refreshOnRequest: true
            },
            multiple: false,
            validation: {
                allowedExtensions: ["pdf"],
                sizeLimit: 5242880,
            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#setneg_form_file_jdwkeg_galery').fineUploader('getUuid', id);
                        $('#setneg_form_file_jdwkeg_uuid').val(uuid);
                        $('#setneg_form_file_jdwkeg_name').val(xhr.uploadName);
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onSubmit: function(id, name) {
                    var uuid = $('#setneg_form_file_jdwkeg_uuid').val();
                    $.get(BASE_URL + '/administrator/setneg_form/delete_file_jdwkeg_file/' + uuid);
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#setneg_form_file_jdwkeg_uuid').val('');
                        $('#setneg_form_file_jdwkeg_name').val('');
                    }
                }
            }
        }); /*end file_jdwkeg galey*/
        var params = {};
        params[csrf] = token;

        $('#setneg_form_file_relevansi_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/setneg_form/upload_file_relevansi_file',
                params: params
            },
            deleteFile: {
                enabled: true, // defaults to false
                endpoint: BASE_URL + '/administrator/setneg_form/delete_file_relevansi_file'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            session: {
                endpoint: BASE_URL + 'administrator/setneg_form/get_file_relevansi_file/<?= $setneg_form->id; ?>',
                refreshOnRequest: true
            },
            multiple: false,
            validation: {
                allowedExtensions: ["pdf"],
                sizeLimit: 5242880,
            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#setneg_form_file_relevansi_galery').fineUploader('getUuid', id);
                        $('#setneg_form_file_relevansi_uuid').val(uuid);
                        $('#setneg_form_file_relevansi_name').val(xhr.uploadName);
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onSubmit: function(id, name) {
                    var uuid = $('#setneg_form_file_relevansi_uuid').val();
                    $.get(BASE_URL + '/administrator/setneg_form/delete_file_relevansi_file/' + uuid);
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#setneg_form_file_relevansi_uuid').val('');
                        $('#setneg_form_file_relevansi_name').val('');
                    }
                }
            }
        }); /*end file_relevansi galey*/
        var params = {};
        params[csrf] = token;

        $('#setneg_form_file_kak_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/setneg_form/upload_file_kak_file',
                params: params
            },
            deleteFile: {
                enabled: true, // defaults to false
                endpoint: BASE_URL + '/administrator/setneg_form/delete_file_kak_file'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            session: {
                endpoint: BASE_URL + 'administrator/setneg_form/get_file_kak_file/<?= $setneg_form->id; ?>',
                refreshOnRequest: true
            },
            multiple: false,
            validation: {
                allowedExtensions: ["pdf"],
                sizeLimit: 5242880,
            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#setneg_form_file_kak_galery').fineUploader('getUuid', id);
                        $('#setneg_form_file_kak_uuid').val(uuid);
                        $('#setneg_form_file_kak_name').val(xhr.uploadName);
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onSubmit: function(id, name) {
                    var uuid = $('#setneg_form_file_kak_uuid').val();
                    $.get(BASE_URL + '/administrator/setneg_form/delete_file_kak_file/' + uuid);
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#setneg_form_file_kak_uuid').val('');
                        $('#setneg_form_file_kak_name').val('');
                    }
                }
            }
        }); /*end file_kak galey*/
        var params = {};
        params[csrf] = token;

        $('#setneg_form_file_ijin_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/setneg_form/upload_file_ijin_file',
                params: params
            },
            deleteFile: {
                enabled: true, // defaults to false
                endpoint: BASE_URL + '/administrator/setneg_form/delete_file_ijin_file'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            session: {
                endpoint: BASE_URL + 'administrator/setneg_form/get_file_ijin_file/<?= $setneg_form->id; ?>',
                refreshOnRequest: true
            },
            multiple: false,
            validation: {
                allowedExtensions: ["pdf"],
                sizeLimit: 5242880,
            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#setneg_form_file_ijin_galery').fineUploader('getUuid', id);
                        $('#setneg_form_file_ijin_uuid').val(uuid);
                        $('#setneg_form_file_ijin_name').val(xhr.uploadName);
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onSubmit: function(id, name) {
                    var uuid = $('#setneg_form_file_ijin_uuid').val();
                    $.get(BASE_URL + '/administrator/setneg_form/delete_file_ijin_file/' + uuid);
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#setneg_form_file_ijin_uuid').val('');
                        $('#setneg_form_file_ijin_name').val('');
                    }
                }
            }
        }); /*end file_ijin galey*/
        var params = {};
        params[csrf] = token;

        $('#setneg_form_file_sk_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/setneg_form/upload_file_sk_file',
                params: params
            },
            deleteFile: {
                enabled: true, // defaults to false
                endpoint: BASE_URL + '/administrator/setneg_form/delete_file_sk_file'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            session: {
                endpoint: BASE_URL + 'administrator/setneg_form/get_file_sk_file/<?= $setneg_form->id; ?>',
                refreshOnRequest: true
            },
            multiple: false,
            validation: {
                allowedExtensions: ["pdf"],
                sizeLimit: 5242880,
            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#setneg_form_file_sk_galery').fineUploader('getUuid', id);
                        $('#setneg_form_file_sk_uuid').val(uuid);
                        $('#setneg_form_file_sk_name').val(xhr.uploadName);
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onSubmit: function(id, name) {
                    var uuid = $('#setneg_form_file_sk_uuid').val();
                    $.get(BASE_URL + '/administrator/setneg_form/delete_file_sk_file/' + uuid);
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#setneg_form_file_sk_uuid').val('');
                        $('#setneg_form_file_sk_name').val('');
                    }
                }
            }
        }); /*end file_sk galey*/
        async function chain() {}
        chain();
    }); /*end doc ready*/
    function cari_pegawai() {
        BootstrapDialog.show({
            title: 'Cari data Simpeg',
            draggable: true,
            message: function(dialog) {
                var $message = $('<div></div>');
                $.get('<?= site_url('administrator/setneg_form/form_cari_pegawai') ?>', {}, function(response) {
                    $message.html(response);
                })
                return $message;
            },
            buttons: [{
                label: 'Ambil',
                icon: 'glyphicon glyphicon-plus',
                cssClass: 'btn-primary',
                action: function(dialog) {
                    $('#identitas').val($('#pegawai option:selected').data('nip'));
                    $('#nama').val($('#pegawai option:selected').data('nama'));
                    $('#tmp_lahir').val($('#pegawai option:selected').data('lahir_tmp'));
                    $('#tgl_lahir').val($('#pegawai option:selected').data('lahir_tgl'));
                    dialog.close();
                }
            }, {
                label: 'Tutup',
                action: function(dialog) {
                    dialog.close();
                }
            }],
        });
    }
</script>