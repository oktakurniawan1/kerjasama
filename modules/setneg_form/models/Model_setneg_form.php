<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_setneg_form extends MY_Model {

    private $primary_key    = 'id';
    private $table_name     = 'setneg_form';
    public $field_search   = ['identitas', 'nama', 'negara_tujuan', 'mob_setneg', 'lama_tinggal', 'validasi', 'status_pengajuan'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "setneg_form.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "setneg_form.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "setneg_form.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "setneg_form.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "setneg_form.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "setneg_form.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
        
        $this->sortable();
        
        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('keg_setneg', 'keg_setneg.id = setneg_form.mob_setneg', 'LEFT');
        $this->db->select('setneg_form.*,keg_setneg.keg_setneg as keg_setneg_keg_setneg');
        return $this;
    }

    public function filter_avaiable() {

        if (is_groups_in(['Dosen','Tendik'])) {
            if (is_non_user()) $nip = fromsess('nip_baru');
            else $nip = get_user_data('username');
            $this->db->where_in('identitas', $nip);
        }

        return $this;
    }
    public function get_karyawan()
    {
        return $this->db->select('nip_baru,gelar_depan,nama,gelar_belakang,dos_kar,nama_unit,telp,email,lahir_tmp,lahir_tgl')->get('karyawan')->result();
    }

}

/* End of file Model_setneg_form.php */
/* Location: ./application/models/Model_setneg_form.php */