<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Mhs Mobility Controller
 *| --------------------------------------------------------------------------
 *| Mhs Mobility site
 *|
 */
class Mhs_mobility extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_mhs_mobility');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * show all Mhs Mobilitys
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('mhs_mobility_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('mhs_mobility_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('mhs_mobility_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('mhs_mobility_filter');
			$this->session->unset_userdata('mhs_mobility_field');
		}
		$filter = $this->session->userdata('mhs_mobility_filter');
		$field = $this->session->userdata('mhs_mobility_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['mhs_mobilitys'] = $this->model_mhs_mobility->get($filter, $field, $this->limit_page, $offset);
		$this->data['mhs_mobility_counts'] = $this->model_mhs_mobility->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/mhs_mobility/index/',
			'total_rows'   => $this->model_mhs_mobility->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Mhs Mobility List');
		$this->render('backend/standart/administrator/mhs_mobility/mhs_mobility_list', $this->data);
	}

	/**
	 * Add new mhs_mobilitys
	 *
	 */
	public function add()
	{
		$this->is_allowed('mhs_mobility_add');

		$this->template->title('Mhs Mobility New');
		$filter_prodi = get_filter_prodi();
		$this->data['filter_prodi'] = $filter_prodi;
		$this->render('backend/standart/administrator/mhs_mobility/mhs_mobility_add', $this->data);
	}

	/**
	 * Add New Mhs Mobilitys
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('mhs_mobility_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('scope', 'Scope', 'trim|required');
		$this->form_validation->set_rules('nim', 'Nim', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kd_prodi', 'Program Studi', 'trim|required|max_length[8]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('judul_kegiatan', 'Judul Kegiatan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('no_tlp', 'Nomor Telepon', 'trim|required|max_length[15]');
		$this->form_validation->set_rules('mhs_mobility_file_ktm_name', 'File KTM', 'trim|required');

		if ($this->input->post('scope') == 'LN') {
			$this->form_validation->set_rules('mhs_mobility_file_nik_name', 'File NIK', 'trim|required');
			$this->form_validation->set_rules('mhs_mobility_file_loa_name', 'File LoA', 'trim|required');
			$this->form_validation->set_rules('mhs_mobility_file_rab_name', 'File RAB', 'trim|required');
			$this->form_validation->set_rules('mhs_mobility_file_drh_name', 'File DRH', 'trim|required');
			$this->form_validation->set_rules('mhs_mobility_file_aktifkul_name', 'File Aktif Kuliah', 'trim|required');
			$this->form_validation->set_rules('mhs_mobility_file_suket_biaya_name', 'File Keterangan Biaya', 'trim|required');
			$this->form_validation->set_rules('mhs_mobility_file_ijin_name', 'File Perijinan', 'trim|required');
			$this->form_validation->set_rules('tgl_file_ijin', 'Tanggal Permohonan', 'trim|required');
			$this->form_validation->set_rules('negara_tujuan', 'Negara_tujuan', 'trim|required|max_length[255]');
			$this->form_validation->set_rules('mhs_mobility_file_jdwkeg_name', 'File Jadwal Kegiatan', 'trim|required');
			$this->form_validation->set_rules('mhs_mobility_file_kontrak_kuliah_name', 'File Kontrak', 'trim|required');
		}

		$this->form_validation->set_rules('bound_id', 'Jenis Kegiatan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('tujuan_institusi', 'Tujuan Institusi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('lokasi', 'Lokasi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'trim|required');
		$this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'trim|required');
		$this->form_validation->set_rules('validasi_mhs', 'Validasi Mahasiswa', 'trim|required');


		if ($this->form_validation->run()) {
			$mhs_mobility_file_nik_uuid = $this->input->post('mhs_mobility_file_nik_uuid');
			$mhs_mobility_file_nik_name = $this->input->post('mhs_mobility_file_nik_name');
			$mhs_mobility_file_loa_uuid = $this->input->post('mhs_mobility_file_loa_uuid');
			$mhs_mobility_file_loa_name = $this->input->post('mhs_mobility_file_loa_name');
			$mhs_mobility_file_ktm_uuid = $this->input->post('mhs_mobility_file_ktm_uuid');
			$mhs_mobility_file_ktm_name = $this->input->post('mhs_mobility_file_ktm_name');
			$mhs_mobility_file_rab_uuid = $this->input->post('mhs_mobility_file_rab_uuid');
			$mhs_mobility_file_rab_name = $this->input->post('mhs_mobility_file_rab_name');
			$mhs_mobility_file_drh_uuid = $this->input->post('mhs_mobility_file_drh_uuid');
			$mhs_mobility_file_drh_name = $this->input->post('mhs_mobility_file_drh_name');
			$mhs_mobility_file_jdwkeg_uuid = $this->input->post('mhs_mobility_file_jdwkeg_uuid');
			$mhs_mobility_file_jdwkeg_name = $this->input->post('mhs_mobility_file_jdwkeg_name');
			$mhs_mobility_file_kontrak_kuliah_uuid = $this->input->post('mhs_mobility_file_kontrak_kuliah_uuid');
			$mhs_mobility_file_kontrak_kuliah_name = $this->input->post('mhs_mobility_file_kontrak_kuliah_name');
			$mhs_mobility_file_aktifkul_uuid = $this->input->post('mhs_mobility_file_aktifkul_uuid');
			$mhs_mobility_file_aktifkul_name = $this->input->post('mhs_mobility_file_aktifkul_name');
			$mhs_mobility_file_suket_biaya_uuid = $this->input->post('mhs_mobility_file_suket_biaya_uuid');
			$mhs_mobility_file_suket_biaya_name = $this->input->post('mhs_mobility_file_suket_biaya_name');
			$mhs_mobility_file_ijin_uuid = $this->input->post('mhs_mobility_file_ijin_uuid');
			$mhs_mobility_file_ijin_name = $this->input->post('mhs_mobility_file_ijin_name');

			$mhs_mobility_file_bukti_kegiatan_uuid = $this->input->post('mhs_mobility_file_bukti_kegiatan_uuid');
			$mhs_mobility_file_bukti_kegiatan_name = $this->input->post('mhs_mobility_file_bukti_kegiatan_name');

			$save_data = [
				'scope' => $this->input->post('scope'),
				'nim' => $this->input->post('nim'),
				'nama' => $this->input->post('nama'),
				'kd_prodi' => $this->input->post('kd_prodi'),
				'email' => $this->input->post('email'),
				'no_tlp' => $this->input->post('no_tlp'),
				'bound_id' => $this->input->post('bound_id'),
				'tujuan_institusi' => $this->input->post('tujuan_institusi'),
				'lokasi' => $this->input->post('lokasi'),
				'negara_tujuan' => $this->input->post('negara_tujuan'),
				'tgl_mulai' => $this->input->post('tgl_mulai'),
				'tgl_selesai' => $this->input->post('tgl_selesai'),
				'tgl_file_ijin' => $this->input->post('tgl_file_ijin'),
				'tgl_input' => date('Y-m-d'),
				'catatan' => $this->input->post('catatan'),
				'validasi_mhs' => $this->input->post('validasi_mhs'),
				'mou_ks' => $this->input->post('mou_ks'),
				'judul_kegiatan' => $this->input->post('judul_kegiatan'),
			];

			if (!is_dir(FCPATH . '/uploads/mhs_mobility/')) {
				mkdir(FCPATH . '/uploads/mhs_mobility/');
			}

			if (!empty($mhs_mobility_file_bukti_kegiatan_uuid)) {
				$mhs_mobility_file_bukti_kegiatan_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_bukti_kegiatan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_bukti_kegiatan_uuid . '/' . $mhs_mobility_file_bukti_kegiatan_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_bukti_kegiatan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_bukti_kegiatan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_bukti_kegiatan'] = $mhs_mobility_file_bukti_kegiatan_name_copy;
			}

			if (!empty($mhs_mobility_file_nik_name)) {
				$mhs_mobility_file_nik_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_nik_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_nik_uuid . '/' . $mhs_mobility_file_nik_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_nik_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_nik_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_nik'] = $mhs_mobility_file_nik_name_copy;
			}

			if (!empty($mhs_mobility_file_loa_name)) {
				$mhs_mobility_file_loa_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_loa_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_loa_uuid . '/' . $mhs_mobility_file_loa_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_loa_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_loa'] = $mhs_mobility_file_loa_name_copy;
			}

			if (!empty($mhs_mobility_file_ktm_name)) {
				$mhs_mobility_file_ktm_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_ktm_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_ktm_uuid . '/' . $mhs_mobility_file_ktm_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_ktm_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_ktm_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ktm'] = $mhs_mobility_file_ktm_name_copy;
			}

			if (!empty($mhs_mobility_file_rab_name)) {
				$mhs_mobility_file_rab_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_rab_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_rab_uuid . '/' . $mhs_mobility_file_rab_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_rab_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_rab_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_rab'] = $mhs_mobility_file_rab_name_copy;
			}

			if (!empty($mhs_mobility_file_drh_name)) {
				$mhs_mobility_file_drh_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_drh_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_drh_uuid . '/' . $mhs_mobility_file_drh_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_drh_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_drh_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_drh'] = $mhs_mobility_file_drh_name_copy;
			}

			if (!empty($mhs_mobility_file_jdwkeg_name)) {
				$mhs_mobility_file_jdwkeg_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_jdwkeg_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_jdwkeg_uuid . '/' . $mhs_mobility_file_jdwkeg_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_jdwkeg_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_jdwkeg_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_jdwkeg'] = $mhs_mobility_file_jdwkeg_name_copy;
			}

			if (!empty($mhs_mobility_file_kontrak_kuliah_name)) {
				$mhs_mobility_file_kontrak_kuliah_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_kontrak_kuliah_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_kontrak_kuliah_uuid . '/' . $mhs_mobility_file_kontrak_kuliah_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_kontrak_kuliah_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_kontrak_kuliah_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}
				$save_data['file_kontrak_kuliah'] = $mhs_mobility_file_kontrak_kuliah_name_copy;
			}

			if (!empty($mhs_mobility_file_aktifkul_name)) {
				$mhs_mobility_file_aktifkul_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_aktifkul_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_aktifkul_uuid . '/' . $mhs_mobility_file_aktifkul_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_aktifkul_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_aktifkul_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_aktifkul'] = $mhs_mobility_file_aktifkul_name_copy;
			}

			if (!empty($mhs_mobility_file_suket_biaya_name)) {
				$mhs_mobility_file_suket_biaya_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_suket_biaya_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_suket_biaya_uuid . '/' . $mhs_mobility_file_suket_biaya_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_suket_biaya_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_suket_biaya_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_suket_biaya'] = $mhs_mobility_file_suket_biaya_name_copy;
			}

			if (!empty($mhs_mobility_file_ijin_name)) {
				$mhs_mobility_file_ijin_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_ijin_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_ijin_uuid . '/' . $mhs_mobility_file_ijin_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_ijin_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_ijin_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ijin'] = $mhs_mobility_file_ijin_name_copy;
			}

			$save_mhs_mobility = $this->model_mhs_mobility->store($save_data);


			if ($save_mhs_mobility) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_mhs_mobility;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/mhs_mobility/edit/' . $save_mhs_mobility, 'Edit Mhs Mobility'),
						anchor('administrator/mhs_mobility', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/mhs_mobility/edit/' . $save_mhs_mobility, 'Edit Mhs Mobility')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/mhs_mobility');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/mhs_mobility');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Mhs Mobilitys
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('mhs_mobility_update');

		$this->data['mhs_mobility'] = $this->model_mhs_mobility->find($id);
		if (!is_groups_in(['Admin', 'Kerjasama'])) {
			if ($this->data['mhs_mobility']->validasi_wd == 'Y') {
				set_message(cclang('Anda tidak bisa mengubah, karena data sudah divalidasi', 'mhs_mobility'), 'warning');
				redirect_back();
			}
		}

		$list_mou = db_get_all_data('mou_kerjasama', "scope='{$this->data['mhs_mobility']->scope}' AND tgl_selesai > curdate() ");
		$cur_mou = db_get_all_data('mou_kerjasama', "id = '{$this->data['mhs_mobility']->mou_ks}'", 'row');
		if (!is_null($cur_mou)) {
			$tgl = DateTime::createFromFormat('Y-m-d', $cur_mou->tgl_selesai);
			$now = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
			if ($tgl < $now) $list_mou[] = $cur_mou;
		}

		$list_mou[] = (object)['id' => '1', 'nama_mitra' => 'Pengajuan Baru'];
		$this->data['list_mou'] = $list_mou;
		$filter_prodi = get_filter_prodi();
		$this->data['filter_prodi'] = $filter_prodi;
		$this->template->title('Mhs Mobility Update');
		$this->render('backend/standart/administrator/mhs_mobility/mhs_mobility_update', $this->data);
	}

	/**
	 * Update Mhs Mobilitys
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('mhs_mobility_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('scope', 'Scope', 'trim|required');
		$this->form_validation->set_rules('nim', 'Nim', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kd_prodi', 'Program Studi', 'trim|required|max_length[8]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('judul_kegiatan', 'Judul Kegiatan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('no_tlp', 'Nomor Telepon', 'trim|required|max_length[15]');
		$this->form_validation->set_rules('mhs_mobility_file_ktm_name', 'File KTM', 'trim|required');

		if ($this->input->post('scope') == 'LN') {
			$this->form_validation->set_rules('mhs_mobility_file_nik_name', 'File NIK', 'trim|required');
			$this->form_validation->set_rules('mhs_mobility_file_loa_name', 'File LoA', 'trim|required');
			$this->form_validation->set_rules('mhs_mobility_file_rab_name', 'File RAB', 'trim|required');
			$this->form_validation->set_rules('mhs_mobility_file_drh_name', 'File DRH', 'trim|required');
			$this->form_validation->set_rules('mhs_mobility_file_aktifkul_name', 'File Aktif Kuliah', 'trim|required');
			$this->form_validation->set_rules('mhs_mobility_file_suket_biaya_name', 'File Keterangan Biaya', 'trim|required');
			$this->form_validation->set_rules('mhs_mobility_file_ijin_name', 'File Perijinan', 'trim|required');
			$this->form_validation->set_rules('tgl_file_ijin', 'Tanggal Permohonan', 'trim|required');
			$this->form_validation->set_rules('negara_tujuan', 'Negara_tujuan', 'trim|required|max_length[255]');
			$this->form_validation->set_rules('mhs_mobility_file_kontrak_kuliah_name', 'File Kontrak', 'trim|required');
			$this->form_validation->set_rules('mhs_mobility_file_jdwkeg_name', 'File Jadwal Kegiatan', 'trim|required');
		}

		$this->form_validation->set_rules('bound_id', 'Jenis Kegiatan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('tujuan_institusi', 'Tujuan Institusi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('lokasi', 'Lokasi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'trim|required');
		$this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'trim|required');
		$this->form_validation->set_rules('validasi_mhs', 'Validasi Mahasiswa', 'trim|required');

		if ($this->form_validation->run()) {
			$mhs_mobility_file_nik_uuid = $this->input->post('mhs_mobility_file_nik_uuid');
			$mhs_mobility_file_nik_name = $this->input->post('mhs_mobility_file_nik_name');
			$mhs_mobility_file_loa_uuid = $this->input->post('mhs_mobility_file_loa_uuid');
			$mhs_mobility_file_loa_name = $this->input->post('mhs_mobility_file_loa_name');
			$mhs_mobility_file_ktm_uuid = $this->input->post('mhs_mobility_file_ktm_uuid');
			$mhs_mobility_file_ktm_name = $this->input->post('mhs_mobility_file_ktm_name');
			$mhs_mobility_file_rab_uuid = $this->input->post('mhs_mobility_file_rab_uuid');
			$mhs_mobility_file_rab_name = $this->input->post('mhs_mobility_file_rab_name');
			$mhs_mobility_file_drh_uuid = $this->input->post('mhs_mobility_file_drh_uuid');
			$mhs_mobility_file_drh_name = $this->input->post('mhs_mobility_file_drh_name');
			$mhs_mobility_file_jdwkeg_uuid = $this->input->post('mhs_mobility_file_jdwkeg_uuid');
			$mhs_mobility_file_jdwkeg_name = $this->input->post('mhs_mobility_file_jdwkeg_name');
			$mhs_mobility_file_kontrak_kuliah_uuid = $this->input->post('mhs_mobility_file_kontrak_kuliah_uuid');
			$mhs_mobility_file_kontrak_kuliah_name = $this->input->post('mhs_mobility_file_kontrak_kuliah_name');
			$mhs_mobility_file_aktifkul_uuid = $this->input->post('mhs_mobility_file_aktifkul_uuid');
			$mhs_mobility_file_aktifkul_name = $this->input->post('mhs_mobility_file_aktifkul_name');
			$mhs_mobility_file_suket_biaya_uuid = $this->input->post('mhs_mobility_file_suket_biaya_uuid');
			$mhs_mobility_file_suket_biaya_name = $this->input->post('mhs_mobility_file_suket_biaya_name');
			$mhs_mobility_file_ijin_uuid = $this->input->post('mhs_mobility_file_ijin_uuid');
			$mhs_mobility_file_ijin_name = $this->input->post('mhs_mobility_file_ijin_name');

			$mhs_mobility_file_bukti_kegiatan_uuid = $this->input->post('mhs_mobility_file_bukti_kegiatan_uuid');
			$mhs_mobility_file_bukti_kegiatan_name = $this->input->post('mhs_mobility_file_bukti_kegiatan_name');

			$save_data = [
				'scope' => $this->input->post('scope'),
				'nim' => $this->input->post('nim'),
				'nama' => $this->input->post('nama'),
				'kd_prodi' => $this->input->post('kd_prodi'),
				'email' => $this->input->post('email'),
				'no_tlp' => $this->input->post('no_tlp'),
				'bound_id' => $this->input->post('bound_id'),
				'tujuan_institusi' => $this->input->post('tujuan_institusi'),
				'lokasi' => $this->input->post('lokasi'),
				'negara_tujuan' => $this->input->post('negara_tujuan'),
				'tgl_mulai' => $this->input->post('tgl_mulai'),
				'tgl_selesai' => $this->input->post('tgl_selesai'),
				'tgl_file_ijin' => $this->input->post('tgl_file_ijin'),
				'catatan' => $this->input->post('catatan'),
				'validasi_mhs' => $this->input->post('validasi_mhs'),
				'validasi_wd' => $this->input->post('validasi_wd'),
				'mou_ks' => $this->input->post('mou_ks'),
				'judul_kegiatan' => $this->input->post('judul_kegiatan'),
			];

			if (!is_dir(FCPATH . '/uploads/mhs_mobility/')) {
				mkdir(FCPATH . '/uploads/mhs_mobility/');
			}

			if (!empty($mhs_mobility_file_bukti_kegiatan_uuid)) {
				$mhs_mobility_file_bukti_kegiatan_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_bukti_kegiatan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_bukti_kegiatan_uuid . '/' . $mhs_mobility_file_bukti_kegiatan_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_bukti_kegiatan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_bukti_kegiatan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_bukti_kegiatan'] = $mhs_mobility_file_bukti_kegiatan_name_copy;
			}

			if (!empty($mhs_mobility_file_nik_uuid)) {
				$mhs_mobility_file_nik_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_nik_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_nik_uuid . '/' . $mhs_mobility_file_nik_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_nik_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_nik_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_nik'] = $mhs_mobility_file_nik_name_copy;
			}

			if (!empty($mhs_mobility_file_loa_uuid)) {
				$mhs_mobility_file_loa_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_loa_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_loa_uuid . '/' . $mhs_mobility_file_loa_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_loa_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_loa'] = $mhs_mobility_file_loa_name_copy;
			}

			if (!empty($mhs_mobility_file_ktm_uuid)) {
				$mhs_mobility_file_ktm_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_ktm_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_ktm_uuid . '/' . $mhs_mobility_file_ktm_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_ktm_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_ktm_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ktm'] = $mhs_mobility_file_ktm_name_copy;
			}

			if (!empty($mhs_mobility_file_rab_uuid)) {
				$mhs_mobility_file_rab_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_rab_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_rab_uuid . '/' . $mhs_mobility_file_rab_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_rab_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_rab_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_rab'] = $mhs_mobility_file_rab_name_copy;
			}

			if (!empty($mhs_mobility_file_drh_uuid)) {
				$mhs_mobility_file_drh_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_drh_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_drh_uuid . '/' . $mhs_mobility_file_drh_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_drh_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_drh_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_drh'] = $mhs_mobility_file_drh_name_copy;
			}

			if (!empty($mhs_mobility_file_jdwkeg_uuid)) {
				$mhs_mobility_file_jdwkeg_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_jdwkeg_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_jdwkeg_uuid . '/' . $mhs_mobility_file_jdwkeg_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_jdwkeg_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_jdwkeg_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_jdwkeg'] = $mhs_mobility_file_jdwkeg_name_copy;
			}

			if (!empty($mhs_mobility_file_kontrak_kuliah_uuid)) {
				$mhs_mobility_file_kontrak_kuliah_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_kontrak_kuliah_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_kontrak_kuliah_uuid . '/' . $mhs_mobility_file_kontrak_kuliah_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_kontrak_kuliah_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_kontrak_kuliah_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_kontrak_kuliah'] = $mhs_mobility_file_kontrak_kuliah_name_copy;
			}

			if (!empty($mhs_mobility_file_aktifkul_uuid)) {
				$mhs_mobility_file_aktifkul_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_aktifkul_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_aktifkul_uuid . '/' . $mhs_mobility_file_aktifkul_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_aktifkul_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_aktifkul_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_aktifkul'] = $mhs_mobility_file_aktifkul_name_copy;
			}

			if (!empty($mhs_mobility_file_suket_biaya_uuid)) {
				$mhs_mobility_file_suket_biaya_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_suket_biaya_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_suket_biaya_uuid . '/' . $mhs_mobility_file_suket_biaya_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_suket_biaya_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_suket_biaya_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_suket_biaya'] = $mhs_mobility_file_suket_biaya_name_copy;
			}

			if (!empty($mhs_mobility_file_ijin_uuid)) {
				$mhs_mobility_file_ijin_name_copy = date('YmdHis') . '-' . $mhs_mobility_file_ijin_name;

				rename(
					FCPATH . 'uploads/tmp/' . $mhs_mobility_file_ijin_uuid . '/' . $mhs_mobility_file_ijin_name,
					FCPATH . 'uploads/mhs_mobility/' . $mhs_mobility_file_ijin_name_copy
				);

				if (!is_file(FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility_file_ijin_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ijin'] = $mhs_mobility_file_ijin_name_copy;
			}

			$save_mhs_mobility = $this->model_mhs_mobility->change($id, $save_data);

			if ($save_mhs_mobility) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/mhs_mobility', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/mhs_mobility/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/mhs_mobility/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Mhs Mobilitys
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('mhs_mobility_delete');

		/**
		 * Pengecualian khusus untuk member/group mahasiswa hanya untuk user mhs ,akan di cek aaauth is_member
		 * oleh kerena itu user group lain jangan dienroll group mahasiswa, akan di tolak dibagian ini.
		 * cek function di helper menggunakan lib aauth method : is_member()
		 */
		if (is_member('Mahasiswa')) {
			$mhsmob	= $this->model_mhs_mobility->find($id);
			if ($mhsmob->nim == fromsess('identitas')) {
				if ($mhsmob->validasi_wd == 'Y') {
					set_message(cclang('Gagal hapus, karena data sudah divalidasi', 'mhs_mobility'), 'warning');
					redirect_back();
				} else {
					if ($mhsmob->validasi_mhs == 'Y') {
						set_message(cclang('Gagal hapus, karena data sudah anda divalidasi', 'mhs_mobility'), 'warning');
						redirect_back();
					}
				}
			} else { // jika nim di mhsmob tidak sama dengan usernamenya maka itu milik orang lain => ditolak
				set_message(cclang('sorry_you_do_not_have_permission_to_access', 'mhs_mobility'), 'error');
				redirect_back();
			}
		}

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'mhs_mobility'), 'success');
		} else {
			set_message(cclang('error_delete', 'mhs_mobility'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Mhs Mobilitys
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('mhs_mobility_view');

		$this->data['mhs_mobility'] = $this->model_mhs_mobility->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Mhs Mobility Detail');
		$this->render('backend/standart/administrator/mhs_mobility/mhs_mobility_view', $this->data);
	}

	/**
	 * delete Mhs Mobilitys
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$mhs_mobility = $this->model_mhs_mobility->find($id);

		if (!empty($mhs_mobility->file_bukti_kegiatan)) {
			$path = FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility->file_bukti_kegiatan;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}

		if (!empty($mhs_mobility->file_nik)) {
			$path = FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility->file_nik;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($mhs_mobility->file_loa)) {
			$path = FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility->file_loa;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($mhs_mobility->file_ktm)) {
			$path = FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility->file_ktm;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($mhs_mobility->file_rab)) {
			$path = FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility->file_rab;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($mhs_mobility->file_drh)) {
			$path = FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility->file_drh;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($mhs_mobility->file_jdwkeg)) {
			$path = FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility->file_jdwkeg;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($mhs_mobility->file_kontrak_kuliah)) {
			$path = FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility->file_kontrak_kuliah;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($mhs_mobility->file_aktifkul)) {
			$path = FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility->file_aktifkul;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($mhs_mobility->file_suket_biaya)) {
			$path = FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility->file_suket_biaya;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($mhs_mobility->file_ijin)) {
			$path = FCPATH . '/uploads/mhs_mobility/' . $mhs_mobility->file_ijin;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}

		return $this->model_mhs_mobility->remove($id);
	}

	/**
	 * Upload Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function upload_file_bukti_kegiatan_file()
	{
		if (!$this->is_allowed('mhs_mobility_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mhs_mobility',
			'allowed_types' => 'pdf|jpg|jpeg',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function delete_file_bukti_kegiatan_file($uuid)
	{
		if (!$this->is_allowed('mhs_mobility_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_bukti_kegiatan',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/'
		]);
	}
	/**
	 * Get Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function get_file_bukti_kegiatan_file($id)
	{
		if (!$this->is_allowed('mhs_mobility_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mhs_mobility = $this->model_mhs_mobility->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_bukti_kegiatan',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/',
			'delete_endpoint'   => 'administrator/mhs_mobility/delete_file_bukti_kegiatan_file'
		]);
	}

	/**
	 * Upload Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function upload_file_nik_file()
	{
		if (!$this->is_allowed('mhs_mobility_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mhs_mobility',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function delete_file_nik_file($uuid)
	{
		if (!$this->is_allowed('mhs_mobility_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_nik',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/'
		]);
	}

	/**
	 * Get Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function get_file_nik_file($id)
	{
		if (!$this->is_allowed('mhs_mobility_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mhs_mobility = $this->model_mhs_mobility->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_nik',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/',
			'delete_endpoint'   => 'administrator/mhs_mobility/delete_file_nik_file'
		]);
	}

	/**
	 * Upload Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function upload_file_loa_file()
	{
		if (!$this->is_allowed('mhs_mobility_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mhs_mobility',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function delete_file_loa_file($uuid)
	{
		if (!$this->is_allowed('mhs_mobility_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_loa',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/'
		]);
	}

	/**
	 * Get Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function get_file_loa_file($id)
	{
		if (!$this->is_allowed('mhs_mobility_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mhs_mobility = $this->model_mhs_mobility->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_loa',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/',
			'delete_endpoint'   => 'administrator/mhs_mobility/delete_file_loa_file'
		]);
	}

	/**
	 * Upload Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function upload_file_ktm_file()
	{
		if (!$this->is_allowed('mhs_mobility_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mhs_mobility',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function delete_file_ktm_file($uuid)
	{
		if (!$this->is_allowed('mhs_mobility_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ktm',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/'
		]);
	}

	/**
	 * Get Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function get_file_ktm_file($id)
	{
		if (!$this->is_allowed('mhs_mobility_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mhs_mobility = $this->model_mhs_mobility->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ktm',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/',
			'delete_endpoint'   => 'administrator/mhs_mobility/delete_file_ktm_file'
		]);
	}

	/**
	 * Upload Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function upload_file_rab_file()
	{
		if (!$this->is_allowed('mhs_mobility_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mhs_mobility',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function delete_file_rab_file($uuid)
	{
		if (!$this->is_allowed('mhs_mobility_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_rab',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/'
		]);
	}

	/**
	 * Get Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function get_file_rab_file($id)
	{
		if (!$this->is_allowed('mhs_mobility_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mhs_mobility = $this->model_mhs_mobility->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_rab',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/',
			'delete_endpoint'   => 'administrator/mhs_mobility/delete_file_rab_file'
		]);
	}

	/**
	 * Upload Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function upload_file_drh_file()
	{
		if (!$this->is_allowed('mhs_mobility_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mhs_mobility',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function delete_file_drh_file($uuid)
	{
		if (!$this->is_allowed('mhs_mobility_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_drh',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/'
		]);
	}

	/**
	 * Get Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function get_file_drh_file($id)
	{
		if (!$this->is_allowed('mhs_mobility_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mhs_mobility = $this->model_mhs_mobility->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_drh',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/',
			'delete_endpoint'   => 'administrator/mhs_mobility/delete_file_drh_file'
		]);
	}

	/**
	 * Upload Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function upload_file_jdwkeg_file()
	{
		if (!$this->is_allowed('mhs_mobility_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mhs_mobility',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function delete_file_jdwkeg_file($uuid)
	{
		if (!$this->is_allowed('mhs_mobility_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_jdwkeg',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/'
		]);
	}
	/**
	 * Get Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function get_file_jdwkeg_file($id)
	{
		if (!$this->is_allowed('mhs_mobility_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mhs_mobility = $this->model_mhs_mobility->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_jdwkeg',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/',
			'delete_endpoint'   => 'administrator/mhs_mobility/delete_file_jdwkeg_file'
		]);
	}

	/**
	 * Upload Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function upload_file_kontrak_kuliah_file()
	{
		if (!$this->is_allowed('mhs_mobility_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mhs_mobility',
			'allowed_types' => 'pdf',
		]);
	}
	/**
	 * Delete Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function delete_file_kontrak_kuliah_file($uuid)
	{
		if (!$this->is_allowed('mhs_mobility_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_kontrak_kuliah',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/'
		]);
	}
	/**
	 * Get Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function get_file_kontrak_kuliah_file($id)
	{
		if (!$this->is_allowed('mhs_mobility_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mhs_mobility = $this->model_mhs_mobility->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_kontrak_kuliah',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/',
			'delete_endpoint'   => 'administrator/mhs_mobility/delete_file_kontrak_kuliah_file'
		]);
	}

	/**
	 * Upload Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function upload_file_aktifkul_file()
	{
		if (!$this->is_allowed('mhs_mobility_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mhs_mobility',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function delete_file_aktifkul_file($uuid)
	{
		if (!$this->is_allowed('mhs_mobility_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_aktifkul',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/'
		]);
	}

	/**
	 * Get Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function get_file_aktifkul_file($id)
	{
		if (!$this->is_allowed('mhs_mobility_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mhs_mobility = $this->model_mhs_mobility->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_aktifkul',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/',
			'delete_endpoint'   => 'administrator/mhs_mobility/delete_file_aktifkul_file'
		]);
	}

	/**
	 * Upload Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function upload_file_suket_biaya_file()
	{
		if (!$this->is_allowed('mhs_mobility_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mhs_mobility',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function delete_file_suket_biaya_file($uuid)
	{
		if (!$this->is_allowed('mhs_mobility_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_suket_biaya',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/'
		]);
	}

	/**
	 * Get Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function get_file_suket_biaya_file($id)
	{
		if (!$this->is_allowed('mhs_mobility_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mhs_mobility = $this->model_mhs_mobility->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_suket_biaya',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/',
			'delete_endpoint'   => 'administrator/mhs_mobility/delete_file_suket_biaya_file'
		]);
	}

	/**
	 * Upload Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function upload_file_ijin_file()
	{
		if (!$this->is_allowed('mhs_mobility_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'mhs_mobility',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function delete_file_ijin_file($uuid)
	{
		if (!$this->is_allowed('mhs_mobility_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ijin',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/'
		]);
	}

	/**
	 * Get Image Mhs Mobility	* 
	 * @return JSON
	 */
	public function get_file_ijin_file($id)
	{
		if (!$this->is_allowed('mhs_mobility_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$mhs_mobility = $this->model_mhs_mobility->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ijin',
			'table_name'        => 'mhs_mobility',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/mhs_mobility/',
			'delete_endpoint'   => 'administrator/mhs_mobility/delete_file_ijin_file'
		]);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('mhs_mobility_export');

		$this->model_mhs_mobility->export(
			'mhs_mobility',
			'mhs_mobility',
			$this->model_mhs_mobility->field_search
		);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('mhs_mobility_export');

		$this->model_mhs_mobility->pdf('mhs_mobility', 'mhs_mobility');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('mhs_mobility_export');

		$table = $title = 'mhs_mobility';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_mhs_mobility->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
	public function tampil_pdf($id)
	{
		$this->load->helper('string');
		$user = $this->model_mhs_mobility->find($id);

		$template	= $template = new \PhpOffice\PhpWord\TemplateProcessor(FCPATH . 'uploads/word/sample.docx');
		$template->setValue('nim', $user->nim);
		$template->setValue('nama', $user->nama);
		$strname	= random_string('alnum', 8);
		$file_doc   = FCPATH . 'uploads/word/' . $strname . '.docx';
		$template->saveAS($file_doc);
		$file_pdf	= FCPATH . 'uploads/word/' . $strname . '.pdf';
		$unoconv 	= \Unoconv\Unoconv::create();
		$unoconv->transcode($file_doc, 'pdf', $file_pdf);

		/*  Run Backgroud
            shell_exec("/usr/bin/unoconv -f pdf ".base_path('storage/files/'.$file_doc)." /dev/null 2>&1 &");
        */
		/*  This method to execute a php script within a php page without making the user wait 
            until it completes executing.
            shell_exec("php Server.php > /dev/null 2>/dev/null &")
        */

		//  wait until finish dont add ">/dev/null 2>&1 &"
		//  shell_exec("/usr/bin/unoconv -f pdf ".base_path('storage/files/'.$file_doc));
		ob_start();
		readfile($file_pdf);
		
		// $content = ob_get_contents();
		// ob_end_clean(); // Membersihkan buffer tanpa mengembalikan konten

		// ob_start();
		// // ... beberapa operasi lain di sini
		// flush(); // Mengirimkan data dalam buffer ke browser

		$content = ob_get_clean();

		@unlink($file_doc); //hapus doc
		@unlink($file_pdf); //hapus pdf

		header('Content-type: application/pdf');
		header('Content-Disposition: inline; filename=filename.pdf');
		echo $content;
	}
	public function getChangeOpsiNegara()
	{
		echo $this->load->view('backend/standart/administrator/mhs_mobility/_opsi_negara', [], TRUE);
	}
	public function form_cari_mahasiswa()
	{
		$ta = $this->input->get('ta');
		$data['mhs'] = $this->model_mhs_mobility->get_mahasiswa($ta);
		$this->load->view('_cari_mahasiswa', $data);
	}
}


/* End of file mhs_mobility.php */
/* Location: ./application/controllers/administrator/Mhs Mobility.php */
