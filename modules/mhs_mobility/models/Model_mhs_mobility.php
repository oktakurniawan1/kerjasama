<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_mhs_mobility extends MY_Model
{

	private $primary_key    = 'id';
	private $table_name     = 'mhs_mobility';
	public $field_search   = ['scope', 'nim', 'nama', 'kd_prodi', 'bound_id', 'tujuan_institusi', 'lokasi', 'tgl_mulai', 'tgl_selesai', 'tgl_input', 'validasi_mhs', 'catatan', 'judul_kegiatan'];

	public function __construct()
	{
		$config = array(
			'primary_key'   => $this->primary_key,
			'table_name'    => $this->table_name,
			'field_search'  => $this->field_search,
		);

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "mhs_mobility." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'kd_prodi') $where .= "OR " . "ref_prodi.nama_prodi LIKE '%" . $q . "%' ";
					if ($field == 'bound_id') $where .= "OR " . "si_ref_kegiatan.bound_name LIKE '%" . $q . "%' ";
					if ($field == 'mou_ks') $where .= "OR " . "mou_kerjasama.nama_mitra LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "mhs_mobility." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'kd_prodi') $where .= "OR " . "ref_prodi.nama_prodi LIKE '%" . $q . "%' ";
					if ($field == 'bound_id') $where .= "OR " . "si_ref_kegiatan.bound_name LIKE '%" . $q . "%' ";
					if ($field == 'mou_ks') $where .= "OR " . "mou_kerjasama.nama_mitra LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			$where .= "(" . "mhs_mobility." . $field . " LIKE '%" . $q . "%' )";
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "mhs_mobility." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'kd_prodi') $where .= "OR " . "ref_prodi.nama_prodi LIKE '%" . $q . "%' ";
					if ($field == 'bound_id') $where .= "OR " . "si_ref_kegiatan.bound_name LIKE '%" . $q . "%' ";
					if ($field == 'mou_ks') $where .= "OR " . "mou_kerjasama.nama_mitra LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "mhs_mobility." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'kd_prodi') $where .= "OR " . "ref_prodi.nama_prodi LIKE '%" . $q . "%' ";
					if ($field == 'bound_id') $where .= "OR " . "si_ref_kegiatan.bound_name LIKE '%" . $q . "%' ";
					if ($field == 'mou_ks') $where .= "OR " . "mou_kerjasama.nama_mitra LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			if ($field == 'kd_prodi') $where .= "(" . "ref_prodi.nama_prodi LIKE '%" . $q . "%') ";
			else if ($field == 'bound_id') $where .= "(" . "si_ref_kegiatan.bound_name LIKE '%" . $q . "%') ";
			else if ($field == 'mou_ks') $where .= "(" . "mou_kerjasama.nama_mitra LIKE '%" . $q . "%') ";
			else $where .= "(" . "mhs_mobility." . $field . " LIKE '%" . $q . "%' )";
		}

		if (is_array($select_field) and count($select_field)) {
			$this->db->select($select_field);
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);
		$this->db->limit($limit, $offset);

		$this->sortable();

		$query = $this->db->get($this->table_name);

		return $query->result();
	}

	public function join_avaiable()
	{
		$this->db->join('ref_prodi', 'ref_prodi.kode = mhs_mobility.kd_prodi', 'LEFT');
		$this->db->join('si_ref_kegiatan', 'si_ref_kegiatan.id = mhs_mobility.bound_id', 'LEFT');
		$this->db->join('mou_kerjasama', 'mou_kerjasama.id = mhs_mobility.mou_ks', 'LEFT');

		$this->db->select('mhs_mobility.*,ref_prodi.nama_prodi as ref_prodi_nama_prodi,si_ref_kegiatan.bound_name as si_ref_kegiatan_bound_name');

		$this->db->select('IF(DATEDIFF(mhs_mobility.tgl_selesai,CURDATE())>0,"aktif","selesai") as status');
		$this->db->select("@sisa:=PERIOD_DIFF(EXTRACT(YEAR_MONTH FROM mhs_mobility.tgl_selesai),EXTRACT(YEAR_MONTH FROM CURDATE())) as _sb,if(@sisa<0,0,@sisa) as sisa_bulan");

		$this->db->select('mou_kerjasama.nama_mitra as nama_mou_kerjasama');
		$this->db->select('mou_kerjasama.doc_file as doc_mou');

		return $this;
	}

	public function filter_avaiable()
	{

		// if (!$this->aauth->is_admin()) {
		// }

		/** jika yang login itu member group mahasiswa, filter yang tampiil hanya nimnya dia */
		if (is_member('Mahasiswa')) {
			if (is_non_user())
				$this->db->where('nim', $this->session->userdata('identitas'));    //inject session dengan menggunakan sso
			else
				$this->db->where('nim', get_user_data('username'));         //username isinya harus nim, => pake user lokal database
		}
		$prodi = [];
		if (is_member('Kaprodi')) {
			if (is_non_user()) $prodi[] = fromsess('kd_prodi');
			else {
				$prodi = get_user_data('prodi');
				$prodi = explode(',', $prodi);
			}
			$this->db->where_in('kd_prodi', $prodi);
		}
		if (is_member('Unit')) {
			if (is_non_user()) $prodi[] = fromsess('kd_prodi');
			else {
				$prodi = get_user_data('prodi');
				$prodi = explode(',', $prodi);
			}
			$this->db->where_in('kd_prodi', $prodi);
		}

		return $this;
	}
	public function get_mahasiswa($ta = null)
	{
		return $this->db->select('*')->get_where('mhs', ['ta' => $ta])->result();
	}
}

/* End of file Model_mhs_mobility.php */
/* Location: ./application/models/Model_mhs_mobility.php */