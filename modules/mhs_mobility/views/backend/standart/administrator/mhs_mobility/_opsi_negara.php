<div class="form-group ">
    <label for="negara_tujuan" class="col-sm-2 control-label">Negara tujuan
        <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <select class="form-control chosen chosen-select-deselect" name="negara_tujuan" id="negara_tujuan" data-placeholder="Select negara">
            <option value=""></option>
            <?php foreach (db_get_all_data('negara') as $row) : ?>
                <option value="<?= $row->nama_negara ?>"><?= $row->nama_negara; ?></option>
            <?php endforeach; ?>
        </select>
        <small class="info help-block">
            <b>Pilih negara</b></small>
    </div>
</div>
<script src="<?= BASE_ASSET; ?>/js/custom.js"></script>