<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Student Mobility <small><?= cclang('new', ['Student Mobility']); ?> </small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/mhs_mobility'); ?>">Mahasiswa Mobility</a></li>
		<li class="active"><?= cclang('new'); ?></li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Student Mobility</h3>
							<h5 class="widget-user-desc"><?= cclang('new', ['Student Mobility']); ?></h5>
							<hr>
						</div>
						<?= form_open('', [
							'name'    => 'form_mhs_mobility',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_mhs_mobility',
							'enctype' => 'multipart/form-data',
							'method'  => 'POST'
						]); ?>
						<div class="form-group ">
							<label for="scope" class="col-sm-2 control-label">DN/LN
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select" name="scope" id="scope" data-placeholder="Select Scope">
									<option value=""></option>
									<option value="DN">Dalam Negeri</option>
									<option value="LN" selected='true'>Luar Negeri</option>
								</select>
								<small class="info help-block">
									DN:Dalam negeri LN:Luar negeri
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nim" class="col-sm-2 control-label">Nim
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<?php if (is_member('Mahasiswa')) : ?>
									<input type="text" class="form-control" name="nim" id="nim" placeholder="Nim" value="<?= $this->session->userdata('identitas'); ?>" readonly>
								<?php else : ?>
									<input type="text" class="form-control" name="nim" id="nim" placeholder="Nim" value="<?= set_value('nim'); ?>">
									<div class='row' style="margin-top:10px">
										<div class="col-sm-2" style="padding-right: 5px;">
											<?= form_dropdown('ta', array_combine(array_reverse(range(2000, date('Y'))), array_reverse(range(2000, date('Y')))), date('Y'), ['class' => 'form-control input-sm']) ?>
										</div>
										<div class="col-sm-4" style="padding-left: 0px;">
											<button class="btn btn-primary btn-sm" type="button" onclick="cari_mahasiswa()">Cari Mahasiswa</button>
										</div>
									</div>
								<?php endif; ?>
								<small class="info help-block">
									<b>Input Nim</b> Max Length : 10, klik tombol cari mahasiswa untuk bantuan cari data mahasiswa dengan filter angkatan.</small>
							</div>
						</div>

						<div class="form-group">
							<label for="nama" class="col-sm-2 control-label">Nama
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<?php if (is_member('Mahasiswa')) : ?>
									<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?= $this->session->userdata('nama'); ?>" readonly>
								<?php else : ?>
									<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?= set_value('nama'); ?>">
								<?php endif; ?>
								<small class="info help-block">
									<b>Input Nama</b> Max Length : 255.</small>
							</div>
						</div>

						<?php if (is_member('Mahasiswa')) : ?>
							<input type="hidden" name='kd_prodi' value='<?= $this->session->userdata('kd_prodi') ?>'> <!-- diabmil dari session auth login-->
						<?php else : ?>
							<div class="form-group ">
								<label for="kd_prodi" class="col-sm-2 control-label">Program Studi
									<i class="required">*</i>
								</label>
								<div class="col-sm-8">
									<select class="form-control chosen chosen-select-deselect" name="kd_prodi" id="kd_prodi" data-placeholder="Select Kd Prodi">
										<option value=""></option>
										<?php foreach ($filter_prodi as $row) : ?>
											<option value="<?= $row->kode ?>"><?= $row->kode .' - '.$row->nama_prodi; ?></option>
										<?php endforeach; ?>
									</select>
									<small class="info help-block">
										<b>Input Kd Prodi</b> Max Length : 8.</small>
								</div>
							</div>
						<?php endif; ?>

						<div class="form-group ">
							<label for="email" class="col-sm-2 control-label">Email
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?= set_value('email'); ?>">
								<small class="info help-block">
									<b>Format Email must</b> Valid Email, <b>Input Email</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="no_tlp" class="col-sm-2 control-label">Nomer Telepon
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="no_tlp" id="no_tlp" placeholder="Nomer Telepon" value="<?= set_value('no_tlp'); ?>">
								<small class="info help-block">
									<b>Format No Tlp must</b> Valid Number, <b>Input No Tlp</b> Max Length : 15.</small>
							</div>
						</div>

						<div class="form-group">
							<label for="judul_kegiatan" class="col-sm-2 control-label">Judul Kegiatan
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="judul_kegiatan" id="judul_kegiatan" placeholder="Judul Kegiatan" value="<?= set_value('judul_kegiatan'); ?>">
								<small class="info help-block">
									<b>Input Judul Kegiatan</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_ktm" class="col-sm-2 control-label">File KTM
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="mhs_mobility_file_ktm_galery"></div>
								<input class="data_file" name="mhs_mobility_file_ktm_uuid" id="mhs_mobility_file_ktm_uuid" type="hidden" value="<?= set_value('mhs_mobility_file_ktm_uuid'); ?>">
								<input class="data_file" name="mhs_mobility_file_ktm_name" id="mhs_mobility_file_ktm_name" type="hidden" value="<?= set_value('mhs_mobility_file_ktm_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5 mb.</small>
							</div>
						</div>

						<div class="form-group scope-ln">
							<label for="" class="col-sm-2 control-label">File kelengkapan tujuan LN
								<i class="required">*</i>
							</label>
							<div class="col-sm-2">
								<div id="mhs_mobility_file_nik_galery"></div>
								<input class="data_file" name="mhs_mobility_file_nik_uuid" id="mhs_mobility_file_nik_uuid" type="hidden" value="<?= set_value('mhs_mobility_file_nik_uuid'); ?>">
								<input class="data_file" name="mhs_mobility_file_nik_name" id="mhs_mobility_file_nik_name" type="hidden" value="<?= set_value('mhs_mobility_file_nik_name'); ?>">
								<small class="info help-block"><b>*File NIK</b>
								</small>
							</div>
							<div class="col-sm-3">
								<div id="mhs_mobility_file_loa_galery"></div>
								<input class="data_file" name="mhs_mobility_file_loa_uuid" id="mhs_mobility_file_loa_uuid" type="hidden" value="<?= set_value('mhs_mobility_file_loa_uuid'); ?>">
								<input class="data_file" name="mhs_mobility_file_loa_name" id="mhs_mobility_file_loa_name" type="hidden" value="<?= set_value('mhs_mobility_file_loa_name'); ?>">
								<small class="info help-block"><b>*File LoA</b>
								</small>
							</div>

							<div class="col-sm-3">
								<div id="mhs_mobility_file_rab_galery"></div>
								<input class="data_file" name="mhs_mobility_file_rab_uuid" id="mhs_mobility_file_rab_uuid" type="hidden" value="<?= set_value('mhs_mobility_file_rab_uuid'); ?>">
								<input class="data_file" name="mhs_mobility_file_rab_name" id="mhs_mobility_file_rab_name" type="hidden" value="<?= set_value('mhs_mobility_file_rab_name'); ?>">
								<small class="info help-block">
									<b>*File RAB</b>, Extension file must PDF, <b>Max size file</b> 5 mb.</small>
							</div>
						</div>
						<div class="form-group scope-ln">
							<label for="" class="col-sm-2 control-label"></label>
							<div class="col-sm-2">
								<div id="mhs_mobility_file_drh_galery"></div>
								<input class="data_file" name="mhs_mobility_file_drh_uuid" id="mhs_mobility_file_drh_uuid" type="hidden" value="<?= set_value('mhs_mobility_file_drh_uuid'); ?>">
								<input class="data_file" name="mhs_mobility_file_drh_name" id="mhs_mobility_file_drh_name" type="hidden" value="<?= set_value('mhs_mobility_file_drh_name'); ?>">
								<small class="info help-block">
									<b>File DRH</b>, Extension file must PDF, <b>Max size file</b> 5 mb.</small>
							</div>

							<div class="col-sm-3">
								<div id="mhs_mobility_file_aktifkul_galery"></div>
								<input class="data_file" name="mhs_mobility_file_aktifkul_uuid" id="mhs_mobility_file_aktifkul_uuid" type="hidden" value="<?= set_value('mhs_mobility_file_aktifkul_uuid'); ?>">
								<input class="data_file" name="mhs_mobility_file_aktifkul_name" id="mhs_mobility_file_aktifkul_name" type="hidden" value="<?= set_value('mhs_mobility_file_aktifkul_name'); ?>">
								<small class="info help-block">
									<b>File Aktif Kuliah</b>, Extension file must PDF, <b>Max size file</b> 5 mb.</small>
							</div>

							<div class="col-sm-3">
								<div id="mhs_mobility_file_suket_biaya_galery"></div>
								<input class="data_file" name="mhs_mobility_file_suket_biaya_uuid" id="mhs_mobility_file_suket_biaya_uuid" type="hidden" value="<?= set_value('mhs_mobility_file_suket_biaya_uuid'); ?>">
								<input class="data_file" name="mhs_mobility_file_suket_biaya_name" id="mhs_mobility_file_suket_biaya_name" type="hidden" value="<?= set_value('mhs_mobility_file_suket_biaya_name'); ?>">
								<small class="info help-block">
									<b>Surat Ketarangan Biaya</b>, Extension file must PDF, <b>Max size file</b> 5 mb.</small>
							</div>
						</div>


						<div class="form-group scope-ln">
							<label for="file_ijin" class="col-sm-2 control-label">File Perijinan
								<i class="required">*</i>
							</label>
							<div class="col-sm-3">
								<input type="text" class="form-control pull-right datepicker" name="tgl_file_ijin" placeholder="Tanggal Surat Perijinan" id="tgl_file_ijin">
								<small class="info help-block">
									<b>Tanggal Perijinan</b>
								</small>
							</div>
							<div class="col-sm-5">
								<div id="mhs_mobility_file_ijin_galery"></div>
								<input class="data_file" name="mhs_mobility_file_ijin_uuid" id="mhs_mobility_file_ijin_uuid" type="hidden" value="<?= set_value('mhs_mobility_file_ijin_uuid'); ?>">
								<input class="data_file" name="mhs_mobility_file_ijin_name" id="mhs_mobility_file_ijin_name" type="hidden" value="<?= set_value('mhs_mobility_file_ijin_name'); ?>">
								<small class="info help-block">
									<b>*File perijinan, extension file must</b> PDF, <b>Max size file</b> 5 mb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="" class="col-sm-2 control-label">Jenis Kegiatan
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="bound_id" id="bound_id" data-placeholder="Select Jenis Kegiatan">
									<option value=""></option>
									<?php foreach (db_get_all_data('si_ref_kegiatan') as $row) : ?>
										<option value="<?= $row->id ?>"><?= $row->bound_name; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Input Bound Id</b> Max Length : 11.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tujuan_institusi" class="col-sm-2 control-label">Tujuan Institusi
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="tujuan_institusi" id="tujuan_institusi" placeholder="Tujuan Institusi" value="<?= set_value('tujuan_institusi'); ?>">
								<small class="info help-block">
									<b>Input Tujuan Institusi</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="bentuk_ks" class="col-sm-2 control-label">Nota Kesepahaman (MoU)
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="mou_ks" id="mou_ks" data-placeholder="Select MoU Kerjasama">
								</select>
								<small class="info help-block">
									<b>Pilih MoU</b>, jika tidak ada silahkan hubungi bagian kerjasama</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="lokasi" class="col-sm-2 control-label">Lokasi/kota
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="lokasi" id="lokasi" placeholder="Lokasi/kota" value="<?= set_value('lokasi'); ?>">
								<small class="info help-block">
									<b>Input Lokasi</b> Max Length : 255.</small>
							</div>
						</div>

						<div id="opsi-negara">
							<div class="form-group ">
								<label for="negara_tujuan" class="col-sm-2 control-label">Negara Tujuan
									<i class="required">*</i>
								</label>
								<div class="col-sm-8">
									<select class="form-control chosen chosen-select-deselect" name="negara_tujuan" id="negara_tujuan" data-placeholder="Select negara">
										<option value=""></option>
										<?php foreach (db_get_all_data('negara') as $row) : ?>
											<option value="<?= $row->nama_negara ?>"><?= $row->nama_negara; ?></option>
										<?php endforeach; ?>
									</select>
									<small class="info help-block">
										<b>Pilih negara</b></small>
								</div>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_mulai" class="col-sm-2 control-label">Tanggal Kegiatan
								<i class="required">*</i>
							</label>
							<div class="col-sm-3">
								<input type="text" class="form-control pull-left datepicker" name="tgl_mulai" placeholder="Tanggal Mulai" id="tgl_mulai">
								<small class="info help-block"><b>Tanggal Mulai</b></small>
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control pull-left datepicker" name="tgl_selesai" placeholder="Tanggal Selesai" id="tgl_selesai">
								<small class="info help-block"><b>Tanggal Selesai</b></small>
							</div>
						</div>

						<div class="form-group scope-ln">
							<label for="file_jdwkeg" class="col-sm-2 control-label">File Kegiatan
								<i class="required">*</i>
							</label>

							<div class="col-sm-2">
								<div id="mhs_mobility_file_jdwkeg_galery"></div>
								<input class="data_file" name="mhs_mobility_file_jdwkeg_uuid" id="mhs_mobility_file_jdwkeg_uuid" type="hidden" value="<?= set_value('mhs_mobility_file_jdwkeg_uuid'); ?>">
								<input class="data_file" name="mhs_mobility_file_jdwkeg_name" id="mhs_mobility_file_jdwkeg_name" type="hidden" value="<?= set_value('mhs_mobility_file_jdwkeg_name'); ?>">
								<small class="info help-block">
									<b>*Jadwal Kegiatan, Extension file must</b> PDF, <b>Max size file</b> 5 mb.</small>
							</div>

							<div class="col-sm-3">
								<div id="mhs_mobility_file_kontrak_kuliah_galery"></div>
								<input class="data_file" name="mhs_mobility_file_kontrak_kuliah_uuid" id="mhs_mobility_file_kontrak_kuliah_uuid" type="hidden" value="<?= set_value('mhs_mobility_file_kontrak_kuliah_uuid'); ?>">
								<input class="data_file" name="mhs_mobility_file_kontrak_kuliah_name" id="mhs_mobility_file_kontrak_kuliah_name" type="hidden" value="<?= set_value('mhs_mobility_file_kontrak_kuliah_name'); ?>">
								<small class="info help-block">
									<b>*Kontrak Kuliah </b>, diisi bagi kegiatan transfer kredit, Extension file must PDF.</small>
							</div>

							<div class="col-sm-3">
								<div id="mhs_mobility_file_bukti_kegiatan_galery"></div>
								<input class="data_file" name="mhs_mobility_file_bukti_kegiatan_uuid" id="mhs_mobility_file_bukti_kegiatan_uuid" type="hidden" value="<?= set_value('mhs_mobility_file_bukti_kegiatan_uuid'); ?>">
								<input class="data_file" name="mhs_mobility_file_bukti_kegiatan_name" id="mhs_mobility_file_bukti_kegiatan_name" type="hidden" value="<?= set_value('mhs_mobility_file_bukti_kegiatan_name'); ?>">
								<small class="info help-block">
									<b>Input File Bukti Kegiatan</b> Max Length : 255.</small>
							</div>

						</div>

						<div class="form-group ">
							<label for="catatan" class="col-sm-2 control-label">Catatan
							</label>
							<div class="col-sm-8">
								<textarea id="catatan" name="catatan" rows="5" class="textarea form-control"><?= set_value('catatan'); ?></textarea>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="validasi_mhs" class="col-sm-2 control-label">Validasi
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select" name="validasi_mhs" id="validasi_mhs" data-placeholder="Select Validasi">
									<option value=""></option>
									<option value="Y">Valid</option>
									<option value="T">Belum Valid</option>
								</select>
								<small class="info help-block">
									<b>Pastikan</b> terlebih dahulu data sudah benar, karena jika validasi : <b>Y</b>
									maka data tidak bisa <b>diubah</b> atau <b>dihapus</b>.
								</small>
							</div>
						</div>

						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	$(document).ready(function() {

		$('#scope').bind('change', function(e) {
			if (e.target.value == 'DN') $('div.form-group.scope-ln').hide();
			else $('div.form-group.scope-ln').show();
		})

		$('#btn_cancel').click(function() {
			swal({
					title: "<?= cclang('are_you_sure'); ?>",
					text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/mhs_mobility';
					}
				});
			return false;
		}); /*end btn cancel*/
		$('.btn_save').click(function() {
			$('.message').fadeOut();

			var form_mhs_mobility = $('#form_mhs_mobility');
			var data_post = form_mhs_mobility.serializeArray();
			var save_type = $(this).attr('data-stype');

			data_post.push({
				name: 'save_type',
				value: save_type
			});
			$('.loading').show();
			$.ajax({
					url: BASE_URL + '/administrator/mhs_mobility/add_save',
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('.steps li').removeClass('error');
					$('form').find('.error-input').remove();
					if (res.success) {
						var id_file_nik = $('#mhs_mobility_file_nik_galery').find('li').attr('qq-file-id');
						var id_file_loa = $('#mhs_mobility_file_loa_galery').find('li').attr('qq-file-id');
						var id_file_ktm = $('#mhs_mobility_file_ktm_galery').find('li').attr('qq-file-id');
						var id_file_rab = $('#mhs_mobility_file_rab_galery').find('li').attr('qq-file-id');
						var id_file_drh = $('#mhs_mobility_file_drh_galery').find('li').attr('qq-file-id');
						var id_file_jdwkeg = $('#mhs_mobility_file_jdwkeg_galery').find('li').attr('qq-file-id');
						var id_file_aktifkul = $('#mhs_mobility_file_aktifkul_galery').find('li').attr('qq-file-id');
						var id_file_suket_biaya = $('#mhs_mobility_file_suket_biaya_galery').find('li').attr('qq-file-id');
						var id_file_ijin = $('#mhs_mobility_file_ijin_galery').find('li').attr('qq-file-id');
						var id_file_bukti_kegiatan = $('#mhs_mobility_file_bukti_kegiatan_galery').find('li').attr('qq-file-id');

						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						resetForm();
						if (typeof id_file_nik !== 'undefined') {
							$('#mhs_mobility_file_nik_galery').fineUploader('deleteFile', id_file_nik);
						}
						if (typeof id_file_loa !== 'undefined') {
							$('#mhs_mobility_file_loa_galery').fineUploader('deleteFile', id_file_loa);
						}
						if (typeof id_file_ktm !== 'undefined') {
							$('#mhs_mobility_file_ktm_galery').fineUploader('deleteFile', id_file_ktm);
						}
						if (typeof id_file_rab !== 'undefined') {
							$('#mhs_mobility_file_rab_galery').fineUploader('deleteFile', id_file_rab);
						}
						if (typeof id_file_drh !== 'undefined') {
							$('#mhs_mobility_file_drh_galery').fineUploader('deleteFile', id_file_drh);
						}
						if (typeof id_file_jdwkeg !== 'undefined') {
							$('#mhs_mobility_file_jdwkeg_galery').fineUploader('deleteFile', id_file_jdwkeg);
						}
						if (typeof id_file_aktifkul !== 'undefined') {
							$('#mhs_mobility_file_aktifkul_galery').fineUploader('deleteFile', id_file_aktifkul);
						}
						if (typeof id_file_suket_biaya !== 'undefined') {
							$('#mhs_mobility_file_suket_biaya_galery').fineUploader('deleteFile', id_file_suket_biaya);
						}
						if (typeof id_file_ijin !== 'undefined') {
							$('#mhs_mobility_file_ijin_galery').fineUploader('deleteFile', id_file_ijin);
						}
						if (typeof id_file_bukti_kegiatan !== 'undefined') {
							$('#mhs_mobility_file_bukti_kegiatan_galery').fineUploader('deleteFile', id_file_bukti_kegiatan);
						}
						$('.chosen option').prop('selected', false).trigger('chosen:updated');

					} else {
						if (res.errors) {

							$.each(res.errors, function(index, val) {
								$('form #' + index).parents('.form-group').addClass('has-error');
								$('form #' + index).parents('.form-group').find('small').prepend(`
                      <div class="error-input">` + val + `</div>
                      `);
							});
							$('.steps li').removeClass('error');
							$('.content section').each(function(index, el) {
								if ($(this).find('.has-error').length) {
									$('.steps li:eq(' + index + ')').addClass('error').find('a').trigger('click');
								}
							});
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/

		var params = {};
		params[csrf] = token;

		$('#mhs_mobility_file_bukti_kegiatan_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_mobility/upload_file_bukti_kegiatan_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mhs_mobility/delete_file_bukti_kegiatan_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["*"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_mobility_file_bukti_kegiatan_galery').fineUploader('getUuid', id);
						$('#mhs_mobility_file_bukti_kegiatan_uuid').val(uuid);
						$('#mhs_mobility_file_bukti_kegiatan_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_mobility_file_bukti_kegiatan_uuid').val();
					$.get(BASE_URL + '/administrator/mhs_mobility/delete_file_bukti_kegiatan_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_mobility_file_bukti_kegiatan_uuid').val('');
						$('#mhs_mobility_file_bukti_kegiatan_name').val('');
					}
				}
			}
		}); /*end file_bukti_kegiatan galery*/

		var params = {};
		params[csrf] = token;

		$('#mhs_mobility_file_nik_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_mobility/upload_file_nik_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mhs_mobility/delete_file_nik_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_mobility_file_nik_galery').fineUploader('getUuid', id);
						$('#mhs_mobility_file_nik_uuid').val(uuid);
						$('#mhs_mobility_file_nik_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_mobility_file_nik_uuid').val();
					if (uuid != '')
						$.get(BASE_URL + '/administrator/mhs_mobility/delete_file_nik_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_mobility_file_nik_uuid').val('');
						$('#mhs_mobility_file_nik_name').val('');
					}
				}
			}
		}); /*end file_nik galery*/
		var params = {};
		params[csrf] = token;

		$('#mhs_mobility_file_loa_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_mobility/upload_file_loa_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mhs_mobility/delete_file_loa_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_mobility_file_loa_galery').fineUploader('getUuid', id);
						$('#mhs_mobility_file_loa_uuid').val(uuid);
						$('#mhs_mobility_file_loa_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_mobility_file_loa_uuid').val();
					if (uuid != '')
						$.get(BASE_URL + '/administrator/mhs_mobility/delete_file_loa_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_mobility_file_loa_uuid').val('');
						$('#mhs_mobility_file_loa_name').val('');
					}
				}
			}
		}); /*end file_loa galery*/
		var params = {};
		params[csrf] = token;

		$('#mhs_mobility_file_ktm_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_mobility/upload_file_ktm_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mhs_mobility/delete_file_ktm_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_mobility_file_ktm_galery').fineUploader('getUuid', id);
						$('#mhs_mobility_file_ktm_uuid').val(uuid);
						$('#mhs_mobility_file_ktm_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_mobility_file_ktm_uuid').val();
					if (uuid != '')
						$.get(BASE_URL + '/administrator/mhs_mobility/delete_file_ktm_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_mobility_file_ktm_uuid').val('');
						$('#mhs_mobility_file_ktm_name').val('');
					}
				}
			}
		}); /*end file_ktm galery*/
		var params = {};
		params[csrf] = token;

		$('#mhs_mobility_file_rab_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_mobility/upload_file_rab_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mhs_mobility/delete_file_rab_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_mobility_file_rab_galery').fineUploader('getUuid', id);
						$('#mhs_mobility_file_rab_uuid').val(uuid);
						$('#mhs_mobility_file_rab_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_mobility_file_rab_uuid').val();
					if (uuid != '')
						$.get(BASE_URL + '/administrator/mhs_mobility/delete_file_rab_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_mobility_file_rab_uuid').val('');
						$('#mhs_mobility_file_rab_name').val('');
					}
				}
			}
		}); /*end file_rab galery*/
		var params = {};
		params[csrf] = token;

		$('#mhs_mobility_file_drh_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_mobility/upload_file_drh_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mhs_mobility/delete_file_drh_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_mobility_file_drh_galery').fineUploader('getUuid', id);
						$('#mhs_mobility_file_drh_uuid').val(uuid);
						$('#mhs_mobility_file_drh_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_mobility_file_drh_uuid').val();
					if (uuid != '')
						$.get(BASE_URL + '/administrator/mhs_mobility/delete_file_drh_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_mobility_file_drh_uuid').val('');
						$('#mhs_mobility_file_drh_name').val('');
					}
				}
			}
		}); /*end file_drh galery*/
		var params = {};
		params[csrf] = token;

		$('#mhs_mobility_file_jdwkeg_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_mobility/upload_file_jdwkeg_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mhs_mobility/delete_file_jdwkeg_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_mobility_file_jdwkeg_galery').fineUploader('getUuid', id);
						$('#mhs_mobility_file_jdwkeg_uuid').val(uuid);
						$('#mhs_mobility_file_jdwkeg_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_mobility_file_jdwkeg_uuid').val();
					if (uuid != '')
						$.get(BASE_URL + '/administrator/mhs_mobility/delete_file_jdwkeg_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_mobility_file_jdwkeg_uuid').val('');
						$('#mhs_mobility_file_jdwkeg_name').val('');
					}
				}
			}
		}); /*end file_jdwkeg galery*/

		var params = {};
		params[csrf] = token;

		$('#mhs_mobility_file_kontrak_kuliah_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_mobility/upload_file_kontrak_kuliah_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mhs_mobility/delete_file_kontrak_kuliah_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 0,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_mobility_file_kontrak_kuliah_galery').fineUploader('getUuid', id);
						$('#mhs_mobility_file_kontrak_kuliah_uuid').val(uuid);
						$('#mhs_mobility_file_kontrak_kuliah_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_mobility_file_kontrak_kuliah_uuid').val();
					$.get(BASE_URL + '/administrator/mhs_mobility/delete_file_kontrak_kuliah_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_mobility_file_kontrak_kuliah_uuid').val('');
						$('#mhs_mobility_file_kontrak_kuliah_name').val('');
					}
				}
			}
		}); /*end file_kontrak_kuliah galery*/

		var params = {};
		params[csrf] = token;

		$('#mhs_mobility_file_aktifkul_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_mobility/upload_file_aktifkul_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mhs_mobility/delete_file_aktifkul_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_mobility_file_aktifkul_galery').fineUploader('getUuid', id);
						$('#mhs_mobility_file_aktifkul_uuid').val(uuid);
						$('#mhs_mobility_file_aktifkul_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_mobility_file_aktifkul_uuid').val();
					if (uuid != '')
						$.get(BASE_URL + '/administrator/mhs_mobility/delete_file_aktifkul_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_mobility_file_aktifkul_uuid').val('');
						$('#mhs_mobility_file_aktifkul_name').val('');
					}
				}
			}
		}); /*end file_aktifkul galery*/
		var params = {};
		params[csrf] = token;

		$('#mhs_mobility_file_suket_biaya_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_mobility/upload_file_suket_biaya_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mhs_mobility/delete_file_suket_biaya_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_mobility_file_suket_biaya_galery').fineUploader('getUuid', id);
						$('#mhs_mobility_file_suket_biaya_uuid').val(uuid);
						$('#mhs_mobility_file_suket_biaya_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_mobility_file_suket_biaya_uuid').val();
					if (uuid != '')
						$.get(BASE_URL + '/administrator/mhs_mobility/delete_file_suket_biaya_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_mobility_file_suket_biaya_uuid').val('');
						$('#mhs_mobility_file_suket_biaya_name').val('');
					}
				}
			}
		}); /*end file_suket_biaya galery*/
		var params = {};
		params[csrf] = token;

		$('#mhs_mobility_file_ijin_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/mhs_mobility/upload_file_ijin_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/mhs_mobility/delete_file_ijin_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#mhs_mobility_file_ijin_galery').fineUploader('getUuid', id);
						$('#mhs_mobility_file_ijin_uuid').val(uuid);
						$('#mhs_mobility_file_ijin_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#mhs_mobility_file_ijin_uuid').val();
					if (uuid != '')
						$.get(BASE_URL + '/administrator/mhs_mobility/delete_file_ijin_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#mhs_mobility_file_ijin_uuid').val('');
						$('#mhs_mobility_file_ijin_name').val('');
					}
				}
			}
		}); /*end file_ijin galery*/
		$('#scope').change(function(e) {
			if (e.target.value == 'LN')
				$.get('<?= site_url('administrator/mhs_mobility/getChangeOpsiNegara') ?>', {}, function(page) {
					$('#opsi-negara').html(page);
				});
			else
				$('#opsi-negara').empty();
			$.get('<?= site_url('administrator/si_kerjasama/get_option_mou') ?>', {
				scope: e.target.value
			}, function(page) {
				$('#mou_ks').html(page).val('').trigger('chosen:updated');
			})
		});
		$.get('<?= site_url('administrator/si_kerjasama/get_option_mou') ?>', {
			scope: $('#scope option:selected').val()
		}, function(page) {
			$('#mou_ks').html(page).val('').trigger('chosen:updated');
		})
	}); /*end doc ready*/
	function cari_mahasiswa() {
		BootstrapDialog.show({
			title: 'Cari data Mahasiswa',
			draggable: true,
			message: function(dialog) {
				var $message = $('<div></div>');
				$.get('<?= site_url('administrator/mhs_mobility/form_cari_mahasiswa') ?>', {
					ta: $('select[name=ta] option:selected').val()
				}, function(response) {
					$message.html(response);
				})
				return $message;
			},
			buttons: [{
				label: 'Ambil',
				icon: 'glyphicon glyphicon-plus',
				cssClass: 'btn-primary',
				action: function(dialog) {
					$('#nim').val($('#mahasiswa option:selected').data('nim'));
					$('#nama').val($('#mahasiswa option:selected').data('nama'));
					$('#kd_prodi').val($('#mahasiswa option:selected').data('prodi')).trigger('chosen:updated');
					$('#email').val($('#mahasiswa option:selected').data('email'));
					$('#no_tlp').val($('#mahasiswa option:selected').data('telp'));
					dialog.close();
				}
			}, {
				label: 'Tutup',
				action: function(dialog) {
					dialog.close();
				}
			}],
		});
	}
</script>