<script defer src="<?= BASE_ASSET; ?>/js/custom.js"></script>
<div class="row">
	<div class="form-group ">
		<label for="" class="col-sm-2 control-label">Cari Mahasiswa</label>
		<div class="col-sm-10">
			<select class="form-control chosen chosen-select-deselect" name="mahasiswa" id="mahasiswa" data-placeholder="cari nim,nama mahasiswa...">
				<option value=""></option>
				<?php foreach ($mhs as $row) :
					$data = "data-nim='$row->nim' data-nama='$row->nama' data-prodi='$row->program_studi' data-email='$row->email' data-telp='$row->telp'"; ?>
					<option <?= $data ?> value="<?= $row->nim ?>"><?= $row->nim .' - '. $row->nama . ' - ' . $row->prodi . ' - ' . $row->ta ?></option>
				<?php endforeach; ?>
			</select>
			<small class="info help-block">
				cari data mahasiswa aktif di sikadu
			</small>
		</div>
	</div>
</div>