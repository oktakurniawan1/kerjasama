<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['studi_lanjut'] = 'Studi Lanjut';
$lang['id'] = 'Id';
$lang['nip'] = 'Nip';
$lang['nama'] = 'Nama';
$lang['kd_prodi'] = 'Kd Prodi';
$lang['prodi'] = 'Prodi/Unit';
$lang['univ_tujuan'] = 'Universitas Tujuan';
$lang['prodi_tujuan'] = 'Program Studi Tujuan';
$lang['negara'] = 'Negara';
$lang['tgl_mulai'] = 'Tanggal Mulai';
$lang['tgl_selesai'] = 'Tanggal Selesai';
$lang['file_loa'] = 'File LOA';
$lang['file_pengantar'] = 'File Pengantar';
$lang['file_ijin_belajar'] = 'File Ijin Belajar';
$lang['file_biaya'] = 'File Keterangan Biaya';
$lang['file_jadwal_kuliah'] = 'File Jadwal Kuliah';
$lang['file_drh'] = 'File DRH';
$lang['file_ktp'] = 'File KTP/NIK';
$lang['file_sk_pns'] = 'File SK PNS';
$lang['file_karpeg'] = 'File Kartu Pegawai';
$lang['file_pulang'] = 'File Keterangan Kembali';
$lang['file_bebas_bealain'] = 'File Bebas Biasiswa Lain';
$lang['catatan'] = 'Catatan';
$lang['validasi'] = 'Validasi';
$lang['is_extend'] = 'Is Extend';
