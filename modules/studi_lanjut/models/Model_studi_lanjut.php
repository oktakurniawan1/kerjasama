<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_studi_lanjut extends MY_Model
{

    private $primary_key    = 'id';
    private $table_name     = 'studi_lanjut';
    public $field_search   = ['nip', 'nama', 'kd_prodi', 'univ_tujuan', 'prodi_tujuan', 'negara', 'tgl_mulai', 'tgl_selesai', 'validasi'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
        );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "studi_lanjut." . $field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "studi_lanjut." . $field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '(' . $where . ')';
        } else {
            $where .= "(" . "studi_lanjut." . $field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "studi_lanjut." . $field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "studi_lanjut." . $field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '(' . $where . ')';
        } else {
            $where .= "(" . "studi_lanjut." . $field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) and count($select_field)) {
            $this->db->select($select_field);
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);

        $this->sortable();

        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable()
    {

        $this->db->select('studi_lanjut.*');
        $this->db->join('ref_prodi p', 'p.kode = studi_lanjut.kd_prodi', 'left');
        $this->db->join("(select nip as nip2 from extend_study  group by nip) as T", 'T.nip2=studi_lanjut.nip', 'left');
        $this->db->select('IF(isnull(nip2),0,1) as is_extend', false);
        $this->db->select('p.nama_prodi');

        return $this;
    }

    public function filter_avaiable()
    {

        // if (!$this->aauth->is_admin()) {
        //     }
        $prodi = [];
        if (is_groups_in(['Dosen', 'Tendik'])) {
            if (is_non_user()) $nip = fromsess('nip_baru');
            else $nip = get_user_data('username');
            $this->db->where('nip', $nip);
        } else if (is_member('Kaprodi')) {
            if (is_non_user()) $prodi[] = fromsess('kd_prodi');
            else {
                $prodi = get_user_data('prodi');
                $prodi = explode(',', $prodi);
            }
            $this->db->where_in('kd_prodi', $prodi);
        } else if (is_member('Unit')) {
            if (is_non_user()) $prodi[] = fromsess('kd_prodi');
            else {
                $prodi = get_user_data('prodi');
                $prodi = explode(',', $prodi);
            }
            $this->db->where_in('kd_prodi', $prodi);
        }

        return $this;
    }
}

/* End of file Model_studi_lanjut.php */
/* Location: ./application/models/Model_studi_lanjut.php */