<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Studi Lanjut Controller
 *| --------------------------------------------------------------------------
 *| Studi Lanjut site
 *|
 */
class Studi_lanjut extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_studi_lanjut');
		$this->lang->load('web_lang', $this->current_lang);
		//cek_isi_survey();
	}

	/**
	 * show all Studi Lanjuts
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('studi_lanjut_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('studi_lanjut_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('studi_lanjut_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('studi_lanjut_filter');
			$this->session->unset_userdata('studi_lanjut_field');
		}
		$filter = $this->session->userdata('studi_lanjut_filter');
		$field = $this->session->userdata('studi_lanjut_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['studi_lanjuts'] = $this->model_studi_lanjut->get($filter, $field, $this->limit_page, $offset);
		$this->data['studi_lanjut_counts'] = $this->model_studi_lanjut->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/studi_lanjut/index/',
			'total_rows'   => $this->model_studi_lanjut->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Studi Lanjut List');
		$this->render('backend/standart/administrator/studi_lanjut/studi_lanjut_list', $this->data);
	}

	/**
	 * Add new studi_lanjuts
	 *
	 */
	public function add()
	{
		$this->is_allowed('studi_lanjut_add');

		$this->template->title('Studi Lanjut New');
		$filter_prodi = get_filter_prodi();
		$this->data['filter_prodi'] = $filter_prodi;
		$this->render('backend/standart/administrator/studi_lanjut/studi_lanjut_add', $this->data);
	}

	/**
	 * Add New Studi Lanjuts
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('studi_lanjut_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('nip', 'Nip', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kd_prodi', 'Kd Prodi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('univ_tujuan', 'Universitas Tujuan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('prodi_tujuan', 'Program Studi Tujuan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('negara', 'Negara', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'trim|required');
		$this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_loa_name', 'File LOA', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_pengantar_name', 'File LOA', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_ijin_belajar_name', 'File Ijin Belajar', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_biaya_name', 'File Keterangan Biaya', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_jadwal_kuliah_name', 'File Jadwal Kuliah', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_drh_name', 'File DRH', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_ktp_name', 'File KTP/NIK', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_sk_pns_name', 'File SK PNS', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_karpeg_name', 'File Kartu Pegawai', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_pulang_name', 'File Keterangan Kembali', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_bebas_bealain_name', 'File Bebas Biasiswa Lain', 'trim|required');
		//tambahan
		// $this->form_validation->set_rules('studi_lanjut_file_bebas_cov19_name', 'File Bebas Cov19', 'trim|required');
		// $this->form_validation->set_rules('studi_lanjut_file_ket_duta_name', 'File Ket Duta', 'trim|required');
		// $this->form_validation->set_rules('studi_lanjut_file_ket_univ_tujuan_name', 'File Ket Univ Tujuan', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_paspor_name', 'File Paspor', 'trim|required');


		if ($this->form_validation->run()) {
			//file pengantar
			$studi_lanjut_file_pengantar_uuid = $this->input->post('studi_lanjut_file_pengantar_uuid');
			$studi_lanjut_file_pengantar_name = $this->input->post('studi_lanjut_file_pengantar_name');
			$studi_lanjut_file_loa_uuid = $this->input->post('studi_lanjut_file_loa_uuid');
			$studi_lanjut_file_loa_name = $this->input->post('studi_lanjut_file_loa_name');
			$studi_lanjut_file_ijin_belajar_uuid = $this->input->post('studi_lanjut_file_ijin_belajar_uuid');
			$studi_lanjut_file_ijin_belajar_name = $this->input->post('studi_lanjut_file_ijin_belajar_name');
			$studi_lanjut_file_biaya_uuid = $this->input->post('studi_lanjut_file_biaya_uuid');
			$studi_lanjut_file_biaya_name = $this->input->post('studi_lanjut_file_biaya_name');
			$studi_lanjut_file_jadwal_kuliah_uuid = $this->input->post('studi_lanjut_file_jadwal_kuliah_uuid');
			$studi_lanjut_file_jadwal_kuliah_name = $this->input->post('studi_lanjut_file_jadwal_kuliah_name');
			$studi_lanjut_file_drh_uuid = $this->input->post('studi_lanjut_file_drh_uuid');
			$studi_lanjut_file_drh_name = $this->input->post('studi_lanjut_file_drh_name');
			$studi_lanjut_file_ktp_uuid = $this->input->post('studi_lanjut_file_ktp_uuid');
			$studi_lanjut_file_ktp_name = $this->input->post('studi_lanjut_file_ktp_name');
			$studi_lanjut_file_sk_pns_uuid = $this->input->post('studi_lanjut_file_sk_pns_uuid');
			$studi_lanjut_file_sk_pns_name = $this->input->post('studi_lanjut_file_sk_pns_name');
			$studi_lanjut_file_karpeg_uuid = $this->input->post('studi_lanjut_file_karpeg_uuid');
			$studi_lanjut_file_karpeg_name = $this->input->post('studi_lanjut_file_karpeg_name');
			$studi_lanjut_file_pulang_uuid = $this->input->post('studi_lanjut_file_pulang_uuid');
			$studi_lanjut_file_pulang_name = $this->input->post('studi_lanjut_file_pulang_name');
			$studi_lanjut_file_bebas_bealain_uuid = $this->input->post('studi_lanjut_file_bebas_bealain_uuid');
			$studi_lanjut_file_bebas_bealain_name = $this->input->post('studi_lanjut_file_bebas_bealain_name');
			//tambahan
			$studi_lanjut_file_bebas_cov19_uuid = $this->input->post('studi_lanjut_file_bebas_cov19_uuid');
			$studi_lanjut_file_bebas_cov19_name = $this->input->post('studi_lanjut_file_bebas_cov19_name');
			$studi_lanjut_file_ket_duta_uuid = $this->input->post('studi_lanjut_file_ket_duta_uuid');
			$studi_lanjut_file_ket_duta_name = $this->input->post('studi_lanjut_file_ket_duta_name');
			$studi_lanjut_file_ket_univ_tujuan_uuid = $this->input->post('studi_lanjut_file_ket_univ_tujuan_uuid');
			$studi_lanjut_file_ket_univ_tujuan_name = $this->input->post('studi_lanjut_file_ket_univ_tujuan_name');
			$studi_lanjut_file_paspor_uuid = $this->input->post('studi_lanjut_file_paspor_uuid');
			$studi_lanjut_file_paspor_name = $this->input->post('studi_lanjut_file_paspor_name');

			$save_data = [
				'nip' => $this->input->post('nip'),
				'nama' => $this->input->post('nama'),
				'kd_prodi' => $this->input->post('kd_prodi'),
				'univ_tujuan' => $this->input->post('univ_tujuan'),
				'prodi_tujuan' => $this->input->post('prodi_tujuan'),
				'negara' => $this->input->post('negara'),
				'tgl_mulai' => $this->input->post('tgl_mulai'),
				'tgl_selesai' => $this->input->post('tgl_selesai'),
				'catatan' => $this->input->post('catatan'),
				'validasi' => $this->input->post('validasi'),
			];

			if (!is_dir(FCPATH . '/uploads/studi_lanjut/')) {
				mkdir(FCPATH . '/uploads/studi_lanjut/');
			}

			//file pengantar
			if (!empty($studi_lanjut_file_pengantar_name)) {
				$studi_lanjut_file_pengantar_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_pengantar_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_pengantar_uuid . '/' . $studi_lanjut_file_pengantar_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_pengantar_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_pengantar_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_pengantar'] = $studi_lanjut_file_pengantar_name_copy;
			}

			//file cov19

			if (!empty($studi_lanjut_file_bebas_cov19_name)) {
				$studi_lanjut_file_bebas_cov19_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_bebas_cov19_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_bebas_cov19_uuid . '/' . $studi_lanjut_file_bebas_cov19_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_bebas_cov19_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_bebas_cov19_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_bebas_cov19'] = $studi_lanjut_file_bebas_cov19_name_copy;
			}

			if (!empty($studi_lanjut_file_ket_duta_name)) {
				$studi_lanjut_file_ket_duta_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_ket_duta_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_ket_duta_uuid . '/' . $studi_lanjut_file_ket_duta_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_ket_duta_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_ket_duta_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ket_duta'] = $studi_lanjut_file_ket_duta_name_copy;
			}

			if (!empty($studi_lanjut_file_ket_univ_tujuan_name)) {
				$studi_lanjut_file_ket_univ_tujuan_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_ket_univ_tujuan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_ket_univ_tujuan_uuid . '/' . $studi_lanjut_file_ket_univ_tujuan_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_ket_univ_tujuan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_ket_univ_tujuan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ket_univ_tujuan'] = $studi_lanjut_file_ket_univ_tujuan_name_copy;
			}

			if (!empty($studi_lanjut_file_paspor_name)) {
				$studi_lanjut_file_paspor_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_paspor_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_paspor_uuid . '/' . $studi_lanjut_file_paspor_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_paspor_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_paspor_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_paspor'] = $studi_lanjut_file_paspor_name_copy;
			}
			//----tambahan

			if (!empty($studi_lanjut_file_loa_name)) {
				$studi_lanjut_file_loa_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_loa_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_loa_uuid . '/' . $studi_lanjut_file_loa_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_loa_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_loa'] = $studi_lanjut_file_loa_name_copy;
			}

			if (!empty($studi_lanjut_file_ijin_belajar_name)) {
				$studi_lanjut_file_ijin_belajar_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_ijin_belajar_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_ijin_belajar_uuid . '/' . $studi_lanjut_file_ijin_belajar_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_ijin_belajar_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_ijin_belajar_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ijin_belajar'] = $studi_lanjut_file_ijin_belajar_name_copy;
			}

			if (!empty($studi_lanjut_file_biaya_name)) {
				$studi_lanjut_file_biaya_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_biaya_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_biaya_uuid . '/' . $studi_lanjut_file_biaya_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_biaya_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_biaya_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_biaya'] = $studi_lanjut_file_biaya_name_copy;
			}

			if (!empty($studi_lanjut_file_jadwal_kuliah_name)) {
				$studi_lanjut_file_jadwal_kuliah_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_jadwal_kuliah_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_jadwal_kuliah_uuid . '/' . $studi_lanjut_file_jadwal_kuliah_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_jadwal_kuliah_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_jadwal_kuliah_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_jadwal_kuliah'] = $studi_lanjut_file_jadwal_kuliah_name_copy;
			}

			if (!empty($studi_lanjut_file_drh_name)) {
				$studi_lanjut_file_drh_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_drh_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_drh_uuid . '/' . $studi_lanjut_file_drh_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_drh_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_drh_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_drh'] = $studi_lanjut_file_drh_name_copy;
			}

			if (!empty($studi_lanjut_file_ktp_name)) {
				$studi_lanjut_file_ktp_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_ktp_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_ktp_uuid . '/' . $studi_lanjut_file_ktp_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_ktp_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_ktp_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ktp'] = $studi_lanjut_file_ktp_name_copy;
			}

			if (!empty($studi_lanjut_file_sk_pns_name)) {
				$studi_lanjut_file_sk_pns_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_sk_pns_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_sk_pns_uuid . '/' . $studi_lanjut_file_sk_pns_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_sk_pns_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_sk_pns_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_sk_pns'] = $studi_lanjut_file_sk_pns_name_copy;
			}

			if (!empty($studi_lanjut_file_karpeg_name)) {
				$studi_lanjut_file_karpeg_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_karpeg_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_karpeg_uuid . '/' . $studi_lanjut_file_karpeg_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_karpeg_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_karpeg_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_karpeg'] = $studi_lanjut_file_karpeg_name_copy;
			}

			if (!empty($studi_lanjut_file_pulang_name)) {
				$studi_lanjut_file_pulang_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_pulang_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_pulang_uuid . '/' . $studi_lanjut_file_pulang_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_pulang_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_pulang_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_pulang'] = $studi_lanjut_file_pulang_name_copy;
			}

			if (!empty($studi_lanjut_file_bebas_bealain_name)) {
				$studi_lanjut_file_bebas_bealain_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_bebas_bealain_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_bebas_bealain_uuid . '/' . $studi_lanjut_file_bebas_bealain_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_bebas_bealain_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_bebas_bealain_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_bebas_bealain'] = $studi_lanjut_file_bebas_bealain_name_copy;
			}


			$save_studi_lanjut = $this->model_studi_lanjut->store($save_data);


			if ($save_studi_lanjut) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_studi_lanjut;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/studi_lanjut/edit/' . $save_studi_lanjut, 'Edit Studi Lanjut'),
						anchor('administrator/studi_lanjut', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/studi_lanjut/edit/' . $save_studi_lanjut, 'Edit Studi Lanjut')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/studi_lanjut');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/studi_lanjut');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Studi Lanjuts
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('studi_lanjut_update');

		$this->data['studi_lanjut'] = $this->model_studi_lanjut->find($id);
		if (!is_groups_in(['Admin', 'Kerjasama'])) {
			if ($this->data['studi_lanjut']->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa mengubah, karena data sudah divalidasi', 'studi_lanjut'), 'warning');
				redirect_back();
			}
		}

		$this->data['studi_lanjut'] = $this->model_studi_lanjut->find($id);
		$filter_prodi = get_filter_prodi();
		$this->data['filter_prodi'] = $filter_prodi;
		$this->template->title('Studi Lanjut Update');
		$this->render('backend/standart/administrator/studi_lanjut/studi_lanjut_update', $this->data);
	}

	/**
	 * Update Studi Lanjuts
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('studi_lanjut_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('nip', 'Nip', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kd_prodi', 'Kd Prodi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('univ_tujuan', 'Universitas Tujuan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('prodi_tujuan', 'Program Studi Tujuan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('negara', 'Negara', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'trim|required');
		$this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_loa_name', 'File LOA', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_pengantar_name', 'File Pengantar', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_ijin_belajar_name', 'File Ijin Belajar', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_biaya_name', 'File Keterangan Biaya', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_jadwal_kuliah_name', 'File Jadwal Kuliah', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_drh_name', 'File DRH', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_ktp_name', 'File KTP/NIK', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_sk_pns_name', 'File SK PNS', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_karpeg_name', 'File Kartu Pegawai', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_pulang_name', 'File Keterangan Kembali', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_bebas_bealain_name', 'File Bebas Biasiswa Lain', 'trim|required');
		//tambahan
		// $this->form_validation->set_rules('studi_lanjut_file_bebas_cov19_name', 'File Bebas Cov19', 'trim|required');
		// $this->form_validation->set_rules('studi_lanjut_file_ket_duta_name', 'File Ket Duta', 'trim|required');
		// $this->form_validation->set_rules('studi_lanjut_file_ket_univ_tujuan_name', 'File Ket Univ Tujuan', 'trim|required');
		$this->form_validation->set_rules('studi_lanjut_file_paspor_name', 'File Paspor', 'trim|required');

		if ($this->form_validation->run()) {
			//file pengantar
			$studi_lanjut_file_pengantar_uuid = $this->input->post('studi_lanjut_file_pengantar_uuid');
			$studi_lanjut_file_pengantar_name = $this->input->post('studi_lanjut_file_pengantar_name');
			$studi_lanjut_file_loa_uuid = $this->input->post('studi_lanjut_file_loa_uuid');
			$studi_lanjut_file_loa_name = $this->input->post('studi_lanjut_file_loa_name');
			$studi_lanjut_file_ijin_belajar_uuid = $this->input->post('studi_lanjut_file_ijin_belajar_uuid');
			$studi_lanjut_file_ijin_belajar_name = $this->input->post('studi_lanjut_file_ijin_belajar_name');
			$studi_lanjut_file_biaya_uuid = $this->input->post('studi_lanjut_file_biaya_uuid');
			$studi_lanjut_file_biaya_name = $this->input->post('studi_lanjut_file_biaya_name');
			$studi_lanjut_file_jadwal_kuliah_uuid = $this->input->post('studi_lanjut_file_jadwal_kuliah_uuid');
			$studi_lanjut_file_jadwal_kuliah_name = $this->input->post('studi_lanjut_file_jadwal_kuliah_name');
			$studi_lanjut_file_drh_uuid = $this->input->post('studi_lanjut_file_drh_uuid');
			$studi_lanjut_file_drh_name = $this->input->post('studi_lanjut_file_drh_name');
			$studi_lanjut_file_ktp_uuid = $this->input->post('studi_lanjut_file_ktp_uuid');
			$studi_lanjut_file_ktp_name = $this->input->post('studi_lanjut_file_ktp_name');
			$studi_lanjut_file_sk_pns_uuid = $this->input->post('studi_lanjut_file_sk_pns_uuid');
			$studi_lanjut_file_sk_pns_name = $this->input->post('studi_lanjut_file_sk_pns_name');
			$studi_lanjut_file_karpeg_uuid = $this->input->post('studi_lanjut_file_karpeg_uuid');
			$studi_lanjut_file_karpeg_name = $this->input->post('studi_lanjut_file_karpeg_name');
			$studi_lanjut_file_pulang_uuid = $this->input->post('studi_lanjut_file_pulang_uuid');
			$studi_lanjut_file_pulang_name = $this->input->post('studi_lanjut_file_pulang_name');
			$studi_lanjut_file_bebas_bealain_uuid = $this->input->post('studi_lanjut_file_bebas_bealain_uuid');
			$studi_lanjut_file_bebas_bealain_name = $this->input->post('studi_lanjut_file_bebas_bealain_name');
			//tambahan
			$studi_lanjut_file_bebas_cov19_uuid = $this->input->post('studi_lanjut_file_bebas_cov19_uuid');
			$studi_lanjut_file_bebas_cov19_name = $this->input->post('studi_lanjut_file_bebas_cov19_name');
			$studi_lanjut_file_ket_duta_uuid = $this->input->post('studi_lanjut_file_ket_duta_uuid');
			$studi_lanjut_file_ket_duta_name = $this->input->post('studi_lanjut_file_ket_duta_name');
			$studi_lanjut_file_ket_univ_tujuan_uuid = $this->input->post('studi_lanjut_file_ket_univ_tujuan_uuid');
			$studi_lanjut_file_ket_univ_tujuan_name = $this->input->post('studi_lanjut_file_ket_univ_tujuan_name');
			$studi_lanjut_file_paspor_uuid = $this->input->post('studi_lanjut_file_paspor_uuid');
			$studi_lanjut_file_paspor_name = $this->input->post('studi_lanjut_file_paspor_name');

			$save_data = [
				'nip' => $this->input->post('nip'),
				'nama' => $this->input->post('nama'),
				'kd_prodi' => $this->input->post('kd_prodi'),
				'univ_tujuan' => $this->input->post('univ_tujuan'),
				'prodi_tujuan' => $this->input->post('prodi_tujuan'),
				'negara' => $this->input->post('negara'),
				'tgl_mulai' => $this->input->post('tgl_mulai'),
				'tgl_selesai' => $this->input->post('tgl_selesai'),
				'catatan' => $this->input->post('catatan'),
				'validasi' => $this->input->post('validasi'),
			];

			if (!is_dir(FCPATH . '/uploads/studi_lanjut/')) {
				mkdir(FCPATH . '/uploads/studi_lanjut/');
			}

			//file pengantar
			if (!empty($studi_lanjut_file_pengantar_uuid)) {
				$studi_lanjut_file_pengantar_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_pengantar_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_pengantar_uuid . '/' . $studi_lanjut_file_pengantar_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_pengantar_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_pengantar_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_pengantar'] = $studi_lanjut_file_pengantar_name_copy;
			}

			if (!empty($studi_lanjut_file_loa_uuid)) {
				$studi_lanjut_file_loa_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_loa_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_loa_uuid . '/' . $studi_lanjut_file_loa_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_loa_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_loa_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_loa'] = $studi_lanjut_file_loa_name_copy;
			}

			//file cov19

			if (!empty($studi_lanjut_file_bebas_cov19_uuid)) {
				$studi_lanjut_file_bebas_cov19_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_bebas_cov19_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_bebas_cov19_uuid . '/' . $studi_lanjut_file_bebas_cov19_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_bebas_cov19_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_bebas_cov19_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_bebas_cov19'] = $studi_lanjut_file_bebas_cov19_name_copy;
			}

			if (!empty($studi_lanjut_file_ket_duta_uuid)) {
				$studi_lanjut_file_ket_duta_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_ket_duta_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_ket_duta_uuid . '/' . $studi_lanjut_file_ket_duta_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_ket_duta_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_ket_duta_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ket_duta'] = $studi_lanjut_file_ket_duta_name_copy;
			}

			if (!empty($studi_lanjut_file_ket_univ_tujuan_uuid)) {
				$studi_lanjut_file_ket_univ_tujuan_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_ket_univ_tujuan_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_ket_univ_tujuan_uuid . '/' . $studi_lanjut_file_ket_univ_tujuan_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_ket_univ_tujuan_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_ket_univ_tujuan_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ket_univ_tujuan'] = $studi_lanjut_file_ket_univ_tujuan_name_copy;
			}

			if (!empty($studi_lanjut_file_paspor_uuid)) {
				$studi_lanjut_file_paspor_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_paspor_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_paspor_uuid . '/' . $studi_lanjut_file_paspor_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_paspor_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_paspor_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_paspor'] = $studi_lanjut_file_paspor_name_copy;
			}
			//----tambahan

			if (!empty($studi_lanjut_file_ijin_belajar_uuid)) {
				$studi_lanjut_file_ijin_belajar_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_ijin_belajar_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_ijin_belajar_uuid . '/' . $studi_lanjut_file_ijin_belajar_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_ijin_belajar_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_ijin_belajar_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ijin_belajar'] = $studi_lanjut_file_ijin_belajar_name_copy;
			}

			if (!empty($studi_lanjut_file_biaya_uuid)) {
				$studi_lanjut_file_biaya_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_biaya_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_biaya_uuid . '/' . $studi_lanjut_file_biaya_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_biaya_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_biaya_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_biaya'] = $studi_lanjut_file_biaya_name_copy;
			}

			if (!empty($studi_lanjut_file_jadwal_kuliah_uuid)) {
				$studi_lanjut_file_jadwal_kuliah_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_jadwal_kuliah_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_jadwal_kuliah_uuid . '/' . $studi_lanjut_file_jadwal_kuliah_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_jadwal_kuliah_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_jadwal_kuliah_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_jadwal_kuliah'] = $studi_lanjut_file_jadwal_kuliah_name_copy;
			}

			if (!empty($studi_lanjut_file_drh_uuid)) {
				$studi_lanjut_file_drh_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_drh_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_drh_uuid . '/' . $studi_lanjut_file_drh_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_drh_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_drh_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_drh'] = $studi_lanjut_file_drh_name_copy;
			}

			if (!empty($studi_lanjut_file_ktp_uuid)) {
				$studi_lanjut_file_ktp_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_ktp_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_ktp_uuid . '/' . $studi_lanjut_file_ktp_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_ktp_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_ktp_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ktp'] = $studi_lanjut_file_ktp_name_copy;
			}

			if (!empty($studi_lanjut_file_sk_pns_uuid)) {
				$studi_lanjut_file_sk_pns_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_sk_pns_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_sk_pns_uuid . '/' . $studi_lanjut_file_sk_pns_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_sk_pns_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_sk_pns_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_sk_pns'] = $studi_lanjut_file_sk_pns_name_copy;
			}

			if (!empty($studi_lanjut_file_karpeg_uuid)) {
				$studi_lanjut_file_karpeg_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_karpeg_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_karpeg_uuid . '/' . $studi_lanjut_file_karpeg_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_karpeg_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_karpeg_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_karpeg'] = $studi_lanjut_file_karpeg_name_copy;
			}

			if (!empty($studi_lanjut_file_pulang_uuid)) {
				$studi_lanjut_file_pulang_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_pulang_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_pulang_uuid . '/' . $studi_lanjut_file_pulang_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_pulang_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_pulang_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_pulang'] = $studi_lanjut_file_pulang_name_copy;
			}

			if (!empty($studi_lanjut_file_bebas_bealain_uuid)) {
				$studi_lanjut_file_bebas_bealain_name_copy = date('YmdHis') . '-' . $studi_lanjut_file_bebas_bealain_name;

				rename(
					FCPATH . 'uploads/tmp/' . $studi_lanjut_file_bebas_bealain_uuid . '/' . $studi_lanjut_file_bebas_bealain_name,
					FCPATH . 'uploads/studi_lanjut/' . $studi_lanjut_file_bebas_bealain_name_copy
				);

				if (!is_file(FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut_file_bebas_bealain_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_bebas_bealain'] = $studi_lanjut_file_bebas_bealain_name_copy;
			}


			$save_studi_lanjut = $this->model_studi_lanjut->change($id, $save_data);

			if ($save_studi_lanjut) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/studi_lanjut', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/studi_lanjut/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/studi_lanjut/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Studi Lanjuts
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('studi_lanjut_delete');

		if (is_groups_in(['Dosen', 'Tendik'])) {
			$studi	= $this->model_studi_lanjut->find($id);
			if ($studi->nip == fromsess('identitas')) {
				if ($studi->validasi == 'Y') {
					set_message(cclang('Gagal hapus, karena data sudah divalidasi', 'studi_lanjut'), 'warning');
					redirect_back();
				}
			} else {
				set_message(cclang('sorry_you_do_not_have_permission_to_access', 'studi_lanjut'), 'error');
				redirect_back();
			}
		}

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'studi_lanjut'), 'success');
		} else {
			set_message(cclang('error_delete', 'studi_lanjut'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Studi Lanjuts
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('studi_lanjut_view');

		$this->data['studi_lanjut'] = $this->model_studi_lanjut->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Studi Lanjut Detail');
		$this->render('backend/standart/administrator/studi_lanjut/studi_lanjut_view', $this->data);
	}

	/**
	 * delete Studi Lanjuts
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$studi_lanjut = $this->model_studi_lanjut->find($id);

		//file pengantar
		if (!empty($studi_lanjut->file_pengantar)) {
			$path = FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut->file_pengantar;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}

		if (!empty($studi_lanjut->file_loa)) {
			$path = FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut->file_loa;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}

		if (!empty($studi_lanjut->file_ijin_belajar)) {
			$path = FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut->file_ijin_belajar;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($studi_lanjut->file_biaya)) {
			$path = FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut->file_biaya;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($studi_lanjut->file_jadwal_kuliah)) {
			$path = FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut->file_jadwal_kuliah;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($studi_lanjut->file_drh)) {
			$path = FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut->file_drh;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($studi_lanjut->file_ktp)) {
			$path = FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut->file_ktp;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($studi_lanjut->file_sk_pns)) {
			$path = FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut->file_sk_pns;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($studi_lanjut->file_karpeg)) {
			$path = FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut->file_karpeg;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($studi_lanjut->file_pulang)) {
			$path = FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut->file_pulang;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($studi_lanjut->file_bebas_bealain)) {
			$path = FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut->file_bebas_bealain;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		//tambahan
		$studi_lanjut = $this->model_studi_lanjut->find($id);

		if (!empty($studi_lanjut->file_bebas_cov19)) {
			$path = FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut->file_bebas_cov19;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($studi_lanjut->file_ket_duta)) {
			$path = FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut->file_ket_duta;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($studi_lanjut->file_ket_univ_tujuan)) {
			$path = FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut->file_ket_univ_tujuan;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($studi_lanjut->file_paspor)) {
			$path = FCPATH . '/uploads/studi_lanjut/' . $studi_lanjut->file_paspor;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}


		return $this->model_studi_lanjut->remove($id);
	}

	//file pengantar
	/**
	 * Upload Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function upload_file_pengantar_file()
	{
		if (!$this->is_allowed('studi_lanjut_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'studi_lanjut',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function delete_file_pengantar_file($uuid)
	{
		if (!$this->is_allowed('studi_lanjut_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_pengantar',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/'
		]);
	}

	/**
	 * Get Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function get_file_pengantar_file($id)
	{
		if (!$this->is_allowed('studi_lanjut_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$studi_lanjut = $this->model_studi_lanjut->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_pengantar',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/',
			'delete_endpoint'   => 'administrator/studi_lanjut/delete_file_pengantar_file'
		]);
	}

	/**
	 * tambahan upload covid19
	 */
	/**
	 * Upload Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function upload_file_bebas_cov19_file()
	{
		if (!$this->is_allowed('studi_lanjut_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'studi_lanjut',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function delete_file_bebas_cov19_file($uuid)
	{
		if (!$this->is_allowed('studi_lanjut_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_bebas_cov19',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/'
		]);
	}

	/**
	 * Get Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function get_file_bebas_cov19_file($id)
	{
		if (!$this->is_allowed('studi_lanjut_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$studi_lanjut = $this->model_studi_lanjut->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_bebas_cov19',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/',
			'delete_endpoint'   => 'administrator/studi_lanjut/delete_file_bebas_cov19_file'
		]);
	}

	/**
	 * Upload Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function upload_file_ket_duta_file()
	{
		if (!$this->is_allowed('studi_lanjut_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'studi_lanjut',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function delete_file_ket_duta_file($uuid)
	{
		if (!$this->is_allowed('studi_lanjut_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ket_duta',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/'
		]);
	}

	/**
	 * Get Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function get_file_ket_duta_file($id)
	{
		if (!$this->is_allowed('studi_lanjut_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$studi_lanjut = $this->model_studi_lanjut->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ket_duta',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/',
			'delete_endpoint'   => 'administrator/studi_lanjut/delete_file_ket_duta_file'
		]);
	}

	/**
	 * Upload Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function upload_file_ket_univ_tujuan_file()
	{
		if (!$this->is_allowed('studi_lanjut_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'studi_lanjut',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function delete_file_ket_univ_tujuan_file($uuid)
	{
		if (!$this->is_allowed('studi_lanjut_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ket_univ_tujuan',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/'
		]);
	}

	/**
	 * Get Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function get_file_ket_univ_tujuan_file($id)
	{
		if (!$this->is_allowed('studi_lanjut_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$studi_lanjut = $this->model_studi_lanjut->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ket_univ_tujuan',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/',
			'delete_endpoint'   => 'administrator/studi_lanjut/delete_file_ket_univ_tujuan_file'
		]);
	}

	/**
	 * Upload Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function upload_file_paspor_file()
	{
		if (!$this->is_allowed('studi_lanjut_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'studi_lanjut',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function delete_file_paspor_file($uuid)
	{
		if (!$this->is_allowed('studi_lanjut_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_paspor',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/'
		]);
	}

	/**
	 * Get Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function get_file_paspor_file($id)
	{
		if (!$this->is_allowed('studi_lanjut_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$studi_lanjut = $this->model_studi_lanjut->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_paspor',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/',
			'delete_endpoint'   => 'administrator/studi_lanjut/delete_file_paspor_file'
		]);
	}
	// ---- tambahan upload covid19

	/**
	 * Upload Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function upload_file_loa_file()
	{
		if (!$this->is_allowed('studi_lanjut_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'studi_lanjut',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function delete_file_loa_file($uuid)
	{
		if (!$this->is_allowed('studi_lanjut_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_loa',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/'
		]);
	}

	/**
	 * Get Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function get_file_loa_file($id)
	{
		if (!$this->is_allowed('studi_lanjut_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$studi_lanjut = $this->model_studi_lanjut->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_loa',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/',
			'delete_endpoint'   => 'administrator/studi_lanjut/delete_file_loa_file'
		]);
	}

	/**
	 * Upload Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function upload_file_ijin_belajar_file()
	{
		if (!$this->is_allowed('studi_lanjut_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'studi_lanjut',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function delete_file_ijin_belajar_file($uuid)
	{
		if (!$this->is_allowed('studi_lanjut_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ijin_belajar',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/'
		]);
	}

	/**
	 * Get Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function get_file_ijin_belajar_file($id)
	{
		if (!$this->is_allowed('studi_lanjut_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$studi_lanjut = $this->model_studi_lanjut->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ijin_belajar',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/',
			'delete_endpoint'   => 'administrator/studi_lanjut/delete_file_ijin_belajar_file'
		]);
	}

	/**
	 * Upload Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function upload_file_biaya_file()
	{
		if (!$this->is_allowed('studi_lanjut_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'studi_lanjut',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function delete_file_biaya_file($uuid)
	{
		if (!$this->is_allowed('studi_lanjut_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_biaya',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/'
		]);
	}

	/**
	 * Get Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function get_file_biaya_file($id)
	{
		if (!$this->is_allowed('studi_lanjut_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$studi_lanjut = $this->model_studi_lanjut->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_biaya',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/',
			'delete_endpoint'   => 'administrator/studi_lanjut/delete_file_biaya_file'
		]);
	}

	/**
	 * Upload Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function upload_file_jadwal_kuliah_file()
	{
		if (!$this->is_allowed('studi_lanjut_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'studi_lanjut',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function delete_file_jadwal_kuliah_file($uuid)
	{
		if (!$this->is_allowed('studi_lanjut_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_jadwal_kuliah',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/'
		]);
	}

	/**
	 * Get Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function get_file_jadwal_kuliah_file($id)
	{
		if (!$this->is_allowed('studi_lanjut_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$studi_lanjut = $this->model_studi_lanjut->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_jadwal_kuliah',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/',
			'delete_endpoint'   => 'administrator/studi_lanjut/delete_file_jadwal_kuliah_file'
		]);
	}

	/**
	 * Upload Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function upload_file_drh_file()
	{
		if (!$this->is_allowed('studi_lanjut_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'studi_lanjut',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function delete_file_drh_file($uuid)
	{
		if (!$this->is_allowed('studi_lanjut_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_drh',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/'
		]);
	}

	/**
	 * Get Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function get_file_drh_file($id)
	{
		if (!$this->is_allowed('studi_lanjut_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$studi_lanjut = $this->model_studi_lanjut->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_drh',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/',
			'delete_endpoint'   => 'administrator/studi_lanjut/delete_file_drh_file'
		]);
	}

	/**
	 * Upload Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function upload_file_ktp_file()
	{
		if (!$this->is_allowed('studi_lanjut_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'studi_lanjut',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function delete_file_ktp_file($uuid)
	{
		if (!$this->is_allowed('studi_lanjut_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ktp',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/'
		]);
	}

	/**
	 * Get Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function get_file_ktp_file($id)
	{
		if (!$this->is_allowed('studi_lanjut_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$studi_lanjut = $this->model_studi_lanjut->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ktp',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/',
			'delete_endpoint'   => 'administrator/studi_lanjut/delete_file_ktp_file'
		]);
	}

	/**
	 * Upload Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function upload_file_sk_pns_file()
	{
		if (!$this->is_allowed('studi_lanjut_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'studi_lanjut',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function delete_file_sk_pns_file($uuid)
	{
		if (!$this->is_allowed('studi_lanjut_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_sk_pns',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/'
		]);
	}

	/**
	 * Get Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function get_file_sk_pns_file($id)
	{
		if (!$this->is_allowed('studi_lanjut_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$studi_lanjut = $this->model_studi_lanjut->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_sk_pns',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/',
			'delete_endpoint'   => 'administrator/studi_lanjut/delete_file_sk_pns_file'
		]);
	}

	/**
	 * Upload Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function upload_file_karpeg_file()
	{
		if (!$this->is_allowed('studi_lanjut_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'studi_lanjut',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function delete_file_karpeg_file($uuid)
	{
		if (!$this->is_allowed('studi_lanjut_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_karpeg',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/'
		]);
	}

	/**
	 * Get Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function get_file_karpeg_file($id)
	{
		if (!$this->is_allowed('studi_lanjut_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$studi_lanjut = $this->model_studi_lanjut->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_karpeg',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/',
			'delete_endpoint'   => 'administrator/studi_lanjut/delete_file_karpeg_file'
		]);
	}

	/**
	 * Upload Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function upload_file_pulang_file()
	{
		if (!$this->is_allowed('studi_lanjut_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'studi_lanjut',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function delete_file_pulang_file($uuid)
	{
		if (!$this->is_allowed('studi_lanjut_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_pulang',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/'
		]);
	}

	/**
	 * Get Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function get_file_pulang_file($id)
	{
		if (!$this->is_allowed('studi_lanjut_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$studi_lanjut = $this->model_studi_lanjut->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_pulang',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/',
			'delete_endpoint'   => 'administrator/studi_lanjut/delete_file_pulang_file'
		]);
	}

	/**
	 * Upload Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function upload_file_bebas_bealain_file()
	{
		if (!$this->is_allowed('studi_lanjut_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'studi_lanjut',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function delete_file_bebas_bealain_file($uuid)
	{
		if (!$this->is_allowed('studi_lanjut_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_bebas_bealain',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/'
		]);
	}

	/**
	 * Get Image Studi Lanjut	* 
	 * @return JSON
	 */
	public function get_file_bebas_bealain_file($id)
	{
		if (!$this->is_allowed('studi_lanjut_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$studi_lanjut = $this->model_studi_lanjut->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_bebas_bealain',
			'table_name'        => 'studi_lanjut',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/studi_lanjut/',
			'delete_endpoint'   => 'administrator/studi_lanjut/delete_file_bebas_bealain_file'
		]);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('studi_lanjut_export');

		$this->model_studi_lanjut->export(
			'studi_lanjut',
			'studi_lanjut',
			$this->model_studi_lanjut->field_search
		);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('studi_lanjut_export');

		$this->model_studi_lanjut->pdf('studi_lanjut', 'studi_lanjut');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('studi_lanjut_export');

		$table = $title = 'studi_lanjut';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_studi_lanjut->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
}


/* End of file studi_lanjut.php */
/* Location: ./application/controllers/administrator/Studi Lanjut.php */