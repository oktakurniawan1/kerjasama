<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Studi Lanjut <small><?= cclang('new', ['Studi Lanjut']); ?> </small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/studi_lanjut'); ?>">Studi Lanjut</a></li>
		<li class="active"><?= cclang('new'); ?></li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Studi Lanjut</h3>
							<h5 class="widget-user-desc"><?= cclang('new', ['Studi Lanjut']); ?></h5>
							<hr>
						</div>
						<?= form_open('', [
							'name'    => 'form_studi_lanjut',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_studi_lanjut',
							'enctype' => 'multipart/form-data',
							'method'  => 'POST'
						]); ?>

						<div class="form-group ">
							<label for="identitas" class="col-sm-2 control-label">NIP/NRP
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<?php if (is_groups_in(['Dosen', 'Tendik'])) :
									if (is_non_user()) : $nip_baru = fromsess('nip_baru');
									else : $nip_baru = get_user_data('username');
									endif; ?>
									<input type="text" class="form-control" name="nip" id="nip" placeholder="NIP/NRP" value="<?= $nip_baru ?> " readonly>
								<?php else : ?>
									<input type="text" class="form-control" name="nip" id="nip" placeholder="NIP/NRP" value="<?= set_value('nip'); ?>">
									<button style='margin-top:10px' class="btn btn-primary btn-sm" type="button" onclick="cari_pegawai()">Cari di Simpeg</button>
								<?php endif; ?>
								<small class="info help-block">
									<b>Input Identitas NIP/NRP</b> Max Length : 20.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nama" class="col-sm-2 control-label">Nama
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<?php if (is_groups_in(['Dosen', 'Tendik'])) :
									if (is_non_user()) : $nama = fromsess('nama');
									else : $nama = get_user_data('full_name');
									endif; ?>
									<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?= $nama; ?>">
								<?php else : ?>
									<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?= set_value('nama'); ?>">
								<?php endif; ?>
								<small class="info help-block">
									<b>Input Nama</b> Max Length : 255.</small>
							</div>
						</div>

						<?php
						if (is_groups_in(['Dosen', 'Tendik'])) :
							if (is_non_user()) : $kd_prodi = fromsess('kd_prodi');
							else : $kd_prodi = '';
							endif;
						else : $kd_prodi = '';
						endif;
						?>

						<div class="form-group ">
							<label for="kd_prodi" class="col-sm-2 control-label">Program studi/Unit
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="kd_prodi" id="kd_prodi" data-placeholder="Program studi/Unit">
									<option value=""></option>
									<?php foreach ($filter_prodi as $row) : ?>
										<option <?= $row->kode ==  $kd_prodi ? 'selected' : ''; ?> value="<?= $row->kode ?>"><?= $row->kode .' - '.$row->nama_prodi; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									Pilih<b> Program studi/Unit</b></small>
							</div>
						</div>

						<div class="form-group ">
							<label for="univ_tujuan" class="col-sm-2 control-label">Universitas Tujuan
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="univ_tujuan" id="univ_tujuan" placeholder="Universitas Tujuan" value="<?= set_value('univ_tujuan'); ?>">
								<small class="info help-block">
									<b>Input Univ Tujuan</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="prodi_tujuan" class="col-sm-2 control-label">Program Studi Tujuan
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="prodi_tujuan" id="prodi_tujuan" placeholder="Program Studi Tujuan" value="<?= set_value('prodi_tujuan'); ?>">
								<small class="info help-block">
									<b>Input Prodi Tujuan</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="negara_tujuan" class="col-sm-2 control-label">Negara Tujuan
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="negara" id="negara" data-placeholder="Select Negara Tujuan">
									<option value=""></option>
									<?php foreach (db_get_all_data('negara') as $row) : ?>
										<option value="<?= $row->nama_negara ?>"><?= $row->nama_negara; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Input Negara Tujuan</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_mulai" class="col-sm-2 control-label">Tanggal Mulai
								<i class="required">*</i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-8">
									<input type="text" class="form-control pull-right datepicker" name="tgl_mulai" placeholder="Tanggal Mulai" id="tgl_mulai">
								</div>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_selesai" class="col-sm-2 control-label">Tanggal Selesai
								<i class="required">*</i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-8">
									<input type="text" class="form-control pull-right datepicker" name="tgl_selesai" placeholder="Tanggal Selesai" id="tgl_selesai">
								</div>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_loa" class="col-sm-2 control-label">File LOA
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="studi_lanjut_file_loa_galery"></div>
								<input class="data_file" name="studi_lanjut_file_loa_uuid" id="studi_lanjut_file_loa_uuid" type="hidden" value="<?= set_value('studi_lanjut_file_loa_uuid'); ?>">
								<input class="data_file" name="studi_lanjut_file_loa_name" id="studi_lanjut_file_loa_name" type="hidden" value="<?= set_value('studi_lanjut_file_loa_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_pengantar" class="col-sm-2 control-label">File Pengantar (Fakultas/Unit)
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="studi_lanjut_file_pengantar_galery"></div>
								<input class="data_file" name="studi_lanjut_file_pengantar_uuid" id="studi_lanjut_file_pengantar_uuid" type="hidden" value="<?= set_value('studi_lanjut_file_pengantar_uuid'); ?>">
								<input class="data_file" name="studi_lanjut_file_pengantar_name" id="studi_lanjut_file_pengantar_name" type="hidden" value="<?= set_value('studi_lanjut_file_pengantar_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_ijin_belajar" class="col-sm-2 control-label">File Surat Perjanjian Tugas/Ijin Belajar
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="studi_lanjut_file_ijin_belajar_galery"></div>
								<input class="data_file" name="studi_lanjut_file_ijin_belajar_uuid" id="studi_lanjut_file_ijin_belajar_uuid" type="hidden" value="<?= set_value('studi_lanjut_file_ijin_belajar_uuid'); ?>">
								<input class="data_file" name="studi_lanjut_file_ijin_belajar_name" id="studi_lanjut_file_ijin_belajar_name" type="hidden" value="<?= set_value('studi_lanjut_file_ijin_belajar_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_biaya" class="col-sm-2 control-label">File Keterangan Biaya
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="studi_lanjut_file_biaya_galery"></div>
								<input class="data_file" name="studi_lanjut_file_biaya_uuid" id="studi_lanjut_file_biaya_uuid" type="hidden" value="<?= set_value('studi_lanjut_file_biaya_uuid'); ?>">
								<input class="data_file" name="studi_lanjut_file_biaya_name" id="studi_lanjut_file_biaya_name" type="hidden" value="<?= set_value('studi_lanjut_file_biaya_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_jadwal_kuliah" class="col-sm-2 control-label">File Jadwal Kuliah
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="studi_lanjut_file_jadwal_kuliah_galery"></div>
								<input class="data_file" name="studi_lanjut_file_jadwal_kuliah_uuid" id="studi_lanjut_file_jadwal_kuliah_uuid" type="hidden" value="<?= set_value('studi_lanjut_file_jadwal_kuliah_uuid'); ?>">
								<input class="data_file" name="studi_lanjut_file_jadwal_kuliah_name" id="studi_lanjut_file_jadwal_kuliah_name" type="hidden" value="<?= set_value('studi_lanjut_file_jadwal_kuliah_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_drh" class="col-sm-2 control-label">File DRH
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="studi_lanjut_file_drh_galery"></div>
								<input class="data_file" name="studi_lanjut_file_drh_uuid" id="studi_lanjut_file_drh_uuid" type="hidden" value="<?= set_value('studi_lanjut_file_drh_uuid'); ?>">
								<input class="data_file" name="studi_lanjut_file_drh_name" id="studi_lanjut_file_drh_name" type="hidden" value="<?= set_value('studi_lanjut_file_drh_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_ktp" class="col-sm-2 control-label">File KTP/NIK
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="studi_lanjut_file_ktp_galery"></div>
								<input class="data_file" name="studi_lanjut_file_ktp_uuid" id="studi_lanjut_file_ktp_uuid" type="hidden" value="<?= set_value('studi_lanjut_file_ktp_uuid'); ?>">
								<input class="data_file" name="studi_lanjut_file_ktp_name" id="studi_lanjut_file_ktp_name" type="hidden" value="<?= set_value('studi_lanjut_file_ktp_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_sk_pns" class="col-sm-2 control-label">File SK PNS
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="studi_lanjut_file_sk_pns_galery"></div>
								<input class="data_file" name="studi_lanjut_file_sk_pns_uuid" id="studi_lanjut_file_sk_pns_uuid" type="hidden" value="<?= set_value('studi_lanjut_file_sk_pns_uuid'); ?>">
								<input class="data_file" name="studi_lanjut_file_sk_pns_name" id="studi_lanjut_file_sk_pns_name" type="hidden" value="<?= set_value('studi_lanjut_file_sk_pns_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_karpeg" class="col-sm-2 control-label">File Kartu Pegawai
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="studi_lanjut_file_karpeg_galery"></div>
								<input class="data_file" name="studi_lanjut_file_karpeg_uuid" id="studi_lanjut_file_karpeg_uuid" type="hidden" value="<?= set_value('studi_lanjut_file_karpeg_uuid'); ?>">
								<input class="data_file" name="studi_lanjut_file_karpeg_name" id="studi_lanjut_file_karpeg_name" type="hidden" value="<?= set_value('studi_lanjut_file_karpeg_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_pulang" class="col-sm-2 control-label">File Keterangan Kembali
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="studi_lanjut_file_pulang_galery"></div>
								<input class="data_file" name="studi_lanjut_file_pulang_uuid" id="studi_lanjut_file_pulang_uuid" type="hidden" value="<?= set_value('studi_lanjut_file_pulang_uuid'); ?>">
								<input class="data_file" name="studi_lanjut_file_pulang_name" id="studi_lanjut_file_pulang_name" type="hidden" value="<?= set_value('studi_lanjut_file_pulang_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_bebas_bealain" class="col-sm-2 control-label">File Bebas Biasiswa Lain
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="studi_lanjut_file_bebas_bealain_galery"></div>
								<input class="data_file" name="studi_lanjut_file_bebas_bealain_uuid" id="studi_lanjut_file_bebas_bealain_uuid" type="hidden" value="<?= set_value('studi_lanjut_file_bebas_bealain_uuid'); ?>">
								<input class="data_file" name="studi_lanjut_file_bebas_bealain_name" id="studi_lanjut_file_bebas_bealain_name" type="hidden" value="<?= set_value('studi_lanjut_file_bebas_bealain_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>
						<!-- tambahan -->

						<div class="form-group ">
							<label for="file_bebas_cov19" class="col-sm-2 control-label">File Keterangan Bebas Covid-19
								<!-- <i class="required">*</i> -->
							</label>
							<div class="col-sm-8">
								<div id="studi_lanjut_file_bebas_cov19_galery"></div>
								<input class="data_file" name="studi_lanjut_file_bebas_cov19_uuid" id="studi_lanjut_file_bebas_cov19_uuid" type="hidden" value="<?= set_value('studi_lanjut_file_bebas_cov19_uuid'); ?>">
								<input class="data_file" name="studi_lanjut_file_bebas_cov19_name" id="studi_lanjut_file_bebas_cov19_name" type="hidden" value="<?= set_value('studi_lanjut_file_bebas_cov19_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_ket_duta" class="col-sm-2 control-label">File Keterangan dari Kedutaan
								<!-- <i class="required">*</i> -->
							</label>
							<div class="col-sm-8">
								<div id="studi_lanjut_file_ket_duta_galery"></div>
								<input class="data_file" name="studi_lanjut_file_ket_duta_uuid" id="studi_lanjut_file_ket_duta_uuid" type="hidden" value="<?= set_value('studi_lanjut_file_ket_duta_uuid'); ?>">
								<input class="data_file" name="studi_lanjut_file_ket_duta_name" id="studi_lanjut_file_ket_duta_name" type="hidden" value="<?= set_value('studi_lanjut_file_ket_duta_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_ket_univ_tujuan" class="col-sm-2 control-label">File Keterangan Universitas Tujuan
								<!-- <i class="required">*</i> -->
							</label>
							<div class="col-sm-8">
								<div id="studi_lanjut_file_ket_univ_tujuan_galery"></div>
								<input class="data_file" name="studi_lanjut_file_ket_univ_tujuan_uuid" id="studi_lanjut_file_ket_univ_tujuan_uuid" type="hidden" value="<?= set_value('studi_lanjut_file_ket_univ_tujuan_uuid'); ?>">
								<input class="data_file" name="studi_lanjut_file_ket_univ_tujuan_name" id="studi_lanjut_file_ket_univ_tujuan_name" type="hidden" value="<?= set_value('studi_lanjut_file_ket_univ_tujuan_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="file_paspor" class="col-sm-2 control-label">File Paspor
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<div id="studi_lanjut_file_paspor_galery"></div>
								<input class="data_file" name="studi_lanjut_file_paspor_uuid" id="studi_lanjut_file_paspor_uuid" type="hidden" value="<?= set_value('studi_lanjut_file_paspor_uuid'); ?>">
								<input class="data_file" name="studi_lanjut_file_paspor_name" id="studi_lanjut_file_paspor_name" type="hidden" value="<?= set_value('studi_lanjut_file_paspor_name'); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
							</div>
						</div>
						<!-- tambahan  -->


						<div class="form-group ">
							<label for="catatan" class="col-sm-2 control-label">Catatan
							</label>
							<div class="col-sm-8">
								<textarea id="catatan" name="catatan" rows="5" class="textarea form-control"><?= set_value('catatan'); ?></textarea>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<?php if (is_groups_in(['Admin', 'Kerjasama'])) : ?>
							<div class="form-group ">
								<label for="validasi" class="col-sm-2 control-label">Validasi
								</label>
								<div class="col-sm-8">
									<select class="form-control chosen chosen-select" name="validasi" id="validasi" data-placeholder="Select Validasi">
										<option value=""></option>
										<option value="T">Invalid</option>
										<option value="Y">Valid</option>
									</select>
									<small class="info help-block">
									</small>
								</div>
							</div>
						<?php endif; ?>


						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	$(document).ready(function() {


		$('#btn_cancel').click(function() {
			swal({
					title: "<?= cclang('are_you_sure'); ?>",
					text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/studi_lanjut';
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();

			var form_studi_lanjut = $('#form_studi_lanjut');
			var data_post = form_studi_lanjut.serializeArray();
			var save_type = $(this).attr('data-stype');

			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: BASE_URL + '/administrator/studi_lanjut/add_save',
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('.steps li').removeClass('error');
					$('form').find('.error-input').remove();
					if (res.success) {
						var id_file_pengantar = $('#studi_lanjut_file_pengantar_galery').find('li').attr('qq-file-id');
						var id_file_loa = $('#studi_lanjut_file_loa_galery').find('li').attr('qq-file-id');
						var id_file_ijin_belajar = $('#studi_lanjut_file_ijin_belajar_galery').find('li').attr('qq-file-id');
						var id_file_biaya = $('#studi_lanjut_file_biaya_galery').find('li').attr('qq-file-id');
						var id_file_jadwal_kuliah = $('#studi_lanjut_file_jadwal_kuliah_galery').find('li').attr('qq-file-id');
						var id_file_drh = $('#studi_lanjut_file_drh_galery').find('li').attr('qq-file-id');
						var id_file_ktp = $('#studi_lanjut_file_ktp_galery').find('li').attr('qq-file-id');
						var id_file_sk_pns = $('#studi_lanjut_file_sk_pns_galery').find('li').attr('qq-file-id');
						var id_file_karpeg = $('#studi_lanjut_file_karpeg_galery').find('li').attr('qq-file-id');
						var id_file_pulang = $('#studi_lanjut_file_pulang_galery').find('li').attr('qq-file-id');
						var id_file_bebas_bealain = $('#studi_lanjut_file_bebas_bealain_galery').find('li').attr('qq-file-id');
						//tambahan
						var id_file_bebas_cov19 = $('#studi_lanjut_file_bebas_cov19_galery').find('li').attr('qq-file-id');
						var id_file_ket_duta = $('#studi_lanjut_file_ket_duta_galery').find('li').attr('qq-file-id');
						var id_file_ket_univ_tujuan = $('#studi_lanjut_file_ket_univ_tujuan_galery').find('li').attr('qq-file-id');
						var id_file_paspor = $('#studi_lanjut_file_paspor_galery').find('li').attr('qq-file-id');

						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						resetForm();
						if (typeof id_file_pengantar !== 'undefined') {
							$('#studi_lanjut_file_pengantar_galery').fineUploader('deleteFile', id_file_pengantar);
						}
						if (typeof id_file_loa !== 'undefined') {
							$('#studi_lanjut_file_loa_galery').fineUploader('deleteFile', id_file_loa);
						}
						if (typeof id_file_ijin_belajar !== 'undefined') {
							$('#studi_lanjut_file_ijin_belajar_galery').fineUploader('deleteFile', id_file_ijin_belajar);
						}
						if (typeof id_file_biaya !== 'undefined') {
							$('#studi_lanjut_file_biaya_galery').fineUploader('deleteFile', id_file_biaya);
						}
						if (typeof id_file_jadwal_kuliah !== 'undefined') {
							$('#studi_lanjut_file_jadwal_kuliah_galery').fineUploader('deleteFile', id_file_jadwal_kuliah);
						}
						if (typeof id_file_drh !== 'undefined') {
							$('#studi_lanjut_file_drh_galery').fineUploader('deleteFile', id_file_drh);
						}
						if (typeof id_file_ktp !== 'undefined') {
							$('#studi_lanjut_file_ktp_galery').fineUploader('deleteFile', id_file_ktp);
						}
						if (typeof id_file_sk_pns !== 'undefined') {
							$('#studi_lanjut_file_sk_pns_galery').fineUploader('deleteFile', id_file_sk_pns);
						}
						if (typeof id_file_karpeg !== 'undefined') {
							$('#studi_lanjut_file_karpeg_galery').fineUploader('deleteFile', id_file_karpeg);
						}
						if (typeof id_file_pulang !== 'undefined') {
							$('#studi_lanjut_file_pulang_galery').fineUploader('deleteFile', id_file_pulang);
						}
						if (typeof id_file_bebas_bealain !== 'undefined') {
							$('#studi_lanjut_file_bebas_bealain_galery').fineUploader('deleteFile', id_file_bebas_bealain);
						}
						//tambahan
						if (typeof id_file_bebas_cov19 !== 'undefined') {
							$('#studi_lanjut_file_bebas_cov19_galery').fineUploader('deleteFile', id_file_bebas_cov19);
						}
						if (typeof id_file_ket_duta !== 'undefined') {
							$('#studi_lanjut_file_ket_duta_galery').fineUploader('deleteFile', id_file_ket_duta);
						}
						if (typeof id_file_ket_univ_tujuan !== 'undefined') {
							$('#studi_lanjut_file_ket_univ_tujuan_galery').fineUploader('deleteFile', id_file_ket_univ_tujuan);
						}
						if (typeof id_file_paspor !== 'undefined') {
							$('#studi_lanjut_file_paspor_galery').fineUploader('deleteFile', id_file_paspor);
						}
						//---tambahan
						$('.chosen option').prop('selected', false).trigger('chosen:updated');

					} else {
						if (res.errors) {

							$.each(res.errors, function(index, val) {
								$('form #' + index).parents('.form-group').addClass('has-error');
								$('form #' + index).parents('.form-group').find('small').prepend(`
                      <div class="error-input">` + val + `</div>
                      `);
							});
							$('.steps li').removeClass('error');
							$('.content section').each(function(index, el) {
								if ($(this).find('.has-error').length) {
									$('.steps li:eq(' + index + ')').addClass('error').find('a').trigger('click');
								}
							});
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/

		//file pengantar
		var params = {};
		params[csrf] = token;

		$('#studi_lanjut_file_pengantar_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/studi_lanjut/upload_file_pengantar_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/studi_lanjut/delete_file_pengantar_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#studi_lanjut_file_pengantar_galery').fineUploader('getUuid', id);
						$('#studi_lanjut_file_pengantar_uuid').val(uuid);
						$('#studi_lanjut_file_pengantar_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#studi_lanjut_file_pengantar_uuid').val();
					$.get(BASE_URL + '/administrator/studi_lanjut/delete_file_pengantar_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#studi_lanjut_file_pengantar_uuid').val('');
						$('#studi_lanjut_file_pengantar_name').val('');
					}
				}
			}
		}); /*end file_pengantar galery*/

		//tambahan

		var params = {};
		params[csrf] = token;

		$('#studi_lanjut_file_bebas_cov19_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/studi_lanjut/upload_file_bebas_cov19_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/studi_lanjut/delete_file_bebas_cov19_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#studi_lanjut_file_bebas_cov19_galery').fineUploader('getUuid', id);
						$('#studi_lanjut_file_bebas_cov19_uuid').val(uuid);
						$('#studi_lanjut_file_bebas_cov19_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#studi_lanjut_file_bebas_cov19_uuid').val();
					$.get(BASE_URL + '/administrator/studi_lanjut/delete_file_bebas_cov19_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#studi_lanjut_file_bebas_cov19_uuid').val('');
						$('#studi_lanjut_file_bebas_cov19_name').val('');
					}
				}
			}
		}); /*end file_bebas_cov19 galery*/
		var params = {};
		params[csrf] = token;

		$('#studi_lanjut_file_ket_duta_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/studi_lanjut/upload_file_ket_duta_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/studi_lanjut/delete_file_ket_duta_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#studi_lanjut_file_ket_duta_galery').fineUploader('getUuid', id);
						$('#studi_lanjut_file_ket_duta_uuid').val(uuid);
						$('#studi_lanjut_file_ket_duta_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#studi_lanjut_file_ket_duta_uuid').val();
					$.get(BASE_URL + '/administrator/studi_lanjut/delete_file_ket_duta_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#studi_lanjut_file_ket_duta_uuid').val('');
						$('#studi_lanjut_file_ket_duta_name').val('');
					}
				}
			}
		}); /*end file_ket_duta galery*/
		var params = {};
		params[csrf] = token;

		$('#studi_lanjut_file_ket_univ_tujuan_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/studi_lanjut/upload_file_ket_univ_tujuan_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/studi_lanjut/delete_file_ket_univ_tujuan_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#studi_lanjut_file_ket_univ_tujuan_galery').fineUploader('getUuid', id);
						$('#studi_lanjut_file_ket_univ_tujuan_uuid').val(uuid);
						$('#studi_lanjut_file_ket_univ_tujuan_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#studi_lanjut_file_ket_univ_tujuan_uuid').val();
					$.get(BASE_URL + '/administrator/studi_lanjut/delete_file_ket_univ_tujuan_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#studi_lanjut_file_ket_univ_tujuan_uuid').val('');
						$('#studi_lanjut_file_ket_univ_tujuan_name').val('');
					}
				}
			}
		}); /*end file_ket_univ_tujuan galery*/
		var params = {};
		params[csrf] = token;

		$('#studi_lanjut_file_paspor_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/studi_lanjut/upload_file_paspor_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/studi_lanjut/delete_file_paspor_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#studi_lanjut_file_paspor_galery').fineUploader('getUuid', id);
						$('#studi_lanjut_file_paspor_uuid').val(uuid);
						$('#studi_lanjut_file_paspor_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#studi_lanjut_file_paspor_uuid').val();
					$.get(BASE_URL + '/administrator/studi_lanjut/delete_file_paspor_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#studi_lanjut_file_paspor_uuid').val('');
						$('#studi_lanjut_file_paspor_name').val('');
					}
				}
			}
		}); /*end file_paspor galery*/
		//----tambahan

		var params = {};
		params[csrf] = token;

		$('#studi_lanjut_file_loa_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/studi_lanjut/upload_file_loa_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/studi_lanjut/delete_file_loa_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#studi_lanjut_file_loa_galery').fineUploader('getUuid', id);
						$('#studi_lanjut_file_loa_uuid').val(uuid);
						$('#studi_lanjut_file_loa_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#studi_lanjut_file_loa_uuid').val();
					$.get(BASE_URL + '/administrator/studi_lanjut/delete_file_loa_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#studi_lanjut_file_loa_uuid').val('');
						$('#studi_lanjut_file_loa_name').val('');
					}
				}
			}
		}); /*end file_loa galery*/
		var params = {};
		params[csrf] = token;

		$('#studi_lanjut_file_ijin_belajar_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/studi_lanjut/upload_file_ijin_belajar_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/studi_lanjut/delete_file_ijin_belajar_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#studi_lanjut_file_ijin_belajar_galery').fineUploader('getUuid', id);
						$('#studi_lanjut_file_ijin_belajar_uuid').val(uuid);
						$('#studi_lanjut_file_ijin_belajar_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#studi_lanjut_file_ijin_belajar_uuid').val();
					$.get(BASE_URL + '/administrator/studi_lanjut/delete_file_ijin_belajar_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#studi_lanjut_file_ijin_belajar_uuid').val('');
						$('#studi_lanjut_file_ijin_belajar_name').val('');
					}
				}
			}
		}); /*end file_ijin_belajar galery*/
		var params = {};
		params[csrf] = token;

		$('#studi_lanjut_file_biaya_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/studi_lanjut/upload_file_biaya_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/studi_lanjut/delete_file_biaya_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#studi_lanjut_file_biaya_galery').fineUploader('getUuid', id);
						$('#studi_lanjut_file_biaya_uuid').val(uuid);
						$('#studi_lanjut_file_biaya_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#studi_lanjut_file_biaya_uuid').val();
					$.get(BASE_URL + '/administrator/studi_lanjut/delete_file_biaya_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#studi_lanjut_file_biaya_uuid').val('');
						$('#studi_lanjut_file_biaya_name').val('');
					}
				}
			}
		}); /*end file_biaya galery*/
		var params = {};
		params[csrf] = token;

		$('#studi_lanjut_file_jadwal_kuliah_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/studi_lanjut/upload_file_jadwal_kuliah_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/studi_lanjut/delete_file_jadwal_kuliah_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#studi_lanjut_file_jadwal_kuliah_galery').fineUploader('getUuid', id);
						$('#studi_lanjut_file_jadwal_kuliah_uuid').val(uuid);
						$('#studi_lanjut_file_jadwal_kuliah_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#studi_lanjut_file_jadwal_kuliah_uuid').val();
					$.get(BASE_URL + '/administrator/studi_lanjut/delete_file_jadwal_kuliah_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#studi_lanjut_file_jadwal_kuliah_uuid').val('');
						$('#studi_lanjut_file_jadwal_kuliah_name').val('');
					}
				}
			}
		}); /*end file_jadwal_kuliah galery*/
		var params = {};
		params[csrf] = token;

		$('#studi_lanjut_file_drh_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/studi_lanjut/upload_file_drh_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/studi_lanjut/delete_file_drh_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#studi_lanjut_file_drh_galery').fineUploader('getUuid', id);
						$('#studi_lanjut_file_drh_uuid').val(uuid);
						$('#studi_lanjut_file_drh_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#studi_lanjut_file_drh_uuid').val();
					$.get(BASE_URL + '/administrator/studi_lanjut/delete_file_drh_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#studi_lanjut_file_drh_uuid').val('');
						$('#studi_lanjut_file_drh_name').val('');
					}
				}
			}
		}); /*end file_drh galery*/
		var params = {};
		params[csrf] = token;

		$('#studi_lanjut_file_ktp_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/studi_lanjut/upload_file_ktp_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/studi_lanjut/delete_file_ktp_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#studi_lanjut_file_ktp_galery').fineUploader('getUuid', id);
						$('#studi_lanjut_file_ktp_uuid').val(uuid);
						$('#studi_lanjut_file_ktp_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#studi_lanjut_file_ktp_uuid').val();
					$.get(BASE_URL + '/administrator/studi_lanjut/delete_file_ktp_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#studi_lanjut_file_ktp_uuid').val('');
						$('#studi_lanjut_file_ktp_name').val('');
					}
				}
			}
		}); /*end file_ktp galery*/
		var params = {};
		params[csrf] = token;

		$('#studi_lanjut_file_sk_pns_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/studi_lanjut/upload_file_sk_pns_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/studi_lanjut/delete_file_sk_pns_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#studi_lanjut_file_sk_pns_galery').fineUploader('getUuid', id);
						$('#studi_lanjut_file_sk_pns_uuid').val(uuid);
						$('#studi_lanjut_file_sk_pns_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#studi_lanjut_file_sk_pns_uuid').val();
					$.get(BASE_URL + '/administrator/studi_lanjut/delete_file_sk_pns_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#studi_lanjut_file_sk_pns_uuid').val('');
						$('#studi_lanjut_file_sk_pns_name').val('');
					}
				}
			}
		}); /*end file_sk_pns galery*/
		var params = {};
		params[csrf] = token;

		$('#studi_lanjut_file_karpeg_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/studi_lanjut/upload_file_karpeg_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/studi_lanjut/delete_file_karpeg_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#studi_lanjut_file_karpeg_galery').fineUploader('getUuid', id);
						$('#studi_lanjut_file_karpeg_uuid').val(uuid);
						$('#studi_lanjut_file_karpeg_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#studi_lanjut_file_karpeg_uuid').val();
					$.get(BASE_URL + '/administrator/studi_lanjut/delete_file_karpeg_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#studi_lanjut_file_karpeg_uuid').val('');
						$('#studi_lanjut_file_karpeg_name').val('');
					}
				}
			}
		}); /*end file_karpeg galery*/
		var params = {};
		params[csrf] = token;

		$('#studi_lanjut_file_pulang_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/studi_lanjut/upload_file_pulang_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/studi_lanjut/delete_file_pulang_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#studi_lanjut_file_pulang_galery').fineUploader('getUuid', id);
						$('#studi_lanjut_file_pulang_uuid').val(uuid);
						$('#studi_lanjut_file_pulang_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#studi_lanjut_file_pulang_uuid').val();
					$.get(BASE_URL + '/administrator/studi_lanjut/delete_file_pulang_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#studi_lanjut_file_pulang_uuid').val('');
						$('#studi_lanjut_file_pulang_name').val('');
					}
				}
			}
		}); /*end file_pulang galery*/
		var params = {};
		params[csrf] = token;

		$('#studi_lanjut_file_bebas_bealain_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/studi_lanjut/upload_file_bebas_bealain_file',
				params: params
			},
			deleteFile: {
				enabled: true,
				endpoint: BASE_URL + '/administrator/studi_lanjut/delete_file_bebas_bealain_file',
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#studi_lanjut_file_bebas_bealain_galery').fineUploader('getUuid', id);
						$('#studi_lanjut_file_bebas_bealain_uuid').val(uuid);
						$('#studi_lanjut_file_bebas_bealain_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#studi_lanjut_file_bebas_bealain_uuid').val();
					$.get(BASE_URL + '/administrator/studi_lanjut/delete_file_bebas_bealain_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#studi_lanjut_file_bebas_bealain_uuid').val('');
						$('#studi_lanjut_file_bebas_bealain_name').val('');
					}
				}
			}
		}); /*end file_bebas_bealain galery*/
	}); /*end doc ready*/
	function cari_pegawai() {
		BootstrapDialog.show({
			title: 'Cari data Simpeg',
			draggable: true,
			message: function(dialog) {
				var $message = $('<div></div>');
				$.get('<?= site_url('administrator/pegawai/form_cari_pegawai') ?>', {}, function(response) {
					$message.html(response);
				})
				return $message;
			},
			buttons: [{
				label: 'Ambil',
				icon: 'glyphicon glyphicon-plus',
				cssClass: 'btn-primary',
				action: function(dialog) {
					$('#nip').val($('#pegawai option:selected').data('nip'));
					$('#nama').val($('#pegawai option:selected').data('nama'));
					dialog.close();
				}
			}, {
				label: 'Tutup',
				action: function(dialog) {
					dialog.close();
				}
			}],

		});
	}
</script>