<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['tridarma'] = 'Tridarma SIPP';
$lang['judul'] = 'Judul';
$lang['penyelenggara'] = 'Penyelenggara';
$lang['tempat'] = 'Tempat';
$lang['tgl_mulai'] = 'Tgl Mulai';
$lang['tgl_selesai'] = 'Tgl Selesai';
$lang['thn_pelaksanaan'] = 'Tahun';
$lang['kategori_kegiatan'] = 'Skim Id';
$lang['skim_kegiatan'] = 'Skim Kegiatan';
$lang['sumber_dana'] = 'Sumber Dana';
$lang['total_dana'] = 'Total Dana';
$lang['status_info'] = 'Status Info';
$lang['total_anggota'] = 'Total Anggota';
$lang['nm_seminar'] = 'Nm Seminar';
$lang['status_pemakalah'] = 'Status Pemakalah';
$lang['status_prosiding'] = 'Status Prosiding';
$lang['tgl_terbit'] = 'Tgl Terbit';
$lang['isbn'] = 'Isbn';
$lang['issn'] = 'Issn';
$lang['pengindeks_jurnal'] = 'Pengindeks Jurnal';
$lang['edisi_jurnal'] = 'Edisi Jurnal';
$lang['hal_jurnal'] = 'Hal Jurnal';
$lang['nm_jurnal'] = 'Nm Jurnal';
$lang['no_jurnal'] = 'No Jurnal';
$lang['penerbit'] = 'Penerbit';
$lang['status_submit'] = 'Status Submit';
$lang['url'] = 'Url';
$lang['vol_jurnal'] = 'Vol Jurnal';
$lang['grade_index'] = 'Grade Index';
$lang['nip'] = 'Nip';
$lang['used'] = 'Used';
