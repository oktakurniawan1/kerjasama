<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_tridarma extends MY_Model {

    private $primary_key    = 'id';
    private $table_name     = 'tridarma';
    public $field_search   = ['judul', 'tgl_mulai', 'tgl_selesai', 'thn_pelaksanaan', 'kategori_kegiatan', 'skim_kegiatan', 'status_pemakalah', 'status_prosiding', 'pengindeks_jurnal', 'status_submit', 'nip', 'used'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "tridarma.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "tridarma.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "tridarma.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "tridarma.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "tridarma.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "tridarma.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
        
        $this->sortable();
        
        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        
        $this->db->select('tridarma.*');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }
    public function updateUsed($post)
    {
        //print_r($post);die();
        $val = ($post['val'] == 'true' ? 'Y' : 'N');
        $this->db->where('id',$post['id'])->update('tridarma',['used'=>$val]);
    }

}

/* End of file Model_tridarma.php */
/* Location: ./application/models/Model_tridarma.php */