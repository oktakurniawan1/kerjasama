<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Tridarma Controller
 *| --------------------------------------------------------------------------
 *| Tridarma site
 *|
 */
class Tridarma extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_tridarma');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * show all Tridarmas
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('tridarma_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('tridarma_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('tridarma_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('tridarma_filter');
			$this->session->unset_userdata('tridarma_field');
		}
		$filter = $this->session->userdata('tridarma_filter');
		$field = $this->session->userdata('tridarma_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['tridarmas'] = $this->model_tridarma->get($filter, $field, $this->limit_page, $offset);
		$this->data['tridarma_counts'] = $this->model_tridarma->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/tridarma/index/',
			'total_rows'   => $this->model_tridarma->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Tridarma List');
		$this->render('backend/standart/administrator/tridarma/tridarma_list', $this->data);
	}

	/**
	 * Update view Tridarmas
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('tridarma_update');

		$this->data['tridarma'] = $this->model_tridarma->find($id);

		$this->template->title('Tridarma Update');
		$this->render('backend/standart/administrator/tridarma/tridarma_update', $this->data);
	}

	/**
	 * Update Tridarmas
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('tridarma_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}
		$this->form_validation->set_rules('judul', 'Judul', 'trim');

		if ($this->form_validation->run()) {

			$save_data = [
				'judul' => $this->input->post('judul'),
				'skim_kegiatan' => $this->input->post('skim_kegiatan'),
				'nip' => $this->input->post('nip'),
				'used' => $this->input->post('used'),
			];


			$save_tridarma = $this->model_tridarma->change($id, $save_data);

			if ($save_tridarma) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/tridarma', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/tridarma/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/tridarma/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Tridarmas
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('tridarma_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'tridarma'), 'success');
		} else {
			set_message(cclang('error_delete', 'tridarma'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Tridarmas
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('tridarma_view');

		$this->data['tridarma'] = $this->model_tridarma->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Tridarma Detail');
		$this->render('backend/standart/administrator/tridarma/tridarma_view', $this->data);
	}

	/**
	 * delete Tridarmas
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$tridarma = $this->model_tridarma->find($id);
		return $this->model_tridarma->remove($id);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('tridarma_export');

		$this->model_tridarma->export(
			'tridarma',
			'tridarma',
			$this->model_tridarma->field_search
		);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('tridarma_export');

		$this->model_tridarma->pdf('tridarma', 'tridarma');
	}

	public function single_pdf($id = null)
	{
		$this->is_allowed('tridarma_export');

		$table = $title = 'tridarma';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_tridarma->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
	public function set_used()
	{
		if($this->model_tridarma->updateUsed($this->input->post()));
		$val = ($this->input->post('val') == 'true' ? 'used' : 'unused');
		echo 'update to : '.$val;
	}
}


/* End of file tridarma.php */
/* Location: ./application/controllers/administrator/Tridarma.php */