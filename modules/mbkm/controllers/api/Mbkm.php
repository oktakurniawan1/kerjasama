<?php
defined('BASEPATH') or exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Mbkm extends API
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_mbkm');
	}

	/**
	 * @api {get} /mbkm/all Get all mbkms.
	 * @apiVersion 0.1.0
	 * @apiName AllMbkm 
	 * @apiGroup mbkm
	 * @apiHeader {String} X-Api-Key Mbkms unique access-key.
	 * @apiPermission Mbkm Cant be Accessed permission name : api_mbkm_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Mbkms.
	 * @apiParam {String} [Field="All Field"] Optional field of Mbkms : id, scope, kd_prodi, mitra_univ, mitra_prodi, mitra_jjg, mou_ks, unit_ks, smt, deskripsi, validasi, tgl_input, tahun.
	 * @apiParam {String} [Start=0] Optional start index of Mbkms.
	 * @apiParam {String} [Limit=10] Optional limit data of Mbkms.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of mbkm.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataMbkm Mbkm data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function row_get()
	{
		$this->is_allowed('api_mbkm_detail', false);

		$this->requiredInput(['kd_prodi', 'mitra_univ', 'mitra_prodi', 'mitra_jjg', 'smt']);

		$kd_prodi = $this->get('kd_prodi');
		$mitra_univ = $this->get('mitra_univ');
		$mitra_prodi = $this->get('mitra_prodi');
		$mitra_jjg = $this->get('mitra_jjg');
		$smt = $this->get('smt');

		$mbkm = $this->model_api_mbkm->getmbkm($kd_prodi, $mitra_univ, $mitra_prodi, $mitra_jjg, $smt);

		if (!$mbkm) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'data not found'
			], API::HTTP_NOT_FOUND);
		}


		$data['mbkm'] = $mbkm;
		if ($data['mbkm']) {

			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Mbkm',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Mbkm not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	public function mitraMou_get()
	{
		$this->requiredInput(['id']);
		$id = $this->get('id');

		$mbkm = $this->model_api_mbkm->getmitramou($id);

		if (!$mbkm) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'data not found'
			], API::HTTP_NOT_FOUND);
		}


		$data['mbkm'] = $mbkm;
		if ($data['mbkm']) {

			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Mbkm',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Mbkm not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function updateprodi_get()
	{
		$this->is_allowed('api_mbkm_detail', false);

		$prodi = $this->model_api_mbkm->getupdateprodi();

		if (!$prodi) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'data not found'
			], API::HTTP_NOT_FOUND);
		}

		$data['prodi'] = $prodi;
		if ($data['prodi']) {

			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Update Prodi',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Prodi not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function all_get()
	{
		$this->is_allowed('api_mbkm_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'scope', 'kd_prodi', 'mitra_univ', 'mitra_prodi', 'mitra_jjg', 'mou_ks', 'unit_ks', 'smt', 'deskripsi', 'validasi', 'tgl_input', 'tahun'];
		$mbkms = $this->model_api_mbkm->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_mbkm->count_all($filter, $field);
		$mbkms = array_map(function ($row) {

			return $row;
		}, $mbkms);

		$data['mbkm'] = $mbkms;

		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Mbkm',
			'data'	 	=> $data,
			'total' 	=> $total,
		], API::HTTP_OK);
	}

	/**
	 * @api {get} /mbkm/detail Detail Mbkm.
	 * @apiVersion 0.1.0
	 * @apiName DetailMbkm
	 * @apiGroup mbkm
	 * @apiHeader {String} X-Api-Key Mbkms unique access-key.
	 * @apiPermission Mbkm Cant be Accessed permission name : api_mbkm_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Mbkms.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of mbkm.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError MbkmNotFound Mbkm data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_mbkm_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'scope', 'kd_prodi', 'mitra_univ', 'mitra_prodi', 'mitra_jjg', 'mou_ks', 'unit_ks', 'smt', 'deskripsi', 'validasi', 'tgl_input', 'tahun'];
		$mbkm = $this->model_api_mbkm->find($id, $select_field);

		if (!$mbkm) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'data not found'
			], API::HTTP_NOT_FOUND);
		}


		$data['mbkm'] = $mbkm;
		if ($data['mbkm']) {

			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Mbkm',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Mbkm not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}


	/**
	 * @api {post} /mbkm/add Add Mbkm.
	 * @apiVersion 0.1.0
	 * @apiName AddMbkm
	 * @apiGroup mbkm
	 * @apiHeader {String} X-Api-Key Mbkms unique access-key.
	 * @apiPermission Mbkm Cant be Accessed permission name : api_mbkm_add
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_mbkm_add', false);


		if ($this->form_validation->run()) {

			$save_data = [];

			$save_mbkm = $this->model_api_mbkm->store($save_data);

			if ($save_mbkm) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);
			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /mbkm/update Update Mbkm.
	 * @apiVersion 0.1.0
	 * @apiName UpdateMbkm
	 * @apiGroup mbkm
	 * @apiHeader {String} X-Api-Key Mbkms unique access-key.
	 * @apiPermission Mbkm Cant be Accessed permission name : api_mbkm_update
	 *
	 * @apiParam {Integer} id Mandatory id of Mbkm.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_mbkm_update', false);



		if ($this->form_validation->run()) {

			$save_data = [];

			$save_mbkm = $this->model_api_mbkm->change($this->post('id'), $save_data);

			if ($save_mbkm) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);
			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /mbkm/delete Delete Mbkm. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteMbkm
	 * @apiGroup mbkm
	 * @apiHeader {String} X-Api-Key Mbkms unique access-key.
	 * @apiPermission Mbkm Cant be Accessed permission name : api_mbkm_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Mbkms .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_mbkm_delete', false);

		$mbkm = $this->model_api_mbkm->find($this->post('id'));

		if (!$mbkm) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Mbkm not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_mbkm->remove($this->post('id'));
		}

		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Mbkm deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Mbkm not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
}

/* End of file Mbkm.php */
/* Location: ./application/controllers/api/Mbkm.php */