<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Mbkm Controller
 *| --------------------------------------------------------------------------
 *| Mbkm site
 *|
 */
class Mbkm extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_mbkm');
		$this->lang->load('web_lang', $this->current_lang);
		//cek_isi_survey();
	}
	/**
	 * Filter data
	 */
	public function filter_session_data()
	{
		$fields['tahun'] = $this->input->post('tahun');
		$fields['negara'] = $this->input->post('negara');
		$fields['scope'] = $this->input->post('scope');
		$fields['smti'] = $this->input->post('smti');
		$fields['kd_prodi'] = $this->input->post('kd_prodi');
		$fields['validasi'] = $this->input->post('validasi');
		
		if ($this->input->post('tahun') == 'all') $fields['smt'] = 'all';
		elseif ($this->input->post('smti') == 'all') $fields['smt'] = 'all';
		else $fields['smt'] = $this->input->post('tahun') . $this->input->post('smti');
		$this->session->set_userdata('is_filtered_mbkm',TRUE);
		$this->session->set_userdata('filterdata_mbkm', $fields);
		echo json_encode(['url' => base_url('administrator/mbkm')]);
	}
	public function clear_filter_session_data()
	{
		$this->session->unset_userdata('filterdata_mbkm');
		$this->session->unset_userdata('is_filtered_mbkm');
		echo json_encode(['url' => base_url('administrator/mbkm')]);
	}
	public function form_filter_data()
	{
		if (!$this->session->has_userdata('filterdata_mbkm')) {
			$fields = ['tahun' => 'all', 'negara' => 'all', 'kd_prodi' => 'all', 'scope' => 'all', 'smti' => 'all', 'validasi' => 'all'];
			$this->session->set_userdata('filterdata_mbkm', $fields);
		}

		$negara = array_column(db_get_all_data('negara'), 'nama_negara', 'nama_negara');
		$tahun = array_combine(range(2015, date('Y')), range(2015, date('Y')));
		$prodi = array_column(get_filter_prodi(), 'nama_prodi', 'kode');
		$prodi['all'] = 'ALL';
		$negara['all'] = 'ALL';
		$tahun['all'] = 'ALL';
		$data['tahun'] = $tahun;
		$data['negara'] = $negara;
		$data['prodi'] = $prodi;
		$data['status'] = array('1' => 'Aktif', '0' => 'Selesai', 'all' => 'ALL');
		$data['smti'] = array('1' => 'Gasal', '2' => 'Genap', 'all' => 'ALL');
		$data['validasi'] = array('Y' => 'Y', 'T' => 'T', '' => '', 'all' => 'ALL');
		$data['scope'] = array('DN' => 'DN', 'LN' => 'LN', 'all' => 'ALL');
		$this->load->view('form-filter-data', $data);
	}
	/**
	 * show all Mbkms
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('mbkm_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('mbkm_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('mbkm_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('mbkm_filter');
			$this->session->unset_userdata('mbkm_field');
		}
		$filter = $this->session->userdata('mbkm_filter');
		$field = $this->session->userdata('mbkm_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['mbkms'] = $this->model_mbkm->get($filter, $field, $this->limit_page, $offset);
		$this->data['mbkm_counts'] = $this->model_mbkm->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/mbkm/index/',
			'total_rows'   => $this->model_mbkm->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Mbkm List');
		$this->render('backend/standart/administrator/mbkm/mbkm_list', $this->data);
	}

	/**
	 * Add new mbkms
	 *
	 */
	public function add()
	{
		$this->is_allowed('mbkm_add');

		$this->template->title('Mbkm New');
		$this->render('backend/standart/administrator/mbkm/mbkm_add', $this->data);
	}

	/**
	 * Add New Mbkms
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('mbkm_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('scope', 'DN/LN', 'trim|required');
		$this->form_validation->set_rules('kd_prodi', 'Program Studi Unnes', 'trim|required');
		$this->form_validation->set_rules('mitra_univ', 'Mitra Perguruan Tinggi', 'trim|required');
		$this->form_validation->set_rules('mitra_prodi', 'Mitra Program Studi', 'trim|required');
		$this->form_validation->set_rules('mou_ks', 'Nota Kesepahaman (MoU)', 'trim|required');
		$this->form_validation->set_rules('unit_ks', 'MoA/IA/LoA (Unit)', 'trim|required');
		$this->form_validation->set_rules('smt', 'Semester', 'trim|required');


		if ($this->form_validation->run()) {

			$prodimitra = db_get_all_data('dikti_prodi', ['id' => $this->input->post('mitra_prodi')], 'row');

			$save_data = [
				'scope' => $this->input->post('scope'),
				'kd_prodi' => $this->input->post('kd_prodi'),
				'mitra_univ' => $this->input->post('mitra_univ'),
				'mitra_prodi' => $prodimitra->kd_prodi,
				'mitra_jjg' => $prodimitra->kdjjg,
				'tgl_input' => date('Y-m-d'),
				'tahun' => $this->input->post('tahun'),
				'mou_ks' => $this->input->post('mou_ks'),
				'unit_ks' => $this->input->post('unit_ks'),
				'smt' => $this->input->post('tahun') . $this->input->post('smt'),
				'deskripsi' => $this->input->post('deskripsi'),
				'validasi' => $this->input->post('validasi'),
			];

			$save_mbkm = $this->model_mbkm->store($save_data);

			if ($save_mbkm) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_mbkm;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/mbkm/edit/' . $save_mbkm, 'Edit Mbkm'),
						anchor('administrator/mbkm', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/mbkm/edit/' . $save_mbkm, 'Edit Mbkm')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/mbkm');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/mbkm');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Mbkms
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('mbkm_update');

		$this->data['mbkm'] = $this->model_mbkm->find($id);

		if (is_groups_in(['Kaprodi', 'Unit'])) {
			$prodi = get_filter_prodi(true);
			$row = $this->model_mbkm->find($id);
			if (!in_array($row->kd_prodi, $prodi)) {
				set_message(cclang('sorry_you_do_not_have_permission_to_access', 'mbkm'), 'error');
				redirect_back();
			}
			/**tolak jika sudah divalidasi */
			if ($row->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa mengubah data, karena data sudah divalidasi', 'mbkm'), 'warning');
				redirect_back();
			}
		}
		$this->data['filter_prodi'] = get_filter_prodi();
		$this->template->title('Mbkm Update');
		$this->render('backend/standart/administrator/mbkm/mbkm_update', $this->data);
	}

	/**
	 * Update Mbkms
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('mbkm_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('scope', 'DN/LN', 'trim|required');
		$this->form_validation->set_rules('kd_prodi', 'Program Studi Unnes', 'trim|required');
		$this->form_validation->set_rules('mitra_univ', 'Mitra Perguruan Tinggi', 'trim|required');
		$this->form_validation->set_rules('mitra_prodi', 'Mitra Program Studi', 'trim|required');
		$this->form_validation->set_rules('mou_ks', 'Nota Kesepahaman (MoU)', 'trim|required');
		$this->form_validation->set_rules('unit_ks', 'MoA/IA/LoA (Unit)', 'trim|required');
		$this->form_validation->set_rules('smt', 'Semester', 'trim|required');

		if ($this->form_validation->run()) {

			$prodimitra = db_get_all_data('dikti_prodi', ['id' => $this->input->post('mitra_prodi')], 'row');

			$save_data = [
				'scope' => $this->input->post('scope'),
				'kd_prodi' => $this->input->post('kd_prodi'),
				'mitra_univ' => $this->input->post('mitra_univ'),
				'mitra_prodi' => $prodimitra->kd_prodi,
				'mitra_jjg' => $prodimitra->kdjjg,
				'mou_ks' => $this->input->post('mou_ks'),
				'unit_ks' => $this->input->post('unit_ks'),
				'tahun' => $this->input->post('tahun'),
				'smt' => $this->input->post('tahun') . $this->input->post('smt'),
				'deskripsi' => $this->input->post('deskripsi'),
				'validasi' => $this->input->post('validasi'),
			];


			$save_mbkm = $this->model_mbkm->change($id, $save_data);

			if ($save_mbkm) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/mbkm', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/mbkm/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/mbkm/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Mbkms
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('mbkm_delete');

		if (is_groups_in(['Kaprodi', 'Unit'])) {
			$prodi = get_filter_prodi(true);
			$row = $this->model_mbkm->find($id);
			if (!in_array($row->kd_prodi, $prodi)) {
				set_message(cclang('sorry_you_do_not_have_permission_to_access', 'mbkm'), 'error');
				redirect_back();
			}
			/**tolak jika sudah divalidasi */
			if ($row->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa menghapus data, karena data sudah divalidasi', 'mbkm'), 'warning');
				redirect_back();
			}
		}

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'mbkm'), 'success');
		} else {
			set_message(cclang('error_delete', 'mbkm'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Mbkms
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('mbkm_view');

		$this->data['mbkm'] = $this->model_mbkm->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Mbkm Detail');
		$this->render('backend/standart/administrator/mbkm/mbkm_view', $this->data);
	}

	/**
	 * delete Mbkms
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$mbkm = $this->model_mbkm->find($id);
		return $this->model_mbkm->remove($id);
	}

	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('mbkm_export');
		
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
			'kd_prodi',
			'ref_prodi_nama_prodi',
			'tahun',
			'scope',
			'mou_kerjasama_nama_mitra',
			'si_kerjasama_nama_mitra',
			'dikti_univ_nama_univ',
			'dikti_prodi_nama_prodi',
			'dikti_prodi_jenjang',
			'smt',
			'validasi'
		];
		$data = $this->model_mbkm->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_mbkm->export2($data, 'file', $selected_field);
		// $this->model_mbkm->export(
		// 	'mbkm',
		// 	'mbkm',
		// 	$this->model_mbkm->field_search
		// );
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('mbkm_export');

		$this->model_mbkm->pdf('mbkm', 'mbkm');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('mbkm_export');

		$table = $title = 'mbkm';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_mbkm->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}

	public function ajax_mitra_prodi($id = null)
	{
		if (!$this->is_allowed('mbkm_list', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}
		$results = db_get_all_data('dikti_prodi', ['kd_univ' => $id]);
		$this->response($results);
	}

	public function ajax_mou_ks($scope = null)
	{
		if (!$this->is_allowed('mbkm_list', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}
		// $results = db_get_all_data('mou_kerjasama', ['scope' => $id]);
		$mou_ks = $this->input->get('mou_ks');
		$results = db_get_all_data('mou_kerjasama', "scope='$scope' AND tgl_selesai > curdate()");
		if (!is_null($mou_ks)) {
			$cur_mou = db_get_all_data('mou_kerjasama', ['id' => $mou_ks], 'row');
			if (!is_null($cur_mou)) {
				$tgl = DateTime::createFromFormat('Y-m-d', $cur_mou->tgl_selesai);
				$now = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
				if ($tgl < $now) $results[] = $cur_mou;
			}
			$results[] = (object)['id' => '1', 'nama_mitra' => 'Pengajuan Baru', 'doc_nomor' => 'XXX'];
		}
		$this->response($results);
	}

	public function ajax_unit_ks($mou_ks = null)
	{
		if (!$this->is_allowed('mbkm_list', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}
		// $results = db_get_all_data('si_kerjasama', ['mou_ks' => $id]);
		$kd_prodi = $this->input->get('kd_prodi');
		$unit_ks = $this->input->get('unit_ks');

		$results = db_get_all_data('si_kerjasama', "mou_ks='$mou_ks' AND tgl_selesai > curdate() AND kd_prodi ='{$kd_prodi}' ");

		if (!is_null($unit_ks)) {
			$cur_moa = db_get_all_data('si_kerjasama', ['id' => $unit_ks], 'row');
			if (!is_null($cur_moa)) {
				$tgl = DateTime::createFromFormat('Y-m-d', $cur_moa->tgl_selesai);
				$now = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
				if ($tgl < $now) $results[] = $cur_moa;
			}
		}

		$this->response($results);
	}
}
/* End of file mbkm.php */
/* Location: ./application/controllers/administrator/Mbkm.php */