<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_mbkm extends MY_Model
{

	private $primary_key    = 'id';
	private $table_name     = 'mbkm';
	public $field_search   = ['scope', 'kd_prodi', 'mitra_univ', 'mitra_prodi', 'mou_ks', 'unit_ks', 'smt', 'tahun', 'validasi'];

	public function __construct()
	{
		$config = array(
			'primary_key'   => $this->primary_key,
			'table_name'    => $this->table_name,
			'field_search'  => $this->field_search,
		);

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "mbkm." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'mitra_univ') $where .= "OR " . "dikti_univ.nama_univ LIKE '%" . $q . "%' ";
					if ($field == 'unit_ks') $where .= "OR " . "si_kerjasama.nama_mitra LIKE '%" . $q . "%' ";
					if ($field == 'kd_prodi') $where .= "OR " . "ref_prodi.nama_prodi LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "mbkm." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'mitra_univ') $where .= "OR " . "dikti_univ.nama_univ LIKE '%" . $q . "%' ";
					if ($field == 'unit_ks') $where .= "OR " . "si_kerjasama.nama_mitra LIKE '%" . $q . "%' ";
					if ($field == 'kd_prodi') $where .= "OR " . "ref_prodi.nama_prodi LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			if ($field == 'mitra_univ') $where .= "(" . "dikti.nama_univ LIKE '%" . $q . "%' )";
			else if ($field == 'kd_prodi') $where .= "(" . "ref_prodi.nama_prodi LIKE '%" . $q . "%' )";
			elseif ($field == 'unit_ks') $where .= "(" . "rsi_kerjasama.nama_mitra LIKE '%" . $q . "%' )";
			else $where .= "(" . "mbkm." . $field . " LIKE '%" . $q . "%' )";
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);

		/**Filter data */
		if ($this->session->has_userdata('filterdata_mbkm')) {
			$filter = fromsess('filterdata_mbkm');
			unset($filter['smti']);
			foreach ($filter as $key => $row) {
				if ($row != 'all') $this->db->having($key,$row);
			}
		}

		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [],$result = TRUE)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "mbkm." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'mitra_univ') $where .= "OR " . "dikti_univ.nama_univ LIKE '%" . $q . "%' ";
					if ($field == 'unit_ks') $where .= "OR " . "si_kerjasama.nama_mitra LIKE '%" . $q . "%' ";
					if ($field == 'kd_prodi') $where .= "OR " . "ref_prodi.nama_prodi LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "mbkm." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'mitra_univ') $where .= "OR " . "dikti_univ.nama_univ LIKE '%" . $q . "%' ";
					if ($field == 'unit_ks') $where .= "OR " . "si_kerjasama.nama_mitra LIKE '%" . $q . "%' ";
					if ($field == 'kd_prodi') $where .= "OR " . "ref_prodi.nama_prodi LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			if ($field == 'mitra_univ') $where .= "(" . "dikti.nama_univ LIKE '%" . $q . "%' )";
			elseif ($field == 'kd_prodi') $where .= "(" . "ref_prodi.nama_prodi LIKE '%" . $q . "%' )";
			elseif ($field == 'unit_ks') $where .= "(" . "rsi_kerjasama.nama_mitra LIKE '%" . $q . "%' )";
			else $where .= "(" . "mbkm." . $field . " LIKE '%" . $q . "%' )";
		}

		if (is_array($select_field) and count($select_field)) {
			$this->db->select($select_field);
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);
		$this->db->limit($limit, $offset);

		$this->sortable();

		/**Filter data */
		if ($this->session->has_userdata('filterdata_mbkm')) {
			$filter = fromsess('filterdata_mbkm');
			unset($filter['smti']);
			foreach ($filter as $key => $row) {
				if ($row != 'all') $this->db->having($key,$row);
			}
		}

		$query = $this->db->get($this->table_name);
		if ($result)
			return $query->result();
		else return $query;
	}

	public function join_avaiable()
	{
		$this->db->join('ref_prodi', 'ref_prodi.kode = mbkm.kd_prodi', 'LEFT');
		$this->db->join('dikti_univ', 'dikti_univ.kd_univ = mbkm.mitra_univ', 'LEFT');
		$this->db->join('dikti_prodi', '(dikti_prodi.kd_prodi = mbkm.mitra_prodi AND dikti_prodi.kdjjg = mbkm.mitra_jjg AND mbkm.mitra_univ = dikti_prodi.kd_univ)', 'INNER');
		$this->db->join('mou_kerjasama', 'mou_kerjasama.id = mbkm.mou_ks', 'LEFT');
		$this->db->join('si_kerjasama', 'si_kerjasama.id = mbkm.unit_ks', 'LEFT');

		$this->db->select('mbkm.*,ref_prodi.nama_prodi as ref_prodi_nama_prodi,dikti_univ.nama_univ as dikti_univ_nama_univ,dikti_prodi.nama_prodi as dikti_prodi_nama_prodi,mou_kerjasama.nama_mitra as mou_kerjasama_nama_mitra,si_kerjasama.nama_mitra as si_kerjasama_nama_mitra');
		$this->db->select('dikti_prodi.kd_prodi as dikti_kd_prodi');
		$this->db->select('dikti_univ.kd_univ as dikti_kd_univ');
		$this->db->select('ref_prodi.kode as unnes_kd_prodi');
		$this->db->select('dikti_prodi.jenjang as dikti_prodi_jenjang');
		$this->db->select('mou_kerjasama.doc_nomor as mou_kerjasama_doc_nomor');
		$this->db->select('si_kerjasama.doc_nomor as si_kerjasama_doc_nomor');


		return $this;
	}

	public function filter_avaiable()
	{

		// if (!$this->aauth->is_admin()) {
		// }
		$filter_prodi = get_filter_prodi();
		$prodi = array_column($filter_prodi,'kode');
		$this->db->where_in('mbkm.kd_prodi', $prodi);

		return $this;
	}
}

/* End of file Model_mbkm.php */
/* Location: ./application/models/Model_mbkm.php */