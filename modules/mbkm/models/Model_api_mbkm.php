<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_api_mbkm extends MY_Model
{

	private $primary_key 	= 'id';
	private $table_name 	= 'mbkm';
	private $field_search 	= ['id', 'scope', 'kd_prodi', 'mitra_univ', 'mitra_prodi', 'mitra_jjg', 'mou_ks', 'unit_ks', 'smt', 'deskripsi', 'validasi', 'tgl_input', 'tahun'];

	public function __construct()
	{
		$config = array(
			'primary_key' 	=> $this->primary_key,
			'table_name' 	=> $this->table_name,
			'field_search' 	=> $this->field_search,
		);

		parent::__construct($config);
	}

	public function getmbkm($kd_prodi, $mitra_univ, $mitra_prodi, $mitra_jjg, $smt)
	{
		$sql = "SELECT
					m.scope,
					m.kd_prodi,
					m.mitra_univ,
					m.mitra_prodi,
					m.mitra_jjg,
					m.smt,
					IF(m.mou_ks = 1,1,o.id) AS id_mou,
					u.id as id_moa,
					IF(m.mou_ks = 1,'Pengajuan Baru',concat( o.nama_mitra, ' - ', o.doc_nomor )) AS mou,
					concat( u.nama_mitra, ' - ', u.doc_nomor ) AS moa,
					m.validasi
				FROM
					mbkm m
					LEFT JOIN mou_kerjasama o ON ( m.mou_ks = o.id )
					LEFT JOIN si_kerjasama u ON (
					m.unit_ks = u.id) where m.kd_prodi = '$kd_prodi' AND m.mitra_univ = '$mitra_univ' AND m.mitra_prodi = '$mitra_prodi' AND m.mitra_jjg = '$mitra_jjg' AND m.smt = $smt ";
		return $this->db->query($sql)->result();
	}
	public function getupdateprodi()
	{
		return $this->db->get('dikti_prodi_updated')->result();
	}
	public function getmitramou($id){
		$this->db->select('mitra_mbkm_unnes.*')
			->select('U.doc_nomor AS _doc_nomor,U.nama_mitra AS _nama_mitra');
		$this->db->join('si_kerjasama U','U.id = mitra_mbkm_unnes.unit_ks','LEFT');
		$this->db->where('mitra_mbkm_unnes.id',$id);
		return $this->db->get('mitra_mbkm_unnes')->result();
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			$where .= "(" . $field . " LIKE '%" . $q . "%' )";
		}

		$this->db->where($where);
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			if (in_array($field, $select_field)) {
				$where .= "(" . $field . " LIKE '%" . $q . "%' )";
			}
		}

		if (is_array($select_field) and count($select_field)) {
			$this->db->select($select_field);
		}

		if ($where) {
			$this->db->where($where);
		}
		$this->db->limit($limit, $offset);
		$this->db->order_by($this->primary_key, "DESC");
		$query = $this->db->get($this->table_name);

		return $query->result();
	}
}

/* End of file Model_mbkm.php */
/* Location: ./application/models/Model_mbkm.php */