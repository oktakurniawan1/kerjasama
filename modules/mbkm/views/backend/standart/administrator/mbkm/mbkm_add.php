<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>

<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Kerjasama MBKM <small><?= cclang('new', ['Kerjasama MBKM']); ?> </small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/mbkm'); ?>">Kerjasama MBKM</a></li>
		<li class="active"><?= cclang('new'); ?></li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Kerjasama MBKM</h3>
							<h5 class="widget-user-desc"><?= cclang('new', ['Kerjasama MBKM']); ?></h5>
							<hr>
						</div>
						<?= form_open('', [
							'name'    => 'form_mbkm',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_mbkm',
							'enctype' => 'multipart/form-data',
							'method'  => 'POST'
						]); ?>

						<div class="form-group ">
							<label for="scope" class="col-sm-2 control-label">DN/LN
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select" name="scope" id="scope" data-placeholder="Select DN/LN">
									<option value=""></option>
									<option value="DN">Dalam Negeri</option>
									<option value="LN">Luar Negeri</option>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="kd_prodi" class="col-sm-2 control-label">Program Studi Unnes
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="kd_prodi" id="kd_prodi" data-placeholder="Select Program Studi Unnes">
									<option value=""></option>
									<?php foreach (db_get_all_data('ref_prodi') as $row) : ?>
										<option value="<?= $row->kode ?>"><?= $row->kode . ' - ' . $row->nama_prodi; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>


						<div class="form-group ">
							<label for="mitra_univ" class="col-sm-2 control-label">Mitra Perguruan Tinggi
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="mitra_univ" id="mitra_univ" data-placeholder="Select Mitra Perguruan Tinggi">
									<option value=""></option>
									<?php foreach (db_get_all_data('dikti_univ') as $row) : ?>
										<option value="<?= $row->kd_univ ?>"><?= $row->kd_univ . ' - ' . $row->nama_univ; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="mitra_prodi" class="col-sm-2 control-label">Mitra Program Studi
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="mitra_prodi" id="mitra_prodi" data-placeholder="Select Mitra Program Studi">
									<option value=""></option>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="mou_ks" class="col-sm-2 control-label">Nota Kesepahaman (MoU)
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="mou_ks" id="mou_ks" data-placeholder="Select Nota Kesepahaman (MoU)">
									<option value=""></option>
								</select>
								<small class="info help-block">
									Pilih MoU, jika tidak ada pilih yang masih dalam <b>PENGAJUAN BARU</b>
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="unit_ks" class="col-sm-2 control-label">MoA/IA/LoA (Unit)
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="unit_ks" id="unit_ks" data-placeholder="Select MoA/IA/LoA (Unit)">
									<option value=""></option>
								</select>
								<small class="info help-block">
									Jika tidak ada, silahkan inputkan MoA/IA/LoA terlebih dahulu.
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="smt" class="col-sm-2 control-label">Semester
								<i class="required">*</i>
							</label>
							<div class="col-sm-3">
								<?php
								$range = range(date('Y') - 2, date('Y'));
								$tahun = array_combine($range, $range);
								echo form_dropdown('tahun', $tahun, date('Y'), ['class' => 'form-control chosen chosen-select', 'data-placeholder' => 'Select Tahun']);
								?>
								<small class="info help-block">
									<?= html_alert("Inputan Semester ini adalah tahun akademik, contoh Tahun : <b>\"" . (date('Y') - 1) . "\"</b> semester : \"<b>1</b>\" disebut tahun akademik <b>Gasal " . (date('Y') - 1) . "</b>, atau semester : \"<b>2</b>\" disebut tahun akademik <b>Genap " . (date('Y') - 1) . "</b>", 'Perhatian', 'warning') ?>

								</small>
							</div>
							<div class="col-sm-2">
								<select class="form-control chosen chosen-select" name="smt" id="smt" data-placeholder="Select Semester">
									<option value="1">1 - Gasal</option>
									<option value="2">2 - Genap</option>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="deskripsi" class="col-sm-2 control-label">Deskripsi
							</label>
							<div class="col-sm-8">
								<textarea id="deskripsi" name="deskripsi" rows="5" class="textarea form-control"><?= set_value('deskripsi'); ?></textarea>
								<small class="info help-block">
								</small>
							</div>
						</div>
						<?php if (is_groups_in(['Admin', 'Kerjasama'])) : ?>
							<div class="form-group ">
								<label for="validasi" class="col-sm-2 control-label">Validasi
								</label>
								<div class="col-sm-8">
									<select class="form-control chosen chosen-select-deselect" name="validasi" id="validasi" data-placeholder="Select Validasi">
										<option value=""></option>
										<option value="Y">Valid</option>
										<option value="T">Invalid</option>
									</select>
									<small class="info help-block">
									</small>
								</div>
							</div>
						<?php endif; ?>

						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	$(document).ready(function() {

		$('#btn_cancel').click(function() {
			swal({
					title: "<?= cclang('are_you_sure'); ?>",
					text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/mbkm';
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();

			var form_mbkm = $('#form_mbkm');
			var data_post = form_mbkm.serializeArray();
			var save_type = $(this).attr('data-stype');

			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: BASE_URL + '/administrator/mbkm/add_save',
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('.steps li').removeClass('error');
					$('form').find('.error-input').remove();
					if (res.success) {

						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						resetForm();
						$('.chosen option').prop('selected', false).trigger('chosen:updated');

					} else {
						if (res.errors) {

							$.each(res.errors, function(index, val) {
								$('form #' + index).parents('.form-group').addClass('has-error');
								$('form #' + index).parents('.form-group').find('small').prepend(`
                      <div class="error-input">` + val + `</div>
                      `);
							});
							$('.steps li').removeClass('error');
							$('.content section').each(function(index, el) {
								if ($(this).find('.has-error').length) {
									$('.steps li:eq(' + index + ')').addClass('error').find('a').trigger('click');
								}
							});
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/

		$('#mitra_univ').change(function(event) {
			var val = $(this).val();
			$.LoadingOverlay('show')
			$.ajax({
					url: BASE_URL + '/administrator/mbkm/ajax_mitra_prodi/' + val,
					dataType: 'JSON',
				})
				.done(function(res) {
					var html = '<option value=""></option>';
					$.each(res, function(index, val) {
						html += '<option value="' + val.id + '">' + val.kd_prodi + ' - ' + val.nama_prodi + ' - ' + val.jenjang + '</option>'
					});
					$('#mitra_prodi').html(html);
					$('#mitra_prodi').trigger('chosen:updated');

				})
				.fail(function() {
					toastr['error']('Error', 'Getting data fail')
				})
				.always(function() {
					$.LoadingOverlay('hide')
				});

		});

		$('#scope').change(function(event) {
			var val = $(this).val();
			$.LoadingOverlay('show')
			$.ajax({
					url: BASE_URL + '/administrator/mbkm/ajax_mou_ks/' + val,
					dataType: 'JSON',
				})
				.done(function(res) {
					var html = '<option value=""></option>';
					$.each(res, function(index, val) {
						html += '<option value="' + val.id + '">' + val.nama_mitra + ' - ' + val.doc_nomor + '</option>';
					});
					html += '<option value="1">' + "Pengajuan Baru" + '</option>';
					$('#mou_ks').html(html);
					$('#mou_ks').trigger('chosen:updated');

				})
				.fail(function() {
					toastr['error']('Error', 'Getting data fail')
				})
				.always(function() {
					$.LoadingOverlay('hide')
				});

		});

		$('#mou_ks').change(function(event) {
			var val = $(this).val();
			$.LoadingOverlay('show')
			$.ajax({
					url: BASE_URL + '/administrator/mbkm/ajax_unit_ks/' + val,
					dataType: 'JSON',
					data: {
						kd_prodi: $('#kd_prodi').val()
					},
					type: 'get',
				})
				.done(function(res) {
					var html = '<option value=""></option>';
					$.each(res, function(index, val) {
						html += '<option value="' + val.id + '">' + val.nama_mitra + ' - ' + val.doc_nomor + '</option>';
					});
					$('#unit_ks').html(html);
					$('#unit_ks').trigger('chosen:updated');

				})
				.fail(function() {
					toastr['error']('Error', 'Getting data fail')
				})
				.always(function() {
					$.LoadingOverlay('hide')
				});

		});
		$('#kd_prodi').change(function(e) {
			$('#mou_ks').trigger('change');
		})

	}); /*end doc ready*/
</script>