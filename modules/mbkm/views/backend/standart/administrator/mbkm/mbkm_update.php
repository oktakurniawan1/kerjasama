<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>

<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Kerjasama MBKM <small>Edit Kerjasama MBKM</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/mbkm'); ?>">Kerjasama MBKM</a></li>
		<li class="active">Edit</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Kerjasama MBKM</h3>
							<h5 class="widget-user-desc">Edit Kerjasama MBKM</h5>
							<hr>
						</div>
						<?= form_open(base_url('administrator/mbkm/edit_save/' . $this->uri->segment(4)), [
							'name'    => 'form_mbkm',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_mbkm',
							'method'  => 'POST'
						]); ?>

						<div class="form-group ">
							<label for="scope" class="col-sm-2 control-label">DN/LN
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select" name="scope" id="scope" data-placeholder="Select DN/LN">
									<option value=""></option>
									<option <?= $mbkm->scope == "DN" ? 'selected' : ''; ?> value="DN">Dalam Negeri</option>
									<option <?= $mbkm->scope == "LN" ? 'selected' : ''; ?> value="LN">Luar Negeri</option>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="kd_prodi" class="col-sm-2 control-label">Program Studi Unnes
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="kd_prodi" id="kd_prodi" data-placeholder="Select Program Studi Unnes">
									<option value=""></option>
									<?php foreach ($filter_prodi as $row) : ?>
										<option <?= $row->kode ==  $mbkm->kd_prodi ? 'selected' : ''; ?> value="<?= $row->kode ?>"><?= $row->kode . ' - ' . $row->nama_prodi; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="mitra_univ" class="col-sm-2 control-label">Mitra Perguruan Tinggi
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="mitra_univ" id="mitra_univ" data-placeholder="Select Mitra Perguruan Tinggi">
									<option value=""></option>
									<?php foreach (db_get_all_data('dikti_univ') as $row) : ?>
										<option <?= $row->kd_univ ==  $mbkm->mitra_univ ? 'selected' : ''; ?> value="<?= $row->kd_univ ?>"><?= $row->kd_univ . ' - ' . $row->nama_univ; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="mitra_prodi" class="col-sm-2 control-label">Mitra Program Studi
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="mitra_prodi" id="mitra_prodi" data-placeholder="Select Mitra Program Studi">
									<option value=""></option>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="mou_ks" class="col-sm-2 control-label">Nota Kesepahaman (MoU)
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="mou_ks" id="mou_ks" data-placeholder="Select Nota Kesepahaman (MoU)">
									<option value=""></option>
								</select>
								<small class="info help-block">
									Pilih MoU, jika tidak ada pilih yang masih dalam <b>PENGAJUAN BARU</b></small>
							</div>
						</div>

						<div class="form-group ">
							<label for="unit_ks" class="col-sm-2 control-label">MoA/IA/LoA (Unit)
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="unit_ks" id="unit_ks" data-placeholder="Select MoA/IA/LoA (Unit)">
									<option value=""></option>
								</select>
								<small class="info help-block">
									Jika tidak ada, silahkan inputkan MoA/IA/LoA terlebih dahulu.
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="smt" class="col-sm-2 control-label">Semester
								<i class="required">*</i>
							</label>
							<div class="col-sm-3">
								<?php
								$range = range(date('Y') - 2, date('Y'));
								$tahun = array_combine($range, $range);
								echo form_dropdown('tahun', $tahun, $mbkm->tahun, ['class' => 'form-control chosen chosen-select', 'data-placeholder' => 'Select Tahun']);
								?>
								<small class="info help-block">
									<?= html_alert("Inputan Semester ini adalah tahun akademik, contoh Tahun : <b>\"" . (date('Y') - 1) . "\"</b> semester : \"<b>1</b>\" disebut tahun akademik <b>Gasal " . (date('Y') - 1) . "</b>, atau semester : \"<b>2</b>\" disebut tahun akademik <b>Genap " . (date('Y') - 1) . "</b>", 'Perhatian', 'warning') ?>
								</small>
							</div>
							<div class="col-sm-2">
								<?php
								$smt_array = ['1' => '1 - Gasal', '2' => '2 - Genap'];
								$smt = substr($mbkm->smt, -1);
								echo form_dropdown('smt', $smt_array, $smt, ['class' => 'form_control chosen chosen-select', 'data-placeholder' => 'Select Semester'])
								?>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="deskripsi" class="col-sm-2 control-label">Deskripsi
							</label>
							<div class="col-sm-8">
								<textarea id="deskripsi" name="deskripsi" rows="5" class="textarea form-control"><?= set_value('deskripsi', $mbkm->deskripsi); ?></textarea>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<?php if (is_groups_in(['Admin', 'Kerjasama'])) : ?>
							<div class="form-group ">
								<label for="validasi" class="col-sm-2 control-label">Validasi
								</label>
								<div class="col-sm-8">
									<select class="form-control chosen chosen-select-deselect" name="validasi" id="validasi" data-placeholder="Select Validasi">
										<option value=""></option>
										<option <?= $mbkm->validasi == "Y" ? 'selected' : ''; ?> value="Y">Valid</option>
										<option <?= $mbkm->validasi == "T" ? 'selected' : ''; ?> value="T">Invalid</option>
									</select>
									<small class="info help-block">
									</small>
								</div>
							</div>
						<?php endif; ?>

						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	$(document).ready(function() {

		$('#btn_cancel').click(function() {
			swal({
					title: "Are you sure?",
					text: "the data that you have created will be in the exhaust!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/mbkm/index/' + "<?= $this->session->userdata('_page') ?>";
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();

			var form_mbkm = $('#form_mbkm');
			var data_post = form_mbkm.serializeArray();
			var save_type = $(this).attr('data-stype');
			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: form_mbkm.attr('action'),
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('form').find('.error-input').remove();
					$('.steps li').removeClass('error');
					if (res.success) {
						var id = $('#mbkm_image_galery').find('li').attr('qq-file-id');
						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						$('.data_file_uuid').val('');

					} else {
						if (res.errors) {
							parseErrorField(res.errors);
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/

		function chained_mitra_prodi(selected, complete) {
			var val = $('#mitra_univ').val();
			$.LoadingOverlay('show')
			return $.ajax({
					url: BASE_URL + '/administrator/mbkm/ajax_mitra_prodi/' + val,
					dataType: 'JSON',
				})
				.done(function(res) {
					var html = '<option value=""></option>';
					$.each(res, function(index, val) {
						html += '<option ' + (selected == val.kd_prodi ? 'selected' : '') + ' value="' + val.id + '">' + val.kd_prodi + ' - ' + val.nama_prodi + ' - ' + val.jenjang + '</option>'
					});
					$('#mitra_prodi').html(html);
					$('#mitra_prodi').trigger('chosen:updated');
					if (typeof complete != 'undefined') {
						complete();
					}

				})
				.fail(function() {
					toastr['error']('Error', 'Getting data fail')
				})
				.always(function() {
					$.LoadingOverlay('hide')
				});
		}

		$('#mitra_univ').change(function(event) {
			chained_mitra_prodi('')
		});

		function chained_mou_ks(selected, complete) {
			var val = $('#scope').val();
			$.LoadingOverlay('show')
			return $.ajax({
					url: BASE_URL + '/administrator/mbkm/ajax_mou_ks/' + val,
					dataType: 'JSON',
					type: 'get',
					data: {
						mou_ks: <?= $mbkm->mou_ks ?>
					},
				})
				.done(function(res) {
					var html = '<option value=""></option>';
					$.each(res, function(index, val) {
						html += '<option ' + (selected == val.id ? 'selected' : '') + ' value="' + val.id + '">' + val.nama_mitra + ' - ' + val.doc_nomor + '</option>'
					});
					$('#mou_ks').html(html);
					$('#mou_ks').trigger('chosen:updated');
					if (typeof complete != 'undefined') {
						complete();
					}

				})
				.fail(function() {
					toastr['error']('Error', 'Getting data fail')
				})
				.always(function() {
					$.LoadingOverlay('hide')
				});
		}

		$('#scope').change(function(event) {
			chained_mou_ks('')
		});

		function chained_unit_ks(selected, complete) {
			var val = $('#mou_ks').val();
			$.LoadingOverlay('show')
			return $.ajax({
					url: BASE_URL + '/administrator/mbkm/ajax_unit_ks/' + val,
					dataType: 'JSON',
					type: 'get',
					data: {
						unit_ks: $('#unit_ks').val(),
						kd_prodi: $('#kd_prodi').val()
					},
				})
				.done(function(res) {
					var html = '<option value=""></option>';
					$.each(res, function(index, val) {
						html += '<option ' + (selected == val.id ? 'selected' : '') + ' value="' + val.id + '">' + val.nama_mitra + ' - ' + val.doc_nomor + '</option>'
					});
					$('#unit_ks').html(html);
					$('#unit_ks').trigger('chosen:updated');
					if (typeof complete != 'undefined') {
						complete();
					}

				})
				.fail(function() {
					toastr['error']('Error', 'Getting data fail')
				})
				.always(function() {
					$.LoadingOverlay('hide')
				});
		}

		$('#mou_ks').change(function(event) {
			chained_unit_ks('')
		});

		$('#kd_prodi').change(function(e) {
			$('#mou_ks').trigger('change');
		})

		async function chain() {
			await chained_mitra_prodi("<?= $mbkm->mitra_prodi ?>");
			await chained_mou_ks("<?= $mbkm->mou_ks ?>");
			await chained_unit_ks("<?= $mbkm->unit_ks ?>");
		}

		chain();
	}); /*end doc ready*/
</script>