<form action="" method="POST" role="form">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label for="">Program Studi</label>
				<?= form_dropdown('kd_prodi', $prodi, fromsess('filterdata_mbkm')['kd_prodi'], ['style' => 'width:100%']) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label for="">Tahun</label>
				<?= form_dropdown('tahun', $tahun, fromsess('filterdata_mbkm')['tahun'], ['class' => 'form-control']) ?>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="">Semester</label>
				<?= form_dropdown('smti', $smti, fromsess('filterdata_mbkm')['smti'], ['class' => 'form-control']) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-5">
			<div class="form-group">
				<label for="">Negara</label>
				<?= form_dropdown('negara', $negara, fromsess('filterdata_mbkm')['negara'], ['style' => 'width:100%']) ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<label for="">Scope</label>
				<?= form_dropdown('scope', $scope, fromsess('filterdata_mbkm')['scope'], ['class' => 'form-control']) ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<label for="">Validasi</label>
				<?= form_dropdown('validasi', $validasi, fromsess('filterdata_mbkm')['validasi'], ['class' => 'form-control']) ?>
			</div>
		</div>
	</div>
</form>

<script>
	$(function() {
		$('select[name=kd_prodi],select[name=negara]').select2({
			theme: 'classic',
			dropdownParent: $('#filtermbkm-dialog'),
		});
	})
</script>