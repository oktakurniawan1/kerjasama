<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['mbkm'] = 'Kerjasama MBKM';
$lang['id'] = 'Id';
$lang['scope'] = 'DN/LN';
$lang['kd_prodi'] = 'Program Studi Unnes';
$lang['mitra_univ'] = 'Mitra Perguruan Tinggi';
$lang['mitra_prodi'] = 'Mitra Program Studi';
$lang['mou_ks'] = 'Nota Kesepahaman (MoU)';
$lang['unit_ks'] = 'MoA/IA/LoA (Unit)';
$lang['smt'] = 'Semester';
$lang['deskripsi'] = 'Deskripsi';
$lang['validasi'] = 'Validasi';
