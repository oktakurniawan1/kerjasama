<?php
defined('BASEPATH') or exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Si_kerjasama extends API
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_si_kerjasama');
	}

	/**
	 * @api {get} /si_kerjasama/all Get all si_kerjasamas.
	 * @apiVersion 0.1.0
	 * @apiName AllSikerjasama 
	 * @apiGroup si_kerjasama
	 * @apiHeader {String} X-Api-Key Si kerjasamas unique access-key.
	 * @apiPermission Si kerjasama Cant be Accessed permission name : api_si_kerjasama_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Si kerjasamas.
	 * @apiParam {String} [Field="All Field"] Optional field of Si kerjasamas : id, scope, mou_ks, jenis_ks, bentuk_ks, doc_tipe, doc_nomor, doc_tanggal, doc_file, tgl_mulai, tgl_selesai, tahun, dana_pendamping, dana_provit, nama_mitra, negara, penanggung_jawab, pj_email, pj_tlp, penanggung_jawab_mitra, pj_mitra_email, pj_mitra_tlp, pj_mitra_alamat, deskripsi, foto1, foto2, tgl_input, kd_prodi, validasi, is_aktif.
	 * @apiParam {String} [Start=0] Optional start index of Si kerjasamas.
	 * @apiParam {String} [Limit=10] Optional limit data of Si kerjasamas.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of si_kerjasama.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataSi kerjasama Si kerjasama data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_si_kerjasama_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'scope', 'mou_ks', 'jenis_ks', 'bentuk_ks', 'doc_tipe', 'doc_nomor', 'doc_tanggal', 'doc_file', 'tgl_mulai', 'tgl_selesai', 'tahun', 'dana_pendamping', 'dana_provit', 'nama_mitra', 'negara', 'penanggung_jawab', 'pj_email', 'pj_tlp', 'penanggung_jawab_mitra', 'pj_mitra_email', 'pj_mitra_tlp', 'pj_mitra_alamat', 'deskripsi', 'foto1', 'foto2', 'tgl_input', 'kd_prodi', 'validasi', 'is_aktif'];
		$si_kerjasamas = $this->model_api_si_kerjasama->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_si_kerjasama->count_all($filter, $field);
		$si_kerjasamas = array_map(function ($row) {

			return $row;
		}, $si_kerjasamas);

		$data['si_kerjasama'] = $si_kerjasamas;

		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Si kerjasama',
			'data'	 	=> $data,
			'total' 	=> $total,
		], API::HTTP_OK);
	}

	public function rekap_unit_iku_get()
	{
		$this->is_allowed('api_si_kerjasama_all', false);
		$data = $this->model_api_si_kerjasama->rekap_unit_iku();
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data UNIT IKU',
			'data'	 	=> $data,
		], API::HTTP_OK);
	}

	public function kategori_iku_get()
	{
		$this->is_allowed('api_si_kerjasama_all', false);
		$data = $this->model_api_si_kerjasama->kategori_iku();
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Ketegori IKU',
			'data'	 	=> $data,
		], API::HTTP_OK);
	}

	public function pendapatanKs_post()
	{
		$this->is_allowed('api_si_kerjasama_all', false);
		$this->form_validation->set_rules('id', 'Parameter ID', 'required|numeric');
		if ($this->form_validation->run()) {
			$data = $this->model_api_si_kerjasama->pendapatanKs($this->post('filter'));
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Data Pendapatan Kerjasama',
				'data'	 	=> $data,
			], API::HTTP_OK);
		} 
		else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function pendapatanKsByID_post()
	{
		$this->is_allowed('api_si_kerjasama_all', false);
		$this->form_validation->set_rules('id', 'Row ID', 'integer|required');
		if ($this->form_validation->run()) {
			$data = $this->model_api_si_kerjasama->pendapatanKsByID($this->post('id'));
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Data Pendapatan Kerjasama',
				'data'	 	=> $data,
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function updatePendapatanKs_post()
	{
		$this->is_allowed('api_si_kerjasama_update', false);
		$this->form_validation->set_rules('id', 'Row ID', 'integer|required');
		$this->form_validation->set_rules('data[]', 'Parameter Data Kolom', 'required');
		if ($this->form_validation->run()) {
			$save_data = $this->post('data');
			$update = $this->model_api_si_kerjasama->updatePendapatan($this->post('id'), $save_data);
			if ($update) {
				$save_data['id'] = $this->post('id');
				$this->response([
					'status' 	=> true,
					'data' => $save_data,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);
			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {get} /si_kerjasama/detail Detail Si kerjasama.
	 * @apiVersion 0.1.0
	 * @apiName DetailSi kerjasama
	 * @apiGroup si_kerjasama
	 * @apiHeader {String} X-Api-Key Si kerjasamas unique access-key.
	 * @apiPermission Si kerjasama Cant be Accessed permission name : api_si_kerjasama_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Si kerjasamas.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of si_kerjasama.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError Si kerjasamaNotFound Si kerjasama data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_si_kerjasama_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'scope', 'mou_ks', 'jenis_ks', 'bentuk_ks', 'doc_tipe', 'doc_nomor', 'doc_tanggal', 'doc_file', 'tgl_mulai', 'tgl_selesai', 'tahun', 'dana_pendamping', 'dana_provit', 'nama_mitra', 'negara', 'penanggung_jawab', 'pj_email', 'pj_tlp', 'penanggung_jawab_mitra', 'pj_mitra_email', 'pj_mitra_tlp', 'pj_mitra_alamat', 'deskripsi', 'foto1', 'foto2', 'tgl_input', 'kd_prodi', 'validasi', 'is_aktif'];
		$si_kerjasama = $this->model_api_si_kerjasama->find($id, $select_field);

		if (!$si_kerjasama) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Blog not found'
			], API::HTTP_NOT_FOUND);
		}


		$data['si_kerjasama'] = $si_kerjasama;
		if ($data['si_kerjasama']) {

			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Si kerjasama',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Si kerjasama not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}


	/**
	 * @api {post} /si_kerjasama/add Add Si kerjasama.
	 * @apiVersion 0.1.0
	 * @apiName AddSi kerjasama
	 * @apiGroup si_kerjasama
	 * @apiHeader {String} X-Api-Key Si kerjasamas unique access-key.
	 * @apiPermission Si kerjasama Cant be Accessed permission name : api_si_kerjasama_add
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_si_kerjasama_add', false);


		if ($this->form_validation->run()) {

			$save_data = [];

			$save_si_kerjasama = $this->model_api_si_kerjasama->store($save_data);

			if ($save_si_kerjasama) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);
			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /si_kerjasama/update Update Si kerjasama.
	 * @apiVersion 0.1.0
	 * @apiName UpdateSi kerjasama
	 * @apiGroup si_kerjasama
	 * @apiHeader {String} X-Api-Key Si kerjasamas unique access-key.
	 * @apiPermission Si kerjasama Cant be Accessed permission name : api_si_kerjasama_update
	 *
	 * @apiParam {Integer} id Mandatory id of Si Kerjasama.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_si_kerjasama_update', false);
		if ($this->form_validation->run()) {
			$save_data = [];

			$save_si_kerjasama = $this->model_api_si_kerjasama->change($this->post('id'), $save_data);

			if ($save_si_kerjasama) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);
			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /si_kerjasama/delete Delete Si kerjasama. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteSi kerjasama
	 * @apiGroup si_kerjasama
	 * @apiHeader {String} X-Api-Key Si kerjasamas unique access-key.
	 * @apiPermission Si kerjasama Cant be Accessed permission name : api_si_kerjasama_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Si kerjasamas .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_si_kerjasama_delete', false);

		$si_kerjasama = $this->model_api_si_kerjasama->find($this->post('id'));

		if (!$si_kerjasama) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Si kerjasama not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_si_kerjasama->remove($this->post('id'));
		}

		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Si kerjasama deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Si kerjasama not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
}

/* End of file Si kerjasama.php */
/* Location: ./application/controllers/api/Si kerjasama.php */