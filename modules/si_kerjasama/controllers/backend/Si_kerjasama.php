<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Si Kerjasama Controller
 *| --------------------------------------------------------------------------
 *| Si Kerjasama site
 *|
 */
class Si_kerjasama extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_si_kerjasama');
		$this->load->model('../../mitra/models/model_mitra');
		$this->load->model('ref_prodi/Model_ref_prodi', 'model_ref_prodi');
		$this->load->model('model_si_bentuk_detil');
		$this->lang->load('web_lang', $this->current_lang);
		//cek_isi_survey();
	}

	public function filter_session_data()
	{
		$fields['jenis_ks']  = $this->input->post('jenis_ks');
		$fields['tahun']     = $this->input->post('tahun');
		$fields['negara']    = $this->input->post('negara');
		$fields['scope']     = $this->input->post('scope');
		$fields['doc_tipe']  = $this->input->post('doc_tipe');
		$fields['kd_prodi']  = $this->input->post('kd_prodi');
		$fields['is_aktif']  = $this->input->post('is_aktif');
		$fields['is_report'] = $this->input->post('is_report');
		$fields['bentuk_ks'] = $this->input->post('bentuk_ks');
		$this->session->set_userdata('filterdata_moa', $fields);
		$this->session->set_userdata('is_filtered_moa', TRUE);
		echo json_encode(['url' => base_url('administrator/si_kerjasama')]);
	}
	public function clear_filter_session_data()
	{
		$this->session->unset_userdata('filterdata_moa');
		$this->session->unset_userdata('is_filtered_moa');
		echo json_encode(['url' => base_url('administrator/si_kerjasama')]);
	}
	public function form_filter_data()
	{
		if (!$this->session->has_userdata('filterdata_moa')) {
			$fields = ['jenis_ks' => 'all', 'tahun' => 'all', 'negara' => 'all', 'is_aktif' => 'all', 'kd_prodi' => 'all', 'scope' => 'all', 'doc_tipe' => 'all', 'bentuk_ks' => null, 'is_report' => 'all'];
			$this->session->set_userdata('filterdata_moa', $fields);
		}

		$negara  = array_column(db_get_all_data('negara'), 'nama_negara', 'nama_negara');
		$tahun   = array_combine(range(2015, date('Y')), range(2015, date('Y')));
		$jenisks = array_column($this->model_si_kerjasama->getJenisKerjasama(), 'jenis_ks_concat', 'id');
		$tmp     = get_filter_prodi();
		// dd($tmp);
		foreach ($tmp as $p) {
			$prodi[$p->kode] = $p->kode . ' - ' . $p->nama_prodi;
		}
		$bentukks         = array_column(db_get_all_data('si_bentuk_ks'), 'bentuk_ks', 'id');
		$prodi['all']     = 'ALL';
		$negara['all']    = 'ALL';
		$tahun['all']     = 'ALL';
		$jenisks['all']   = 'ALL';
		$data['jenisks']  = $jenisks;
		$data['tahun']    = $tahun;
		$data['negara']   = $negara;
		$data['bentukks'] = $bentukks;
		$data['prodi']    = $prodi;
		$data['doctipe']  = ['MoA' => 'MoA', 'MoM' => 'MoM', 'LoA' => 'LoA', 'LoI' => 'LoI', 'IA' => 'IA', 'all' => 'ALL'];
		$data['status']   = array('1' => 'Aktif', '0' => 'Selesai', 'all' => 'ALL');
		$data['scope']    = array('DN' => 'DN', 'LN' => 'LN', 'all' => 'ALL');
		$this->load->view('form-filter-data', $data);
	}
	/**
	 * show all Si Kerjasamas
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('si_kerjasama_list');
		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('q', $q);
		if (!is_null($f)) $this->session->set_userdata('f', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('q');
			$this->session->unset_userdata('f');
		}
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['si_kerjasamas'] = $this->model_si_kerjasama->get($filter, $field, $this->limit_page, $offset)->model_result();
		$this->data['si_kerjasama_counts'] = $this->model_si_kerjasama->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/si_kerjasama/index/',
			'total_rows'   => $this->model_si_kerjasama->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Tabel Kerjasama List');
		$this->render('backend/standart/administrator/si_kerjasama/si_kerjasama_list', $this->data);
	}

	/**
	 * Add new si_kerjasamas
	 *
	 */
	public function add()
	{
		$this->is_allowed('si_kerjasama_add');
		$this->template->title('Tabel Kerjasama New');
		$filter_prodi               = get_filter_prodi();
		$this->data['filter_prodi'] = $filter_prodi;
		$this->data['jenis_ks']     = $this->model_si_kerjasama->getJenisKerjasama();
		$this->render('backend/standart/administrator/si_kerjasama/si_kerjasama_add', $this->data);
	}

	/**
	 * Add New Si Kerjasamas
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		// dd($this->input->post());
		if (!$this->is_allowed('si_kerjasama_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('kd_prodi', 'Program Studi', 'trim|required');
		$this->form_validation->set_rules('scope', 'Scope', 'trim|required');
		$this->form_validation->set_rules('bentuk_ks[]', 'Bentuk Kerjasama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('doc_tipe', 'Tipe Dokumen', 'trim|required');
		$this->form_validation->set_rules('doc_nomor', 'Nomor Dokumen', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('doc_tanggal', 'Tanggal Dokumen', 'trim|required');
		$this->form_validation->set_rules('si_kerjasama_doc_file_name', 'File Dokumen', 'trim|required');
		$this->form_validation->set_rules('negara', 'Negara', 'trim|required');
		$this->form_validation->set_rules('nama_mitra', 'Nama Mitra', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('penanggung_jawab', 'Penanggung Jawab', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('penanggung_jawab_mitra', 'Mitra Penanggung Jawab', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|min_length[10]');
		$this->form_validation->set_rules('mou_ks', 'Nota Kesepahaman', 'required');
		$this->form_validation->set_rules('jenis_ks', 'Jenis Kerjasama', 'required');
		//$this->form_validation->set_rules('si_kerjasama_foto1_name', 'Bukti Foto 1', 'trim|required');
		//$this->form_validation->set_rules('si_kerjasama_foto2_name', 'Bukti Foto 2', 'trim|required');


		if ($this->form_validation->run()) {
			$si_kerjasama_doc_file_uuid = $this->input->post('si_kerjasama_doc_file_uuid');
			$si_kerjasama_doc_file_name = $this->input->post('si_kerjasama_doc_file_name');
			$si_kerjasama_foto1_uuid = $this->input->post('si_kerjasama_foto1_uuid');
			$si_kerjasama_foto1_name = $this->input->post('si_kerjasama_foto1_name');
			$si_kerjasama_foto2_uuid = $this->input->post('si_kerjasama_foto2_uuid');
			$si_kerjasama_foto2_name = $this->input->post('si_kerjasama_foto2_name');

			//cek tanggal mulai kerjasama harus diantara tgl MoU
			$tgl_mulai = DateTime::createFromFormat('Y-m-d', $this->input->post('tgl_mulai'));
			$tgl_selesai = DateTime::createFromFormat('Y-m-d', $this->input->post('tgl_selesai'));
			if ($tgl_mulai > $tgl_selesai) {
				echo json_encode([
					'success' => false,
					'message' => cclang('Tanggal selesai melampaui dari tanggal mulai')
				]);
				exit;
			}
			if ($this->input->post('mou_ks') != '1') {
				$mou = db_get_all_data('mou_kerjasama', ['id' => $this->input->post('mou_ks')], 'row');
				if (!is_null($mou)) {
					$tgl_selesai_mou = DateTime::createFromFormat('Y-m-d', $mou->tgl_selesai);
					if ($tgl_mulai > $tgl_selesai_mou) {
						echo json_encode([
							'success' => false,
							'message' => cclang('Tanggal mulai kerjasama melampaui masa berlaku pada MoU')
						]);
						exit;
					}
				}
			}

			// simpan kode_fak
			$fak = $this->model_ref_prodi->get_single(['kode' => $this->input->post('kd_prodi')]);

			$save_data = [
				'kode_fak'               => $fak->kode_fak,
				'kd_prodi'               => $this->input->post('kd_prodi'),
				'scope'                  => $this->input->post('scope'),
				'mou_ks'                 => $this->input->post('mou_ks'),
				'jenis_ks'               => $this->input->post('jenis_ks'),
				'doc_tipe'               => $this->input->post('doc_tipe'),
				'doc_nomor'              => $this->input->post('doc_nomor'),
				'doc_tanggal'            => $this->input->post('doc_tanggal'),
				'negara'                 => $this->input->post('negara'),
				'tgl_mulai'              => $this->input->post('tgl_mulai'),
				'tgl_selesai'            => $this->input->post('tgl_selesai'),
				'dana_pendamping'        => preg_replace('/[^0-9]/', '', $this->input->post('dana_pendamping')),
				'dana_provit'            => preg_replace('/[^0-9]/', '', $this->input->post('dana_provit')),
				'id_mitra'               => $this->input->post('id_mitra'),
				'nama_mitra'             => $this->input->post('nama_mitra'),
				'penanggung_jawab'       => $this->input->post('penanggung_jawab'),
				'pj_email'               => $this->input->post('pj_email'),
				'pj_tlp'                 => $this->input->post('pj_tlp'),
				'pj_nip'                 => $this->input->post('pj_nip'),
				'penanggung_jawab_mitra' => $this->input->post('penanggung_jawab_mitra'),
				'pj_mitra_email'         => $this->input->post('pj_mitra_email'),
				'pj_mitra_tlp'           => $this->input->post('pj_mitra_tlp'),
				'pj_mitra_alamat'        => $this->input->post('pj_mitra_alamat'),
				'deskripsi'              => $this->input->post('deskripsi'),
				'validasi'               => $this->input->post('validasi'),
				'tgl_input'              => date('Y-m-d'),
				'created_by'             => $this->session->userdata('username'),
			];

			if (!is_dir(FCPATH . '/uploads/si_kerjasama/')) {
				mkdir(FCPATH . '/uploads/si_kerjasama/');
			}

			if (!empty($si_kerjasama_doc_file_name)) {
				$si_kerjasama_doc_file_name_copy = date('YmdHis') . '-' . $si_kerjasama_doc_file_name;

				rename(
					FCPATH . 'uploads/tmp/' . $si_kerjasama_doc_file_uuid . '/' . $si_kerjasama_doc_file_name,
					FCPATH . 'uploads/si_kerjasama/' . $si_kerjasama_doc_file_name_copy
				);

				if (!is_file(FCPATH . '/uploads/si_kerjasama/' . $si_kerjasama_doc_file_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['doc_file'] = $si_kerjasama_doc_file_name_copy;
			}

			if (!empty($si_kerjasama_foto1_name)) {
				$si_kerjasama_foto1_name_copy = date('YmdHis') . '-' . $si_kerjasama_foto1_name;

				rename(
					FCPATH . 'uploads/tmp/' . $si_kerjasama_foto1_uuid . '/' . $si_kerjasama_foto1_name,
					FCPATH . 'uploads/si_kerjasama/' . $si_kerjasama_foto1_name_copy
				);

				if (!is_file(FCPATH . '/uploads/si_kerjasama/' . $si_kerjasama_foto1_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['foto1'] = $si_kerjasama_foto1_name_copy;
			}

			if (!empty($si_kerjasama_foto2_name)) {
				$si_kerjasama_foto2_name_copy = date('YmdHis') . '-' . $si_kerjasama_foto2_name;

				rename(
					FCPATH . 'uploads/tmp/' . $si_kerjasama_foto2_uuid . '/' . $si_kerjasama_foto2_name,
					FCPATH . 'uploads/si_kerjasama/' . $si_kerjasama_foto2_name_copy
				);

				if (!is_file(FCPATH . '/uploads/si_kerjasama/' . $si_kerjasama_foto2_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['foto2'] = $si_kerjasama_foto2_name_copy;
			}
			$bentuk_ks = (array) $this->input->post('bentuk_ks');

			if (count($bentuk_ks) > 0)
				$save_data['bentuk_ks'] = implode(',', $this->input->post('bentuk_ks'));
			else
				$save_data['bentuk_ks'] = null;

			$save_si_kerjasama = $this->model_si_kerjasama->store($save_data);
			//new insert bentuk detil kerjasama
			$detil['id_moa'] = $save_si_kerjasama;
			foreach ($bentuk_ks as $id) {
				$detil['id_bentuk_ks'] = $id;
				$detil['id_sasaran'] = $this->input->post('sasaran')[$id];
				$detil['id_indikator'] = $this->input->post('indikator')[$id];
				$detil['nilai'] = $this->input->post('nilai')[$id];
				$detil['nilai_satuan'] = $this->input->post('nilai_satuan')[$id];
				$this->model_si_bentuk_detil->store($detil);
			}

			if ($save_si_kerjasama) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_si_kerjasama;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/si_kerjasama/edit/' . $save_si_kerjasama, 'Edit Si Kerjasama'),
						anchor('administrator/si_kerjasama', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/si_kerjasama/edit/' . $save_si_kerjasama, 'Edit Si Kerjasama')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/si_kerjasama');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/si_kerjasama');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Si Kerjasamas
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('si_kerjasama_update');

		$this->data['si_kerjasama'] = $this->model_si_kerjasama->find($id);

		if (!is_groups_in(['Admin', 'Kerjasama'])) :
			if ($this->data['si_kerjasama']->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa mengubah, karena data sudah divalidasi', 'si_kerjasama'), 'warning');
				redirect_back();
			}
		endif;

		$list_mou = db_get_all_data('mou_kerjasama', "scope='{$this->data['si_kerjasama']->scope}' AND tgl_selesai > curdate() ");
		$cur_mou = db_get_all_data('mou_kerjasama', "id = '{$this->data['si_kerjasama']->mou_ks}'", 'row');

		if (!is_null($cur_mou)) {
			$tgl = DateTime::createFromFormat('Y-m-d', $cur_mou->tgl_selesai);
			$now = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
			if ($tgl < $now) $list_mou[] = $cur_mou;
		}

		$list_mou[] = (object)['id' => '1', 'nama_mitra' => 'Pengajuan Baru'];
		$this->data['list_mou'] = $list_mou;
		$this->template->title('Tabel Kerjasama Update');
		$filter_prodi               = get_filter_prodi();
		$this->data['filter_prodi'] = $filter_prodi;
		$this->data['jenis_ks']     = $this->model_si_kerjasama->getJenisKerjasama();
		$this->render('backend/standart/administrator/si_kerjasama/si_kerjasama_update', $this->data);
	}

	/**
	 * Update Si Kerjasamas
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		// dd($this->input->post());
		if (!$this->is_allowed('si_kerjasama_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}
		$this->form_validation->set_rules('kd_prodi', 'Program Studi', 'trim|required');
		$this->form_validation->set_rules('scope', 'Scope', 'trim|required');
		$this->form_validation->set_rules('bentuk_ks[]', 'Bentuk Kerjasama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('doc_tipe', 'Tipe Dokumen', 'trim|required');
		$this->form_validation->set_rules('doc_nomor', 'Nomor Dokumen', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('doc_tanggal', 'Tanggal Dokumen', 'trim|required');
		$this->form_validation->set_rules('si_kerjasama_doc_file_name', 'File Dokumen', 'trim|required');
		$this->form_validation->set_rules('negara', 'Negara', 'trim|required');
		$this->form_validation->set_rules('nama_mitra', 'Nama Mitra', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('penanggung_jawab', 'Penanggung Jawab', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('penanggung_jawab_mitra', 'Mitra Penanggung Jawab', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|min_length[10]');
		$this->form_validation->set_rules('mou_ks', 'Nota Kesepahaman', 'required');
		$this->form_validation->set_rules('jenis_ks', 'Jenis Kerjasama', 'required');
		//$this->form_validation->set_rules('si_kerjasama_foto1_name', 'Bukti Foto 1', 'trim|required');
		//$this->form_validation->set_rules('si_kerjasama_foto2_name', 'Bukti Foto 2', 'trim|required');

		if ($this->form_validation->run()) {
			$si_kerjasama_doc_file_uuid = $this->input->post('si_kerjasama_doc_file_uuid');
			$si_kerjasama_doc_file_name = $this->input->post('si_kerjasama_doc_file_name');
			$si_kerjasama_foto1_uuid = $this->input->post('si_kerjasama_foto1_uuid');
			$si_kerjasama_foto1_name = $this->input->post('si_kerjasama_foto1_name');
			$si_kerjasama_foto2_uuid = $this->input->post('si_kerjasama_foto2_uuid');
			$si_kerjasama_foto2_name = $this->input->post('si_kerjasama_foto2_name');

			//cek tanggal mulai kerjasama harus diantara tgl MoU
			$tgl_mulai = DateTime::createFromFormat('Y-m-d', $this->input->post('tgl_mulai'));
			$tgl_selesai = DateTime::createFromFormat('Y-m-d', $this->input->post('tgl_selesai'));
			if ($tgl_mulai > $tgl_selesai) {
				echo json_encode([
					'success' => false,
					'message' => cclang('Tanggal selesai melampaui dari tanggal mulai')
				]);
				exit;
			}
			if ($this->input->post('mou_ks') != '1') {
				$mou = db_get_all_data('mou_kerjasama', ['id' => $this->input->post('mou_ks')], 'row');
				if (!is_null($mou)) {
					$tgl_selesai_mou = DateTime::createFromFormat('Y-m-d', $mou->tgl_selesai);
					if ($tgl_mulai > $tgl_selesai_mou) {
						echo json_encode([
							'success' => false,
							'message' => cclang('Tanggal mulai kerjasama melampaui masa berlaku pada MoU')
						]);
						exit;
					}
				}
			}

			// simpan kode_fak
			$fak = $this->model_ref_prodi->get_single(['kode' => $this->input->post('kd_prodi')]);

			$save_data = [
				'kode_fak'               => $fak->kode_fak,
				'kd_prodi'               => $this->input->post('kd_prodi'),
				'scope'                  => $this->input->post('scope'),
				'mou_ks'                 => $this->input->post('mou_ks'),
				'jenis_ks'               => $this->input->post('jenis_ks'),
				'doc_tipe'               => $this->input->post('doc_tipe'),
				'doc_nomor'              => $this->input->post('doc_nomor'),
				'doc_tanggal'            => $this->input->post('doc_tanggal'),
				'negara'                 => $this->input->post('negara'),
				'tgl_mulai'              => $this->input->post('tgl_mulai'),
				'tgl_selesai'            => $this->input->post('tgl_selesai'),
				'dana_pendamping'        => preg_replace('/[^0-9]/', '', $this->input->post('dana_pendamping')),
				'dana_provit'            => preg_replace('/[^0-9]/', '', $this->input->post('dana_provit')),
				'id_mitra'               => $this->input->post('id_mitra'),
				'nama_mitra'             => $this->input->post('nama_mitra'),
				'penanggung_jawab'       => $this->input->post('penanggung_jawab'),
				'pj_email'               => $this->input->post('pj_email'),
				'pj_tlp'                 => $this->input->post('pj_tlp'),
				'pj_nip'                 => $this->input->post('pj_nip'),
				'penanggung_jawab_mitra' => $this->input->post('penanggung_jawab_mitra'),
				'pj_mitra_email'         => $this->input->post('pj_mitra_email'),
				'pj_mitra_tlp'           => $this->input->post('pj_mitra_tlp'),
				'pj_mitra_alamat'        => $this->input->post('pj_mitra_alamat'),
				'deskripsi'              => $this->input->post('deskripsi'),
				'validasi'               => $this->input->post('validasi'),
				'perbaikan'              => $this->input->post('validasi') == 'Y' ? '' : $this->input->post('perbaikan'),
			];

			if (!is_dir(FCPATH . '/uploads/si_kerjasama/')) {
				mkdir(FCPATH . '/uploads/si_kerjasama/');
			}

			if (!empty($si_kerjasama_doc_file_uuid)) {
				$si_kerjasama_doc_file_name_copy = date('YmdHis') . '-' . $si_kerjasama_doc_file_name;

				rename(
					FCPATH . 'uploads/tmp/' . $si_kerjasama_doc_file_uuid . '/' . $si_kerjasama_doc_file_name,
					FCPATH . 'uploads/si_kerjasama/' . $si_kerjasama_doc_file_name_copy
				);

				if (!is_file(FCPATH . '/uploads/si_kerjasama/' . $si_kerjasama_doc_file_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['doc_file'] = $si_kerjasama_doc_file_name_copy;
			}

			if (!empty($si_kerjasama_foto1_uuid)) {
				$si_kerjasama_foto1_name_copy = date('YmdHis') . '-' . $si_kerjasama_foto1_name;

				rename(
					FCPATH . 'uploads/tmp/' . $si_kerjasama_foto1_uuid . '/' . $si_kerjasama_foto1_name,
					FCPATH . 'uploads/si_kerjasama/' . $si_kerjasama_foto1_name_copy
				);

				if (!is_file(FCPATH . '/uploads/si_kerjasama/' . $si_kerjasama_foto1_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['foto1'] = $si_kerjasama_foto1_name_copy;
			}

			if (!empty($si_kerjasama_foto2_uuid)) {
				$si_kerjasama_foto2_name_copy = date('YmdHis') . '-' . $si_kerjasama_foto2_name;

				rename(
					FCPATH . 'uploads/tmp/' . $si_kerjasama_foto2_uuid . '/' . $si_kerjasama_foto2_name,
					FCPATH . 'uploads/si_kerjasama/' . $si_kerjasama_foto2_name_copy
				);

				if (!is_file(FCPATH . '/uploads/si_kerjasama/' . $si_kerjasama_foto2_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['foto2'] = $si_kerjasama_foto2_name_copy;
			}

			$bentuk_ks = (array) $this->input->post('bentuk_ks');

			if (count($bentuk_ks) > 0)
				$save_data['bentuk_ks'] = implode(',', $this->input->post('bentuk_ks'));
			else
				$save_data['bentuk_ks'] = null;

			$save_si_kerjasama = $this->model_si_kerjasama->change($id, $save_data);
			// send notification to user
			if(is_groups_in(['Admin','Kerjasama'])){
				if($this->input->post('validasi') == 'N'){
					antrianTelegram('Ada input kerjasama yang harus anda diperbaiki.', [$this->model_si_kerjasama->created_by]);
				}
			}

			//update bentuk detil kerjasama
			$res = $this->model_si_bentuk_detil->get_where(['id_moa' => $id]);
			$b = [];

			foreach ($res as $row) {
				if (!in_array($row->id_bentuk_ks, $bentuk_ks)) $this->model_si_bentuk_detil->remove($row->id);
				$b[] = $row->id_bentuk_ks;
			}
			$b1 = false;
			$b2 = false;
			foreach ($bentuk_ks as $i) {
				if (!in_array($i, $b)) {
					$detil['id_moa'] = $id;
					$detil['id_bentuk_ks'] = $i;
					$detil['id_sasaran'] = $this->input->post('sasaran')[$i];
					$detil['id_indikator'] = $this->input->post('indikator')[$i];
					$b1 = $this->model_si_bentuk_detil->store($detil);
				}
				if (isset($this->input->post('sasaran')[$i])) {
					$where['id_moa'] = $id;
					$where['id_bentuk_ks'] = $i;
					$detil['id_sasaran'] = $this->input->post('sasaran')[$i];
					$b2 = $this->model_si_bentuk_detil->updateDetailBentuk($where, $detil);
				}
				if (isset($this->input->post('indikator')[$i])) {
					$where['id_moa'] = $id;
					$where['id_bentuk_ks'] = $i;
					$detil['id_indikator'] = $this->input->post('indikator')[$i];
					$b2 = $this->model_si_bentuk_detil->updateDetailBentuk($where, $detil);
				}
				if (isset($this->input->post('nilai')[$i])) {
					$where['id_moa'] = $id;
					$where['id_bentuk_ks'] = $i;
					$detil['nilai'] = $this->input->post('nilai')[$i];
					$b3 = $this->model_si_bentuk_detil->updateDetailBentuk($where, $detil);
				}
				if (isset($this->input->post('nilai_satuan')[$i])) {
					$where['id_moa'] = $id;
					$where['id_bentuk_ks'] = $i;
					$detil['nilai_satuan'] = $this->input->post('nilai_satuan')[$i];
					$b4 = $this->model_si_bentuk_detil->updateDetailBentuk($where, $detil);
				}
			}

			if ($save_si_kerjasama || ($b1) || ($b2) || ($b3) || ($b4)) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/si_kerjasama', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/si_kerjasama/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/si_kerjasama/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$errros = [];
			foreach ($this->form_validation->error_array() as $key => $value) {
				$newKey = rtrim($key, '[]');
				$errros[$newKey] = $value;
			}
			$this->data['errors'] = $errros;
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Si Kerjasamas
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('si_kerjasama_delete');

		/** grup kaprodi diberi akses delete di modul ini, tp dibatasi akses deletenya  prodi wilayahnya dia saja
		 * mesi sudah di filter di list , jaga2 kalo akses url langsung */

		if (is_groups_in(['Kaprodi', 'Unit'])) {
			$prodi = get_filter_prodi(true);
			$row = $this->model_si_kerjasama->find($id);
			if (!in_array($row->kd_prodi, $prodi)) {
				set_message(cclang('sorry_you_do_not_have_permission_to_access', 'si_kerjasama'), 'error');
				redirect_back();
			}
			/**tolak jika sudah divalidasi */
			if ($row->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa menghapus data, karena data sudah divalidasi', 'si_kerjasama'), 'warning');
				redirect_back();
			}
		}


		$this->load->helper('file');
		$arr_id = $this->input->get('id');
		$remove = false;
		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}
		if ($remove) {
			set_message(cclang('has_been_deleted', 'si_kerjasama'), 'success');
		} else {
			set_message(cclang('error_delete', 'si_kerjasama'), 'error');
		}
		redirect_back();
	}

	/**
	 * View view Si Kerjasamas
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('si_kerjasama_view');

		$this->data['si_kerjasama'] = $this->model_si_kerjasama->join_avaiable()->filter_avaiable()->find($id);
		$this->data['jenis_ks'] = array_column($this->model_si_kerjasama->getJenisKerjasama(), 'jenis_ks_concat', 'id');
		$this->template->title('Tabel Kerjasama Detail');
		$this->render('backend/standart/administrator/si_kerjasama/si_kerjasama_view', $this->data);
	}

	/**
	 * delete Si Kerjasamas
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$si_kerjasama = $this->model_si_kerjasama->find($id);

		if (!empty($si_kerjasama->doc_file)) {
			$path = FCPATH . '/uploads/si_kerjasama/' . $si_kerjasama->doc_file;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($si_kerjasama->foto1)) {
			$path = FCPATH . '/uploads/si_kerjasama/' . $si_kerjasama->foto1;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($si_kerjasama->foto2)) {
			$path = FCPATH . '/uploads/si_kerjasama/' . $si_kerjasama->foto2;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}


		return $this->model_si_kerjasama->remove($id);
	}

	/**
	 * Upload Image Si Kerjasama	* 
	 * @return JSON
	 */
	public function upload_doc_file_file()
	{
		if (!$this->is_allowed('si_kerjasama_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'si_kerjasama',
			'allowed_types' => 'jpg|pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Si Kerjasama	* 
	 * @return JSON
	 */
	public function delete_doc_file_file($uuid)
	{
		if (!$this->is_allowed('si_kerjasama_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'doc_file',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'si_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/si_kerjasama/'
		]);
	}

	/**
	 * Delete Image Si Kerjasama	* 
	 * @return JSON
	 */
	public function delete_laporan_file_file($uuid)
	{
		if (!$this->is_allowed('si_kerjasama_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'laporan_file',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'si_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/si_kerjasama/'
		]);
	}

	/**
	 * Get Image Si Kerjasama	* 
	 * @return JSON
	 */
	public function get_doc_file_file($id)
	{
		if (!$this->is_allowed('si_kerjasama_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$si_kerjasama = $this->model_si_kerjasama->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'doc_file',
			'table_name'        => 'si_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/si_kerjasama/',
			'delete_endpoint'   => 'administrator/si_kerjasama/delete_doc_file_file'
		]);
	}
	
	/**
	 * Upload laporan Si Kerjasama	* 
	 * @return JSON
	 */
	public function get_laporan_file_file($id)
	{
		if (!$this->is_allowed('si_kerjasama_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$si_kerjasama = $this->model_si_kerjasama->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'laporan_file',
			'table_name'        => 'si_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/si_kerjasama/',
			'delete_endpoint'   => 'administrator/si_kerjasama/delete_laporan_file_file'
		]);
	}

	/**
	 * Upload Image Si Kerjasama	* 
	 * @return JSON
	 */
	public function upload_foto1_file()
	{
		if (!$this->is_allowed('si_kerjasama_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'si_kerjasama',
			'allowed_types' => 'jpg|jpeg',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Upload Laporan Si Kerjasama	* 
	 * @return JSON
	 */
	public function upload_laporan_file_file()
	{
		if (!$this->is_allowed('si_kerjasama_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'si_kerjasama',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Si Kerjasama	* 
	 * @return JSON
	 */
	public function delete_foto1_file($uuid)
	{
		if (!$this->is_allowed('si_kerjasama_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'foto1',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'si_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/si_kerjasama/'
		]);
	}

	/**
	 * Get Image Si Kerjasama	* 
	 * @return JSON
	 */
	public function get_foto1_file($id)
	{
		if (!$this->is_allowed('si_kerjasama_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$si_kerjasama = $this->model_si_kerjasama->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'foto1',
			'table_name'        => 'si_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/si_kerjasama/',
			'delete_endpoint'   => 'administrator/si_kerjasama/delete_foto1_file'
		]);
	}

	/**
	 * Upload Image Si Kerjasama	* 
	 * @return JSON
	 */
	public function upload_foto2_file()
	{
		if (!$this->is_allowed('si_kerjasama_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'si_kerjasama',
			'allowed_types' => 'jpg|jpeg',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Si Kerjasama	* 
	 * @return JSON
	 */
	public function delete_foto2_file($uuid)
	{
		if (!$this->is_allowed('si_kerjasama_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'foto2',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'si_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/si_kerjasama/'
		]);
	}

	/**
	 * Get Image Si Kerjasama	* 
	 * @return JSON
	 */
	public function get_foto2_file($id)
	{
		if (!$this->is_allowed('si_kerjasama_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$si_kerjasama = $this->model_si_kerjasama->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'foto2',
			'table_name'        => 'si_kerjasama',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/si_kerjasama/',
			'delete_endpoint'   => 'administrator/si_kerjasama/delete_foto2_file'
		]);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('si_kerjasama_export');

		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
			'scope',
			'si_jenis_ks_jenis_ks',
			'doc_nomor',
			'doc_tipe',
			'doc_mou',
			'doc_file',
			'bentuk_kerjasama',
			'doc_tanggal',
			'tahun',
			'tgl_mulai',
			'tgl_selesai',
			'dana_pendamping',
			'dana_provit',
			'nama_mitra',
			'negara',
			'penanggung_jawab',
			'penanggung_jawab_mitra',
			'deskripsi',
			'ref_prodi_nama_prodi',
			'nama_fak_unit',
			'nama_mou_kerjasama',
			'link_laporan',
			'status',
			'sisa_bulan',
		];
		$format = ['doc_file' => 'link', 'doc_mou' => 'link', 'link_laporan' => 'url'];
		$sql = $this->model_si_kerjasama->get($filter, $field, 0, $offset, [])->model_sql();
		$result = $this->model_si_kerjasama->insertJobExport($sql, 'Kerja sama Unit', $format, $selected_field);

		echo json_encode(['result' => $result]);

		// $data = $this->model_si_kerjasama->get($filter, $field, 0, $offset, [])->model_object();
		// $this->model_si_kerjasama->export2($data, 'file', $selected_field, $format);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('si_kerjasama_export');

		$this->model_si_kerjasama->pdf('si_kerjasama', 'si_kerjasama');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('si_kerjasama_export');

		$table = $title = 'si_kerjasama';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_si_kerjasama->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
	public function get_option_mou()
	{
		echo $this->model_si_kerjasama->get_option_mou($this->input->get());
	}
	public function get_option_moa()
	{
		echo $this->model_si_kerjasama->get_option_moa($this->input->get());
	}
	public function form_input_perkembangan()
	{
		$data['id_moa'] = $this->input->get('id');
		$this->load->view('form-update-perkembangan', $data);
	}
	public function form_cari_mitra()
	{
		// dd(get_filter_prodi(true));
		// dd(['120140','120141','120142']);
		$page = $this->input->get('page');
		$data['page'] = (is_null($page) ? 2 : 1);
		$this->load->view('form-cari-mitra', $data);
	}
	public function browse_mitra()
	{
		$filter_prodi = $this->input->post('filter_prodi');
		$page = $this->input->post('page');
		echo $this->model_mitra->browse_mitra($filter_prodi, $page);
	}

	public function browse_bentuk_ks()
	{
		echo $this->model_si_bentuk_detil->browse_bentuk_ks($this->input->get('id_moa'));
	}
	public function update_nilai_bentuk_ks()
	{
		$this->model_si_bentuk_detil->change($this->input->post('id'), ['nilai' => $this->input->post('nilai')]);
	}
	public function getFormDetilBentuk_ks()
	{
		$data['bentukname'] = $this->input->get('bentukname');
		$data['div'] = $this->input->get('div');
		$data['idbentuk'] = $this->input->get('id');
		$data['satuan'] = [
			'mahasiswa' => 'mahasiswa',
			'dosen' => 'dosen',
			'judul' => 'judul',
			'buku' => 'buku',
			'artikel' => 'artikel',
			'sks' => 'sks',
			'rupiah' => 'rupiah'
		];
		if ($this->input->get('mode') == 'edit') {
			$where['id_moa'] = $this->input->get('id_moa');
			$where['id_bentuk_ks'] = $this->input->get('id');
			$detil = db_get_all_data('bentuk_ks_detil', $where, 'row');
			// dd($detil);
			if (is_null($detil)) {
				$data['id_sasaran'] = null;
				$data['id_indikator'] = null;
				$data['nilai'] = null;
				$data['nilai_satuan'] = null;
			} else {
				$data['id_sasaran'] = $detil->id_sasaran;
				$data['id_indikator'] = $detil->id_indikator;
				$data['nilai'] = $detil->nilai;
				$data['nilai_satuan'] = $detil->nilai_satuan;
			}
		} else {
			$data['id_sasaran'] = null;
			$data['id_indikator'] = null;
			$data['nilai'] = null;
			$data['nilai_satuan'] = null;
		}
		$data['ref_sasaran'] = $this->model_si_bentuk_detil->getSasaran($this->input->get('id'));
		$data['ref_indikator'] = $this->model_si_bentuk_detil->getIndikator($data['id_sasaran']);
		$this->load->view('form-detil-bentuk-ks', $data);
	}
	public function get_option_bentuk_indikator()
	{
		$data = $this->model_si_bentuk_detil->getIndikator($this->input->get('id_sasaran'));
		echo build_query_option($data, 'id', 'nama_indikator', ['id_sasaran' => $this->input->get('id_sasaran')]);
	}

	public function laporan($id)
	{
		$this->is_allowed('si_kerjasama_update');
		$this->data['si_kerjasama'] = $this->model_si_kerjasama->find($id);
		$this->template->title('Tabel Kerjasama Update');
		$this->render('backend/standart/administrator/si_kerjasama/si_kerjasama_update_laporan', $this->data);
	}

	public function laporan_save($id)
	{
		if (!$this->is_allowed('si_kerjasama_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}
		$this->form_validation->set_rules('bentuk_ks[]', 'Bentuk Kerjasama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('desc_laporan', 'Deskripsi laporan', 'trim|required|min_length[10]');
		$this->form_validation->set_rules('pj_nip', 'Nip penanggung jawab', 'trim|required');
		$this->form_validation->set_rules('links', 'Link dokumentasi', 'trim|required');
		$this->form_validation->set_rules('pj_ttd_id', 'Penanggung Jawab Tanda Tangan', 'required');
		$this->form_validation->set_rules('pejabat_ttd_id', 'Tanda Tangan Pejabat', 'required');
		// $this->form_validation->set_rules('pj_mitra_ttd_id', 'Tanda Tangan Mitra', 'required');
		if($this->input->post('use_file_upload_laporan') == '1'){
			$this->form_validation->set_rules('si_kerjasama_laporan_file_name', 'File Laporan', 'trim|required');
		}

		if ($this->form_validation->run()) {
			$si_kerjasama_foto1_uuid        = $this->input->post('si_kerjasama_foto1_uuid');
			$si_kerjasama_foto1_name        = $this->input->post('si_kerjasama_foto1_name');
			$si_kerjasama_laporan_file_uuid = $this->input->post('si_kerjasama_laporan_file_uuid');
			$si_kerjasama_laporan_file_name = $this->input->post('si_kerjasama_laporan_file_name');

			$save_data = [
				'desc_laporan'            => $this->input->post('desc_laporan'),
				'links'                   => $this->input->post('links'),
				'pj_nip'                  => $this->input->post('pj_nip'),
				'pj_ttd_id'               => $this->input->post('pj_ttd_id'),
				'is_report'               => $this->input->post('is_report'),
				'pejabat_nip'             => $this->input->post('pejabat_nip'),
				'pejabat_nama'            => $this->input->post('pejabat_nama'),
				'pejabat_ttd_id'          => $this->input->post('pejabat_ttd_id'),
				'pj_mitra_ttd_id'         => $this->input->post('pj_mitra_ttd_id'),
				'tgl_laporan'             => $this->input->post('tgl_laporan'),
				'use_file_upload_laporan' => $this->input->post('use_file_upload_laporan'),
			];

			if (!is_dir(FCPATH . '/uploads/si_kerjasama/')) {
				mkdir(FCPATH . '/uploads/si_kerjasama/');
			}


			if (!empty($si_kerjasama_foto1_uuid)) {
				$si_kerjasama_foto1_name_copy = date('YmdHis') . '-' . $si_kerjasama_foto1_name;

				rename(
					FCPATH . 'uploads/tmp/' . $si_kerjasama_foto1_uuid . '/' . $si_kerjasama_foto1_name,
					FCPATH . 'uploads/si_kerjasama/' . $si_kerjasama_foto1_name_copy
				);

				if (!is_file(FCPATH . '/uploads/si_kerjasama/' . $si_kerjasama_foto1_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['foto1'] = $si_kerjasama_foto1_name_copy;
			}

			if (!empty($si_kerjasama_laporan_file_uuid)) {
				$si_kerjasama_laporan_file_name_copy = date('YmdHis') . '-' . $si_kerjasama_laporan_file_name;

				rename(
					FCPATH . 'uploads/tmp/' . $si_kerjasama_laporan_file_uuid . '/' . $si_kerjasama_laporan_file_name,
					FCPATH . 'uploads/si_kerjasama/' . $si_kerjasama_laporan_file_name_copy
				);

				if (!is_file(FCPATH . '/uploads/si_kerjasama/' . $si_kerjasama_laporan_file_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['laporan_file'] = $si_kerjasama_laporan_file_name_copy;
			}

			$bentuk_ks = (array) $this->input->post('bentuk_ks');

			if (count($bentuk_ks) > 0)
				$save_data['bentuk_ks'] = implode(',', $this->input->post('bentuk_ks'));
			else
				$save_data['bentuk_ks'] = null;

			$save_si_kerjasama = $this->model_si_kerjasama->change($id, $save_data);

			//update bentuk detil kerjasama
			$res = $this->model_si_bentuk_detil->get_where(['id_moa' => $id]);
			$b = [];

			foreach ($res as $row) {
				if (!in_array($row->id_bentuk_ks, $bentuk_ks)) $this->model_si_bentuk_detil->remove($row->id);
				$b[] = $row->id_bentuk_ks;
			}
			$b1 = false;
			$b2 = false;
			foreach ($bentuk_ks as $i) {
				if (!in_array($i, $b)) {
					$detil['id_moa'] = $id;
					$detil['id_bentuk_ks'] = $i;
					$detil['id_sasaran'] = $this->input->post('sasaran')[$i];
					$detil['id_indikator'] = $this->input->post('indikator')[$i];
					$b1 = $this->model_si_bentuk_detil->store($detil);
				}
				if (isset($this->input->post('sasaran')[$i])) {
					$where['id_moa'] = $id;
					$where['id_bentuk_ks'] = $i;
					$detil['id_sasaran'] = $this->input->post('sasaran')[$i];
					$b2 = $this->model_si_bentuk_detil->updateDetailBentuk($where, $detil);
				}
				if (isset($this->input->post('indikator')[$i])) {
					$where['id_moa'] = $id;
					$where['id_bentuk_ks'] = $i;
					$detil['id_indikator'] = $this->input->post('indikator')[$i];
					$b2 = $this->model_si_bentuk_detil->updateDetailBentuk($where, $detil);
				}
				if (isset($this->input->post('nilai')[$i])) {
					$where['id_moa'] = $id;
					$where['id_bentuk_ks'] = $i;
					$detil['nilai'] = $this->input->post('nilai')[$i];
					$b3 = $this->model_si_bentuk_detil->updateDetailBentuk($where, $detil);
				}
				if (isset($this->input->post('nilai_satuan')[$i])) {
					$where['id_moa'] = $id;
					$where['id_bentuk_ks'] = $i;
					$detil['nilai_satuan'] = $this->input->post('nilai_satuan')[$i];
					$b4 = $this->model_si_bentuk_detil->updateDetailBentuk($where, $detil);
				}
			}

			if ($save_si_kerjasama || ($b1) || ($b2) || ($b3) || ($b4)) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/si_kerjasama', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/si_kerjasama/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/si_kerjasama/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$errros = [];
			foreach ($this->form_validation->error_array() as $key => $value) {
				$newKey = rtrim($key, '[]');
				$errros[$newKey] = $value;
			}
			$this->data['errors'] = $errros;
		}

		echo json_encode($this->data);
	}

	public function ajax_change_pj_nip($nip = null)
	{
		$results = db_get_all_data('karyawan_ttd', ['nip' => $nip]);
		$this->response($results);
	}

	public function laporan_preview($id)
	{
		$data = $this->model_si_kerjasama->getRekapById($id);
		$this->load->helper('string');
		$template	= $template = new \PhpOffice\PhpWord\TemplateProcessor(FCPATH . 'uploads/word/laporan_ks_unit.docx');
		$data = $this->model_si_kerjasama->getRekapById($id);
		$kata = stripos($data->nama_jenis_ks, 'kerja sama') !== 0 ? 'Kerja sama ' : '';
		$template->setValue('judul', $kata . $data->nama_jenis_ks . ', ' . $data->unit . ' dengan ' . htmlspecialchars($data->nama_mitra));
		$template->setValue('nomor', $data->doc_nomor);
		$template->setValue('tipe', $data->doc_tipe);
		$template->setValue('docfile', site_url('file/' . $data->doc_file));
		$template->setValue('mitra', htmlspecialchars($data->nama_mitra));
		$template->setValue('hasil', htmlspecialchars($data->desc_laporan));
		$template->setValue('penanggung_jawab', $data->ttd_pj_nama);
		$template->setValue('nip_pj', $data->ttd_pj_nip);
		$template->setValue('pj_jabatan', $data->ttd_pj_jabatan);
		$template->setValue('pejabat_jabatan', $data->ttd_pejabat_jabatan);
		$template->setValue('nama_pejabat', $data->ttd_pejabat_nama);
		$template->setValue('nip_pejabat', $data->ttd_pejabat_nip);
		$template->setValue('nama_ttd_mitra', $data->ttd_mitra_nama);
		$template->setValue('tanggal', format_tgl_surat($data->tgl_laporan));
		//ttd penanggung jawab
		if (file_exists(FCPATH . '/uploads/karyawan_ttd/' . $data->ttd_pj_file) && !empty($data->ttd_pj_file))
			$template->setImageValue('pj_ttd', ['path' => FCPATH . '/uploads/karyawan_ttd/' . $data->ttd_pj_file, 'width' => 200, 'height' => 85, 'ratio' => true]);
		else
			$template->setValue('pj_ttd', '[ file tandatangan tidak ada ]');
		// ttd pejabat
		if (file_exists(FCPATH . '/uploads/karyawan_ttd/' . $data->ttd_pejabat_file) && !empty($data->ttd_pejabat_file))
			$template->setImageValue('pejabat_ttd', ['path' => FCPATH . '/uploads/karyawan_ttd/' . $data->ttd_pejabat_file, 'width' => 200, 'height' => 85, 'ratio' => true]);
		else
			$template->setValue('pejabat_ttd', '[ file tandatangan tidak ada ]');
		// ttd mitra
		if (file_exists(FCPATH . '/uploads/mitra/' . $data->ttd_mitra_file) && !empty($data->ttd_mitra_file)){
			$template->setValue('captionmitra','Mitra');
			$template->setImageValue('ttd_mitra', ['path' => FCPATH . '/uploads/mitra/' . $data->ttd_mitra_file, 'width' => 200, 'height' => 85, 'ratio' => true]);
		}
		else{
			$template->setValue('captionmitra','');
			$template->setValue('ttd_mitra', '');
		}
		$template->setValue('foto', site_url('file/' . $data->foto1));
		$bentuk = $this->model_si_kerjasama->getDetailBentuk($id);
		// dd($bentuk);
		$template->cloneRow('bentuk_ks', count($bentuk));
		$template->cloneRow('item_bentuk_ks', count($bentuk));
		foreach ($bentuk as $i => $row) {
			$template->setValue('bentuk_ks#' . ($i + 1), $row->bentuk_ks);
			$template->setValue('item_bentuk_ks#' . ($i + 1), $row->bentuk_ks);
			$template->setValue('nama_sasaran#' . ($i + 1), $row->nama_sasaran);
			$template->setValue('nama_indikator#' . ($i + 1), $row->nama_indikator);
			$template->setValue('nilai#' . ($i + 1), $row->nilai);
			$template->setValue('satuan#' . ($i + 1), $row->nilai_satuan);
		}
		//link / tautan
		$lines = explode("\n", $data->links);
		$validUrls = array();
		foreach ($lines as $url) {
			$urlParts = parse_url($url);
			if ($urlParts !== false) {
				$validUrls[] = $url;
			}
		}
		$template->cloneRow('link', count($validUrls));
		foreach ($validUrls as $i => $link) {
			$template->setValue('link#' . ($i + 1), htmlspecialchars($link));
		}
		// die();
		$strname	= random_string('alnum', 8);
		$file_doc   = FCPATH . 'uploads/word/' . $strname . '.docx';
		$template->saveAS($file_doc);
		$file_pdf	= FCPATH . 'uploads/word/' . $strname . '.pdf';
		$unoconv 	= \Unoconv\Unoconv::create();
		$unoconv->transcode($file_doc, 'pdf', $file_pdf);
		ob_start();
		readfile($file_pdf);
		$content = ob_get_clean();
		@unlink($file_doc); //hapus doc
		@unlink($file_pdf); //hapus pdf
		header('Content-type: application/pdf');
		header('Content-Disposition: inline; filename=' . $data->kd_prodi . '_' . $data->id . '.pdf');
		echo $content;
	}
}
/* End of file si_kerjasama.php */
/* Location: ./application/controllers/administrator/Si Kerjasama.php */
