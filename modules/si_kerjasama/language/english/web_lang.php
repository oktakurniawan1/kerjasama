<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['si_kerjasama'] = 'Data Kerjasama Unit';
$lang['id'] = 'Id';
$lang['kd_prodi'] = 'Prodi/Unit';
$lang['scope'] = 'DN/LN';
$lang['bentuk_ks'] = 'Bentuk Kerjasama';
$lang['doc_tipe'] = 'Tipe Dokumen';
$lang['doc_nomor'] = 'Nomor Dokumen';
$lang['doc_tanggal'] = 'Tgl Dokumen';
$lang['doc_file'] = 'File Dokumen';
$lang['negara'] = 'Negara';
$lang['tgl_mulai'] = 'Tgl Mulai';
$lang['tgl_input'] = 'Tgl Input';
$lang['tgl_selesai'] = 'Tgl Selesai';
$lang['nama_mitra'] = 'Nama Mitra';
$lang['penanggung_jawab'] = 'Penanggung Jawab';
$lang['deskripsi'] = 'Deskripsi';
$lang['foto1'] = 'Bukti Foto 1';
$lang['foto2'] = 'Bukti Foto 2';
$lang['validasi'] = 'Validasi';
