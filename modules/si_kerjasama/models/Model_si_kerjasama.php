<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_si_kerjasama extends MY_Model
{

	private $primary_key    = 'id';
	private $table_name     = 'si_kerjasama';
	public $field_search   = ['kd_prodi', 'scope', 'jenis_ks', 'bentuk_ks', 'doc_tipe', 'doc_nomor', 'doc_tanggal', 'tgl_mulai', 'tgl_selesai', 'nama_mitra', 'penanggung_jawab', 'tgl_input', 'validasi', 'mou_ks', 'negara', 'doc_file', 'laporan_file', 'use_file_upload_laporan', 'perbaikan'];

	//public $export_data;

	public function __construct()
	{
		$config = array(
			'primary_key'   => $this->primary_key,
			'table_name'    => $this->table_name,
			'field_search'  => $this->field_search,
		);

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "si_kerjasama." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'mou_ks') $where .= "OR " . "mou_kerjasama.nama_mitra LIKE '%" . $q . "%' ";
					if ($field == 'kd_prodi') $where .= "OR " . "ref_prodi.nama_prodi LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "si_kerjasama." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'mou_ks') $where .= "OR " . "mou_kerjasama.nama_mitra LIKE '%" . $q . "%' ";
					if ($field == 'kd_prodi') $where .= "OR " . "ref_prodi.nama_prodi LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			if ($field == 'mou_ks') $where .= "(" . "mou_kerjasama.nama_mitra LIKE '%" . $q . "%' )";
			else if ($field == 'kd_prodi') $where .= "(" . "ref_prodi.nama_prodi LIKE '%" . $q . "%' )";
			elseif ($field == 'status') $where = NULL;
			else $where .= "(" . "si_kerjasama." . $field . " LIKE '%" . $q . "%' )";
		}
		$this->join_avaiable()->filter_avaiable();

		/**form filter data */
		if ($this->session->has_userdata('filterdata_moa')) {
			foreach (fromsess('filterdata_moa') as $key => $row) {
				if ($key != 'bentuk_ks') {
					if ($row != 'all') {
						if ($key == 'kd_prodi') {
							$db_instance = $this->load->database('default', TRUE);
							$ref = $db_instance->get_where('ref_prodi', ['kode' => $row])->row();
							$kode_fak = $ref->kode_fak;
							$tingkat = $ref->tingkat;
							if ($kode_fak != 0 && $tingkat == 2) {
								$prodi_list = implode(',', array_map(function ($value) {
									return "'$value'";
								}, array_column($db_instance->get_where('ref_prodi', ['kode_fak' => $kode_fak])->result_array(), 'kode')));
								$this->db->having("kd_prodi IN ($prodi_list)");
							} else $this->db->having("kd_prodi", $row);
						} else
							$this->db->having($key, $row);
					}
				}
				if (($key == 'bentuk_ks') && (is_array($row))) {
					$this->db->group_start();
					foreach ($row as $b) {
						// $this->db->or_where("LOCATE(concat(',',$b,','),concat(',',si_kerjasama.bentuk_ks,','))>0");
						$this->db->or_where("FIND_IN_SET($b,si_kerjasama.bentuk_ks)>0");
					}
					$this->db->group_end();
				}
			}
		}
		$this->db->order_by('si_kerjasama.id'); //fix bug when use HAVING sql with variable(@) field CI, must using  any order_by
		if (!is_null($where)) $this->db->where($where);
		$query = $this->db->get($this->table_name);
		//$this->export_data = $query;
		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "si_kerjasama." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'mou_ks') $where .= "OR " . "mou_kerjasama.nama_mitra LIKE '%" . $q . "%' ";
					if ($field == 'kd_prodi') $where .= "OR " . "ref_prodi.nama_prodi LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "si_kerjasama." . $field . " LIKE '%" . $q . "%' ";
					if ($field == 'mou_ks') $where .= "OR " . "mou_kerjasama.nama_mitra LIKE '%" . $q . "%' ";
					if ($field == 'kd_prodi') $where .= "OR " . "ref_prodi.nama_prodi LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			if ($field == 'mou_ks') $where .= "(" . "mou_kerjasama.nama_mitra LIKE '%" . $q . "%' )";
			elseif ($field == 'kd_prodi') $where .= "(" . "ref_prodi.nama_prodi LIKE '%" . $q . "%' )";
			elseif ($field == 'status') $where = NULL;
			else $where .= "(" . "si_kerjasama." . $field . " LIKE '%" . $q . "%' )";
		}
		if ($field == 'status') $this->db->having('status', $q);
		if (is_array($select_field) and count($select_field)) {
			$this->db->select($select_field);
		}

		$this->join_avaiable()->filter_avaiable();
		if (!is_null($where)) $this->db->where($where);

		/**form filter data */
		if ($this->session->has_userdata('filterdata_moa')) {
			foreach (fromsess('filterdata_moa') as $key => $row) {
				if ($key != 'bentuk_ks') {
					if ($row != 'all') {
						if ($key == 'kd_prodi') {
							$db_instance = $this->load->database('default', TRUE);
							$ref = $db_instance->get_where('ref_prodi', ['kode' => $row])->row();
							$kode_fak = $ref->kode_fak;
							$tingkat = $ref->tingkat;
							if ($kode_fak != '0' && $tingkat == '2') {
								$prodi_list = implode(',', array_map(function ($value) {
									return "'$value'";
								}, array_column($db_instance->get_where('ref_prodi', ['kode_fak' => $kode_fak])->result_array(), 'kode')));
								$this->db->having("kd_prodi IN ($prodi_list)");
							} else $this->db->having("kd_prodi", $row);
						} else {
							$this->db->having($key, $row);
						}
					}
				}
				if (($key == 'bentuk_ks') && (is_array($row))) {
					$this->db->group_start();
					foreach ($row as $b) {
						$this->db->or_where("LOCATE(concat(',',$b,','),concat(',',si_kerjasama.bentuk_ks,','))>0");
					}
					$this->db->group_end();
				}
			}
		}

		$this->db->limit($limit, $offset);

		$this->sortable();
		$this->db->select('CONCAT("' . site_url('laporan/unit/') . '",si_kerjasama.id) AS link_laporan', false);

		$this->query = $this->db;
		return $this;
	}

	public function join_avaiable()
	{
		$this->db->join('si_jenis_ks', 'si_jenis_ks.id = si_kerjasama.jenis_ks', 'LEFT');
		$this->db->select('si_jenis_ks.jenis_ks as si_jenis_ks_jenis_ks');

		$this->db->join('ref_prodi', 'ref_prodi.kode = si_kerjasama.kd_prodi', 'LEFT');
		$this->db->join('fakultas', 'fakultas.kode = ref_prodi.kode_fak', 'LEFT');
		$this->db->join('mou_kerjasama', 'mou_kerjasama.id = si_kerjasama.mou_ks', 'LEFT');
		$this->db->join('view_bentukdetil_ks', 'si_kerjasama.id = view_bentukdetil_ks.id_moa', 'LEFT');

		$this->db->select('si_kerjasama.*,ref_prodi.nama_prodi as ref_prodi_nama_prodi');
		$this->db->select('fakultas.nama_singkat as nama_fak_unit');
		$this->db->select('mou_kerjasama.nama_mitra as nama_mou_kerjasama');
		$this->db->select('mou_kerjasama.doc_file as doc_mou');
		$this->db->select('view_bentukdetil_ks.bentukks as bentukks');
		$this->db->select('REPLACE(view_bentukdetil_ks.bentukks2,"#","\n") as bentuk_kerjasama');
		$this->db->select('IF(DATEDIFF(si_kerjasama.tgl_selesai,CURDATE())>0,"aktif","selesai") as status');
		$this->db->select("@sisa:=PERIOD_DIFF(EXTRACT(YEAR_MONTH FROM si_kerjasama.tgl_selesai),EXTRACT(YEAR_MONTH FROM CURDATE())) as _sb,if(@sisa<0,0,@sisa) as sisa_bulan");
		return $this;
	}

	public function filter_avaiable()
	{
		/*
		if (!$this->aauth->is_admin()) {
		}*/
		$filter_prodi = get_filter_prodi();
		$prodi = array_column($filter_prodi, 'kode');
		$this->db->where_in('kd_prodi', $prodi);
		return $this;
	}
	public function get_option_mou($where)
	{
		foreach ($where as $key => $val) {
			$this->db->where($key, $val);
		}
		$data = $this->db->select('id,concat(nama_mitra," - ",doc_nomor) as name', false)->where('tgl_selesai > CURDATE()', null, false)->get('mou_kerjasama')->result();
		$option = build_option($data);
		$option .= "<option value='1'>Pengajuan Baru</option>";
		return $option;
	}
	public function get_option_moa($where)
	{
		foreach ($where as $key => $val) {
			$this->db->where($key, $val);
		}
		$sql = $this->db->select('id,concat(nama_mitra," - ",doc_nomor) as name', false)->where('tgl_selesai > CURDATE()', null, false);
		$data = $sql->get('si_kerjasama')->result();
		$option = build_option($data);
		return $option;
	}
	public function getDetailBentuk($id_moa)
	{
		$this->db->select('b.id, b.id_bentuk_ks, fb.bentuk_ks, id_moa, ms.id_ref_sasaran, rs.nama_sasaran, mi.id_ref_indikator, ri.nama_indikator, b.nilai, b.nilai_satuan');
		$this->db->from('bentuk_ks_detil b');
		$this->db->join('si_bentuk_ks fb', 'fb.id = b.id_bentuk_ks', 'INNER');
		$this->db->join('ref_bentuk_ks_sasaran ms', 'b.id_sasaran = ms.id', 'LEFT');
		$this->db->join('ref_bentuk_ks_indikator mi', 'b.id_indikator = mi.id', 'LEFT');
		$this->db->join('ref_sasaran rs', 'ms.id_ref_sasaran = rs.id', 'LEFT');
		$this->db->join('ref_indikator ri', 'mi.id_ref_indikator = ri.id', 'LEFT');
		$this->db->where('id_moa', $id_moa);
		$query = $this->db->get();
		return $query->result();
	}
	public function getRekapById($id)
	{
		$sql = "SELECT
			s.id, 
			s.kd_prodi, 
			s.scope, 
			s.jenis_ks, 
			s.doc_tipe, 
			s.doc_nomor, 
			s.doc_file, 
			s.tahun,
			s.penanggung_jawab,
			s.pj_nip,
			s.nama_mitra, 
			s.negara, 
			s.is_aktif, 
			s.foto1, 
			s.links,
			s.deskripsi, 
			s.desc_laporan, 
			s.tgl_mulai, 
			s.tgl_selesai, 
			s.is_report,
			s.tgl_laporan,
			s.kd_prodi,
			p.nama_prodi,
			tj.nip AS ttd_pj_nip,
			tj.nama AS ttd_pj_nama,
			tj.jabatan AS ttd_pj_jabatan,
			tj.file_ttd AS ttd_pj_file,
			tp.nip AS ttd_pejabat_nip,
			tp.nama AS ttd_pejabat_nama,
			tp.jabatan AS ttd_pejabat_jabatan,
			tp.file_ttd AS ttd_pejabat_file,
			mt.nama_ttd AS ttd_mitra_nama,
			mt.file_ttd AS ttd_mitra_file,
			(CASE p.tingkat 
				WHEN 1 THEN 'UNIVERSITAS NEGERI SEMARANG' 
				WHEN 2 THEN concat('FAKULTAS ',f.nama) 
				WHEN 3 THEN concat('Program Studi ',p.nama_prodi) 
				WHEN 4 THEN concat('Unit ',p.nama_prodi) 
				WHEN 5 THEN concat('Unit ',p.nama_prodi)
				ELSE '' END
			) AS unit, 
			s.kode_fak,
			js.jenis_ks AS nama_jenis_ks
		FROM
			si_kerjasama AS s
		LEFT JOIN si_jenis_ks AS js ON (s.jenis_ks = js.id)
		LEFT JOIN ref_prodi AS p ON (s.kd_prodi = p.kode)
		LEFT JOIN fakultas AS f ON (p.kode_fak = f.kode)
		LEFT JOIN karyawan_ttd AS tj ON (s.pj_ttd_id = tj.id)
		LEFT JOIN karyawan_ttd AS tp ON (s.pejabat_ttd_id = tp.id)
		LEFT JOIN mitra_kerjasama_ttd AS mt ON (s.pj_mitra_ttd_id = mt.id)
		WHERE s.id = $id";

		return $this->db->query($sql)->row();
	}
	public function getJenisKerjasama()
	{
		$this->db->select('j.id, j.jenis_ks, k.bobot_iku');
		$this->db->select("CASE WHEN k.bobot_iku IS NOT NULL THEN CONCAT(j.jenis_ks, ' - IKU : ', k.bobot_iku) ELSE j.jenis_ks END AS jenis_ks_concat", false);
		$this->db->from('si_jenis_ks j');
		$this->db->join('iku_map_ks m', 'j.id = m.id_jenis_ks', 'left');
		$this->db->join('iku_ks k', 'm.id_iku = k.id', 'left');
		$this->db->order_by('j.id');
		return $this->db->get()->result();
	}
	public function check_bentuk_ks($bentuk_ks_id)
	{
		$this->db->where("FIND_IN_SET('$bentuk_ks_id', bentuk_ks) >", 0);
		$this->db->or_where("EXISTS (SELECT * FROM ref_bentuk_ks_sasaran WHERE id_bentuk_ks = '$bentuk_ks_id')");
		$this->db->from('si_kerjasama');
		$count = $this->db->count_all_results();
		return $count > 0;
	}
}

/* End of file Model_si_kerjasama.php */
/* Location: ./application/models/Model_si_kerjasama.php */
