<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_si_bentuk_detil extends MY_Model
{
	private $primary_key    = 'id';
	private $table_name     = 'bentuk_ks_detil';

	public function __construct()
	{
		$config = array(
			'primary_key'   => $this->primary_key,
			'table_name'    => $this->table_name,
		);
		parent::__construct($config);
	}
	public function browse_bentuk_ks()
	{
		$id = $this->input->post('id_moa');
		$t1 = $this->table_name;
		$t2 = 'si_bentuk_ks';
		$t3 = 'ref_bentuk_ks_sasaran';
		$t4 = 'ref_bentuk_ks_indikator';
		$t5 = 'ref_sasaran';
		$t6 = 'ref_indikator';
		$this->load->library('Datatables', null, 'dt');
		$this->dt->set_database('default');
		$this->dt->from($t1)->join("$t2", "$t1.id_bentuk_ks = $t2.id AND $t1.id_moa = $id");
		$this->db->join($t3,"$t1.id_sasaran = $t3.id",'left');
		$this->db->join($t4,"$t1.id_indikator = $t4.id",'left');
		$this->db->join($t5,"$t3.id_ref_sasaran = $t5.id",'left');
		$this->db->join($t6,"$t4.id_ref_indikator = $t6.id",'left');
		$this->dt->select("$t1.id,$t2.bentuk_ks,$t1.nilai,$t5.nama_sasaran,$t6.nama_indikator,$t1.nilai_satuan");
		return $this->dt->generate();
	}
	public function updateDetailBentuk($where, $data)
	{
		return $this->db->where($where)->update('bentuk_ks_detil',$data);
	}
	public function getSasaran($id){
		$sql = "SELECT a.*,b.nama_sasaran FROM ref_bentuk_ks_sasaran a INNER JOIN ref_sasaran b ON (a.id_ref_sasaran = b.id AND id_bentuk_ks = ?)";
		return $this->db->query($sql,[$id])->result();
	}
	public function getIndikator($id_sasaran){
		$sql = "SELECT a.*,b.nama_indikator FROM ref_bentuk_ks_indikator a INNER JOIN ref_indikator b ON (a.id_ref_indikator = b.id AND id_sasaran = ?)";
		return $this->db->query($sql,[$id_sasaran])->result();
	}
}
