<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_api_si_kerjasama extends MY_Model
{

	private $primary_key 	= 'id';
	private $table_name 	= 'si_kerjasama';
	private $field_search 	= ['id', 'scope', 'mou_ks', 'jenis_ks', 'bentuk_ks', 'doc_tipe', 'doc_nomor', 'doc_tanggal', 'doc_file', 'tgl_mulai', 'tgl_selesai', 'tahun', 'dana_pendamping', 'dana_provit', 'nama_mitra', 'negara', 'penanggung_jawab', 'pj_email', 'pj_tlp', 'penanggung_jawab_mitra', 'pj_mitra_email', 'pj_mitra_tlp', 'pj_mitra_alamat', 'deskripsi', 'foto1', 'foto2', 'tgl_input', 'kd_prodi', 'validasi', 'is_aktif'];

	public function __construct()
	{
		$config = array(
			'primary_key' 	=> $this->primary_key,
			'table_name' 	=> $this->table_name,
			'field_search' 	=> $this->field_search,
		);

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			$where .= "(" . $field . " LIKE '%" . $q . "%' )";
		}

		$this->db->where($where);
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			if (in_array($field, $select_field)) {
				$where .= "(" . $field . " LIKE '%" . $q . "%' )";
			}
		}

		if (is_array($select_field) and count($select_field)) {
			$this->db->select($select_field);
		}

		if ($where) {
			$this->db->where($where);
		}
		$this->db->limit($limit, $offset);
		$this->db->order_by($this->primary_key, "DESC");
		$query = $this->db->get($this->table_name);

		return $query->result();
	}
	
	public function rekap_unit_iku(){
		$sql = "SELECT
						U.id,
						U.scope, 
						U.mou_ks, 
						IFNULL(M.doc_nomor,'Tanpa Mou') as nomor_mou,
						U.jenis_ks,
						j.jenis_ks AS nama_jenis_ks,
						U.bentuk_ks, 
						U.doc_tipe, 
						U.doc_nomor, 
						U.doc_tanggal,
						IF(DAYNAME(U.tgl_mulai) is null,null,U.tgl_mulai) AS tgl_mulai, 
						IF(DAYNAME(U.tgl_selesai) is null,null,U.tgl_selesai) AS tgl_selesai,  
						U.tahun, 
						U.nama_mitra, 
						U.negara, 
						U.kd_prodi,
						p.nama_prodi,
						p.kode_fak,
						f.nama_singkat as fakultas,
						p.tingkat,
						U.validasi, 
						U.is_aktif,
						U.is_report,
						m.id_iku AS maping_id_iku,
						w.nama_iku_ks AS maping_ks_iku,
						w.bobot,
						w.bobot_kategori,
						w.bobot_iku
				FROM
					si_kerjasama AS U
				INNER JOIN (si_jenis_ks AS j,ref_prodi AS p,fakultas AS f, iku_map_ks m,iku_ks w) 
				ON 
				(
					U.jenis_ks = j.id 
					AND U.kd_prodi = p.kode 
					AND p.kode_fak = f.kode
					AND m.id_jenis_ks = j.id
					AND w.id = m.id_iku
					AND (U.tahun BETWEEN ".(date('Y')-5)." AND ".date('Y').")
					AND p.tingkat = 3
				)
				LEFT JOIN mou_kerjasama M ON (U.mou_ks = M.id)";
		return $this->db->query($sql)->result();
	}
	public function kategori_iku(){
		return $this->db->select('*')->get('iku_ks')->result();
	}

	/**
	 Pendapatan Kerjasama
	 */
	public function pendapatanKs($where = []){
		$this->db->select("
			s.id,
			s.kd_prodi, 
			s.nilai_kontrak, 
			s.nilai_diterima, 
			s.bank_nama, 
			s.bank_rekening, 
			s.bank_atasnama, 
			s.kebutuhan_rab,
			s.validasi,
			CONCAT('".BASE_URL."file/',s.file_pengantar) AS file_pengantar, 
			CONCAT('".BASE_URL."file/',s.file_sptjm) AS file_sptjm, 
			CONCAT('".BASE_URL."file/',s.file_rab) AS file_rab, 
			CONCAT('".BASE_URL."file/',s.file_pendukung) AS file_pendukung, 
			u.doc_nomor as moa_doc_nomor, 
			u.doc_tanggal as moa_doc_tanggal, 
			u.tahun as moa_doc_tahun, 
			u.doc_tipe as moa_doc_tipe,
			CONCAT('".BASE_URL."file/',u.doc_file) AS moa_doc_file, 
			u.nama_mitra as moa_nama_mitra, 
			u.tgl_mulai as moa_tgl_mulai, 
			u.tgl_selesai as moa_tgl_selesai,
			s.siakun_nilai_kontrak,
			s.siakun_nilai_diterima, 
			s.siakun_kebutuhan_rab,
			s.siakun_valid,
			s.sianggar_nilai_kontrak,
			s.sianggar_nilai_diterima, 
			s.sianggar_kebutuhan_rab, 
			s.sianggar_valid
			",false)
		->from('pendapatan_kerjasama AS s')
		->join("si_kerjasama AS u","s.unit_ks = u.id");
		if(!empty($where)) $this->db->where($where);
		return $this->db->get()->result();
	}
	public function updatePendapatan($id,$data){
		return $this->db->where('id',$id)->update('pendapatan_kerjasama',$data);
	}
	public function pendapatanKsByID($id){
		$sql = "SELECT
			s.id,
			s.kd_prodi, 
			s.nilai_kontrak, 
			s.nilai_diterima, 
			s.kebutuhan_rab,
			s.validasi,
			s.bank_nama, 
			s.bank_rekening, 
			s.bank_atasnama, 
			CONCAT('".BASE_URL."file/',s.file_pengantar) AS file_pengantar, 
			CONCAT('".BASE_URL."file/',s.file_sptjm) AS file_sptjm, 
			CONCAT('".BASE_URL."file/',s.file_rab) AS file_rab, 
			CONCAT('".BASE_URL."file/',s.file_pendukung) AS file_pendukung, 
			u.doc_nomor as moa_doc_nomor,
			u.doc_tanggal as moa_doc_tanggal, 
			u.tahun as moa_doc_tahun, 
			u.doc_tipe as moa_doc_tipe,
			CONCAT('".BASE_URL."file/',u.doc_file) AS moa_doc_file, 
			u.nama_mitra as moa_nama_mitra, 
			u.tgl_mulai as moa_tgl_mulai, 
			u.tgl_selesai as moa_tgl_selesai,
			s.siakun_nilai_kontrak,
			s.siakun_nilai_diterima, 
			s.siakun_kebutuhan_rab,
			s.siakun_valid,
			s.sianggar_nilai_kontrak,
			s.sianggar_nilai_diterima, 
			s.sianggar_kebutuhan_rab,
			s.sianggar_valid
		FROM
			pendapatan_kerjasama AS s
		INNER JOIN si_kerjasama AS u ON 	(s.unit_ks = u.id AND s.id = $id)";
		return $this->db->query($sql)->row();
	}
}

/* End of file Model_si_kerjasama.php */
/* Location: ./application/models/Model_si_kerjasama.php */