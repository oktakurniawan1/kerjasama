<!-- Fine Uploader Gallery CSS file
====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<script type="text/javascript">
	function domo() {

		// Binding keys
		$('*').bind('keydown', 'Ctrl+s', function assets() {
			$('#btn_save').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+x', function assets() {
			$('#btn_cancel').trigger('click');
			return false;
		});

		$('*').bind('keydown', 'Ctrl+d', function assets() {
			$('.btn_save_back').trigger('click');
			return false;
		});

	}

	jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Data Kerjasama <small>Edit Data Kerjasama Unit</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/si_kerjasama'); ?>">Data Kerjasama</a></li>
		<li class="active">Edit</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Data Kerjasama Unit</h3>
							<h5 class="widget-user-desc">Edit Laporan Data Kerjasama Unit</h5>
							<hr>
						</div>
						<?= form_open(base_url('administrator/si_kerjasama/laporan_save/' . $this->uri->segment(4)), [
							'name'    => 'form_si_kerjasama',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_si_kerjasama',
							'method'  => 'POST'
						]); ?>

						<table class="table table-hover">
							<thead>
								<tr>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td></td>
								</tr>
							</tbody>
						</table>

						<div class="form-group ">
							<label for="bentuk_ks" class="col-sm-2 control-label">Bentuk Kerjasama
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select" name="bentuk_ks[]" id="bentuk_ks" tab-index="5" multiple data-placeholder="Select Bentuk Kerjasama">
									<option value=""></option>
									<?php foreach (db_get_all_data('si_bentuk_ks') as $row) : ?>
										<option <?= array_search($row->id, explode(',', $si_kerjasama->bentuk_ks)) !== false ? 'selected="selected"' : ''; ?> value="<?= $row->id; ?>"><?= ucwords($row->bentuk_ks); ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
								<div id='div-detil-bentukks'></div>
							</div>
						</div>

						<div class="form-group ">
							<label for="desc_laporan" class="col-sm-2 control-label">Keterangan
								<!-- <i class="required">*</i> -->
							</label>
							<div class="col-sm-8">
								<textarea id="desc_laporan" name="desc_laporan" rows="5" cols="80" class="form-control"> <?= set_value('desc_laporan', $si_kerjasama->desc_laporan); ?></textarea>
								<small class="info help-block">
									<b>Hasil/Outcome laporan</b> Min Length : 10.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="desc_laporan" class="col-sm-2 control-label">Tautan/link Bukti Kegiatan
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<textarea id="desc_laporan" name="links" rows="5" cols="80" class="form-control"> <?= set_value('links', $si_kerjasama->links); ?></textarea>
								<small class="info help-block">
									<b>Pisahkan tautan dengan baris</b> newline</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="foto1" class="col-sm-2 control-label">Bukti Foto
								<!-- <i class="required">*</i> -->
							</label>
							<div class="col-sm-8">
								<div id="si_kerjasama_foto1_galery"></div>
								<input class="data_file data_file_uuid" name="si_kerjasama_foto1_uuid" id="si_kerjasama_foto1_uuid" type="hidden" value="<?= set_value('si_kerjasama_foto1_uuid'); ?>">
								<input class="data_file" name="si_kerjasama_foto1_name" id="si_kerjasama_foto1_name" type="hidden" value="<?= set_value('si_kerjasama_foto1_name', $si_kerjasama->foto1); ?>">
								<small class="info help-block">
									<b>Extension file must</b> JPG,JPEG, <b>Max size file</b> 5 mb.</small>
							</div>
						</div>

						<div class="form-group">
							<label for="penanggung_jawab" class="col-sm-2 control-label">Penanggung Jawab
								<i class="required">*</i>
							</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="penanggung_jawab" disabled id="penanggung_jawab" placeholder="Penanggung jawab" value="<?= set_value('penanggung_jawab', $si_kerjasama->penanggung_jawab); ?>">
								<small class="info help-block">
									<button class="btn btn-primary btn-sm" type="button" onclick="cari_pegawai('pj')">Cari di Simpeg</button>
								</small>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control" readonly name="pj_nip" id="pj_nip" placeholder="NIP/NRP" value="<?= set_value('pj_nip', $si_kerjasama->pj_nip); ?>">
								<small class="info help-block"><b>NIP</b></small>
							</div>
							<div class="col-sm-3">
								<select class="form-control chosen chosen-select-deselect" name="pj_ttd_id" id="pj_ttd_id" data-placeholder="Select Penandatangan">
									<option value=""></option>
									<?php foreach (db_get_all_data('karyawan_ttd', ['nip' => $si_kerjasama->pj_nip]) as $row) : ?>
										<option <?= $row->id ==  $si_kerjasama->pj_ttd_id ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->nip . ' - ' . $row->nama . ' - ' . $row->jabatan ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Penandatangan</b>
								</small>
							</div>
						</div>

						<div class="form-group">
							<label for="pejabat" class="col-sm-2 control-label">Pejabat Mengetahui
								<i class="required">*</i>
							</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" readonly name="pejabat_nama" id="pejabat_nama" placeholder="Nama Pejabat" value="<?= set_value('pejabat_nama', $si_kerjasama->pejabat_nama); ?>">
								<small class="info help-block">
									<button class="btn btn-primary btn-sm" type="button" onclick="cari_pegawai('pejabat')">Cari di Simpeg</button>
								</small>
							</div>
							<div class="col-sm-2">
								<input type="text" class="form-control" readonly name="pejabat_nip" id="pejabat_nip" placeholder="NIP/NRP" value="<?= set_value('pejabat_nip', $si_kerjasama->pejabat_nip); ?>">
								<small class="info help-block"><b>NIP</b></small>
							</div>
							<div class="col-sm-3">
								<select class="form-control chosen chosen-select-deselect" name="pejabat_ttd_id" id="pejabat_ttd_id" data-placeholder="Select Penandatangan">
									<option value=""></option>
									<?php foreach (db_get_all_data('karyawan_ttd', ['nip' => $si_kerjasama->pejabat_nip]) as $row) : ?>
										<option <?= $row->id ==  $si_kerjasama->pejabat_ttd_id ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->nip . ' - ' . $row->nama . ' | ' . $row->jabatan ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Penandatangan</b>
								</small>
							</div>
						</div>
						<div class="form-group">
							<label for="instruction" class="col-sm-2 control-label"></label>
							<div class="col-sm-6">
								<div class="alert alert-info" role="alert">
									<strong>Info !</strong> Kosongi tanda tangan mitra jika tidak ada tanda tangan mitra.
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="mitra" class="col-sm-2 control-label">Mitra
								<!-- <i class="required">*</i> -->
							</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" readonly name="nama_mitra" id="nama_mitra" placeholder="Nama Mitra" value="<?= set_value('nama_mitra', $si_kerjasama->nama_mitra); ?>">
								<small class="info help-block">
									Nama mitra.
								</small>
							</div>
							<div class="col-sm-3">
								<select class="form-control chosen chosen-select-deselect" name="pj_mitra_ttd_id" id="pj_mitra_ttd_id" data-placeholder="Select Penandatangan">
									<option value=""></option>
									<?php foreach (db_get_all_data('mitra_kerjasama_ttd', ['id_mitra' => $si_kerjasama->id_mitra]) as $row) : ?>
										<option <?= $row->id ==  $si_kerjasama->pj_mitra_ttd_id ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->nama_ttd  ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Tanda tangan mitra</b>
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="tgl_mulai" class="col-sm-2 control-label">Tanggal Laporan
								<i class="required">*</i>
							</label>
							<div class="col-sm-6">
								<div class="input-group date col-sm-4">
									<input type="text" class="form-control pull-right datepicker" name="tgl_laporan" placeholder="Tanggal Penandatanganan " id="tgl_laporan" value="<?= set_value('tgl_laporan', $si_kerjasama->tgl_laporan); ?>">
								</div>
								<small class="info help-block">
									<b>Tanggal penandatanganan</b>
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="is_report" class="col-sm-2 control-label">Data Lengkap ?
							</label>
							<div class="col-sm-6">
								<div class="col-md-2 padding-left-0">
									<label>
										<input type="radio" class="flat-red" name="is_report" id="is_report" value="1" <?= $si_kerjasama->is_report == "1" ? "checked" : ""; ?>>
										Yes
									</label>
								</div>
								<div class="col-md-14">
									<label>
										<input type="radio" class="flat-red" name="is_report" id="is_report" value="0" <?= $si_kerjasama->is_report == "0" ? "checked" : ""; ?>>
										No
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="instruction" class="col-sm-2 control-label"></label>
							<div class="col-sm-6">
								<div class="alert alert-info" role="alert">
									<strong>Info !</strong> Untuk laporan yang dibuat dengan mengunggah file, silakan <code>preivew</code> laporan terlebih dahulu dengan menekan tombol <code>preivew</code>. Setelah itu, simpan laporan sebagai file PDF, dan unggah kembali file tersebut menggunakan formulir unggahan di bawah ini.
								</div>
							</div>
						</div>

						<div class="form-group ">
							<label for="laporan_file" class="col-sm-2 control-label">File Laporan
								<!-- <i class="required">*</i> -->
							</label>
							<div class="col-sm-8">
								<div id="si_kerjasama_laporan_file_galery"></div>
								<input class="data_file data_file_uuid" name="si_kerjasama_laporan_file_uuid" id="si_kerjasama_laporan_file_uuid" type="hidden" value="<?= set_value('si_kerjasama_laporan_file_uuid'); ?>">
								<input class="data_file" name="si_kerjasama_laporan_file_name" id="si_kerjasama_laporan_file_name" type="hidden" value="<?= set_value('si_kerjasama_laporan_file_name', $si_kerjasama->laporan_file); ?>">
								<small class="info help-block">
									<b>Extension file must</b> PDF, <b>Max size file</b> 5 mb.</small>
							</div>
						</div>

						<div class="form-group">
							<label for="use_file_upload_laporan" class="col-sm-2 control-label">Gunakan File Upload sebagai Laporan</label>
							<div class="col-sm-6">
								<input type="hidden" name="use_file_upload_laporan" value="0">
								<input type="checkbox" name="use_file_upload_laporan" id="use_file_upload_laporan" value="1" <?= $si_kerjasama->use_file_upload_laporan == "1" ? "checked" : ""; ?>>
							</div>
						</div>

						<br>
						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom" style="margin-top: 10px;">
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-warning" id=" btn_preview" title="Preview" target="__blank" href="<?= base_url('administrator/si_kerjasama/laporan_preview/' . $si_kerjasama->id) ?>">
								<i class="fa fa-file-archive-o"></i> Preview
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<script src="<?= BASE_ASSET; ?>ckeditor/ckeditor.js"></script>
<!-- Page script -->
<script>
	$(document).ready(function() {
		// CKEDITOR.replace('desc_laporan');
		// var desc_laporan = CKEDITOR.instances.desc_laporan;

		$('#btn_cancel').click(function() {
			swal({
					title: "Are you sure?",
					text: "the data that you have created will be in the exhaust!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/si_kerjasama/index/' + <?= $this->session->userdata('_page') ?>;
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();
			// $('#desc_laporan').val(desc_laporan.getData());

			var form_si_kerjasama = $('#form_si_kerjasama');
			var data_post = form_si_kerjasama.serializeArray();
			var save_type = $(this).attr('data-stype');
			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: form_si_kerjasama.attr('action'),
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('form').find('.error-input').remove();
					$('.steps li').removeClass('error');
					if (res.success) {
						var id = $('#si_kerjasama_image_galery').find('li').attr('qq-file-id');
						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						$('.data_file_uuid').val('');

					} else {
						if (res.errors) {
							parseErrorField(res.errors);
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/

		var params = {};
		params[csrf] = token;

		$('#si_kerjasama_foto1_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/si_kerjasama/upload_foto1_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/si_kerjasama/delete_foto1_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/si_kerjasama/get_foto1_file/<?= $si_kerjasama->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["jpg", "jpeg"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#si_kerjasama_foto1_galery').fineUploader('getUuid', id);
						$('#si_kerjasama_foto1_uuid').val(uuid);
						$('#si_kerjasama_foto1_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#si_kerjasama_foto1_uuid').val();
					$.get(BASE_URL + '/administrator/si_kerjasama/delete_foto1_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#si_kerjasama_foto1_uuid').val('');
						$('#si_kerjasama_foto1_name').val('');
					}
				}
			}
		}); /*end foto1 galey*/

		var params = {};
		params[csrf] = token;

		$('#si_kerjasama_laporan_file_galery').fineUploader({
			template: 'qq-template-gallery',
			request: {
				endpoint: BASE_URL + '/administrator/si_kerjasama/upload_laporan_file_file',
				params: params
			},
			deleteFile: {
				enabled: true, // defaults to false
				endpoint: BASE_URL + '/administrator/si_kerjasama/delete_laporan_file_file'
			},
			thumbnails: {
				placeholders: {
					waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
					notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
				}
			},
			session: {
				endpoint: BASE_URL + 'administrator/si_kerjasama/get_laporan_file_file/<?= $si_kerjasama->id; ?>',
				refreshOnRequest: true
			},
			multiple: false,
			validation: {
				allowedExtensions: ["pdf"],
				sizeLimit: 5242880,
			},
			showMessage: function(msg) {
				toastr['error'](msg);
			},
			callbacks: {
				onComplete: function(id, name, xhr) {
					if (xhr.success) {
						var uuid = $('#si_kerjasama_laporan_file_galery').fineUploader('getUuid', id);
						$('#si_kerjasama_laporan_file_uuid').val(uuid);
						$('#si_kerjasama_laporan_file_name').val(xhr.uploadName);
					} else {
						toastr['error'](xhr.error);
					}
				},
				onSubmit: function(id, name) {
					var uuid = $('#si_kerjasama_laporan_file_uuid').val();
					$.get(BASE_URL + '/administrator/si_kerjasama/delete_laporan_file_file/' + uuid);
				},
				onDeleteComplete: function(id, xhr, isError) {
					if (isError == false) {
						$('#si_kerjasama_laporan_file_uuid').val('');
						$('#si_kerjasama_laporan_file_name').val('');
					}
				}
			}
		}); /*end laporan_file galey*/

		async function chain() {}
		chain();

		//bentuk detail kerjasama
		let bentukks = $('#bentuk_ks option:selected');
		$.each(bentukks, function(i, opt) {
			// console.log(opt.value);
			var id_moa = '<?= $si_kerjasama->id ?>'
			$.get('<?= site_url('administrator/si_kerjasama/getFormDetilBentuk_ks') ?>', {
				div: opt.value,
				bentukname: opt.text,
				id: opt.value,
				mode: 'edit',
				id_moa: id_moa
			}, function(page) {
				$('#div-detil-bentukks').append(page)
			})
		})

		$('#bentuk_ks').change(function(e) {
			let bentukks = $('option:selected', this)
			$.each(bentukks, function(i, opt) {
				// console.log(opt.value);
				if ($('#babi_' + opt.value).length == 0)
					$.get('<?= site_url('administrator/si_kerjasama/getFormDetilBentuk_ks') ?>', {
						div: opt.value,
						bentukname: opt.text,
						id: opt.value,
						mode: 'add'
					}, function(page) {
						$('#div-detil-bentukks').append(page)
					})
			})
			let remove = $('option:not(:selected)', this)
			// console.log(remove);
			$.each(remove, function(i, opt) {
				if ($('#babi_' + opt.value).length)
					$('#babi_' + opt.value).remove();
			})
		})
		$('#pj_nip').change(function(event) {
			var val = $(this).val();
			$.LoadingOverlay('show')
			$.ajax({
					url: BASE_URL + '/administrator/si_kerjasama/ajax_change_pj_nip/' + val,
					dataType: 'JSON',
				})
				.done(function(res) {
					var html = '<option value=""></option>';
					$.each(res, function(index, val) {
						html += '<option value="' + val.id + '">' + val.nama + ' - ' + val.jabatan + '</option>'
					});
					$('#pj_ttd_id').html(html);
					$('#pj_ttd_id').trigger('chosen:updated');

				})
				.fail(function() {
					toastr['error']('Error', 'Getting data fail')
				})
				.always(function() {
					$.LoadingOverlay('hide')
				});
		});
		$('#pejabat_nip').change(function(event) {
			var val = $(this).val();
			$.LoadingOverlay('show')
			$.ajax({
					url: BASE_URL + '/administrator/si_kerjasama/ajax_change_pj_nip/' + val,
					dataType: 'JSON',
				})
				.done(function(res) {
					var html = '<option value=""></option>';
					$.each(res, function(index, val) {
						html += '<option value="' + val.id + '">' + val.nama + ' - ' + val.jabatan + '</option>'
					});
					$('#pejabat_ttd_id').html(html);
					$('#pejabat_ttd_id').trigger('chosen:updated');

				})
				.fail(function() {
					toastr['error']('Error', 'Getting data fail')
				})
				.always(function() {
					$.LoadingOverlay('hide')
				});
		});
	}); /*end doc ready*/
	function cari_pegawai(val) {
		BootstrapDialog.show({
			title: 'Cari data Simpeg',
			draggable: true,
			message: function(dialog) {
				var $message = $('<div></div>');
				$.get('<?= site_url('administrator/pegawai/form_cari_pegawai') ?>', {}, function(response) {
					$message.html(response);
				})
				return $message;
			},
			buttons: [{
				label: 'Ambil',
				icon: 'glyphicon glyphicon-plus',
				cssClass: 'btn-primary',
				action: function(dialog) {
					if (val == 'pj') {
						$('#pj_nip').val($('#pegawai option:selected').data('nip'));
						$('#pj_nip').trigger('change');
					} else if (val == 'pejabat') {
						$('#pejabat_nip').val($('#pegawai option:selected').data('nip'));
						$('#pejabat_nama').val($('#pegawai option:selected').data('nama'));
						$('#pejabat_nip').trigger('change');
					}
					dialog.close();
				}
			}, {
				label: 'Tutup',
				action: function(dialog) {
					dialog.close();
				}
			}],
		});
	}
</script>