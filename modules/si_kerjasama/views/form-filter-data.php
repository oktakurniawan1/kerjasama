<form action="" method="POST" role="form">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label for="">Program Studi</label>
				<?= form_dropdown('kd_prodi', $prodi, fromsess('filterdata_moa')['kd_prodi'], ['style' => 'width:100%']) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8">
			<div class="form-group">
				<label for="">Jenis Kerjsama</label>
				<?= form_dropdown('jenis_ks', $jenisks, fromsess('filterdata_moa')['jenis_ks'], ['style' => 'width:100%']) ?>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="">Tahun</label>
				<?= form_dropdown('tahun', $tahun, fromsess('filterdata_moa')['tahun'], ['class' => 'form-control']) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-5">
			<div class="form-group">
				<label for="">Negara</label>
				<?= form_dropdown('negara', $negara, fromsess('filterdata_moa')['negara'], ['style' => 'width:100%']) ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<label for="">Scope</label>
				<?= form_dropdown('scope', $scope, fromsess('filterdata_moa')['scope'], ['class' => 'form-control']) ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="">Tipe</label>
				<?= form_dropdown('doc_tipe', $doctipe, fromsess('filterdata_moa')['doc_tipe'], ['class' => 'form-control']) ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<label for="">Status</label>
				<?= form_dropdown('is_aktif', $status, fromsess('filterdata_moa')['is_aktif'], ['class' => 'form-control']) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<label for="">Laporan</label>
				<?= form_dropdown('is_report', ['1' => 'OK', '0' => 'NOT OK', 'all' => 'ALL'], fromsess('filterdata_moa')['is_report'], ['class' => 'form-control']) ?>
			</div>
		</div>
		<div class="col-md-10">
			<div class="form-group">
				<label for="">Bentuk Kerjasama</label>
				<?= form_dropdown('bentuk_ks', $bentukks, fromsess('filterdata_moa')['bentuk_ks'], ['multiple' => 'multiple', 'style' => 'width:100%']) ?>
			</div>
		</div>
	</div>
</form>

<script>
	$(function() {
		$('select[name=kd_prodi],select[name=negara],select[name=jenis_ks]').select2({
			theme: 'classic',
			dropdownParent: $('#filtermoa-dialog'),
		});
		$('select[name=bentuk_ks]').select2({
			tags: true
		})
	})
</script>