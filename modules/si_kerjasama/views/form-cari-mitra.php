<?php if($page == 2) :?>
<div class="checkbox">
	<label>
		<input type="checkbox" value="" onclick="open_mitra(this)">
		Filter hanya prodi saya
	</label>
</div>
<?php endif;?>

<table class="table table-hover table-bordered" id="table1">
	<thead>
		<tr>
			<th>id</th>
			<th>Scope</th>
			<th>Nama mitra</th>
			<th>Alamat</th>
			<th>Email</th>
			<th>Telp</th>
			<th>Jenis</th>
			<th>Pilih</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
		</tr>
	</tbody>
</table>
<script>
	var oTable;
	$(function(){
		open_mitra();
	})

function open_mitra(el){
	var filter_prodi = $(el).is(':checked');
	oTable = $('#table1').DataTable({
		language: {
			"search": "Cari mitra:",
			"paginate": {
			// "previous": "Sebelumnya",
			// "next": "Selanjutnya"
			},
			"zeroRecords": "Data tidak ditemukan",
			// "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
			// "infoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
			// "infoFiltered": "(disaring dari _MAX_ total entri)"
		},
		processing: true,
		autoWidth : false,
		serverSide: true,
		destroy   : true,
		// ordering    : false,
		lengthChange: false,
		pageLength  : 4,
		order:[[2,'ASC']],
		// pagingType: 'simple',
		ajax: {
			'url': '<?php echo site_url('administrator/si_kerjasama/browse_mitra') ?>',
			'data': {
				<?= csrf_token()?>,
				page        : <?=$page?>,
				filter_prodi: filter_prodi
			},
			'type':'post', 
		},
		columns: [
			{data: 'id',visible: false},
			{data: 'scope'},
			{data: 'nama_mitra'},
			{data: 'alamat'},
			{data: 'email'},
			{data: 'telp'},
			{data: 'jenis_mitra'},
			{data:null,defaultContent:"<input type='radio' name='selectRow'>",orderable:false,searchable:false}
		],
	})
}
	
</script>