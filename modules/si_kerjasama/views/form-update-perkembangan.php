<table class="table table-hover table-striped table-bordered" id='table1'>
	<thead>
		<tr>
			<th>_id</th>
			<th>Bentuk Kerjasama</th>
			<th>Sasaran</th>
			<th>Indikator</th>
			<th>Luaran (Volume)</th>
			<th>Satuan</th>
		</tr>
	</thead>
	<tbody>
		<tr>
		</tr>
	</tbody>
</table>

<script>
	var oTable;
	$(function() {
		oTable = $('#table1').DataTable({
			processing  : true,
			serverSide  : true,
			ordering    : false,
			lengthChange: false,
			searching   : false,
			pageLength  : 4,
			// pagingType: 'simple',
			ajax: {
				'url': '<?php echo site_url('administrator/si_kerjasama/browse_bentuk_ks') ?>',
				'data': {
					id_moa: '<?= $id_moa ?>',
					<?= csrf_token()?>,
				},
				'type':'post', 
			},
			columns: [
				{data: 'id',visible: false},
				{data: 'bentuk_ks'},
				{data: 'nama_sasaran'},
				{data: 'nama_indikator'},
				{data: 'nilai'},
				{data: 'nilai_satuan'},
			],
		}).MakeCellsEditable({
			"onUpdate"   : update_data,
			"inputCss"   : 'form-control input-sm',
			'wrapperHtml': '<div class="alert alert-info">{content}</div>',
			"columns"    : [4,5],
			"confirmationButton": {
				"confirmCss": 'btn btn-success btn-xs',
				"cancelCss" : 'btn btn-warning btn-xs'
			},
			"inputTypes": [
				{
					"column":5, 
					"type":"list", 
					"options":[
						{"value": "mahasiswa", "display": "mahasiswa"},
						{"value": "dosen", "display": "dosen"},
						{"value": "judul", "display": "judul"},
						{"value": "buku", "display": "buku"},
						{"value": "artikel", "display": "artikel"},
						{"value": "sks", "display": "sks"},
						{"value": "rupiah", "display": "rupiah"},
					] 
				},
			]
		});
	})

	function update_data(updatedCell, updatedRow, oldValue) {
		var url    = '<?php echo site_url('administrator/si_kerjasama/update_nilai_bentuk_ks') ?>';
		var data   = updatedRow.data();
		data[csrf] = token;
		$.post(url, data, function(response) {
			oTable.draw(false);
		})
	}
</script>