<div id="babi_<?= $div ?>">
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title text-sm"><?= $bentukname ?></h3>
		</div>
		<div class="panel-body">
			<div class="form-group ">
				<label for="" class="col-sm-2 control-label text-sm">Sasaran</label>
				<div class="col-sm-8">
					<select class="form-control input-sm" name="sasaran[<?= $idbentuk?>]">
						<option value=""></option>
						<?php foreach ($ref_sasaran as $row) : ?>
							<?php if(!is_null($id_sasaran) && $id_sasaran == $row->id) :?>
								<option value="<?= $row->id ?>" selected ><?= $row->nama_sasaran; ?></option>
							<?php else :?>
								<option value="<?= $row->id ?>"><?= $row->nama_sasaran; ?></option>
							<?php endif?>
						<?php endforeach; ?>
					</select>
					<small class="info help-block">
					</small>
				</div>
			</div>
			<div class="form-group ">
				<label for="" class="col-sm-2 control-label text-sm">Indikator</label>
				<div class="col-sm-8">
					<select class="form-control input-sm" name="indikator[<?= $idbentuk?>]">
					<option value=""></option>
					<?php foreach ($ref_indikator as $row) : ?>
							<?php if(!is_null($id_indikator) && $id_indikator == $row->id) :?>
								<option value="<?= $row->id ?>" selected ><?= $row->nama_indikator; ?></option>
							<?php else :?>
								<option value="<?= $row->id ?>"><?= $row->nama_indikator; ?></option>
							<?php endif?>
						<?php endforeach; ?>
					</select>
					<small class="info help-block">
					</small>
				</div>
			</div>
			<div class="form-group ">
				<label for="" class="col-sm-2 control-label text-sm">Luaran</label>
				<div class="col-sm-6">
					<input type="number" class='form-control input-sm' name="nilai[<?= $idbentuk?>]" value="<?= $nilai?>">
					<small class="info help-block">volume</small>
				</div>
				<div class="col-sm-2">
				<select class="form-control input-sm" name="nilai_satuan[<?= $idbentuk?>]">
					<option value=""></option>
					<?php foreach ($satuan as $s) : ?>
							<?php if(!is_null($nilai_satuan) && $nilai_satuan == $s) :?>
								<option value="<?= $s ?>" selected ><?= $s; ?></option>
							<?php else :?>
								<option value="<?= $s ?>"><?= $s; ?></option>
							<?php endif?>
						<?php endforeach; ?>
					</select>
					<small class="info help-block">satuan</small>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(function(){
		$("select[name='sasaran[<?= $idbentuk?>]']").change(function(e){
			$.get('<?= site_url('administrator/si_kerjasama/get_option_bentuk_indikator')?>',{id_sasaran:e.target.value},function(page){
				$("select[name='indikator[<?= $idbentuk?>]']").html(page);
			});
		})
	})
</script>