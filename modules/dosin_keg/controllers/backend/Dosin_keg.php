<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Dosin Keg Controller
*| --------------------------------------------------------------------------
*| Dosin Keg site
*|
*/
class Dosin_keg extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_dosin_keg');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Dosin Kegs
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('dosin_keg_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if(!is_null($q)) $this->session->set_userdata('dosin_keg_filter',$q);
		if(!is_null($f)) $this->session->set_userdata('dosin_keg_field',$f);
		$this->session->set_userdata('_page',$offset);

		if(!is_null($this->input->get('reset_filter'))){
			$this->session->unset_userdata('dosin_keg_filter');
			$this->session->unset_userdata('dosin_keg_field');
		}
		$filter = $this->session->userdata('dosin_keg_filter');
		$field = $this->session->userdata('dosin_keg_field');
		$offset = $this->session->userdata('_page',$offset);

		$this->data['dosin_kegs'] = $this->model_dosin_keg->get($filter, $field, $this->limit_page, $offset);
		$this->data['dosin_keg_counts'] = $this->model_dosin_keg->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/dosin_keg/index/',
			'total_rows'   => $this->model_dosin_keg->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Dosin Keg List');
		$this->render('backend/standart/administrator/dosin_keg/dosin_keg_list', $this->data);
	}
	
	/**
	* Add new dosin_kegs
	*
	*/
	public function add()
	{
		$this->is_allowed('dosin_keg_add');

		$this->template->title('Dosin Keg New');
		$this->render('backend/standart/administrator/dosin_keg/dosin_keg_add', $this->data);
	}

	/**
	* Add New Dosin Kegs
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('dosin_keg_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('kegiatan', 'Nama Kegiatan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('aktivitas', 'TIpe Aktivitas', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'kegiatan' => $this->input->post('kegiatan'),
				'aktivitas' => $this->input->post('aktivitas'),
			];

			
			$save_dosin_keg = $this->model_dosin_keg->store($save_data);
            

			if ($save_dosin_keg) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_dosin_keg;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/dosin_keg/edit/' . $save_dosin_keg, 'Edit Dosin Keg'),
						anchor('administrator/dosin_keg', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/dosin_keg/edit/' . $save_dosin_keg, 'Edit Dosin Keg')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dosin_keg');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dosin_keg');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Dosin Kegs
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('dosin_keg_update');

		$this->data['dosin_keg'] = $this->model_dosin_keg->find($id);

		$this->template->title('Dosin Keg Update');
		$this->render('backend/standart/administrator/dosin_keg/dosin_keg_update', $this->data);
	}

	/**
	* Update Dosin Kegs
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('dosin_keg_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('kegiatan', 'Nama Kegiatan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('aktivitas', 'TIpe Aktivitas', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'kegiatan' => $this->input->post('kegiatan'),
				'aktivitas' => $this->input->post('aktivitas'),
			];

			
			$save_dosin_keg = $this->model_dosin_keg->change($id, $save_data);

			if ($save_dosin_keg) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/dosin_keg', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/dosin_keg/index/'.$this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/dosin_keg/index/'.$this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Dosin Kegs
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('dosin_keg_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'dosin_keg'), 'success');
        } else {
            set_message(cclang('error_delete', 'dosin_keg'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Dosin Kegs
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('dosin_keg_view');

		$this->data['dosin_keg'] = $this->model_dosin_keg->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Dosin Keg Detail');
		$this->render('backend/standart/administrator/dosin_keg/dosin_keg_view', $this->data);
	}
	
	/**
	* delete Dosin Kegs
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$dosin_keg = $this->model_dosin_keg->find($id);

		
		
		return $this->model_dosin_keg->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('dosin_keg_export');

		$this->model_dosin_keg->export(
			'dosin_keg', 
			'dosin_keg',
			$this->model_dosin_keg->field_search
		);
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('dosin_keg_export');

		$this->model_dosin_keg->pdf('dosin_keg', 'dosin_keg');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('dosin_keg_export');

		$table = $title = 'dosin_keg';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_dosin_keg->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file dosin_keg.php */
/* Location: ./application/controllers/administrator/Dosin Keg.php */