<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['extend_study'] = 'Perpanjangan Studi';
$lang['id'] = 'Id';
$lang['id_study'] = 'Universitas Studi';
$lang['nip'] = 'Nip';
$lang['file_transkrip'] = 'File Transkrip';
$lang['file_rekomendasi'] = 'File Rekomendasi';
$lang['file_ijin_lama'] = 'File Ijin Lama';
$lang['file_tugas_belajar'] = 'File Tugas Belajar';
$lang['file_progres_laporan_studi'] = 'File Progres Laporan Studi';
$lang['file_ket_kuliah_online'] = 'File Ket. Kuliah Online';
$lang['tgl_input'] = 'Tgl Input';
$lang['catatan'] = 'Catatan';
$lang['validasi'] = 'Validasi';
