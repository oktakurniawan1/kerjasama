<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Extend Study Controller
 *| --------------------------------------------------------------------------
 *| Extend Study site
 *|
 */
class Extend_study extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_extend_study');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * show all Extend Studys
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('extend_study_list');

		$filter_nip = $this->input->get('identitas');
		if (is_null($filter_nip)) {
			$filter_nip = $this->session->userdata('filter_nip_extend_study');
			if (is_null($filter_nip)) {
				set_message(cclang('Identitas tidak diketemukan !', 'extend_study'), 'warning');
				redirect_back('administrator/studi_lanjut');
			} 
		} else {
			$row = $this->model_extend_study->find_studi_lanjut($filter_nip);
			if (is_null($row)) {
				set_message(cclang('Identitas tidak terdaftar di database studi lanjut !', 'extend_study'), 'warning');
				redirect_back('administrator/studi_lanjut');
			} else {
				if (!is_groups_in(['Admin', 'Kerjasama','Unit','Kaprodi'])) {
					if (is_non_user()) {
						$nips = [fromsess('identitas'),fromsess('nip_baru')];
						if (!in_array($row->nip,$nips)) {
							set_message(cclang('sorry_you_do_not_have_permission_to_access', 'extend_study'), 'warning');
							redirect_back('administrator/studi_lanjut');
						}
					}else{
						$identitas = get_user_data('username');
						if($identitas != $row->nip){
							set_message(cclang('sorry_you_do_not_have_permission_to_access', 'extend_study'), 'warning');
							redirect_back('administrator/studi_lanjut');
						}
					}
				}
				$this->session->set_userdata('filter_nip_extend_study', $row->nip);
				$this->session->set_userdata('filter_nama_extend_study', $row->nama);
			}
		}
		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('extend_study_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('extend_study_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('extend_study_filter');
			$this->session->unset_userdata('extend_study_field');
		}
		$filter = $this->session->userdata('extend_study_filter');
		$field = $this->session->userdata('extend_study_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['extend_studys'] = $this->model_extend_study->get($filter, $field, $this->limit_page, $offset);
		$this->data['extend_study_counts'] = $this->model_extend_study->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/extend_study/index/',
			'total_rows'   => $this->model_extend_study->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Extend Study List');
		$this->render('backend/standart/administrator/extend_study/extend_study_list', $this->data);
	}

	/**
	 * Add new extend_studys
	 *
	 */
	public function add()
	{
		$this->is_allowed('extend_study_add');
		if (is_null($this->session->userdata('filter_nip_extend_study'))) {
			set_message(cclang('Identitas tidak diketemukan !', 'extend_study'), 'warning');
			redirect_back('administrator/studi_lanjut');
		}
		$this->template->title('Extend Study New');
		$this->render('backend/standart/administrator/extend_study/extend_study_add', $this->data);
	}

	/**
	 * Add New Extend Studys
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('extend_study_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('nip', 'Nip', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('extend_study_file_transkrip_name', 'File Transkrip', 'trim|required');
		$this->form_validation->set_rules('extend_study_file_rekomendasi_name', 'File Rekomendasi', 'trim|required');
		$this->form_validation->set_rules('extend_study_file_tugas_belajar_name', 'File Tugas Belajar', 'trim|required');
		$this->form_validation->set_rules('extend_study_file_ijin_lama_name', 'File Ijin Lama', 'trim|required');

		if ($this->form_validation->run()) {
			$extend_study_file_progres_laporan_studi_uuid = $this->input->post('extend_study_file_progres_laporan_studi_uuid');
			$extend_study_file_progres_laporan_studi_name = $this->input->post('extend_study_file_progres_laporan_studi_name');
			$extend_study_file_ket_kuliah_online_uuid = $this->input->post('extend_study_file_ket_kuliah_online_uuid');
			$extend_study_file_ket_kuliah_online_name = $this->input->post('extend_study_file_ket_kuliah_online_name');
			$extend_study_file_transkrip_uuid = $this->input->post('extend_study_file_transkrip_uuid');
			$extend_study_file_transkrip_name = $this->input->post('extend_study_file_transkrip_name');
			$extend_study_file_rekomendasi_uuid = $this->input->post('extend_study_file_rekomendasi_uuid');
			$extend_study_file_rekomendasi_name = $this->input->post('extend_study_file_rekomendasi_name');
			$extend_study_file_ijin_lama_uuid = $this->input->post('extend_study_file_ijin_lama_uuid');
			$extend_study_file_ijin_lama_name = $this->input->post('extend_study_file_ijin_lama_name');
			$extend_study_file_tugas_belajar_uuid = $this->input->post('extend_study_file_tugas_belajar_uuid');
			$extend_study_file_tugas_belajar_name = $this->input->post('extend_study_file_tugas_belajar_name');

			$save_data = [
				'id_study' => $this->input->post('id_study'),
				'nip' => $this->input->post('nip'),
				'catatan' => $this->input->post('catatan'),
				'tgl_input' => date('Y-m-d'),
				'validasi' => $this->input->post('validasi'),
			];

			if (!is_dir(FCPATH . '/uploads/extend_study/')) {
				mkdir(FCPATH . '/uploads/extend_study/');
			}

			if (!is_dir(FCPATH . '/uploads/extend_study/')) {
				mkdir(FCPATH . '/uploads/extend_study/');
			}

			if (!empty($extend_study_file_progres_laporan_studi_name)) {
				$extend_study_file_progres_laporan_studi_name_copy = date('YmdHis') . '-' . $extend_study_file_progres_laporan_studi_name;

				rename(FCPATH . 'uploads/tmp/' . $extend_study_file_progres_laporan_studi_uuid . '/' . $extend_study_file_progres_laporan_studi_name, 
						FCPATH . 'uploads/extend_study/' . $extend_study_file_progres_laporan_studi_name_copy);

				if (!is_file(FCPATH . '/uploads/extend_study/' . $extend_study_file_progres_laporan_studi_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_progres_laporan_studi'] = $extend_study_file_progres_laporan_studi_name_copy;
			}
		
			if (!empty($extend_study_file_ket_kuliah_online_name)) {
				$extend_study_file_ket_kuliah_online_name_copy = date('YmdHis') . '-' . $extend_study_file_ket_kuliah_online_name;

				rename(FCPATH . 'uploads/tmp/' . $extend_study_file_ket_kuliah_online_uuid . '/' . $extend_study_file_ket_kuliah_online_name, 
						FCPATH . 'uploads/extend_study/' . $extend_study_file_ket_kuliah_online_name_copy);

				if (!is_file(FCPATH . '/uploads/extend_study/' . $extend_study_file_ket_kuliah_online_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_ket_kuliah_online'] = $extend_study_file_ket_kuliah_online_name_copy;
			}

			if (!empty($extend_study_file_tugas_belajar_name)) {
				$extend_study_file_tugas_belajar_name_copy = date('YmdHis') . '-' . $extend_study_file_tugas_belajar_name;

				rename(FCPATH . 'uploads/tmp/' . $extend_study_file_tugas_belajar_uuid . '/' . $extend_study_file_tugas_belajar_name, 
						FCPATH . 'uploads/extend_study/' . $extend_study_file_tugas_belajar_name_copy);

				if (!is_file(FCPATH . '/uploads/extend_study/' . $extend_study_file_tugas_belajar_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_tugas_belajar'] = $extend_study_file_tugas_belajar_name_copy;
			}

			if (!empty($extend_study_file_transkrip_name)) {
				$extend_study_file_transkrip_name_copy = date('YmdHis') . '-' . $extend_study_file_transkrip_name;

				rename(
					FCPATH . 'uploads/tmp/' . $extend_study_file_transkrip_uuid . '/' . $extend_study_file_transkrip_name,
					FCPATH . 'uploads/extend_study/' . $extend_study_file_transkrip_name_copy
				);

				if (!is_file(FCPATH . '/uploads/extend_study/' . $extend_study_file_transkrip_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_transkrip'] = $extend_study_file_transkrip_name_copy;
			}

			if (!empty($extend_study_file_rekomendasi_name)) {
				$extend_study_file_rekomendasi_name_copy = date('YmdHis') . '-' . $extend_study_file_rekomendasi_name;

				rename(
					FCPATH . 'uploads/tmp/' . $extend_study_file_rekomendasi_uuid . '/' . $extend_study_file_rekomendasi_name,
					FCPATH . 'uploads/extend_study/' . $extend_study_file_rekomendasi_name_copy
				);

				if (!is_file(FCPATH . '/uploads/extend_study/' . $extend_study_file_rekomendasi_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_rekomendasi'] = $extend_study_file_rekomendasi_name_copy;
			}

			if (!empty($extend_study_file_ijin_lama_name)) {
				$extend_study_file_ijin_lama_name_copy = date('YmdHis') . '-' . $extend_study_file_ijin_lama_name;

				rename(
					FCPATH . 'uploads/tmp/' . $extend_study_file_ijin_lama_uuid . '/' . $extend_study_file_ijin_lama_name,
					FCPATH . 'uploads/extend_study/' . $extend_study_file_ijin_lama_name_copy
				);

				if (!is_file(FCPATH . '/uploads/extend_study/' . $extend_study_file_ijin_lama_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ijin_lama'] = $extend_study_file_ijin_lama_name_copy;
			}


			$save_extend_study = $this->model_extend_study->store($save_data);


			if ($save_extend_study) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_extend_study;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/extend_study/edit/' . $save_extend_study, 'Edit Extend Study'),
						anchor('administrator/extend_study', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/extend_study/edit/' . $save_extend_study, 'Edit Extend Study')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/extend_study');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/extend_study');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Extend Studys
	 *
	 * @var $id String
	 */
	public function edit($id = null)
	{
		$this->is_allowed('extend_study_update');

		if (is_null($this->session->userdata('filter_nip_extend_study'))) {
			set_message(cclang('Identitas tidak diketemukan !', 'extend_study'), 'warning');
			redirect_back('administrator/studi_lanjut');
		}
		
		$row = $this->model_extend_study->filter_avaiable()->join_avaiable()->find($id);
		$this->data['extend_study'] = $row;
		if (!($this->data['extend_study'])) exit;

		if (!is_groups_in(['Admin', 'Kerjasama'])) {
			if ($this->data['extend_study']->validasi == 'Y') {
				set_message(cclang('Anda tidak bisa mengubah, karena data sudah divalidasi', 'mhs_mobility'), 'warning');
				redirect_back();
			}
		}

		$this->template->title('Extend Study Update');
		$this->render('backend/standart/administrator/extend_study/extend_study_update', $this->data);
	}

	/**
	 * Update Extend Studys
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('extend_study_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('nip', 'Nip', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('extend_study_file_transkrip_name', 'File Transkrip', 'trim|required');
		$this->form_validation->set_rules('extend_study_file_rekomendasi_name', 'File Rekomendasi', 'trim|required');
		$this->form_validation->set_rules('extend_study_file_ijin_lama_name', 'File Ijin Lama', 'trim|required');
		$this->form_validation->set_rules('extend_study_file_tugas_belajar_name', 'File Tugas Belajar', 'trim|required');

		if ($this->form_validation->run()) {
			$extend_study_file_progres_laporan_studi_uuid = $this->input->post('extend_study_file_progres_laporan_studi_uuid');
			$extend_study_file_progres_laporan_studi_name = $this->input->post('extend_study_file_progres_laporan_studi_name');
			$extend_study_file_ket_kuliah_online_uuid = $this->input->post('extend_study_file_ket_kuliah_online_uuid');
			$extend_study_file_ket_kuliah_online_name = $this->input->post('extend_study_file_ket_kuliah_online_name');
			$extend_study_file_transkrip_uuid = $this->input->post('extend_study_file_transkrip_uuid');
			$extend_study_file_transkrip_name = $this->input->post('extend_study_file_transkrip_name');
			$extend_study_file_rekomendasi_uuid = $this->input->post('extend_study_file_rekomendasi_uuid');
			$extend_study_file_rekomendasi_name = $this->input->post('extend_study_file_rekomendasi_name');
			$extend_study_file_ijin_lama_uuid = $this->input->post('extend_study_file_ijin_lama_uuid');
			$extend_study_file_ijin_lama_name = $this->input->post('extend_study_file_ijin_lama_name');
			$extend_study_file_tugas_belajar_uuid = $this->input->post('extend_study_file_tugas_belajar_uuid');
			$extend_study_file_tugas_belajar_name = $this->input->post('extend_study_file_tugas_belajar_name');

			$save_data = [
				'id_study' => $this->input->post('id_study'),
				'nip' => $this->input->post('nip'),
				'catatan' => $this->input->post('catatan'),
				'validasi' => $this->input->post('validasi'),
			];

			if (!is_dir(FCPATH . '/uploads/extend_study/')) {
				mkdir(FCPATH . '/uploads/extend_study/');
			}

			if (!empty($extend_study_file_progres_laporan_studi_uuid)) {
				$extend_study_file_progres_laporan_studi_name_copy = date('YmdHis') . '-' . $extend_study_file_progres_laporan_studi_name;

				rename(FCPATH . 'uploads/tmp/' . $extend_study_file_progres_laporan_studi_uuid . '/' . $extend_study_file_progres_laporan_studi_name, 
						FCPATH . 'uploads/extend_study/' . $extend_study_file_progres_laporan_studi_name_copy);

				if (!is_file(FCPATH . '/uploads/extend_study/' . $extend_study_file_progres_laporan_studi_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_progres_laporan_studi'] = $extend_study_file_progres_laporan_studi_name_copy;
			}
		
			if (!empty($extend_study_file_ket_kuliah_online_uuid)) {
				$extend_study_file_ket_kuliah_online_name_copy = date('YmdHis') . '-' . $extend_study_file_ket_kuliah_online_name;

				rename(FCPATH . 'uploads/tmp/' . $extend_study_file_ket_kuliah_online_uuid . '/' . $extend_study_file_ket_kuliah_online_name, 
						FCPATH . 'uploads/extend_study/' . $extend_study_file_ket_kuliah_online_name_copy);

				if (!is_file(FCPATH . '/uploads/extend_study/' . $extend_study_file_ket_kuliah_online_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_ket_kuliah_online'] = $extend_study_file_ket_kuliah_online_name_copy;
			}

			if (!empty($extend_study_file_tugas_belajar_uuid)) {
				$extend_study_file_tugas_belajar_name_copy = date('YmdHis') . '-' . $extend_study_file_tugas_belajar_name;

				rename(FCPATH . 'uploads/tmp/' . $extend_study_file_tugas_belajar_uuid . '/' . $extend_study_file_tugas_belajar_name, 
						FCPATH . 'uploads/extend_study/' . $extend_study_file_tugas_belajar_name_copy);

				if (!is_file(FCPATH . '/uploads/extend_study/' . $extend_study_file_tugas_belajar_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_tugas_belajar'] = $extend_study_file_tugas_belajar_name_copy;
			}

			if (!is_dir(FCPATH . '/uploads/extend_study/')) {
				mkdir(FCPATH . '/uploads/extend_study/');
			}

			if (!empty($extend_study_file_transkrip_uuid)) {
				$extend_study_file_transkrip_name_copy = date('YmdHis') . '-' . $extend_study_file_transkrip_name;

				rename(
					FCPATH . 'uploads/tmp/' . $extend_study_file_transkrip_uuid . '/' . $extend_study_file_transkrip_name,
					FCPATH . 'uploads/extend_study/' . $extend_study_file_transkrip_name_copy
				);

				if (!is_file(FCPATH . '/uploads/extend_study/' . $extend_study_file_transkrip_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_transkrip'] = $extend_study_file_transkrip_name_copy;
			}

			if (!empty($extend_study_file_rekomendasi_uuid)) {
				$extend_study_file_rekomendasi_name_copy = date('YmdHis') . '-' . $extend_study_file_rekomendasi_name;

				rename(
					FCPATH . 'uploads/tmp/' . $extend_study_file_rekomendasi_uuid . '/' . $extend_study_file_rekomendasi_name,
					FCPATH . 'uploads/extend_study/' . $extend_study_file_rekomendasi_name_copy
				);

				if (!is_file(FCPATH . '/uploads/extend_study/' . $extend_study_file_rekomendasi_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_rekomendasi'] = $extend_study_file_rekomendasi_name_copy;
			}

			if (!empty($extend_study_file_ijin_lama_uuid)) {
				$extend_study_file_ijin_lama_name_copy = date('YmdHis') . '-' . $extend_study_file_ijin_lama_name;

				rename(
					FCPATH . 'uploads/tmp/' . $extend_study_file_ijin_lama_uuid . '/' . $extend_study_file_ijin_lama_name,
					FCPATH . 'uploads/extend_study/' . $extend_study_file_ijin_lama_name_copy
				);

				if (!is_file(FCPATH . '/uploads/extend_study/' . $extend_study_file_ijin_lama_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
					]);
					exit;
				}

				$save_data['file_ijin_lama'] = $extend_study_file_ijin_lama_name_copy;
			}


			$save_extend_study = $this->model_extend_study->change($id, $save_data);

			if ($save_extend_study) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/extend_study', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/extend_study/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/extend_study/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Extend Studys
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('extend_study_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'extend_study'), 'success');
		} else {
			set_message(cclang('error_delete', 'extend_study'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Extend Studys
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('extend_study_view');

		$this->data['extend_study'] = $this->model_extend_study->join_avaiable()->filter_avaiable()->find($id);
		if (is_null($this->data['extend_study'])) exit;

		$this->template->title('Extend Study Detail');
		$this->render('backend/standart/administrator/extend_study/extend_study_view', $this->data);
	}

	/**
	 * delete Extend Studys
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$extend_study = $this->model_extend_study->find($id);

		if (!empty($extend_study->file_progres_laporan_studi)) {
			$path = FCPATH . '/uploads/extend_study/' . $extend_study->file_progres_laporan_studi;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($extend_study->file_ket_kuliah_online)) {
			$path = FCPATH . '/uploads/extend_study/' . $extend_study->file_ket_kuliah_online;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}

		if (!empty($extend_study->file_tugas_belajar)) {
			$path = FCPATH . '/uploads/extend_study/' . $extend_study->file_tugas_belajar;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}

		if (!empty($extend_study->file_transkrip)) {
			$path = FCPATH . '/uploads/extend_study/' . $extend_study->file_transkrip;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($extend_study->file_rekomendasi)) {
			$path = FCPATH . '/uploads/extend_study/' . $extend_study->file_rekomendasi;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($extend_study->file_ijin_lama)) {
			$path = FCPATH . '/uploads/extend_study/' . $extend_study->file_ijin_lama;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}


		return $this->model_extend_study->remove($id);
	}

	/**
	* Upload Image Extend Study	* 
	* @return JSON
	*/
	public function upload_file_progres_laporan_studi_file()
	{
		if (!$this->is_allowed('extend_study_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'extend_study',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	* Delete Image Extend Study	* 
	* @return JSON
	*/
	public function delete_file_progres_laporan_studi_file($uuid)
	{
		if (!$this->is_allowed('extend_study_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_progres_laporan_studi', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'extend_study',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/extend_study/'
        ]);
	}

	/**
	* Get Image Extend Study	* 
	* @return JSON
	*/
	public function get_file_progres_laporan_studi_file($id)
	{
		if (!$this->is_allowed('extend_study_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$extend_study = $this->model_extend_study->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_progres_laporan_studi', 
            'table_name'        => 'extend_study',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/extend_study/',
            'delete_endpoint'   => 'administrator/extend_study/delete_file_progres_laporan_studi_file'
        ]);
	}
	
	/**
	* Upload Image Extend Study	* 
	* @return JSON
	*/
	public function upload_file_ket_kuliah_online_file()
	{
		if (!$this->is_allowed('extend_study_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'extend_study',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	* Delete Image Extend Study	* 
	* @return JSON
	*/
	public function delete_file_ket_kuliah_online_file($uuid)
	{
		if (!$this->is_allowed('extend_study_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_ket_kuliah_online', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'extend_study',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/extend_study/'
        ]);
	}

	/**
	* Get Image Extend Study	* 
	* @return JSON
	*/
	public function get_file_ket_kuliah_online_file($id)
	{
		if (!$this->is_allowed('extend_study_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$extend_study = $this->model_extend_study->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_ket_kuliah_online', 
            'table_name'        => 'extend_study',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/extend_study/',
            'delete_endpoint'   => 'administrator/extend_study/delete_file_ket_kuliah_online_file'
        ]);
	}

	/**
	* Upload Image Extend Study	* 
	* @return JSON
	*/
	public function upload_file_tugas_belajar_file()
	{
		if (!$this->is_allowed('extend_study_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'extend_study',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	* Delete Image Extend Study	* 
	* @return JSON
	*/
	public function delete_file_tugas_belajar_file($uuid)
	{
		if (!$this->is_allowed('extend_study_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_tugas_belajar', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'extend_study',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/extend_study/'
        ]);
	}

	/**
	* Get Image Extend Study	* 
	* @return JSON
	*/
	public function get_file_tugas_belajar_file($id)
	{
		if (!$this->is_allowed('extend_study_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$extend_study = $this->model_extend_study->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_tugas_belajar', 
            'table_name'        => 'extend_study',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/extend_study/',
            'delete_endpoint'   => 'administrator/extend_study/delete_file_tugas_belajar_file'
        ]);
	}

	/**
	 * Upload Image Extend Study	* 
	 * @return JSON
	 */
	public function upload_file_transkrip_file()
	{
		if (!$this->is_allowed('extend_study_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'extend_study',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Extend Study	* 
	 * @return JSON
	 */
	public function delete_file_transkrip_file($uuid)
	{
		if (!$this->is_allowed('extend_study_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_transkrip',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'extend_study',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/extend_study/'
		]);
	}

	/**
	 * Get Image Extend Study	* 
	 * @return JSON
	 */
	public function get_file_transkrip_file($id)
	{
		if (!$this->is_allowed('extend_study_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$extend_study = $this->model_extend_study->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_transkrip',
			'table_name'        => 'extend_study',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/extend_study/',
			'delete_endpoint'   => 'administrator/extend_study/delete_file_transkrip_file'
		]);
	}

	/**
	 * Upload Image Extend Study	* 
	 * @return JSON
	 */
	public function upload_file_rekomendasi_file()
	{
		if (!$this->is_allowed('extend_study_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'extend_study',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Extend Study	* 
	 * @return JSON
	 */
	public function delete_file_rekomendasi_file($uuid)
	{
		if (!$this->is_allowed('extend_study_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_rekomendasi',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'extend_study',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/extend_study/'
		]);
	}

	/**
	 * Get Image Extend Study	* 
	 * @return JSON
	 */
	public function get_file_rekomendasi_file($id)
	{
		if (!$this->is_allowed('extend_study_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$extend_study = $this->model_extend_study->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_rekomendasi',
			'table_name'        => 'extend_study',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/extend_study/',
			'delete_endpoint'   => 'administrator/extend_study/delete_file_rekomendasi_file'
		]);
	}

	/**
	 * Upload Image Extend Study	* 
	 * @return JSON
	 */
	public function upload_file_ijin_lama_file()
	{
		if (!$this->is_allowed('extend_study_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'extend_study',
			'allowed_types' => 'pdf',
			'max_size' 	 	=> 5120,
		]);
	}

	/**
	 * Delete Image Extend Study	* 
	 * @return JSON
	 */
	public function delete_file_ijin_lama_file($uuid)
	{
		if (!$this->is_allowed('extend_study_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		echo $this->delete_file([
			'uuid'              => $uuid,
			'delete_by'         => $this->input->get('by'),
			'field_name'        => 'file_ijin_lama',
			'upload_path_tmp'   => './uploads/tmp/',
			'table_name'        => 'extend_study',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/extend_study/'
		]);
	}

	/**
	 * Get Image Extend Study	* 
	 * @return JSON
	 */
	public function get_file_ijin_lama_file($id)
	{
		if (!$this->is_allowed('extend_study_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
			]);
			exit;
		}

		$extend_study = $this->model_extend_study->find($id);

		echo $this->get_file([
			'uuid'              => $id,
			'delete_by'         => 'id',
			'field_name'        => 'file_ijin_lama',
			'table_name'        => 'extend_study',
			'primary_key'       => 'id',
			'upload_path'       => 'uploads/extend_study/',
			'delete_endpoint'   => 'administrator/extend_study/delete_file_ijin_lama_file'
		]);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('extend_study_export');

		$this->model_extend_study->export(
			'extend_study',
			'extend_study',
			$this->model_extend_study->field_search
		);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('extend_study_export');

		$this->model_extend_study->pdf('extend_study', 'extend_study');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('extend_study_export');

		$table = $title = 'extend_study';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_extend_study->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
}


/* End of file extend_study.php */
/* Location: ./application/controllers/administrator/Extend Study.php */