<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo() {

        // Binding keys
        $('*').bind('keydown', 'Ctrl+s', function assets() {
            $('#btn_save').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+x', function assets() {
            $('#btn_cancel').trigger('click');
            return false;
        });

        $('*').bind('keydown', 'Ctrl+d', function assets() {
            $('.btn_save_back').trigger('click');
            return false;
        });

    }

    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Perpanjangan Studi <small>Edit Perpanjangan Studi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="<?= site_url('administrator/extend_study'); ?>">Perpanjangan Studi</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Perpanjangan Studi</h3>
                            <h5 class="widget-user-desc">Edit Perpanjangan Studi</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/extend_study/edit_save/' . $this->uri->segment(4)), [
                            'name'    => 'form_extend_study',
                            'class'   => 'form-horizontal form-step',
                            'id'      => 'form_extend_study',
                            'method'  => 'POST'
                        ]); ?>

                        <div class="form-group ">
                            <label for="id_study" class="col-sm-2 control-label">Universitas Studi
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control chosen chosen-select-deselect" name="id_study" id="id_study" data-placeholder="Select Universitas Studi">
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('studi_lanjut',['nip'=>$this->session->userdata('filter_nip_extend_study')]) as $row) : ?>
                                        <option <?= $row->id ==  $extend_study->id_study ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->univ_tujuan; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>



                        <div class="form-group ">
                            <label for="nip" class="col-sm-2 control-label">Nip
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nip" readonly id="nip" placeholder="Nip" value="<?= set_value('nip', $extend_study->nip); ?>">
                                <small class="info help-block">
                                    <b>Input Nip</b> Max Length : 255.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="nama" class="col-sm-2 control-label">Nama
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nama" disabled id="nama" placeholder="Nama" value="<?= set_value('nama', $extend_study->nama); ?>">
                                <small class="info help-block">
                                    <b>Input Nama</b> Max Length : 255.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="file_transkrip" class="col-sm-2 control-label">File Transkrip
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="extend_study_file_transkrip_galery"></div>
                                <input class="data_file data_file_uuid" name="extend_study_file_transkrip_uuid" id="extend_study_file_transkrip_uuid" type="hidden" value="<?= set_value('extend_study_file_transkrip_uuid'); ?>">
                                <input class="data_file" name="extend_study_file_transkrip_name" id="extend_study_file_transkrip_name" type="hidden" value="<?= set_value('extend_study_file_transkrip_name', $extend_study->file_transkrip); ?>">
                                <small class="info help-block">
                                    <b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="file_rekomendasi" class="col-sm-2 control-label">File Rekomendasi
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="extend_study_file_rekomendasi_galery"></div>
                                <input class="data_file data_file_uuid" name="extend_study_file_rekomendasi_uuid" id="extend_study_file_rekomendasi_uuid" type="hidden" value="<?= set_value('extend_study_file_rekomendasi_uuid'); ?>">
                                <input class="data_file" name="extend_study_file_rekomendasi_name" id="extend_study_file_rekomendasi_name" type="hidden" value="<?= set_value('extend_study_file_rekomendasi_name', $extend_study->file_rekomendasi); ?>">
                                <small class="info help-block">
                                    <b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="file_ijin_lama" class="col-sm-2 control-label">File Ijin Lama
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="extend_study_file_ijin_lama_galery"></div>
                                <input class="data_file data_file_uuid" name="extend_study_file_ijin_lama_uuid" id="extend_study_file_ijin_lama_uuid" type="hidden" value="<?= set_value('extend_study_file_ijin_lama_uuid'); ?>">
                                <input class="data_file" name="extend_study_file_ijin_lama_name" id="extend_study_file_ijin_lama_name" type="hidden" value="<?= set_value('extend_study_file_ijin_lama_name', $extend_study->file_ijin_lama); ?>">
                                <small class="info help-block">
                                    <b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="file_tugas_belajar" class="col-sm-2 control-label">File Tugas Belajar
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="extend_study_file_tugas_belajar_galery"></div>
                                <input class="data_file data_file_uuid" name="extend_study_file_tugas_belajar_uuid" id="extend_study_file_tugas_belajar_uuid" type="hidden" value="<?= set_value('extend_study_file_tugas_belajar_uuid'); ?>">
                                <input class="data_file" name="extend_study_file_tugas_belajar_name" id="extend_study_file_tugas_belajar_name" type="hidden" value="<?= set_value('extend_study_file_tugas_belajar_name', $extend_study->file_tugas_belajar); ?>">
                                <small class="info help-block">
                                    <b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="file_progres_laporan_studi" class="col-sm-2 control-label">File Progres Laporan Studi
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="extend_study_file_progres_laporan_studi_galery"></div>
                                <input class="data_file data_file_uuid" name="extend_study_file_progres_laporan_studi_uuid" id="extend_study_file_progres_laporan_studi_uuid" type="hidden" value="<?= set_value('extend_study_file_progres_laporan_studi_uuid'); ?>">
                                <input class="data_file" name="extend_study_file_progres_laporan_studi_name" id="extend_study_file_progres_laporan_studi_name" type="hidden" value="<?= set_value('extend_study_file_progres_laporan_studi_name', $extend_study->file_progres_laporan_studi); ?>">
                                <small class="info help-block">
                                    <b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="file_ket_kuliah_online" class="col-sm-2 control-label">File Ket. Kuliah Online
                            </label>
                            <div class="col-sm-8">
                                <div id="extend_study_file_ket_kuliah_online_galery"></div>
                                <input class="data_file data_file_uuid" name="extend_study_file_ket_kuliah_online_uuid" id="extend_study_file_ket_kuliah_online_uuid" type="hidden" value="<?= set_value('extend_study_file_ket_kuliah_online_uuid'); ?>">
                                <input class="data_file" name="extend_study_file_ket_kuliah_online_name" id="extend_study_file_ket_kuliah_online_name" type="hidden" value="<?= set_value('extend_study_file_ket_kuliah_online_name', $extend_study->file_ket_kuliah_online); ?>">
                                <small class="info help-block">
                                    <b>Extension file must</b> PDF, <b>Max size file</b> 5120 kb.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="catatan" class="col-sm-2 control-label">Catatan
                            </label>
                            <div class="col-sm-8">
                                <textarea id="catatan" name="catatan" rows="5" class="textarea form-control"><?= set_value('catatan', $extend_study->catatan); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                        <?php if (is_groups_in(['Admin', 'Kerjasama'])) : ?>
                            <div class="form-group ">
                                <label for="validasi" class="col-sm-2 control-label">Validasi
                                </label>
                                <div class="col-sm-8">
                                    <select class="form-control chosen chosen-select" name="validasi" id="validasi" data-placeholder="Select Validasi">
                                        <option value=""></option>
                                        <option <?= $extend_study->validasi == "T" ? 'selected' : ''; ?> value="T">Tidak</option>
                                        <option <?= $extend_study->validasi == "Y" ? 'selected' : ''; ?> value="Y">Ya</option>
                                    </select>
                                    <small class="info help-block">
                                    </small>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="message"></div>
                        <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                                <i class="fa fa-save"></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                                <i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                                <i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                                <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
                                <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function() {
        $('#btn_cancel').click(function() {
            swal({
                    title: "Are you sure?",
                    text: "the data that you have created will be in the exhaust!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes!",
                    cancelButtonText: "No!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                        window.location.href = BASE_URL + 'administrator/extend_study/index/' + "<?= $this->session->userdata('_page') ?>";
                    }
                });

            return false;
        }); /*end btn cancel*/

        $('.btn_save').click(function() {
            $('.message').fadeOut();

            var form_extend_study = $('#form_extend_study');
            var data_post = form_extend_study.serializeArray();
            var save_type = $(this).attr('data-stype');
            data_post.push({
                name: 'save_type',
                value: save_type
            });

            $('.loading').show();

            $.ajax({
                    url: form_extend_study.attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: data_post,
                })
                .done(function(res) {
                    $('form').find('.form-group').removeClass('has-error');
                    $('form').find('.error-input').remove();
                    $('.steps li').removeClass('error');
                    if (res.success) {
                        var id = $('#extend_study_image_galery').find('li').attr('qq-file-id');
                        if (save_type == 'back') {
                            window.location.href = res.redirect;
                            return;
                        }

                        $('.message').printMessage({
                            message: res.message
                        });
                        $('.message').fadeIn();
                        $('.data_file_uuid').val('');

                    } else {
                        if (res.errors) {
                            parseErrorField(res.errors);
                        }
                        $('.message').printMessage({
                            message: res.message,
                            type: 'warning'
                        });
                    }

                })
                .fail(function() {
                    $('.message').printMessage({
                        message: 'Error save data',
                        type: 'warning'
                    });
                })
                .always(function() {
                    $('.loading').hide();
                    $('html, body').animate({
                        scrollTop: $(document).height()
                    }, 2000);
                });

            return false;
        }); /*end btn save*/

        var params = {};
        params[csrf] = token;

        $('#extend_study_file_progres_laporan_studi_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/extend_study/upload_file_progres_laporan_studi_file',
                params: params
            },
            deleteFile: {
                enabled: true, // defaults to false
                endpoint: BASE_URL + '/administrator/extend_study/delete_file_progres_laporan_studi_file'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            session: {
                endpoint: BASE_URL + 'administrator/extend_study/get_file_progres_laporan_studi_file/<?= $extend_study->id; ?>',
                refreshOnRequest: true
            },
            multiple: false,
            validation: {
                allowedExtensions: ["pdf"],
                sizeLimit: 5242880,
            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#extend_study_file_progres_laporan_studi_galery').fineUploader('getUuid', id);
                        $('#extend_study_file_progres_laporan_studi_uuid').val(uuid);
                        $('#extend_study_file_progres_laporan_studi_name').val(xhr.uploadName);
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onSubmit: function(id, name) {
                    var uuid = $('#extend_study_file_progres_laporan_studi_uuid').val();
                    $.get(BASE_URL + '/administrator/extend_study/delete_file_progres_laporan_studi_file/' + uuid);
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#extend_study_file_progres_laporan_studi_uuid').val('');
                        $('#extend_study_file_progres_laporan_studi_name').val('');
                    }
                }
            }
        }); /*end file_progres_laporan_studi galey*/
        var params = {};
        params[csrf] = token;

        $('#extend_study_file_ket_kuliah_online_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/extend_study/upload_file_ket_kuliah_online_file',
                params: params
            },
            deleteFile: {
                enabled: true, // defaults to false
                endpoint: BASE_URL + '/administrator/extend_study/delete_file_ket_kuliah_online_file'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            session: {
                endpoint: BASE_URL + 'administrator/extend_study/get_file_ket_kuliah_online_file/<?= $extend_study->id; ?>',
                refreshOnRequest: true
            },
            multiple: false,
            validation: {
                allowedExtensions: ["pdf"],
                sizeLimit: 5242880,
            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#extend_study_file_ket_kuliah_online_galery').fineUploader('getUuid', id);
                        $('#extend_study_file_ket_kuliah_online_uuid').val(uuid);
                        $('#extend_study_file_ket_kuliah_online_name').val(xhr.uploadName);
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onSubmit: function(id, name) {
                    var uuid = $('#extend_study_file_ket_kuliah_online_uuid').val();
                    $.get(BASE_URL + '/administrator/extend_study/delete_file_ket_kuliah_online_file/' + uuid);
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#extend_study_file_ket_kuliah_online_uuid').val('');
                        $('#extend_study_file_ket_kuliah_online_name').val('');
                    }
                }
            }
        }); /*end file_ket_kuliah_online galey*/

        var params = {};
        params[csrf] = token;

        $('#extend_study_file_transkrip_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/extend_study/upload_file_transkrip_file',
                params: params
            },
            deleteFile: {
                enabled: true, // defaults to false
                endpoint: BASE_URL + '/administrator/extend_study/delete_file_transkrip_file'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            session: {
                endpoint: BASE_URL + 'administrator/extend_study/get_file_transkrip_file/<?= $extend_study->id; ?>',
                refreshOnRequest: true
            },
            multiple: false,
            validation: {
                allowedExtensions: ["pdf"],
                sizeLimit: 5242880,
            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#extend_study_file_transkrip_galery').fineUploader('getUuid', id);
                        $('#extend_study_file_transkrip_uuid').val(uuid);
                        $('#extend_study_file_transkrip_name').val(xhr.uploadName);
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onSubmit: function(id, name) {
                    var uuid = $('#extend_study_file_transkrip_uuid').val();
                    $.get(BASE_URL + '/administrator/extend_study/delete_file_transkrip_file/' + uuid);
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#extend_study_file_transkrip_uuid').val('');
                        $('#extend_study_file_transkrip_name').val('');
                    }
                }
            }
        }); /*end file_transkrip galey*/

        var params = {};
        params[csrf] = token;

        $('#extend_study_file_tugas_belajar_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/extend_study/upload_file_tugas_belajar_file',
                params: params
            },
            deleteFile: {
                enabled: true, // defaults to false
                endpoint: BASE_URL + '/administrator/extend_study/delete_file_tugas_belajar_file'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            session: {
                endpoint: BASE_URL + 'administrator/extend_study/get_file_tugas_belajar_file/<?= $extend_study->id; ?>',
                refreshOnRequest: true
            },
            multiple: false,
            validation: {
                allowedExtensions: ["pdf"],
                sizeLimit: 5242880,
            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#extend_study_file_tugas_belajar_galery').fineUploader('getUuid', id);
                        $('#extend_study_file_tugas_belajar_uuid').val(uuid);
                        $('#extend_study_file_tugas_belajar_name').val(xhr.uploadName);
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onSubmit: function(id, name) {
                    var uuid = $('#extend_study_file_tugas_belajar_uuid').val();
                    $.get(BASE_URL + '/administrator/extend_study/delete_file_tugas_belajar_file/' + uuid);
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#extend_study_file_tugas_belajar_uuid').val('');
                        $('#extend_study_file_tugas_belajar_name').val('');
                    }
                }
            }
        }); /*end file_tugas_belajar galey*/

        var params = {};
        params[csrf] = token;

        $('#extend_study_file_rekomendasi_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/extend_study/upload_file_rekomendasi_file',
                params: params
            },
            deleteFile: {
                enabled: true, // defaults to false
                endpoint: BASE_URL + '/administrator/extend_study/delete_file_rekomendasi_file'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            session: {
                endpoint: BASE_URL + 'administrator/extend_study/get_file_rekomendasi_file/<?= $extend_study->id; ?>',
                refreshOnRequest: true
            },
            multiple: false,
            validation: {
                allowedExtensions: ["pdf"],
                sizeLimit: 5242880,
            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#extend_study_file_rekomendasi_galery').fineUploader('getUuid', id);
                        $('#extend_study_file_rekomendasi_uuid').val(uuid);
                        $('#extend_study_file_rekomendasi_name').val(xhr.uploadName);
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onSubmit: function(id, name) {
                    var uuid = $('#extend_study_file_rekomendasi_uuid').val();
                    $.get(BASE_URL + '/administrator/extend_study/delete_file_rekomendasi_file/' + uuid);
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#extend_study_file_rekomendasi_uuid').val('');
                        $('#extend_study_file_rekomendasi_name').val('');
                    }
                }
            }
        }); /*end file_rekomendasi galey*/
        var params = {};
        params[csrf] = token;

        $('#extend_study_file_ijin_lama_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/extend_study/upload_file_ijin_lama_file',
                params: params
            },
            deleteFile: {
                enabled: true, // defaults to false
                endpoint: BASE_URL + '/administrator/extend_study/delete_file_ijin_lama_file'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            session: {
                endpoint: BASE_URL + 'administrator/extend_study/get_file_ijin_lama_file/<?= $extend_study->id; ?>',
                refreshOnRequest: true
            },
            multiple: false,
            validation: {
                allowedExtensions: ["pdf"],
                sizeLimit: 5242880,
            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#extend_study_file_ijin_lama_galery').fineUploader('getUuid', id);
                        $('#extend_study_file_ijin_lama_uuid').val(uuid);
                        $('#extend_study_file_ijin_lama_name').val(xhr.uploadName);
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onSubmit: function(id, name) {
                    var uuid = $('#extend_study_file_ijin_lama_uuid').val();
                    $.get(BASE_URL + '/administrator/extend_study/delete_file_ijin_lama_file/' + uuid);
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#extend_study_file_ijin_lama_uuid').val('');
                        $('#extend_study_file_ijin_lama_name').val('');
                    }
                }
            }
        }); /*end file_ijin_lama galey*/
        async function chain() {}
        chain();
    }); /*end doc ready*/
</script>