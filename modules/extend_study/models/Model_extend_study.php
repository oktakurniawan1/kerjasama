<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_extend_study extends MY_Model {

    private $primary_key    = 'id';
    private $table_name     = 'extend_study';
    public $field_search   = ['id_study', 'nip', 'file_transkrip', 'validasi'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "extend_study.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "extend_study.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "extend_study.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "extend_study.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "extend_study.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "extend_study.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
        
        $this->sortable();
        
        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {

        $this->db->join('studi_lanjut', 'studi_lanjut.id = extend_study.id_study', 'LEFT');
        $this->db->select('extend_study.*,studi_lanjut.univ_tujuan as studi_lanjut_univ_tujuan');
        $this->db->select('studi_lanjut.nama as nama');
        
        return $this;
    }

    public function filter_avaiable() {

        // if (!$this->aauth->is_admin()) {
        //     }

        // return $this;
        $this->db->where('extend_study.nip',$this->session->userdata('filter_nip_extend_study'));
        return $this;
    }
    public function find_studi_lanjut($nip = '')
    {
        return $this->db->get_where('studi_lanjut',['nip'=>$nip])->row();
    }

}

/* End of file Model_extend_study.php */
/* Location: ./application/models/Model_extend_study.php */