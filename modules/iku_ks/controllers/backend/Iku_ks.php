<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Iku Ks Controller
 *| --------------------------------------------------------------------------
 *| Iku Ks site
 *|
 */
class Iku_ks extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_iku_ks');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * show all Iku Kss
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('iku_ks_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('iku_ks_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('iku_ks_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('iku_ks_filter');
			$this->session->unset_userdata('iku_ks_field');
		}
		$filter = $this->session->userdata('iku_ks_filter');
		$field = $this->session->userdata('iku_ks_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['iku_kss'] = $this->model_iku_ks->get($filter, $field, $this->limit_page, $offset);
		$this->data['iku_ks_counts'] = $this->model_iku_ks->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/iku_ks/index/',
			'total_rows'   => $this->model_iku_ks->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Jenis Kerjasama IKU List');
		$this->render('backend/standart/administrator/iku_ks/iku_ks_list', $this->data);
	}

	/**
	 * Add new iku_kss
	 *
	 */
	public function add()
	{
		$this->is_allowed('iku_ks_add');

		$this->template->title('Jenis Kerjasama IKU New');
		$this->render('backend/standart/administrator/iku_ks/iku_ks_add', $this->data);
	}

	/**
	 * Add New Iku Kss
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('iku_ks_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('nama_iku_ks', 'Nama Jenis Kerjasama', 'trim|required');
		$this->form_validation->set_rules('bobot_iku', 'Bobot IKU', 'trim|required|callback_valid_number');


		if ($this->form_validation->run()) {

			$save_data = [
				'nama_iku_ks' => $this->input->post('nama_iku_ks'),
				'bobot_iku' => $this->input->post('bobot_iku'),
			];


			$save_iku_ks = $this->model_iku_ks->store($save_data);


			if ($save_iku_ks) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_iku_ks;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/iku_ks/edit/' . $save_iku_ks, 'Edit Iku Ks'),
						anchor('administrator/iku_ks', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/iku_ks/edit/' . $save_iku_ks, 'Edit Iku Ks')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/iku_ks');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/iku_ks');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Iku Kss
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('iku_ks_update');

		$this->data['iku_ks'] = $this->model_iku_ks->find($id);

		$this->template->title('Jenis Kerjasama IKU Update');
		$this->render('backend/standart/administrator/iku_ks/iku_ks_update', $this->data);
	}

	/**
	 * Update Iku Kss
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('iku_ks_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('nama_iku_ks', 'Nama Jenis Kerjasama', 'trim|required');
		$this->form_validation->set_rules('bobot_iku', 'Bobot IKU', 'trim|required|callback_valid_number');

		if ($this->form_validation->run()) {

			$save_data = [
				'nama_iku_ks' => $this->input->post('nama_iku_ks'),
				'bobot_iku' => $this->input->post('bobot_iku'),
			];


			$save_iku_ks = $this->model_iku_ks->change($id, $save_data);

			if ($save_iku_ks) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/iku_ks', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/iku_ks/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/iku_ks/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Iku Kss
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('iku_ks_delete');

		//cek di mapping iku
		$cek = $this->model_iku_ks->cek_mapping($id);
		if ($cek) {
			set_message('Data ini sudah digunakan di maping IKU', 'error');
			redirect_back();
		}

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'iku_ks'), 'success');
		} else {
			set_message(cclang('error_delete', 'iku_ks'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Iku Kss
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('iku_ks_view');

		$this->data['iku_ks'] = $this->model_iku_ks->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Jenis Kerjasama IKU Detail');
		$this->render('backend/standart/administrator/iku_ks/iku_ks_view', $this->data);
	}

	/**
	 * delete Iku Kss
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$iku_ks = $this->model_iku_ks->find($id);



		return $this->model_iku_ks->remove($id);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
			'nama_iku_ks',
			'bobot_iku',
		];
		$data = $this->model_iku_ks->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_iku_ks->export2($data, 'file', $selected_field);

		/*
		$this->is_allowed('iku_ks_export');

		$this->model_iku_ks->export(
			'iku_ks', 
			'iku_ks',
			$this->model_iku_ks->field_search
		);
		*/
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('iku_ks_export');

		$this->model_iku_ks->pdf('iku_ks', 'iku_ks');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('iku_ks_export');

		$table = $title = 'iku_ks';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_iku_ks->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
}


/* End of file iku_ks.php */
/* Location: ./application/controllers/administrator/Iku Ks.php */