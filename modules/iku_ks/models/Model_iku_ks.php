<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_iku_ks extends MY_Model
{

    private $primary_key    = 'id';
    private $table_name     = 'iku_ks';
    public $field_search   = ['nama_iku_ks', 'bobot_iku'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
        );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "iku_ks." . $field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "iku_ks." . $field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '(' . $where . ')';
        } else {
            $where .= "(" . "iku_ks." . $field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);

        /**form filter data */
        if ($this->session->has_userdata('filterdata_iku_ks')) {
            foreach (fromsess('filterdata_iku_ks') as $key => $row) {
                if ($row != 'all') $this->db->having($key, $row);
            }
        }
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [], $result = TRUE)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "iku_ks." . $field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "iku_ks." . $field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '(' . $where . ')';
        } else {
            $where .= "(" . "iku_ks." . $field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) and count($select_field)) {
            $this->db->select($select_field);
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);

        /**form filter data */
        if ($this->session->has_userdata('filterdata_iku_ks')) {
            foreach (fromsess('filterdata_iku_ks') as $key => $row) {
                if ($row != 'all') $this->db->having($key, $row);
            }
        }
        $this->db->limit($limit, $offset);

        $this->sortable();

        $query = $this->db->get($this->table_name);

        if ($result)
            return $query->result();
        else return $query;
    }

    public function join_avaiable()
    {

        $this->db->select('iku_ks.*');


        return $this;
    }

    public function filter_avaiable()
    {

        if (!$this->aauth->is_admin()) {
        }

        return $this;
    }
    public function cek_mapping($id_iku){
        $this->db->select('iku_map_ks.id_iku');
        $this->db->join('iku_map_ks', 'iku_map_ks.id_iku = iku_ks.id', 'left');
        $this->db->where('iku_map_ks.id_iku', $id_iku);
        $query = $this->db->get($this->table_name);
        return $query->result();
    }
}

/* End of file Model_iku_ks.php */
/* Location: ./application/models/Model_iku_ks.php */