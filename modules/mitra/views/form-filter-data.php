<form action="" method="POST" role="form">
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label for="">Scope</label>
				<?= form_dropdown('scope', $scope, fromsess('filterdata_mitra')['scope'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label for="">Jenis</label>
				<?= form_dropdown('jenis', $jenis, fromsess('filterdata_mitra')['jenis'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="">Negara</label>
				<?= form_dropdown('negara', $negara, fromsess('filterdata_mitra')['negara'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label for="">Prodi</label>
				<?= form_dropdown('kd_prodi', $prodi, fromsess('filterdata_mitra')['kd_prodi'], ['class' => 'form-control', 'style' => 'width:100%']) ?>
			</div>
		</div>
	</div>
</form>

<script>
	$(function() {
		$('select[name=name1],select[name=scope],select[name=jenis],select[name=negara],select[name=kd_prodi]').select2({
			theme: 'classic',
			dropdownParent: $('#filtermitra-dialog'),
		});
		// $('select[name=name4]').select2({
		// 	tags:true
		// })
	})
</script>