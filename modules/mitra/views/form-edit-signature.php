<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
<form method="POST" enctype="multipart/form-data" id="form_upload_ttd">
    <input type="hidden" name="id_mitra" value="<?= $ttd_mitra->id_mitra ?>">
    <input type="hidden" name="id_ttd" value="<?= $ttd_mitra->id ?>">
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    <div class="form-group">
        <label for="mitra_ttd">Nama Penanda tangan</label>
        <input type="text" class="form-control" id="mitra_ttd" name="mitra_ttd" value="<?= $ttd_mitra->nama_ttd ?>">
    </div>
    <div class="form-group">
        <label for="file_ttd">File tanda tangan</label>
        <input type="file" class="form-control" id="file_ttd" name="file_ttd">
    </div>
    <button type="button" class="btn btn-primary" id="btn_upload_ttd">Update</button>
</form>
<div class="message" style="margin-top: 10px;"></div>
<script>
    $(document).ready(function() {
        $('#btn_upload_ttd').click(function() {
            var form_data = new FormData($('#form_upload_ttd')[0]);
            $.LoadingOverlay('show');
            $.ajax({
                url: '<?= base_url('administrator/mitra/update_tanda_tangan_mitra') ?>',
                type: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response) {
                    if (response.status) {
                        $.LoadingOverlay('hide');
                        $('.message').html('<div class="alert alert-success" role="alert">Berhasil update tanda tangan</div>');
                    } else {
                        $.LoadingOverlay('hide');
                        $('.message').html('<div class="alert alert-danger" role="alert">Gagal update tanda tangan</div>');
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });
    });
</script>