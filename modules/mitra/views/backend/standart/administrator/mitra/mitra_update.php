<script src="<?= BASE_ASSET; ?>/js/dataTables.cellEdit.js"></script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Mitra <small>Edit Mitra</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class=""><a href="<?= site_url('administrator/mitra'); ?>">Mitra</a></li>
		<li class="active">Edit</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-body ">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header ">
							<div class="widget-user-image">
								<img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">Mitra</h3>
							<h5 class="widget-user-desc">Edit Mitra</h5>
							<hr>
						</div>
						<?= form_open(base_url('administrator/mitra/edit_save/' . $this->uri->segment(4)), [
							'name'    => 'form_mitra',
							'class'   => 'form-horizontal form-step',
							'id'      => 'form_mitra',
							'method'  => 'POST'
						]); ?>

						<div class="form-group ">
							<label for="scope" class="col-sm-2 control-label">Scope
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select" name="scope" id="scope" data-placeholder="Select Scope">
									<option value=""></option>
									<option <?= $mitra->scope == "DN" ? 'selected' : ''; ?> value="DN">DN</option>
									<option <?= $mitra->scope == "LN" ? 'selected' : ''; ?> value="LN">LN</option>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="nama_mitra" class="col-sm-2 control-label">Nama Mitra
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nama_mitra" id="nama_mitra" placeholder="Nama Mitra" value="<?= set_value('nama_mitra', $mitra->nama_mitra); ?>">
								<small class="info help-block">
									<b>Input Nama Mitra</b> Max Length : 255.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="jenis" class="col-sm-2 control-label">Jenis
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="jenis" id="jenis" data-placeholder="Select Jenis">
									<option value=""></option>
									<?php foreach (db_get_all_data('jenis_mitra') as $row) : ?>
										<option <?= $row->id ==  $mitra->jenis ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->jenis_mitra; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Input Jenis</b> Max Length : 11.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="telp" class="col-sm-2 control-label">Telepon
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="telp" id="telp" placeholder="Telepon" value="<?= set_value('telp', $mitra->telp); ?>">
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="email" class="col-sm-2 control-label">Email
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?= set_value('email', $mitra->email); ?>">
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="alamat" class="col-sm-2 control-label">Alamat
							</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" value="<?= set_value('alamat', $mitra->alamat); ?>">
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="negara" class="col-sm-2 control-label">Negara
								<i class="required">*</i>
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="negara" id="negara" data-placeholder="Select Negara">
									<option value=""></option>
									<?php foreach (db_get_all_data('negara') as $row) : ?>
										<option <?= $row->nama_negara ==  $mitra->negara ? 'selected' : ''; ?> value="<?= $row->nama_negara ?>"><?= $row->nama_negara; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
									<b>Input Negara</b> Max Length : 100.</small>
							</div>
						</div>

						<div class="form-group ">
							<label for="kd_prodi" class="col-sm-2 control-label">Unit / Prodi
							</label>
							<div class="col-sm-8">
								<select class="form-control chosen chosen-select-deselect" name="kd_prodi" id="kd_prodi" data-placeholder="Select Prodi" <?= ($change_prodi == false ? "disabled" : '') ?>>
									<option value=""></option>
									<?php foreach (get_filter_prodi(false) as $row) : ?>
										<option <?= $row->kode ==  $mitra->kd_prodi ? 'selected' : ''; ?> value="<?= $row->kode ?>"><?= $row->kode . ' - ' . $row->nama_prodi; ?></option>
									<?php endforeach; ?>
								</select>
								<small class="info help-block">
								</small>
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-2 control-label">Tanda tangan</label>
							<div class="col-sm-8">
								<table class="table table-bordered table-hover" id="table_signature"">
									<thead class="bg-info">
										<tr>
											<th>ID</th>
											<th>ID Mitra</th>
											<th>Created At</th>
											<th>Nama penandatangan</th>
											<th>File Tandatangan</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
								<button type="button" class="btn btn-primary btn-sm" onclick="form_add_signature('<?php echo $mitra->id; ?>')">Tambah Tanda tangan</button>
							</div>
						</div>
						<br>

						<div class="message"></div>
						<div class="row-fluid col-md-7 container-button-bottom">
							<br>
							<button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
								<i class="fa fa-save"></i> <?= cclang('save_button'); ?>
							</button>
							<a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
								<i class="ion ion-ios-list-outline"></i> <?= cclang('save_and_go_the_list_button'); ?>
							</a>
							<a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
								<i class="fa fa-undo"></i> <?= cclang('cancel_button'); ?>
							</a>
							<span class="loading loading-hide">
								<img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg">
								<i><?= cclang('loading_saving_data'); ?></i>
							</span>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				<!--/box body -->
			</div>
			<!--/box -->
		</div>
	</div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	$(document).ready(function() {
		$('#btn_cancel').click(function() {
			swal({
					title: "Are you sure?",
					text: "the data that you have created will be in the exhaust!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					cancelButtonText: "No!",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href = BASE_URL + 'administrator/mitra/index/' + "<?= $this->session->userdata('_page') ?>";
					}
				});

			return false;
		}); /*end btn cancel*/

		$('.btn_save').click(function() {
			$('.message').fadeOut();

			var form_mitra = $('#form_mitra');
			var data_post = form_mitra.serializeArray();
			var save_type = $(this).attr('data-stype');
			data_post.push({
				name: 'save_type',
				value: save_type
			});

			$('.loading').show();

			$.ajax({
					url: form_mitra.attr('action'),
					type: 'POST',
					dataType: 'json',
					data: data_post,
				})
				.done(function(res) {
					$('form').find('.form-group').removeClass('has-error');
					$('form').find('.error-input').remove();
					$('.steps li').removeClass('error');
					if (res.success) {
						var id = $('#mitra_image_galery').find('li').attr('qq-file-id');
						if (save_type == 'back') {
							window.location.href = res.redirect;
							return;
						}

						$('.message').printMessage({
							message: res.message
						});
						$('.message').fadeIn();
						$('.data_file_uuid').val('');

					} else {
						if (res.errors) {
							parseErrorField(res.errors);
						}
						$('.message').printMessage({
							message: res.message,
							type: 'warning'
						});
					}

				})
				.fail(function() {
					$('.message').printMessage({
						message: 'Error save data',
						type: 'warning'
					});
				})
				.always(function() {
					$('.loading').hide();
					$('html, body').animate({
						scrollTop: $(document).height()
					}, 2000);
				});

			return false;
		}); /*end btn save*/
		async function chain() {}
		chain();
	}); /*end doc ready*/

	function form_add_signature(id) {
		BootstrapDialog.show({
			title  : 'Form Upload Tanda tangan',
			size   : 'size-wide',
			type   : BootstrapDialog.TYPE_WARNING,
			message: function(dialog) {
				var $message = $('<div class="table-responsive"></div>');
				$.get('<?= site_url('administrator/mitra/form_add_signature') ?>', {
					id: id
				}, function(page) {
					$message.html(page);
				});
				return $message;
			},
			buttons: [{
				label: 'Tutup',
				action: function(dialog) {
					oTable.draw(false);
					dialog.close();
				}
			}],
		});
	}
	function form_edit_signature(mitra_id, signature_id) {
		BootstrapDialog.show({
			title: 'Form edit Tanda tangan',
			size: 'size-wide',
			type: BootstrapDialog.TYPE_WARNING,
			message: function(dialog) {
				var $message = $('<div class="table-responsive"></div>');
				$.get('<?= site_url('administrator/mitra/form_edit_signature') ?>', {
					signature_id: signature_id,
					mitra_id: mitra_id
				}, function(page) {
					$message.html(page);
				});
				return $message;
			},
			buttons: [{
				label: 'Tutup',
				action: function(dialog) {
					oTable.draw(false);
					dialog.close();
				}
			}],
		});
	}
	var oTable;
	$(function() {
		var mitraId = "<?=$mitra->id?>";
		oTable = $('#table_signature').DataTable({
			processing  : true,
			serverSide  : true,
			pageLength  : 5,
			lengthChange: false,
			//lengthMenu: [6, 10, 15, 25],
			ajax: {
				url : '<?php echo site_url('administrator/mitra/browse_signature') ?>',
				type: 'post',
				data: {
					id_mitra: '<?=$mitra->id ?>',
					<?= csrf_token() ?>
				},
			},
			columns: [
				{data: 'id',visible: false,searchable: false,},
				{data: 'id_mitra',visible:false,searchable:false},
				{data: 'created_at'},
				{data: 'nama_ttd'},
				{data: 'file_ttd',render:function(data,row,type){
					if (data) {
						return "<a class='fancybox' rel='group' href='<?=BASE_URL?>uploads/mitra/" + data + "'><img src='<?=BASE_URL?>uploads/mitra/" + data + "' class='image-responsive' alt='image file_ttd' title='file_ttd " + data + "' width='40px'></a>";
					} else {
						return "<span class='label label-warning'>Tidak ada file</span>";
					}
				}},
				{className: 'text-center',searchable: false,orderable: false,width: '30px'}
			],
			columnDefs: [{
				'targets': -1,
				'data': null,
				render: function(data, type, row) {
					return "<button class='btn btn-default btn-xs' type='button' onclick='form_edit_signature(" + mitraId + "," + row.id + ")'><i class='fa fa-edit text-primary'></i></button> " +
							"<button class='btn btn-default btn-xs' type='button' onclick='hapus_signature(" + row.id + ")'><i class='fa fa-trash text-danger'></i></button> ";
				}
			}]
		})
	})
	function hapus_signature(id){
		swal({
			title: "Are you sure?",
			text: "the data that you have created will be in the exhaust!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes!",
			cancelButtonText: "No!",
			closeOnConfirm: true,
			closeOnCancel: true
		},
		function(willDelete) {
			if (willDelete) {
				$.get('<?= site_url('administrator/mitra/hapus_signature') ?>', {
					id: id
				}, function(res) {
					if (res.success) {
						oTable.draw(false);
					} else {
						swal("Data gagal dihapus", {
							icon: "error",
						});
					}
				},'json');
			}
		});
		return false;
}
</script>