<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_mitra extends MY_Model
{

	private $primary_key    = 'id';
	private $table_name     = 'mitra';
	public $field_search   = ['scope', 'nama_mitra', 'jenis', 'telp', 'email', 'alamat', 'negara'];

	public function __construct()
	{
		$config = array(
			'primary_key'   => $this->primary_key,
			'table_name'    => $this->table_name,
			'field_search'  => $this->field_search,
		);

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "mitra." . $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "mitra." . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			$where .= "(" . "mitra." . $field . " LIKE '%" . $q . "%' )";
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);
		$this->db->where($this->table_name . '.deleted_at', NULL);

		/**form filter data */
		if ($this->session->has_userdata('filterdata_mitra')) {
			foreach (fromsess('filterdata_mitra') as $key => $row) {
				if ($row != 'all') $this->db->having($key, $row);
			}
		}
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [], $result = TRUE)
	{
		$iterasi = 1;
		$num = count($this->field_search);
		$where = NULL;
		$q = $this->scurity($q);
		$field = $this->scurity($field);

		if (empty($field)) {
			foreach ($this->field_search as $field) {
				if ($iterasi == 1) {
					$where .= "mitra." . $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . "mitra." . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			$where .= "(" . "mitra." . $field . " LIKE '%" . $q . "%' )";
		}

		if (is_array($select_field) and count($select_field)) {
			$this->db->select($select_field);
		}

		$this->join_avaiable()->filter_avaiable();
		$this->db->where($where);
		$this->db->where($this->table_name . '.deleted_at', NULL);

		/**form filter data */
		if ($this->session->has_userdata('filterdata_mitra')) {
			foreach (fromsess('filterdata_mitra') as $key => $row) {
				if ($row != 'all') $this->db->having($key, $row);
			}
		}
		$this->db->limit($limit, $offset);

		$this->sortable();

		$query = $this->db->get($this->table_name);

		if ($result)
			return $query->result();
		else return $query;
	}

	public function join_avaiable()
	{
		$this->db->join('jenis_mitra', 'jenis_mitra.id = mitra.jenis', 'LEFT');
		$this->db->join('negara', 'negara.nama_negara = mitra.negara', 'LEFT');
		$this->db->join('ref_prodi', 'ref_prodi.kode = mitra.kd_prodi', 'LEFT');
		$this->db->select('mitra.*,jenis_mitra.jenis_mitra as jenis_mitra_jenis_mitra,negara.nama_negara as negara_nama_negara');
		$this->db->select('ref_prodi.nama_prodi AS prodi');

		return $this;
	}

	public function filter_avaiable()
	{

		// $prodi = [];
		// if (is_member('Kaprodi')) {
		// 	if (is_non_user()) $prodi[] = fromsess('kd_prodi');
		// 	else {
		// 		$prodi = get_user_data('prodi');
		// 		$prodi = explode(',', $prodi);
		// 	}
		// 	$this->db->where_in('kd_prodi', $prodi);
		// } else if (is_member('Unit')) {
		// 	if (is_non_user()) $prodi[] = fromsess('kd_prodi');
		// 	else {
		// 		$prodi = get_user_data('prodi');
		// 		$prodi = explode(',', $prodi);
		// 	}
		// 	$this->db->where_in('kd_prodi', $prodi);
		// }

		return $this;
	}
	public function browse_mitra($filter_prodi = 'false', $page = 2)
	{
		if ($page == 1) $t1 = 'mitra_mou';
		else $t1 = $this->table_name;
		$t2 = 'jenis_mitra';

		if ($filter_prodi == 'true') $filter = get_filter_prodi(true);

		$this->load->library('Datatables', null, 'dt');
		$this->dt->set_database('default');
		$this->dt->from($t1)->join("$t2", "$t1.jenis = $t2.id", 'left');
		$this->dt->where("$t1.deleted_at", NULL);
		// filter prodi
		if ($filter_prodi == 'true') $this->dt->where_in($t1 . '.kd_prodi', $filter);
		$this->dt->select("$t1.id,$t1.scope,$t2.jenis_mitra,$t1.telp,$t1.alamat,$t1.nama_mitra,$t1.email");
		return $this->dt->generate();
	}
	public function get_mitra_by_id($id_mitra)
	{
		$this->load->library('Datatables', null, 'dtable');
		// $this->dtable->set_database('default');
		$this->dtable->select('id,id_mitra,created_at,nama_ttd,file_ttd')->where(['id_mitra' => $id_mitra])->from('mitra_kerjasama_ttd');
		return $this->dtable->generate();
	}
	public function get_ttd_mitra($id_mitra, $id)
	{
		$this->db->select('*')->from('mitra_kerjasama_ttd')->where('id_mitra', $id_mitra)->where('id', $id);
		return $this->db->get()->row();
	}
}

/* End of file Model_mitra.php */
/* Location: ./application/models/Model_mitra.php */