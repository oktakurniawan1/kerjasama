<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Mitra Controller
 *| --------------------------------------------------------------------------
 *| Mitra site
 *|
 */
class Mitra extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_mitra');
		$this->lang->load('web_lang', $this->current_lang);
		$this->load->library('removebg_service');
	}

	/**
	 * form filter data 
	 */
	public function filter_session_data()
	{
		$fields['scope'] = $this->input->post('scope');
		$fields['jenis'] = $this->input->post('jenis');
		$fields['negara'] = $this->input->post('negara');
		$fields['kd_prodi'] = $this->input->post('kd_prodi');
		$this->session->set_userdata('filterdata_mitra', $fields);
		$this->session->set_userdata('is_filtered_mitra', TRUE);
		echo json_encode(['url' => base_url('administrator/mitra')]);
	}

	public function clear_filter_session_data()
	{
		$this->session->unset_userdata('filterdata_mitra');
		$this->session->unset_userdata('is_filtered_mitra');
		echo json_encode(['url' => base_url('administrator/mitra')]);
	}

	public function form_filter_data()
	{
		if (!$this->session->has_userdata('filterdata_mitra')) {
			$fields['scope']    = 'all';
			$fields['jenis']    = 'all';
			$fields['negara']   = 'all';
			$fields['kd_prodi'] = 'all';
			$this->session->set_userdata('filterdata_mitra', $fields);
		}

		$negara        = array_column(db_get_all_data('negara'), 'nama_negara', 'nama_negara');
		$jenis         = array_column(db_get_all_data('jenis_mitra'), 'jenis_mitra', 'id');
		$prodi			= array_column(get_filter_prodi(false), 'nama_prodi', 'kode');
		$negara['all'] = 'ALL';
		$jenis['all']  = 'ALL';
		$prodi['all']  = 'ALL';

		$data['scope'] = array('DN' => 'DN', 'LN' => 'LN', 'all' => 'ALL');
		$data['jenis'] = $jenis;
		$data['negara'] = $negara;
		$data['prodi'] = $prodi;
		$this->load->view('form-filter-data', $data);
	}
	/**
	 * show all Mitras
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('mitra_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('mitra_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('mitra_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('mitra_filter');
			$this->session->unset_userdata('mitra_field');
		}
		$filter = $this->session->userdata('mitra_filter');
		$field = $this->session->userdata('mitra_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['mitras'] = $this->model_mitra->get($filter, $field, $this->limit_page, $offset);
		$this->data['mitra_counts'] = $this->model_mitra->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/mitra/index/',
			'total_rows'   => $this->model_mitra->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Mitra List');
		$this->render('backend/standart/administrator/mitra/mitra_list', $this->data);
	}

	/**
	 * Add new mitras
	 *
	 */
	public function add()
	{
		$this->is_allowed('mitra_add');

		$this->template->title('Mitra New');
		$this->render('backend/standart/administrator/mitra/mitra_add', $this->data);
	}

	/**
	 * Add New Mitras
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('mitra_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('scope', 'Scope', 'trim|required');
		$this->form_validation->set_rules('nama_mitra', 'Nama Mitra', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('jenis', 'Jenis', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('negara', 'Negara', 'trim|required|max_length[100]');


		if ($this->form_validation->run()) {

			$save_data = [
				'scope' => $this->input->post('scope'),
				'nama_mitra' => $this->input->post('nama_mitra'),
				'jenis' => $this->input->post('jenis'),
				'telp' => $this->input->post('telp'),
				'email' => $this->input->post('email'),
				'alamat' => $this->input->post('alamat'),
				'negara' => $this->input->post('negara'),
			];


			$save_mitra = $this->model_mitra->store($save_data);


			if ($save_mitra) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_mitra;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/mitra/edit/' . $save_mitra, 'Edit Mitra'),
						anchor('administrator/mitra', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/mitra/edit/' . $save_mitra, 'Edit Mitra')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/mitra');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/mitra');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Mitras
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('mitra_update');

		$this->data['mitra'] = $this->model_mitra->find($id);

		if (!empty(trim($this->data['mitra']->kd_prodi))) {
			if (in_array($this->data['mitra']->kd_prodi, get_filter_prodi(true))) 	$this->data['change_prodi'] = TRUE;
			else $this->data['change_prodi'] = FALSE;
		} else $this->data['change_prodi'] = TRUE;

		$this->template->title('Mitra Update');
		$this->render('backend/standart/administrator/mitra/mitra_update', $this->data);
	}

	/**
	 * Update Mitras
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('mitra_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('scope', 'Scope', 'trim|required');
		$this->form_validation->set_rules('nama_mitra', 'Nama Mitra', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('jenis', 'Jenis', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('negara', 'Negara', 'trim|required|max_length[100]');

		if ($this->form_validation->run()) {

			$save_data = [
				'scope' => $this->input->post('scope'),
				'nama_mitra' => $this->input->post('nama_mitra'),
				'jenis' => $this->input->post('jenis'),
				'telp' => $this->input->post('telp'),
				'email' => $this->input->post('email'),
				'alamat' => $this->input->post('alamat'),
				'negara' => $this->input->post('negara'),
			];
			if (!is_null($this->input->post('kd_prodi'))) $save_data['kd_prodi'] = $this->input->post('kd_prodi');

			$save_mitra = $this->model_mitra->change($id, $save_data);

			if ($save_mitra) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/mitra', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/mitra/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/mitra/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Mitras
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('mitra_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'mitra'), 'success');
		} else {
			set_message(cclang('error_delete', 'mitra'), 'error');
		}

		redirect_back();
	}

	/**
	 * View view Mitras
	 *
	 * @var $id String
	 */
	public function view($id)
	{
		$this->is_allowed('mitra_view');
		$this->data['mitra'] = $this->model_mitra->join_avaiable()->filter_avaiable()->find($id);
		$this->template->title('Mitra Detail');
		$this->render('backend/standart/administrator/mitra/mitra_view', $this->data);
	}

	/**
	 * delete Mitras
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$mitra = $this->model_mitra->find($id);
		// return $this->model_mitra->remove($id);
		return $this->model_mitra->softdelete($id);
	}


	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$filter = $this->session->userdata('q');
		$field = $this->session->userdata('f');
		$offset = $this->session->userdata('_page');

		$selected_field = [
			'scope',
			'nama_mitra',
			'jenis',
			'telp',
			'email',
			'alamat',
			'negara',
		];
		$data = $this->model_mitra->get($filter, $field, 0, $offset, [], FALSE);
		$this->model_mitra->export2($data, 'file', $selected_field);

		/*
		$this->is_allowed('mitra_export');

		$this->model_mitra->export(
			'mitra', 
			'mitra',
			$this->model_mitra->field_search
		);
		*/
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('mitra_export');

		$this->model_mitra->pdf('mitra', 'mitra');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('mitra_export');

		$table = $title = 'mitra';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_mitra->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
	/**
	 * update signature
	 * @return void 
	 */
	public function form_edit_signature()
	{
		$mitra_id = $this->input->get('mitra_id');
		$signature_id = $this->input->get('signature_id');
		$data['ttd_mitra'] = $this->model_mitra->get_ttd_mitra($mitra_id, $signature_id);
		$this->load->view('form-edit-signature', $data);
	}

	public function update_tanda_tangan_mitra()
	{
		$config['upload_path']      = FCPATH . '/uploads/mitra/';
		$config['allowed_types']    = 'png|jpg|jpeg';
		$config['max_size']         = 2048;
		$config['file_name']        = 'mitra_ttd_' . date('YmdHis') . $this->session->userdata('user_id');
		$config['overwrite']        = TRUE;
		$config['file_ext_tolower'] = TRUE;
		$config['detect_mime']      = TRUE;
		$config['mod_mime_fix']     = TRUE;
		$this->load->library('upload', $config);

		$updateData = [
			'created_at' => date('Y-m-d H:i:s'),
			'nama_ttd'   => $this->input->post('mitra_ttd'),
		];

		if (!empty($_FILES['file_ttd']['name'])) {
			if (!$this->upload->do_upload('file_ttd')) {
				$error = array('error' => $this->upload->display_errors());
				echo json_encode(['status' => FALSE, 'message' => $error]);
				return;
			} else {
				$data               = array('upload_data' => $this->upload->data());
				$uploaded_file_path = $data['upload_data']['full_path'];
				$result             = $this->removebg_service->removeBg($uploaded_file_path);
				file_put_contents($uploaded_file_path, $result);
				$mitra_ttd = $this->model_mitra->get_ttd_mitra($this->input->post('id_mitra'), $this->input->post('id_ttd'));
				if (file_exists(FCPATH . '/uploads/mitra/' . $mitra_ttd->file_ttd))  unlink(FCPATH . '/uploads/mitra/' . $mitra_ttd->file_ttd);
				$updateData['file_ttd'] = $data['upload_data']['file_name'];
			}
		}

		$this->db->where('id', $this->input->post('id_ttd'))
			->where('id_mitra', $this->input->post('id_mitra'))
			->update('mitra_kerjasama_ttd', $updateData);
		echo json_encode(['status' => TRUE, 'message' => $updateData]);
	}
	/**
	 * form add signature
	 * @return void 
	 */
	public function form_add_signature()
	{
		$data['mitra_id'] = $this->input->get('id');
		$this->load->view('form-add-signature', $data);
	}
	public function upload_tanda_tangan_mitra()
	{
		// Check if file is included in the form submission
		if (isset($_FILES['file_ttd']) && $_FILES['file_ttd']['size'] > 0) {
			$config['upload_path']      = FCPATH . '/uploads/mitra/';
			$config['allowed_types']    = 'png|jpg|jpeg';
			$config['max_size']         = 2048;
			$config['file_name']        = 'mitra_ttd_' . date('YmdHis') . $this->session->userdata('user_id');
			$config['overwrite']        = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$config['detect_mime']      = TRUE;
			$config['mod_mime_fix']     = TRUE;
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('file_ttd')) {
				$error = array('error' => $this->upload->display_errors());
				echo json_encode(['status' => FALSE, 'message' => $error]);
				return;
			} else {
				$data               = array('upload_data' => $this->upload->data());
				$uploaded_file_path = $data['upload_data']['full_path'];
				$result             = $this->removebg_service->removeBg($uploaded_file_path);
				file_put_contents($uploaded_file_path, $result);
				$file_ttd           = $data['upload_data']['file_name'];
			}
		} else {
			$file_ttd = null;
		}
		// Insert data into the database
		$this->db->insert('mitra_kerjasama_ttd', [
			'id_mitra'   => $this->input->post('mitra_id'),
			'created_at' => date('Y-m-d H:i:s'),
			'nama_ttd'   => $this->input->post('mitra_ttd'),
			'file_ttd'   => $file_ttd
		]);

		echo json_encode(['status' => TRUE, 'message' => 'Data successfully saved']);
	}
	public function browse_signature()
	{
		echo $this->model_mitra->get_mitra_by_id($this->input->post('id_mitra'));
	}
	function hapus_signature()
	{
		$id = $this->input->get('id');
		$query = $this->db->get_where('mitra_kerjasama_ttd', array('id' => $id), 'row');
		$row = $query->row();
		$response = array(
			'success' => false,
			'message' => 'Data tidak ditemukan'
		);

		if (isset($row)) {
			$file_ttd = $row->file_ttd;
			if (!empty($file_ttd)) {
				$file_path = FCPATH . '/uploads/mitra/' . $file_ttd;
				if (file_exists($file_path) && is_file($file_path)) {
					unlink($file_path);
				}
			}
			$this->db->delete('mitra_kerjasama_ttd', array('id' => $id));
			$response = array(
				'success' => true,
				'message' => 'Data berhasil dihapus'
			);
		} else {
			$response = array(
				'success' => false,
				'message' => 'Data tidak ditemukan'
			);
		}
		echo json_encode($response);
	}
}


/* End of file mitra.php */
/* Location: ./application/controllers/administrator/Mitra.php */