<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['mitra'] = 'Mitra';
$lang['id'] = 'Id';
$lang['scope'] = 'Scope';
$lang['nama_mitra'] = 'Nama Mitra';
$lang['jenis'] = 'Jenis';
$lang['telp'] = 'Telepon';
$lang['email'] = 'Email';
$lang['alamat'] = 'Alamat';
$lang['negara'] = 'Negara';
