<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *| --------------------------------------------------------------------------
 *| Skim Sipp Controller
 *| --------------------------------------------------------------------------
 *| Skim Sipp site
 *|
 */
class Skim_sipp extends Admin
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_skim_sipp');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	 * show all Skim Sipps
	 *
	 * @var $offset String
	 */
	public function index($offset = 0)
	{
		$this->is_allowed('skim_sipp_list');

		$q = $this->input->get('q');
		$f = $this->input->get('f');
		if (!is_null($q)) $this->session->set_userdata('skim_sipp_filter', $q);
		if (!is_null($f)) $this->session->set_userdata('skim_sipp_field', $f);
		$this->session->set_userdata('_page', $offset);

		if (!is_null($this->input->get('reset_filter'))) {
			$this->session->unset_userdata('skim_sipp_filter');
			$this->session->unset_userdata('skim_sipp_field');
		}
		$filter = $this->session->userdata('skim_sipp_filter');
		$field = $this->session->userdata('skim_sipp_field');
		$offset = $this->session->userdata('_page', $offset);

		$this->data['skim_sipps'] = $this->model_skim_sipp->get($filter, $field, $this->limit_page, $offset);
		$this->data['skim_sipp_counts'] = $this->model_skim_sipp->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/skim_sipp/index/',
			'total_rows'   => $this->model_skim_sipp->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Skim Sipp List');
		$this->render('backend/standart/administrator/skim_sipp/skim_sipp_list', $this->data);
	}

	/**
	 * Add new skim_sipps
	 *
	 */
	public function add()
	{
		$this->is_allowed('skim_sipp_add');

		$this->template->title('Skim Sipp New');
		$this->render('backend/standart/administrator/skim_sipp/skim_sipp_add', $this->data);
	}

	/**
	 * Add New Skim Sipps
	 *
	 * @return JSON
	 */
	public function add_save()
	{
		if (!$this->is_allowed('skim_sipp_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('output_ks', 'output Kerjasama', 'trim|max_length[5]');

		if ($this->form_validation->run()) {

			$save_data = [
				'output_ks' => $this->input->post('output_ks'),
				'used' => $this->input->post('used'),
			];

			$save_skim_sipp = $this->model_skim_sipp->store($save_data);

			if ($save_skim_sipp) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_skim_sipp;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/skim_sipp/edit/' . $save_skim_sipp, 'Edit Skim Sipp'),
						anchor('administrator/skim_sipp', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
							anchor('administrator/skim_sipp/edit/' . $save_skim_sipp, 'Edit Skim Sipp')
						]),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/skim_sipp');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/skim_sipp');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * Update view Skim Sipps
	 *
	 * @var $id String
	 */
	public function edit($id)
	{
		$this->is_allowed('skim_sipp_update');

		$this->data['skim_sipp'] = $this->model_skim_sipp->find($id);

		$this->template->title('Skim Sipp Update');
		$this->render('backend/standart/administrator/skim_sipp/skim_sipp_update', $this->data);
	}

	/**
	 * Update Skim Sipps
	 *
	 * @var $id String
	 */
	public function edit_save($id)
	{
		if (!$this->is_allowed('skim_sipp_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
			]);
			exit;
		}

		$this->form_validation->set_rules('output_ks', 'output Kerjasama', 'trim|max_length[5]');

		if ($this->form_validation->run()) {

			$save_data = [
				'output_ks' => $this->input->post('output_ks'),
				'used' => $this->input->post('used'),
			];

			$save_skim_sipp = $this->model_skim_sipp->change($id, $save_data);

			if ($save_skim_sipp) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/skim_sipp', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', []),
						'success'
					);

					$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/skim_sipp/index/' . $this->session->userdata('_page'));
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/skim_sipp/index/' . $this->session->userdata('_page'));
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

	/**
	 * delete Skim Sipps
	 *
	 * @var $id String
	 */
	public function delete($id = null)
	{
		$this->is_allowed('skim_sipp_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
			set_message(cclang('has_been_deleted', 'skim_sipp'), 'success');
		} else {
			set_message(cclang('error_delete', 'skim_sipp'), 'error');
		}

		redirect_back();
	}

	/**
	 * delete Skim Sipps
	 *
	 * @var $id String
	 */
	private function _remove($id)
	{
		$skim_sipp = $this->model_skim_sipp->find($id);
		return $this->model_skim_sipp->remove($id);
	}

	/**
	 * Export to excel
	 *
	 * @return Files Excel .xls
	 */
	public function export()
	{
		$this->is_allowed('skim_sipp_export');

		$this->model_skim_sipp->export(
			'skim_sipp',
			'skim_sipp',
			$this->model_skim_sipp->field_search
		);
	}

	/**
	 * Export to PDF
	 *
	 * @return Files PDF .pdf
	 */
	public function export_pdf()
	{
		$this->is_allowed('skim_sipp_export');

		$this->model_skim_sipp->pdf('skim_sipp', 'skim_sipp');
	}

	public function single_pdf($id = null)
	{
		$this->is_allowed('skim_sipp_export');

		$table = $title = 'skim_sipp';
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->setDefaultFont('stsongstdlight');

		$result = $this->db->get($table);

		$data = $this->model_skim_sipp->find($id);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
			'data' => $data,
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}
	public function set_used()
	{
		if($this->model_skim_sipp->updateUsed($this->input->post()));
		$val = ($this->input->post('val') == 'true' ? 'used' : 'unused');
		echo 'update to : '.$val;
	}
}

/* End of file skim_sipp.php */
/* Location: ./application/controllers/administrator/Skim Sipp.php */