<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_skim_sipp extends MY_Model
{

    private $primary_key    = 'id';
    private $table_name     = 'skim_sipp';
    public $field_search   = ['bidang', 'id_skim', 'nama_skim', 'output_ks', 'used'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
        );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "skim_sipp." . $field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "skim_sipp." . $field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '(' . $where . ')';
        } else {
            $where .= "(" . "skim_sipp." . $field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "skim_sipp." . $field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "skim_sipp." . $field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '(' . $where . ')';
        } else {
            $where .= "(" . "skim_sipp." . $field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) and count($select_field)) {
            $this->db->select($select_field);
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);

        $this->sortable();

        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable()
    {
        $this->db->join('output_sipp', 'output_sipp.id = skim_sipp.output_ks', 'LEFT');

        $this->db->select('skim_sipp.*,output_sipp.output_name as output_sipp_output_name');


        return $this;
    }

    public function filter_avaiable()
    {

        if (!$this->aauth->is_admin()) {
        }

        return $this;
    }
    public function updateUsed($post)
    {
        //print_r($post);die();
        $val = ($post['val'] == 'true' ? 'Y' : 'N');
        $this->db->where('id',$post['id'])->update('skim_sipp',['used'=>$val]);
    }
}

/* End of file Model_skim_sipp.php */
/* Location: ./application/models/Model_skim_sipp.php */