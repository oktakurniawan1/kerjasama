<?php
defined('BASEPATH') or exit('No direct script access allowed');


class MY_Model extends CI_Model
{

	private $primary_key = 'id';
	private $table_name = 'table';
	private $field_search = [];

	protected $query;

	public function __construct($config = array())
	{
		parent::__construct();

		foreach ($config as $key => $val) {
			if (isset($this->$key))
				$this->$key = $val;
		}

		$this->load->database();
	}
	public function model_sql()
	{
		return $this->query->get_compiled_select($this->table_name);
	}
	public function model_result()
	{
		return $this->query->get($this->table_name)->result();
	}
	public function model_object()
	{
		return $this->query->get($this->table_name);
	}

	public function remove($id = NULL)
	{
		$this->catatlog($this->table_name, 'delete', []);
		$this->db->where($this->primary_key, $id);
		return $this->db->delete($this->table_name);
	}

	public function softdelete($id = NULL)
	{
		$this->catatlog($this->table_name, 'soft delete', []);
		$this->db->where($this->primary_key, $id);
		return $this->db->update($this->table_name, ['deleted_at' => date('Y-m-d H:i:s')]);
	}

	public function change($id = NULL, $data = array())
	{
		$this->catatlog($this->table_name, 'update', $data);
		// $this->antrian_telegram('UBAH DATA',$data);
		$this->db->where($this->primary_key, $id);
		$this->db->update($this->table_name, $data);

		return $this->db->affected_rows();
	}

	public function find($id = NULL, $select_field = [])
	{
		if (is_array($select_field) and count($select_field)) {
			$this->db->select($select_field);
		}

		$this->db->where("" . $this->table_name . '.' . $this->primary_key, $id);
		$query = $this->db->get($this->table_name);

		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return FALSE;
		}
	}

	public function find_all()
	{
		$this->db->order_by($this->primary_key, 'DESC');
		$query = $this->db->get($this->table_name);

		return $query->result();
	}

	public function store($data = array())
	{
		$this->catatlog($this->table_name, 'insert', $data);
		$this->antrian_telegram('TAMBAH DATA', $data);
		$this->db->insert($this->table_name, $data);
		return $this->db->insert_id();
	}

	public function get_all_data($table = '')
	{
		$query = $this->db->get($table);

		return $query->result();
	}

	public function get_where($where)
	{
		$query = $this->db->get_where($this->table_name, $where);

		return $query->result();
	}


	public function get_single($where)
	{
		$query = $this->db->get_where($this->table_name, $where);

		return $query->row();
	}

	public function scurity($input)
	{
		return mysqli_real_escape_string($this->db->conn_id, $input);
	}

	public function generate_url($field, $text = null, $except = null)
	{
		$url = url_title($text);
		if ($except) {
			$this->db->where($this->primary_key . " != " . $except);
		}
		$this->db->order_by($this->primary_key, 'DESC');
		$data = $this->db->get_where($this->table_name, [$field => $url])->row();

		if ($data) {
			return $url . ($data->id += 1);
		}

		return $url;
	}

	public function export($table, $subject = 'file', $field_search = [])
	{
		$iterasi = 1;
		$where = NULL;
		$q = $this->scurity($this->input->get('q'));
		$field = $this->scurity($this->input->get('f'));
		if (empty($field)) {
			foreach ($field_search as $field) {
				if ($iterasi == 1) {
					$where .= $table . "." . $field . " LIKE '%" . $q . "%' ";
				} else {
					$where .= "OR " . $table . "." . $field . " LIKE '%" . $q . "%' ";
				}
				$iterasi++;
			}

			$where = '(' . $where . ')';
		} else {
			$where .= "(" . $table . "." . $field . " LIKE '%" . $q . "%' )";
		}

		if (method_exists($this, 'join_avaiable') && method_exists($this, 'filter_avaiable')) {
			$this->join_avaiable()->filter_avaiable();
		}
		if (!empty($q)) {
			$this->db->where($where);
		}

		$this->load->library('excel');

		$result = $this->db->get($table);

		$this->excel->setActiveSheetIndex(0);

		$fields = $result->list_fields();

		$alphabet = 'ABCDEFGHIJKLMOPQRSTUVWXYZ';
		$alphabet_arr = str_split($alphabet);
		$column = [];

		foreach ($alphabet_arr as $alpha) {
			$column[] =  $alpha;
		}

		foreach ($alphabet_arr as $alpha) {
			foreach ($alphabet_arr as $alpha2) {
				$column[] =  $alpha . $alpha2;
			}
		}
		foreach ($alphabet_arr as $alpha) {
			foreach ($alphabet_arr as $alpha2) {
				foreach ($alphabet_arr as $alpha3) {
					$column[] =  $alpha . $alpha2 . $alpha3;
				}
			}
		}

		foreach ($column as $col) {
			$this->excel->getActiveSheet()->getColumnDimension($col)->setWidth(20);
		}

		$col_total = $column[count($fields) - 1];

		//styling
		$this->excel->getActiveSheet()->getStyle('A1:' . $col_total . '1')->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'DA3232')
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				)
			)
		);

		$phpColor = new PHPExcel_Style_Color();
		$phpColor->setRGB('FFFFFF');

		$this->excel->getActiveSheet()->getStyle('A1:' . $col_total . '1')->getFont()->setColor($phpColor);

		$this->excel->getActiveSheet()->getRowDimension(1)->setRowHeight(40);

		$this->excel->getActiveSheet()->getStyle('A1:' . $col_total . '1')
			->getAlignment()->setWrapText(true);

		$col = 0;
		foreach ($fields as $field) {

			$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, ucwords(str_replace('_', ' ', $field)));
			$col++;
		}

		$row = 2;
		foreach ($result->result() as $data) {
			$col = 0;
			foreach ($fields as $field) {
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
				$col++;
			}

			$row++;
		}

		//set border
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		$this->excel->getActiveSheet()->getStyle('A1:' . $col_total . '' . $row)->applyFromArray($styleArray);

		$this->excel->getActiveSheet()->setTitle(ucwords($subject));

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename=' . ucwords($subject) . '-' . date('Y-m-d') . '.xls');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		$objWriter->save('php://output');
	}

	public function export2($result, $subject = 'file', $selected_field = [], $format = [], $local = false, $file_name = '')
	{
		$this->load->library('excel');

		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Data');

		if (count($selected_field))
			// $fields = array_intersect($result->list_fields(), $selected_field);
			$fields = $selected_field;
		else $fields = $result->list_fields();

		$alphabet = 'ABCDEFGHIJKLMOPQRSTUVWXYZ';
		$alphabet_arr = str_split($alphabet);
		$column = [];

		foreach ($alphabet_arr as $alpha) {
			$column[] =  $alpha;
		}

		foreach ($alphabet_arr as $alpha) {
			foreach ($alphabet_arr as $alpha2) {
				$column[] =  $alpha . $alpha2;
			}
		}
		foreach ($alphabet_arr as $alpha) {
			foreach ($alphabet_arr as $alpha2) {
				foreach ($alphabet_arr as $alpha3) {
					$column[] =  $alpha . $alpha2 . $alpha3;
				}
			}
		}

		foreach ($column as $col) {
			$this->excel->getActiveSheet()->getColumnDimension($col)->setWidth(20);
		}

		$col_total = $column[count($fields) - 1];

		//styling
		$this->excel->getActiveSheet()->getStyle('A1:' . $col_total . '1')->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'DA3232')
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				)
			)
		);

		$phpColor = new PHPExcel_Style_Color();
		$phpColor->setRGB('FFFFFF');

		$this->excel->getActiveSheet()->getStyle('A1:' . $col_total . '1')->getFont()->setColor($phpColor);

		$this->excel->getActiveSheet()->getRowDimension(1)->setRowHeight(40);

		$this->excel->getActiveSheet()->getStyle('A1:' . $col_total . '1')
			->getAlignment()->setWrapText(true);

		$col = 0;
		foreach ($fields as $field) {

			$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, ucwords(str_replace('_', ' ', $field)));
			$col++;
		}

		$row = 2;
		foreach ($result->result() as $data) {
			$col = 0;
			foreach ($fields as $field) {
				if (isset($format[$field])) {
					switch ($format[$field]) {
						case 'link':
							# code...
							if ($data->$field != '') {
								$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
								$link = is_cli() ? $this->config->item('site_url_cli') . 'file/' . $data->$field : site_url('file/' . $data->$field);
								$this->excel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getHyperlink()->setUrl($link);
							}
							break;
						case 'url':
							if (isURL($data->$field)) {
								$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
								$this->excel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getHyperlink()->setUrl($data->$field);
							} else
								$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
							break;
						default:
							# code...
							break;
					}
				} else
					$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
				$col++;
			}

			$row++;
		}

		//set border
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		$this->excel->getActiveSheet()->getStyle('A1:' . $col_total . '' . $row)->applyFromArray($styleArray);

		$this->excel->getActiveSheet()->setTitle(ucwords($subject));

		/**
		 *  save localhost
		 */
		if ($local) {
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			$objWriter->save(FCPATH . 'uploads/excel/' . $file_name);
			return;
		}

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename=' . ucwords($subject) . '-' . date('Y-m-d') . '.xls');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		$objWriter->save('php://output');
	}

	public function pdf($table, $title)
	{
		$this->load->library('HtmlPdf');

		$config = array(
			'orientation' => 'p',
			'format' => 'a4',
			'marges' => array(5, 5, 5, 5)
		);

		$this->pdf = new HtmlPdf($config);

		$result = $this->db->get($table);
		$fields = $result->list_fields();

		$content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf', [
			'results' => $result->result(),
			'fields' => $fields,
			'title' => $title
		], TRUE);

		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output($table . '.pdf', 'H');
	}

	public function generate_id($suffix = null)
	{
		$format = $suffix . (new DateTime)->format('Ymd');
		$exist = $this->db->query('SELECT * FROM ' . $this->table_name . ' WHERE ' . $this->primary_key . '  LIKE "%' . $format . '%" ORDER BY ' . $this->primary_key . ' DESC');

		$numbering = '0001';
		if ($exist->num_rows()) {
			$last = $exist->row();
			$last_numbering = substr($last->{$this->primary_key}, -4);
			$next_number = $last_numbering += 1;
			$numbering = sprintf("%04d", $next_number);

			return $format . $numbering;
		} else {
			return $format . $numbering;
		}
	}

	public function sortable()
	{
		$sb = $this->input->get('sb');
		$st = $this->input->get('st') == 'ASC' ? 'ASC' : 'DESC';
		if ($sb) {
			if (in_array($sb, $this->field_search)) {
				$this->db->order_by($sb, $st);
			}
		} else {
			$this->db->order_by($this->table_name . '.' . $this->primary_key, "DESC");
		}
	}
	private function catatlog($tablename, $action, $data = array())
	{
		if ($tablename != 'syslog') {
			$user = get_user_name();
			$log['username'] = $user->username;
			$log['full_name'] = $user->full_name;
			$log['action'] = $action;
			$log['table_name'] = $tablename;
			$log['waktu'] = date('Y-m-d H:i:s');
			$log['data'] = json_encode($data);
			$this->db->insert('syslog', $log);
		}
	}
	private function notif_validasi()
	{
		$tablearray = ['si_kerjasama', 'mbkm', 'dosen_inbound1', 'dosen_inbound2', 'dosen_inbound3', 'dosen_inbound4', 'dosen_inbound5', 'studi_lanjut'];
	}
	private function antrian_telegram($action, $data)
	{
		$user = get_user_name();
		$tablearray = ['si_kerjasama', 'mbkm', 'dosen_inbound1', 'dosen_inbound2', 'dosen_inbound3', 'dosen_inbound4', 'dosen_inbound5', 'studi_lanjut'];
		if (!is_groups_in(['Admin', 'Kerjasama'])) {
			if (in_array($this->table_name, $tablearray)) {
				$cek = db_get_all_data('telegram_notif', false, 'result');
				$map = $this->config->item('notif_map');
				$notif = $this->config->item('notif_telegram');
				$kolom = $map[$this->table_name];
				foreach ($cek as $row) {
					if ($row->$kolom == 'Y') {
						$nomor = '';
						$data2['identitas'] = $row->nip;
						$data2['waktu'] = date('Y-m-d H:i:s');
						if (isset($data['doc_nomor'])) $nomor = ' Nomor Dokumen : ' . $data['doc_nomor'];
						$data2['pesan'] = '__' . date('Y-m-d H:i:s') . '__ ' . $user->full_name . ' melakuan ' . $action . ' pada ' . $notif[$kolom] . $nomor;
						$this->db->insert('antrian_telegram', $data2);
					}
				}
			}
		}
	}
	public function insertJobExport(string $sql, string $title, array $format = [], $selected_field = [])
	{
		$this->load->helper('string');
		$data['sql_query']      = $sql;
		$data['title']          = $title;
		$data['file_name']      = random_string('alnum', 10) . '.xls';
		$data['selected_field'] = json_encode($selected_field);
		$data['created_at']     = date('Y-m-d:H:i:s');
		$data['format']         = json_encode($format);
		$data['user_id']        = $this->aauth->get_user_id();
		return $this->db->insert('export_file', $data);
	}
}

/* End of file My_Model.php */

/* Location: ./application/core/My_Model.php */