<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Charts_Bar extends CI_Driver
{
    private $chartdata;

    public function __construct()
    {
        $this->chartdata['type'] = 'bar';

    }

    public function show_chart($data){



        $this->chartdata['series'] = array(
            'name' => $data['chart_data_name'],
            'type' => 'bar',
            'barWidth' => '60%',
            'data' => $data['y-data']
        );

        $this->chartdata['legend'] = $data['chart_data_name'];

        $this->chartdata["div_id"] = $data['div_id'];
        if(isset($data['multiple']))
            $this->chartdata["multiple"] = $data['multiple'];


        $this->chartdata['tooltip'] = array(
            'trigger' => 'axis',
            'axisPointer' => array('type' => 'shadow')
        );


        $this->chartdata['x-axis'] = array(
            'name' => $data['x-axis_title'],
            'axisTick' => array('alignWithLabel' => 'true'),
            'data' => $data['x-data']
        );
        $this->chartdata ['y-axis_title'] = "value";


        return $this->create($this->chartdata);

    }
}