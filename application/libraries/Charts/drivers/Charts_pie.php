<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Charts_pie extends CI_Driver
{
    private $chartdata;

    public function __construct()
    {
        $this->chartdata['type'] = 'pie';
        $this->chart = new Charts();
    }

    public function show_chart($data){

        $this->chartdata['series'] = array(
            'type' => 'pie',
            'data' => $data['p-data']
        );

        $this->chartdata['legend'] = array(
            'data' => $data['legend'],
            'bottom' => 10
        );

        $this->chartdata["div_id"] = $data['div_id'];

        if(isset($data['sub_title'])){
            $this->chartdata['title'] = array(
                "text" => $data['chart_data_name'],
                "subtext" => $data['sub_title'],
                "x"    => "center"
            );
        }else {
            $this->chartdata['title'] = array(
                "text" => $data['chart_data_name'],
                "x"    => "center"
            );
        }

        return $this->create($this->chartdata);

    }
}