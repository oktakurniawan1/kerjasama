<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Charts_line extends CI_Driver
{
    private $chartdata;

    public function __construct()
    {
        $this->chartdata['type'] = 'line';
        $this->chartdata['id'] = 'line';
    }

    public function show_chart($data){
        if(isset($data['multiple']))
            $this->chartdata["multiple"] = $data['multiple'];

        $this->chartdata['series'] = array(
            'name' => $data['chart_data_name'],
            'type' => 'line',
            'smooth' => 'true',
            'data' => $data['y-data']
        );

        $this->chartdata['legend'] = $data['chart_data_name'];

        $this->chartdata['x-axis'] = array(
                                    'name' => $data['x-axis_title'],
                                    'data' => $data['x-data']
                                );
        $this->chartdata ['y-axis_title'] = "value";

        $this->chartdata["div_id"] = $data['div_id'];

        return $this->create($this->chartdata);

    }
}