<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Charts extends CI_Driver_Library {

    public $valid_drivers;
    public $CI;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->subs = array('bar','line', 'map','pie','multiple');
        $this->valid_drivers = $this->subs;

        $this->multiple_chart = new Hisune\EchartsPHP\ECharts;
        $this->multiple_yaxis = new Hisune\EchartsPHP\Doc\IDE\YAxis;
        $this->multiple_chart->tooltip->show = true;


    }

    public function create($data){

        if(isset($data['multiple'])){
            $chart = $this->multiple_chart;
            $yaxis = $this->multiple_yaxis;
        }else {
            $chart = new Hisune\EchartsPHP\ECharts;
            $yaxis = new Hisune\EchartsPHP\Doc\IDE\YAxis;
            $chart->tooltip->show = true;
        }

        switch($data['type']){
            case 'pie':
                $chart->legend[] = $data['legend'];
                break;

            default :

                $chart->legend->data[] = $data['legend'];

                $chart->color = array("#3398DB");

                if(isset($data['x-axis']))
                    $chart->xAxis[] = $data['x-axis'];

                $yaxis->type = $data['y-axis_title'];

                $chart->addYAxis($yaxis);

                break;

        }

        if(isset($data['tooltip']))
            $chart->tooltip = $data['tooltip'];

        if(isset($data['grid']))
            $chart->grid = $data['grid'];

        if(isset($data['title'])){
            $chart->title[] = $data['title'];
        }

        $chart->series[] = $data['series'];

        $chart =  $chart->render($data['div_id']);

        return $chart;

    }


}