<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tree
{

    protected $CI;
    protected $table;
    protected $id_column = 'id';
    protected $parent_column = 'parent_id';

    public function __construct($params = [])
    {
        // Get the CI instance
        $this->CI = &get_instance();

        // Set table name if provided
        if (isset($params['table'])) {
            $this->table = $params['table'];
        }

        // Set custom id column if provided
        if (isset($params['id_column'])) {
            $this->id_column = $params['id_column'];
        }

        // Set custom parent column if provided
        if (isset($params['parent_column'])) {
            $this->parent_column = $params['parent_column'];
        }
    }

    public function hasChildren($id)
    {
        $this->CI->db->where($this->parent_column, $id);
        $query = $this->CI->db->get($this->table);
        return $query->num_rows() > 0;
    }

    public function setTable($table)
    {
        $this->table = $table;
    }

    public function setIdColumn($id_column)
    {
        $this->id_column = $id_column;
    }

    public function setParentColumn($parent_column)
    {
        $this->parent_column = $parent_column;
    }

    public function getChilds($parent_id)
    {
        $this->CI->db->where($this->parent_column, $parent_id);
        $query = $this->CI->db->get($this->table);
        return $query->result();
    }

    public function getParent($child_id)
    {
        $this->CI->db->where($this->id_column, $child_id);
        $query = $this->CI->db->get($this->table);
        $row = $query->row();

        if ($row) {
            $this->CI->db->where($this->id_column, $row->{$this->parent_column});
            $query = $this->CI->db->get($this->table);
            return $query->row();
        }

        return null;
    }

    public function getAllTree($parent_id = null)
    {
        $tree = [];
        $this->CI->db->where($this->parent_column, $parent_id);
        $query = $this->CI->db->get($this->table);
        $results = $query->result();

        foreach ($results as $result) {
            $result->childs = $this->getAllTree($result->{$this->id_column});
            $tree[] = $result;
        }

        return $tree;
    }

    public function getAll($parent_id = null, $level = 0, &$result = [])
    {
        $this->CI->db->where($this->parent_column, $parent_id);
        $query = $this->CI->db->get($this->table);
        $rows = $query->result();

        foreach ($rows as $row) {
            $row->level        = $level;
            $row->has_children = $this->hasChildren($row->{$this->id_column});
            $result[]          = $row;
            $this->getAll($row->{$this->id_column}, $level + 1, $result);
        }

        return $result;
    }

    public function getAllParents($child_id)
    {
        $parents = [];
        $current_id = $child_id;

        while ($current_id) {
            $this->CI->db->where($this->id_column, $current_id);
            $query = $this->CI->db->get($this->table);
            $row = $query->row();

            if ($row) {
                $parents[] = $row;
                $current_id = $row->{$this->parent_column};
            } else {
                $current_id = null;
            }
        }

        return $parents;
    }
}
