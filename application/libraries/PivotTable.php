<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class PivotTable
{

    protected $ci;
    protected $data;
    protected $columns;
    protected $rows;
    protected $values;
    protected $aggregation;
    protected $valueFunction;
    protected $customAggregationFunction;
    protected $theme;
    protected $spreadsheet;
    protected $filename;

    public function __construct()
    {
        // Memperoleh instance CodeIgniter
        $this->ci = &get_instance();
    }

    // Mengatur data input
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    // Mengatur kolom
    public function setColumns($columns)
    {
        $this->columns = $columns;
        return $this;
    }

    // Mengatur baris
    public function setRows($rows)
    {
        $this->rows = $rows;
        return $this;
    }

    // Mengatur nilai
    public function setValues($values, $aggregation = 'SUM')
    {
        if (is_callable($values)) {
            $this->valueFunction = $values;
            $this->aggregation = $aggregation;
        } else {
            $this->values = $values;
            $this->aggregation = $aggregation;
        }
        return $this;
    }

    // Mengatur fungsi agregasi khusus
    public function setCustomAggregation($function)
    {
        if (is_callable($function)) {
            $this->customAggregationFunction = $function;
        }
        return $this;
    }

    // Mengatur tema
    public function setTheme($theme)
    {
        $this->theme = $theme;
        return $this;
    }

    // Fungsi untuk membuat pivot table
    public function makePivot()
    {
        $pivot = [];
        $columns = [];
        $rows = [];

        foreach ($this->data as $item) {
            $rowKey = $item[$this->rows];
            $colKey = $item[$this->columns];

            if (!isset($rows[$rowKey])) {
                $rows[$rowKey] = [];
            }
            if (!isset($columns[$colKey])) {
                $columns[$colKey] = [];
            }

            $value = isset($this->valueFunction) ? call_user_func($this->valueFunction, $item) : $item[$this->values];
            $rows[$rowKey][$colKey][] = $value;
            $columns[$colKey][$rowKey][] = $value;
        }

        foreach ($rows as $rowKey => $cols) {
            foreach ($columns as $colKey => $vals) {
                if (!isset($pivot[$rowKey])) {
                    $pivot[$rowKey] = [];
                }
                if (!isset($pivot[$rowKey][$colKey])) {
                    $pivot[$rowKey][$colKey] = 0;
                }

                switch ($this->aggregation) {
                    case 'SUM':
                        $pivot[$rowKey][$colKey] = array_sum($cols[$colKey]);
                        break;
                    case 'COUNT':
                        $pivot[$rowKey][$colKey] = count($cols[$colKey]);
                        break;
                    case 'AVERAGE':
                        $pivot[$rowKey][$colKey] = array_sum($cols[$colKey]) / count($cols[$colKey]);
                        break;
                    case 'CUSTOM':
                        if (isset($this->customAggregationFunction)) {
                            $pivot[$rowKey][$colKey] = call_user_func($this->customAggregationFunction, $cols[$colKey]);
                        }
                        break;
                        // Tambahkan kasus lain sesuai kebutuhan
                }
            }
        }

        return $pivot;
    }

    // Fungsi untuk mengekspor pivot table ke Excel
    public function exportToExcel($filename = 'pivot_table.xlsx')
    {
        $pivot = $this->makePivot();

        $this->spreadsheet = new Spreadsheet();
        $sheet = $this->spreadsheet->getActiveSheet();

        // Menulis header kolom
        $columnHeaders = array_keys(reset($pivot));
        $sheet->fromArray(array_merge([''], $columnHeaders), NULL, 'A1');

        // Menulis data baris
        $rowNumber = 2;
        foreach ($pivot as $rowKey => $rowData) {
            $rowDataWithKey = array_merge([$rowKey], array_values($rowData));
            $sheet->fromArray($rowDataWithKey, NULL, 'A' . $rowNumber);
            $rowNumber++;
        }

        $this->filename = $filename;
        return $this;
    }

    // Fungsi untuk menyimpan file ke path
    public function save($path)
    {
        $writer = new Xlsx($this->spreadsheet);
        $filePath = rtrim($path, '/') . '/' . $this->filename;
        $writer->save($filePath);
        return $filePath;
    }

    // Fungsi untuk mengunduh file
    public function download()
    {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . basename($this->filename) . '"');
        header('Cache-Control: max-age=0');

        $writer = IOFactory::createWriter($this->spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

    // Fungsi untuk menampilkan pivot table dalam format HTML
    public function toHtmlTable()
    {
        $pivot = $this->makePivot();

        // Menentukan kelas CSS berdasarkan tema
        $tableClass = '';
        $headerClass = '';
        $cellClass = '';

        switch ($this->theme) {
            case 'bootstrap':
                $tableClass = 'table table-bordered table-striped';
                $headerClass = 'table-dark';
                break;
            case 'tailwind':
                $tableClass = 'min-w-full divide-y divide-gray-200';
                $headerClass = 'bg-gray-50';
                $cellClass = 'px-6 py-4 whitespace-nowrap';
                break;
            default:
                break;
        }

        $html = '<table class="' . $tableClass . '" border="1" cellpadding="5" cellspacing="0">';

        // Menulis header kolom
        $columnHeaders = array_keys(reset($pivot));
        $html .= '<tr><th class="' . $headerClass . '"></th>';
        foreach ($columnHeaders as $header) {
            $html .= '<th class="' . $headerClass . '">' . $header . '</th>';
        }
        $html .= '</tr>';

        // Menulis data baris
        foreach ($pivot as $rowKey => $rowData) {
            $html .= '<tr><th class="' . $headerClass . '">' . $rowKey . '</th>';
            foreach ($rowData as $cell) {
                $html .= '<td class="' . $cellClass . '">' . $cell . '</td>';
            }
            $html .= '</tr>';
        }

        $html .= '</table>';
        return $html;
    }
}
