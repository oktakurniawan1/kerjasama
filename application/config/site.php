<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| Your site name,
|
*/
$config['site_name'] = 'Sistem Informasi Kerjasama';
$config['version'] = '3.1.0';
