<?php

$notif['notif_1'] = 'Kerjasama Unit';
$notif['notif_2'] = 'Kerjasama MBKM';
$notif['notif_3'] = 'Dosen Inbound';
$notif['notif_4'] = 'Mahasiswa Mobility';
$notif['notif_5'] = 'Pengajuan Setneg';
$notif['notif_6'] = 'Dosen Studi Lanjut';

$config['notif_telegram'] = $notif;

$config['notif_map'] = array(
	'si_kerjasama' 	=> 'notif_1',
	'mbkm' 				=> 'notif_2',
	'dosen_inbound1'	=> 'notif_3',
	'dosen_inbound2'	=> 'notif_3',
	'dosen_inbound3'	=> 'notif_3',
	'dosen_inbound4'	=> 'notif_3',
	'dosen_inbound5'	=> 'notif_3',
	'dosen_inbound6'	=> 'notif_3',
	'mhs_mobility'		=> 'notif_4',
	'perjalun'			=> 'notif_5',
	'studi_lanjut'		=> 'notif_6',
);
