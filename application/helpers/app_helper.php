<?php

use GuzzleHttp\Client;

if (!function_exists('get_mysql_version')) {
	function get_mysql_version()
	{
		$mysql_info = explode(' ', mysqli_get_client_info());
		$mysql_version = isset($mysql_info[1]) ? $mysql_info[1] : false;
		$mysql_version_number = explode('-', $mysql_version)[0];
		if ($mysql_version_number) {
			return $mysql_version_number;
		} else if (isset($mysql_info[0])) {
			return (int) substr($mysql_info[0], 0, 3);
		}
		return 5;
	}
}

if (!function_exists('get_database_config')) {
	function get_database_config($param = '')
	{
		if (file_exists($file_path = APPPATH . '/config/database.php')) {
			include($file_path);
		}
		if (isset($db[$active_group][$param])) {
			return $db[$active_group][$param];
		}
	}
}

if (!function_exists('redirect_back')) {
	function redirect_back($url = '')
	{
		if (isset($_SERVER['HTTP_REFERER'])) {
			header('Location: ' . $_SERVER['HTTP_REFERER']);
		} else {
			redirect($url);
		}
		exit;
	}
}

if (!function_exists('db_get_all_data')) {
	function db_get_all_data($table_name = null, $where = false, $res = 'result')
	{
		$ci = &get_instance();
		if ($where) {
			$ci->db->where($where);
		}
		$query = $ci->db->get($table_name);
		switch ($res) {
			case 'result':
				return $query->result();
				break;
			case 'row':
				return $query->row();
				break;
			case 'row_array':
				return $query->row_array();
				break;
			case 'num':
				return $query->num_rows();
				break;
			default:
				return $query->result();
				break;
		}
	}
}
if (!function_exists('db_get_select')) {
	function db_get_select($select = '*', $table_name = null, $where = false, $res = 'result')
	{
		$ci = &get_instance();
		if ($where) {
			$ci->db->where($where);
		}
		$query = $ci->db->select($select)->get($table_name);
		switch ($res) {
			case 'result':
				return $query->result();
				break;
			case 'row':
				return $query->row();
				break;
			case 'row_array':
				return $query->row_array();
				break;
			case 'num':
				return $query->num_rows();
				break;
			default:
				return $query->result();
				break;
		}
	}
}

function build_data_option($table_name = null, $key, $val, $where = false, $default = array())
{
	if (!empty($default))
		$option = "<option value='{$default[0]}'>{$default[1]}</option>";
	else
		$option = "<option value=''></option>";

	$ci = &get_instance();
	if ($where) {
		$ci->db->where($where);
	}
	$query = $ci->db->get($table_name)->result();
	foreach ($query as $row) {
		$option .= "<option value='{$row->$key}'>{$row->$val}</option>";
	}
	return $option;
}

function build_query_option($query, $key, $val, $where = false, $default = array())
{
	if (!empty($default))
		$option = "<option value='{$default[0]}'>{$default[1]}</option>";
	else
		$option = "<option value=''></option>";

	$ci = &get_instance();
	if ($where) {
		$ci->db->where($where);
	}
	foreach ($query as $row) {
		$option .= "<option value='{$row->$key}'>{$row->$val}</option>";
	}
	return $option;
}

function send_telegram($identitas, $pesan)
{

	$client = new Client([
		'base_uri' => 'https://services.unnes.ac.id/api/',
		'timeout' => 30,
		'verify' => false,
		'auth' => ['simkerjasama', 'kadalbuntung']
	]);
	$response = $client->request('POST', 'sendnotif', [
		'form_params' => ['identitas' => [$identitas], 'pesan' => $pesan]
	]);
	return $response;
}

function get_user_name()
{
	$user = new StdClass();
	if (is_non_user()) {
		$username = fromsess('identitas');
		$full_name = fromsess('nama');
	} else {
		$username = get_user_data('username');
		$full_name = get_user_data('full_name');
	}
	$user->username = $username;
	$user->full_name = $full_name;
	return $user;
}


function get_data_option($table_name = null, $key, $val, $where = false, $default = array())
{
	$tmp = array();
	if (!empty($default)) $tmp[$default[0]] = $default[1];
	$ci = &get_instance();
	if ($where) {
		$ci->db->where($where);
	}
	$query = $ci->db->get($table_name)->result();
	foreach ($query as $row) {
		$tmp[$row->$key] = $row->$val;
	}
	return $tmp;
}

if (!function_exists('db_get_wherein')) {
	function db_get_wherein($table_name = null, $where = false)
	{
		$ci = &get_instance();
		if ($where) {
			$ci->db->where_in($where[0], $where[1]);
		}
		$query = $ci->db->get($table_name);
		return $query->result();
	}
}
if (!function_exists('db_pluck')) {
	function db_pluck($table_name = null, $key_column = null, $value_column = null, $where = false)
	{
		$ci = &get_instance();
		if ($where) {
			$ci->db->where($where);
		}
		$ci->db->select([$key_column, $value_column]);
		$query = $ci->db->get($table_name);
		$result = $query->result();
		$plucked = array();
		foreach ($result as $row) {
			$plucked[$row->$key_column] = $row->$value_column;
		}
		return $plucked;
	}
}

if (!function_exists('get_filter_prodi')) {
	function get_filter_prodi($flat = false)
	{
		$ci           = &get_instance();
		$filter_prodi = [];

		if (is_groups_in(['Unit', 'Kaprodi'])) {
			if (is_non_user()) {
				$prodi = fromsess('kd_prodi');
				$filter_prodi = $ci->db->query("SELECT * FROM ref_prodi WHERE kode = '$prodi' ")->result();
			} else {
				$filter_prodi = get_hirarki_prodi();
			}
		} else if (is_groups_in(['Admin', 'Kerjasama', 'Viewer'])) {
			$filter_prodi = $ci->db->query("SELECT * FROM ref_prodi")->result();
		} else if (is_member('Dosen')) {
			if (is_non_user()) {
				$prodi = fromsess('kd_prodi');
				$filter_prodi = $ci->db->query("SELECT * FROM ref_prodi WHERE kode = '$prodi' ")->result();
			} else $filter_prodi = get_hirarki_prodi();
		} else if (is_member('Tendik')) {
			if (is_non_user()) {
				$prodi = fromsess('kd_prodi');
				$filter_prodi = $ci->db->query("SELECT * FROM ref_prodi WHERE kode = '$prodi' ")->result();
			} else $filter_prodi = get_hirarki_prodi();
		}
		if ($flat) {
			foreach ($filter_prodi as $row) {
				$kodes[] = $row->kode;
			}
			return $kodes;
		} else return $filter_prodi;
	}
}
function antrianTelegram($pesan, $username)
{
	$ci = &get_instance();
	$data['identitas'] = $username;
	$data['waktu'] = date('Y-m-d H:i:s');
	$data['pesan'] = $pesan;
	$ci->db->insert('antrian_telegram', $data);
}

function get_hirarki_prodi()
{
	$ci           = &get_instance();
	$filter_prodi = [];
	$prodi = get_user_data('prodi');
	$arr_prodi = explode(',', $prodi);
	$prodi_imp = "'" . implode("','", $arr_prodi) . "'";

	//cari kode fakultas
	$prodi_fak = $ci->db->query("SELECT tingkat,kode_fak FROM ref_prodi WHERE kode IN ($prodi_imp) ")->result_array();
	$fak = [];
	foreach ($prodi_fak as $row) {
		if ($row['tingkat'] == 2) $fak[] = $row['kode_fak'];
	}
	// cari id unit dari parent fakultas
	$id_unit = [];
	if (!empty($fak)) {
		$fak_imp = "'" . implode("','", $fak) . "'";
		$unit = $ci->db->query("SELECT id FROM ref_prodi WHERE kode_fak IN ($fak_imp) ")->result_array();
		$id_unit = array_column($unit, 'id');
	}
	//cari id unit prodi akses
	$unit2    = $ci->db->query("SELECT * FROM ref_prodi WHERE kode IN ($prodi_imp)")->result_array();
	$id_unit2 = array_column($unit2, 'id');

	// gabungkan
	$id_unit_merger     = array_unique(array_merge($id_unit, $id_unit2));
	$id_unit_merger_imp = "'" . implode("','", $id_unit_merger) . "'";
	$filter_prodi       = $ci->db->query("SELECT * FROM ref_prodi WHERE id IN ($id_unit_merger_imp) ")->result();

	return $filter_prodi;
}
if (!function_exists('implode_quote')) {
	function implode_quote($array)
	{
		return sprintf("'%s'", implode("','", $array));
	}
}


if (!function_exists('is_image')) {
	function is_image($filename = '')
	{
		$array = explode('.', $filename);
		$extension = strtolower(end($array));
		$list_image_ext = ['', 'png', 'jpg', 'jpeg', 'gif'];
		if (array_search($extension, $list_image_ext)) {
			return TRUE;
		}
		return FALSE;
	}
}
if (!function_exists('time_ago')) {
	function time_ago($timestamp)
	{
		$time_ago = strtotime($timestamp);
		$current_time = time();
		$time_difference = $current_time - $time_ago;
		$seconds = $time_difference;
		$minutes      = round($seconds / 60);           // value 60 is seconds
		$hours        = round($seconds / 3600);         // value 3600 is 60 minutes * 60 sec
		$days         = round($seconds / 86400);        // value 86400 is 24 hours * 60 minutes * 60 sec
		$weeks        = round($seconds / 604800);       // value 604800 is 7 days * 24 hours * 60 minutes * 60 sec
		$months       = round($seconds / 2629440);      // value 2629440 is ((365+365+365+365+366)/5/12) days * 24 hours * 60 minutes * 60 sec
		$years        = round($seconds / 31553280);     // value 31553280 is ((365+365+365+365+366)/5) days * 24 hours * 60 minutes * 60 sec

		if ($seconds <= 60) {
			return "Baru saja";
		} else if ($minutes <= 60) {
			if ($minutes == 1) {
				return "1 menit lalu";
			} else {
				return "$minutes menit lalu";
			}
		} else if ($hours <= 24) {
			if ($hours == 1) {
				return "1 jam lalu";
			} else {
				return "$hours jam lalu";
			}
		} else if ($days <= 7) {
			if ($days == 1) {
				return "kemarin";
			} else {
				return "$days hari lalu";
			}
		} else if ($weeks <= 4.3) {  // 4.3 == 30/7
			if ($weeks == 1) {
				return "1 minggu lalu";
			} else {
				return "$weeks minggu lalu";
			}
		} else if ($months <= 12) {
			if ($months == 1) {
				return "1 bulan lalu";
			} else {
				return "$months bulan lalu";
			}
		} else {
			if ($years == 1) {
				return "1 tahun lalu";
			} else {
				return "$years tahun lalu";
			}
		}
	}
}

if (!function_exists('format_tgl_surat')) {
	function format_tgl_surat($date)
	{
		// Validasi apakah parameter adalah tanggal
		if (!strtotime($date)) {
			return 'Invalid date';
		}

		$day_names = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', "Jum'at", 'Sabtu');
		$month_names = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
		$timestamp = strtotime($date);

		if ($timestamp === false) {
			return 'Invalid date';
		}

		$day = date('w', $timestamp);
		$month = date('n', $timestamp);
		$formatted_date = $day_names[$day] . ', ' . date('j', $timestamp) . ' ' . $month_names[$month - 1] . ' ' . date('Y', $timestamp);
		return $formatted_date;
	}
}

function isURL($url)
{
	return filter_var($url, FILTER_VALIDATE_URL) !== false;
}

if (!function_exists('clean_snake_case')) {
	function clean_snake_case($text = '')
	{
		$text = preg_replace('/_/', ' ', $text);
		return $text;
	}
}

if (!function_exists('get_user_groups')) {
	function get_user_groups($id_user = '')
	{
		$ci = &get_instance();
		return $ci->aauth->get_user_groups($id_user);
	}
}

if (!function_exists('is_member')) {
	function is_member($group_name)
	{
		$ci = &get_instance();
		//$group_id = $ci->aauth->get_group_id($group_name);
		return $ci->aauth->is_member($group_name);
	}
}

if (!function_exists('is_groups_in')) {
	function is_groups_in($groups = [], $user_id = false)
	{
		$ci = &get_instance();
		$ids = $ci->aauth->get_user_groups($user_id);
		$result = [];
		foreach ($ids as $row) {
			$result[] = $row->name;
		}
		return count(array_intersect($groups, $result)) > 0;
	}
}
if (!function_exists('is_only_groups')) {
	function is_only_groups($groups = [], $user_id = false)
	{
		$ci = &get_instance();
		$ids = $ci->aauth->get_user_groups($user_id);
		$result = [];
		foreach ($ids as $row) {
			$result[] = $row->name;
		}
		$result = array_diff($result, ['Default']);
		if (count($result) > 1) return false;
		else
			return count(array_intersect($groups, $result)) > 0;
	}
}

if (!function_exists('is_admin')) {
	function is_admin()
	{
		$ci = &get_instance();
		return $ci->aauth->is_admin();
	}
}

function obj2arr($d)
{
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}
	if (is_array($d)) {
		/*
        * Using __FUNCTION__ (Magic constant)
        * for recursive call
        */
		return array_map(__FUNCTION__, $d);
	} else {
		// Return array
		return $d;
	}
}

function arr2obj($d)
{
	if (is_array($d)) {
		/*
        * Using __FUNCTION__ (Magic constant)
        * for recursive call
        */
		return (object) array_map(__FUNCTION__, $d);
	} else {
		// Return object
		return $d;
	}
}

function find_rules($fieldname, $validation_rules)
{
	foreach ($validation_rules as $rule) {
		if ($rule['field'] === $fieldname && isset($rule['rules'])) {
			if (strpos($rule['rules'], 'required') !== false) {
				return true;
			}
		}
	}
	return false;
}

function enkrip_dekrip($string = '', $act = 'enkrip')
{
	// Store the cipher method 
	$ciphering = "AES-128-CTR";
	// Use OpenSSl Encryption method 
	$iv_length = openssl_cipher_iv_length($ciphering);
	$options = 0;
	// Non-NULL Initialization Vector for encryption 
	$encryption_iv = '1234567891011121';
	// Store the encryption key 
	$encryption_key = "GeeksforGeeks";
	// Use openssl_encrypt() function to encrypt the data
	if ($act == 'enkrip') {
		return openssl_encrypt(
			$string,
			$ciphering,
			$encryption_key,
			$options,
			$encryption_iv
		);
	} else {
		// Non-NULL Initialization Vector for decryption 
		$decryption_iv = '1234567891011121';
		// Store the decryption key 
		$decryption_key = "GeeksforGeeks";
		// Use openssl_decrypt() function to decrypt the data 
		return openssl_decrypt(
			$string,
			$ciphering,
			$decryption_key,
			$options,
			$decryption_iv
		);
	}
}


if (!function_exists('get_user_data')) {
	function get_user_data($field_name = '')
	{
		$ci = &get_instance();
		$user_id = $ci->session->userdata('id');
		if ($user_id) {
			if (empty($field_name)) {
				return $ci->aauth->get_user($user_id);
			} else {
				return $ci->aauth->get_user($user_id)->$field_name;
			}
		}
		return false;
	}
}

if (!function_exists('is_allowed')) {
	function is_allowed($permission, Closure $func)
	{
		$ci = &get_instance();
		$reflection = new ReflectionFunction($func);
		$arguments  = $reflection->getParameters();
		if ($ci->aauth->is_allowed($permission)) {
			call_user_func($func, $arguments);
		} else {
			ob_start();
			call_user_func($func, $arguments);
			$buffer = ob_get_contents();
			ob_end_clean();
		}
	}
}

if (!function_exists('is_non_user')) {
	function is_non_user()
	{
		$ci = &get_instance();
		return $ci->session->userdata('non_user');
	}
}

if (!function_exists('fromsess')) {
	function fromsess($key = '')
	{
		$ci = &get_instance();
		if ($key == '') return $ci->session->userdata();
		else return $ci->session->userdata($key);
	}
}

if (!function_exists('message_flash')) {
	function message_flash($message, $type)
	{
		$ci = &get_instance();
		$ci->session->set_flashdata('f_message', $message);
		$ci->session->set_flashdata('f_type', $type);
	}
}

if (!function_exists('html_alert')) {
	function html_alert($message, $title = 'Informasi', $type = 'info')
	{
		return
			"<div class='alert alert-$type'>
			 <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
			 <strong>$title</strong> $message
			</div>";
	}
}

if (!function_exists('display_menu_module')) {
	function display_menu_module($parent, $level, $menu_type, $ignore_active = false)
	{
		$ci = &get_instance();
		$ci->load->database();
		$ci->load->model('menu/model_menu');
		$menu_type_id = $ci->model_menu->get_id_menu_type_by_flag($menu_type);
		$result = $ci->db->query("SELECT a.id, a.label, a.type, a.active, a.link, Deriv1.Count FROM `menu` a  LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count FROM `menu` GROUP BY parent) Deriv1 ON a.id = Deriv1.parent WHERE a.menu_type_id = " . $menu_type_id . " AND a.parent=" . $parent . " " . ($ignore_active ? '' : 'and active = 1') . " order by `sort` ASC")->result();
		$ret = '';
		$display_menu_dropdown = function ($row = null) {
			return '<div class="dropdown  pull-right">
					    <a class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-cog "></i>
					    <span class="caret"></span></a>
					    <ul class="dropdown-menu">
					      <li><a class="remove-data" href="javascript:void()" data-href="' . site_url('administrator/menu/delete/' . $row->id) . '"><i class="fa fa-trash btn-action"></i> <span class="text-danger">Delete</span></a></li>
					      <li><a href="' . site_url('administrator/menu/edit/' . $row->id) . '"><i class="fa fa-pencil btn-action"></i> Edit</a></li> 
					      ' . ($row->type != 'label' ? '
					      <li><a href="' . site_url('administrator/menu/add/' . app()->uri->segment(4) . '?parent=' . $row->id) . '"><i class="fa fa-level-down btn-action"></i> Add child</a></li>
					      ' : '') . '
					    </ul>
					  </div>';
		};
		if ($result) {
			$ret .= '<ol class="dd-list">';
			foreach ($result as $row) {
				if ($row->Count > 0) {
					$ret .= '<li class="dd-item dd3-item ' . ($row->active ? '' : 'menu-toggle-activate_inactive') . ' menu-toggle-activate" data-id="' . $row->id . '" data-status="' . $row->active . '">';
					if ($row->type != 'label') {
						$ret .= '<div class="dd-handle dd3-handle dd-handles"></div>';
						$ret .= '<div class="dd3-content">' . _ent($row->label);
					} else {
						$ret .= '<div class="dd-handle dd3-handle dd-handles dd-handle-label"></div>';
						$ret .= '<div class="dd3-content "><b>' . _ent($row->label) . '</b>';
					}
					$ret .= $display_menu_dropdown($row);
					$ret .= '</div>';
					$ret .= display_menu_module($row->id, $level + 1, $menu_type, $ignore_active);
					$ret .= "</li>";
				} elseif ($row->Count == 0) {
					$ret .= '<li class="dd-item dd3-item ' . ($row->active ? '' : 'menu-toggle-activate_inactive') . ' menu-toggle-activate" data-id="' . $row->id . '" data-status="' . $row->active . '">';
					if ($row->type != 'label') {
						$ret .= '<div class="dd-handle dd3-handle dd-handles"></div>';
						$ret .= '<div class="dd3-content">' . _ent($row->label);
					} else {
						$ret .= '<div class="dd-handle dd3-handle dd-handles dd-handle-label"></div>';
						$ret .= '<div class="dd3-content  "><b>' . _ent($row->label) . '</b>';
					}
					$ret .= $display_menu_dropdown($row);
					$ret .= '</div></li>';
				}
			}
			$ret .= "</ol>";
		}
		return $ret;
	}
}

if (!function_exists('display_menu_admin')) {
	function display_menu_admin($parent, $level)
	{
		$ci = &get_instance();
		$ci->load->database();
		$ci->load->model('menu/model_menu');
		$result = $ci->db->query("SELECT a.id, a.label,a.icon_color, a.type, a.link,a.icon, Deriv1.Count FROM `menu` a  LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count FROM `menu` GROUP BY parent) Deriv1 ON a.id = Deriv1.parent WHERE a.menu_type_id = 1 AND a.parent=" . $parent . " and active = 1  order by `sort` ASC")->result();
		$ret = '';
		if ($result) {
			if (($level > 1) and ($parent > 0)) {
				$ret .= '<ul class="treeview-menu">';
			} else {
				$ret = '';
			}
			foreach ($result as $row) {
				$row->link = parse_nav_url($row->link);
				$perms = 'menu_' . strtolower(str_replace(' ', '_', $row->label));
				$links = explode('/', $row->link);
				$segments = array_slice($ci->uri->segment_array(), 0, count($links));
				if (implode('/', $segments) == implode('/', $links)) {
					$active = 'active';
				} else {
					$active = '';
				}
				$link = filter_var($row->link, FILTER_VALIDATE_URL) ? $row->link : base_url($row->link);
				if ($row->type == 'label') {
					if ($ci->aauth->is_allowed($perms)) {
						$ret .= '<li class="header treeview">' . _ent($row->label) . '</li>';
					}
				} else {
					if ($row->Count > 0) {
						if ($ci->aauth->is_allowed($perms)) {
							$ret .= '<li class="' . $active . ' "> 
										        	<a href="' . $link . '">';
							if ($parent) {
								$ret .= '<i class="fa fa-circle-o ' . _ent($row->icon_color) . '"></i> <span>' . _ent($row->label) . '</span>
									            <span class="pull-right-container">
									              <i class="fa fa-angle-left pull-right"></i>
									            </span>
									          </a>';
							} else {
								$ret .= '<i class="fa ' . _ent($row->icon) . ' ' . _ent($row->icon_color) . '"></i> <span>' . _ent($row->label) . '</span>
									            <span class="pull-right-container">
									              <i class="fa fa-angle-left pull-right"></i>
									            </span>
									          </a>';
							}
							$ret .= display_menu_admin($row->id, $level + 1);
							$ret .= "</li>";
						}
					} elseif ($row->Count == 0) {
						if ($ci->aauth->is_allowed($perms)) {
							$ret .= '<li class="' . $active . ' "> 
										        	<a href="' . $link . '">';
							if ($parent) {
								$ret .= '<i class="fa fa-circle-o ' . _ent($row->icon_color) . '"></i> <span>' . _ent($row->label) . '</span>
									            <span class="pull-right-container"></i>
									            </span>
									          </a>';
							} else {
								$ret .= '<i class="fa ' . _ent($row->icon) . ' ' . _ent($row->icon_color) . '"></i> <span>' . _ent($row->label) . '</span>
									            <span class="pull-right-container"></i>
									            </span>
									          </a>';
							}
							$ret .= "</li>";
						}
					}
				}
				if ($row->link == ADMIN_NAMESPACE_URL . '/extension') {
					$ret .= cicool()->getSidebar();
				}
			}
			if ($level != 1) {
				$ret .= '</ul>';
			}
		}
		return $ret;
	}
}

if (!function_exists('set_message')) {
	function set_message($message = null, $type = 'success')
	{
		$ci = &get_instance();
		$ci->session->set_flashdata('f_message', $message);
		$ci->session->set_flashdata('f_type', $type);
	}
}

if (!function_exists('form_builder')) {
	function form_builder($id = 0)
	{
		$ci = &get_instance();
		$model_form = $ci->load->model('form/model_form');
		$form = $ci->model_form->find($id);
		if ($form) {
			$form_name = strtolower($form->table_name);
			$ci->template->title($form->title);
			return $ci->load->view('form/public/' . $form_name . '/' . $form_name, [], true);
		} else {
			return false;
		}
	}
}

if (!function_exists('get_icon_file')) {
	function get_icon_file($file_name = '')
	{
		$extension_list = [
			'avi' => ['avi'],
			'css' => ['css'],
			'csv' => ['csv'],
			'eps' => ['eps'],
			'html' => ['html', 'htm'],
			'jpg' => ['jpg', 'jpeg'],
			'mov' => ['mov', 'mp4', '3gp'],
			'mp3' => ['mp3'],
			'pdf' => ['pdf'],
			'png' => ['png'],
			'ppt' => ['ppt', 'pptx'],
			'rar' => ['rar'],
			'raw' => ['raw'],
			'ttf' => ['ttf'],
			'txt' => ['txt'],
			'wav' => ['wav'],
			'xls' => ['xls', 'xlsx'],
			'zip' => ['zip'],
			'doc' => ['docx', 'doc']
		];
		$file_name_arr = explode('.', $file_name);
		if (is_array($file_name_arr)) {
			foreach ($extension_list as $ext => $list_ext) {
				if (in_array(end($file_name_arr), $list_ext)) {
					return BASE_ASSET . 'img/icon/' . $ext . '.png';
				}
			}
		}
		return BASE_ASSET . 'img/icon/any.png';
	}
}
function callApiWithURL($url, $data = false, $username = '', $password = '')
{
	if ($username && $password) {
		$process = curl_init();
		curl_setopt($process, CURLOPT_URL, $url);
		curl_setopt($process, CURLOPT_USERPWD, $username . ":" . $password);
		curl_setopt($process, CURLOPT_TIMEOUT, 30);
		curl_setopt($process, CURLOPT_SSL_VERIFYPEER, false);
		if (is_array($data) && count($data) > 0) {
			$data = http_build_query($data);
			curl_setopt($process, CURLOPT_POST, 1);
			curl_setopt($process, CURLOPT_POSTFIELDS, $data);
		}
		curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
		$return = curl_exec($process);
		curl_close($process);
	} else {
		$return = file_get_contents($url);
	}
	return $return;
}
function callApi($url, $auth, $endpoint, $method, $param = [])
{
	$client = new Client([
		'base_uri' => $url,
		'auth' => $auth
	]);
	$response = $client->request($method, $endpoint, [
		'query' => $param
	]);

	$data = json_decode($response->getBody());
	return obj2arr($data);
}

function getInfoMhs($nim, $smt = 0)
{
	$ci = &get_instance();
	$smt = getCurrTBakademik();
	$auth = $ci->config->item('authpagoda');
	$param['nim'] = $nim;
	$param['smt'] = $smt[0]['smt'];
	return callApi($ci->config->item('apipagoda'), $auth, 'getInfoMhs', 'GET', $param);
}

function getCurrTBakademik()
{
	$ci = &get_instance();
	$auth = $ci->config->item('authpagoda');
	return callApi($ci->config->item('apipagoda'), $auth, 'getCurrTBakademik', 'GET');
}
function getAKMMahasiswa($nims)
{
	$ci = &get_instance();
	$auth = $ci->config->item('authpagoda');
	$param['nim'] = $nims;
	$smt = getCurrTBakademik();
	$param['smt'] = $smt[0]['smt'];
	return callApi($ci->config->item('apipagoda'), $auth, 'getAKMfromList', 'POST', $param);
}

function callApiWithCurl($url, $data = false, $username = '', $password = '')
{
	if (!function_exists('curl_init')) {
		die('CURL is not installed!');
	}
	$process = curl_init();
	curl_setopt($process, CURLOPT_URL, $url);
	curl_setopt($process, CURLOPT_USERPWD, $username . ":" . $password);
	curl_setopt($process, CURLOPT_TIMEOUT, 30);
	curl_setopt($process, CURLOPT_SSL_VERIFYPEER, false);
	if (is_array($data) && count($data) > 0) {
		$data = http_build_query($data);
		curl_setopt($process, CURLOPT_POST, 1);
		curl_setopt($process, CURLOPT_POSTFIELDS, $data);
	}
	curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
	$output = curl_exec($process);
	curl_close($process);
	return $output;
}
function csrf_token()
{
	$CI = &get_instance();
	$csrf = array(
		'name' => $CI->security->get_csrf_token_name(),
		'hash' => $CI->security->get_csrf_hash()
	);
	return "'{$csrf['name']}':'{$csrf['hash']}'";
}

if (!function_exists('check_is_image_ext')) {
	function check_is_image_ext($file_name = '')
	{
		$extension_list = [
			'jpg' => ['jpg', 'jpeg'],
			'png' => ['png']
		];
		$file_name_arr = explode('.', $file_name);
		if (is_array($file_name_arr)) {
			foreach ($extension_list as $ext => $list_ext) {
				if (in_array(end($file_name_arr), $list_ext)) {
					return $file_name;
				}
			}
		}
		return get_icon_file($file_name);
	}
}

if (!function_exists('build_rules')) {
	function build_rules($delimiter = '|', $rules = [])
	{
		if (count($rules)) {
			return $delimiter . implode($delimiter, $rules);
		}
	}
}

if (!function_exists('_ent')) {
	function _ent($string = null)
	{
		return htmlentities($string);
	}
}

if (!function_exists('dd')) {
	function dd($array)
	{
		echo '<pre>';
		print_r($array);
		echo '</pre>';
	}
}

if (!function_exists('get_captcha')) {
	function get_captcha($string = null)
	{
		$ci = &get_instance();
		$ci->load->helper('captcha');
		$vals = array(
			'img_path'      => './captcha/',
			'img_url'       => base_url('/captcha/'),
			'font_path'     => FCPATH . '/asset/font/captcha.ttf',
			'img_width'     => '150',
			'img_height'    => 30,
			'expiration'    => 7200,
			'word_length'   => 4,
			'font_size'     => 15,
			'img_id'        => 'image-captcha',
			'pool'          => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
			// White background and border, black text and red grid
			'colors'        => array(
				'background' => array(255, 255, 255),
				'border' => array(255, 255, 255),
				'text' => array(0, 0, 0),
				'grid' => array(190, 190, 190)
			)
		);
		$cap = create_captcha($vals);
		$expiration = time() - 7200; // Two hour limit
		$ci->db->where('captcha_time < ', $expiration)
			->delete('captcha');
		$data = array(
			'captcha_time'  => $cap['time'],
			'ip_address'    => $ci->input->ip_address(),
			'word'          => $cap['word']
		);
		$query = $ci->db->insert_string('captcha', $data);
		$ci->db->query($query);
		return $cap;
	}
}

if (!function_exists('display_block_element')) {
	function display_block_element()
	{
		$ci = &get_instance();
		$ci->load->database();
		$result = $ci->db->query("SELECT * FROM `page_block_element` GROUP BY `group_name` ")->result();
		$childs = $ci->db->query("SELECT * FROM `page_block_element`")->result();
		$child_list = [];
		foreach ($childs as $row) {
			$child_list[$row->group_name][] = $row;
		}
		$ret = null;
		foreach ($result as $row) {
			$ret .= '<li><a href="#">' . ucwords($row->group_name) . '</a>';
			if (isset($child_list[$row->group_name])) {
				$ret .= '<ul>';
				foreach ($child_list[$row->group_name] as $child) {
					$ret .= '<li class="block-item" data-src="' . $child->content . '" data-block-name="' . $child->block_name . '">
				                <div class="nav-content-wrapper noselect">
				                  <i class="fa fa-gear"></i>
				                  <div class="tool-nav delete">
				                    <i class="fa fa-trash"></i> <span class="info-nav">Delete</span>
				                  </div>
				                  <div class="tool-nav source">
				                    <i class="fa fa-code"></i> <span class="info-nav">Source</span>
				                  </div>
				                  <div class="tool-nav copy">
				                    <i class="fa fa-copy"></i> <span class="info-nav">Copy</span>
				                  </div>
				                  <div class="tool-nav handle">
				                    <i class="fa fa-arrows"></i> <span class="info-nav">Move</span>
				                  </div>
				                </div>
				              <img src="' . BASE_ASSET . 'img/header10.png" data-src="aadas/asdasd.html" class="preview-only">
				              <div id="element' . $child->id . '" class="block-content"><div class="edit"></div></div>
				            </li>';
				}
				$ret .= '</ul>';
			}
			$ret .= '</li>';
		}
		return $ret;
	}
}

if (!function_exists('get_extensions')) {
	function get_extensions($type = false)
	{
		$ci = &get_instance();
		$ci->load->helper('directory');
		$ext_paths[] = FCPATH . 'cc-content/extensions/';
		foreach (Modules::$locations as $path => $dir) {
			$path_items = directory_map($path, 3);
			foreach ($path_items as $path_item => $sub_path_item) {
				$check = $path . $path_item . '/extensions/';
				if (is_dir($check)) {
					$ext_paths[] = $check;
				}
			}
		}
		$list_extension = [];
		foreach ($ext_paths as $ext_path) {
			$dir = directory_map($ext_path, 2);
			foreach ($dir as $dirname => $childs) {
				if (is_file($ext_path . $dirname . '/ext.json')) {
					$ext_info = file_get_contents($ext_path . $dirname . '/ext.json');
					$ext_info_array = json_decode($ext_info);
					$ext_info_array->path = $ext_path . $dirname;
					$ext_info_array->dirname = $dirname;
					$list_extension[$ext_info_array->type][] = $ext_info_array;
				}
			}
		}
		if ($type !== false) {
			if (isset($list_extension[$type])) {
				return $list_extension[$type];
			}
		} else {
			return $list_extension;
		}
		return false;
	}
}

if (!function_exists('get_installed_extension')) {
	function get_installed_extension()
	{
		$ci = &get_instance();
		$ci->load->library('cc_extension');
		$extensions = $ci->cc_extension->getExtensions();
		$actived = [];
		foreach ($extensions as $extension) {
			if (is_dir($extension->item->path)) {
				$actived[] = $extension->item->regid;
			}
		}
		return $actived;
	}
}

if (!function_exists('get_page_element')) {
	function get_page_element($group = false)
	{
		$ci = &get_instance();
		$ci->cc_page_element->get_page_element();
	}
}

if (!function_exists('load_extensions')) {
	function load_extensions()
	{
		$ci = &get_instance();
		$ci->load->helper('directory');
		$ci->load->library('cc_extension');
		$list_extensions = get_extensions();
		if (!is_array($list_extensions)) {
			return false;
		}
		$ext_load = null;
		$cc_core = get_instance();
		$current_uri = $ci->uri->uri_string;
		foreach ($list_extensions as $type => $extensions) {
			foreach ($extensions as $ext) {
				if (isset($ext->loader)) {
					if (isset($ext->routes)) {
						foreach ($ext->routes as $route) {
							// Convert wildcards to RegEx
							$route = str_replace(array(':any', ':num'), array('[^/]+', '[0-9]+'), $route);
							if (preg_match('#^' . $route . '$#', $current_uri, $matches)) {
								foreach ($ext->loader as $filename) {
									if (is_file($ext->path . $filename)) {
										if ($ci->input->method()) {
											$ccExtension = new Cc_extension_item($ext);
											include  $ext->path . $filename;
										}
									}
								}
							}
						}
					} else {
						foreach ($ext->loader as $filename) {
							if (is_file($ext->path . $filename)) {
								if (file_exists($ext->path . 'actived')) {
									debug('loaded' . $ext->path . $filename);
									if ($ci->input->method() == 'get') {
										$ccExtension = new Cc_extension_item($ext);
										include  $ext->path . $filename;
									} else {
										$ccExtension = new Cc_extension_item($ext);
										ob_start();
										include  $ext->path . $filename;
										$buffer = ob_get_contents();
										ob_end_clean();
									}
								}
							} else {
								debug('not loaded' . $ext->path . $filename);
							}
						}
					}
				}
			}
		}
		return false;
	}
}

if (!function_exists('url_extension')) {
	function url_extension($ext = null)
	{
		return BASE_URL . 'cc-content/extensions/' . $ext;
	}
}

if (!function_exists('get_option')) {
	function get_option($option_name = null, $default = null)
	{
		$ci = &get_instance();
		$ci->load->library('cc_app');
		return $ci->cc_app->getOption($option_name, $default);
	}
}

if (!function_exists('add_option')) {
	function add_option($option_name = null, $option_value = null)
	{
		$ci = &get_instance();
		$ci->load->library('cc_app');
		return $ci->cc_app->addOption($option_name, $option_value);
	}
}

if (!function_exists('set_option')) {
	function set_option($option_name = null, $option_value = null)
	{
		$ci = &get_instance();
		return $ci->cc_app->setOption($option_name, $option_value);
	}
}

if (!function_exists('delete_option')) {
	function delete_option($option_name = null)
	{
		$ci = &get_instance();
		return $ci->cc_app->deleteOption($option_name);
	}
}

if (!function_exists('option_exists')) {
	function option_exists($option_name = null)
	{
		$ci = &get_instance();
		return $ci->cc_app->optionExists($option_name);
	}
}

if (!function_exists('theme_url')) {
	function theme_url($url_additional = null)
	{
		$ci = &get_instance();
		$active_theme = get_option('active_theme', 'cicool');

		return BASE_URL . 'cc-content/themes/' . $active_theme . '/' . $url_additional;
	}
}

if (!function_exists('theme_asset')) {
	function theme_asset()
	{
		return theme_url('asset/');
	}
}

if (!function_exists('theme_zircos')) {
	function theme_zircos()
	{
		return theme_url('zircos_asset/');
	}
}

if (!function_exists('site_name')) {
	function site_name()
	{
		return get_option('site_name');
	}
}

if (!function_exists('installation_complete')) {
	function installation_complete()
	{
		return is_file(FCPATH . '/application/config/site.php');
	}
}

if (!function_exists('get_menu')) {
	function get_menu($menu_type = null)
	{
		$ci = &get_instance();
		$ci->load->database();
		$ci->load->model('menu/model_menu');
		if (is_numeric($menu_type)) {
			$menu_type_id = $menu_type;
		} else {
			$menu_type_id = $ci->model_menu->get_id_menu_type_by_flag($menu_type);
		}
		$menus = $ci->db
			->where(['menu_type_id' =>  $menu_type_id])
			->order_by('sort', 'ASC')
			->get('menu')
			->result();
		$menu_parents = $ci->db
			->where(['menu_type_id' => $menu_type_id, 'parent' => 0])
			->order_by('sort', 'ASC')
			->get('menu')
			->result();
		$new = array();
		foreach ($menus as $a) {
			$new[$a->parent][] = $a;
		}
		$news = array();
		$menus_tree = array();
		foreach ($menus as $a) {
			$news[$a->parent][] = $a;
		}
		foreach ($menu_parents as $new) {
			$menus_tree = array_merge($menus_tree, create_tree($news, array($new)));
		}
		return $menus_tree;
	}
}

if (!function_exists('create_tree')) {
	function create_tree(&$list, $parent)
	{
		$tree = array();
		foreach ($parent as $k => $l) {
			if (isset($list[$l->id])) {
				$l->children = create_tree($list, $list[$l->id]);
			}
			$tree[] = $l;
		}
		return $tree;
	}
}

if (!function_exists('get_header')) {
	function get_header()
	{
		$ci = &get_instance();
		return $ci->cc_app->getHeader();
	}
}
if (!function_exists('get_rightbar')) {
	function get_rightbar()
	{
		$ci = &get_instance();
		return $ci->cc_app->getRightbar();
	}
}

if (!function_exists('get_footer')) {
	function get_footer()
	{
		$ci = &get_instance();
		return $ci->cc_app->getFooter();
	}
}

if (!function_exists('get_navigation')) {
	function get_navigation()
	{
		$ci = &get_instance();
		return $ci->cc_app->getNavigation();
	}
}

if (!function_exists('get_view_component')) {
	function get_view_component($view = null, $data = [])
	{
		$ci = &get_instance();
		return $ci->template->build($view, $data, true);
	}
}

if (!function_exists('generate_key')) {
	function generate_key($length = 40)
	{
		$ci = &get_instance();
		$salt = base_convert(bin2hex($ci->security->get_random_bytes(64)), 16, 36);
		if ($salt === FALSE) {
			$salt = hash('sha256', time() . mt_rand());
		}
		$ci->load->config('config');
		$new_key = substr($salt, 0, $length);
		return $new_key;
	}
}

if (!function_exists('get_table_not_allowed_for_builder')) {
	function get_table_not_allowed_for_builder()
	{
		return [
			'aauth_group_to_group',
			'aauth_groups',
			'aauth_login_attempts',
			'aauth_perm_to_group',
			'aauth_perm_to_user',
			'aauth_perms',
			'aauth_pms',
			'aauth_user',
			'aauth_user_to_group',
			'aauth_user_variables',
			'aauth_users',
			'captcha',
			'cc_options',
			'cc_session',
			'crud',
			'crud_custom_option',
			'crud_field',
			'crud_field_validation',
			'crud_input_type',
			'crud_input_validation',
			'form',
			'form_custom_attribute',
			'form_custom_option',
			'form_field',
			'form_field_validation',
			'keys',
			'menu',
			'menu_icon',
			'menu_type',
			'migrations',
			'page',
			'page_block_element',
			'rest',
			'rest_field',
			'rest_field_validation',
			'rest_input_type',
			'cc_log',
			'cc_block_client',
			'cc_block',
			'cc_visitor'
		];
	}
}

if (!function_exists('app')) {
	function app()
	{
		return get_instance();
	}
}

if (!function_exists('getallheaders')) {
	function getallheaders()
	{
		$headers = '';
		foreach ($_SERVER as $name => $value) {
			if (substr($name, 0, 5) == 'HTTP_') {
				$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
			}
		}
		return $headers;
	}
}

if (!function_exists('cclang')) {
	function cclang($langkey = null, $params = [])
	{
		if (!is_array($params)) {
			$params = [$params];
		}
		$lang = lang($langkey);
		$idx = 1;
		foreach ($params as $value) {
			$lang = str_replace('$' . $idx++, $value, $lang);
		}
		$lang = preg_replace('/\$([0-9])/', '', $lang);
		if (!$lang) {
			return ucwords($langkey);
		}
		return $lang;
	}
}

if (!function_exists('get_langs')) {
	function get_langs()
	{
		return [
			[
				'folder_name' => 'english',
				'name' => 'English',
				'initial_name' => 'gb',
				'icon_name' => 'flag-icon-gb',
			],
			[
				'folder_name' => 'indonesian',
				'name' => 'Indonesian',
				'initial_name' => 'id',
				'icon_name' => 'flag-icon-id',
			],
			[
				'folder_name' => 'italian',
				'name' => 'Italian',
				'initial_name' => 'it',
				'icon_name' => 'flag-icon-it',
			],
			[
				'folder_name' => 'arabic',
				'name' => 'Arabic',
				'initial_name' => 'ar',
				'icon_name' => 'flag-icon-sa',
			],
			[
				'folder_name' => 'portuguese-brazilian',
				'name' => 'Brazil',
				'initial_name' => 'br',
				'icon_name' => 'flag-icon-br',
			],
			[
				'folder_name' => 'german',
				'name' => 'Germany',
				'initial_name' => 'de',
				'icon_name' => 'flag-icon-de',
			],
			[
				'folder_name' => 'french',
				'name' => 'France',
				'initial_name' => 'fr',
				'icon_name' => 'flag-icon-fr',
			],
			[
				'folder_name' => 'spanish',
				'name' => 'Spain',
				'initial_name' => 'es',
				'icon_name' => 'flag-icon-es',
			],
			[
				'folder_name' => 'russian',
				'name' => 'Russia',
				'initial_name' => 'ru',
				'icon_name' => 'flag-icon-ru',
			],
			[
				'folder_name' => 'japanese',
				'name' => 'Japan',
				'initial_name' => 'ja',
				'icon_name' => 'flag-icon-jp',
			],
			[
				'folder_name' => 'traditional-chinese',
				'name' => 'Chinese',
				'initial_name' => 'zh-TW',
				'icon_name' => 'flag-icon-cn',
			],
			[
				'folder_name' => 'turkish',
				'name' => 'Turkey',
				'initial_name' => 'tr',
				'icon_name' => 'flag-icon-tr',
			],
			[
				'folder_name' => 'dutch',
				'name' => 'Netherland',
				'initial_name' => 'nl',
				'icon_name' => 'flag-icon-nl',
			],
		];
	}
}

if (!function_exists('get_current_lang')) {
	function get_current_lang()
	{
		$ci = &get_instance();
		return get_cookie('language') ? get_cookie('language') : $ci->config->item('language');
	}
}

if (!function_exists('get_current_initial_lang')) {
	function get_current_initial_lang()
	{
		$current_lang = get_current_lang();
		foreach (get_langs() as $lang) {
			if ($current_lang == $lang['folder_name']) {
				return $lang['icon_name'];
			}
		}
	}
}

if (!function_exists('get_geolocation')) {

	function get_geolocation($ip)
	{
		/*$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}"));
		if (isset($details->country)) {
			return strtolower($details->country);
		}*/
		return 'gb';
	}
}

if (!function_exists('get_lang_by_ip')) {

	function get_lang_by_ip($ip)
	{
		$location = get_geolocation($ip);
		foreach (get_langs() as $key) {
			if ($key['initial_name'] == $location) {
				return $key['folder_name'];
			}
		}
		return 'english';
	}
}

if (!function_exists('debug')) {

	function debug($vars = null)
	{
		return get_instance()->console->debug($vars);
	}
}

if (!function_exists('cicool')) {
	function cicool()
	{
		app()->load->library('cc_app');
		return app()->cc_app->initialize();
	}
}

if (!function_exists('webPageUrl')) {
	function webPageUrl($page, $params = [])
	{
		$params = array_merge(['page' => $page], $params);
		return site_url('administrator/web-page?' . http_build_query($params));
	}
}

if (!function_exists('recurse_copy')) {
	function recurse_copy($src, $dst)
	{
		$dir = opendir($src);
		@mkdir($dst);
		while (false !== ($file = readdir($dir))) {
			if (($file != '.') && ($file != '..')) {
				if (is_dir($src . '/' . $file)) {
					recurse_copy($src . '/' . $file, $dst . '/' . $file);
				} else {
					copy($src . '/' . $file, $dst . '/' . $file);
				}
			}
		}
		closedir($dir);
	}
}

if (!function_exists('create_childern')) {

	function create_childern($childern, $parent, $tree)
	{
		foreach ($childern as $child) :
?>
			<option <?= $child->id == $parent ? 'selected="selected"' : ''; ?> value="<?= $child->id; ?>"><?= str_repeat('----', $tree) ?> <?= ucwords($child->label); ?></option>
			<?php if (isset($child->children) and count($child->children)) :
				$tree++;
			?>
				<?php create_childern($child->children, $parent, $tree); ?>
			<?php endif ?>
<?php endforeach;
	}
}

if (!function_exists('extendsObject')) {
	function extendsObject($obj = [])
	{
		return $obj;
	}
}

if (!function_exists('get_all_blog')) {
	function get_all_blog()
	{
		return db_get_all_data('blog', ['status' => 'publish']);
	}
}

if (!function_exists('parse_nav_url')) {
	function parse_nav_url($url)
	{
		return str_replace([
			'{admin_url}'
		], [
			ADMIN_NAMESPACE_URL
		], $url);
	}
}

if (!function_exists('admin_url')) {
	function admin_url($url = null)
	{
		return base_url(ADMIN_NAMESPACE_URL . '/' . $url);
	}
}

if (!function_exists('currency_format')) {
	function currency_format($number = '')
	{
		$thausand_separator = get_option('ecommerce_thausand_separator', '.');
		$decimal_separator = get_option('ecommerce_decimal_separator', ',');
		$decimal_length = get_option('ecommerce_decimal_length', 0);
		$text = 'IDR ' . number_format($number, $decimal_length, $decimal_separator, $thausand_separator);
		return $text;
	}
}
function format_date($date, $format)
{
	$date = date_create($date);
	return date_format($date, $format);
}

if (!function_exists('get_theme_list')) {
	function get_theme_list()
	{
		$result = directory_map('cc-content/themes', 1);
		$result = array_map(function ($item) {
			return str_replace(DIRECTORY_SEPARATOR, '', $item);
		}, $result);
		return $result;
	}
}

if (!function_exists('get_message')) {
	function get_message($id)
	{
		app()->load->model('model_message');
		return app()->model_message->find($id);
	}
}

if (!function_exists('now')) {
	function now()
	{
		return date('Y-m-d H:i:s');
	}
}

if (!function_exists('now_utc')) {
	function now_utc()
	{
		date_default_timezone_set('UTC');
		return date('Y-m-d H:i:s');
	}
}

if (!function_exists('join_multi_select')) {
	function join_multi_select(
		$values = null,
		$relation_table = null,
		$relation_value = null,
		$relation_label = null
	) {
		if ($values) {
			$result = app()->db
				->select($relation_label)
				->where_in($relation_value, explode(',', $values))
				->get($relation_table)->result();
			$labels = [];
			foreach ($result as $row) {
				$labels[] = $row->{$relation_label};
			}
			return implode(',', $labels);
		}
		return '-';
	}
}

if (!function_exists('is_dir_empty')) {
	function is_dir_empty($dir)
	{
		if (!is_readable($dir)) return true;
		return (count(scandir($dir)) == 2);
	}
}

if (!function_exists('get_user_first_group')) {
	function get_user_first_group($user_id = null)
	{
		app()->load->library('aauth');
		foreach (app()->aauth->get_user_groups($user_id) as $row) {
			if (strtolower($row->name) != 'default') {
				return $row;
			}
		}
		return false;
	}
}
function build_option($data)
{
	$option = "<option value=''></option>";
	foreach ($data as $row) {
		$option .= "<option value='$row->id'>$row->name</option>";
	}
	return $option;
}
function delete_col(&$array, $key)
{
	return array_walk($array, function (&$v) use ($key) {
		unset($v[$key]);
	});
}
function cek_isi_survey()
{
	$data = db_get_all_data('survey_data', ['username' => get_user_name()->username]);
	if (!empty($data)) return;
	else redirect('survey');
}
function cartesian(array $input)
{
	$result = [[]];
	foreach ($input as $key => $values) {
		$append = [];
		foreach ($values as $value) {
			foreach ($result as $data) {
				$append[] = $data + [$key => $value];
			}
		}
		$result = $append;
	}
	return $result;
}
function array_semester($a)
{
	$b = cartesian($a);
	return array_map(function ($v) {
		return $v[0] . $v[1];
	}, $b);
}
function keyBy($array, $key)
{
	$result = [];

	foreach ($array as $item) {
		if (isset($item[$key])) {
			$result[$item[$key]] = $item;
		}
	}

	return $result;
}
function export_excel($result, $subject = 'file', $selected_field = [], $format = [])
{
	$ci = &get_instance();
	$ci->load->library('excel');

	$ci->excel->setActiveSheetIndex(0);

	if ($result instanceof CI_DB_mysqli_result) {
		if (count($selected_field))
			// $fields = array_intersect($result->list_fields(), $selected_field);
			$fields = $selected_field;
		else $fields = $result->list_fields();
	} else $fields = $selected_field;

	$alphabet = 'ABCDEFGHIJKLMOPQRSTUVWXYZ';
	$alphabet_arr = str_split($alphabet);
	$column = [];

	foreach ($alphabet_arr as $alpha) {
		$column[] =  $alpha;
	}

	foreach ($alphabet_arr as $alpha) {
		foreach ($alphabet_arr as $alpha2) {
			$column[] =  $alpha . $alpha2;
		}
	}
	foreach ($alphabet_arr as $alpha) {
		foreach ($alphabet_arr as $alpha2) {
			foreach ($alphabet_arr as $alpha3) {
				$column[] =  $alpha . $alpha2 . $alpha3;
			}
		}
	}

	foreach ($column as $col) {
		$ci->excel->getActiveSheet()->getColumnDimension($col)->setWidth(20);
	}

	$col_total = $column[count($fields) - 1];

	//styling
	$ci->excel->getActiveSheet()->getStyle('A1:' . $col_total . '1')->applyFromArray(
		array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'DA3232')
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			)
		)
	);

	$phpColor = new PHPExcel_Style_Color();
	$phpColor->setRGB('FFFFFF');

	$ci->excel->getActiveSheet()->getStyle('A1:' . $col_total . '1')->getFont()->setColor($phpColor);

	$ci->excel->getActiveSheet()->getRowDimension(1)->setRowHeight(40);

	$ci->excel->getActiveSheet()->getStyle('A1:' . $col_total . '1')
		->getAlignment()->setWrapText(true);

	$col = 0;
	foreach ($fields as $field) {

		$ci->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, ucwords(str_replace('_', ' ', $field)));
		$col++;
	}

	$row = 2;
	if ($result instanceof CI_DB_mysqli_result) $result = $result->result();
	foreach ($result as $data) {
		$col = 0;
		foreach ($fields as $field) {
			$datafield = (is_object($data) ? $data->$field : $data[$fields[$col]]);
			if (isset($format[$field])) {
				switch ($format[$field]) {
					case 'link':
						# code...
						if ($datafield != '') {
							$ci->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $datafield);
							$ci->excel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getHyperlink()->setUrl(site_url('file/' . $datafield));
						}
						break;
					default:
						# code...
						break;
				}
			} else
				$ci->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $datafield);
			$col++;
		}

		$row++;
	}

	//set border
	$styleArray = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);
	$ci->excel->getActiveSheet()->getStyle('A1:' . $col_total . '' . $row)->applyFromArray($styleArray);

	$ci->excel->getActiveSheet()->setTitle(ucwords($subject));

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename=' . ucwords($subject) . '-' . date('Y-m-d') . '.xls');
	header('Cache-Control: max-age=0');
	header('Cache-Control: max-age=1');

	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
	header('Cache-Control: cache, must-revalidate');
	header('Pragma: public');

	$objWriter = PHPExcel_IOFactory::createWriter($ci->excel, 'Excel5');
	$objWriter->save('php://output');
}
