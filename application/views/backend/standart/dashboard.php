<style type="text/css">
	.widget-user-header {
		padding-left: 20px !important;
	}
</style>

<link rel="stylesheet" href="<?= BASE_ASSET; ?>admin-lte/plugins/morris/morris.css">

<section class="content-header">
	<h1>
		<?= cclang('dashboard') ?>
		<small>

			<?= cclang('control_panel') ?>
		</small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="#">
				<i class="fa fa-dashboard">
				</i>
				<?= cclang('home') ?>
			</a>
		</li>
		<li class="active">
			<?= cclang('dashboard') ?>
		</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<!-- 16:9 aspect ratio -->
		<div class="col-md-12">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe class="embed-responsive-item" src="https://app.powerbi.com/view?r=eyJrIjoiYmE1NTI3OGMtMDY1OS00N2Y2LThlYzYtYWFhZGNiOWM4MTU5IiwidCI6IjI5ZTVlYTIwLWRhYWEtNDI5MS1hMmFlLTg3NGU2M2MxZjI5MiIsImMiOjEwfQ%3D%3D&pageName=ReportSection"></iframe>
			</div>
		</div>
	</div>
	<div class="row">
		<!-- 16:9 aspect ratio -->
		<div class="col-md-12">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe class="embed-responsive-item" src="https://app.powerbi.com/view?r=eyJrIjoiOTM2NzkzY2MtZDk5ZC00ZTQ1LWE3NWEtYTlkNDc1MmY4ZjZmIiwidCI6IjI5ZTVlYTIwLWRhYWEtNDI5MS1hMmFlLTg3NGU2M2MxZjI5MiIsImMiOjEwfQ%3D%3D&pageName=ReportSection"></iframe>
			</div>
		</div>
	</div>

	<!-- /.row -->
	<?php cicool()->eventListen('dashboard_content_bottom'); ?>

</section>
<!-- /.content -->