<form action="" method="POST" role="form">
   <div class="row">
	<?php foreach($this->crud_builder->getFieldShowInColumn(true) as $input){?>
      <div class="col-md-8">
         <div class="form-group">
            <label for=""><?= $input['name']?></label>
            {php_open_tag_echo} form_dropdown('<?= $input['name']?>', $<?= $input['name']?>, fromsess('filterdata_{table_name}')['<?= $input['name']?>'], ['class' => 'form-control','style' => 'width:100%']) {php_close_tag}
         </div>
      </div>
	<?php }?>
   </div>
</form>

<script>
	$(function() {
		// $('select[name=name1],select[name=name2],select[name=name3]').select2({
		// 	theme: 'classic',
		// 	dropdownParent: $('#filter{table_name}-dialog'),
		// });
		// $('select[name=name4]').select2({
		// 	tags:true
		// })
	})
</script>