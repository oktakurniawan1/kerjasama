<?php
defined('BASEPATH') or exit('No direct script access allowed');

use GuzzleHttp\Client;

class CI_Removebg_service
{

	protected $client;

	public function __construct()
	{
		$this->client = new Client([
			'base_uri' => 'removebg:5000/',
		]);
	}

	public function removeBg($file)
	{
		try {
			$response = $this->client->request('POST', 'remove-background', [
				'multipart' => [
					[
						'name'     => 'file',
						'contents' => fopen($file, 'r'),
					],
				],
			]);

			return $response->getBody();
		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}
}
