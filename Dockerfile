FROM php:7.2.34-apache

# With the Dockerfile
# ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
# RUN chmod +x /usr/local/bin/install-php-extensions && \
#     install-php-extensions gd xdebug 

# With curl
RUN curl -sSLf \
	-o /usr/local/bin/install-php-extensions \
	https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions && \
	chmod +x /usr/local/bin/install-php-extensions


RUN apt-get -y update 
# RUN apt-get install -y cron 
RUN apt-get install -y nano  
RUN apt-get install -y default-mysql-client  
RUN apt-get install -y unzip  
RUN apt-get install -y zip
RUN apt-get install -y wkhtmltopdf 
RUN apt-get install -y vim 
RUN apt-get install -y supervisor 
RUN apt-get install -y libreoffice-core 
RUN apt-get install -y unoconv
RUN apt-get install -y git 
RUN apt-get install -y sudo
RUN apt-get install -y curl
RUN apt-get install -y wget
# RUN apt-get install -y apache2
RUN a2enmod rewrite
# Copying the script from a Docker image
# COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN install-php-extensions gd
RUN install-php-extensions imagick
RUN install-php-extensions xdebug
RUN install-php-extensions mysqli
RUN install-php-extensions zip
RUN install-php-extensions pdo_mysql
RUN install-php-extensions mcrypt
RUN install-php-extensions memcache
RUN install-php-extensions curl
RUN install-php-extensions json
RUN install-php-extensions mbstring
RUN install-php-extensions xml
RUN install-php-extensions bcmath
RUN install-php-extensions readline
RUN install-php-extensions opcache
RUN install-php-extensions @composer


# Create new web user for apache and grant sudo without password
RUN useradd -m web -d /home/web -g www-data -s /bin/bash
RUN usermod -aG sudo web
RUN echo 'web ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# cleaning
RUN rm -rf /var/cache/apt/archives && \
	rm -rf /var/lib/apt/lists/* && \
	apt-get clean && \
	apt-get autoremove  && \
	apt-get -q autoclean