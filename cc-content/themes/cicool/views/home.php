<?= get_header(); ?>

<body id="page-top">
	<?= get_navigation(); ?>

	<header>
		<div class="header-content">
			<div class="header-content-inner">
				<h2 id="homeHeading">Selamat datang di Sistem Informasi Kerjasama (SIMKS)</h2>
				<p> Untuk masuk sistem silahkan klik tombol sign in, aplikasi ini juga bisa dilihat di laman <br><code><?= 'apps.unnes.ac.id' ?> </code></p>
				<a href="#" class="btn btn-primary btn-xl page-scroll" target="blank"><i class="fa fa-smile-o"></i> Simple Dashboard</a>
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#mou">Mou</a></li>
					<li><a data-toggle="tab" href="#unit">Kerjasama Unit</a></li>
				</ul>
				<div class="tab-content">
					<div id="mou" class="tab-pane fade in active">
						<div class="row">
							<div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" src="https://app.powerbi.com/view?r=eyJrIjoiYmE1NTI3OGMtMDY1OS00N2Y2LThlYzYtYWFhZGNiOWM4MTU5IiwidCI6IjI5ZTVlYTIwLWRhYWEtNDI5MS1hMmFlLTg3NGU2M2MxZjI5MiIsImMiOjEwfQ%3D%3D&pageName=ReportSection"></iframe>
							</div>
						</div>
					</div>
					<div id="unit" class="tab-pane fade">
						<div class="row">
							<div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" src="https://app.powerbi.com/view?r=eyJrIjoiOTM2NzkzY2MtZDk5ZC00ZTQ1LWE3NWEtYTlkNDc1MmY4ZjZmIiwidCI6IjI5ZTVlYTIwLWRhYWEtNDI5MS1hMmFlLTg3NGU2M2MxZjI5MiIsImMiOjEwfQ%3D%3D&pageName=ReportSection"></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<?= get_footer(); ?>