<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> <?= isset($title) ? $title : site_name() ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?= get_option('site_description'); ?>">
    <meta name="keywords" content="<?= get_option('keywords'); ?>">
    <meta name="author" content="<?= get_option('author'); ?>">
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?= theme_zircos(); ?>images/favicon.ico">

    <!-- App css -->
    <link href="<?= theme_zircos(); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" id="bootstrap-stylesheet" />
    <link href="<?= theme_zircos(); ?>css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= theme_zircos(); ?>css/app.min.css" rel="stylesheet" type="text/css" id="app-stylesheet" />
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>flag-icon/css/flag-icon.css" rel="stylesheet" media="all" />
    <script src="<?= theme_zircos(); ?>js/vendor.min.js"></script>
</head>