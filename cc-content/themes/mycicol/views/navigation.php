<!-- Navigation Bar-->
<header id="topnav">
    <!-- Topbar Start -->
    <div class="navbar-custom">
        <div class="container-fluid">
            <ul class="list-unstyled topnav-menu float-right mb-0">

                <!-- seting bahasa -->
                <li class="dropdown d-none d-lg-block">
                    <a class="nav-link dropdown-toggle mr-0" data-toggle="dropdown" href="#" role="button"
                        aria-haspopup="false" aria-expanded="false">
                        <span class="flag-icon <?= get_current_initial_lang(); ?> mr-2"></span>
                        <?= get_current_lang(); ?> </a>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <?php foreach (get_langs() as $lang) : ?>
                        <a href="<?= site_url('web/switch_lang/' . $lang['folder_name']); ?>"
                            class="dropdown-item notify-item">
                            <span
                                class="flag-icon <?= $lang['icon_name']; ?> mr-2"></span><?= $lang['name']; ?></span></a>
                        <?php endforeach; ?>
                    </div>
                </li>
                <?php if (app()->aauth->is_loggedin()) { ?>
                <li class="dropdown notification-list">
                    <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown"
                        href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <img src="<?= BASE_URL . 'uploads/user/' . (!empty(get_user_data('avatar')) ? get_user_data('avatar') : 'default.png'); ?>"
                            class="rounded-circle" alt="User Image">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                        <!-- item-->
                        <div class="dropdown-header noti-title">
                            <h6 class="text-overflow m-0"><?= cclang('welcome'); ?></h6>
                        </div>

                        <!-- item-->
                        <a href="<?= site_url('administrator/dashboard'); ?>" class="dropdown-item notify-item">
                            <i class="mdi mdi-monitor-dashboard"></i>
                            <span><?= cclang('dashboard'); ?></a></span>
                        </a>
                        <!-- item-->
                        <a href="<?= site_url('administrator/user/profile'); ?>" class="dropdown-item notify-item">
                            <i class="mdi mdi-account-outline"></i>
                            <span><?= cclang('profile'); ?></a></span>
                        </a>

                        <div class="dropdown-divider"></div>
                        <!-- item-->
                        <a href="<?= site_url('administrator/auth/logout'); ?>" class="dropdown-item notify-item">
                            <i class="mdi mdi-logout-variant"></i>
                            <span><?= cclang('logout'); ?></span>
                        </a>
                    </div>
                </li>
                <?php } else { ?>
                <li class="dropdown notification-list">
                    <a href="<?= site_url('administrator/auth/login'); ?>" class="nav-link">
                        <i class="mdi mdi-login noti-icon"></i>
                    </a>
                </li>
                <?php } ?>

            </ul>

            <!-- LOGO -->
            <div class="logo-box">
                <a href="<?= site_url('index.php')?>" class="logo text-center">
                    <span class="logo-lg">
                        <!-- <img src="<?= theme_zircos(); ?>images/logo.png" alt="" height="30"> -->
                        <span class="logo-lg-text-light"><?= isset($title) ? $title : site_name() ?></span>
                    </span>
                    <span class="logo-sm">
                        <!-- <span class="logo-sm-text-dark">Z</span> -->
                        <img src="<?= theme_zircos(); ?>images/logo-sm.png" alt="" height="22">
                    </span>
                </a>
            </div>
            <!-- <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
    
                                <li class="d-none d-sm-block">
                                    <form class="app-search">
                                        <div class="app-search-box">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Search...">
                                                <div class="input-group-append">
                                                    <button class="btn" type="submit">
                                                        <i class="fas fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </li>
                            </ul> -->
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- end Topbar -->

    <div class="topbar-menu">
        <div class="container-fluid">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">

                    <li class="has-submenu">
                        <a href="<?= site_url('index.php')?>"> <i
                                class="mdi mdi-view-dashboard"></i><?= cclang('home'); ?></a>
                    </li>
                    <li class="has-submenu">
                        <a href="#"> <i class="mdi mdi-layers"></i>Data Statistik
                        </a>
                        <ul class="submenu">
                            <li><a href="<?= site_url('front/statistik_fakultas')?>">Diagram Fakultas</a></li>
                            <li><a href="blogs-blog-list.html">Diagram Berdasarkan Jenis Kerjasama</a></li>
                        </ul>
                    </li>
                    <li class="has-submenu">
                        <a href="#">
                            <i class="mdi mdi-table"></i>Rekap Kerjasama
                        </a>
                    </li>
                    <li class="has-submenu">
                        <a href="#">
                            <i class="mdi mdi-home-export-outline"></i>Rekap Implementasi Unit
                        </a>
                    </li>
                    <li class="has-submenu">
                        <a href="#">
                            <i class="mdi mdi-account-box"></i>Lecture Mobility
                        </a>
                    </li>

                    <li class="has-submenu">
                        <a href="<?= site_url('front/form_regstudent_mob')?>">
                            <i class="mdi mdi-account-circle"></i>Student Mobility
                        </a>
                    </li>

                </ul>
                <!-- End navigation menu -->

                <div class="clearfix"></div>
            </div>
            <!-- end #navigation -->
        </div>
        <!-- end container -->
    </div>
    <!-- end navbar-custom -->
</header>
<!-- End Navigation Bar-->
<script src="<?= theme_zircos(); ?>js/vendor.min.js"></script>
