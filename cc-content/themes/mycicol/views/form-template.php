<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/AdminLTE.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/morris/morris.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/all.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/sweet-alert/sweetalert.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/toastr/build/toastr.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/fancy-box/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/chosen/chosen.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/css/custom.css?timestamp=201803311526">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>datetimepicker/jquery.datetimepicker.css" />
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>js-scroll/style/jquery.jscrollpane.css" rel="stylesheet"
        media="all" />
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>flag-icon/css/flag-icon.css" rel="stylesheet" media="all" />
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/icheck.min.js"></script>
    <!-- <script src="<?= BASE_ASSET; ?>/sweet-alert/sweetalert-dev.js"></script> -->
    <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <script src="<?= BASE_ASSET; ?>/toastr/toastr.js"></script>
    <script src="<?= BASE_ASSET; ?>/fancy-box/source/jquery.fancybox.js?v=2.1.5"></script>
    <script src="<?= BASE_ASSET; ?>/datetimepicker/build/jquery.datetimepicker.full.js"></script>
    <script src="<?= BASE_ASSET; ?>/editor/dist/js/medium-editor.js"></script>
    <!-- <script src="<?= BASE_ASSET; ?>js/cc-extension.js"></script> -->
    <!-- <script src="<?= BASE_ASSET; ?>/js/cc-page-element.js"></script> -->
    <!-- <script src="<?= BASE_ASSET; ?>/stepper/jquery.steps.min.js"></script> -->
    <!-- <link href="<?= BASE_ASSET; ?>/stepper/css/jquery.steps.css" rel="stylesheet"> -->
    <script>
        var BASE_URL = "<?= base_url(); ?>";
        var HTTP_REFERER = "<?= isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/'; ?>";
        var csrf = '<?= $this->security->get_csrf_token_name(); ?>';
        var token = '<?= $this->security->get_csrf_hash(); ?>';

        $(document).ready(function () {

            toastr.options = {
                "positionClass": "toast-top-center",
            }

            var f_message = '<?= $this->session->flashdata('
            f_message '); ?>';
            var f_type = '<?= $this->session->flashdata('
            f_type '); ?>';

            if (f_message.length > 0) {
                toastr[f_type](f_message);
            }

            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
        });
    </script>
</head>

<body id="page-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-body ">
                        <!-- Widget: user widget style 1 -->
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <h3 class="widget-user-username"><?= $title?></h3>
                            <hr>
                        </div>
                        <?= $form ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js" type="text/javascript">
    </script>
    <script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
    <!-- <script src="<//?= BASE_ASSET; ?>jquery-switch-button/jquery.switchButton.js"></script> -->
    <!-- <script src="<//?= BASE_ASSET; ?>/js/jquery.ui.touch-punch.js"></script> -->
    <!-- <script src="<//?= BASE_ASSET; ?>/admin-lte/bootstrap/js/bootstrap.min.js"></script> -->
    <!-- <script src="<//?= BASE_ASSET; ?>/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js"></script> -->
    <!-- <script src="<//?= BASE_ASSET; ?>/admin-lte/plugins/fastclick/fastclick.js"></script> -->
    <!-- <script src="<//?= BASE_ASSET; ?>/admin-lte/dist/js/app.min.js"></script> -->
    <!-- <script src="<//?= BASE_ASSET; ?>/admin-lte/dist/js/adminlte.js"></script> -->
    <!-- <script src="<//?= BASE_ASSET; ?>js-scroll/script/jquery.jscrollpane.min.js"></script> -->
    <!-- <script src="<//?= BASE_ASSET; ?>jquery-switch-button/jquery.switchButton.js"></script> -->
    <!-- <script src="<//?= BASE_ASSET; ?>/js/custom.js"></script> -->
    <script src="<?= BASE_ASSET; ?>js/jquery.hotkeys.js"></script>
</body>

</html>