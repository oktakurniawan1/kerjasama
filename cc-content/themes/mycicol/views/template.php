<?= get_header(); ?>
	 <body data-layout="horizontal">
	 <?= get_navigation(); ?>
		  <div id="wrapper">
				<div class="content-page">
					 <div class="content">
						<!-- Start Content-->
						<div class="container-fluid">
					 	<?= $content ?>
						</div>
					 </div>
					 <?= get_footer(); ?>
				</div>
				<!-- End Page content -->
		  </div>
		  <?= get_rightbar(); ?>
	 </body>
</html>